# Xuc

XiVOcc / XiVOuc integration server

## Install dependencies

```bash
git clone git@gitlab.com:xivoxc/play-authentication.git
cd play-authentication
git checkout 2021.15.07-play2.8
sbt clean compile publishLocal

git clone git@gitlab.com:xivocc/asterisk-java.git
cd asterisk-java
git checkout xivocc_version
mvn install

git clone git@gitlab.com:xivo.solutions/xivo-javactilib.git
cd xivo-javactilib
mvn install
```

You need to have java 8 enabled for xivo-javactilib to work properly.

## How to launch app in dev

Create a conf file named application-*.conf like `application-dev.conf` and fill the variable : 
```
include "application.conf"

XIVO_HOST = <TOFILL>
USM_EVENT_AMQP_SECRET = <TOFILL># USM_EVENT_AMQP_SECRET in custom env
PLAY_AUTH_TOKEN = <TOFILL># PLAY_AUTH_TOKEN custom env
RECORDING_SERVER_HOST = <TOFILL>
logger.file = conf/debug-logger.xml
XIVO_AMI_SECRET = <TOFILL> # secret in /etc/asterisk/manager.d/02-xivocc.conf on pbx
```

You can also directly override a configuration key if no env variable is defined.

Starting the application in play framework :

```sh
sbt run -Dhttp.port=8090 -Dconfig.file=conf/application-dev.conf
```

If you want to bind xuc with an HTTPS port, you will need some extra parameters `--add-exports=java.base/sun.security.x509=ALL-UNNAMED -Dhttps.port=8443`
The command will then look like this :
```sh
sbt run --add-exports=java.base/sun.security.x509=ALL-UNNAMED -Dhttp.port=8090 -Dhttps.port=8443 -Dconfig.file=conf/application-dev.conf
```

## Optional logs for development

### WS and Ami messages

To log WS and Ami messages as JSON to file `logs/xuc_events.log`, use config `conf/debug-logger.xml` and change priority
level for logger `"xucevents"` to `"DEVEL"`:

```xml
<logger name="xucevents" level="DEVEL" additivity="false">
    <appender-ref ref="XUCEVENTS" />
</logger>
```

Or you can enable selectively using Logback's
[Level Inheritance](http://logback.qos.ch/manual/architecture.html#effectiveLevel). Definned loggers:

* `"xucevents.cti.in."` + CTI user name
* `"xucevents.cti.out."` + CTI user name
* `"xucevents.ws.in."` + CTI user name
* `"xucevents.ws.out."` + CTI user name

Configuration for logging only incomming AMI for user john events to both file and console:

```xml
<logger name="xucevents.cti.in.john" level="DEBUG">
    <appender-ref ref="XUCEVENTS" />
    <appender-ref ref="STDOUT" />
</logger>
```

To have JSON pretty-printed (human readable) in `conf/application.conf` change `logger.xucevents.prettyPrint=false` to `true`.

## Miscellaneous

* Close xuc port

```sh
iptables -A INPUT -p tcp --dport 9000 -j DROP
```

* Open xuc port

```sh
iptables -D INPUT -p tcp --dport 9000 -j DROP
```

## Testing

For the tests you need:

* a link towards your XiVO dev with
  * an access to the AMI
  * and an access to the webservice
* a test Database

### Link to XiVO

In the `/etc/hosts` of your machine, map the IP of an existing XiVO to the hostnames

* `xivo-integration` (see `BaseTest.scala` for example)
* `xivo` (see `test/resources/application.conf`)

For example, if your XiVO is accessible on `192.168.51.232`, you should have:

```
192.168.51.232 xivo-integration xivo
```

#### AMI Access

The tests will try to connect with user `xuc` and password `xucpass` for AMI access.
Therefore in `/etc/asterisk/manager.d/02-xivocc.conf` make sure you have a user `xuc` configured with secret `xucpass` as in extract:

```plain
[xuc]
secret = xucpass
...
```

Make sure also your IP address is authorized in the ACL.
For example, if your XIVO is on `192.168.51.232` and your PC has IP `192.168.51.10`, you should have a line:

```plain
permit=192.168.51.10/255.255.255.255
```

#### Webservice Access

The tests need also an access to the Webservice. For this:

* go to Configuration -> Web Services Access
* add a Webvservice user with:
  * user: `xivows`
  * host: `*your_host_ip*`

See [documentation](https://documentation.xivo.solutions/en/latest/installation/xivocc/installation/manual_configuration.html#customizations-in-the-web-interface).

### Test Database

### Setup a test database *testdata* user *test* pwd *test*

```sql
CREATE USER test WITH PASSWORD 'test';
CREATE DATABASE testdata WITH OWNER test;
```

In activator start test

## Integration Tests

* Have access to a xivo with a configured webservice user `xivows` password `xivows` host `your_host_ip`
* In file `/etc/hosts` add `xivo-integration` and `xivo` pointing to XiVO ip address
* You also need a user `xuc` with password `xucpass` for AMI access. Or you must use your password in the `IntegrationSpec` class.
* Should have in Xivo a user with username `bruce` password `0000`
* In activator console : `integrationTest:test`
* RUN with `wsapi:testOnly *WsApiSpec*`

Make sure you have your IP added to Configuration -> Web Services Access on Xivo at `192.168.225.199` - see [documentation](https://documentation.xivo.solutions/en/latest/installation/xivocc/installation/manual_configuration.html#customizations-in-the-web-interface).

## Notes

Some test output with no effect :

```scala
java.lang.NullPointerException
    at pekko.actor.ActorLogging$class.log(Actor.scala:286)
```

### Kamon

use specific kamon branch

<http://kamon.io>

```sh
docker run -d -p 8000:80 -p 8125:8125/udp -p 8126:8126 --name kamon-grafana-dashboard kamon/grafana_graphite
activator stage
cd target/universal/stage/
./bin/xuc -J-javaagent:./lib/org.aspectj.aspectjweaver-1.8.6.jar -Dlogger.file=conf/debug-logger.xml
    -DtechMetrics.jvm=false -Dhttp.port=8090 -Dxivohost=192.168.51.232 -Dxuc.outboundLength=4 -Dconfig.file=./conf/application.conf
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.
