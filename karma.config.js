module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        port: 9876,
        colors: false,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['ChromeHeadless'],
       
        singleRun: false,
        autoWatchBatchDelay: 300,
  
        reporters: ['spec'],

        files: [
            './public/javascripts/**/*.js',
            './public/javascripts/*.js',            
            './app/assets/javascripts/*.js',
            './test/assets/**/*.spec.js'
        ],
        exclude: [
            './public/javascripts/SIPml-api.js'
        ]
    });
  };
  