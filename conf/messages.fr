
header.XiVO=XiVO

login.legend=Authentication

login.phoneNumberHolder=Numéro de poste agent

login.panel=XiVO Login
login.user=Utilisateur
login.userHolder=Utilisateur xivo client
login.agent=Agent phone number
login.agentHolder=par ex. 2002
login.password=Mot de passe
login.passwordHolder=Mot de passe xivo client
login.button=Se connecter
login.image=XiVO login
login.ctiError=Identiants invalides ou serveur CTI injoignable

xivo.dialbox=numéro ou nom
xivo.agentLogin=Logger Agent
xivo.agentLogout=Délogger Agent
xivo.disconnect=Sortir

agent.callDetails=Appel en cours
agent.phone=Poste
agent.queues=Files d''attente
agent.details=Agent

cancel=Annuler


# Default messages

# --- Constraints
constraint.required=Obligatoire
constraint.min=Valeur minimale: {0}
constraint.max=Valeur minimale: {0}
constraint.minLength=Longeur minimale: {0}
constraint.maxLength=Longeur minimale: {0}
constraint.email=Email

# --- Formats
format.date=Date (''{0}'')
format.numeric=Numérique
format.real=Real

# --- Errors
error.invalid=Valeur invalide
error.required=Ce champ est obligatoire
error.number=Veuillez saisir un nombre
error.real=Veuillez saisir un nombre
error.real.precision=Veuillez saisir un nombre avec au maximum {0} chiffre(s) avec {1} decimale(s)
error.min=Doit être supérieur ou égal à {0}
error.min.strict=Doit être supérieur à {0}
error.max=Doit être inférieur ou égal à {0}
error.max.strict=Doit être inférieur à {0}
error.minLength=Longeur minimale est {0}
error.maxLength=Longeur maximale est {0}
error.email=Veuillez saisir une adresse email
error.pattern=au format {0}
error.date=Veuillez saisir une date

error.globalError=Veuillez corriger les erreurs pour pouvoir continuer.
#
# ccmanager
#
ccmanager.queueSelectionTitle=Choisir les files d''attente à afficher
ccmanager.addAgentSelectionTitle=Cliquer pour ajouter un agent
ccmanager.editAgentTitle=Editer l'agent
ccmanager.queueNumber=Numéro
ccmanager.queueDisplayName=Nom
ccmanager.queuePenalty=Pénalité
ccmanager.expand=Voir plus
ccmanager.agentView=Vue Agent
ccmanager.groupView=Vue Groupe
ccmanager.groupId=Id
ccmanager.groupName=Nom
ccmanager.dismiss=Fermer
ccmanager.firstName=Prénom
ccmanager.lastName=Nom
ccmanager.number=Numéro
ccmanager.queue=File
ccmanager.group=Groupe
ccmanager.Login=Login
ccmanager.Logout=Logout
ccmanager.Pause=Pause
ccmanager.UnPause=UnPause
ccmanager.Listen=Ecouter
ccmanager.showAll=Tout les agents