package system

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import com.google.inject.name.Named
import javax.inject.{Inject, Singleton}
import play.api.Logger
import play.api.inject.ApplicationLifecycle
import services.ActorIds
import xivo.xuc.XucBaseConfig
import xivo.xucami.XucBaseAmiConfig
import xivo.xucstats.XucBaseStatsConfig
import xucserver.info.BuildInfo

import scala.concurrent.Future

@Singleton
class ApplicationStart @Inject() (
    lifecycle: ApplicationLifecycle,
    actorSystem: ActorSystem,
    xucConfig: XucBaseConfig,
    xucAmiConfig: XucBaseAmiConfig,
    xucStatsConfig: XucBaseStatsConfig,
    @Named(ActorIds.MainRunnerId) mainRunnerRef: ActorRef
) {

  val log: Logger = Logger(getClass.getName)
  log.info("Starting")
  init()

  def init(): Unit = {
    dumpConfig

    if (xucConfig.EventUser.isEmpty) {
      log.error("xivocti.eventUser missing unable to start")
      actorSystem.terminate()
    }

    lifecycle.addStopHook { () =>
      Future.successful(log.info("Stopping xuc server"))
    }
  }

  private def dumpConfig: Unit = {
    val xucPeers = xucConfig.xucPeers.toString()

    log.info(s"Starting xuc version ${BuildInfo.version}")
    log.info("-------------parameters---------------------------------")
    log.info(s"host                      : ${xucConfig.XIVOCTI_HOST}")
    log.info(s"port                      : ${xucConfig.XIVOCTI_PORT}")
    log.info(s"wsHost                    : ${xucConfig.XivoWs_host}")
    log.info(s"wsUser                    : ${xucConfig.XivoWs_wsUser}")
    log.info(s"wsPort                    : ${xucConfig.XivoWs_port}")
    log.info(
      s"peers                     : ${xucConfig.xucPeers.mkString(", ")}"
    )
    log.info(s"Cti Version               : ${xucConfig.XivoCtiVersion}")
    log.info(s"outboundLength            : ${xucConfig.outboundLength}")
    log.info(s"credentials expiration    : ${xucConfig.Authentication.expires}")
    log.info(
      s"Cti user prevent login    : ${xucConfig.Authentication.preventXucUserLogin}"
    )
    log.info(s"SIP driver                : ${xucConfig.sipDriver}")
    log.info("-------------SSO parameters---------------------------------")
    log.info("--- CAS SSO")
    log.info(
      s"CAS URL                   : ${xucConfig.casServerUrl.getOrElse("")}"
    )
    log.info("--- OpenID SSO")
    log.info(s"OIDC Enable               : ${xucConfig.oidcEnable}")
    log.info(
      s"OIDC URL                  : ${xucConfig.oidcServerUrl.getOrElse("")}"
    )
    log.info(
      s"OIDC additional URLs     : ${xucConfig.oidcAdditionalTrustedServersUrl.mkString(", ")}"
    )
    log.info(
      s"OIDC Client ID            : ${xucConfig.oidcClientId.mkString(", ")}"
    )
    log.info(
      s"OIDC additional Client IDs            : ${xucConfig.oidcAdditionalTrustedClientIDs.mkString(", ")}"
    )
    log.info(
      s"OIDC Audience             : ${xucConfig.oidcAudience.mkString(", ")}"
    )
    log.info(
      s"OIDC Username Field       : ${xucConfig.oidcUsernameField.getOrElse("")}"
    )
    log.info("-------------AMI parameters-----------------------------")
    log.info(s"IpAddress                 : ${xucAmiConfig.ipAddress}")
    log.info(s"Port                      : ${xucAmiConfig.port}")
    log.info(s"Username                  : ${xucAmiConfig.username}")
    log.info(s"Secret                    : ${xucAmiConfig.secret}")
    log.info("-------------Stat parameters----------------------------")
    log.info(s"resetSchedule             : ${xucStatsConfig.resetSchedule}")
    log.info(s"initFromMidnight          : ${xucStatsConfig.initFromMidnight}")
    log.info(s"statsLogReporter          : ${xucStatsConfig.statsLogReporter}")
    log.info(
      s"statsLogReporterPeriod    : ${xucStatsConfig.statsLogReporterPeriod}"
    )
    log.info(
      s"queues.statTresholdsInSec : ${xucStatsConfig.queuesStatTresholdsInSec.toString()}"
    )
    log.info("-------------Recording parameters-----------------------")
    log.info(s"recordingHost             : ${xucConfig.recordingHost}")
    log.info(s"recordingPort             : ${xucConfig.recordingPort}")
    log.info(s"recordingToken            : ${xucConfig.recordingToken}")
    log.info("-------------Chat Server parameters---------------------")
    log.info(s"chatEnable                : ${xucConfig.chatEnable}")
    log.info(s"chatHost                  : ${xucConfig.chatHost}")
    log.info(s"chatPort                  : ${xucConfig.chatPort}")
    log.info("--------------Deprecated API allowed hosts--------------")
    log.info(
      s"hosts                     : ${xucConfig.ipAccepted.mkString(",")}"
    )
    log.info("--------------------------------------------------------")
  }

}
