package system

import services.MediatorWrapper
import com.google.inject.AbstractModule
import play.api.libs.concurrent.PekkoGuiceSupport
import services.agent._
import services.callhistory.{CallHistoryEnricher, CallHistoryManager}
import services.calltracking.{
  AsteriskGraphTracker,
  ChannelTracker,
  ConferenceTracker,
  DevicesTracker
}
import services.config._
import services.directory.DirectoryTransformer
import services.line.PhoneController
import services._
import services.chat.ChatService
import services.userPreference.UserPreferenceService
import services.voicemail.VoiceMailManager
import stats.{
  QueueEventDispatcher,
  QueueStatCache,
  QueueStatManager,
  StatsAggregator
}
import xivo.ami.AmiBusConnector
import xivo.data.{QLogDispatcher, QLogTransfer}
import xivo.directory.PersonalContactRepository
import xivo.rabbitmq.{UsageRabbitEventsManager, XivoRabbitEventsManager}
import xivo.services.{TokenRetriever, XivoAuthentication, XivoDirectory}
import xivo.xucami.AmiSupervisor
import services.video.{VideoEventManager, VideoService}
import us.theatr.pekko.quartz.QuartzActor

class ActorModule extends AbstractModule with PekkoGuiceSupport {
  override def configure: Unit = {
    bind(classOf[ApplicationStart]).asEagerSingleton()
    bind(classOf[MediatorWrapper]).asEagerSingleton()

    bindActor[AgentConfig](ActorIds.AgentConfigId)
    bindActor[AgentDeviceManager](ActorIds.AgentDeviceManagerId)
    bindActor[AgentGroupSetter](ActorIds.AgentGroupSetterId)
    bindActor[AgentManager](ActorIds.AgentManagerId)
    bindActor[AmiBusConnector](ActorIds.AmiBusConnectorId)
    bindActor[AmiSupervisor](ActorIds.XucAmiSupervisor)
    bindActor[AsteriskGraphTracker](ActorIds.AsteriskGraphTrackerId)
    bindActor[CallHistoryManager](ActorIds.CallHistoryManagerId)
    bindActor[CallbackManager](ActorIds.CallbackMgrInterfaceId)
    bindActor[ChannelTracker](ActorIds.ChannelTrackerId)
    bindActor[ChatService](ChatService.serviceName)
    bindActor[ClientLogger](ActorIds.ClientLogger)
    bindActor[ConferenceTracker](ActorIds.ConferenceTrackerId)
    bindActor[ConfigDispatcher](ActorIds.ConfigDispatcherId)
    bindActor[ConfigManager](ActorIds.ConfigManagerId)
    bindActor[ConfigServiceManager](ActorIds.ConfigServiceManagerId)
    bindActor[CtiRouterFactory](ActorIds.CtiRouterFactoryId)
    bindActor[DefaultQueueMembershipRepository](
      ActorIds.DefaultQueueMembershipRepositoryIds
    )
    bindActor[DevicesTracker](ActorIds.DevicesTrackerId)
    bindActor[DirectoryTransformer](ActorIds.DirectoryTransformerId)
    bindActor[EventPublisher](ActorIds.EventPublisherId)
    bindActor[ExtensionManager](ActorIds.ExtensionManager)
    bindActor[MainRunner](ActorIds.MainRunnerId)
    bindActor[QLogDispatcher](ActorIds.QLogDispatcherId)
    bindActor[QLogTransfer](ActorIds.QLogTransferId)
    bindActor[QuartzActor](ActorIds.QuartzActorId)
    bindActor[QueueDispatcher](ActorIds.QueueDispatcherId)
    bindActor[QueueEventDispatcher](ActorIds.QueueEventDispatcherId)
    bindActor[QueueStatCache](ActorIds.QStatCacheId)
    bindActor[StatsAggregator](ActorIds.GlobalAggregatorId)
    bindActor[StatusPublish](ActorIds.StatusPublishId)
    bindActor[UserPreferenceService](ActorIds.UserPreferenceService)
    bindActor[UsageRabbitEventsManager](ActorIds.UsageEventsId)
    bindActor[VideoEventManager](ActorIds.VideoEventManager)
    bindActor[VoiceMailManager](ActorIds.VoiceMailManagerId)
    bindActor[VideoService](VideoService.serviceName)
    bindActor[XivoAuthentication](ActorIds.XivoAuthenticationId)
    bindActor[XivoDirectory](ActorIds.XivoDirectoryInterfaceId)
    bindActor[XivoRabbitEventsManager](ActorIds.XivoRabbitManagerId)

    bindActorFactory[AgentAction, AgentAction.Factory]
    bindActorFactory[AgentStatCollector, AgentStatCollector.Factory]
    bindActorFactory[CallHistoryEnricher, CallHistoryEnricher.Factory]
    bindActorFactory[CtiFilter, CtiFilter.Factory]
    bindActorFactory[CtiRouter, CtiRouter.Factory]
    bindActorFactory[
      PersonalContactRepository,
      PersonalContactRepository.Factory
    ]
    bindActorFactory[PhoneController, PhoneController.Factory]
    bindActorFactory[QueueStatManager, QueueStatManager.Factory]
    bindActorFactory[TokenRetriever, TokenRetriever.Factory]
  }
}
