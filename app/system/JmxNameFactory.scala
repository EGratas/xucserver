package system

import javax.management.{MalformedObjectNameException, ObjectName}

import com.codahale.metrics.jmx.ObjectNameFactory
import scala.jdk.CollectionConverters._

/**
  */
class JmxNameFactory extends ObjectNameFactory {
  override def createName(
      `type`: String,
      domain: String,
      name: String
  ): ObjectName =
    try {
      val (root, keys) = if (name.contains(".")) {
        val names = name.split("\\.")
        if (names.size > 2) {
          (
            domain + "." + names.dropRight(2).mkString("."),
            Map("type" -> names(names.size - 2), "name" -> names.last)
          )
        } else {
          (domain + "." + names(0), Map("name" -> names.last))
        }
      } else {
        (domain, Map("name" -> name))
      }

      val objectName: ObjectName =
        new ObjectName(root, new java.util.Hashtable(keys.asJava))
      if (objectName.isPattern)
        new ObjectName(domain, "name", ObjectName.quote(name))
      else
        objectName
    } catch {
      case e: MalformedObjectNameException =>
        try return new ObjectName(domain, "name", ObjectName.quote(name))
        catch {
          case e1: MalformedObjectNameException =>
            throw new RuntimeException(e1)
        }
    }
}
