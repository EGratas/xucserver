package services.voicemail

import org.apache.pekko.actor.*
import org.apache.pekko.pattern.pipe
import com.google.inject.Inject
import helpers.{FastLogging, JmxActorSingletonMonitor, JmxLongMetric}
import models.XivoUser
import org.asteriskjava.manager.action.VoicemailUserStatusAction
import org.asteriskjava.manager.event.{
  MessageWaitingEvent,
  VoicemailUserDetailEvent
}
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Format, JsPath, Json}
import services.XucAmiBus
import services.XucAmiBus.{AmiAction, AmiEvent, AmiType}
import xivo.models.{VoiceMail, VoiceMailFactory}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

sealed trait VoiceMailRequest
case class SubscribeToVoiceMail(xivoUser: XivoUser)     extends VoiceMailRequest
case class UnsubscribeFromVoiceMail(xivoUser: XivoUser) extends VoiceMailRequest

sealed trait VoiceMailEvent
case class VoiceMailStatusUpdate(
    voiceMailId: Long,
    newMessages: Int = 0,
    waitingMessages: Int = 0,
    oldMessages: Int = 0
) extends VoiceMailEvent

case class AddVoiceMail(xivoUser: XivoUser, voiceMail: VoiceMail)
case class VoiceMailRef(ctiRouterRef: Option[ActorRef], xivoUser: XivoUser)

object VoiceMailManager {

  final val serviceName = "VoiceMailManager"
  type VoiceMailName = String
  type UserName      = String

  def props(xucAmiBus: XucAmiBus, voiceMailFactory: VoiceMailFactory): Props =
    Props(new VoiceMailManager(xucAmiBus, voiceMailFactory))
}

object VoiceMailStatusUpdate {
  implicit val format: Format[VoiceMailStatusUpdate] = (
    (JsPath \ "voiceMailId").format[Long] and
      (JsPath \ "newMessages").formatWithDefault[Int](0) and
      (JsPath \ "waitingMessages").formatWithDefault[Int](0) and
      (JsPath \ "oldMessages").formatWithDefault[Int](0)
  )(
    VoiceMailStatusUpdate.apply,
    o => (o.voiceMailId, o.newMessages, o.waitingMessages, o.oldMessages)
  )
}

class VoiceMailManager @Inject() (
    xucAmiBus: XucAmiBus,
    voiceMailFactory: VoiceMailFactory
) extends Actor
    with FastLogging
    with JmxActorSingletonMonitor {
  var users: Map[VoiceMailManager.UserName, VoiceMail] =
    Map.empty[VoiceMailManager.UserName, VoiceMail]
  var voiceMails: Map[VoiceMailManager.VoiceMailName, Set[VoiceMailRef]] =
    Map.empty[VoiceMailManager.VoiceMailName, Set[VoiceMailRef]]
  val jmxVmUsersCount: Try[JmxLongMetric] = jmxBean.addLong(
    "VoiceMailUsers",
    0L,
    Some("Number of users subscription to voicemail update")
  )
  val jmxVmCount: Try[JmxLongMetric] =
    jmxBean.addLong("VoiceMails", 0L, Some("Number of loaded voicemail"))

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering voicemail mbean"))
    xucAmiBus.subscribe(self, AmiType.AmiEvent)
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
  }

  override def receive: Receive = {

    case SubscribeToVoiceMail(xivoUser) =>
      log.debug(s"Ask for subscribing User Voicemail : ${xivoUser.username}")
      getAndAddVoiceMail(xivoUser, sender()) pipeTo self

    case AddVoiceMail(xivoUser, voiceMail) =>
      log.info(
        s"Add voicemail ${voiceMail.name} subscription to ${xivoUser.username} and get current status"
      )
      xivoUser.username.foreach(u => {
        users += (u -> voiceMail)
        xucAmiBus.publish(
          AmiAction(
            new VoicemailUserStatusAction(voiceMail.context, voiceMail.number)
          )
        )
      })
      jmxVmUsersCount.set(users.size)

    case UnsubscribeFromVoiceMail(xivoUser) =>
      log.info(s"Unsubscribe User Voicemail : ${xivoUser.username}")
      xivoUser.username.foreach(u =>
        users
          .get(u)
          .foreach(vm =>
            voiceMails += (vm.name -> voiceMails(vm.name)
              .filterNot(v => v.xivoUser == xivoUser))
          )
      )
      users -= xivoUser.username.getOrElse("")
      jmxVmUsersCount.set(users.size)

    case amievt: AmiEvent =>
      amievt.message match {
        case mwe: MessageWaitingEvent =>
          log.debug("MessageWaitingEvent {}", mwe)
          notifyVoiceMailUsers(
            mwe.getMailbox,
            mwe.getNew,
            mwe.getWaiting,
            mwe.getOld
          )

        case vue: VoicemailUserDetailEvent =>
          log.debug("VoicemailUserDetailEvent {}", vue)
          if (vue.getNewMessageCount > 0) {
            notifyVoiceMailUsers(
              s"${vue.getVoicemailbox}@${vue.getVmContext}",
              vue.getNewMessageCount,
              0,
              vue.getOldMessageCount
            )
          }

        case _ =>
      }
  }

  def notifyVoiceMailUsers(
      mailboxName: String,
      newCount: Int,
      waitCount: Int,
      oldCOunt: Int
  ): Unit = {
    def sendToSubscribedUser(vmr: VoiceMailRef): Unit =
      users.get(vmr.xivoUser.username.getOrElse("")) match {
        case Some(vm) =>
          log.info(
            s"Notify user ${vmr.xivoUser.username} with new voicemail status"
          )
          vmr.ctiRouterRef.foreach(
            _ ! VoiceMailStatusUpdate(vm.id, newCount, waitCount, oldCOunt)
          )
        case _ =>
      }

    voiceMails.get(mailboxName) match {
      case Some(refs) => refs.foreach(sendToSubscribedUser)
      case _          =>
    }
  }

  def getAndAddVoiceMail(
      user: XivoUser,
      ctiRouterRef: ActorRef
  ): Future[AddVoiceMail] =
    voiceMailFactory
      .get(user.username.getOrElse(""))
      .flatMap(o => {
        o.map(v => {
          voiceMails
            .get(v.name)
            .fold {
              voiceMails += (v.name -> Set(
                VoiceMailRef(Option(ctiRouterRef), user)
              ))
              jmxVmCount.set(voiceMails.size)
            } { set =>
              voiceMails += (v.name -> (set + VoiceMailRef(
                Option(ctiRouterRef),
                user
              )))
            }
          Future.successful(AddVoiceMail(user, v))
        }).getOrElse(
          Future.failed(
            new NoSuchElementException(
              s"User ${user.username} doesn't have any voicemail"
            )
          )
        )
      })
      .recoverWith {
        case e: NoSuchElementException =>
          log.debug(e.getMessage)
          Future.failed(e)
        case e =>
          val err =
            s"Error while fetching user voicemail ${user.username}: ${e.getMessage}"
          log.error(new Exception("Internal Error"), s"$err")
          Future.failed(new NoSuchElementException(s"$err"))
      }
}
