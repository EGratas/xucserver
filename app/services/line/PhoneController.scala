package services.line

import org.apache.pekko.actor.*
import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import com.google.inject.name.Named
import models.XivoUser
import org.asteriskjava.live.HangupCause
import org.asteriskjava.manager.action.{HangupAction, SetVarAction}
import services.XucAmiBus.AmiAction
import services.calltracking.DeviceConferenceAction.*
import services.calltracking.RetrieveUtil.RetrieveQueueCallContext
import services.calltracking.SingleDeviceTracker.{
  DeviceConferenceMessage,
  SendCurrentCallsPhoneEvents
}
import services.calltracking.{DeviceCall, DeviceCalls, SingleDeviceTracker, _}
import services.config.ConfigDispatcher.*
import services.config.{
  AgentOnPhone,
  ConfigRepository,
  ObjectType,
  OutboundQueue
}
import services.request.PhoneRequest.*
import services.request.{
  MonitorPause,
  MonitorUnpause,
  PhoneRequest,
  UserBaseRequest
}
import services.{ActorIds, PhoneNumberSanitizer, XucAmiBus, XucEventBus}
import xivo.events.AgentState
import xivo.events.AgentState.{AgentLoggedOut, AgentReady}
import xivo.models.{AgentQueueMember, Line, QueueConfigUpdate}
import xivo.phonedevices.{DeviceAdapter, DeviceAdapterFactory, SetDataCommand}
import xivo.xuc.TransferConfig

import scala.concurrent.ExecutionContextExecutor

object PhoneController {
  case object Start

  trait Factory {
    def apply(phoneNb: String, line: Line, xivoUser: XivoUser): Actor
  }
}

class PhoneController @Inject() (
    configRepository: ConfigRepository,
    bus: XucEventBus,
    amiBus: XucAmiBus,
    transferUtil: TransferUtil,
    recordingActionController: RecordingActionController,
    config: TransferConfig,
    deviceAdapterFactory: DeviceAdapterFactory,
    retrieveUtil: RetrieveUtil,
    @Named(ActorIds.DevicesTrackerId) devicesTracker: ActorRef,
    @Named(ActorIds.ChannelTrackerId) channelTracker: ActorRef,
    @Named(ActorIds.AsteriskGraphTrackerId) asteriskGraphTracker: ActorRef,
    @Named(ActorIds.AmiBusConnectorId) amiBusConnector: ActorRef,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
    @Assisted phoneNb: String,
    @Assisted line: Line,
    @Assisted xivoUser: XivoUser
) extends Actor
    with ActorLogging {

  val deviceAdapter: DeviceAdapter =
    deviceAdapterFactory.getAdapter(line, phoneNb)
  var agentId: Option[Long]            = None
  var agentNum: Option[String]         = None
  var queue: Option[QueueConfigUpdate] = None
  var enableOutboundDial               = false

  var calls: Map[String, DeviceCall] = Map.empty

  override def preStart(): Unit = {
    devicesTracker ! SingleDeviceTracker.MonitorCalls(line.interface)
    xivoUser.mobile_phone_number.foreach(mobile =>
      devicesTracker ! SipDeviceTracker.WatchOutboundCallTo(
        line.interface,
        mobile
      )
    )
  }

  override def postStop(): Unit = {
    bus.unsubscribe(self)
    devicesTracker ! SingleDeviceTracker.UnMonitorCalls(line.interface)
  }

  def getCallFromUniqueId(uniqueId: String): Option[DeviceCall] = {
    calls.values.find(call =>
      call.channel.exists(channel => channel.id == uniqueId)
    )
  }

  def buildHangupActions(uniqueId: String): List[AmiAction] = {
    val maybeCall = getCallFromUniqueId(uniqueId)

    maybeCall
      .map(call => {
        val setVar = new SetVarAction()
        setVar.setChannel(call.channelName)
        setVar.setVariable("CHANNEL(hangupsource)")
        setVar.setValue(call.channelName)
        val action = new HangupAction()
        action.setChannel(call.channelName)
        action.setCause(HangupCause.AST_CAUSE_NORMAL_CLEARING.getCode)
        val mds = call.channel map { _.mdsName }
        List(
          AmiAction(setVar, Some(uniqueId), targetMds = mds),
          AmiAction(action, Some(uniqueId), targetMds = mds)
        )
      })
      .getOrElse(List())
  }

  override def receive: Receive = mainReceive orElse doRecordingAction

  def mainReceive: Receive = {
    case PhoneController.Start =>
      bus.subscribe(self, XucEventBus.allAgentsEventsTopic)
      configDispatcher ! RequestConfig(self, GetAgentOnPhone(phoneNb))

    case DeviceCalls(_, newCalls) => calls = newCalls

    case AgentOnPhone(agent, _) =>
      agentId = Some(agent)
      agentNum = agentId.flatMap(configRepository.getAgent).map(_.number)
      configDispatcher ! RequestConfig(self, GetQueuesForAgent(agent))
      configDispatcher ! RequestStatus(self, agent.toInt, ObjectType.TypeAgent)

    case QueuesForAgent(queues, _) =>
      configDispatcher ! RequestConfig(
        self,
        OutboundQueueQuery(queues.map(_.queueId))
      )

    case OutboundQueue(q) =>
      log.info(s"Using queue ${q.name} (${q.number} for outbound dialing")
      queue = Some(q)

    case AgentLoggedOut(_, _, number, _, _, _) if number == phoneNb =>
      agentId = None
      queue = None
      bus.unsubscribe(self, XucEventBus.configTopic(ObjectType.TypeQueueMember))

    case AgentReady(id, _, number, queueIds, _, _) if number == phoneNb =>
      agentId = Some(id)
      enableOutboundDial = true
      bus.subscribe(self, XucEventBus.configTopic(ObjectType.TypeQueueMember))
      configDispatcher ! RequestConfig(
        self,
        OutboundQueueQuery(queueIds.map(_.toLong))
      )

    case s: AgentState if s.phoneNb == phoneNb => enableOutboundDial = false

    case AgentQueueMember(agId, qId, penalty) if agentId.contains(agId) =>
      if (queue.isEmpty && penalty >= 0) {
        configDispatcher ! RequestConfig(
          self,
          OutboundQueueQuery(List(qId.toLong))
        )
      } else if (queue.map(_.id).contains(qId) && penalty < 0) {
        queue = None
      }

    case UserBaseRequest(_, rq: PhoneRequest, xucUser) =>
      rq match {
        case Dial(destination, variables, domain) =>
          log.debug("websocket command Dial {}", destination)
          val sanitizedNumber = PhoneNumberSanitizer.sanitize(destination)
          log.debug(
            s"Dial: destination number sanitized from {} to {}",
            destination,
            sanitizedNumber
          )
          if (agentId.isDefined && queue.isDefined && enableOutboundDial)
            configRepository
              .getLineUser(line.id)
              .map(_.id)
              .foreach(xivoUserId =>
                deviceAdapter.odial(
                  sanitizedNumber,
                  agentId.get,
                  queue.map(_.number).get,
                  variables,
                  xivoUserId,
                  sender(),
                  line.driver
                )
              )
          else
            deviceAdapter.dial(sanitizedNumber, variables, sender())

        case DialFromMobile(destination, variables) =>
          log.debug("websocket command DialFromMobile {}", destination)
          xivoUser.mobile_phone_number match {
            case None | Some("") =>
              log.info(
                "{} has empty mobile number, aborting DialFromMobile",
                xucUser
              )
            case Some(mobileNumber) =>
              val destinationSanitized =
                PhoneNumberSanitizer.sanitize(destination)
              val mobileNumberSanitized =
                PhoneNumberSanitizer.sanitize(mobileNumber)
              log.debug(
                s"DialFromMobile sanitization: destination {} -> {}, mobile {} -> {}",
                destination,
                destinationSanitized,
                mobileNumber,
                mobileNumberSanitized
              )
              val callerName = xivoUser.lastname
                .map(xivoUser.firstname + " " + _)
                .getOrElse(xivoUser.firstname)
              deviceAdapter.dialFromMobile(
                mobileNumberSanitized,
                destinationSanitized,
                phoneNb,
                callerName,
                variables,
                sender(),
                xivoUser.id
              )
          }

        case DialByUsername(username, variables, domain) =>
          configRepository.phoneNumberForUser(username) match {
            case Some(number) =>
              self.forward(
                UserBaseRequest(sender(), Dial(number, variables), xucUser)
              )
            case None =>
              log.warning(
                s"Dial number not found for username $username, aborting DialByUsername"
              )
          }

        case ListenCallbackMessage(voiceMsgRef, variables) =>
          log.debug("websocket command ListenCallbackMessage {}", voiceMsgRef)
          deviceAdapter.listenCallbackMessage(voiceMsgRef, variables, sender())

        case Hangup(uniqueId: Option[String]) =>
          uniqueId.fold(deviceAdapter.hangup(sender()))(id => {
            val actions = buildHangupActions(id)
            if (actions.isEmpty) {
              log.warning(
                s"No channel $uniqueId found to hangup, call legacy implementation"
              )
              deviceAdapter.hangup(sender())
            } else {
              actions.map(amiBus.publish)
            }
          })

        case ToggleMicrophone(uniqueId: Option[String]) =>
          deviceAdapter.toggleMicrophone(
            uniqueId.flatMap(getCallFromUniqueId),
            sender()
          )

        case AttendedTransfer(destination, device) =>
          if (config.enableAmiTransfer) {
            log.debug(
              s"AttendedTransfer (amitransfer) starting attended transfer to $destination: trying to find user for line name:${line.name} id:${line.id}"
            )
            configRepository
              .getLineUser(line.id)
              .foreach(xivoUser => {
                implicit val ec: ExecutionContextExecutor = context.dispatcher
                transferUtil
                  .attendedTransfer(
                    calls,
                    destination,
                    device,
                    line,
                    xivoUser,
                    deviceAdapter,
                    sender()
                  )(context)
                  .failed
                  .foreach(t => log.error(t, "error while transferring"))
              })
          } else {
            log.debug(
              s"AttendedTransfer (deviceadapter) starting attended transfer to $destination: from line name:${line.name} id:${line.id}"
            )
            deviceAdapter.attendedTransfer(destination, sender())
          }

        case CompleteTransfer =>
          if (config.enableAmiTransfer) {
            transferUtil
              .getChannelsForTransfer(calls)
              .foreach(c => {
                log.debug(s"CompleteTransfer for $c")
                implicit val ec: ExecutionContextExecutor = context.dispatcher
                transferUtil
                  .completeTransfer(c, agentNum = agentNum)(context)
                  .failed
                  .foreach(t =>
                    log.error(t, s"Got error while transferring $c")
                  )
              })
          } else {
            deviceAdapter.completeTransfer(sender())
          }

        case CancelTransfer => deviceAdapter.cancelTransfer(sender())

        case DirectTransfer(destination) =>
          deviceAdapter.directTransfer(destination, sender())

        case Conference => deviceAdapter.conference(sender())

        case ConferenceInvite(numConf, exten, role, earlyJoin, variables) =>
          val maybeUser = configRepository
            .getLineForPhoneNb(exten)
            .flatMap(lineId => configRepository.getLineUser(lineId))

          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Invite(numConf, exten, role, earlyJoin, variables, maybeUser)
          )

        case ConferenceMute(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Mute(numConf, index)
          )

        case ConferenceUnmute(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Unmute(numConf, index)
          )

        case ConferenceMuteAll(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            MuteAll(numConf)
          )

        case ConferenceUnmuteAll(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            UnmuteAll(numConf)
          )

        case ConferenceMuteMe(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            MuteMe(numConf)
          )

        case ConferenceUnmuteMe(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            UnmuteMe(numConf)
          )

        case ConferenceKick(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Kick(numConf, index)
          )

        case ConferenceClose(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Close(numConf)
          )

        case ConferenceDeafen(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Deafen(numConf, index)
          )

        case ConferenceUndeafen(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Undeafen(numConf, index)
          )

        case ConferenceReset(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Reset(numConf)
          )

        case Hold(uniqueId: Option[String]) =>
          deviceAdapter.hold(uniqueId.flatMap(getCallFromUniqueId), sender())

        case Answer(uniqueId: Option[String]) =>
          deviceAdapter.answer(uniqueId.flatMap(getCallFromUniqueId), sender())

        case SetData(variables) => sender() ! SetDataCommand(phoneNb, variables)

        case GetCurrentCallsPhoneEvents =>
          devicesTracker ! SendCurrentCallsPhoneEvents(line.interface)

        case s: SendDtmfRequest => deviceAdapter.sendDtmf(s.key, sender())

        case RetrieveQueueCall(queueCall, variables) =>
          log.debug("websocket command RetrieveQueueCall {}", queueCall.channel)
          implicit val ec: ExecutionContextExecutor = context.dispatcher
          val agentNum =
            agentId.flatMap(configRepository.getAgent).map(_.number)
          val agentName = xivoUser.lastname
            .map(xivoUser.firstname + " " + _)
            .getOrElse(xivoUser.firstname)

          retrieveUtil.processRetrieve(
            calls.values.toList,
            RetrieveQueueCallContext(
              line,
              queueCall,
              variables,
              agentNum,
              agentName
            ),
            deviceAdapter,
            sender()
          )

        case _ => log.error("Unsupported phone request received {}", rq)
      }

    case e: DeviceConferenceCommandErrorType =>
      context.parent ! e
  }

  def doRecordingAction: Receive = {

    case mp: MonitorPause =>
      log.debug("Received monitor pause {}", calls)
      recordingActionController
        .getMonitoredChannel(calls)
        .foreach(channel => {
          log.debug("Found channel to pause recording {}", channel)
          recordingActionController.pauseRecording(channel)
        })

    case mup: MonitorUnpause =>
      log.debug("Received monitor pause {}", calls)
      recordingActionController
        .getMonitoredChannelOnPause(calls)
        .foreach(channel => {
          log.debug("Found channel to un pause recording {}", channel)
          recordingActionController.unPauseRecording(channel)
        })

  }
}
