package services.line

import com.google.inject.{ImplementedBy, Inject}
import services.XucAmiBus
import services.calltracking.DeviceCall
import services.channel.ActionBuilder
import xivo.xucami.models.MonitorState.MonitorState
import xivo.xucami.models.{Channel, MonitorState}

@ImplementedBy(classOf[RecordingActionControllerImpl])
trait RecordingActionController {
  def getMonitoredChannel(calls: Map[String, DeviceCall]): Option[Channel]

  def getMonitoredChannelOnPause(
      calls: Map[String, DeviceCall]
  ): Option[Channel]

  def unPauseRecording(recordedChannel: Channel): Unit

  def pauseRecording(recordedChannel: Channel): Unit

}

class RecordingActionControllerImpl @Inject() (amiBus: XucAmiBus)
    extends RecordingActionController
    with ActionBuilder {

  def findChannel(
      calls: Map[String, DeviceCall],
      mstate: MonitorState
  ): Option[Channel] =
    calls.values.flatMap(_.monitoredChannels).find(_.monitored == mstate)

  override def getMonitoredChannel(
      calls: Map[String, DeviceCall]
  ): Option[Channel] = findChannel(calls, MonitorState.ACTIVE)

  override def getMonitoredChannelOnPause(
      calls: Map[String, DeviceCall]
  ): Option[Channel] = findChannel(calls, MonitorState.PAUSED)

  override def pauseRecording(recordedChannel: Channel): Unit =
    amiBus.publish(pauseAction(recordedChannel.name, recordedChannel.id))

  override def unPauseRecording(recordedChannel: Channel): Unit =
    amiBus.publish(unPauseAction(recordedChannel.name, recordedChannel.id))

}
