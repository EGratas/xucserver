package services.request

sealed trait HistoryParam
case class HistorySize(size: Int) extends HistoryParam
case class HistoryDays(days: Int) extends HistoryParam
