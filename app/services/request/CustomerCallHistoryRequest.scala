package services.request

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.*
import xivo.models.{
  FindCustomerCallHistoryRequest,
  FindCustomerCallHistoryResponse
}

case class CustomerCallHistoryRequestWithId(
    id: Long,
    request: FindCustomerCallHistoryRequest
) extends XucRequest

object CustomerCallHistoryRequestWithId {
  implicit val format: Format[CustomerCallHistoryRequestWithId] =
    (
      (JsPath \ "id").format[Long] and
        (JsPath \ "request").format[FindCustomerCallHistoryRequest]
    )(CustomerCallHistoryRequestWithId.apply, o => (o.id, o.request))

  def validate(json: JsValue): JsResult[CustomerCallHistoryRequestWithId] =
    json.validate[CustomerCallHistoryRequestWithId]
}

case class CustomerCallHistoryResponseWithId(
    id: Long,
    response: FindCustomerCallHistoryResponse
)

object CustomerCallHistoryResponseWithId {
  implicit val writesRequestList: Writes[CustomerCallHistoryResponseWithId] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "reponse").write[FindCustomerCallHistoryResponse]
  )(o => (o.id, o.response))
}
