package services.request

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, JsResult, JsSuccess, JsValue, Json, Reads}

class DirectoryRequest extends XucRequest

case class DirectoryLookUp(term: String) extends DirectoryRequest
object DirectoryLookUp {
  implicit val DirectoryLookUpRead: Reads[DirectoryLookUp] =
    (JsPath \ "term").read[String].map(o => DirectoryLookUp(o))

  def validate(json: JsValue): JsResult[DirectoryLookUp] =
    json.validate[DirectoryLookUp]
}

case object GetFavorites extends DirectoryRequest {
  def validate(json: JsValue): JsSuccess[GetFavorites.type] = JsSuccess(
    GetFavorites
  )
}

case class AddFavorite(contactId: String, source: String)
    extends DirectoryRequest
object AddFavorite {
  implicit val SetFavoriteRead: Reads[AddFavorite] = (
    (JsPath \ "contactId").read[String] and
      (JsPath \ "source").read[String]
  )(AddFavorite.apply)
  def validate(json: JsValue): JsResult[AddFavorite] =
    json.validate[AddFavorite]
}

case class RemoveFavorite(contactId: String, source: String)
    extends DirectoryRequest
object RemoveFavorite {
  implicit val RemoveFavoriteRead: Reads[RemoveFavorite] = (
    (JsPath \ "contactId").read[String] and
      (JsPath \ "source").read[String]
  )(RemoveFavorite.apply)
  def validate(json: JsValue): JsResult[RemoveFavorite] =
    json.validate[RemoveFavorite]
}
