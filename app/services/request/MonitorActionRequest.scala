package services.request

import play.api.libs.json.{JsPath, JsValue, Reads}
import xivo.models.Agent
import play.api.libs.json.JsResult

class MonitorActionRequest extends XucRequest

case class MonitorPause(id: Agent.Id) extends MonitorActionRequest

object MonitorPause {
  def validate(json: JsValue): JsResult[MonitorPause] =
    json.validate[MonitorPause]

  implicit val MonitorPauseRead: Reads[MonitorPause] =
    (JsPath \ "agentid").read[Long].map(MonitorPause.apply _)

}

case class MonitorUnpause(id: Agent.Id) extends MonitorActionRequest

object MonitorUnpause {
  def validate(json: JsValue): JsResult[MonitorUnpause] =
    json.validate[MonitorUnpause]

  implicit val MonitorUnpauseRead: Reads[MonitorUnpause] =
    (JsPath \ "agentid").read[Long].map(MonitorUnpause.apply _)

}
