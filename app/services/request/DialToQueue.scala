package services.request

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json._

case class DialToQueue(
    destination: String,
    queueName: String,
    callerIdNumber: String,
    variables: Map[String, String] = Map(),
    domain: String = "default"
) extends XucRequest

object DialToQueue {
  implicit val jsonReads: Reads[DialToQueue] = (
    (JsPath \ "destination").read[String] and
      (JsPath \ "queueName").read[String] and
      (JsPath \ "callerIdNumber").read[String] and
      (JsPath \ "variables")
        .readNullable[Map[String, String]]
        .map(_.getOrElse(Map())) and
      (JsPath \ "domain")
        .readNullable[String]
        .map(_.getOrElse("default"))
  )(DialToQueue.apply _)
  def validate(json: JsValue): JsResult[DialToQueue] =
    json.validate[DialToQueue]
  def callerId(callerIdNumber: String): String =
    List(
      if (callerIdNumber.isEmpty) None else Some("<" + callerIdNumber + ">")
    ).flatten.mkString(" ")
}
