package services.request

import play.api.libs.json.{JsPath, JsResult, JsValue, Reads}

case class GetUserCallHistoryByDays(param: HistoryDays) extends XucRequest
case class UserCallHistoryRequestByDays(param: HistoryDays, ctiUserName: String)
    extends XucRequest

object GetUserCallHistoryByDays {
  def validate(json: JsValue): JsResult[GetUserCallHistoryByDays] = {
    json.validate[GetUserCallHistoryByDays]
  }

  implicit val reads: Reads[GetUserCallHistoryByDays] = {
    (JsPath \ "days")
      .read[Int]
      .map(days => GetUserCallHistoryByDays(HistoryDays(days)))
  }
}
