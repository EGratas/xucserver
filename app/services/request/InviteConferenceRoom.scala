package services.request

import play.api.libs.json.{JsPath, JsValue, Reads}
import play.api.libs.json.JsResult

case class InviteConferenceRoom(userId: Int) extends XucRequest

object InviteConferenceRoom {
  def validate(json: JsValue): JsResult[InviteConferenceRoom] =
    json.validate[InviteConferenceRoom]

  implicit val reads: Reads[InviteConferenceRoom] =
    (JsPath \ "userId").read[Int].map(id => InviteConferenceRoom(id))
}
