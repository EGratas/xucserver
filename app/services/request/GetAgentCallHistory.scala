package services.request

import play.api.libs.json.{JsPath, JsValue, Reads}
import play.api.libs.json.JsResult

case class GetAgentCallHistory(size: Int) extends XucRequest
case class AgentCallHistoryRequest(size: Int, ctiUserName: String)
    extends XucRequest

object GetAgentCallHistory {
  def validate(json: JsValue): JsResult[GetAgentCallHistory] =
    json.validate[GetAgentCallHistory]

  implicit val reads: Reads[GetAgentCallHistory] =
    (JsPath \ "size").read[Int].map(GetAgentCallHistory.apply)
}
