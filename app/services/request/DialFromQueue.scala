package services.request

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class DialFromQueue(
    destination: String,
    queueId: Long,
    callerIdName: String,
    callerIdNumber: String,
    variables: Map[String, String] = Map(),
    domain: String
) extends XucRequest {
  require(!callerIdName.contains("\""))
  require(!callerIdNumber.contains("<"))
  require(!callerIdNumber.contains(">"))
}
object DialFromQueue {
  implicit val jsonReads: Reads[DialFromQueue] = (
    (JsPath \ "destination").read[String] and
      (JsPath \ "queueId").read[Long] and
      (JsPath \ "callerIdName")
        .readNullable[String]
        .map(_.getOrElse(""))
        .filterNot(JsonValidationError("Can not contain quotation mark"))(
          _.contains("\"")
        ) and
      (JsPath \ "callerIdNumber")
        .readNullable[String]
        .map(_.getOrElse(""))
        .filterNot(JsonValidationError("Can not contain <"))(_.contains("<"))
        .filterNot(JsonValidationError("Can not contain >"))(
          _.contains(">")
        ) and
      (JsPath \ "variables")
        .readNullable[Map[String, String]]
        .map(_.getOrElse(Map())) and
      (JsPath \ "domain")
        .readNullable[String]
        .map(_.getOrElse("default"))
  )(DialFromQueue.apply _)

  def validate(json: JsValue): JsResult[DialFromQueue] =
    json.validate[DialFromQueue]

  def callerId(callerIdName: String, callerIdNumber: String): String =
    List(
      if (callerIdName.isEmpty) None else Some("\"" + callerIdName + "\""),
      if (callerIdNumber.isEmpty) None else Some("<" + callerIdNumber + ">")
    ).flatten.mkString(" ")
}
