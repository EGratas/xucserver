package services.request

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, JsValue, Reads}
import xivo.models.Agent
import play.api.libs.json.JsResult

class AgentActionRequest extends XucRequest

case class AgentListen(id: Agent.Id, fromUser: Option[Long] = None)
    extends AgentActionRequest

object AgentListen {
  def validate(json: JsValue): JsResult[AgentListen] =
    json.validate[AgentListen]

  implicit val AgentListenRead: Reads[AgentListen] =
    (JsPath \ "agentid").read[Long].map(AgentListen.apply(_))

}

case class AgentLoginRequest(
    id: Option[Agent.Id],
    phoneNumber: Option[String],
    agentNumber: Option[Agent.Number] = None
) extends AgentActionRequest

object AgentLoginRequest {

  def validate(json: JsValue): JsResult[AgentLoginRequest] =
    json.validate[AgentLoginRequest]

  val allFields: Reads[AgentLoginRequest] = (
    (JsPath \ "agentid").readNullable[Agent.Id] and
      (JsPath \ "agentphonenumber").read[String] and
      (JsPath \ "agentnumber").read[Agent.Number]
  )((id, phone, number) =>
    AgentLoginRequest.apply(id, Some(phone), Some(number))
  )

  val onlyAgentId: Reads[AgentLoginRequest] = {
    (JsPath \ "agentid")
      .read[Long]
      .map(id => AgentLoginRequest.apply(Some(id), None))
  }
  val onlyPhoneNumber: Reads[AgentLoginRequest] = {
    (JsPath \ "agentphonenumber")
      .read[String]
      .map(phoneNb => AgentLoginRequest.apply(None, Some(phoneNb)))
  }
  val onlyPhoneNumberAsLong: Reads[AgentLoginRequest] = {
    (JsPath \ "agentphonenumber")
      .read[Long]
      .map(phoneNb => AgentLoginRequest.apply(None, Some(phoneNb.toString)))
  }
  val agentIdAndAgentPhoneNumber: Reads[AgentLoginRequest] = {
    ((JsPath \ "agentid").read[Long] and
      (JsPath \ "agentphonenumber").read[String])((id, phoneNb) =>
      AgentLoginRequest.apply(Some(id), Some(phoneNb))
    )
  }

  implicit val AgentLoginRequestRead: Reads[AgentLoginRequest] =
    allFields orElse agentIdAndAgentPhoneNumber orElse onlyAgentId orElse onlyPhoneNumber orElse onlyPhoneNumberAsLong
}

case class AgentLogoutRequest(id: Option[Agent.Id]) extends AgentActionRequest

object AgentLogoutRequest {
  def validate(json: JsValue): JsResult[AgentLogoutRequest] =
    json.validate[AgentLogoutRequest]
  implicit val AgentLogoutRequestReads: Reads[AgentLogoutRequest] =
    (JsPath \ "agentid")
      .readNullable[Agent.Id]
      .map(AgentLogoutRequest.apply _)
}
case class AgentLogout(phoneNumber: String) extends AgentActionRequest

case class AgentPauseRequest(
    id: Option[Agent.Id],
    reason: Option[String] = None
) extends AgentActionRequest

object AgentPauseRequest {
  def validate(json: JsValue): JsResult[AgentPauseRequest] =
    json.validate[AgentPauseRequest]
  implicit val AgentPauseRequestReads: Reads[AgentPauseRequest] =
    ((JsPath \ "agentid").readNullable[Agent.Id] and
      (JsPath \ "reason").readNullable[String])((id, reason) =>
      AgentPauseRequest.apply(id, reason)
    )
}

case class AgentUnPauseRequest(id: Option[Agent.Id]) extends AgentActionRequest

object AgentUnPauseRequest {
  def validate(json: JsValue): JsResult[AgentUnPauseRequest] =
    json.validate[AgentUnPauseRequest]
  implicit val AgentUnPauseRequestReads: Reads[AgentUnPauseRequest] =
    (JsPath \ "agentid")
      .readNullable[Agent.Id]
      .map(AgentUnPauseRequest.apply _)
}

case class AgentTogglePause(phoneNumber: String) extends AgentActionRequest
