package services.request

import services.XucAmiBus.{AmiMessage, SetVarActionRequest, SetVarRequest}
import services.request
import services.request.PhoneKey.PhoneKey
import xivo.models.Agent

object PhoneKey extends Enumeration {
  type PhoneKey = Value
  val Pause: request.PhoneKey.Value = Value
  val Logon: request.PhoneKey.Value = Value
  val Spied: request.PhoneKey.Value = Value
}

object KeyLightRequest {
  val LogonPrefix   = "30"
  val PausePrefix   = "34"
  val SpiedPrefix   = "35"
  val AmiVarSuffix  = "DEVICE_STATE(Custom:***"
  val AmiLightOn    = "INUSE"
  val AmiLightOff   = "NOT_INUSE"
  val AmiLightBlink = "ONHOLD"
}

abstract class KeyLightRequest extends XucRequest {
  def toAmi: AmiMessage
}

case class TurnOffKeyLight(phoneNb: String, key: PhoneKey)
    extends KeyLightRequest {
  import KeyLightRequest._
  def toAmi: AmiMessage =
    key match {
      case PhoneKey.Pause =>
        SetVarRequest(
          SetVarActionRequest(
            s"$AmiVarSuffix$PausePrefix$phoneNb)",
            AmiLightOff
          )
        )
      case PhoneKey.Logon =>
        SetVarRequest(
          SetVarActionRequest(
            s"$AmiVarSuffix$LogonPrefix$phoneNb)",
            AmiLightOff
          )
        )
      case PhoneKey.Spied =>
        SetVarRequest(
          SetVarActionRequest(
            s"$AmiVarSuffix$SpiedPrefix$phoneNb)",
            AmiLightOff
          )
        )
    }
}

case class TurnOnKeyLight(phoneNb: String, key: PhoneKey)
    extends KeyLightRequest {
  import KeyLightRequest._
  def toAmi: AmiMessage =
    key match {
      case PhoneKey.Pause =>
        SetVarRequest(
          SetVarActionRequest(s"$AmiVarSuffix$PausePrefix$phoneNb)", AmiLightOn)
        )
      case PhoneKey.Logon =>
        SetVarRequest(
          SetVarActionRequest(s"$AmiVarSuffix$LogonPrefix$phoneNb)", AmiLightOn)
        )
      case PhoneKey.Spied =>
        SetVarRequest(
          SetVarActionRequest(s"$AmiVarSuffix$SpiedPrefix$phoneNb)", AmiLightOn)
        )
    }
}

case class TurnOffKeyLightWithAgNum(
    phoneNb: String,
    key: PhoneKey,
    agentNb: Agent.Number
) extends KeyLightRequest {
  import KeyLightRequest._
  def toAmi: AmiMessage =
    key match {
      case PhoneKey.Logon =>
        SetVarRequest(
          SetVarActionRequest(
            s"$AmiVarSuffix$LogonPrefix$phoneNb*$agentNb)",
            AmiLightOff
          )
        )
    }
}

case class TurnOnKeyLightWithAgNum(
    phoneNb: String,
    key: PhoneKey,
    agentNb: Agent.Number
) extends KeyLightRequest {
  import KeyLightRequest._
  def toAmi: AmiMessage =
    key match {
      case PhoneKey.Logon =>
        SetVarRequest(
          SetVarActionRequest(
            s"$AmiVarSuffix$LogonPrefix$phoneNb*$agentNb)",
            AmiLightOn
          )
        )
    }
}

case class BlinkKeyLight(phoneNb: String, key: PhoneKey)
    extends KeyLightRequest {
  import KeyLightRequest._
  def toAmi: AmiMessage =
    key match {
      case PhoneKey.Pause =>
        SetVarRequest(
          SetVarActionRequest(
            s"$AmiVarSuffix$PausePrefix$phoneNb)",
            AmiLightBlink
          )
        )
    }
}
