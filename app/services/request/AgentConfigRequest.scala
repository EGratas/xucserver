package services.request

import models.QueueMembership
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, JsValue, Reads}
import play.api.libs.json.JsResult

class AgentConfigRequest extends XucRequest

case class MoveAgentInGroup(
    groupId: Long,
    fromQueueId: Long,
    fromPenalty: Int,
    toQueueId: Long,
    toPenalty: Int
) extends AgentConfigRequest
object MoveAgentInGroup {
  def validate(json: JsValue): JsResult[MoveAgentInGroup] =
    json.validate[MoveAgentInGroup]

  implicit val MoveAgentInGroupReads: Reads[MoveAgentInGroup] = (
    (JsPath \ "groupId").read[Long] and
      (JsPath \ "fromQueueId").read[Long] and
      (JsPath \ "fromPenalty").read[Int] and
      (JsPath \ "toQueueId").read[Long] and
      (JsPath \ "toPenalty").read[Int]
  )(MoveAgentInGroup.apply _)
}

case class AddAgentInGroup(
    groupId: Long,
    fromQueueId: Long,
    fromPenalty: Int,
    toQueueId: Long,
    toPenalty: Int
) extends AgentConfigRequest
object AddAgentInGroup {
  def validate(json: JsValue): JsResult[AddAgentInGroup] =
    json.validate[AddAgentInGroup]

  implicit val AddAgentInGroupReads: Reads[AddAgentInGroup] = (
    (JsPath \ "groupId").read[Long] and
      (JsPath \ "fromQueueId").read[Long] and
      (JsPath \ "fromPenalty").read[Int] and
      (JsPath \ "toQueueId").read[Long] and
      (JsPath \ "toPenalty").read[Int]
  )(AddAgentInGroup.apply _)
}

case class RemoveAgentGroupFromQueueGroup(
    groupId: Long,
    queueId: Long,
    penalty: Int
) extends AgentConfigRequest

object RemoveAgentGroupFromQueueGroup {
  def validate(json: JsValue): JsResult[RemoveAgentGroupFromQueueGroup] =
    json.validate[RemoveAgentGroupFromQueueGroup]

  implicit val RemoveAgentGroupFromQueueGroupReads
      : Reads[RemoveAgentGroupFromQueueGroup] = (
    (JsPath \ "groupId").read[Long] and
      (JsPath \ "queueId").read[Long] and
      (JsPath \ "penalty").read[Int]
  )(RemoveAgentGroupFromQueueGroup.apply _)

}

case class AddAgentsNotInQueueFromGroupTo(
    groupId: Long,
    queueId: Long,
    penalty: Int
) extends AgentConfigRequest

object AddAgentsNotInQueueFromGroupTo {
  def validate(json: JsValue): JsResult[AddAgentsNotInQueueFromGroupTo] =
    json.validate[AddAgentsNotInQueueFromGroupTo]

  implicit val AddAgentsNotInQueueFromGroupToReads
      : Reads[AddAgentsNotInQueueFromGroupTo] = (
    (JsPath \ "groupId").read[Long] and
      (JsPath \ "queueId").read[Long] and
      (JsPath \ "penalty").read[Int]
  )(AddAgentsNotInQueueFromGroupTo.apply _)

}

case class SetAgentGroup(agentId: Long, groupId: Long)
    extends AgentConfigRequest

object SetAgentGroup {
  def validate(json: JsValue): JsResult[SetAgentGroup] =
    json.validate[SetAgentGroup]

  implicit val setAgentGroupRead: Reads[SetAgentGroup] =
    ((JsPath \ "agentId").read[Long] and
      (JsPath \ "groupId").read[Long])(SetAgentGroup.apply _)
}

case class GetUserDefaultMembership(userId: Long) extends AgentConfigRequest

object GetUserDefaultMembership {
  def validate(json: JsValue): JsResult[GetUserDefaultMembership] =
    json.validate[GetUserDefaultMembership]

  implicit val getUserDefaultMembership: Reads[GetUserDefaultMembership] =
    (JsPath \ "userId").read[Long].map(GetUserDefaultMembership.apply _)
}

case class SetUserDefaultMembership(
    userId: Long,
    membership: List[QueueMembership]
) extends AgentConfigRequest

object SetUserDefaultMembership {
  import QueueMembership._

  def validate(json: JsValue): JsResult[SetUserDefaultMembership] =
    json.validate[SetUserDefaultMembership]

  implicit val setUserDefaultMembership: Reads[SetUserDefaultMembership] =
    ((JsPath \ "userId").read[Long] and
      (JsPath \ "membership").read[List[QueueMembership]])(
      SetUserDefaultMembership.apply _
    )
}

case class SetUsersDefaultMembership(
    userIds: List[Long],
    membership: List[QueueMembership]
) extends AgentConfigRequest

object SetUsersDefaultMembership {
  import QueueMembership._

  def validate(json: JsValue): JsResult[SetUsersDefaultMembership] =
    json.validate[SetUsersDefaultMembership]

  implicit val setUsersDefaultMembership: Reads[SetUsersDefaultMembership] =
    ((JsPath \ "userIds").read[List[Long]] and
      (JsPath \ "membership").read[List[QueueMembership]])(
      SetUsersDefaultMembership.apply _
    )
}

case class ApplyUsersDefaultMembership(userIds: List[Long])
    extends AgentConfigRequest

object ApplyUsersDefaultMembership {
  def validate(json: JsValue): JsResult[ApplyUsersDefaultMembership] =
    json.validate[ApplyUsersDefaultMembership]

  implicit val applyUsersDefaultMembership: Reads[ApplyUsersDefaultMembership] =
    (JsPath \ "userIds")
      .read[List[Long]]
      .map(ApplyUsersDefaultMembership.apply _)
}
