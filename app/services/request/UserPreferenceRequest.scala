package services.request

import org.apache.pekko.actor.ActorRef
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, JsResult, JsValue, Json, Reads}
import xivo.models.UserPreference

import scala.util.{Failure, Success, Try}

sealed trait UserPreferenceRequest extends XucRequest {
  val userId: Option[Long]

  def enrichWithUserId(userId: Long): Try[UserPreferenceRequest] =
    this match {
      case up: SetUserPreferenceRequest =>
        Success(up.copy(userId = Some(userId)))
      case _ => Failure(new Exception("Cannot enrich with user id"))
    }
}

case class SetUserPreferenceRequest(
    userId: Option[Long],
    key: String,
    value: String,
    value_type: String
) extends UserPreferenceRequest

case class UserPreferencesInit(userId: Option[Long], ctiRouterRef: ActorRef)
    extends UserPreferenceRequest

case class UserPreferenceDisconnect(userId: Option[Long])

sealed trait UserPreferenceChange
case class UserPreferenceEdited(userId: Long)  extends UserPreferenceChange
case class UserPreferenceCreated(userId: Long) extends UserPreferenceChange
case class UserPreferenceDeleted(userId: Long) extends UserPreferenceChange

case object SetUserPreferenceRequest {
  implicit val reads: Reads[SetUserPreferenceRequest] = (
    (JsPath \ "userId").readNullable[Long] and
      (JsPath \ "key").read[String] and
      (JsPath \ "value").read[String] and
      (JsPath \ "value_type").read[String]
  )(SetUserPreferenceRequest.apply)

  def validate(json: JsValue): JsResult[SetUserPreferenceRequest] =
    json.validate[SetUserPreferenceRequest]
}

case class UserPreferencesWithRef(
    ctiRouterRef: ActorRef,
    preferences: List[UserPreference]
)
