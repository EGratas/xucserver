package services.request

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, JsValue, Json, Reads}
import play.api.libs.json.JsResult

abstract class UserActionRequest extends XucRequest
abstract class ForwardRequest(destination: String, state: Boolean)
    extends UserActionRequest

case class NaForward(destination: String, state: Boolean)
    extends ForwardRequest(destination, state)
case class UncForward(destination: String, state: Boolean)
    extends ForwardRequest(destination, state)
case class BusyForward(destination: String, state: Boolean)
    extends ForwardRequest(destination, state)

object NaForward {
  def validate(json: JsValue): JsResult[NaForward] = json.validate[NaForward]
  implicit val NaForwardRead: Reads[NaForward] = (
    (JsPath \ "destination").read[String] and
      (JsPath \ "state").read[Boolean]
  )(NaForward.apply)
}

object UncForward {
  def validate(json: JsValue): JsResult[UncForward] = json.validate[UncForward]
  implicit val UncForwardRead: Reads[UncForward] = (
    (JsPath \ "destination").read[String] and
      (JsPath \ "state").read[Boolean]
  )(UncForward.apply)
}

object BusyForward {
  def validate(json: JsValue): JsResult[BusyForward] =
    json.validate[BusyForward]
  implicit val BusyForwardRead: Reads[BusyForward] = (
    (JsPath \ "destination").read[String] and
      (JsPath \ "state").read[Boolean]
  )(BusyForward.apply)
}

case class UserStatusUpdateReq(userId: Option[Long], status: String)
    extends UserActionRequest

object UserStatusUpdateReq {
  def validate(json: JsValue): JsResult[UserStatusUpdateReq] =
    json.validate[UserStatusUpdateReq]
  implicit val UserStatusUpdateReqRead: Reads[UserStatusUpdateReq] = (
    (JsPath \ "userId").readNullable[Long] and
      (JsPath \ "status").read[String]
  )(UserStatusUpdateReq.apply)
}

case class DndReq(state: Boolean, domain: String = "default")
    extends UserActionRequest
object DndReq {
  def validate(json: JsValue): JsResult[DndReq] = json.validate[DndReq]
  implicit val DndReqRead: Reads[DndReq] = ((JsPath \ "state").read[Boolean] and
    (JsPath \ "domain")
      .readNullable[String]
      .map(_.getOrElse("default")))(DndReq.apply _)
}
