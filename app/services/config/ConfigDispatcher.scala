package services.config

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import org.apache.pekko.pattern._
import org.apache.pekko.util.Timeout
import com.codahale.metrics.{MetricRegistry, SharedMetricRegistries}
import com.google.inject.Inject
import com.google.inject.name.Named
import helpers.{JmxActorSingletonMonitor, JmxLongMetric}
import models._
import org.asteriskjava.manager.event.{ExtensionStatusEvent, QueueEntryEvent}
import org.joda.time.{DateTime, DateTimeZone}
import org.xivo.cti.message.{
  AgentConfigUpdate => _,
  QueueConfigUpdate => _,
  UserConfigUpdate => _,
  _
}
import org.xivo.cti.model.{Meetme, PhoneHintStatus, StatName}
import play.api.libs.json._
import services.XucAmiBus.{AmiExtensionStatusEvent, AmiFailure, AmiType}
import services.XucEventBus.XucEvent
import services.XucStatsEventBus.{AggregatedStatEvent, Stat, StatUpdate}
import services._
import services.agent.AgentStatistic
import services.calltracking.DevicesTracker
import services.config.ExtensionManager.GetExtensionStatus
import services.request._
import services.video.model.{UserVideoEvent, VideoStatusEvent}
import stats.DoubleGauge
import xivo.events.cti.models.{
  CtiStatusLegacyMapWrapper,
  CtiStatusMapWrapper,
  CtiStatusTraits
}
import xivo.events.{AgentState, CurrentCallsPhoneEvents, PhoneEvent}
import xivo.models.XivoObject.{ObjectDefinition, ObjectType => StatObjectType}
import xivo.models._
import xivo.websocket.{
  WsConferenceEvent,
  WsConferenceParticipant,
  WsConferenceParticipantEvent
}
import xivo.xuc.XucBaseConfig
import xivo.xucami.models.{AttendedTransferFinished, EnterQueue, LeaveQueue}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success, Try}

case class UserPhoneStatus(username: String, status: PhoneHintStatus)

case class OutboundQueue(queue: QueueConfigUpdate)

case class AgentOnPhone(agentId: Agent.Id, phoneNumber: String)

object ObjectType extends Enumeration {
  type ObjectType = Value
  val TypeQueue: ObjectType.Value            = Value
  val TypeAgent: ObjectType.Value            = Value
  val TypeQueueMember: ObjectType.Value      = Value
  val TypeBaseQueueMember: ObjectType.Value  = Value
  val TypeUser: ObjectType.Value             = Value
  val TypePhone: ObjectType.Value            = Value
  val TypeLine: ObjectType.Value             = Value
  val TypeAgentGroup: ObjectType.Value       = Value
  val TypeMeetme: ObjectType.Value           = Value
  val TypeCallback: ObjectType.Value         = Value
  val TypePhoneHint: ObjectType.Value        = Value
  val TypeIce: ObjectType.Value              = Value
  val TypeVideoStatusEvent: ObjectType.Value = Value
}

object ConfigDispatcher {

  import services.config.AgentDirectoryEntry.agentDirectoryEntryWrites

  implicit val agentDirectoryWrites: Writes[AgentDirectory] =
    (c: AgentDirectory) => {
      Json.obj("directory" -> Json.toJson(c.directory))
    }

  trait DeviceType {
    def name: String
  }
  case object TypePhoneDevice extends DeviceType {
    val name = "TypePhoneDevice"
  }

  case object TypeDefaultDevice extends DeviceType {
    val name = "TypeDefaultDevice"
  }

  case class AgentDirectory(directory: List[AgentDirectoryEntry])

  object AgentDirectory {
    def toJson(agentDirectory: AgentDirectory): JsValue =
      Json.toJson(agentDirectory)
  }

  import services.config.ObjectType._

  case class RequestStatus(requester: ActorRef, id: Int, objectType: ObjectType)

  case class ConfigChangeRequest(
      requester: ActorRef,
      update: UpdateConfigRequest
  )

  sealed class ConfigQuery extends XucRequest

  case object GetAgentStates extends ConfigQuery {
    def validate(json: JsValue): JsSuccess[GetAgentStates.type] = JsSuccess(
      GetAgentStates
    )
  }

  case object GetAgentStatistics extends ConfigQuery

  case object GetAgentDirectory extends ConfigQuery {
    def validate(json: JsValue): JsSuccess[GetAgentDirectory.type] = JsSuccess(
      GetAgentDirectory
    )
  }

  case class GetConfig(objectType: ObjectType) extends ConfigQuery

  case object GetIceConfig extends ConfigQuery {
    def validate(json: JsValue): JsSuccess[GetIceConfig.type] = JsSuccess(
      GetIceConfig
    )
  }

  case class GetAgents(groupId: Long, queueId: Long, penalty: Int)
      extends ConfigQuery

  case class GetAgentByUserId(userId: Long) extends ConfigQuery

  case class GetAgentsNotInQueue(groupId: Long, queueId: Long)
      extends ConfigQuery

  case class GetQueuesForAgent(agentId: Agent.Id) extends ConfigQuery

  case class GetUserServices(userId: Int) extends ConfigQuery

  case class GetUserConfig(userId: Long) extends ConfigQuery

  case object GetQueueStatistics extends ConfigQuery

  case class RequestConfig(requester: ActorRef, request: ConfigQuery)

  case class RequestConfigForUsername(
      requester: ActorRef,
      request: ConfigQuery,
      username: Option[String]
  )

  case class RequestConfigDeviceChange(
      requester: ActorRef,
      deviceType: DeviceType,
      username: Option[String]
  )

  case class RefreshAgentQueueMember(
      agentId: Agent.Id,
      updatedAgentQueueMembers: List[AgentQueueMember]
  )

  case class RefreshAgent(agentId: Long)

  case class RefreshLine(endpoint: Endpoint)

  object GetConfig {
    def validate(json: JsValue): JsResult[GetConfig] = json.validate[GetConfig]

    def apply(objectType: String): GetConfig =
      objectType match {
        case "queue"       => GetConfig(TypeQueue)
        case "agent"       => GetConfig(TypeAgent)
        case "queuemember" => GetConfig(TypeQueueMember)
        case "line"        => GetConfig(TypeLine)
        case unknown       => GetConfig(TypeAgent)
      }

    implicit val GetConfigRead: Reads[GetConfig] =
      (JsPath \ "objectType").read[String].map(GetConfig.apply)
  }

  case class GetList(objectType: ObjectType) extends ConfigQuery

  object GetList {
    def apply(objectType: String): GetList =
      objectType match {
        case "queue"           => GetList(TypeQueue)
        case "agent"           => GetList(TypeAgent)
        case "agentgroup"      => GetList(TypeAgentGroup)
        case "queuemember"     => GetList(TypeQueueMember)
        case "meetme"          => GetList(TypeMeetme)
        case "basequeuemember" => GetList(TypeBaseQueueMember)
        case unknown           => GetList(TypeAgent)
      }

    def validate(json: JsValue): JsResult[GetList] = json.validate[GetList]

    implicit val GetListRead: Reads[GetList] =
      (JsPath \ "objectType").read[String].map(GetList.apply)
  }

  case class LineConfigQueryByNb(number: String) extends ConfigQuery

  case class LineConfigQueryById(id: Int) extends ConfigQuery

  case class LineConfigDeviceChangeQuery(id: Int, deviceType: DeviceType)
      extends ConfigQuery

  case class OutboundQueueQuery(queueIds: List[Long]) extends ConfigQuery

  case class GetAgentOnPhone(phoneNumber: String) extends ConfigQuery

  case class QueueList(queues: List[QueueConfigUpdate])

  case class AgentList(agents: List[Agent])

  case class AgentGroupList(agentGroups: List[AgentGroup])

  case class AgentQueueMemberList(agentQueueMembers: List[AgentQueueMember])

  case class MeetmeList(meetmes: List[Meetme])

  case class QueuesForAgent(queues: List[AgentQueueMember], agentId: Agent.Id)

  case class GetQueueCalls(queueId: Long) extends ConfigQuery

  case class MonitorPhoneHint(ref: ActorRef, numbers: List[String])

  case class MonitorVideoStatus(ref: ActorRef, users: List[String])

  case object ToggleUniqueAccountDevice {
    def apply(deviceType: String): ToggleUniqueAccountDevice =
      deviceType match {
        case "phone"  => ToggleUniqueAccountDevice(None, TypePhoneDevice)
        case "webrtc" => ToggleUniqueAccountDevice(None, TypeDefaultDevice)
        case _        => ToggleUniqueAccountDevice(None, TypeDefaultDevice)
      }
    implicit val deviceTypeReads: Reads[ToggleUniqueAccountDevice] =
      (JsPath \ "deviceType").read[String].map(ToggleUniqueAccountDevice.apply)
    def validate(json: JsValue): JsResult[ToggleUniqueAccountDevice] =
      json.validate[ToggleUniqueAccountDevice]
  }

  case class ToggleUniqueAccountDevice(
      currentLine: Option[Line],
      deviceType: DeviceType
  ) extends ConfigQuery

  case class DisplayNameLookup(username: String) extends ConfigQuery
  object DisplayNameLookup {
    implicit val reads: Reads[DisplayNameLookup] =
      (JsPath \ "username").read[String].map(DisplayNameLookup.apply)
    def validate(json: JsValue): JsResult[DisplayNameLookup] =
      json.validate[DisplayNameLookup]
  }
}

trait MetricUpdate {
  def updateOrCreateMetric(
      queueId: Int,
      statName: String,
      statValue: Double
  ): Unit

  def prefix(queueId: Int, statName: String): String =
    s"Queue.$queueId.$statName"
}

trait MetricCache extends MetricUpdate {
  val xucConfig: XucBaseConfig

  val defaultRegistry: MetricRegistry =
    SharedMetricRegistries.getOrCreate(xucConfig.statsMetricsRegistryName)

  def updateOrCreateMetric(
      queueId: Int,
      statName: String,
      statValue: Double
  ): Unit =
    (defaultRegistry.getGauges.get(prefix(queueId, statName)) match {
      case gauge: DoubleGauge => gauge
      case _ =>
        defaultRegistry.register(prefix(queueId, statName), new DoubleGauge())
    }).setValue(statValue)
}

class ConfigDispatcher @Inject() (
    val configRepository: ConfigRepository,
    @Named(ActorIds.AgentManagerId) val agentManager: ActorRef,
    @Named(
      ActorIds.DefaultQueueMembershipRepositoryIds
    ) val defaultMembershipRepo: ActorRef,
    @Named(ActorIds.GlobalAggregatorId) val statAgreggator: ActorRef,
    val dbUser: XivoUserDao,
    val dbWebServiceUser: WebServiceUserDao,
    val eventBus: XucEventBus,
    val amiBus: XucAmiBus,
    val agentQueueMemberFactory: AgentQueueMemberFactory,
    val agentGroupFactory: AgentGroupFactory,
    val userLineNumberFactory: UserLineNumberFactory,
    @Named(ActorIds.DevicesTrackerId) devicesTracker: ActorRef,
    @Named(ActorIds.ConfigServiceManagerId) val configServiceManager: ActorRef,
    @Named(ActorIds.StatusPublishId) val statusPublish: ActorRef,
    @Named(ActorIds.ExtensionManager) val extensionManager: ActorRef,
    val agentLoginStatusDao: AgentLoginStatusDao,
    val extensionPattern: ExtensionPatternFactory,
    val xucConfig: XucBaseConfig,
    @Named(ActorIds.UserPreferenceService) userPreferenceService: ActorRef,
    @Named(ActorIds.CtiRouterFactoryId) ctiRouterFactory: ActorRef,
    @Named(ActorIds.AmiBusConnectorId) val amiBusConnector: ActorRef
) extends Actor
    with ActorLogging
    with ConfigInitializer
    with MetricCache
    with JmxActorSingletonMonitor
    with CtiStatusTraits {
  this: ConfigInitializer with MetricUpdate =>

  import services.config.ObjectType._
  import services.config.ConfigDispatcher._
  import services.config.ConfigManager.PublishUserPhoneStatuses

  val jmxAgentCount: Try[JmxLongMetric] =
    jmxBean.addLong("Agents", 0L, Some("Number of loaded agents"))

  val jmxUsersDndCount: Try[JmxLongMetric] =
    jmxBean.addLong("Users", 0L, Some("Number of users in Do Not Disturb mode"))

  log.info("ConfigDispatcher started " + self)

  eventBus.subscribe(self, XucEventBus.allAgentsEventsTopic)
  eventBus.subscribe(self, XucEventBus.allAgentsStatsTopic)
  amiBus.subscribe(self, AmiType.QueueEvent)
  amiBus.subscribe(self, AmiType.AmiService)
  amiBus.subscribe(self, AmiType.TransferEvent)
  amiBus.subscribe(self, AmiType.ExtensionStatusEvent)

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))

    extensionPattern
      .getAll()
      .foreach(_.foreach(exten => self ! exten))
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
  }

  def now(): DateTime = new DateTime(DateTimeZone.UTC)

  def emptyLineConfig: LineConfig =
    LineConfig("-", "-")

  def configDispatcherReceive: Receive = {

    case e: ExtensionPattern =>
      configRepository.updateExtensionPattern(e)

    case UserConfigUpdated(userId, _) =>
      log.debug(s"User config updated for $userId")
      updateUserByWS(userId)

    case UserServicesUpdated(userId, services) =>
      log.debug(s"User services updated for $userId: $services)")
      val previousServices = configRepository.getUserServices(userId)
      configRepository.onUserServicesUpdated(userId, services)
      eventBus.publish(UserServicesUpdated(userId, services))

      val funKeys = for {
        phoneprogPattern <-
          configRepository.getExtensionPattern(ExtensionName.ProgrammableKey)
        dndPattern <- configRepository.getExtensionPattern(ExtensionName.DND)
        fwdbusyPattern <-
          configRepository.getExtensionPattern(ExtensionName.BusyForward)
        fwdrnaPattern <-
          configRepository.getExtensionPattern(ExtensionName.NoAnswerForward)
        fwduncPattern <- configRepository.getExtensionPattern(
          ExtensionName.UnconditionalForward
        )
      } yield ExtensionPattern.userServices(
        userId.toString,
        previousServices,
        services,
        phoneprogPattern,
        dndPattern,
        fwdbusyPattern,
        fwdrnaPattern,
        fwduncPattern
      )

      funKeys
        .getOrElse(List.empty)
        .map(_.toAmi)
        .foreach(amiBus.publish)

      jmxUsersDndCount.set(
        configRepository.userServices.count(u => u._2.dndEnabled)
      )

    case phoneConfig: PhoneConfigUpdate =>
      log.debug(s"phoneConfigUpdate: $phoneConfig")
      attemptLineUpdate(phoneConfig.getUserId.toLong, None)

    case ConfigChangeRequest(requester, updateLineForUser: UpdateLineForUser) =>
      log.debug(s"Processing UpdateLineForUser: ${updateLineForUser.userId}")
      attemptLineUpdate(updateLineForUser.userId, Some(requester))

    case userLine: UserLineNumber =>
      extensionManager ! GetExtensionStatus(userLine.number)
      processLineUpdate(userLine)

    case AmiExtensionStatusEvent(exs: ExtensionStatusEvent) =>
      configRepository.updatePhoneStatus(exs.getExten, exs.getStatus)
      configRepository.getUserPhoneStatus(
        exs.getExten,
        exs.getStatus
      ) foreach (statusPublish ! _)
      configRepository.getPhoneHintStatusEventByNumber(
        exs.getExten
      ) foreach eventBus.publish

    case queueStats: QueueStatistics =>
      log.debug(s"queue statistics : $queueStats")
      val stats = queueStats.getCounters.asScala
        .withFilter(s =>
          !Seq(StatName.LongestWaitTime, StatName.WaitingCalls)
            .contains(s.getStatName)
        )
        .map { counter =>
          Stat(counter.getStatName.name, counter.getValue.toDouble)
        }
        .toList
      statAgreggator ! StatUpdate(
        ObjectDefinition(StatObjectType.Queue, Some(queueStats.getQueueId)),
        stats
      )
      log.debug(
        s"Aggregates: ${StatUpdate(ObjectDefinition(StatObjectType.Queue, Some(queueStats.getQueueId)), stats)}"
      )

    case PublishUserPhoneStatuses =>
      configRepository.getAllUserPhoneStatuses.foreach(sender() ! _)

    case MonitorPhoneHint(ref, numbers) =>
      log.debug(
        "{} subscribes to phone hint event of {}",
        ref.path.name,
        numbers
      )
      unsubscribeFromAllPhoneHints(ref)
      for (number <- numbers) {
        eventBus.subscribe(ref, XucEventBus.phoneHintEventTopic(number))
        configRepository
          .getPhoneHintStatusEventByNumber(number)
          .foreach(eventBus.publish)
      }

    case MonitorVideoStatus(ref, users) =>
      log.debug(
        "{} subscribes to video status event of {}",
        ref.path.name,
        users
      )
      unsubscribeFromAllVideoStatus(ref)
      for (user <- users) {
        eventBus.subscribe(ref, XucEventBus.videoStatusTopic(user))
        configRepository
          .getVideoStatus(user)
          .foreach(eventBus.publish)
      }

    case RequestConfig(requester, lcr: LineConfigQueryByNb) =>
      log.debug(s"$requester config request : $lcr")
      configRepository.getLineConfig(lcr) match {
        case Some(lineConfig) =>
          requester ! lineConfig
        case None =>
          log.error(s"unable to find any config for nb ${lcr.number}")
      }

    case RequestConfig(requester, lcr: LineConfigQueryById) =>
      log.debug(s"$requester config request : $lcr")

      configRepository.getLineConfig(lcr) match {
        case Some(lineConfig) =>
          lineConfig.line.foreach(
            devicesTracker ! DevicesTracker.EnsureTrackerFor(_)
          )
          requester ! lineConfig
        case None =>
          log.error(s"unable to find any config for id ${lcr.id}")
          requester ! emptyLineConfig
      }

    case RequestConfig(requester, query: GetAgentByUserId) =>
      log.debug(s"$requester config request : $query")
      configRepository.getAgentByUserId(query.userId) match {
        case Some(agent) => requester ! agent
        case None =>
          log.error(s"unable to find agent for user id ${query.userId}")
      }

    case RequestConfig(_, query: GetUserConfig) =>
      log.debug(s"user config request : $query")
      val userId = query.userId
      val userConfigUpdate: Option[UserConfigUpdate] = for {
        user     <- configRepository.getCtiUser(userId)
        services <- configRepository.getUserServices(userId.toInt)
      } yield getUserConfigUpdate(user, services)

      userConfigUpdate.foreach(c => eventBus.publish(c))

    case RequestConfig(requester, query: GetUserServices) =>
      log.debug(s"$requester config request : $query")
      configRepository
        .getUserServices(query.userId) match {
        case Some(services) =>
          self ! UserServicesUpdated(query.userId, services)
          requester ! services
        case None =>
          configRepository.configServerRequester
            .getUserServices(query.userId)
            .foreach(services => {
              self ! UserServicesUpdated(query.userId, services)
              requester ! services
            })
      }

    case RequestConfig(requester, GetIceConfig) =>
      log.debug(s"$requester ice config request")
      requester ! configRepository.getIceConfig

    case RequestConfigForUsername(
          requester,
          ToggleUniqueAccountDevice(currentLine, deviceType),
          username
        ) =>
      username.flatMap(username =>
        configRepository.getLineForUser(username)
      ) match {
        case Some(line) =>
          self ! ConfigChangeRequest(
            requester,
            ChangeDeviceForUser(
              LineConfigQueryById(line.id),
              currentLine,
              deviceType
            )
          )
        case None =>
          log.warning(s"User $username has no line, returning empty config")
          requester ! emptyLineConfig
      }

    case ConfigChangeRequest(
          requester,
          ChangeDeviceForUser(lcr, currentLine, deviceType)
        ) =>
      log.debug(s"$requester config request : $lcr")
      configRepository.updateLineDevice(lcr.id, deviceType)
      configRepository.getLineConfig(lcr) match {
        case Some(lineConfig) =>
          for {
            newLine <- lineConfig.line
            oldLine <- currentLine
          } devicesTracker ! DevicesTracker.SwitchTrackerFor(oldLine, newLine)
          requester ! lineConfig
        case None =>
          log.error(s"unable to find any config for id ${lcr.id}")
          requester ! emptyLineConfig
      }

    case RequestConfigForUsername(requester, GetConfig(objectType), username) =>
      log.debug(s"$requester config request : $objectType")
      objectType match {
        case TypeAgent =>
          requester ! configRepository.getAgents().foreach(requester ! _)
        case TypeQueue => configRepository.getQueues().foreach(requester ! _)
        case TypeQueueMember =>
          configRepository.getAgentQueueMembers().foreach(requester ! _)
        case TypeLine =>
          username.flatMap(username =>
            configRepository.getLineForUser(username)
          ) match {
            case Some(line) =>
              requestConfigOrUpdateLine(requester, username, line)
            case None =>
              log.warning(s"User $username has no line, returning empty config")
              requester ! emptyLineConfig
          }
        case unknown =>
      }

    case RequestConfig(requester, GetList(objectType)) =>
      log.debug(s"$requester config list request : $objectType")
      objectType match {
        case TypeQueue => requester ! QueueList(configRepository.getQueues())
        case TypeAgent => requester ! AgentList(configRepository.getAgents())
        case TypeAgentGroup =>
          requester ! AgentGroupList(agentGroupFactory.all())
        case TypeQueueMember =>
          requester ! AgentQueueMemberList(
            configRepository.getAgentQueueMembers()
          )
        case TypeMeetme =>
          requester ! MeetmeList(configRepository.getMeetmeList)
        case TypeBaseQueueMember =>
          defaultMembershipRepo.tell(GetAllEntries, requester)
        case unknown =>
      }

    case RequestConfig(requester, GetAgents(groupId, queueId, penalty)) =>
      requester ! AgentList(
        configRepository.getAgents(groupId, queueId, penalty)
      )

    case RequestConfig(requester, GetAgentsNotInQueue(groupId, queueId)) =>
      requester ! AgentList(
        configRepository.getAgentsNotInQueue(groupId, queueId)
      )

    case RequestConfig(requester, OutboundQueueQuery(queueIds)) =>
      configRepository
        .getOutboundQueue(queueIds)
        .foreach(requester ! OutboundQueue(_))

    case RequestStatus(requester, id, TypeAgent) =>
      configRepository.getAgentState(id).foreach(requester ! _)

    case RequestStatus(requester, id, TypeUser) =>
      configRepository.getUserStatus(id).foreach(requester ! _)

    case RequestStatus(requester, id, TypePhone) =>
      configRepository.getPhoneStatus(id).foreach(requester ! _)

    case agentState: AgentState =>
      log.debug(s"Receiving AgentStatus: $agentState ${agentState.cause}")
      configRepository.onAgentState(agentState)

    case agentQueueMember: AgentQueueMember
        if !configRepository.queueMemberExists(agentQueueMember) =>
      if (agentQueueMember.penalty >= 0)
        configRepository.updateOrAddQueueMembers(agentQueueMember)
      eventBus.publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeQueueMember),
          agentQueueMember
        )
      )

    case RequestConfig(requester, GetAgentDirectory) =>
      val dir = AgentDirectory(configRepository.getAgentDirectory)
      log.debug(s"Agent directory: $dir")
      requester ! dir

    case RequestConfig(requester, GetAgentStates) =>
      configRepository.getAgentStates.foreach(requester ! _)

    case RequestConfig(requester, GetAgentStatistics) =>
      configRepository.getAgentStatistics.foreach(requester ! _)

    case RequestConfig(requester, GetQueueCalls(queueId)) =>
      requester ! configRepository.getQueueCalls(queueId)

    case RequestConfig(requester, GetAgentOnPhone(phoneNumber)) =>
      configRepository
        .getAgentLoggedOnPhoneNumber(phoneNumber)
        .foreach(requester ! AgentOnPhone(_, phoneNumber))

    case RequestConfig(requester, GetQueuesForAgent(agentId)) =>
      requester ! QueuesForAgent(
        configRepository.getQueuesForAgent(agentId),
        agentId
      )

    case ConfigChangeRequest(
          requester,
          SetAgentQueue(agentId, queueId, penalty)
        ) =>
      agentQueueMemberFactory
        .setAgentQueue(agentId, queueId, penalty)
        .foreach(processAgentQueueMemberUpdate(_))

    case ConfigChangeRequest(
          requester,
          RemoveAgentFromQueue(agentId, queueId)
        ) =>
      agentQueueMemberFactory.removeAgentFromQueue(agentId, queueId)

    case meetmeUpdate: MeetmeUpdate =>
      configRepository.onMeetmeUpdate(meetmeUpdate.getMeetmeList.asScala.toList)
      eventBus.publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeMeetme),
          MeetmeList(meetmeUpdate.getMeetmeList.asScala.toList)
        )
      )

    case agStat: AgentStatistic => configRepository.updateAgentStatistic(agStat)

    case EnterQueue(queue, uniqueId, queueCall, _) =>
      configRepository.onQueueCallReceived(queue, uniqueId, queueCall)
      configRepository.getQueueCalls(queue).foreach(eventBus.publish)
      publishWaitingCallsStatistics(queue)

    case LeaveQueue(queue, uniqueId, _, _) =>
      configRepository.onQueueCallFinished(queue, uniqueId)
      configRepository.getQueueCalls(queue).foreach(eventBus.publish)
      publishWaitingCallsStatistics(queue)

    case AttendedTransferFinished(fromUniqueId, toUniqueId) =>
      configRepository
        .onQueueCallTransferred(fromUniqueId, toUniqueId)
        .foreach(publishWaitingCallsStatistics)

    case AmiFailure(_, mdsName) =>
      log.warning(s"Received AmiFailure($mdsName), clearing queuecalls")
      configRepository.removeQueueCallsFromMds(mdsName)
      configRepository
        .getQueues()
        .foreach(q =>
          configRepository.getQueueCalls(q.name).foreach(eventBus.publish)
        )

    case agentConfig: AgentConfigUpdate =>
      configRepository.addAgent(
        configRepository.convertAgentConfigUpdateToAgent(agentConfig)
      )
      jmxAgentCount.set(configRepository.agents.size)

      val agentQueueMembersUpdated: List[AgentQueueMember] =
        configRepository.convertAgentConfigUpdateToQueueMember(agentConfig)

      self ! RefreshAgent(agentConfig.id)
      self ! RefreshAgentQueueMember(agentConfig.id, agentQueueMembersUpdated)

    case RefreshAgentQueueMember(
          agentId: Agent.Id,
          updatedAgentQueueMembers: List[AgentQueueMember]
        ) =>
      updatedAgentQueueMembers.foreach(agentQueueMember =>
        if (!configRepository.queueMemberExists(agentQueueMember)) {
          if (agentQueueMember.penalty >= 0)
            configRepository.updateOrAddQueueMembers(agentQueueMember)
          eventBus.publish(
            XucEvent(
              XucEventBus.configTopic(ObjectType.TypeQueueMember),
              agentQueueMember
            )
          )
        }
      )

      val agentQueueMembersToRemove = {
        updatedAgentQueueMembers match {
          case Nil => configRepository.filterOutQueueMember(agentId)
          case _ =>
            configRepository.getAgentQueueMembersToRemove(
              updatedAgentQueueMembers
            )
        }
      }

      agentQueueMembersToRemove.foreach { agentToRemoveFromQueue =>
        configRepository.removeQueueMember(agentToRemoveFromQueue)
        processAgentQueueMemberUpdate(agentToRemoveFromQueue.copy(penalty = -1))
      }

    case RefreshAgent(agentId) =>
      configRepository.loadAgent(agentId)
      configRepository.getAgent(agentId).foreach(eventBus.publish)

    case RefreshLine(endpoint: Endpoint) =>
      log.debug(s"Refresh line from endpoint($endpoint)")
      configRepository
        .getLineByEndpoint(endpoint)
        .map { line =>
          log.debug(s"Refresh line id ${line.id}")
          configRepository
            .getLineUser(line.id)
            .map(user => configRepository.loadUserLine(user.id, line.id))
        }

    case RequestConfig(requester, GetQueueStatistics) =>
      sendWaitingCallsStatisticsOnRequest(requester)

    case RequestConfig(requester, DisplayNameLookup(username)) =>
      configRepository
        .getCtiUserDisplayName(username)
        .map(displayName => requester ! UserDisplayName(username, displayName))
        .getOrElse(requester ! UserDisplayName(username, username))

    case queueConfig: QueueConfigUpdate =>
      configRepository.updateQueueConfig(queueConfig)
      configRepository.getQueue(queueConfig.id).foreach(eventBus.publish)

    case iceServer: IceServer =>
      configRepository.updateIceConfig(iceServer)

    case ctiStatus: CtiStatusMapWrapper =>
      configRepository.updateCtiStatuses(ctiStatus.map)

    // Deprecated It will be removed
    case ctiStatusLegacy: CtiStatusLegacyMapWrapper =>
      configRepository.updateCtiStatusesLegacy(ctiStatusLegacy.map)

    case mobileCfgStatus: MobileAppConfig =>
      configRepository.updateMobileConfigStatus(mobileCfgStatus.valid)

    case queueEntry: QueueEntryEvent =>
      configRepository
        .updateQueueCallCallerId(
          queueEntry.getQueue,
          queueEntry.getChannel,
          queueEntry.getUniqueId,
          queueEntry.getCallerIdNum,
          queueEntry.getCallerIdName
        )
        .map(_ =>
          configRepository
            .getQueueCalls(queueEntry.getQueue)
            .foreach(eventBus.publish)
        )

    case RemoveAgentQueueMember(agentId) =>
      configRepository
        .filterOutQueueMember(agentId)
        .foreach { aqm =>
          configRepository.removeQueueMember(aqm)
          processAgentQueueMemberUpdate(aqm.copy(penalty = -1))
        }

    case e: PhoneEvent =>
      eventBus.publish(addUsernameToEvent(e))

    case e: CurrentCallsPhoneEvents =>
      eventBus.publish(addUsernameToEvent(e))

    case e: WsConferenceParticipantEvent =>
      eventBus.publish(addUsernameToEvent(e))

    case e: WsConferenceEvent =>
      eventBus.publish(addUsernameToEvent(e))

    case e: UserVideoEvent =>
      configRepository.updateVideoStatus(e)
      eventBus.publish(
        VideoStatusEvent(
          e.fromUser,
          UserVideoEvent.matchStatusToEvent(e.status)
        )
      )

    case up: UserPreferenceChange =>
      userPreferenceService ! up

    case e: MobilePushTokenChange =>
      e match {
        case d: MobilePushTokenDeleted =>
          propagateMobilePushTokenChange(d.userId)
        case a: MobilePushTokenAdded => propagateMobilePushTokenChange(a.userId)
      }

    case WebserviceUserActionCreated(login) =>
      log.info(s"Webservice user $login created")
      updateWebServiceUser(login)

    case WebserviceUserActionEdited(login) =>
      log.info(s"Webservice user $login updated")
      updateWebServiceUser(login)

    case WebserviceUsersActionReload =>
      log.info(s"Webservice users needs to be reloaded")
      updateWebServiceUsers()

    case unknown =>
      log.debug(s"Unknown message received: $unknown from: ${sender()}")
  }

  private def requestConfigOrUpdateLine(
      requester: ActorRef,
      username: Option[String],
      line: Line
  ): Unit = {
    configRepository.getLineConfig(LineConfigQueryById(line.id)) match {
      case Some(_) =>
        self ! RequestConfig(requester, LineConfigQueryById(line.id))
      case None =>
        username
          .flatMap(u => configRepository.getCtiUser(u))
          .map(_.id)
          .map(uId => attemptLineUpdate(uId, Some(requester)))
          .getOrElse(
            self ! RequestConfig(requester, LineConfigQueryById(line.id))
          )
    }
  }

  def addUsernameToEvent(event: PhoneEvent): PhoneEvent =
    event.copy(
      username = configRepository.userNameFromPhoneNb(event.otherDN)
    )

  def addUsernameToEvent(
      event: CurrentCallsPhoneEvents
  ): CurrentCallsPhoneEvents =
    event.copy(
      phoneEvents = event.phoneEvents
        .map(addUsernameToEvent)
    )

  def addUsernameToEvent(
      event: WsConferenceParticipantEvent
  ): WsConferenceParticipantEvent =
    event.copy(
      username = configRepository.userNameFromPhoneNb(event.callerIdNum)
    )

  def addUsernameToEvent(
      event: WsConferenceParticipant
  ): WsConferenceParticipant =
    event.copy(
      username = configRepository.userNameFromPhoneNb(event.callerIdNum)
    )

  def addUsernameToEvent(event: WsConferenceEvent): WsConferenceEvent =
    event.copy(
      participants = event.participants
        .map(addUsernameToEvent)
    )

  def receive: Receive = configInitializerReceive orElse configDispatcherReceive

  private def waitingCallsStats(queue: String): List[Stat] =
    configRepository
      .findWaitingCallsStatistics(queue)
      .map { wcs =>
        val longestWaitingTime =
          (now().getMillis - wcs.oldestWaitingFrom.getMillis) / 1000
        List(
          Stat(StatName.WaitingCalls.name, wcs.numberOfWaitingCalls.toDouble),
          Stat(StatName.LongestWaitTime.name, longestWaitingTime.toDouble)
        )
      }
      .getOrElse(
        List(Stat(StatName.WaitingCalls.name, 0.0d))
      )

  private def publishWaitingCallsStatistics(queue: String): Unit =
    configRepository.getQueue(queue).foreach { queueConfigUpdate =>
      val message = StatUpdate(
        ObjectDefinition(
          StatObjectType.Queue,
          Some(queueConfigUpdate.id.toInt)
        ),
        waitingCallsStats(queue)
      )
      statAgreggator ! message
      log.debug(s"Aggregates : $message (internally created)")
    }

  private def sendWaitingCallsStatisticsOnRequest(requester: ActorRef): Unit =
    configRepository.getQueues().foreach { queueConfigUpdate =>
      val message = AggregatedStatEvent(
        ObjectDefinition(
          StatObjectType.Queue,
          Some(queueConfigUpdate.id.toInt)
        ),
        waitingCallsStats(queueConfigUpdate.name)
      )
      requester ! message
    }

  private def propagateMobilePushTokenChange(id: Long) = {
    val myself                    = self
    implicit val timeout: Timeout = Timeout(3.second)
    for {
      user     <- configRepository.getCtiUser(id)
      username <- user.username
    } yield {
      (ctiRouterFactory ? GetRouter(XucUser(username, user)))
        .mapTo[Router]
        .map(router => {
          myself ! RequestConfigForUsername(
            router.ref,
            GetConfig("line"),
            Some(username)
          )
        })
    }
  }

  private def processLineUpdate(userLine: UserLineNumber): Unit = {
    configRepository.updatePhoneLine(
      userLine.lineId,
      userLine.number,
      userLine.userId
    )
  }

  private def attemptLineUpdate(
      userId: Long,
      requester: Option[ActorRef]
  ): Unit = {
    val myself         = self
    val userLineNumber = Future(userLineNumberFactory.get(userId))
    userLineNumber.onComplete {
      case Failure(error) =>
        log.warning(
          s"Unable to get UserLineNumber for user $userId because of: $error"
        )
      case Success(Some(userLineNumber)) =>
        myself ! userLineNumber
        requester map (requester =>
          myself ! RequestConfig(
            requester,
            LineConfigQueryById(userLineNumber.lineId.toInt)
          )
        )
        link ! messageFactory.createGetPhoneStatus(
          userLineNumber.lineId.toString
        )
      case Success(None) =>
        log.warning(s"Line and extension number for user: $userId not found")
    }
  }

  private def processAgentQueueMemberUpdate(agentQM: AgentQueueMember): Unit = {
    if (!configRepository.queueMemberExists(agentQM)) {
      if (agentQM.penalty >= 0)
        configRepository.updateOrAddQueueMembers(agentQM)
      eventBus.publish(
        XucEvent(XucEventBus.configTopic(ObjectType.TypeQueueMember), agentQM)
      )
    }
  }

  private def unsubscribeFromAllPhoneHints(ref: ActorRef): Unit = {
    eventBus.unsubscribe(ref, XucEventBus.allPhoneHintEventTopic)
  }

  private def unsubscribeFromAllVideoStatus(ref: ActorRef): Unit = {
    eventBus.unsubscribe(ref, XucEventBus.allVideoStatusTopic)
  }

  private def getUserConfigUpdate(
      user: XivoUser,
      services: UserServices
  ): UserConfigUpdate = {
    val agentId: Agent.Id =
      configRepository.getAgentByUserId(user.id).map(_.id).getOrElse(0L)
    val lineIds: List[Int] =
      configRepository.getLineForUser(user.id).map(_.id).toList

    UserConfigUpdate(
      userId = user.id,
      firstName = user.firstname,
      lastName = user.lastname.getOrElse(""),
      fullName = user.fullName,
      agentId = agentId,
      dndEnabled = services.dndEnabled,
      naFwdEnabled = services.noanswer.enabled,
      naFwdDestination = services.noanswer.destination,
      uncFwdEnabled = services.unconditional.enabled,
      uncFwdDestination = services.unconditional.destination,
      busyFwdEnabled = services.busy.enabled,
      busyFwdDestination = services.busy.destination,
      mobileNumber = user.mobile_phone_number.getOrElse(""),
      lineIds = lineIds,
      voiceMailId = user.voicemailid.getOrElse(0),
      voiceMailEnabled = user.voicemailid.isDefined
    )
  }
}
