package services.config

import org.apache.pekko.actor._
import com.google.inject.Inject
import command.{AgentInitLoggedIn, AgentInitLoggedOut}
import services.AgentActorFactory
import xivo.ami.AgentCallUpdate
import xivo.events.{AgentQueues, EventAgent, EventAgentLogin}
import xivo.models.Agent

import scala.collection.mutable.{Map => MMap}

class AgentManager @Inject() (factory: AgentActorFactory)
    extends Actor
    with ActorLogging {

  log.info(s"Starting AgentManager $self ")

  val agFsms: MMap[Agent.Id, ActorRef] = MMap()

  def receive: PartialFunction[Any, Unit] = {

    case agentQueues: AgentQueues =>
      factory.get(agentQueues.agentId) foreach (_ ! agentQueues)

    case event: EventAgent => factory.get(event.agentId) foreach (_ ! event)

    case agentInitLoggedIn: AgentInitLoggedIn =>
      log.debug(s"creating agent actors for ${agentInitLoggedIn.id}")
      factory.getOrCreateAgentStatCollector(agentInitLoggedIn.id, context)
      factory.getOrCreate(
        agentInitLoggedIn.id,
        agentInitLoggedIn.number,
        context
      ) ! EventAgentLogin(
        agentInitLoggedIn.id,
        agentInitLoggedIn.phoneNumber,
        agentInitLoggedIn.number
      )

    case agentInitLoggedOut: AgentInitLoggedOut =>
      log.debug(s"creating agent actors for ${agentInitLoggedOut.id}")
      factory.getOrCreateAgentStatCollector(agentInitLoggedOut.id, context)
      factory.getOrCreate(
        agentInitLoggedOut.id,
        agentInitLoggedOut.number,
        context
      )

    case agentCallUpdate: AgentCallUpdate =>
      factory get agentCallUpdate.agentId match {
        case Some(actor) =>
          actor ! agentCallUpdate
        case None =>
          log.warning(
            s"Received channel update for unknown agent: $agentCallUpdate"
          )
      }

    case unknown => log.debug(s"Uknown message received: $unknown")
  }

}
