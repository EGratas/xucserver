package services.config

import org.apache.pekko.actor.{ActorLogging, ActorRef}
import command.{AgentInitLoggedIn, AgentInitLoggedOut}
import helpers.JmxLongMetric
import models.{WebServiceUser, XivoUser}
import org.joda.time.DateTime
import org.json.JSONObject
import org.xivo.cti.message.request.UsersAdded
import org.xivo.cti.message.{UserConfigUpdate => DeprecatedUserConfigUpdate, _}
import org.xivo.cti.{CtiMessage, MessageFactory}
import services.XucEventBus
import services.XucEventBus.XucEvent
import services.config.ConfigDispatcher.{GetUserConfig, RequestConfig}
import services.config.ObjectType
import services.config.ConfigInitializer._
import services.config.ConfigServiceManager._
import xivo.models.{Agent, QueueConfigUpdate, UserConfigUpdated}
import xivo.phonedevices.QueueSummaryCommand

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success, Try}

object ConfigInitializer {
  case object CheckRequests
  case class ConfigRequest(request: JSONObject)
  case class UpdateUsers(xivoUsers: List[XivoUser])
  case class UpdateWebServiceUser(webServiceUser: WebServiceUser)
  case class UpdateWebServiceUsers(webServiceUsers: List[WebServiceUser])
  case class UpdateAgent(agent: Agent)
  case class UpdateQueue(queue: QueueConfigUpdate)
  case object LoadAgentQueueMembers
}

trait ConfigInitializer {
  this: ConfigDispatcher with ActorLogging =>

  import services.config.ConfigInitializer.ConfigRequest
  import services.config.ConfigManager.InitConfig

  protected[config] var messageFactory = new MessageFactory

  protected[config] var requestsToSend: List[JSONObject] =
    List(
      messageFactory.createGetPhonesList(),
      messageFactory.createGetQueues(),
      messageFactory.createSubscribeToQueueStats(),
      messageFactory.createSubscribeToMeetmeUpdate()
    )
  protected[config] var link: ActorRef             = _
  protected[config] var requests: List[JSONObject] = requestsToSend

  val jmxUserCount: Try[JmxLongMetric] =
    jmxBean.addLong("Users", 0L, Some("Number of loaded users"))

  log.info(s"ConfigInitializer started")

  def configInitializerReceive: Receive = {

    case InitConfig(ctiLink) =>
      log.info(s"Configuration initialization requested using $ctiLink")
      updateUsersByWS()
      updateWebServiceUsers()
      updateQueues()
      link = ctiLink
      requests = requestsToSend
      loadAgentQueueMembers()
      loadIceConfig()
      loadCtiStatuses()
      loadQueueSummary()
      checkMobileConfig()
      processRequests()

    case CheckRequests => if (requests.nonEmpty) processRequests()

    case ConfigRequest(request) =>
      log.debug(s"will send : $request")
      link ! request
      processRequests()

    case UpdateUsers(xivoUsers) =>
      log.debug(s"Updating $xivoUsers")
      xivoUsers.foreach(configRepository.updateUser)
      xivoUsers.foreach(user =>
        configRepository.configServerRequester
          .getUserServices(user.id)
          .map(services =>
            configRepository.onUserServicesUpdated(user.id.toInt, services)
          )
      )
      xivoUsers.foreach { user =>
        val userConfig = UserConfigUpdated(user.id, user.agentId)
        applyUserConfig(userConfig)
        userLineNumberFactory
          .get(userConfig.userId)
          .map(uln =>
            configRepository.loadUserLine(userConfig.userId, uln.lineId)
          )
        self ! RequestConfig(self, GetUserConfig(user.id))
      }
      jmxUserCount.set(configRepository.ctiUsers.size)

    case UpdateWebServiceUser(webServiceUser) =>
      log.debug(s"Updating $webServiceUser")
      configRepository.updateWebServiceUser(webServiceUser)
      jmxUserCount.set(configRepository.webServiceUsers.size)

    case UpdateWebServiceUsers(webServiceUsers) =>
      log.debug(s"Updating all $webServiceUsers")
      configRepository.webServiceUsers =
        webServiceUsers.map(u => u.login -> u).toMap
      jmxUserCount.set(configRepository.webServiceUsers.size)

    case UpdateAgent(agent) =>
      log.debug(s"$agent")
      agentLoginStatusDao.getLoginStatus(agent.id) match {
        case Some(agLStatus) =>
          log.debug(s"agent $agent.id already logged in $agLStatus")
          configRepository.getAgentState(agent.id) match {
            case None =>
              agentManager ! AgentInitLoggedIn(
                agent.id,
                agent.number,
                agLStatus.phoneNumber,
                agLStatus.loginDate
              )
            case Some(_) =>
          }
        case None =>
          agentManager ! AgentInitLoggedOut(
            agent.id,
            agent.number,
            new DateTime()
          )
      }
      link ! messageFactory.createGetAgentStatus(agent.id.toString)
      configRepository
        .getAgentQueueMemberRequest(agent.number)
        .foreach(link ! _)
      eventBus.publish(
        XucEvent(XucEventBus.configTopic(ObjectType.TypeAgent), agent)
      )

    case UpdateQueue(queueConfig) =>
      configRepository.updateQueueConfig(queueConfig)

    case LoadAgentQueueMembers =>
      loadAgentQueueMembers()

    case message: CtiMessage =>
      message match {
        case usersAdded: UsersAdded =>
          usersAdded.getIds.asScala.foreach(id => updateUserByWS(id.toLong))
          val getUserStatuses = usersAdded.getIds.asScala.map { userId =>
            messageFactory.createGetUserStatus(userId.toString)
          }
          requests = messageFactory
            .createGetPhonesList() :: getUserStatuses.toList ::: requests
          processRequests()

        case message: IdsList =>
          val newRequests = message match {
            case phoneIds: PhoneIdsList => configRepository.onPhoneIds(phoneIds)
            case userIds: UserIdsList =>
              log.debug(s"Deprecated : UserIdsList requests for $userIds")
              List()
            case queueIds: QueueIds => configRepository.onQueueIds(queueIds)
            case queueMemberIds: QueueMemberIds =>
              configRepository.onQueueMemberIds(queueMemberIds)
            case _ => List()
          }
          log.debug("new requests = " + newRequests)
          requests = requests ++ newRequests

        case userConfig: DeprecatedUserConfigUpdate =>
          log.debug(s"Deprecated : $userConfig")

        case m: CtiMessage => configDispatcherReceive(m)
      }
  }

  private def processRequests(): Unit =
    requests match {
      case List() =>
        log.info("Pending requests done, checking for new requests")
        checkPendingRequests()
      case toProcess =>
        self ! ConfigRequest(toProcess.head)
        requests = toProcess.tail
    }

  private def checkPendingRequests(): Unit = {
    context.system.scheduler.scheduleOnce(2.seconds) {
      self ! CheckRequests
    }
  }

  private[config] def updateUsersByWS(): Unit =
    dbUser.getCtiUsers().onComplete {
      case Success(users) => queueUsersUpdate(users)
      case Failure(ex) =>
        log.error(ex, "unable to retrieve list of users using web services")
    }

  private[config] def updateUserByWS(userId: Long): Unit =
    dbUser.getCtiUser(userId).onComplete {
      case Success(Some(user)) => queueUsersUpdate(List(user))
      case Success(None) =>
        log.warning(s"user.id=$userId not found by web services")
      case Failure(ex) =>
        log.error(ex, s"unable to retrieve user.id=$userId using web services")
    }

  private def queueUsersUpdate(xivoUsers: List[XivoUser]): Unit = {
    log.debug(s"Queuing users update $xivoUsers")
    self ! UpdateUsers(xivoUsers)
  }

  private[config] def updateWebServiceUsers(): Unit =
    dbWebServiceUser.getWebServiceUsers().onComplete {
      case Success(users) =>
        log.debug(s"webservice users update $users")
        self ! UpdateWebServiceUsers(users)
      case Failure(ex) =>
        log.error(ex, "unable to retrieve list of webservice users in XiVO db ")
    }

  private[config] def updateWebServiceUser(login: String): Unit =
    dbWebServiceUser.getWebServiceUser(login).onComplete {
      case Success(Some(user)) =>
        log.debug(s"webservice user update $user")
        self ! UpdateWebServiceUser(user)
      case Success(None) =>
        log.warning(s"login $login not found as webservice user in XiVO db")
      case Failure(ex) =>
        log
          .error(
            ex,
            s"unable to retrieve login $login as webservice user in XiVO db"
          )
    }

  private def updateQueues(): Unit = {
    configServiceManager ! GetQueueConfigAll
  }

  private def loadAgentQueueMembers(): Unit = {
    configServiceManager ! GetAgentConfigAll
  }

  private def loadIceConfig(): Unit = {
    configServiceManager ! GetIceServer
  }

  private def loadCtiStatuses(): Unit = {
    configServiceManager ! GetCtiStatuses
  }

  private def loadQueueSummary(): Unit = {
    amiBusConnector ! QueueSummaryCommand("")
  }

  private def checkMobileConfig(): Unit = {
    configServiceManager ! GetMobileConfig
  }

  private def applyUserConfig(userConfig: UserConfigUpdated): Unit = {
    configRepository.onUserConfigUpdate(userConfig)
    userConfig.agentId.foreach(id => updateAgent(id))
  }

  private def updateAgent(agentId: Agent.Id): Unit = {
    if (agentId != 0) {
      configRepository.loadAgent(agentId)
      configRepository.getAgent(agentId) match {
        case Some(agent) => self ! UpdateAgent(agent)
        case None =>
          log.error(s"Config : Cannot find any agent in database for $agentId")
      }
    }
  }

}
