package services.config

import com.google.inject.Inject
import models.*
import org.slf4j.LoggerFactory
import play.api.Logger
import play.api.http.Status
import play.api.libs.json.*
import play.api.libs.ws.{
  writeableOf_JsValue,
  writeableOf_String,
  WSClient,
  WSRequest,
  WSResponse
}
import xivo.events.cti.models.{CtiStatus, CtiStatusLegacy}
import xivo.models.*
import xivo.network.{WebServiceException, XiVOWS}
import xivo.xuc.{ConfigServerConfig, XucBaseConfig}
import play.api.libs.ws.JsonBodyWritables.writeableOf_JsValue
import play.api.libs.ws.WSBodyWritables.writeableOf_JsValue
import play.api.libs.ws.DefaultBodyWritables.writeableOf_String
import play.api.libs.ws.WSBodyWritables.writeableOf_String

import java.util.UUID
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.*
import org.slf4j

class ConfigWS @Inject() (
    xivows: XiVOWS,
    configServerConfig: ConfigServerConfig,
    xucConfig: XucBaseConfig
) {
  val ws: WSClient  = xivows.WS
  val ApiRelease1_0 = "1.0"
  val ApiRelease2_0 = "2.0"
  val Protocol      = "http"
  val defaultHdr: List[(String, String)] = List(
    ("Accept", "application/json"),
    ("X-Auth-Token", configServerConfig.configToken)
  )

  private def getConfigHttpContextURLPart: String = {
    configServerConfig.configHttpContext.lastOption match {
      case None      => ""
      case Some('/') => configServerConfig.configHttpContext
      case _         => configServerConfig.configHttpContext + "/"
    }
  }

  def getConfigWsUrl(
      resource: String,
      apiVersion: String = ApiRelease1_0,
      maybeUsername: Option[String] = None
  ): String =
    s"$Protocol://${configServerConfig.configHost}:${configServerConfig.configPort}/${getConfigHttpContextURLPart}api/$apiVersion/$resource${maybeUsername
      .map(username => s"?username=$username")
      .getOrElse("")}"

  def request(
      resource: String,
      method: String,
      body: Option[JsValue] = None,
      apiVersion: String = ApiRelease1_0,
      username: Option[String] = None
  ): WSRequest = {
    val request = ws
      .url(getConfigWsUrl(resource, apiVersion, username))
      .withHttpHeaders(defaultHdr: _*)
      .withRequestTimeout(xucConfig.WsRequestTimeout.millis)
      .withMethod(method)

    body match {
      case Some(b) => request.withBody(b)
      case None    => request
    }
  }
}
trait ResponseWS {
  val logger: slf4j.Logger = LoggerFactory.getLogger(getClass)

  def processResponse[T](response: WSResponse)(implicit rds: Reads[T]): T =
    response.json.validate[T] match {
      case e: JsError =>
        logger.error(s"Could not read from config server : $e")
        throw new WebServiceException("Non understandable JSON returned")
      case s: JsSuccess[T] =>
        logger.debug(s"${s.get}")
        s.get
    }

  def checkError(response: WSResponse): Unit =
    if (response.status >= 400) {
      logger.error(
        s"""Web service call failed with code ${response.status} and message "${response.body}""""
      )
      throw new WebServiceException(
        s"""Web service call failed with code ${response.status} and message "${response.body}""""
      )
    }

}

class ConfigServerRequester @Inject() (configWS: ConfigWS) extends ResponseWS {
  val log: Logger = Logger(getClass.getName)

  def getCallbackLists: Future[List[CallbackList]] =
    configWS
      .request("callback_lists", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[List[CallbackList]](response)
      })

  def importCsvCallback(listUuid: String, csv: String): Future[Unit] =
    configWS
      .request(s"callback_lists/$listUuid/callback_requests/csv", "POST")
      .withBody(csv)
      .execute()
      .map(checkError)

  def takeCallback(uuid: String, agent: Agent): Future[Unit] =
    configWS
      .request(s"callback_requests/$uuid/take", "POST")
      .withBody(Json.obj("agentId" -> agent.id))
      .execute()
      .map(checkError)

  implicit lazy val listReads: Reads[List[UUID]] = (JsPath \ "ids")
    .read[List[String]]
    .map(lstr => lstr.map(el => UUID.fromString(el)))

  def getTakenCallbacks(agentId: Long): Future[List[UUID]] =
    configWS
      .request(s"callback_requests/agent/$agentId/taken", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[List[UUID]](response)
      })

  def releaseCallback(uuid: String): Future[Unit] =
    configWS
      .request(s"callback_requests/$uuid/release", "POST")
      .execute()
      .map(checkError)

  def getCallbackRequest(uuid: String): Future[CallbackRequest] =
    configWS
      .request(s"callback_requests/$uuid", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[CallbackRequest](response)
      })

  def findCallbackRequest(
      criteria: FindCallbackRequest
  ): Future[FindCallbackResponse] =
    configWS
      .request(s"callback_requests/find", "POST")
      .withBody(Json.toJson(criteria))
      .execute()
      .map(response => {
        checkError(response)
        processResponse[FindCallbackResponse](response)
      })

  def clotureRequest(uuid: UUID): Future[Unit] =
    configWS
      .request(s"callback_requests/$uuid/cloture", "POST")
      .execute()
      .map(checkError)

  def unclotureRequest(uuid: UUID): Future[Unit] =
    configWS
      .request(s"callback_requests/$uuid/uncloture", "POST")
      .execute()
      .map(checkError)

  def rescheduleCallback(
      uuid: UUID,
      rescheduleCallback: RescheduleCallback
  ): Future[Unit] =
    configWS
      .request(s"callback_requests/$uuid/reschedule", "POST")
      .withBody(Json.toJson(rescheduleCallback))
      .execute()
      .map(checkError)

  def getPreferredCallbackPeriods(): Future[List[PreferredCallbackPeriod]] =
    configWS
      .request(s"preferred_callback_periods", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[List[PreferredCallbackPeriod]](response)
      })

  def getAllDefaultMembership: Future[List[UserQueueDefaultMembership]] =
    configWS
      .request(s"user_membership/", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[List[UserQueueDefaultMembership]](response)
      })

  def setUserDefaultMembership(
      userId: Long,
      membership: List[QueueMembership]
  ): Future[Unit] =
    configWS
      .request(s"user_membership/$userId", "POST")
      .withBody(Json.toJson(membership))
      .execute()
      .map(checkError)

  def setUsersDefaultMembership(
      usersMembership: UsersQueueMembership
  ): Future[Unit] =
    configWS
      .request(s"user_membership/bulk", "POST")
      .withBody(Json.toJson(usersMembership))
      .execute()
      .map(checkError)

  def getCallQualifications(queueId: Long): Future[List[CallQualification]] = {
    configWS
      .request(s"call_qualification/queue/$queueId", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[List[CallQualification]](response)
      })
  }

  def exportQualificationsCsv(
      queueId: Long,
      from: String,
      to: String
  ): Future[String] = {
    configWS
      .request(s"call_qualification_answer/$queueId/$from/$to", "GET")
      .execute()
      .map(response => {
        checkError(response)
        response.body
      })
  }

  def createCallQualificationAnswer(
      callQualificationAnswer: CallQualificationAnswer
  ): Future[Long] = {
    val a = Json.toJson(callQualificationAnswer)
    configWS
      .request(s"call_qualification_answer", "POST")
      .withBody(a)
      .execute()
      .map(response => {
        checkError(response)
        processResponse[Long](response)
      })
  }

  def getRights(username: String): Future[WSResponse] = {
    configWS
      .request(s"rights/user/$username", "GET")
      .execute()
  }

  def forward(
      uri: String,
      method: String,
      body: Option[JsValue] = None,
      apiVersion: String = "1.0",
      username: Option[String]
  ): Future[WSResponse] = {
    log.info(
      s"""Forwarding to configmgt: $method $uri version $apiVersion for user ${username
        .getOrElse("unknown")}, data $body"""
    )
    configWS.request(s"$uri", method, body, apiVersion, username).execute()
  }

  def getQueueConfig(queueId: Long): Future[QueueConfigUpdate] = {
    configWS
      .request(s"queue_config/$queueId", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[QueueConfigUpdate](response)
      })
  }

  def getQueueConfigAll: Future[List[QueueConfigUpdate]] = {
    configWS
      .request("queue_config", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[List[QueueConfigUpdate]](response)
      })
  }

  def getAgentConfig(agentId: Long): Future[AgentConfigUpdate] = {
    configWS
      .request(s"agent_config/$agentId", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[AgentConfigUpdate](response)
      })
  }

  def getAgentConfigAll: Future[List[AgentConfigUpdate]] = {
    configWS
      .request("agent_config", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[List[AgentConfigUpdate]](response)
      })
  }

  def getMediaServerConfig(mdsId: Long): Future[MediaServerConfig] =
    configWS
      .request(s"mediaserver/$mdsId", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[MediaServerConfig](response)
      })

  def getMediaServerConfigAll: Future[List[MediaServerConfig]] =
    configWS
      .request("mediaserver", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[List[MediaServerConfig]](response)
      })

  def getUserPreference(userId: Long, key: String): Future[UserPreference] = {
    configWS
      .request(
        s"users/$userId/preferences/$key",
        "GET",
        None,
        configWS.ApiRelease2_0,
        None
      )
      .execute()
      .map(response => {
        checkError(response)
        processResponse[UserPreferencePayload](response)
      })
      .map(pref => UserPreference(userId, key, pref.value, pref.value_type))
  }

  def setUserPreference(
      userId: Long,
      key: String,
      value: String,
      valueType: String
  ): Future[Unit] =
    setUserPreference(UserPreference(userId, key, value, valueType))

  def setUserPreference(pref: UserPreference): Future[Unit] = {
    val payload = Some(
      Json.toJson(UserPreferencePayload(None, pref.value, pref.valueType))
    )
    val url = s"users/${pref.userId}/preferences/${pref.key}"
    configWS
      .request(url, "PUT", payload, configWS.ApiRelease2_0, None)
      .execute()
      .flatMap(response => {
        if (response.status == Status.NOT_FOUND) {
          configWS
            .request(url, "POST", payload, configWS.ApiRelease2_0, None)
            .execute()
        } else {
          Future.successful(response)
        }
      })
      .map(checkError)
  }

  def getUserPreferences(userId: Long): Future[List[UserPreference]] =
    configWS
      .request(
        s"users/$userId/preferences",
        "GET",
        None,
        configWS.ApiRelease2_0,
        None
      )
      .execute()
      .map(response => {
        checkError(response)
        processResponse[List[UserPreferencePayload]](response)
      })
      .map(prefList =>
        for {
          pref    <- prefList
          prefKey <- pref.key
        } yield UserPreference(userId, prefKey, pref.value, pref.value_type)
      )

  def getUserServices(userId: Long): Future[UserServices] = {
    configWS
      .request(
        s"users/$userId/services",
        "GET",
        None,
        configWS.ApiRelease2_0,
        None
      )
      .execute()
      .map(response => {
        checkError(response)
        processResponse[UserServices](response)
      })
  }

  def setUserServices(
      userId: Long,
      services: PartialUserServices
  ): Future[PartialUserServices] = {
    val payload = Some(Json.toJson(services))
    configWS
      .request(
        s"users/$userId/services",
        "PUT",
        payload,
        configWS.ApiRelease2_0,
        None
      )
      .execute()
      .map(response => {
        checkError(response)
        processResponse[PartialUserServices](response)
      })
  }

  def getIceServer: Future[IceServer] = {
    configWS
      .request(s"sip/ice_servers", "GET")
      .execute()
      .map(response => {
        checkError(response)
        processResponse[IceServer](response)
      })
  }

  def getCtiStatuses: Future[Map[String, List[CtiStatus]]] = {
    configWS
      .request(
        s"internal/cti/ctistatus",
        "GET",
        None,
        configWS.ApiRelease2_0,
        None
      )
      .execute()
      .map(response => {
        checkError(response)
        processResponse[Map[String, List[CtiStatus]]](response)
      })
  }

  def getCtiStatusesLegacy: Future[Map[String, List[CtiStatusLegacy]]] = {
    configWS
      .request(
        s"internal/cti/ctistatus/legacy",
        "GET",
        None,
        configWS.ApiRelease2_0,
        None
      )
      .execute()
      .map(response => {
        checkError(response)
        processResponse[Map[String, List[CtiStatusLegacy]]](response)
      })
  }

  def getMeetingRoomToken(
      room: String,
      userId: Option[Long],
      roomType: MeetingRoom
  ): Future[MeetingRoomToken] = {
    configWS
      .request(
        userId.fold(s"meetingrooms/token/$room")(id => {
          roomType match {
            case PersonalMeetingRoom  => s"meetingrooms/token/$room?userId=$id"
            case StaticMeetingRoom    => s"meetingrooms/token/$room"
            case TemporaryMeetingRoom => s"meetingrooms/temporary/token/$room"
          }
        }),
        "GET",
        None,
        configWS.ApiRelease2_0,
        None
      )
      .execute()
      .map(response => {
        checkError(response)
        processResponse[MeetingRoomToken](response)
      })
  }

  def setMobileAppPushToken(
      username: Option[String],
      mobileAppPushToken: MobileAppPushToken
  ): Future[Unit] = {
    configWS
      .request(
        s"mobile/push/register",
        "POST",
        Some(Json.toJson(mobileAppPushToken)),
        configWS.ApiRelease2_0,
        username
      )
      .execute()
      .map(response => {
        checkError(response)
      })
  }

  def deleteMobileAppPushToken(username: Option[String]): Future[Unit] = {
    configWS
      .request(
        s"mobile/push/register",
        "DELETE",
        None,
        configWS.ApiRelease2_0,
        username
      )
      .execute()
      .map(response => {
        checkError(response)
      })
  }

  def getMeetingRoomAlias(
      room: String
  ): Future[MeetingRoomAlias] = {
    configWS
      .request(
        s"meetingrooms/alias/$room",
        "GET",
        None,
        configWS.ApiRelease2_0,
        None
      )
      .execute()
      .map(response => {
        checkError(response)
        processResponse[MeetingRoomAlias](response)
      })
  }

  def checkValidMobileAppSetup: Future[MobileAppConfig] = {
    configWS
      .request(s"mobile/push/check", "GET", None, configWS.ApiRelease2_0, None)
      .execute()
      .flatMap(response => {
        if (response.status == Status.NO_CONTENT) {
          Future.successful(MobileAppConfig(true))
        } else {
          Future.successful(MobileAppConfig(false))
        }
      })
  }

}
