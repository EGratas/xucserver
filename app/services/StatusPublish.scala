package services

import org.apache.pekko.actor.{Actor, ActorLogging}
import com.google.inject.Inject
import services.config.UserPhoneStatus

class StatusPublish @Inject() (publisher: StatusPublisher)
    extends Actor
    with ActorLogging {

  log.info("StatusPublish started " + self)

  def receive: PartialFunction[Any, Unit] = {

    case userPhoneStatus: UserPhoneStatus =>
      log.debug("received : " + userPhoneStatus)
      publisher.publishStatus(userPhoneStatus)

    case unknown => log.debug("Uknown message received: ")
  }

}
