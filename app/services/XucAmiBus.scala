package services

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.event.{ActorEventBus, SubchannelClassification}
import org.apache.pekko.util.Subclassification
import com.google.inject.{Inject, Singleton}
import models.XivoUser
import org.asteriskjava.manager.action._
import org.asteriskjava.manager.event.ExtensionStatusEvent
import play.api.Logger
import services.calltracking.SipDriver.{PJSIP, SIP, SipDriver}
import services.channel.ChannelRequestProc.ChannelActionRequest
import xivo.phonedevices.WebRTCDeviceBearer
import xivo.xucami.models.{CallerId, Channel}

import scala.jdk.CollectionConverters._

object XucAmiBus {
  type AmiObjectReference = String
  type XucManagerEvent    = org.asteriskjava.manager.event.ManagerEvent
  type XucManagerAction   = org.asteriskjava.manager.action.ManagerAction
  type XucManagerResponse = (
      org.asteriskjava.manager.response.ManagerResponse,
      Option[AmiAction]
  )

  val OriginateTimeout = 18000L
  val DefaultContext   = "default"

  object AmiType {
    val separator                       = "/"
    val AmiEvent: String                = separator + "AmiEvent"
    val AmiAction: String               = separator + "AmiAction"
    val AmiAgentEvent: String           = separator + "AmiAgentEvent"
    val AmiRequest: String              = separator + "AmiRequest"
    val AmiResponse: String             = separator + "AmiResponse"
    val AmiService: String              = separator + "AmiService"
    val ChannelEvent: String            = separator + "ChannelEvent"
    val ChannelActionRequest: String    = separator + "ChannelActionRequest"
    val ExtensionStatusEvent: String    = separator + "ExtensionStatusEvent"
    val ListenActionRequest: String     = separator + "ListenActionRequest"
    val BeepActionRequest: String       = separator + "BeepActionRequest"
    val QueuePauseActionRequest: String = separator + "QueuePauseActionRequest"
    val QueueUnpauseActionRequest: String =
      separator + "QueueUnpauseActionRequest"
    val OutBoundDial: String        = separator + "OutBoundDial"
    val QueueEvent: String          = separator + "QueueEvent"
    val SetVarActionRequest: String = separator + "SetVarActionRequest"
    val TransferEvent: String       = separator + "TransferEvent"
  }

  trait AmiMessage {
    val message: Any
    val classifier: String
  }

  case class AmiEvent(message: XucManagerEvent, sourceMds: String)
      extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.AmiEvent
  }
  case class AmiExtensionStatusEvent(message: ExtensionStatusEvent)
      extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.ExtensionStatusEvent
  }
  case class AmiFailure(message: String, sourceMds: String) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.AmiService
  }
  case class AmiConnected(mds: String) extends AmiMessage {
    val message: String                = s"$mds is now connected"
    val classifier: AmiObjectReference = AmiType.AmiService
  }

  case class AmiAgentEvent(message: XucManagerEvent) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.AmiAgentEvent
  }
  case class AmiAction(
      message: XucManagerAction,
      reference: Option[AmiObjectReference] = None,
      requester: Option[ActorRef] = None,
      targetMds: Option[String] = None
  ) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.AmiAction
  }
  case class AmiResponse(message: XucManagerResponse) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.AmiResponse
  }
  case class ChannelEvent(message: Channel, resended: Boolean = false)
      extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.ChannelEvent
  }

  case class SpyChannels(spyerChannel: Channel, spyeeChannel: Channel)
  case class SpyStarted(message: SpyChannels) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.ChannelEvent
  }
  case class SpyStopped(message: Channel) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.ChannelEvent
  }
  case class DialAnswered(message: Channel) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.ChannelEvent
  }

  case class ChannelRequest(message: ChannelActionRequest) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.ChannelActionRequest
  }

  trait AmiActionRequest {
    def buildAction(): XucManagerAction
  }

  object GenericAutoAnswerHeader {
    val name                              = "Call-Info"
    def content(xivoHost: String): String = s"<sip:$xivoHost>;answer-after=0"

    def mkString(xivoHost: String): String = s"$name:${content(xivoHost)}"
  }
  object WebRTCAutoAnswerHeader {
    val name    = "Alert-Info"
    val content = "xivo-autoanswer"

    def mkString: String = s"$name: $content"
  }
  object YealinkAutoAnswerHeader {
    val name    = "Alert-Info"
    val content = "info = alert-autoanswer"

    def mkString: String = s"$name: $content"
  }

  val XivoDeviceHeader = "X-XIVO-DEVICE"
  def getXivoDeviceHeaderValue(device: Option[WebRTCDeviceBearer]): String = {
    device match {
      case Some(d) => d.name
      case _       => "WebApp"
    }
  }

  trait AmiVariableInheritance {
    val prefix: String
  }

  object InheritChannelOnly extends AmiVariableInheritance {
    val prefix: String = s""
  }
  object InheritChildren extends AmiVariableInheritance {
    val prefix: String = s"_"
  }
  object InheritDescendants extends AmiVariableInheritance {
    val prefix: String = s"__"
  }

  trait OriginateActionRequest extends AmiActionRequest {
    val variables: Map[String, String]
    protected def setUserVariables(
        action: OriginateAction,
        inheritance: AmiVariableInheritance
    ): Unit = {
      val prefix = inheritance.prefix
      for ((k, v) <- variables) action.setVariable(s"${prefix}USR_$k", v)
    }

    protected def copyUserVariables(action: OriginateAction): Unit =
      for (
        (k, v) <-
          variables.view.filterKeys(_.startsWith(Channel.UserDataPrefix)).toMap
      ) action.setVariable(k, v)

    def enableAutoAnswer(
        action: OriginateAction,
        xivoHost: String,
        propagation: AmiVariableInheritance = InheritChannelOnly,
        interface: String,
        device: Option[WebRTCDeviceBearer],
        driver: SipDriver
    ): Unit = {
      val prefix = propagation.prefix

      driver match {
        case PJSIP if propagation == InheritChannelOnly =>
          action.setVariable(
            s"PJSIP_HEADER(add,${GenericAutoAnswerHeader.name})",
            GenericAutoAnswerHeader.content(xivoHost)
          )
          action.setVariable(
            s"PJSIP_HEADER(add,${YealinkAutoAnswerHeader.name})",
            s"${YealinkAutoAnswerHeader.content};${WebRTCAutoAnswerHeader.content}"
          )
          action.setVariable(
            s"PJSIP_HEADER(add,$XivoDeviceHeader)",
            s"${getXivoDeviceHeaderValue(device)}"
          )
        case PJSIP
            if propagation == InheritChildren || propagation == InheritDescendants =>
          action.setVariable(
            s"${prefix}XIVO_AUTOANSWER_HDR1_NAME",
            GenericAutoAnswerHeader.name
          )
          action.setVariable(
            s"${prefix}XIVO_AUTOANSWER_HDR1_CONTENT",
            GenericAutoAnswerHeader.content(xivoHost)
          )
          action.setVariable(
            s"${prefix}XIVO_AUTOANSWER_HDR2_NAME",
            WebRTCAutoAnswerHeader.name
          )
          action.setVariable(
            s"${prefix}XIVO_AUTOANSWER_HDR2_CONTENT",
            WebRTCAutoAnswerHeader.content
          )
          action.setVariable(
            s"${prefix}XIVO_AUTOANSWER_HDR3_NAME",
            YealinkAutoAnswerHeader.name
          )
          action.setVariable(
            s"${prefix}XIVO_AUTOANSWER_HDR3_CONTENT",
            YealinkAutoAnswerHeader.content
          )
          action.setVariable(
            prefix + XivoDeviceHeader,
            getXivoDeviceHeaderValue(device)
          )
        case _ =>
          action.setVariable(
            s"${prefix}SIPADDHEADER51",
            GenericAutoAnswerHeader.mkString(xivoHost)
          )
          action.setVariable(
            s"${prefix}SIPADDHEADER52",
            WebRTCAutoAnswerHeader.mkString
          )
          action.setVariable(
            s"${prefix}SIPADDHEADER53",
            YealinkAutoAnswerHeader.mkString
          )
          action.setVariable(
            prefix + XivoDeviceHeader,
            getXivoDeviceHeaderValue(device)
          )

      }
    }
  }

  case class SetVarRequest(message: SetVarActionRequest) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.SetVarActionRequest
  }

  case class SetVarActionRequest(name: String, value: String)

  case class ListenRequest(message: ListenActionRequest) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.ListenActionRequest
  }

  case class ListenActionRequest(listener: String, listened: String) {
    def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel(listener)
      action.setAsync(true)
      action.setData(s"$listened,bdqsS")
      action.setCallerId("Listen")
      action.setApplication("ChanSpy")
      action
    }
  }

  case class BeepRequest(message: BeepActionRequest) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.BeepActionRequest
  }

  case class BeepActionRequest(listened: String) {
    def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel("Local/s@xivo-play-beep-to-agent")
      action.setVariable("XIVO_CHANNEL_TO_BEEP", listened)
      action.setData(s"beep")
      action.setApplication("Playback")
      action.setAsync(true)
      action
    }
  }

  case class AmiRequest(
      message: AmiActionRequest,
      targetMds: Option[String] = None
  ) extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.AmiRequest
  }

  case class OutBoundDialActionRequest(
      destination: String,
      skill: String,
      queueNumber: String,
      variables: Map[String, String],
      xivoHost: String,
      xivoUserId: Long,
      driver: SipDriver
  ) extends OriginateActionRequest {
    override def buildAction(): OriginateAction = {
      val action    = new OriginateAction()
      val interface = s"Local/$queueNumber@default/n"
      action.setChannel(interface)
      action.setAsync(true)
      action.setContext(DefaultContext)
      action.setExten(destination)
      action.setPriority(1)
      action.setCallerId(destination)
      action.setTimeout(OriginateTimeout)
      action.setVariable("XIVO_QUEUESKILLRULESET", skill)
      action.setVariable("_XIVO_USERID", xivoUserId.toString)
      enableAutoAnswer(
        action,
        xivoHost,
        InheritDescendants,
        interface,
        None,
        driver
      )
      action.setVariable(
        "__" + Channel.VarNames.xucCallType,
        Channel.callTypeValOutboundOriginate
      )
      setUserVariables(action, InheritChannelOnly)
      action
    }
  }

  case class DialFromQueueActionRequest(
      destination: String,
      queueNumber: String,
      callerId: String,
      variables: Map[String, String],
      xivoHost: String
  ) extends OriginateActionRequest {
    override def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel(s"Local/$queueNumber@default/n")
      action.setAsync(true)
      action.setContext(DefaultContext)
      action.setExten(destination)
      action.setPriority(1)
      action.setCallerId(callerId)
      action.setTimeout(OriginateTimeout)
      action.setVariable("XUC_CALLTYPE", Channel.callTypeValOutboundOriginate)
      setUserVariables(action, InheritChannelOnly)
      action
    }
  }

  case class DialToQueueActionRequest(
      destination: String,
      queueNumber: String,
      callerId: String,
      variables: Map[String, String],
      xivoHost: String
  ) extends OriginateActionRequest {
    override def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel(s"Local/$destination@default/n")
      action.setCallerId(callerId)
      action.setTimeout(OriginateTimeout)
      action.setAsync(true)
      action.setExten(queueNumber)
      action.setContext(DefaultContext)
      action.setPriority(1)
      action.setVariable("XUC_CALLTYPE", Channel.callTypeValOutboundOriginate)
      setUserVariables(action, InheritDescendants)
      action
    }
  }

  case class DialActionRequest(
      callerInterface: String,
      callerContext: String,
      callerId: CallerId,
      destination: String,
      variables: Map[String, String],
      xivoHost: String,
      driver: SipDriver
  ) extends OriginateActionRequest {
    override def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel(callerInterface)
      action.setAsync(true)
      action.setPriority(1)
      action.setTimeout(OriginateTimeout)
      action.setContext(callerContext)
      action.setCallerId(destination)
      val exten = if (PhoneNumberSanitizer.isSipUri(destination)) {
        val uri = destination.split("@")
        action.setVariable(Channel.VarNames.XiVOSipDomain, uri(1))
        uri(0)
      } else {
        destination
      }
      action.setExten(exten)
      enableAutoAnswer(
        action,
        xivoHost,
        InheritChannelOnly,
        callerInterface,
        None,
        driver
      )
      action.setVariable(
        Channel.VarNames.xucCallType,
        Channel.callTypeValOriginate
      )
      action.setVariable("XIVO_ORIG_CID_NUM", callerId.number)
      action.setVariable("XIVO_ORIG_CID_NAME", callerId.name)
      setUserVariables(action, InheritChannelOnly)
      action
    }
  }

  case class DialWithLocalChannelRequest(
      callerInterface: String,
      callerName: String,
      callerNum: String,
      destination: String,
      device: Option[WebRTCDeviceBearer],
      xivoHost: String,
      xivoUserId: Long,
      userContext: String,
      variables: Map[String, String] = Map.empty,
      driver: SipDriver
  ) extends OriginateActionRequest {
    override def buildAction(): OriginateAction = {
      val a = new OriginateAction()
      a.setChannel(callerInterface)
      a.setAsync(true)
      a.setTimeout(OriginateTimeout)
      a.setApplication("Dial")
      a.setData(s"Local/$destination@$userContext/n")
      a.setCallerId(destination)
      enableAutoAnswer(
        a,
        xivoHost,
        InheritChannelOnly,
        callerInterface,
        device,
        driver
      )
      a.setVariable(Channel.VarNames.xucCallType, Channel.callTypeValOriginate)
      a.setVariable("_XIVO_USERID", xivoUserId.toString)
      a.setVariable("_XIVO_ORIG_CID_NUM", callerNum)
      a.setVariable("_XIVO_ORIG_CID_NAME", callerName)
      copyUserVariables(a)
      a
    }
  }

  case class DialFromMobileActionRequest(
      mobileNumber: String,
      destination: String,
      callerNumber: String,
      callerName: String,
      variables: Map[String, String],
      xivoHost: String,
      xivoUserId: Long
  ) extends OriginateActionRequest {
    override def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel(s"Local/$mobileNumber@default/n")
      action.setAsync(true)
      action.setPriority(1)
      action.setTimeout(OriginateTimeout)
      action.setContext(DefaultContext)
      if (callerName == "") {
        action.setCallerId(callerNumber)
      } else {
        action.setCallerId("\"" + callerName + "\" <" + callerNumber + ">")
        action.setVariable("XIVO_ORIG_CID_NAME", callerName)
      }
      action.setExten(destination)
      action.setVariable(
        Channel.VarNames.xucCallType,
        Channel.callTypeValOriginate
      )
      action.setVariable(
        "_" + Channel.VarNames.xucCallSource,
        Channel.callSourceMobile
      )
      action.setVariable("XIVO_ORIG_CID_NUM", callerNumber)
      action.setVariable("XIVO_USERID", xivoUserId.toString)
      setUserVariables(action, InheritChannelOnly)
      action

    }
  }

  case class InviteInConferenceActionRequest(
      guest: String,
      confNo: String,
      pin: Option[String],
      isAdmin: Boolean,
      earlyJoin: Boolean,
      context: String,
      variables: Map[String, String],
      user: Option[XivoUser]
  ) extends OriginateActionRequest {
    override def buildAction(): OriginateAction = {
      val a             = new OriginateAction()
      val pinOption     = pin.map(p => "," + p).getOrElse("")
      val meetmeOptions = if (isAdmin) "dTaM(default)" else "dTM(default)"
      val role          = if (isAdmin) "ADMIN" else "USER"
      val guestChannel = if (PhoneNumberSanitizer.isSipUri(guest)) {
        val uri = guest.split("@")
        a.setVariable(Channel.VarNames.XiVOSipDomain, uri(1))
        s"Local/${uri(0)}@$context"
      } else {
        s"Local/$guest@$context"
      }

      if (earlyJoin) {
        a.setChannel(s"Local/conf-$confNo@xivo-join-conf-${role.toLowerCase}")
        a.setApplication("Dial")
        a.setData(guestChannel)
        a.setVariable("XIVO_ORIG_CID_NUM", guest)
        a.setVariable("XIVO_ORIG_CID_NAME", user.map(_.fullName).getOrElse(" "))
      } else {
        a.setChannel(guestChannel)
        a.setApplication("MeetMe")
        a.setData(s"$confNo,$meetmeOptions$pinOption")
      }

      a.setCallerId(s""""Conference" <$confNo>""")
      a.setAsync(true)
      a.setTimeout(OriginateTimeout)

      a.setVariable(Channel.VarNames.xucCallType, Channel.callTypeValOriginate)
      a.setVariable(Channel.VarNames.ConferenceNumberVariable, confNo)
      a.setVariable(Channel.VarNames.ConferenceRoleVariable, role)
      a.setVariable(Channel.VarNames.ConferenceInvitation, "true")
      pin.foreach(p => a.setVariable(Channel.VarNames.ConferencePinVariable, p))
      setUserVariables(a, InheritChildren)
      a
    }
  }

  case class ListenCallbackMessageActionRequest(
      sourceChannel: String,
      voiceMsgRef: String,
      variables: Map[String, String],
      xivoHost: String,
      driver: SipDriver
  ) extends OriginateActionRequest {
    override def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel(sourceChannel)
      action.setAsync(true)
      action.setPriority(1)
      action.setTimeout(OriginateTimeout)
      action.setContext("cback_listen-message")
      action.setCallerId("Message")
      action.setExten("s")
      enableAutoAnswer(
        action,
        xivoHost,
        InheritChannelOnly,
        sourceChannel,
        None,
        driver
      )
      action.setVariable(
        Channel.VarNames.xucCallType,
        Channel.callTypeValOriginate
      )
      action.setVariable("CBACK_VOICEMSG_ID", voiceMsgRef)
      setUserVariables(action, InheritChannelOnly)
      action
    }
  }

  case class RetrieveQueueCallActionRequest(
      sourceChannel: String,
      sourceNumber: Option[String],
      sourceName: String,
      callerChannel: String,
      callerNumber: String,
      callerName: Option[String],
      variables: Map[String, String],
      xivoHost: String,
      callId: String,
      queueName: Option[String],
      agentNum: Option[String],
      autoAnswer: Boolean,
      driver: SipDriver
  ) extends OriginateActionRequest {
    override def buildAction(): OriginateAction = {
      val a = new OriginateAction()
      a.setChannel(sourceChannel)
      a.setAsync(true)
      a.setTimeout(OriginateTimeout)
      a.setExten("s")
      a.setContext("xivocc_switchboard_retrieve")
      a.setPriority(1)
      if (callerName.isEmpty) {
        a.setCallerId(callerNumber)
      } else {
        a.setCallerId("\"" + callerName.get + "\" <" + callerNumber + ">")
      }
      a.setVariable("XIVO_CID_NUM", callerNumber)
      a.setVariable("XIVO_CID_NAME", callerName.getOrElse(""))
      a.setVariable("XIVO_CHANNEL", callerChannel)
      if (autoAnswer) {
        enableAutoAnswer(
          a,
          xivoHost,
          InheritChannelOnly,
          sourceChannel,
          None,
          driver
        )
      }
      a.setVariable(Channel.VarNames.xucCallType, Channel.callTypeValOriginate)
      a.setVariable("XIVO_ORIG_CID_NUM", sourceNumber.getOrElse(""))
      a.setVariable("XIVO_ORIG_CID_NAME", sourceName)
      a.setVariable("XIVO_UNIQUE_ID", callId)
      a.setVariable("XIVO_RETRIEVE_QUEUE_NAME", queueName.getOrElse(""))
      a.setVariable("XIVO_AGENT_NUM", agentNum.getOrElse(""))
      a.setVariable(Channel.VarNames.XiVOSwitchBoardRetrieve, "")
      setUserVariables(a, InheritChannelOnly)
      a
    }
  }

  case class SipNotifyRequest(
      channel: String,
      content: Map[String, String],
      driver: SipDriver
  ) extends AmiActionRequest {
    override def buildAction(): XucManagerAction = {
      driver match {
        case SIP =>
          val action = new SipNotifyAction(channel)
          action.setVariables(content.asJava)
          action
        case PJSIP =>
          val action = new PJSIPNotifyAction
          action.setEndpoint(channel)
          action.setVariables(content.asJava)
          action
      }
    }
  }

  case class SetDataRequest()

  case class SetChannelVarRequest(
      channel: String,
      variable: String,
      value: String
  ) extends AmiActionRequest {
    override def buildAction(): SetVarAction =
      new SetVarAction(channel, variable, value)
  }

  case class RedirectRequest(
      channel: String,
      context: String,
      exten: String,
      priority: Int
  ) extends AmiActionRequest {
    override def buildAction(): RedirectAction =
      new RedirectAction(channel, context, exten, priority)
  }

  case class MultipleRedirectRequest(
      channel: String,
      context: String,
      exten: String,
      priority: Int,
      extraChannel: String,
      extraContext: String,
      extraExten: String,
      extraPriority: Int
  ) extends AmiActionRequest {
    override def buildAction(): RedirectAction =
      new RedirectAction(
        channel,
        extraChannel,
        context,
        exten,
        priority,
        extraContext,
        extraExten,
        extraPriority
      )
  }

  case class BridgeActionRequest(channel1: String, channel2: String)
      extends AmiActionRequest {
    override def buildAction(): BridgeAction =
      new BridgeAction(channel1, channel2)
  }

  case class HangupActionRequest(channel: String) extends AmiActionRequest {
    override def buildAction(): HangupAction =
      new HangupAction(channel, 16)
  }

  case class LocalOptimizeAwayRequest(channel: String)
      extends AmiActionRequest {
    override def buildAction(): LocalOptimizeAwayAction =
      new LocalOptimizeAwayAction(channel)
  }

  case class MeetMeMuteRequest(numConf: String, userIndex: Int)
      extends AmiActionRequest {
    override def buildAction(): MeetMeMuteAction =
      new MeetMeMuteAction(numConf, userIndex)
  }

  case class MeetMeUnmuteRequest(numConf: String, userIndex: Int)
      extends AmiActionRequest {
    override def buildAction(): MeetMeUnmuteAction =
      new MeetMeUnmuteAction(numConf, userIndex)
  }

  case class MeetMeKickRequest(numConf: String, userIndex: Int)
      extends AmiActionRequest {
    override def buildAction(): CommandAction =
      new CommandAction(s"meetme kick $numConf $userIndex")
  }

  case class MeetMeDeafenRequest(channel: String, userIndex: Int)
      extends AmiActionRequest {
    override def buildAction(): MuteAudioAction = {
      new MuteAudioAction(
        channel,
        MuteAudioAction.Direction.OUT,
        MuteAudioAction.State.MUTE
      )
    }
  }

  case class MeetMeUndeafenRequest(channel: String, userIndex: Int)
      extends AmiActionRequest {
    override def buildAction(): MuteAudioAction = {
      new MuteAudioAction(
        channel,
        MuteAudioAction.Direction.OUT,
        MuteAudioAction.State.UNMUTE
      )
    }
  }

  case class QueueLogEventRequest(
      queue: String,
      event: String,
      uniqueId: String,
      message: String,
      interface: Option[String] = None
  ) extends AmiActionRequest {
    override def buildAction(): QueueLogAction = {
      val a = new QueueLogAction(queue, event)
      a.setUniqueId(uniqueId)
      a.setMessage(message)
      a.setInterface(interface.getOrElse("NONE"))
      a
    }
  }

  case class QueuePauseRequest(message: QueuePauseActionRequest)
      extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.QueuePauseActionRequest
  }

  case class QueuePauseActionRequest(
      iface: String,
      reason: Option[String] = None
  ) extends AmiActionRequest {
    override def buildAction(): QueuePauseAction = {
      val a = new QueuePauseAction(iface)
      reason.foreach(a.setReason(_))
      a
    }
  }

  case class QueueUnpauseRequest(message: QueueUnpauseActionRequest)
      extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.QueueUnpauseActionRequest
  }

  case class QueueUnpauseActionRequest(iface: String) extends AmiActionRequest {
    override def buildAction(): QueuePauseAction = {
      val a = new QueuePauseAction(iface, false)
      a
    }
  }

  case class QueueStatusRequest(member: String) extends AmiActionRequest {
    override def buildAction(): QueueStatusAction = {
      val r = new QueueStatusAction()
      r.setMember(member)
      r
    }
  }

  case class QueueSummaryMessage(message: QueueSummaryRequest)
      extends AmiMessage {
    val classifier: AmiObjectReference = AmiType.AmiAction
  }

  case class QueueSummaryRequest(queue: String) extends AmiActionRequest {
    override def buildAction(): QueueSummaryAction = {
      new QueueSummaryAction(queue)
    }
  }
}

@Singleton
class XucAmiBus @Inject() (implicit val system: ActorSystem)
    extends ActorEventBus
    with SubchannelClassification
    with EventBusSupervision {
  type Event      = XucAmiBus.AmiMessage
  type Classifier = String

  override def publish(event: Event): Unit = super.publish(event)
  override def unsubscribe(subscriber: Subscriber, from: Classifier): Boolean =
    super.unsubscribe(subscriber, from)
  override def unsubscribe(subscriber: Subscriber): Unit =
    super.unsubscribe(subscriber)

  override protected def subclassification: Subclassification[Classifier] =
    new Subclassification[Classifier] {
      def isEqual(x: Classifier, y: Classifier): Boolean = x.equals(y)

      def isSubclass(x: Classifier, y: Classifier): Boolean = {
        if (y.length == 0) {
          false
        } else {
          x.startsWith(y)
        }
      }
    }

  override def classify(event: Event): String = event.classifier

  override protected def publish(event: Event, subscriber: Subscriber): Unit =
    subscriber ! event

  override def subscribe(subscriber: Subscriber, to: Classifier): Boolean = {
    val logger = Logger(getClass.getName)
    logger.debug(s"subscribe : $subscriber to : $to")
    super.subscribe(subscriber, to)
  }
}
