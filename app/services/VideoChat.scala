package services

import org.apache.pekko.actor.ActorRef
import models.XivoUser
import services.VideoChat.{VideoChatStates, VideoChatStatus, VideoChatUser}
import services.chat.ChatService.Username
import services.video.model.VideoInvitationEvent

trait VideoChat[E <: VideoChatStates[
  E,
  S,
  U
], S <: VideoChatStatus, U <: VideoChatUser] {

  def sendToRouter(
      username: String,
      msg: VideoInvitationEvent,
      status: S
  ): Map[Username, VideoChatStates[E, S, U]] => Boolean = {
    (users: Map[Username, VideoChatStates[E, S, U]]) =>
      users
        .get(username)
        .filter(_.status == status)
        .map(_.ctiRouterRef.foreach(_ ! msg))
        .isDefined
  }

  def connectXivoUser(xivoUser: XivoUser, state: String => E): Map[
    Username,
    VideoChatStates[E, S, U]
  ] => Map[Username, VideoChatStates[E, S, U]] = {
    (users: Map[Username, VideoChatStates[E, S, U]]) =>
      xivoUser.username
        .map { username =>
          users + (username -> state(username))
        }
        .getOrElse(users)
  }

  def disconnectXivoUser(xivoUser: XivoUser, status: S): Map[
    Username,
    VideoChatStates[E, S, U]
  ] => Map[Username, VideoChatStates[E, S, U]] = {
    (users: Map[Username, VideoChatStates[E, S, U]]) =>
      xivoUser.username
        .flatMap(users.get)
        .map { userState => userState.withStatus(status) }
        .map(_.disconnect)
        .map(newState => users.updated(newState.user.username, newState))
        .getOrElse(users)
  }
}

object VideoChat {
  type Username = String

  trait VideoChatStates[E, S, U] {
    val status: S
    val user: U
    val ctiRouterRef: Option[ActorRef]
    val xivoUser: XivoUser
    def withStatus(status: S): E
    def disconnect: E
  }

  sealed trait VideoChatStatus

  sealed trait VideoStatus extends VideoChatStatus
  object VideoStatus {
    case object Available   extends VideoStatus
    case object Unavailable extends VideoStatus
  }

  sealed trait ChatStatus extends VideoChatStatus
  object ChatStatus {
    case object Available   extends ChatStatus
    case object Unavailable extends ChatStatus
  }

  trait VideoChatUser {
    val username: String
  }
}
