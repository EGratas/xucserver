package services.calltracking

import com.google.inject.Inject
import com.google.inject.name.Named
import org.apache.pekko.actor._
import org.asteriskjava.manager.action.CoreShowChannelsAction
import org.asteriskjava.manager.event.{
  CoreShowChannelEvent,
  CoreShowChannelsCompleteEvent,
  DialBeginEvent,
  ManagerEvent
}
import services.{ActorIds, XucAmiBus}
import services.XucAmiBus._
import services.ServiceStatus._
import graph._
import helpers.FastLogging
import helpers.JmxLongMetric
import scala.util.Try

object AsteriskGraphTracker {
  final val serviceName = "AsteriskGraphTracker"
  type AsteriskPath = BaseGraph.Path[AsteriskObject]
  def AsteriskPath(xs: AsteriskObject*): List[AsteriskObject] =
    BaseGraph.Path(xs: _*)
  def pathsEndpoints(paths: Set[AsteriskPath]): Set[NodeChannelLike] =
    paths.map(_.collect({ case c: NodeChannelLike => c })).flatMap(_.lastOption)
  def pathsEndChannels(paths: Set[AsteriskPath]): Set[NodeChannel] =
    pathsEndpoints(paths).collect({ case c: NodeChannel => c })
  def pathsFirstChannels(paths: Set[AsteriskPath]): Set[NodeChannelLike] =
    paths.map(_.collect({ case c: NodeChannelLike => c })).flatMap(_.headOption)

  def localChannelExtension(channelName: String): Option[String] =
    if (channelName.startsWith("Local/") && channelName.contains('@'))
      Some(channelName.substring(6, channelName.indexOf('@')))
    else None

  case class GetPathsFromChannel(c: NodeChannelLike)
  sealed trait AsteriskGraphTrackerMessage
  case class PathsFromChannel(c: NodeChannelLike, paths: Set[AsteriskPath])
      extends AsteriskGraphTrackerMessage
  case class LoopDetected(nodes: Set[NodeLocalChannel])
      extends AsteriskGraphTrackerMessage
}

import AsteriskGraphTracker._

class AsteriskGraphTracker @Inject() (
    xucAmiBus: XucAmiBus,
    @Named(ActorIds.ChannelTrackerId) channelTracker: ActorRef
) extends Actor
    with FastLogging
    with BaseTracker[AsteriskGraphTrackerMessage] {

  val jmxLoopDetectedCount: Try[JmxLongMetric] =
    jmxBean.addLong("LoopDetected", 0L, Some("Number of Loop detected"))
  val jmxGraphSize: Try[JmxLongMetric] =
    jmxBean.addLong("GraphSize", 0L, Some("Number of links in graph"))
  var graph: AsteriskGraph           = AsteriskGraph()
  var readyRequester: List[ActorRef] = List.empty[ActorRef]

  def detectLoop(evt: ManagerEvent): Boolean = {
    evt match {
      case dial: DialBeginEvent =>
        val channel     = Option(dial.getChannel)
        val destination = Option(dial.getDestination)
        val check = for {
          c1   <- channel
          c2   <- destination
          ext1 <- localChannelExtension(c1)
          ext2 <- localChannelExtension(c2)
          if ext1 == ext2
        } yield true

        check.getOrElse(false)
      case _ => false
    }
  }

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
    xucAmiBus.subscribe(self, AmiType.AmiEvent)
    xucAmiBus.subscribe(self, AmiType.AmiService)
    xucAmiBus.publish(AmiAction(new CoreShowChannelsAction(), None, Some(self)))
    context.become(handleWatchers orElse receive)
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
    xucAmiBus.unsubscribe(self)
  }

  override def receive: PartialFunction[Any, Unit] = {
    case GetServiceStatus =>
      sender() ! ServiceStarting(AsteriskGraphTracker.serviceName, self)

    case NotifyWhenServiceReady =>
      readyRequester = sender() :: readyRequester

    case evt: AmiEvent =>
      evt.message match {
        case c: CoreShowChannelEvent =>
          log.debug("CoreShowChannelEvent {}", c)
          graph = AmiEventToAsteriskEdge(graph, evt.message, evt.sourceMds)._1
        case c: CoreShowChannelsCompleteEvent =>
          log.debug("INITIAL GRAPH:" + graph.prettyPrint())
          readyRequester.foreach(
            _ ! ServiceReady(AsteriskGraphTracker.serviceName, self)
          )
          readyRequester = List.empty
          context.become(handleWatchers orElse ready)
        case _ =>
          log.debug("Ignoring initial event {}", evt)
      }
  }

  def ready: Receive = {
    case GetServiceStatus | NotifyWhenServiceReady =>
      sender() ! ServiceReady(AsteriskGraphTracker.serviceName, self)

    case GetPathsFromChannel(c) =>
      sender() ! PathsFromChannel(c, graph.pathsFrom(c))

    case AmiFailure(message, mdsName) =>
      log.warning(s"Received AmiFailure($message), clearing graph")
      graph.links.keys
        .collect { case c @ NodeChannel(_, `mdsName`) => c }
        .foreach(c => {
          notify(c.name, PathsFromChannel(c, Set.empty))
        })

      val newLinks = graph.links.view.filterKeys(_.sourceMds != mdsName).toMap
      graph = new AsteriskGraph(newLinks)

    case evt: AmiEvent =>
      val (g, channels) =
        AmiEventToAsteriskEdge(graph, evt.message, evt.sourceMds)
      if (detectLoop(evt.message)) {
        log.warning(s"Found loop impacting $channels")
        jmxLoopDetectedCount.inc()
        val localChannelsImpacted = channels.collect {
          case c: NodeLocalChannel => c
        }
        localChannelsImpacted.foreach(n => {
          graph = graph.del(n)
        })
        val loopMsg = LoopDetected(localChannelsImpacted)
        channelTracker ! loopMsg
        notify("Local/", loopMsg)
      } else {
        graph = g
      }
      jmxGraphSize.set(graph.links.size)
      channels.foreach(c =>
        notify(c.name, PathsFromChannel(c, graph.pathsFrom(c)))
      )
      if (channels.nonEmpty) {
        log.debug(
          "Graph dump will follow\n----------------\n{}\n----------------",
          graph.prettyPrint()
        )
      }
  }

}
