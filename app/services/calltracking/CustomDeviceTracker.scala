package services.calltracking

import org.apache.pekko.actor.*
import services.calltracking.DeviceMessage.DeviceInterface
import services.calltracking.graph.{NodeChannel, NodeLocalChannel}
import services.{MediatorWrapper, XucAmiBus, XucEventBus}
import xivo.models.Line
import xivo.xuc.DeviceTrackerConfig
import xivo.xucami.models.{Channel, ChannelState}

class CustomDeviceTracker(
    line: Line,
    eventBus: XucEventBus,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig,
    configDispatcher: ActorRef,
    mediatorWrapper: MediatorWrapper
) extends SipDeviceTracker(
      line,
      eventBus,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig,
      configDispatcher,
      mediatorWrapper
    ) {
  import AsteriskGraphTracker._
  import BaseTracker._

  override val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.CustomDeviceTrackerType
  var watchedChannels: List[NodeChannel] = List.empty

  override val channelStartsWith: DeviceInterface = line.interface

  override def customBehavior: Receive =
    customDeviceBehavior orElse super.customBehavior

  def extractRealChannel(paths: Set[AsteriskPath]): Option[NodeChannel] =
    pathsEndChannels(
      paths
        .filter(_.collect { case n: NodeLocalChannel => n }.isEmpty)
    ).headOption

  def customDeviceBehavior: Receive = {
    case PathsFromChannel(channel @ NodeLocalChannel(name, _), paths) =>
      log.debug(s"Got path for $name: $paths")

      if (name.endsWith(";2")) {
        extractRealChannel(paths)
          .filterNot(watchedChannels.contains(_))
          .foreach(realChannel => {
            log.warning(s"Subscribing to $realChannel")
            watchedChannels = realChannel :: watchedChannels
            channelTracker ! WatchChannelStartingWith(realChannel.name)
            graphTracker ! WatchChannelStartingWith(realChannel.name)
            context.parent ! DevicesTracker
              .RegisterActor(realChannel.name, self)
          })
      }

    case c: Channel if c.isLocal =>
      log.debug(s"Ignoring local channel ${c.name} in CustomDeviceTracker")

    case c: Channel if !c.isLocal && c.state == ChannelState.HUNGUP =>
      watchedChannels = watchedChannels.filterNot(_.name == c.name)
      channelTracker ! UnWatchChannelStartingWith(c.name)
      graphTracker ! UnWatchChannelStartingWith(c.name)
      context.parent ! DevicesTracker.UnRegisterActor(c.name, self)

      applyDefaultBehavior(c)
  }

  def applyDefaultBehavior(m: Any): Unit = {
    if (super.customBehavior.isDefinedAt(m)) {
      super.customBehavior.apply(m)
    }
  }

  override def findCallByChannel(channel: String): Option[DeviceCall] =
    tracker.calls.values
      .filter(_.remoteChannels.contains(channel))
      .headOption
      .orElse(super.findCallByChannel(channel))
}
