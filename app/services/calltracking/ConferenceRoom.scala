package services.calltracking

import org.asteriskjava.manager.event.AbstractMeetMeEvent
import org.joda.time.DateTime
import xivo.models.MeetMeConference
import xivo.xucami.models.CallerId

sealed trait ConferenceStatus
case object ConferenceAvailable extends ConferenceStatus
case object ConferenceBusy      extends ConferenceStatus

sealed trait ConferenceParticipantRole
case object OrganizerRole extends ConferenceParticipantRole
case object UserRole      extends ConferenceParticipantRole

object ConferenceParticipantRole {
  def fromString(s: String): Option[ConferenceParticipantRole] =
    s match {
      case "ADMIN" => Some(OrganizerRole)
      case "USER"  => Some(UserRole)
      case _       => None
    }
}

final case class ConferenceParticipant(
    numConf: String,
    index: Int,
    channelName: String,
    callerId: CallerId,
    joinTime: DateTime,
    isTalking: Boolean = false,
    role: ConferenceParticipantRole = UserRole,
    isMuted: Boolean = false,
    isDeaf: Boolean = false
) {
  def withChannelName(channelName: String): ConferenceParticipant =
    this.copy(channelName = channelName)
}

object ConferenceParticipant {
  def apply(evt: AbstractMeetMeEvent): ConferenceParticipant = {
    ConferenceParticipant(
      evt.getMeetMe,
      evt.getUser,
      evt.getChannel,
      CallerId(evt.getCallerIdName, evt.getCallerIdNum),
      DateTime.now
    )
  }
}

final case class ConferenceRoom(
    number: String,
    name: String,
    status: ConferenceStatus,
    startTime: Option[DateTime],
    participants: List[ConferenceParticipant],
    mds: String,
    userPin: Option[String] = None,
    adminPin: Option[String] = None
) {

  def addParticipant(participant: ConferenceParticipant): ConferenceRoom = {
    val ps = participants :+ participant
    status match {
      case ConferenceAvailable =>
        this.copy(
          participants = ps,
          status = ConferenceBusy,
          startTime = Some(participant.joinTime)
        )
      case ConferenceBusy => this.copy(participants = ps)
    }
  }

  def removeParticipant(participant: ConferenceParticipant): ConferenceRoom = {
    val ps = participants.filter(_.index != participant.index)
    if (ps.isEmpty) {
      this.copy(
        participants = List.empty,
        status = ConferenceAvailable,
        startTime = None
      )
    } else {
      this.copy(participants = ps)
    }
  }

  def getParticipant(index: Int): Option[ConferenceParticipant] =
    participants.find(_.index == index)

  def updateParticipant(
      index: Int
  )(fn: ConferenceParticipant => ConferenceParticipant): ConferenceRoom =
    this.copy(
      participants = participants.map(p =>
        if (p.index == index)
          fn(p)
        else
          p
      )
    )
}

object ConferenceRoom {

  def apply(meetme: MeetMeConference): ConferenceRoom =
    ConferenceRoom(
      meetme.confno,
      meetme.name,
      ConferenceAvailable,
      None,
      List.empty,
      "default",
      meetme.user_pin,
      meetme.admin_pin
    )

  def empty(confno: String, mds: String): ConferenceRoom =
    ConferenceRoom(
      confno,
      s"Conference",
      ConferenceAvailable,
      None,
      List.empty,
      mds
    )

}
