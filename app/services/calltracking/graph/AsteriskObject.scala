package services.calltracking.graph

import services.calltracking.graph.NodeBridge.BridgeCreator

sealed trait AsteriskObject {
  def sourceMds: String
  def name: String
  def prettyPrint: String = toString
  type BridgeCreator = String
}

sealed trait NodeChannelLike extends AsteriskObject
case class NodeChannel(name: String, sourceMds: String)
    extends NodeChannelLike {
  override def prettyPrint: String = s"NodeChannel($name)"
}
case class NodeLocalChannel(name: String, sourceMds: String)
    extends NodeChannelLike {
  override def prettyPrint: String = s"NodeLocalChannel($name)"
}

object NodeBridge {
  type BridgeCreator = String
}
sealed trait NodeBridgeLike extends AsteriskObject
case class NodeBridge(
    name: String,
    sourceMds: String,
    bridgeCreator: Option[BridgeCreator]
) extends NodeBridgeLike {
  override def prettyPrint: String =
    s"NodeBridge($name, ${bridgeCreator.getOrElse("")})"
}

case class NodeLocalBridge(lc1: NodeLocalChannel, lc2: NodeLocalChannel)
    extends NodeBridgeLike {
  override def sourceMds: BridgeCreator = lc1.sourceMds
  override def name: String             = s"$lc1#$lc2"
  override def prettyPrint              = "LocalBridge(...)"
}
case class NodeDial(c1: NodeChannelLike, c2: NodeChannelLike)
    extends NodeBridgeLike {
  override def sourceMds: BridgeCreator = c1.sourceMds
  override def name: String             = s"${c1.name}#${c2.name}"
  override def prettyPrint              = "Dial(...)"
}
case class NodeOptimize(c1: NodeChannelLike, c2: NodeChannelLike)
    extends NodeBridgeLike {
  override def sourceMds: BridgeCreator = c1.sourceMds
  override def name: String             = s"${c1.name}#${c2.name}"
  override def prettyPrint              = "Optimize(...)"
}
case class NodeMdsTrunkBridge(callId: String) extends NodeBridgeLike {
  override def sourceMds    = "None"
  override def name: String = s"NodeMdsTrunkBridge($callId)"
}

object NodeChannel {
  def from(name: String, sourceMds: String): Option[NodeChannelLike] =
    Option(name)
      .map(n =>
        if (n.startsWith("Local/"))
          NodeLocalChannel(name, sourceMds)
        else
          NodeChannel(name, sourceMds)
      )
}

object NodeChannelLikeOrdering extends Ordering[NodeChannelLike] {
  def compare(a: NodeChannelLike, b: NodeChannelLike): Int =
    (a, b) match {
      case (NodeChannel(_, _), NodeLocalChannel(_, _)) => -1
      case (NodeLocalChannel(_, _), NodeChannel(_, _)) => 1
      case (a, b)                                      => a.name.compareTo(b.name)
    }
}
