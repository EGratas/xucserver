package services.calltracking

import org.apache.pekko.actor._
import services.{MediatorWrapper, XucAmiBus}
import services.calltracking.DeviceMessage._
import xivo.xuc.DeviceTrackerConfig

class MediaServerTrunkDeviceTracker(
    trunkInterface: DeviceInterface,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig,
    mediatorWrapper: MediatorWrapper
) extends UnknownDeviceTracker(
      trunkInterface,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig,
      mediatorWrapper
    ) {

  override val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.MediaServerTrunkDeviceTrackerType

}
