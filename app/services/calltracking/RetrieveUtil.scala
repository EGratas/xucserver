package services.calltracking

import java.util.NoSuchElementException
import org.apache.pekko.actor.{ActorContext, ActorRef}
import org.apache.pekko.pattern.ask
import org.apache.pekko.util.Timeout
import com.google.inject.Inject
import com.google.inject.name.Named
import play.api.Logger
import services.ActorIds
import services.calltracking.ChannelTracker.{GetChannel, NoSuchChannel}
import services.calltracking.RetrieveUtil.{
  RetrieveChannel,
  RetrieveNotAllowed,
  RetrieveQueueCallContext
}
import services.calltracking.graph.NodeChannel
import xivo.models.Line
import xivo.phonedevices.DeviceAdapter
import xivo.xucami.models.ChannelState.ChannelState
import xivo.xucami.models.{Channel, ChannelState, QueueCall}

import scala.concurrent.duration.*
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

object RetrieveUtil {
  case class RetrieveChannel(id: String, queueName: Option[String])
  case class RetrieveQueueCallContext(
      line: Line,
      queueCall: QueueCall,
      variables: Map[String, String],
      agentNum: Option[String],
      agentName: String
  )
  case class RetrieveNotAllowed(msg: String) extends Exception
}

class RetrieveUtil @Inject() (
    @Named(ActorIds.ChannelTrackerId) channelTracker: ActorRef
) {

  val log: Logger = Logger(getClass.getName)

  def processRetrieve(
      calls: List[DeviceCall],
      retrieveQueueCallContext: RetrieveQueueCallContext,
      deviceAdapter: DeviceAdapter,
      theSender: ActorRef
  )(implicit context: ActorContext): Future[Any] = {
    implicit val ec: ExecutionContextExecutor = context.dispatcher

    getChannelToRetrieve(retrieveQueueCallContext.queueCall)
      .flatMap { channelToRetrieve =>
        getCurrentChannel(calls) match {
          case Some(currentChannel) =>
            executeWithCurrentChannel(
              currentChannel,
              retrieveQueueCallContext,
              channelToRetrieve,
              deviceAdapter,
              theSender
            )
          case None =>
            executeWithNoCurrentChannel(
              retrieveQueueCallContext,
              channelToRetrieve,
              deviceAdapter,
              theSender
            )
        }
      }
      .recover {
        case e: NoSuchElementException =>
          log.warn(s"Unable to retrieve. ${e.getMessage}")
        case e: ChannelStateTimeout =>
          log.warn(s"Unable to retrieve. ${e.getMessage}")
        case e: RetrieveNotAllowed => log.warn(s"Unable to retrieve. ${e.msg}")
        case e                     => log.warn(s"Unable to retrieve. $e")
      }
  }

  private def getCurrentChannel(calls: List[DeviceCall]): Option[DeviceCall] = {
    calls.headOption
  }

  private def getCurrentChannelState(call: DeviceCall): Option[ChannelState] = {
    call.channel.map(_.state)
  }

  private def getChannelToRetrieve(
      queueCall: QueueCall
  )(implicit context: ExecutionContext): Future[Channel] = {
    channelTracker
      .ask(GetChannel(queueCall.channel))(Timeout(2.seconds))
      .flatMap {
        case c: Channel => Future.successful(c)
        case NoSuchChannel(_) =>
          Future.failed(
            new NoSuchElementException(
              s"Channel ${queueCall.channel} not found in the channel tracker"
            )
          )
      }
  }

  private def waitForChannelState(
      deviceCall: DeviceCall,
      expectedState: ChannelState
  )(implicit context: ActorContext) = {
    deviceCall.channel match {
      case Some(channel) =>
        WaitForChannelState(
          channelTracker,
          NodeChannel(channel.name, channel.mdsName),
          expectedState,
          Timeout(2.second)
        )
      case None =>
        Future.failed(
          new NoSuchElementException(
            s"Unable to retrieve, device call ${deviceCall.channelName} has no channel associated."
          )
        )
    }
  }

  private def executeActionOnChannel(
      channelState: ChannelState,
      deviceAdapter: DeviceAdapter,
      theSender: ActorRef
  ): Option[ChannelState] = {
    val expectedChannelState = channelState match {
      case ChannelState.UP =>
        deviceAdapter.hold(None, theSender)
        Some(ChannelState.HOLD)
      case ChannelState.RINGING =>
        deviceAdapter.hangup(theSender)
        Some(ChannelState.HUNGUP)
      case _ => None
    }
    expectedChannelState
  }

  private def executeWithCurrentChannel(
      currentCall: DeviceCall,
      retrieveQueueCallContext: RetrieveQueueCallContext,
      channelToRetrieve: Channel,
      deviceAdapter: DeviceAdapter,
      theSender: ActorRef
  )(implicit context: ActorContext): Future[Unit] = {
    implicit val ec: ExecutionContextExecutor = context.dispatcher
    getCurrentChannelState(currentCall)
      .map { state =>
        executeActionOnChannel(state, deviceAdapter, theSender) match {
          case Some(expectedState) =>
            waitForChannelState(currentCall, expectedState)
              .map(_ =>
                executeRetrieve(
                  deviceAdapter,
                  retrieveQueueCallContext,
                  RetrieveChannel(
                    channelToRetrieve.id,
                    channelToRetrieve.lastQueueName
                  ),
                  theSender
                )
              )
          case None =>
            Future.failed(
              RetrieveNotAllowed(
                s"Retrieve not allowed while channel is in state: $state"
              )
            )
        }
      }
      .getOrElse(
        Future.successful(
          executeRetrieve(
            deviceAdapter,
            retrieveQueueCallContext,
            RetrieveChannel(
              channelToRetrieve.id,
              channelToRetrieve.lastQueueName
            ),
            theSender
          )
        )
      )
  }

  private def executeWithNoCurrentChannel(
      retrieveQueueCallContext: RetrieveQueueCallContext,
      channelToRetrieve: Channel,
      deviceAdapter: DeviceAdapter,
      theSender: ActorRef
  )(implicit context: ActorContext): Future[Unit] = {
    Future.successful(
      executeRetrieve(
        deviceAdapter,
        retrieveQueueCallContext,
        RetrieveChannel(channelToRetrieve.id, channelToRetrieve.lastQueueName),
        theSender
      )
    )
  }

  private def executeRetrieve(
      deviceAdapter: DeviceAdapter,
      retrieveQueueCall: RetrieveQueueCallContext,
      retrieveChannel: RetrieveChannel,
      theSender: ActorRef
  ): Unit = {
    deviceAdapter.retrieveQueueCall(
      sourceChannel = retrieveQueueCall.line.interface,
      sourceNumber = retrieveQueueCall.line.number,
      sourceName = retrieveQueueCall.agentName,
      queueCall = retrieveQueueCall.queueCall,
      variables = retrieveQueueCall.variables,
      sender = theSender,
      callId = retrieveChannel.id,
      queueName = retrieveChannel.queueName,
      agentNum = retrieveQueueCall.agentNum,
      autoAnswer = true,
      retrieveQueueCall.line.driver
    )
  }
}
