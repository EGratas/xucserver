package services.calltracking

import com.google.inject.{ImplementedBy, Inject}
import com.google.inject.name.Named
import helpers.{FastLogging, JmxActorSingletonMonitor, JmxLongMetric}
import org.apache.pekko.actor._
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.Subscribe
import services.{ActorIds, MediatorWrapper, XucAmiBus, XucEventBus}
import services.XucAmiBus.{AmiConnected, AmiType}

import scala.concurrent.ExecutionContext.Implicits.global
import xivo.models.{
  AnonymousChannelFeature,
  DAHDITrunk,
  Line,
  LineFactory,
  LocalChannelFeature,
  MediaServerTrunk,
  Trunk,
  XivoFeature
}
import xivo.xuc.{DeviceTrackerConfig, XucConfig}

import scala.util.Try

object DeviceMessage {
  type DeviceInterface = String
}

trait DeviceMessage {
  import DeviceMessage._

  def isFor(interface: DeviceInterface): Boolean
}

object DevicesTracker {
  final val serviceName: String = "DevicesTracker"

  case class RegisterActor(interface: String, ref: ActorRef)
  case class UnRegisterActor(interface: String, ref: ActorRef)
  case class EnsureTrackerFor(feature: XivoFeature)
  case class SwitchTrackerFor(currentLine: Line, line: Line)

}

@ImplementedBy(classOf[DeviceActorFactoryImpl])
trait DeviceActorFactory {
  def props(f: XivoFeature): Props
}

class DeviceActorFactoryImpl @Inject() (
    @Named(ActorIds.ChannelTrackerId) channelTracker: ActorRef,
    @Named(ActorIds.AsteriskGraphTrackerId) graphTracker: ActorRef,
    eventBus: XucEventBus,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
    mediatorWrapper: MediatorWrapper
) extends DeviceActorFactory {

  def props(feature: XivoFeature): Props =
    feature match {
      case l: Line if l.isCustom =>
        Props(
          new CustomDeviceTracker(
            l,
            eventBus,
            channelTracker,
            graphTracker,
            xucAmiBus,
            deviceTrackerConfig,
            configDispatcher,
            mediatorWrapper
          )
        )
      case l: Line =>
        Props(
          new SipDeviceTracker(
            l,
            eventBus,
            channelTracker,
            graphTracker,
            xucAmiBus,
            deviceTrackerConfig,
            configDispatcher,
            mediatorWrapper
          )
        )
      case t: Trunk =>
        Props(
          new TrunkDeviceTracker(
            t.interface,
            channelTracker,
            graphTracker,
            xucAmiBus,
            deviceTrackerConfig
          )
        )
      case m: MediaServerTrunk =>
        Props(
          new MediaServerTrunkDeviceTracker(
            m.interface,
            channelTracker,
            graphTracker,
            xucAmiBus,
            deviceTrackerConfig,
            mediatorWrapper
          )
        )
      case DAHDITrunk =>
        Props(
          new TrunkDeviceTracker(
            DAHDITrunk.interface,
            channelTracker,
            graphTracker,
            xucAmiBus,
            deviceTrackerConfig
          )
        )
      case _ =>
        Props(
          new UnknownDeviceTracker(
            feature.interface,
            channelTracker,
            graphTracker,
            xucAmiBus,
            deviceTrackerConfig,
            mediatorWrapper
          )
        )
    }

}

class DevicesTracker @Inject() (
    lineFactory: LineFactory,
    deviceActorFactory: DeviceActorFactory,
    xucAmiBus: XucAmiBus,
    xucConfig: XucConfig,
    mediatorWrapper: MediatorWrapper
) extends Actor
    with FastLogging
    with JmxActorSingletonMonitor {

  type InterfaceSet = Set[DeviceMessage.DeviceInterface]
  type MessageMap   = Map[DeviceMessage.DeviceInterface, List[DeviceMessage]]
  type ActorMap     = Map[DeviceMessage.DeviceInterface, List[ActorRef]]

  val jmxDeviceCount: Try[JmxLongMetric] =
    jmxBean.addLong("Devices", 0L, Some("Number of Device actor"))

  val mediator: ActorRef = mediatorWrapper.getMediator(context.system)

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
    xucAmiBus.subscribe(self, AmiType.AmiService)
    mediator ! Subscribe(ConferenceTracker.conferenceEventTopic, self)

    val devices = for {
      lines  <- lineFactory.all()
      trunks <- lineFactory.trunks
    } yield lines ++ trunks

    devices.failed.foreach(t =>
      if (context != null)
        log.error(t, "Error while fetching lines & trunks from database")
    )

    devices.foreach(_.foreach(d => self ! DevicesTracker.EnsureTrackerFor(d)))

    self ! DevicesTracker.EnsureTrackerFor(DAHDITrunk)
    self ! DevicesTracker.EnsureTrackerFor(LocalChannelFeature)
    self ! DevicesTracker.EnsureTrackerFor(
      AnonymousChannelFeature(xucConfig.sipDriver)
    )

    context.become(processMessage(Set.empty, Map.empty, Map.empty))
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
  }

  override def receive: Receive = Actor.emptyBehavior

  def processMessage(
      pendingActors: InterfaceSet,
      pendingMessages: MessageMap,
      deviceActors: ActorMap
  ): Receive = {
    case DevicesTracker.RegisterActor(interface, ref) =>
      log.debug("Registering DeviceActor {} for {}", ref, interface)
      jmxDeviceCount.inc()
      pendingMessages.get(interface).foreach(_.foreach(ref ! _))

      val actorRefs = ref :: deviceActors.getOrElse(interface, List.empty)

      context.become(
        processMessage(
          pendingActors - interface,
          pendingMessages - interface,
          deviceActors + (interface -> actorRefs)
        )
      )

    case DevicesTracker.UnRegisterActor(interface, ref) =>
      val actorRefs =
        deviceActors.getOrElse(interface, List.empty).filterNot(_ == ref)
      val newActors =
        if (actorRefs.isEmpty)
          deviceActors - interface
        else
          deviceActors + (interface -> actorRefs)
      jmxDeviceCount.dec()

      context.become(
        processMessage(
          pendingActors,
          pendingMessages,
          newActors
        )
      )

    case DevicesTracker.SwitchTrackerFor(oldLine, newLine) =>
      log.debug(
        "Switching DeviceActor from {} to {}",
        oldLine.interface,
        newLine.interface
      )
      deviceActors
        .get(oldLine.trackingInterface)
        .foreach(al => al.foreach(_ ! PoisonPill))
      self ! DevicesTracker.EnsureTrackerFor(newLine)

    case DevicesTracker.EnsureTrackerFor(feature: XivoFeature) =>
      log.debug("Ensuring DeviceActor for {}", feature.trackingInterface)
      Option(feature.trackingInterface)
        .filterNot(deviceActors.contains)
        .filterNot(pendingActors.contains)
        .foreach(iface => {
          context
            .actorOf(deviceActorFactory.props(feature), iface.replace('/', ':'))
          context.become(
            processMessage(pendingActors + iface, pendingMessages, deviceActors)
          )
        })

    case AmiConnected(mdsName) =>
      self ! DevicesTracker.EnsureTrackerFor(
        MediaServerTrunk("to-" + mdsName, xucConfig.sipDriver)
      )
      self ! DevicesTracker.EnsureTrackerFor(
        MediaServerTrunk("from-" + mdsName, xucConfig.sipDriver)
      )

    case msg: DeviceMessage =>
      log.debug(s"Receive DeviceMessage $msg")
      deviceActors
        .filter(i => msg.isFor(i._1))
        .flatMap(_._2)
        .foreach(_ forward msg)

      val targets = pendingActors.filter(msg.isFor)
      if (targets.nonEmpty) {
        val newMessages: MessageMap = targets
          .map(iface => {
            val msgList = pendingMessages.getOrElse(iface, List.empty)
            iface -> (msgList :+ msg)
          })
          .toMap
        context.become(
          processMessage(
            pendingActors,
            pendingMessages ++ newMessages,
            deviceActors
          )
        )
      }
  }
}
