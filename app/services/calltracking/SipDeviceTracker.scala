package services.calltracking

import org.apache.pekko.actor._

import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.{
  Publish,
  Subscribe,
  SubscribeAck,
  Unsubscribe
}
import services.XucAmiBus.AmiRequest
import services.calltracking.ConferenceTracker._
import services.calltracking.DeviceConferenceAction._
import services.calltracking.DeviceMessage._
import services.calltracking.SingleDeviceTracker.PartyInformation
import services.calltracking.graph.{NodeChannelLike, NodeLocalChannel}
import services.{MediatorWrapper, XucAmiBus, XucEventBus}
import xivo.events.{CurrentCallsPhoneEvents, PhoneEvent}
import xivo.models.Line
import xivo.websocket._
import xivo.xuc.DeviceTrackerConfig
import xivo.xucami.models.Channel

object SipDeviceTracker {
  case class WatchOutboundCallTo(interface: DeviceInterface, number: String)
      extends SingleDeviceTracker.DeviceMessageWithInterface
  case class UnWatchOutboundCallTo(interface: DeviceInterface, number: String)
      extends SingleDeviceTracker.DeviceMessageWithInterface
}

object SipDriver extends Enumeration {
  type SipDriver = Value
  val SIP, PJSIP                       = Value
  def toEnum(d: String): Option[Value] = values.find(_.toString == d)
}

class SipDeviceTracker(
    line: Line,
    eventBus: XucEventBus,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig,
    configDispatcher: ActorRef,
    mediatorWrapper: MediatorWrapper
) extends SingleDeviceTracker(
      line.trackingInterface,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig
    ) {

  import BaseTracker._
  import SingleDeviceTracker.{
    DeviceConferenceMessage,
    SendCurrentCallsPhoneEvents
  }

  val mediator: ActorRef = mediatorWrapper.getMediator(context.system)
  val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.SipDeviceTrackerType
  var lastPhoneEvent: Option[PhoneEvent] = None
  var mobileIface: Option[String]        = None

  override def customBehavior: Receive =
    sipDeviceBehavior orElse super.customBehavior

  override def postStop(): Unit = {
    clearMobileRegistration()
    super.postStop()
  }

  def sipDeviceBehavior: Receive = {
    case SendCurrentCallsPhoneEvents(_) =>
      sendCurrentCallsPhoneEvents(getCalls().toList)

    case evt: DeviceConference =>
      log.debug("{}: {}", line.interface, evt)
      evt match {
        case joinEvent: DeviceJoinConference =>
          mediator ! Subscribe(
            joinEvent.topic,
            self
          )
          tracker = tracker.withConferenceEvent(evt)
          sendConferenceEvents(evt.conference, evt.topic)
        case leaveEvent: DeviceLeaveConference =>
          onDeviceConferenceEvent(evt)
          mediator ! Unsubscribe(
            leaveEvent.topic,
            self
          )
          tracker = tracker.withConferenceEvent(evt)
      }

    case SubscribeAck(Subscribe(msg: String, None, _)) =>
      ConferenceTracker
        .getConfNameFromTopic(msg)
        .map(confName =>
          for {
            conf <- tracker.conferences
              .get(confName)
              .toSet[ConferenceWithChannels]
            chanNames <- conf.channelNames
            chanName  <- chanNames
          } yield GetParticipantConfRole(chanName)
        )
        .getOrElse(Set.empty)
        .foreach(evt =>
          mediator ! Publish(ConferenceTracker.conferenceEventTopic, evt)
        )

    case evt: ConferenceParticipantChange =>
      log.debug("{}: {}", line.interface, evt)
      if (evt.isInstanceOf[ParticipantLeaveConference] && isMe(evt)) {
        tracker.conferences
          .get(evt.numConf)
          .map(c =>
            DeviceLeaveConference(
              c.conference,
              evt.participant.channelName,
              c.topic,
              None
            )
          )
          .foreach(self forward _)
        toAmiRequests(Reset(evt.numConf))
          .getOrElse(List.empty)
          .foreach(r => xucAmiBus.publish(r))
      }
      tracker = tracker.withConferenceParticipantEvent(evt)
      onConferenceParticipantChange(evt)

    case DeviceConferenceMessage(_, command) =>
      val requests = toAmiRequests(command)

      requests match {
        case Right(list) => list.foreach(r => xucAmiBus.publish(r))
        case Left(e)     => sender() ! e
      }

    case SipDeviceTracker.WatchOutboundCallTo(_, mobile) =>
      val iface = s"Local/$mobile@"
      if (!mobileIface.contains(iface)) {
        clearMobileRegistration()
        channelTracker ! WatchChannelStartingWith(iface)
        graphTracker ! WatchChannelStartingWith(iface)
        context.parent ! DevicesTracker.RegisterActor(iface, self)
        mobileIface = Some(iface)
      }

    case SipDeviceTracker.UnWatchOutboundCallTo(_, mobile) =>
      clearMobileRegistration()
  }

  private def sendConferenceEvents(
      conf: ConferenceRoom,
      topic: String
  ): Unit = {
    log.debug("{}: {}", line.interface, conf)
    tracker = tracker.updateConference(conf)
    val joinEvents = tracker.conferences
      .get(conf.number)
      .map(c =>
        c.channelNames.flatten.map(ch =>
          DeviceJoinConference(c.conference, ch, topic)
        )
      )
      .getOrElse(List.empty)

    joinEvents.foreach(e => onDeviceConferenceEvent(e))
  }

  def clearMobileRegistration(): Unit = {
    mobileIface.foreach(iface => {
      channelTracker ! UnWatchChannelStartingWith(iface)
      graphTracker ! UnWatchChannelStartingWith(iface)
      context.parent ! DevicesTracker.UnRegisterActor(iface, self)

      tracker.calls.keys
        .filter(_.startsWith(iface))
        .foreach(self ! RemoveChannel(_))
    })
    mobileIface = None
  }

  def notify(call: DeviceCall, redoLast: Boolean = false): Unit = {
    DeviceCallToPhoneEvent(call, line)
      .filterNot(event =>
        if (redoLast) { false }
        else { lastPhoneEvent.contains(event) }
      )
      .foreach(e => {
        log.debug("{}: {}", line.interface, e)
        lastPhoneEvent = Some(e)
        configDispatcher ! e
      })
  }

  def sendCurrentCallsPhoneEvents(calls: List[DeviceCall]): Unit = {
    configDispatcher ! CurrentCallsPhoneEvents(
      line.number.get,
      calls.map(DeviceCallToPhoneEvent(_, line)).flatten
    )
    for {
      c <- tracker.conferences.values
      conference = c.conference
      channels   = c.channelNames
      channelNameSet <- channels
      channelName    <- channelNameSet
      call           <- findCallByChannel(channelName)
      channel        <- call.channel
      number         <- line.number
    } {
      val me       = c.findMe().map(_.index)
      val joinConf = DeviceJoinConference(conference, channelName, c.topic)
      val wsmsg    = WsConferenceEvent.from(joinConf, channel.id, number, me)
      log.debug(s"publishing $wsmsg")
      configDispatcher ! wsmsg
    }
  }

  def onDeviceConferenceEvent(
      evt: DeviceConference
  ): Unit = {
    val me = tracker.conferences
      .get(evt.conference.number)
      .map(_.findMe().map(_.index))
      .getOrElse(List.empty)

    for {
      call    <- findCallByChannel(evt.channel)
      channel <- call.channel
      number  <- line.number
    } {
      val wsmsg = WsConferenceEvent.from(evt, channel.id, number, me)
      log.debug(s"publishing $wsmsg")
      configDispatcher ! wsmsg
    }
  }

  def onConferenceParticipantChange(evt: ConferenceParticipantChange): Unit = {
    for {
      c <- tracker.conferences.get(evt.numConf)
      conf     = c.conference
      channels = c.channelNames
      channelNameSet <- channels
      channelName    <- channelNameSet
      call           <- findCallByChannel(channelName)
      channel        <- call.channel
      number         <- line.number
    } {
      val me    = c.findMe().map(_.index)
      val wsmsg = WsConferenceParticipantEvent.from(evt, channel.id, number, me)
      log.debug(s"publishing $wsmsg")
      configDispatcher ! wsmsg
    }
  }

  def onPartyInformation(evt: PartyInformation): Unit = {}

  def findCallByChannel(channel: String): Option[DeviceCall] =
    tracker.calls.get(channel)

  def isMe(evt: ConferenceParticipantChange): Boolean = {
    tracker.conferences
      .get(evt.numConf)
      .map(_.channelNames.flatten.contains(evt.participant.channelName))
      .getOrElse(false)
  }

  def toAmiRequests(
      command: DeviceConferenceCommand
  ): Either[DeviceConferenceCommandErrorType, List[XucAmiBus.AmiRequest]] =
    for {
      actions <- DeviceConferenceAction.toAmi(
        tracker.conferences,
        command,
        line.context
      )
      conf <-
        tracker.conferences.get(command.numConf).toRight(ConferenceNotFound)
      mds      = conf.conference.mds
      requests = actions.map(AmiRequest(_, Some(mds)))
    } yield requests

  override def filterChannel(c: Channel): Boolean =
    !(c.isLocal && c.name.endsWith(";2"))
  override def filterNodeChannel(c: NodeChannelLike): Boolean =
    c match {
      case NodeLocalChannel(name, _) if c.name.endsWith(";2") => false
      case _                                                  => true
    }
}
