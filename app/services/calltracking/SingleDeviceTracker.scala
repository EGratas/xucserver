package services.calltracking

import org.apache.pekko.actor._
import helpers.{FastLogging, JmxActorMonitor}
import org.asteriskjava.manager.action.{StopMonitorAction, UnpauseMonitorAction}
import services.XucAmiBus
import services.XucAmiBus.AmiAction
import services.calltracking.DeviceConferenceAction._
import services.calltracking.DeviceMessage._
import services.calltracking.graph.NodeChannelLike
import xivo.xuc.DeviceTrackerConfig
import xivo.xucami.models.{Channel, ChannelState, MonitorState}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import helpers.JmxLongMetric
import scala.util.Try

object SingleDeviceTracker {

  trait DeviceMessageWithInterface extends DeviceMessage {
    def interface: DeviceInterface
    override def isFor(dstInterface: DeviceInterface): Boolean =
      interface.stripSuffix("-") == dstInterface.stripSuffix("-")
  }

  case object GetCalls
  case class Calls(calls: List[DeviceCall])
  case class PartyInformation(
      partyChannelName: DeviceInterface,
      channel: Channel,
      sourceTrackerType: DeviceTrackerType
  ) extends DeviceMessage {

    override def isFor(interface: DeviceInterface): Boolean =
      partyChannelName.startsWith(interface)
  }

  case class MonitorCalls(interface: DeviceInterface)
      extends DeviceMessageWithInterface
  case class UnMonitorCalls(interface: DeviceInterface)
      extends DeviceMessageWithInterface
  case class DeviceConferenceMessage(
      interface: DeviceInterface,
      command: DeviceConferenceCommand
  ) extends DeviceMessageWithInterface

  case class SendCurrentCallsPhoneEvents(interface: DeviceInterface)
      extends DeviceMessageWithInterface

  sealed trait DeviceTrackerType
  case object SipDeviceTrackerType              extends DeviceTrackerType
  case object CustomDeviceTrackerType           extends DeviceTrackerType
  case object TrunkDeviceTrackerType            extends DeviceTrackerType
  case object MediaServerTrunkDeviceTrackerType extends DeviceTrackerType
  case object UnknownDeviceTrackerType          extends DeviceTrackerType

  def getRecordingAction(
      myMonitorState: Option[MonitorState.MonitorState],
      otherRecordingMode: Option[String],
      otherMonitorState: Option[MonitorState.MonitorState],
      channelName: String
  ): Option[AmiAction] = {
    (for {
      s  <- myMonitorState
      om <- otherRecordingMode
      os <- otherMonitorState
    } yield (s, om, os)) collect {
      case (MonitorState.ACTIVE, "notrecorded", _) =>
        AmiAction(new StopMonitorAction(channelName))
      case (MonitorState.PAUSED, "recorded", _) =>
        AmiAction(new UnpauseMonitorAction(channelName))
      case (MonitorState.PAUSED, "notrecorded", _) =>
        AmiAction(new StopMonitorAction(channelName))
      case (MonitorState.DISABLED, "notrecorded", MonitorState.ACTIVE) =>
        AmiAction(new StopMonitorAction(channelName))
      case (MonitorState.DISABLED, "notrecorded", MonitorState.PAUSED) =>
        AmiAction(new StopMonitorAction(channelName))
    }
  }
}

abstract class SingleDeviceTracker(
    interface: DeviceInterface,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig
) extends Actor
    with FastLogging
    with JmxActorMonitor {

  import AsteriskGraphTracker._
  import BaseTracker._
  import SingleDeviceTracker._

  case class RemoveChannel(name: String)
  case class RemoveLoopChannel(name: String)

  val jmxCallCount: Try[JmxLongMetric] =
    jmxBean.addLong("Calls", 0L, Some("Current number of call"))
  val jmxPartyInformationCount: Try[JmxLongMetric] = jmxBean.addLong(
    "PartyInformations",
    0L,
    Some("Total number of PartyInformation received")
  )
  val jmxPathsCount: Try[JmxLongMetric] = jmxBean.addLong(
    "PathsFromChannel",
    0L,
    Some("Total number of PathsFromChannel received")
  )
  val jmxChannelsCount: Try[JmxLongMetric] = jmxBean.addLong(
    "ChannelEvent",
    0L,
    Some("Total number of Channel message received")
  )
  jmxBean.registerOperation(
    "stop",
    stopme,
    Some("Stop actor. Warning, it will not be restarted automatically !")
  )
  jmxBean.registerOperation(
    "clearCalls",
    clearcalls,
    Some("Clear list of calls from this tracker but does not hangup them.")
  )

  var tracker: CallTracker        = CallTracker()
  var callWatchers: Set[ActorRef] = Set.empty
  var loopedChannels: Set[String] = Set.empty

  var lastNotification: Option[DeviceCall] = None

  val deviceTrackerType: DeviceTrackerType

  val channelStartsWith: DeviceInterface = interface

  override def preStart(): Unit = {
    log.debug(s"Starting DeviceTracker for $interface ($channelStartsWith)")
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
    channelTracker ! WatchChannelStartingWith(channelStartsWith)
    graphTracker ! WatchChannelStartingWith(channelStartsWith)
    context.parent ! DevicesTracker.RegisterActor(channelStartsWith, self)
    context.become(customBehavior)
  }

  override def postStop(): Unit = {
    log.debug(s"Stopping DeviceTracker for $interface ($channelStartsWith)")
    channelTracker ! UnWatchChannelStartingWith(channelStartsWith)
    graphTracker ! UnWatchChannelStartingWith(channelStartsWith)
    context.parent ! DevicesTracker.UnRegisterActor(channelStartsWith, self)
    jmxBean.unregister()
  }

  def stopme(): Unit = self ! PoisonPill

  def clearcalls(): Unit = {
    tracker.calls.keys.foreach(self ! RemoveChannel(_))
  }

  def customBehavior: Receive = receive

  override def receive: PartialFunction[Any, Unit] = {
    case PathsFromChannel(c: NodeChannelLike, paths) =>
      jmxPathsCount.inc()
      if (!loopedChannels.contains(c.name) && filterNodeChannel(c)) {
        for {
          call    <- tracker.calls.get(c.name)
          channel <- call.channel
          newConnection <-
            pathsEndChannels(paths) -- pathsEndChannels(call.paths)
        } context.parent ! PartyInformation(
          newConnection.name,
          channel,
          deviceTrackerType
        )

        tracker = tracker.withPaths(c, paths)
        notify(c.name)
      }
      log.debug("{}: PathsFromChannel({}) => {}", interface, paths, tracker)

    case c: Channel =>
      jmxChannelsCount.inc()
      if (!loopedChannels.contains(c.name) && filterChannel(c)) {
        tracker = tracker.withChannel(c)
        jmxCallCount.set(tracker.calls.size)
        if (c.state == ChannelState.HUNGUP) {
          scheduler.scheduleOnce(2.seconds, self, RemoveChannel(c.name))
        }
        for {
          call   <- tracker.calls.get(c.name)
          remote <- pathsEndChannels(call.paths)
        } context.parent ! PartyInformation(remote.name, c, deviceTrackerType)
        notify(c.name)
      }
      log.debug("{}: Channel({}) => {}", interface, c.name, tracker)

    case RemoveChannel(name) =>
      tracker = tracker.removeChannel(name)
      jmxCallCount.set(tracker.calls.size)
      log.debug("{}: RemoveChannel({}) => {}", interface, name, tracker)

    case RemoveLoopChannel(name) =>
      loopedChannels = loopedChannels - name
      log.debug("{}: RemoveLoopChannel({}) => {}", interface, name)

    case PartyInformation(partyChannelName, channel, sourceTrackerType) =>
      jmxPartyInformationCount.inc()
      tracker = tracker.withPartyInformation(partyChannelName, channel)
      onPartyInformation(
        PartyInformation(partyChannelName, channel, sourceTrackerType)
      )
      toggleRecording(
        PartyInformation(partyChannelName, channel, sourceTrackerType)
      )
      notify(partyChannelName)
      log.debug(
        "{}: PartyInformation({}) => {}",
        interface,
        partyChannelName,
        tracker
      )

    case GetCalls =>
      val calls = getCalls()
      sender() ! Calls(calls.toList)

    case MonitorCalls(_) =>
      callWatchers = callWatchers + sender()
      sender() ! DeviceCalls(interface, tracker.calls)

    case UnMonitorCalls(_) =>
      callWatchers = callWatchers - sender()

    case LoopDetected(channels) =>
      val channelNames = channels.map(_.name)
      loopedChannels = loopedChannels ++ channelNames
      channelNames.foreach(name => {
        tracker = tracker.removeChannel(name)
        scheduler.scheduleOnce(30.seconds, self, RemoveLoopChannel(name))
      })
      jmxCallCount.set(tracker.calls.size)
  }

  protected def getCalls(): Iterable[DeviceCall] =
    tracker.calls.values.filter(
      _.channel.forall(_.state != ChannelState.HUNGUP)
    )

  def scheduler: Scheduler = context.system.scheduler

  def notify(channelName: String): Unit = {
    callWatchers.foreach(
      _ ! DeviceCalls(
        interface,
        tracker.calls.filter(
          _._2.channel.forall(_.state != ChannelState.HUNGUP)
        )
      )
    )
    tracker.calls
      .get(channelName)
      .filterNot(lastNotification.contains)
      .foreach(c => {
        lastNotification = Some(c)
        notify(c)
      })
  }

  def notify(call: DeviceCall, redoLast: Boolean = false): Unit

  def onPartyInformation(evt: PartyInformation): Unit

  def toggleRecording(evt: PartyInformation): Unit = {

    def logAmiAction(
        act: XucAmiBus.AmiAction,
        channel: Option[Channel],
        channelName: String,
        call: DeviceCall,
        additionalMessage: String
    ): Unit = {
      log.info(
        s"${act.message.getAction} " +
          s"on channel $channelName " +
          s"for recordingid:${channel
            .foreach(_.variables.getOrElse("recordingid", ""))} " +
          s"(channel ${evt.partyChannelName} " +
          additionalMessage
      )
    }

    def publishRecordActionToAmi(
        myMonitorState: Option[MonitorState.MonitorState],
        call: DeviceCall
    ): Unit = {
      val otherPartyRecMode =
        evt.channel.variables.get(Channel.VarNames.RecordingMode)
      val otherPartyMonitorState = Option(evt.channel.monitored)
      val action = getRecordingAction(
        myMonitorState,
        otherPartyRecMode,
        otherPartyMonitorState,
        evt.partyChannelName
      )

      action.foreach(act => {
        xucAmiBus.publish(act)
        logAmiAction(
          act,
          call.channel,
          evt.partyChannelName,
          call,
          s" was bridged with channel ${evt.channel.name} " +
            s"which is in ${otherPartyRecMode.getOrElse("")} mode)"
        )
      })
    }

    def publishExplicitStopRecordActionToAmi(call: DeviceCall): Unit = {
      // This part is run only to stop monitor for monitored channels in path which are not the end channel
      // It purposely does not uses the getRecordingAction method because it acts the other way round :
      // here it stop recording according to myMonitorState AND myRecMode AND otherPartyMonitorState
      // (getRecordingAction stops/starts recordin according to myMonitorState AND otherPartyRecMode)
      val myRecMode = call.channel.map(
        _.variables.getOrElse(Channel.VarNames.RecordingMode, "")
      )
      if (myRecMode.contains("notrecorded")) {
        if (
          evt.channel.monitored == MonitorState.ACTIVE || evt.channel.monitored == MonitorState.PAUSED
        ) {
          val action = new StopMonitorAction(evt.channel.name)
          xucAmiBus.publish(AmiAction(action))
          logAmiAction(
            AmiAction(action),
            Some(evt.channel),
            evt.channel.name,
            call,
            s"in ${myRecMode.getOrElse("")} mode " +
              s"was bridged with channel ${evt.channel.name} which was being recorded)"
          )
        }
      }
    }

    if (deviceTrackerConfig.enableRecordingRules) {
      tracker.calls
        .get(evt.partyChannelName)
        .foreach(call => {
          val myMonitorState = call.channel.map(_.monitored)
          if (pathsEndChannels(call.paths).exists(_.name == evt.channel.name)) {
            publishRecordActionToAmi(myMonitorState, call)
          } else if (myMonitorState.contains(MonitorState.DISABLED)) {
            publishExplicitStopRecordActionToAmi(call)
          }
        })
    }
  }

  def filterChannel(c: Channel): Boolean             = true
  def filterNodeChannel(c: NodeChannelLike): Boolean = true
}
