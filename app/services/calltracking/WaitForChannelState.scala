package services.calltracking

import org.apache.pekko.actor.*

import scala.concurrent.{
  CanAwait,
  ExecutionContext,
  ExecutionContextExecutor,
  Future,
  Promise
}
import scala.util.Try
import services.calltracking.BaseTracker.*
import services.calltracking.graph.NodeChannelLike
import xivo.xucami.models.{Channel, ChannelState}
import org.apache.pekko.util.Timeout

import scala.concurrent.duration.*

sealed trait ChannelStateResult
case object ChannelStateOk extends ChannelStateResult
case object ChannelStateKo
    extends Exception("Channel is not in expected state")
    with ChannelStateResult
case class ChannelStateTimeout(channel: String)
    extends Exception(
      s"Timeout while waiting for channel $channel to change state"
    )
    with ChannelStateResult

class WaitForChannelState(
    channelTracker: ActorRef,
    channel: NodeChannelLike,
    expectedState: ChannelState.ChannelState,
    result: Promise[ChannelStateResult]
) extends Actor
    with ActorLogging {

  override def preStart(): Unit = {
    channelTracker ! WatchChannelStartingWith(channel.name)
  }

  override def postStop(): Unit = {
    channelTracker ! UnWatchChannelStartingWith(channel.name)
  }

  override def receive: PartialFunction[Any, Unit] = { case c: Channel =>
    if (c.state == expectedState) {
      log.debug(s"$channel is in state $expectedState")
      result.success(ChannelStateOk)
      context.stop(self)
    } else if (c.state == ChannelState.HUNGUP) {
      log.debug(s"$channel is hung up")
      result.failure(ChannelStateKo)
      context.stop(self)
    }
  }
}

abstract class CancellableWaitForChannelState(
    future: Future[ChannelStateResult]
) extends Future[ChannelStateResult] {
  def cancel(): Unit

  override def onComplete[U](f: Try[ChannelStateResult] => U)(implicit
      executor: ExecutionContext
  ): Unit = future.onComplete(f)

  override def isCompleted: Boolean = future.isCompleted

  override def value: Option[Try[ChannelStateResult]] = future.value

  override def result(atMost: Duration)(implicit
      permit: CanAwait
  ): ChannelStateResult = future.result(atMost)

  override def ready(atMost: Duration)(implicit permit: CanAwait): this.type = {
    future.ready(atMost)
    this
  }

  override def transform[S](f: Try[ChannelStateResult] => Try[S])(implicit
      executor: ExecutionContext
  ): Future[S] = future.transform(f)

  override def transformWith[S](f: Try[ChannelStateResult] => Future[S])(
      implicit executor: ExecutionContext
  ): Future[S] = future.transformWith(f)

}

object WaitForChannelState {
  private val defaultTimeout: Timeout = Timeout(1.minute)

  def apply(
      channelTracker: ActorRef,
      channel: NodeChannelLike,
      expectedState: ChannelState.ChannelState
  )(implicit context: ActorContext): CancellableWaitForChannelState =
    apply(channelTracker, channel, expectedState, defaultTimeout)(context)

  def apply(
      channelTracker: ActorRef,
      channel: NodeChannelLike,
      expectedState: ChannelState.ChannelState,
      timeout: Timeout
  )(implicit context: ActorContext): CancellableWaitForChannelState = {
    val result                                = Promise[ChannelStateResult]()
    val scheduler                             = context.system.scheduler
    implicit val ec: ExecutionContextExecutor = context.dispatcher
    val actor = context.actorOf(
      Props(
        new WaitForChannelState(channelTracker, channel, expectedState, result)
      )
    )
    val f = scheduler.scheduleOnce(timeout.duration) {
      result tryComplete scala.util.Failure(ChannelStateTimeout(channel.name))
    }

    result.future onComplete { _ =>
      actor ! PoisonPill
      f.cancel()
    }

    new CancellableWaitForChannelState(result.future) {
      override def cancel(): Unit = {
        actor ! PoisonPill
        f.cancel()
      }
    }
  }
}
