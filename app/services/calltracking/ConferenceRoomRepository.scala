package services.calltracking

object ConferenceRoomRepository {
  type ConferenceMap         = Map[String, ConferenceRoom]
  type ChannelParticipantMap = Map[String, ConferenceParticipant]

  val empty: ConferenceRoomRepository =
    ConferenceRoomRepository(Map.empty, Map.empty)
}

import ConferenceRoomRepository._

case class ConferenceRoomRepository(
    conferences: ConferenceMap,
    channelMap: ChannelParticipantMap
) {

  def numbers: Set[String] = conferences.keySet

  def updateConference(c: ConferenceRoom): ConferenceRoomRepository = {
    val updatedConf = conferences
      .get(c.number)
      .map(_.copy(name = c.name))
      .getOrElse(c)

    ConferenceRoomRepository(
      conferences.updated(c.number, updatedConf),
      channelMap
    )
  }

  def removeConference(numConf: String): ConferenceRoomRepository = {
    if (conferences.get(numConf).filter(_.participants.isEmpty).isDefined) {
      val removedChannels = conferences
        .get(numConf)
        .map(_.participants.map(_.channelName))
        .getOrElse(List.empty)

      ConferenceRoomRepository(
        conferences - numConf,
        channelMap -- removedChannels
      )
    } else { this }
  }

  def getConference(numConf: String): Option[ConferenceRoom] =
    conferences.get(numConf)

  def addParticipant(
      p: ConferenceParticipant,
      mds: String
  ): ConferenceRoomRepository = {
    val c = conferences
      .getOrElse(p.numConf, ConferenceRoom.empty(p.numConf, mds))
      .addParticipant(p)
    val channels = channelMap.updated(p.channelName, p)
    ConferenceRoomRepository(
      conferences.updated(c.number, c),
      channels
    )
  }

  def removeParticipant(
      p: ConferenceParticipant,
      mds: String
  ): ConferenceRoomRepository = {
    val c = conferences
      .getOrElse(p.numConf, ConferenceRoom.empty(p.numConf, mds))
      .removeParticipant(p)
    val channels = channelMap.updated(p.channelName, p)
    ConferenceRoomRepository(
      conferences.updated(c.number, c),
      channels
    )
  }

  def updateParticipant(numConf: String, index: Int)(
      f: ConferenceParticipant => ConferenceParticipant
  ): ConferenceRoomRepository = {
    conferences
      .get(numConf)
      .map(_.updateParticipant(index)(f))
      .map(c => {
        ConferenceRoomRepository(
          conferences.updated(c.number, c),
          channelMap
        )
      })
      .getOrElse(this)
  }

  def getParticipant(
      numConf: String,
      index: Int
  ): Option[ConferenceParticipant] =
    conferences.get(numConf).flatMap(_.getParticipant(index))

  def getParticipantByChannel(c: String): Option[ConferenceParticipant] =
    channelMap.get(c)
}
