package services.calltracking

import org.apache.pekko.actor._
import org.asteriskjava.manager.action.StopMonitorAction
import services.XucAmiBus.AmiAction
import services.calltracking.SingleDeviceTracker.PartyInformation
import services.XucAmiBus
import xivo.xuc.DeviceTrackerConfig
import xivo.xucami.models.MonitorState
import DeviceMessage._

class TrunkDeviceTracker(
    trunkInterface: DeviceInterface,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig
) extends SingleDeviceTracker(
      trunkInterface,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig
    ) {
  val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.TrunkDeviceTrackerType
  def notify(call: DeviceCall, redoLast: Boolean = false): Unit = {}

  def onPartyInformation(evt: PartyInformation): Unit = {
    if (deviceTrackerConfig.stopRecordingUponExternalXfer) {
      if (evt.sourceTrackerType == SingleDeviceTracker.TrunkDeviceTrackerType) {
        tracker.calls
          .get(evt.partyChannelName)
          .foreach(call => {
            if (call.channel.map(_.monitored).contains(MonitorState.ACTIVE)) {
              log.info(
                "Stop recording for recordingid:{} (external channel {} was bridged with external channel {})",
                call.variables.get("recordingid"),
                evt.partyChannelName,
                evt.channel.name
              )
              val action = new StopMonitorAction(evt.partyChannelName)
              xucAmiBus.publish(AmiAction(action))
            }
          })
      }
    }
  }
}
