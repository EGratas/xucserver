package services.calltracking

import org.apache.pekko.actor._
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.Publish
import services.{MediatorWrapper, XucAmiBus}
import services.calltracking.ConferenceTracker._
import services.calltracking.DeviceMessage._
import services.calltracking.SingleDeviceTracker.PartyInformation
import xivo.xuc.DeviceTrackerConfig

class UnknownDeviceTracker(
    interface: DeviceInterface,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig,
    mediatorWrapper: MediatorWrapper
) extends SingleDeviceTracker(
      interface,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig
    ) {
  val mediator: ActorRef = mediatorWrapper.getMediator(context.system)

  override def customBehavior: Receive =
    redirectConferenceEvent orElse super.customBehavior

  private def forwardToDevicesTracker(msg: DeviceMessage): Unit =
    context.parent.forward(msg)
  private def forwardToMediator(msg: DeviceMessage): Unit =
    mediator ! Publish(conferenceEventTopic, msg)

  private def retrieveSipCallID(channel: String): Option[String] = {
    tracker.calls.get(channel).flatMap(_.variables.get("XIVO_SIPCALLID"))
  }

  def redirectConferenceEvent: Receive = {
    case e @ DeviceJoinConference(_, channel, _, None, _) =>
      log.debug(s"Receive join event without sip call id : $e")
      getRemoteChannelName(channel) match {
        case Some(chan) =>
          log.debug(s"Forward event to DevicesTracker for channel $chan")
          forwardToDevicesTracker(e.copy(channel = chan))
        case None =>
          retrieveSipCallID(channel).map(sipCallId => {
            log.debug(s"Forward event to Mediator with sip call id $sipCallId")
            forwardToMediator(
              e.copy(sipCallId = Some(sipCallId), localChannel = Some(channel))
            )
          })
      }

    case evt @ DeviceJoinConference(_, _, _, Some(sipCallId), _) =>
      log.debug(s"Receive join event with sip call id : $evt")
      tracker.calls.values
        .find(
          _.variables.exists(variable =>
            variable._1 == "XIVO_SIPCALLID" && variable._2 == sipCallId
          )
        )
        .flatMap(dc => getRemoteChannelName(dc.channelName))
        .map(c => evt.copy(channel = c))
        .foreach(forwardToDevicesTracker)

    case evt @ DeviceLeaveConference(_, _, _, Some(sipCallId)) =>
      log.debug(s"Receive leave event with sip call id : $evt")
      tracker.calls.values
        .find(
          _.variables.exists(variable =>
            variable._1 == "XIVO_SIPCALLID" && variable._2 == sipCallId
          )
        )
        .flatMap(dc => getRemoteChannelName(dc.channelName))
        .map(c => evt.copy(channel = c))
        .foreach(forwardToDevicesTracker)

    case e @ DeviceLeaveConference(_, channel, _, _) =>
      log.debug(s"Receive join event without sip call id : $e")
      getRemoteChannelName(channel) match {
        case Some(remoteChannel) =>
          log.debug(
            s"Forward event to DevicesTracker for channel $remoteChannel"
          )
          forwardToDevicesTracker(e.copy(channel = remoteChannel))
        case None =>
          retrieveSipCallID(channel).map(sipCallId => {
            log.debug(s"Forward event to Mediator with sip call id $sipCallId")
            forwardToMediator(
              e.copy(sipCallId = Some(sipCallId))
            )
          })
      }
  }

  private def getRemoteChannelName(channel: String): Option[String] =
    tracker.calls
      .get(channel)
      .map(call => AsteriskGraphTracker.pathsEndChannels(call.paths))
      .flatMap(_.headOption)
      .map(_.name)

  val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.UnknownDeviceTrackerType
  def notify(call: DeviceCall, redoLast: Boolean = false): Unit = {}
  def onPartyInformation(evt: PartyInformation): Unit = {}
}
