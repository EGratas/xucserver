package services

import org.apache.pekko.actor.{Actor, ActorRef, PoisonPill}
import com.fasterxml.jackson.databind.JsonNode
import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import com.google.inject.name.Named
import models.{LineConfig, XucUser}
import org.xivo.cti.MessageFactory
import org.xivo.cti.message._
import org.xivo.cti.message.{UserConfigUpdate => DeprecatedUserConfigUpdate}
import org.xivo.cti.model.PhoneHintStatus
import play.api.Logger
import play.api.libs.concurrent.InjectedActorSupport
import play.api.libs.json.JsValue
import services.XucStatsEventBus.AggregatedStatEvent
import services.callhistory.CallHistoryEnricher
import services.config.ConfigDispatcher._
import services.config.ConfigRepository
import services.request._
import services.video.model.{UserVideoEvent, VideoStatusEvent}
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped}
import xivo.events._
import xivo.models.{
  Agent,
  UserConfigUpdate,
  UserConfigUpdated,
  UserServicesUpdated
}
import xivo.network.LoggedOn
import xivo.websocket.WsBus.WsContent
import xivo.websocket._

trait CtiFilterLog {
  this: CtiFilter =>
  val logger: Logger = Logger(
    getClass.getPackage.getName + ".CtiFilter." + self.path.name
  )
  private def logMessage(msg: String): String = {
    s"[${user.username}]-${filterConfig.userId.getOrElse("..")}-${user.phoneNumber
      .getOrElse("..")}-${filterConfig.agentId.getOrElse("..")}" +
      s"-${filterConfig.lineConfig map (lcf => s"${lcf.id} - ${lcf.phoneNb}")} - $msg"
  }
  object log {
    def debug(msg: String): Unit = logger.debug(logMessage(msg))
    def info(msg: String): Unit  = logger.info(logMessage(msg))
    def error(msg: String): Unit = logger.error(logMessage(msg))
    def warn(msg: String): Unit  = logger.warn(logMessage(msg))
  }
}

object CtiFilter {
  trait Factory {
    def apply(
        user: XucUser,
        @Assisted("myRouter") myRouter: ActorRef,
        @Assisted("personalContactRepo") personalContactRepo: ActorRef,
        configRepo: ConfigRepository
    ): Actor
  }
}
case class FilterConfig(
    userId: Option[Long],
    agentId: Option[Agent.Id],
    lineConfig: Option[LineConfig]
) {
  def withAgentId(agentId: Agent.Id): FilterConfig =
    this.copy(agentId = Some(agentId))
  def withUserId(userId: Long): FilterConfig = this.copy(userId = Some(userId))
  def withLineConfig(lineConfig: LineConfig): FilterConfig =
    this.copy(lineConfig = Some(lineConfig))
  def withNoLineConfig: FilterConfig = this.copy(lineConfig = None)
}
object FilterConfig {
  def apply(): FilterConfig = FilterConfig(None, None, lineConfig = None)
}

sealed trait filterEventChecker {
  this: CtiFilter =>
  def eventIsForMe(userServicesUpdated: UserServicesUpdated): Boolean =
    userServicesUpdated.userId == user.xivoUser.id
  def eventIsForMe(phoneStatusUpdate: PhoneStatusUpdate): Boolean = {
    filterConfig.lineConfig match {
      case Some(lineConfig) =>
        lineConfig.id == phoneStatusUpdate.getLineId.toString
      case _ => false
    }
  }
  def eventIsForMe(configUpdate: UserConfigUpdate): Boolean =
    filterConfig.userId.contains(configUpdate.userId)

  def eventIsForMe(phoneHintStatusEvent: PhoneHintStatusEvent): Boolean =
    filterConfig.lineConfig.exists(lineConfig =>
      lineConfig.phoneNb == phoneHintStatusEvent.number
    )
}

class CtiFilter @Inject() (
    @Assisted val user: XucUser,
    @Assisted("myRouter") myRouter: ActorRef,
    @Assisted("personalContactRepo") personalContactRepo: ActorRef,
    @Assisted val configRepo: ConfigRepository,
    eventBus: XucEventBus,
    callHistoryEnricherFactory: CallHistoryEnricher.Factory,
    @Named(ActorIds.UserPreferenceService) userPreferenceService: ActorRef
) extends Actor
    with filterEventChecker
    with CtiFilterLog
    with InjectedActorSupport {

  protected[services] var messageFactory             = new MessageFactory
  protected[services] var filterConfig: FilterConfig = FilterConfig()
  private var userConfig: Option[UserConfigUpdate]   = None
  private var currentAgentState: Option[AgentState]  = None

  override def preStart(): Unit = {
    eventBus.subscribe(self, XucEventBus.userEventTopic(user.xivoUser.id))
    myRouter ! RequestConfig(
      self,
      GetUserServices(user.xivoUser.id.toInt)
    )
  }

  log.info(s"starting cti filter ${self.path} ${context.parent}")

  var nextActorId: Long = 0

  def getNextActorId: Long = {
    nextActorId = nextActorId + 1
    nextActorId
  }

  private def updateAgentId(update: UserConfigUpdate): Unit = {
    import services.config.ObjectType.TypeAgent
    if (update.agentId != 0) {
      filterConfig = filterConfig.withAgentId(update.agentId)
      val agentTopic    = XucEventBus.agentEventTopic(update.agentId)
      val agentLogTopic = XucEventBus.agentLogTransitionTopic(update.agentId)
      eventBus.unsubscribe(self, agentTopic)
      eventBus.unsubscribe(self, agentLogTopic)
      eventBus.subscribe(self, agentTopic)
      eventBus.subscribe(self, agentLogTopic)
      myRouter ! RequestStatus(self, update.agentId.toInt, TypeAgent)
      log.info(s"agentId  ${update.agentId} saved subscribed to $agentTopic")
    }
  }
  private def updatePhoneId(update: UserConfigUpdate): Unit = {
    update.lineIds match {
      case Nil =>
      case phoneId :: tail =>
        log.info(s"Used phoneId : $phoneId")
        if (
          filterConfig.agentId.isDefined && filterConfig.lineConfig.isDefined
        ) {
          log.info(
            s"User is agent ${filterConfig.agentId} and line already defined : ${filterConfig.lineConfig}"
          )
        } else {
          myRouter ! BaseRequest(self, UpdateLineForUser(update.userId))
        }
    }
  }

  private def processUserServicesUpdated(updated: UserServicesUpdated): Unit = {

    def addServices(
        updated: UserServicesUpdated,
        config: UserConfigUpdate
    ): UserConfigUpdate = {
      config.copy(
        userId = updated.userId,
        agentId = config.agentId,
        dndEnabled = updated.services.dndEnabled,
        naFwdEnabled = updated.services.noanswer.enabled,
        naFwdDestination = updated.services.noanswer.destination,
        uncFwdEnabled = updated.services.unconditional.enabled,
        uncFwdDestination = updated.services.unconditional.destination,
        busyFwdEnabled = updated.services.busy.enabled,
        busyFwdDestination = updated.services.busy.destination
      )
    }

    if (eventIsForMe(updated)) {
      userConfig = userConfig.map(config => addServices(updated, config))
    }

    userConfig.foreach(config => {
      myRouter ! WebSocketEvent.createEvent(config)
    })
  }

  private def processPhoneStatusUpdate(
      phoneStatusUpdate: PhoneStatusUpdate
  ): Unit = {
    if (eventIsForMe(phoneStatusUpdate)) {
      try {
        val hintStatus = PhoneHintStatus.getHintStatus(
          Integer.decode(phoneStatusUpdate.getHintStatus)
        )
        myRouter ! WebSocketEvent.createEvent(hintStatus)
      } catch {
        case e: NumberFormatException =>
          log.debug(
            "Invalid PhoneHintStatus value, dropping message: " + phoneStatusUpdate
          )
      }
    }
  }

  private def processPhoneHintStatusEvent(
      phoneHintStatusEvent: PhoneHintStatusEvent
  ): Unit =
    if (eventIsForMe(phoneHintStatusEvent))
      myRouter ! WebSocketEvent.createEvent(phoneHintStatusEvent.status)

  private def getLoggedInOnAnotherPhoneError(
      currentPhone: String,
      requestedPhone: String
  ): JsValue = {
    WebSocketEvent.createError(
      WSMsgType.AgentError,
      "LoggedInOnAnotherPhone",
      Map(
        "phoneNb"     -> currentPhone,
        "RequestedNb" -> requestedPhone
      )
    )
  }

  import services.config.ObjectType.TypeUser
  import xivo.events.AgentState.AgentLoggedOut

  def receive: PartialFunction[Any, Unit] =
    processAgentRequest orElse processUserRequest orElse mainReceive

  def processAgentRequest: Receive = {

    case AgentLoginRequest(None, Some(phoneNumber), None) =>
      log.debug(s"AgentLoginRequest(user=$user, filterConfig=$filterConfig)")
      filterConfig.agentId match {
        case Some(agId) =>
          getLoggedAgentPhoneNumber match {
            case Some(n) if n != phoneNumber =>
              myRouter ! getLoggedInOnAnotherPhoneError(n, phoneNumber)
              currentAgentState
                .map(WebSocketEvent.createEvent)
                .foreach(myRouter ! _)
            case _ =>
              configRepo
                .getLineConfig(LineConfigQueryByNb(phoneNumber))
                .flatMap(_.line)
                .filter(l => l.ua)
                .fold(
                  myRouter ! BaseRequest(
                    self,
                    AgentLoginRequest(Some(agId.toLong), Some(phoneNumber))
                  )
                )(_ =>
                  myRouter ! WebSocketEvent
                    .createError(WSMsgType.AgentError, s"NotUaPosition")
                )
          }
        case None =>
          myRouter ! WebSocketEvent.createError(
            WSMsgType.AgentError,
            s"NotAnAgent"
          )
      }

    case AgentLogoutRequest(None) =>
      filterConfig.agentId match {
        case Some(agId) =>
          myRouter ! BaseRequest(self, AgentLogoutRequest(Some(agId.toLong)))
        case None =>
          myRouter ! WebSocketEvent.createError(
            WSMsgType.AgentError,
            s"NotAnAgent"
          )
      }

    case AgentPauseRequest(None, reason) =>
      filterConfig.agentId match {
        case Some(agId) =>
          myRouter ! BaseRequest(
            self,
            AgentPauseRequest(Some(agId.toLong), reason)
          )
        case None =>
          myRouter ! WebSocketEvent.createError(
            WSMsgType.AgentError,
            s"NotAnAgent"
          )
      }

    case AgentUnPauseRequest(None) =>
      filterConfig.agentId match {
        case Some(agId) =>
          myRouter ! BaseRequest(self, AgentUnPauseRequest(Some(agId.toLong)))
        case None =>
          myRouter ! WebSocketEvent.createError(
            WSMsgType.AgentError,
            s"NotAnAgent"
          )
      }
  }

  def processUserRequest: Receive = {
    case UserStatusUpdateReq(None, status) =>
      filterConfig.userId match {
        case Some(id) =>
          myRouter ! BaseRequest(self, UserStatusUpdateReq(Some(id), status))
        case None =>
      }

    case BaseRequest(ref, GetUserCallHistory(param)) =>
      injectedChild(
        callHistoryEnricherFactory(
          ref,
          personalContactRepo,
          param,
          user.username
        ),
        s"callHistoryEnricher$getNextActorId"
      )

    case BaseRequest(ref, GetUserCallHistoryByDays(param)) =>
      injectedChild(
        callHistoryEnricherFactory(
          ref,
          personalContactRepo,
          param,
          user.username
        ),
        s"callHistoryEnricher$getNextActorId"
      )
  }

  def mainReceive: Receive = {

    case ln: LoggedOn =>
      filterConfig = filterConfig.withUserId(ln.userId.toLong)
      log.info(s"LoggedOn Saving userId: ${ln.userId}")

      sender() ! WebSocketEvent.createLoggedOnEvent()
      sender() ! WebSocketEvent.createEventLegacy(
        configRepo.getCtiStatusesLegacy(ln.user.xivoUser.ctiProfileId.get).get
      )
      sender() ! WebSocketEvent.createEvent(
        configRepo.getCtiStatuses(ln.user.xivoUser.ctiProfileId.get).get
      )
      sender() ! RequestStatus(self, ln.userId.toInt, TypeUser)
      myRouter ! UserConfigUpdated(user.xivoUser.id, None)
      myRouter ! RequestConfig(
        self,
        GetUserConfig(user.xivoUser.id)
      )
      myRouter ! RequestConfig(
        self,
        GetUserServices(user.xivoUser.id.toInt)
      )
      userPreferenceService ! UserPreferencesInit(
        Some(user.xivoUser.id),
        sender()
      )
      eventBus.subscribe(self, XucEventBus.videoStatusTopic(ln.user.username))

    case ClientConnected(connectedActor, ln) =>
      log.info(
        s"client connected : ${ln.user} - ${ln.userId} - ${configRepo
          .getCtiStatusesLegacy(ln.user.xivoUser.ctiProfileId.get)
          .size} "
      )
      connectedActor ! WsContent(WebSocketEvent.createLoggedOnEvent())

      connectedActor ! WsContent(
        WebSocketEvent.createEventLegacy(
          configRepo.getCtiStatusesLegacy(ln.user.xivoUser.ctiProfileId.get).get
        )
      )
      connectedActor ! WsContent(
        WebSocketEvent.createEvent(
          configRepo.getCtiStatuses(ln.user.xivoUser.ctiProfileId.get).get
        )
      )
      myRouter ! RequestStatus(self, ln.userId.toInt, TypeUser)
      myRouter ! RequestConfig(
        self,
        GetUserConfig(user.xivoUser.id.toInt)
      )
      myRouter ! RequestConfig(
        self,
        GetUserServices(user.xivoUser.id.toInt)
      )

      filterConfig.lineConfig.foreach(lcf =>
        configRepo
          .getPhoneHintStatusByNumber(lcf.phoneNb)
          .foreach(phs => {
            connectedActor ! WsContent(WebSocketEvent.createEvent(phs))
          })
      )

      userPreferenceService ! ClientConnected(connectedActor, ln)

    case sheet: Sheet =>
      log.info(s"Sending Sheet : $sheet")
      myRouter ! WebSocketEvent.createEvent(sheet)

    case userConfigUpdate: UserConfigUpdate =>
      if (eventIsForMe(userConfigUpdate)) {
        userConfig = Some(userConfigUpdate)

        userConfig.foreach { config =>
          updateAgentId(config)
          updatePhoneId(config)
        }
        myRouter ! WebSocketEvent.createEvent(userConfigUpdate)
      }

    case userConfigUpdate: DeprecatedUserConfigUpdate =>
      log.debug(s"Deprecated : $userConfigUpdate")

    case userServicesUpdate: UserServicesUpdated =>
      log.debug(s"Processing UserServicesUpdated : $userServicesUpdate")
      processUserServicesUpdated(userServicesUpdate)

    case phoneStatusUpdate: PhoneStatusUpdate =>
      processPhoneStatusUpdate(phoneStatusUpdate)

    case phoneHintStatusEvent: PhoneHintStatusEvent =>
      processPhoneHintStatusEvent(phoneHintStatusEvent)

    case queueStatistics: QueueStatistics =>
      log.debug(
        s"processing queueStatistics : $queueStatistics received from ${sender()}"
      )
      sender() ! WebSocketEvent.createEvent(queueStatistics)

    case statEvent: AggregatedStatEvent =>
      log.debug(s"stat received : $statEvent")
      myRouter ! WebSocketEvent.createEvent(statEvent)

    case lcf: LineConfig =>
      filterConfig.lineConfig.foreach(lc => {
        log.debug(s"unsubscribe from ${lc.phoneNb} event")
        eventBus.unsubscribe(self, XucEventBus.phoneEventTopic(lc.phoneNb))
        eventBus.unsubscribe(self, XucEventBus.lineEventTopic(lc.phoneNb))
        eventBus.unsubscribe(self, XucEventBus.phoneHintEventTopic(lc.phoneNb))
      })

      eventBus.subscribe(self, XucEventBus.phoneEventTopic(lcf.phoneNb))
      eventBus.subscribe(self, XucEventBus.lineEventTopic(lcf.phoneNb))
      eventBus.subscribe(self, XucEventBus.phoneHintEventTopic(lcf.phoneNb))
      myRouter ! lcf
      filterConfig = filterConfig.withLineConfig(lcf)
      log.info(
        s"updated : phone id ${lcf.id} number ${lcf.phoneNb} line ${lcf.line}"
      )

    case jsonMessage: JsonNode => myRouter ! jsonMessage

    case agentState: AgentState => receiveAgentState(agentState)

    case AgentLoggingIn(agentId, loggingPhoneNumber) =>
      myRouter ! RequestConfig(self, LineConfigQueryByNb(loggingPhoneNumber))

    case AgentLoggingOut(agentId) =>
      myRouter ! BaseRequest(self, ReleaseAllCallbacks(agentId))

    case agentListen: AgentListen =>
      filterConfig.userId match {
        case Some(userId) =>
          myRouter ! agentListen.copy(fromUser = filterConfig.userId)
        case None =>
          log.error("Cannot listen valid userid not found")
          myRouter ! InvalidRequest(
            "Cannot listen valid userid not found",
            s"$agentListen"
          )
      }

    case agls: AgentListenStarted =>
      myRouter ! WebSocketEvent.createEvent(agls)

    case agls: AgentListenStopped =>
      myRouter ! WebSocketEvent.createEvent(agls)

    case phoneEvent: PhoneEvent =>
      log.info(s"$phoneEvent")
      myRouter ! WebSocketEvent.createEvent(phoneEvent)

    case videoEvent: UserVideoEvent =>
      log.info(s"$videoEvent")
      myRouter ! WebSocketEvent.createEvent(
        VideoStatusEvent(
          videoEvent.fromUser,
          UserVideoEvent.matchStatusToEvent(videoEvent.status)
        )
      )

    case e: WsConferenceEvent =>
      log.info(s"$e")
      myRouter ! WebSocketEvent.createEvent(e)

    case e: WsConferenceParticipantEvent =>
      log.debug(s"$e")
      myRouter ! WebSocketEvent.createEvent(e)

    case ccpe: CurrentCallsPhoneEvents =>
      log.info(s"$ccpe")
      myRouter ! WebSocketEvent.createEvent(ccpe)

    case _ =>
  }

  private def receiveAgentState(agentState: xivo.events.AgentState): Unit = {
    log.info(s"$agentState received from ${sender()}")
    currentAgentState = Some(agentState)
    myRouter ! WebSocketEvent.createEvent(agentState)

    if (!agentState.isInstanceOf[AgentLoggedOut]) {
      filterConfig.lineConfig match {
        case Some(lcf) =>
          if (lcf.phoneNb != agentState.phoneNb)
            myRouter ! RequestConfig(
              self,
              LineConfigQueryByNb(agentState.phoneNb)
            )
        case None =>
          myRouter ! RequestConfig(
            self,
            LineConfigQueryByNb(agentState.phoneNb)
          )
      }
    }
  }

  def getLoggedAgentPhoneNumber: Option[String] = {
    currentAgentState
      .filterNot(_.isInstanceOf[AgentLoggedOut])
      .map(_.phoneNb)
      .filterNot(_.isEmpty)
  }

  override def postStop(): Unit = {
    eventBus.unsubscribe(self, XucEventBus.userEventTopic(user.xivoUser.id))
    eventBus.unsubscribe(self, XucEventBus.videoStatusTopic(user.username))
    personalContactRepo ! PoisonPill
  }
}
