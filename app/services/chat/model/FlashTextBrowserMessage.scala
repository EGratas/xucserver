package services.chat.model

import play.api.libs.functional.syntax.*
import play.api.libs.json.*
import services.request.XucRequest

import java.time.OffsetDateTime

sealed trait BrowserRequest {
  def enrichWithSender(from: String): FlashTextRequest
}

case class BrowserSendDirectMessage(to: String, message: String, sequence: Long)
    extends BrowserRequest {
  def enrichWithSender(from: String): SendDirectMessage =
    SendDirectMessage(from, to, message, sequence)
}

case class BrowserGetDirectMessageHistory(to: String, sequence: Long)
    extends BrowserRequest {
  def enrichWithSender(from: String): GetDirectMessageHistory =
    GetDirectMessageHistory((from, to), sequence)
}

case class BrowserMarkAsReadDirectChannel(to: String, sequence: Long)
    extends BrowserRequest {
  def enrichWithSender(from: String): MarkDirectchannelAsRead =
    MarkDirectchannelAsRead((from, to), sequence)
}

case class BrowserRequestEnvelope(request: BrowserRequest) extends XucRequest
case class BrowserEventEnvelope(event: FlashTextEvent)

object BrowserRequestEnvelope {
  implicit val read: Reads[BrowserRequestEnvelope] = (JsPath \ "request")
    .read[String]
    .flatMap {
      case "FlashTextDirectMessage" =>
        BrowserSendDirectMessage.read.map[BrowserRequest](identity)
      case "FlashTextDirectMessageHistory" =>
        BrowserGetDirectMessageHistory.read.map[BrowserRequest](identity)
      case "FlashTextMarkAsRead" =>
        BrowserMarkAsReadDirectChannel.read.map[BrowserRequest](identity)
      case request =>
        Reads[BrowserRequest] { js =>
          JsError(s"Unknown FlashTextBrowserRequest $request !")
        }
    }
    .map(BrowserRequestEnvelope(_))

  def validate(json: JsValue): JsResult[BrowserRequestEnvelope] =
    json.validate[BrowserRequestEnvelope]
}

object BrowserSendDirectMessage {
  implicit val read: Reads[BrowserSendDirectMessage] = (
    (__ \ "to" \ "username").read[String] and
      (__ \ "message").read[String] and
      (__ \ "sequence").read[Long]
  )(BrowserSendDirectMessage.apply _)
}

object BrowserGetDirectMessageHistory {
  implicit val read: Reads[BrowserGetDirectMessageHistory] = (
    (__ \ "to" \ "username").read[String] and
      (__ \ "sequence").read[Long]
  )(BrowserGetDirectMessageHistory.apply _)
}

object BrowserMarkAsReadDirectChannel {
  implicit val read: Reads[BrowserMarkAsReadDirectChannel] = (
    (__ \ "to" \ "username").read[String] and
      (__ \ "sequence").read[Long]
  )(BrowserMarkAsReadDirectChannel.apply _)
}

object BrowserEventEnvelope {

  implicit val writeBEEnvelope: writeBEEnvelope = new writeBEEnvelope
  class writeBEEnvelope extends Writes[BrowserEventEnvelope] {
    implicit val ackWriter: OWrites[RequestAck] = (
      (JsPath \ "sequence").write[Long] and
        (JsPath \ "offline").write[Boolean] and
        (JsPath \ "date").write[OffsetDateTime]
    )(o => (o.sequence, o.offline, o.date))

    implicit val nackWriter: OWrites[RequestNack] = (
      (JsPath \ "sequence").write[Long]
    ).contramap((o) => o.sequence)

    implicit val messageWriter: OWrites[Message] = (m: Message) =>
      Json.obj(
        "from"     -> m.from,
        "to"       -> m.to,
        "sequence" -> m.sequence,
        "message"  -> m.message,
        "date"     -> m.date
      )

    implicit val historyWriter: OWrites[MessageHistory] = (
      (JsPath \ "users").write[Tuple2[ChatUser, ChatUser]] and
        (JsPath \ "messages").write[List[Message]] and
        (JsPath \ "sequence").write[Long]
    )(o => (o.users, o.messages, o.sequence))

    implicit val historyUnreadWriter: OWrites[MessageUnreadNotification] = (
      (JsPath \ "messages").write[List[Message]] and
        (JsPath \ "sequence").write[Long]
    )(o => (o.messages, o.sequence))

    implicit val messageMarkedAsReadWriter: OWrites[MessageMarkAsRead] = (
      (JsPath \ "username").write[String] and
        (JsPath \ "status").write[String] and
        (JsPath \ "sequence").write[Long]
    )(o => (o.username, o.status, o.sequence))

    def writes(o: BrowserEventEnvelope): JsValue = {
      o.event match {
        case r: RequestAck if r.offline =>
          Json.obj(
            "event" -> JsString("FlashTextSendMessageAckOffline")
          ) ++ Json.toJsObject(r)
        case r: RequestAck =>
          Json.obj("event" -> JsString("FlashTextSendMessageAck")) ++ Json
            .toJsObject(r)
        case r: RequestNack =>
          Json.obj("event" -> JsString("FlashTextSendMessageNack")) ++ Json
            .toJsObject(r)
        case m: Message =>
          Json.obj("event" -> JsString("FlashTextUserMessage")) ++ Json
            .toJsObject(m)
        case m: MessageHistory =>
          Json.obj("event" -> JsString("FlashTextUserMessageHistory")) ++ Json
            .toJsObject(m)
        case m: MessageUnreadNotification =>
          Json.obj("event" -> JsString("FlashTextUnreadMessages")) ++ Json
            .toJsObject(m)
        case m: MessageMarkAsRead =>
          Json.obj("event" -> JsString("FlashTextMarkedAsRead")) ++ Json
            .toJsObject(m)
      }
    }
  }
}
