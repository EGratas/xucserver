package services.chat

import java.util.NoSuchElementException

import com.google.inject.Inject
import models.XivoUser
import services.config.ConfigRepository
import services.chat.model.{ChatUser, Message}
import xivo.models._

import scala.concurrent.Future

class ChatUtil @Inject() (configRepo: ConfigRepository) {

  def mattermostOrderRecipients(
      userFrom: MattermostGuid,
      userTo: MattermostGuid
  ): List[MattermostDirectMessage] => List[Message] = {
    (posts: List[MattermostDirectMessage]) =>
      posts.map { p =>
        val (from, to) = {
          if (p.user_id == userFrom.guid)
            (
              ChatUser(
                userFrom.username,
                getPhoneNumber(userFrom.username),
                userFrom.displayName,
                Some(userFrom.guid)
              ),
              ChatUser(
                userTo.username,
                getPhoneNumber(userTo.username),
                userTo.displayName,
                Some(userTo.guid)
              )
            )
          else
            (
              ChatUser(
                userTo.username,
                getPhoneNumber(userTo.username),
                userTo.displayName,
                Some(userTo.guid)
              ),
              ChatUser(
                userFrom.username,
                getPhoneNumber(userFrom.username),
                userFrom.displayName,
                Some(userFrom.guid)
              )
            )
        }
        Message(from, to, p.message, p.offsetDateTime, 0)
      }
  }

  def mattermostOrderRecipients(
      m: MattermostUnreadNotification
  ): Future[Message] = {
    (for {
      xivoUserTo   <- configRepo.getCtiUser(m.userIdTo)
      xivoUserFrom <- configRepo.getCtiUser(m.usernameFrom)
    } yield {
      val (from, to) = m.direction match {
        case DirectionIsFromUser =>
          (
            ChatUser(
              m.usernameFrom,
              getPhoneNumber(m.usernameFrom),
              getDisplayName(xivoUserFrom),
              None
            ),
            ChatUser(
              xivoUserTo.username.get,
              getPhoneNumber(xivoUserTo.username.get),
              getDisplayName(xivoUserTo),
              None
            )
          )
        case DirectionIsToUser =>
          (
            ChatUser(
              xivoUserTo.username.get,
              getPhoneNumber(xivoUserTo.username.get),
              getDisplayName(xivoUserTo),
              None
            ),
            ChatUser(
              m.usernameFrom,
              getPhoneNumber(m.usernameFrom),
              getDisplayName(xivoUserFrom),
              None
            )
          )
      }
      Future.successful(
        Message(from, to, m.message.message, m.offsetDateTime, 0)
      )
    }).getOrElse(
      Future.failed(
        new NoSuchElementException(
          s"User ${m.userIdTo} not found in Mattermost."
        )
      )
    )
  }

  def getDisplayName(xivoUser: XivoUser): Option[String] =
    Some(
      List(xivoUser.firstname, xivoUser.lastname.getOrElse("")).mkString(" ")
    )
  def getPhoneNumber(username: String): Option[String] =
    configRepo.getLineForUser(username).flatMap(_.number)
}
