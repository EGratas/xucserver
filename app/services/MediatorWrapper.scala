package services

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.cluster.pubsub.DistributedPubSub

class MediatorWrapper {
  def getMediator(acSys: ActorSystem): ActorRef = {
    DistributedPubSub(acSys).mediator
  }
}
