package services

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.event.{ActorEventBus, SubchannelClassification}
import org.apache.pekko.util.Subclassification
import com.google.inject.{Inject, Singleton}
import models.{CallbackList, QueueCallList}
import services.XucEventBus.TopicType.TopicType
import services.XucEventBus.{Topic, XucEvent}
import services.agent.{AgentStatistic, AgentTransition}
import services.config.ObjectType
import services.config.ObjectType.ObjectType
import services.request.*
import services.video.model.VideoStatusEvent
import xivo.ami.AmiBusConnector.AgentListenNotification
import xivo.events.*
import xivo.models.{
  Agent,
  QueueConfigUpdate,
  UserConfigUpdate,
  UserServicesUpdated
}
import xivo.websocket.*

object XucEventBus {

  def configTopic(objectType: ObjectType): Topic =
    Topic(TopicType.Config, objectType)

  def allAgentsEventsTopic: Topic = Topic(TopicType.Event, ObjectType.TypeAgent)
  def agentEventTopic(agentId: Agent.Id): Topic =
    Topic(TopicType.Event, ObjectType.TypeAgent, Some(agentId))
  def lineEventTopic(lineNumber: String): Topic =
    Topic(TopicType.Event, ObjectType.TypeLine, Some(lineNumber))
  def allPhoneEventsTopic: Topic = Topic(TopicType.Event, ObjectType.TypePhone)
  def phoneEventTopic(phoneNumber: String): Topic =
    Topic(TopicType.Event, ObjectType.TypePhone, Some(phoneNumber))
  def currentCallsPhoneEvents(phoneNumber: String): Topic =
    Topic(TopicType.Event, ObjectType.TypePhone, Some(phoneNumber))
  def statEventTopic(agentId: Agent.Id): Topic =
    Topic(TopicType.Stat, ObjectType.TypeAgent, Some(agentId))
  def allAgentsStatsTopic: Topic = Topic(TopicType.Stat, ObjectType.TypeAgent)
  def agentTransitionTopic(agentId: Agent.Id): Topic =
    Topic(TopicType.Transition, ObjectType.TypeAgent, Some(agentId))
  def agentLogTransitionTopic(agentId: Agent.Id): Topic =
    Topic(TopicType.AgentLogTransition, ObjectType.TypeAgent, Some(agentId))
  def queueCallsTopic(queueId: Long): Topic =
    Topic(TopicType.Event, ObjectType.TypeQueue, Some(queueId))
  def queueConfigUpdateTopic: Topic =
    Topic(TopicType.Config, ObjectType.TypeQueue)
  def callbackListsTopic(): Topic =
    Topic(TopicType.Config, ObjectType.TypeCallback)
  def phoneHintEventTopic(phoneNumber: String): Topic =
    Topic(TopicType.Event, ObjectType.TypePhoneHint, Some(phoneNumber))
  def allPhoneHintEventTopic: Topic =
    Topic(TopicType.Event, ObjectType.TypePhoneHint)
  def userEventTopic(userId: Long): Topic =
    Topic(TopicType.Event, ObjectType.TypeUser, Some(userId))
  def videoStatusTopic(fromUser: String): Topic =
    Topic(TopicType.Event, ObjectType.TypeVideoStatusEvent, Some(fromUser))
  def allVideoStatusTopic: Topic =
    Topic(TopicType.Event, ObjectType.TypeVideoStatusEvent)

  case class XucEvent(val topic: Topic, val message: Any)

  object TopicType extends Enumeration {
    type TopicType = Value
    val Event: XucEventBus.TopicType.Value              = Value
    val Transition: XucEventBus.TopicType.Value         = Value
    val Config: XucEventBus.TopicType.Value             = Value
    val Stat: XucEventBus.TopicType.Value               = Value
    val AgentLogTransition: XucEventBus.TopicType.Value = Value
  }
  case class Topic(
      topicType: TopicType,
      objectType: ObjectType,
      objectId: Option[Any] = None
  )
}

@Singleton
class XucEventBus @Inject() (implicit val system: ActorSystem)
    extends ActorEventBus
    with SubchannelClassification
    with EventBusSupervision {
  type Event      = XucEvent
  type Classifier = Topic

  protected def subclassification: Subclassification[Classifier] =
    new Subclassification[Classifier] {
      def isEqual(x: Classifier, y: Classifier): Boolean = x == y
      def isSubclass(x: Classifier, y: Classifier): Boolean = {
        x.topicType == y.topicType && x.objectType == y.objectType && (x.objectId != None && y.objectId == None) || x == y
      }
    }

  override def classify(event: Event): Topic = event.topic
  override def publish(event: Event): Unit   = super.publish(event)
  override def subscribe(subscriber: Subscriber, to: Classifier): Boolean =
    super.subscribe(subscriber, to)
  override def unsubscribe(subscriber: Subscriber, from: Classifier): Boolean =
    super.unsubscribe(subscriber, from)
  override def unsubscribe(subscriber: Subscriber): Unit =
    super.unsubscribe(subscriber)

  protected def publish(event: Event, subscriber: Subscriber): Unit =
    subscriber ! event.message

  def publish(agentState: AgentState): Unit =
    publish(
      XucEvent(XucEventBus.agentEventTopic(agentState.agentId), agentState)
    )

  def publish(agentError: AgentError): Unit =
    publish(XucEvent(XucEventBus.allAgentsEventsTopic, agentError))

  def publish(agentListenStarted: AgentListenNotification): Unit =
    publish(
      XucEvent(
        XucEventBus.lineEventTopic(agentListenStarted.phoneNumber),
        agentListenStarted
      )
    )

  def publish(agentStatistic: AgentStatistic): Unit =
    publish(
      XucEvent(
        XucEventBus.statEventTopic(agentStatistic.agentId),
        agentStatistic
      )
    )

  def publish(agentTransition: AgentTransition, agentId: Agent.Id): Unit =
    publish(
      XucEvent(XucEventBus.agentTransitionTopic(agentId), agentTransition)
    )

  def publish(queueCalls: QueueCallList): Unit =
    publish(
      XucEvent(XucEventBus.queueCallsTopic(queueCalls.queueId), queueCalls)
    )

  def publish(queueConfig: QueueConfigUpdate): Unit =
    publish(XucEvent(XucEventBus.queueConfigUpdateTopic, queueConfig))

  def publish(agent: Agent): Unit =
    publish(XucEvent(XucEventBus.agentEventTopic(agent.id), agent))

  def publish(phoneEvent: PhoneEvent): Unit =
    publish(XucEvent(XucEventBus.phoneEventTopic(phoneEvent.DN), phoneEvent))

  def publish(ccpe: CurrentCallsPhoneEvents): Unit =
    publish(XucEvent(XucEventBus.phoneEventTopic(ccpe.DN), ccpe))

  def publish(evt: WsConferenceEvent): Unit =
    publish(XucEvent(XucEventBus.phoneEventTopic(evt.phoneNumber), evt))

  def publish(evt: WsConferenceParticipantEvent): Unit =
    publish(XucEvent(XucEventBus.phoneEventTopic(evt.phoneNumber), evt))

  def publish(callbacks: List[CallbackList]): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbacks))

  def publish(callbackTaken: CallbackTaken): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbackTaken))

  def publish(callbackReleased: CallbackReleased): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbackReleased))

  def publish(callbackClotured: CallbackClotured): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbackClotured))

  def publish(callbackUpdated: CallbackRequestUpdated): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbackUpdated))

  def publish(e: PhoneHintStatusEvent): Unit =
    publish(XucEvent(XucEventBus.phoneHintEventTopic(e.number), e))

  def publish(e: AgentLogTransition): Unit =
    publish(XucEvent(XucEventBus.agentLogTransitionTopic(e.agentId), e))

  def publish(e: UserServicesUpdated): Unit =
    publish(XucEvent(XucEventBus.userEventTopic(e.userId), e))

  def publish(e: UserConfigUpdate): Unit =
    publish(XucEvent(XucEventBus.userEventTopic(e.userId), e))

  def publish(e: VideoStatusEvent): Unit =
    publish(XucEvent(XucEventBus.videoStatusTopic(e.fromUser), e))
}
