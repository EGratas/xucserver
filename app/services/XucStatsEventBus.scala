package services

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.event.ActorEventBus
import org.apache.pekko.event.SubchannelClassification
import org.apache.pekko.util.Subclassification
import com.google.inject.{Inject, Singleton}
import stats.Statistic.RequestStat
import xivo.models.XivoObject.ObjectDefinition
import org.apache.pekko.actor.ActorRef
import play.api.Logger
import play.api.libs.functional.syntax._
import play.api.libs.json._

object XucStatsEventBus {
  trait StatEvent {
    val xivoObject: ObjectDefinition
    val stat: List[Stat]
  }
  case class Stat(statRef: String, value: Double)
  object Stat {
    implicit val statWrites: OWrites[Stat] =
      ((__ \ "statName").write[String] and
        (__ \ "value").write[Double])(s => (s.statRef, s.value))
  }
  case class AggregatedStatEvent(
      override val xivoObject: ObjectDefinition,
      override val stat: List[Stat]
  ) extends StatEvent
  case class StatUpdate(
      override val xivoObject: ObjectDefinition,
      override val stat: List[Stat]
  ) extends StatEvent

  object AggregatedStatEvent {
    implicit val AggregatedStatEventWrites: OWrites[AggregatedStatEvent] =
      ((__ \ "queueId").write[Long] and
        (__ \ "counters").write[List[Stat]])(
        unlift(AggregatedStatEvent.extract)
      )

    def extract(ags: AggregatedStatEvent): Option[(Long, List[Stat])] =
      Some((ags.xivoObject.objectRef.get.toLong, ags.stat))
  }
}

@Singleton
class XucStatsEventBus @Inject() (implicit val system: ActorSystem)
    extends ActorEventBus
    with SubchannelClassification
    with EventBusSupervision {
  type Event      = XucStatsEventBus.StatEvent
  type Classifier = ObjectDefinition
  val logger: Logger = Logger(getClass.getName)

  private var statCache: Option[ActorRef] = None

  def setStatCache(statCache: ActorRef): Unit = this.statCache = Some(statCache)

  override def publish(event: Event): Unit = super.publish(event)
  override def unsubscribe(subscriber: Subscriber, from: Classifier): Boolean =
    super.unsubscribe(subscriber, from)
  override def unsubscribe(subscriber: Subscriber): Unit =
    super.unsubscribe(subscriber)

  protected def subclassification: Subclassification[Classifier] =
    new Subclassification[Classifier] {
      def isEqual(x: Classifier, y: Classifier): Boolean = x == y
      def isSubclass(x: Classifier, y: Classifier): Boolean =
        x.objectType == y.objectType && (!y.hasObjectRef)
    }

  override def classify(event: Event): ObjectDefinition = event.xivoObject

  protected def publish(event: Event, subscriber: Subscriber): Unit =
    subscriber ! event

  override def subscribe(subscriber: Subscriber, to: Classifier): Boolean = {
    logger.debug(s"subscribe : $subscriber to : $to")
    statCache.foreach(_ ! RequestStat(subscriber, to))
    super.subscribe(subscriber, to)
  }
}
