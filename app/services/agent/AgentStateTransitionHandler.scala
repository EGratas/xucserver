package services.agent

import services.calltracking.SingleDeviceTracker.{MonitorCalls, UnMonitorCalls}
import services.config.ConfigDispatcher.LineConfigQueryByNb
import services.AgentStateFSM
import services.AgentStateFSM._
import xivo.events.{AgentLoggingIn, AgentLoggingOut, AgentState}
import xivo.events.AgentState._

trait AgentStateTHandler {
  this: AgentStateFSM =>

  def whenLoggingIn(context: MContext): Unit
  def whenLoggingOut(context: MContext): Unit
  def publishState(state: MAgentStates, context: MContext): Unit
  def publishTransition(
      from: MAgentStates,
      fromContext: MContext,
      to: MAgentStates,
      toContext: MContext
  ): Unit

}
trait AgentStateProdTHandler extends AgentStateTHandler {
  this: AgentStateFSM =>

  def getInterface(data: MContext): Option[String] =
    data match {
      case AgentStateContext(_, Some(phoneNb), _, _, _) =>
        for {
          lineConfig <-
            configRepository.getLineConfig(LineConfigQueryByNb(phoneNb))
          line <- lineConfig.line
          interface = line.interface
        } yield interface
      case _ => None
    }

  def whenLoggingIn(context: MContext): Unit = {
    getInterface(context).foreach(devicesTracker ! MonitorCalls(_))
    context match {
      case AgentStateContext(_, Some(phoneNb), _, _, _) =>
        eventBus.publish(AgentLoggingIn(agentId, phoneNb))
      case _ =>
    }

  }

  def whenLoggingOut(context: MContext): Unit = {
    getInterface(context).foreach(devicesTracker ! UnMonitorCalls(_))
    eventBus.publish(AgentLoggingOut(agentId))
  }

  private def createAgentStateEvent(
      state: MAgentStates,
      context: MContext
  ): Option[AgentState] = {
    context match {
      case AgentStateContext(cause, phoneNb, currentCall, queues, since) =>
        val agentPhoneNb = phoneNb.getOrElse("")
        state match {
          case MAgentLoggedOut =>
            Some(
              AgentLoggedOut(
                agentId,
                since,
                agentPhoneNb,
                queues,
                agentNumber,
                None
              )
            )
          case MAgentReady if currentCall.isEmpty =>
            Some(
              AgentReady(
                agentId,
                since,
                agentPhoneNb,
                queues,
                None,
                agentNumber
              )
            )
          case MAgentPaused if currentCall.isEmpty =>
            Some(
              AgentOnPause(
                agentId,
                since,
                agentPhoneNb,
                queues,
                cause,
                agentNumber
              )
            )
          case MAgentOnWrapup if currentCall.isEmpty =>
            Some(
              AgentOnWrapup(
                agentId,
                since,
                agentPhoneNb,
                queues,
                cause,
                agentNumber
              )
            )
          case _ if currentCall.isDefined =>
            currentCall match {
              case Some(
                    AgentCall(AgentCallState.OnCall, acd, direction, monitor)
                  ) =>
                Some(
                  AgentOnCall(
                    agentId,
                    since,
                    acd,
                    direction,
                    agentPhoneNb,
                    queues,
                    state.isPause,
                    cause,
                    monitor,
                    agentNumber
                  )
                )
              case Some(AgentCall(AgentCallState.Ringing, acd, direction, _)) =>
                Some(
                  AgentRinging(
                    agentId,
                    since,
                    acd,
                    agentPhoneNb,
                    queues,
                    cause,
                    agentNumber
                  )
                )
              case Some(AgentCall(AgentCallState.Dialing, acd, direction, _)) =>
                Some(
                  AgentDialing(
                    agentId,
                    since,
                    agentPhoneNb,
                    queues,
                    cause,
                    agentNumber
                  )
                )
              case _ => None
            }
          case _ => None
        }
      case _ =>
        state match {
          case MAgentLoggedOut =>
            Some(
              AgentLoggedOut(
                agentId,
                context.lastChanged,
                "",
                List.empty,
                agentNumber,
                None
              )
            )
          case _ => None
        }
    }

  }

  def publishState(state: MAgentStates, context: MContext): Unit = {
    createAgentStateEvent(state, context)
      .filter(!previousAgentStateEvent.contains(_))
      .foreach(evt => {
        log.debug(s"Publishing Agent $agentId in state $state with $context")
        previousAgentStateEvent = Some(evt)
        eventBus.publish(evt)
      })
  }

  def publishTransition(
      from: MAgentStates,
      fromContext: MContext,
      to: MAgentStates,
      toContext: MContext
  ): Unit = {
    eventBus.publish(AgentTransition(from, fromContext, to, toContext), agentId)
  }
}
