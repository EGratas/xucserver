package services.agent

import org.apache.pekko.actor.{ActorRef, Props}
import services.agent.AgentInGroupAction.AgentsDestination
import services.config.ConfigDispatcher._
import services.request.SetAgentQueue
import xivo.models.Agent

object AgentFromGroupNotInQueueAdder {
  def props(
      groupId: Long,
      queueId: Long,
      penalty: Int,
      configDispatcher: ActorRef
  ): Props =
    Props(
      new AgentFromGroupNotInQueueAdder(
        groupId,
        queueId,
        penalty,
        configDispatcher
      )
    )
}

class AgentFromGroupNotInQueueAdder(
    groupId: Long,
    queueId: Long,
    penalty: Int,
    configDispatcher: ActorRef
) extends AgentInGroupAction(groupId, queueId, penalty, configDispatcher) {

  override def receive: Receive = {
    case AgentsDestination(toQueueId, toPenalty) =>
      configDispatcher ! RequestConfig(
        self,
        GetAgentsNotInQueue(groupId, queueId)
      )
      context.become(forAllAgents(Some(toQueueId), Some(toPenalty)))
  }

  override def actionOnAgent(
      agent: Agent,
      toQueueId: Option[Long],
      toPenalty: Option[Int]
  ): Unit = {
    configDispatcher ! ConfigChangeRequest(
      self,
      SetAgentQueue(agent.id, toQueueId.get, toPenalty.get)
    )
  }
}
