package services.agent

import org.apache.pekko.actor.{ActorRef, Props}
import services.config.ConfigDispatcher._
import services.request.{RemoveAgentFromQueue, SetAgentQueue}
import xivo.models.Agent

object AgentInGroupMover {
  def props(
      groupId: Long,
      fromQueueId: Long,
      fromPenalty: Int,
      configDispatcher: ActorRef
  ): Props =
    Props(
      new AgentInGroupMover(groupId, fromQueueId, fromPenalty, configDispatcher)
    )
}

class AgentInGroupMover(
    groupId: Long,
    fromQueueId: Long,
    fromPenalty: Int,
    configDispatcher: ActorRef
) extends AgentInGroupAction(
      groupId,
      fromQueueId,
      fromPenalty,
      configDispatcher
    ) {

  def actionOnAgent(
      agent: Agent,
      toQueueId: Option[Long],
      toPenalty: Option[Int]
  ): Unit = {
    configDispatcher ! ConfigChangeRequest(
      self,
      SetAgentQueue(agent.id, toQueueId.get, toPenalty.get)
    )
    if (toQueueId.get != fromQueueId)
      configDispatcher ! ConfigChangeRequest(
        self,
        RemoveAgentFromQueue(agent.id, fromQueueId)
      )
  }
}
