package services.agent

import org.apache.pekko.actor.{Actor, ActorRef}
import com.google.inject.Inject
import com.google.inject.name.Named
import services.ActorIds
import services.config.ConfigDispatcher.RefreshAgent
import services.request.SetAgentGroup
import xivo.models.AgentFactory

class AgentGroupSetter @Inject() (
    agent: AgentFactory,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef
) extends Actor {

  override def receive: Receive = { case SetAgentGroup(agentId, groupId) =>
    agent.moveAgentToGroup(agentId, groupId)
    configDispatcher ! RefreshAgent(agentId)
  }
}
