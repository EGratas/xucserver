package services.agent

import org.apache.pekko.actor.{ActorRef, Props}
import services.config.ConfigDispatcher.{ConfigChangeRequest}
import services.request.RemoveAgentFromQueue
import xivo.models.Agent

object AgentGroupRemover {
  def props(
      groupId: Long,
      queueId: Long,
      penalty: Int,
      configDispatcher: ActorRef
  ): Props = Props(
    new AgentGroupRemover(groupId, queueId, penalty, configDispatcher)
  )
}

class AgentGroupRemover(
    groupId: Long,
    queueId: Long,
    penalty: Int,
    configDispatcher: ActorRef
) extends AgentInGroupAction(groupId, queueId, penalty, configDispatcher) {
  override def actionOnAgent(
      agent: Agent,
      toQueueId: Option[Long],
      penalty: Option[Int]
  ): Unit = {
    configDispatcher ! ConfigChangeRequest(
      self,
      RemoveAgentFromQueue(agent.id, queueId)
    )
  }
}
