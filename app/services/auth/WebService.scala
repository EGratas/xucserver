package services.auth

import org.apache.pekko.actor.ActorRef
import com.google.inject.Inject
import com.google.inject.name.Named
import models.Token
import models.ws.auth._
import play.api.Logger
import services.ActorIds
import xivo.services.XivoAuthentication
import xivo.xuc.XucBaseConfig

import scala.concurrent.Future
import controllers.helpers.AuthenticatedAction

class WebService @Inject() (
    @Named(ActorIds.XivoAuthenticationId) xivoAuthentication: ActorRef,
    config: XucBaseConfig
) {
  val log: Logger = Logger(getClass.getName)

  def authenticate(
      login: String,
      password: String,
      expiration: Int
  ): Future[Token] = {
    if (login.isEmpty) {
      Future.failed(
        new AuthenticationException(
          AuthenticationError.InvalidCredentials,
          "Invalid credentials"
        )
      )
    } else {
      log.info(s"Checking credentials for webservice user $login")
      XivoAuthentication
        .getWebServiceTokenHelper(
          xivoAuthentication,
          login,
          password,
          expiration
        )
    }
  }

  def getAuthenticationInformation(
      login: String,
      token: Token
  ): AuthenticationInformation = {
    AuthenticationInformation(
      login,
      token.expiresAt.getMillis / 1000,
      token.issuedAt.getMillis / 1000,
      AuthenticatedAction.webServiceUserType,
      token.acls,
      None
    )
  }

  def encodeToJWT(
      authenticationInformation: AuthenticationInformation
  ): String = {
    log.debug(s"Encoding JWT token")
    AuthenticationInformation.encode(
      authenticationInformation,
      config.Authentication.secret
    )
  }
}
