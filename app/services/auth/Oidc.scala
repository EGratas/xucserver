package services.auth

import org.apache.pekko.actor.ActorRef
import cats.data.Validated._
import cats.data.ValidatedNec
import cats.implicits._
import helpers.OptionToTry.OptionToTry
import models.XivoUser
import models.ws.auth.SoftwareType.SoftwareType
import models.ws.auth.{
  AuthenticationError,
  AuthenticationException,
  AuthenticationFailure,
  AuthenticationInformation
}
import pdi.jwt.{JwtAlgorithm, JwtJson, JwtOptions}
import play.api.libs.json.JsObject
import play.api.libs.ws.WSClient
import services.config.ConfigRepository
import xivo.xuc.{OidcSsoConfig, XucBaseConfig}

import scala.annotation.unused
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.util.{Failure, Success, Try}
import controllers.helpers.AuthenticatedAction
import controllers.helpers.XivoAuthenticationHelper.getXivoAuthToken

trait Oidc {

  type Token           = String
  type OidcErrorsOr[A] = ValidatedNec[AuthenticationFailure, A]

  val waitResult: FiniteDuration = 10.seconds

  private def getPublicKey(wsUrl: String, ws: WSClient): Try[String] = {
    val f = ws
      .url(wsUrl)
      .get()
      .map { response =>
        (response.json \ "public_key").as[String]
      }
      .recover({ case _ =>
        throw new AuthenticationException(
          AuthenticationError.OidcAuthenticationFailed,
          "Open Id Connect public key to deserialize token can't be retrieved"
        )
      })
    Success(Await.result(f, waitResult))
  }

  private def checkForMainOIDCConfig(
      config: Option[String],
      error: String
  ): Try[String] = {
    config match {
      case Some(value) => Success(value)
      case None =>
        Failure(
          new AuthenticationException(
            AuthenticationError.OidcAuthenticationFailed,
            error
          )
        )
    }
  }

  private def getLoginFromToken(
      token: String,
      config: OidcSsoConfig,
      oidcMainClientID: String,
      oidcMainURL: String,
      ws: WSClient
  ): Try[String] = {
    def tokenError[E](s: String)(@unused e: E) =
      AuthenticationFailure(AuthenticationError.InvalidToken, s).invalidNec

    def validate(content: JsObject): Try[JsObject] = {
      val getIssuer: OidcErrorsOr[String] = (content \ "iss")
        .validate[String]
        .fold(tokenError("Unable to get issuer from token"), Valid(_))
      val getAudience: OidcErrorsOr[List[String]] = (content \ "aud")
        .validate[List[String]]
        .fold(tokenError("Unable to get audience from token"), Valid(_))
      val getOneAudience: OidcErrorsOr[List[String]] = (content \ "aud")
        .validate[String]
        .fold(
          tokenError("Unable to get audience from token"),
          s => Valid(List(s))
        )
      val getExpiration: OidcErrorsOr[Long] = (content \ "exp")
        .validate[Long]
        .fold(tokenError("Unable to get expiration from token"), Valid(_))
      val getClientId: OidcErrorsOr[String] = (content \ "azp")
        .validate[String]
        .fold(tokenError("Unable to get authorized party from token"), Valid(_))

      def validateIssuer(tokenIssuer: String): OidcErrorsOr[String] = {

        val issuers        = config.oidcAdditionalTrustedServersUrl :+ oidcMainURL
        val matchingIssuer = issuers.find(_.equals(tokenIssuer))

        if (matchingIssuer.nonEmpty)
          Valid(tokenIssuer)
        else
          AuthenticationFailure(
            AuthenticationError.InvalidToken,
            s"Issuer $tokenIssuer not present in issuers list ${issuers.toString()}"
          ).invalidNec
      }

      def validateIssuerSignature(
          issuer: String,
          token: String,
          ws: WSClient
      ): OidcErrorsOr[String] = {
        getPublicKey(issuer, ws) match {
          case Success(pubKey) =>
            JwtJson.decodeJson(token, pubKey, Seq(JwtAlgorithm.RS256)) match {
              case Failure(_) =>
                throw new AuthenticationException(
                  AuthenticationError.OidcAuthenticationFailed,
                  "Token signature does not work with the issuer public key"
                )
              case Success(value) => Valid(value.toString())
            }
          case Failure(_) =>
            throw new AuthenticationException(
              AuthenticationError.OidcAuthenticationFailed,
              "Could not get issuer public key"
            )
        }
      }

      def validateClientId(clientId: String): OidcErrorsOr[String] = {

        val clientIDs =
          config.oidcAdditionalTrustedClientIDs :+ oidcMainClientID
        val matchingClientID = clientIDs.find(_.equals(clientId))

        if (matchingClientID.nonEmpty)
          Valid(clientId)
        else
          AuthenticationFailure(
            AuthenticationError.InvalidToken,
            s"Client id $clientId not present in client IDs list ${clientIDs.toString()}"
          ).invalidNec
      }

      def validateAudience(audience: List[String]): OidcErrorsOr[List[String]] =
        if (config.oidcAudience.intersect(audience).nonEmpty)
          Valid(audience)
        else
          AuthenticationFailure(
            AuthenticationError.InvalidToken,
            s"The list of audience $audience from the token does not contain any authorized audience from configured audiences ${config.oidcAudience}"
          ).invalidNec

      def validateExpiration(expiration: Long): OidcErrorsOr[Long] =
        if (System.currentTimeMillis() <= expiration * 1000)
          Valid(expiration)
        else
          AuthenticationFailure(
            AuthenticationError.InvalidToken,
            s"Token expired at $expiration"
          ).invalidNec

      val getAndCheckIssuer: OidcErrorsOr[String] =
        getIssuer
          .andThen(validateIssuer)
          .andThen(validateIssuerSignature(_, token, ws))

      getAndCheckIssuer *>
        getClientId.andThen(validateClientId) *>
        (getAudience <+> getOneAudience).andThen(validateAudience) *>
        getExpiration.andThen(validateExpiration) match {
        case Valid(_) => Success(content)
        case Invalid(errors) =>
          Failure(
            new AuthenticationException(
              AuthenticationError.OidcAuthenticationFailed,
              errors.map(_.message).mkString_(", ")
            )
          )
      }
    }

    JwtJson
      .decodeJson(token, JwtOptions(signature = false))
      .flatMap(validate _)
      .map(content =>
        (content \ config.oidcUsernameField.getOrElse("preferred_username"))
          .as[String]
      )
      .recoverWith({ case t =>
        Failure(
          new AuthenticationException(
            AuthenticationError.InvalidToken,
            t.getMessage
          )
        )
      })
  }

  def authenticate(
      token: Option[String],
      config: XucBaseConfig,
      repo: ConfigRepository,
      ws: WSClient,
      softwareType: Option[SoftwareType]
  )(implicit
      xivoAuthentication: ActorRef
  ): Try[(AuthenticationInformation, XivoUser)] = {

    if (!config.oidcEnable) {
      Failure(
        new AuthenticationException(
          AuthenticationError.OidcNotEnabled,
          "OpenId Connect authentication is not enabled"
        )
      )
    } else {
      token
        .map(t => {
          val now = AuthenticatedAction.now()
          for {
            oidcMainURL <- checkForMainOIDCConfig(
              config.oidcServerUrl,
              "OIDC server main URL is not set"
            )
            oidcMainClientID <- checkForMainOIDCConfig(
              config.oidcClientId,
              "OIDC server main client ID is not set"
            )
            login <- getLoginFromToken(
              t,
              config,
              oidcMainClientID,
              oidcMainURL,
              ws
            )
            xivoUser <-
              repo
                .getCtiUser(login)
                .toTry(
                  new AuthenticationException(
                    AuthenticationError.UserNotFound,
                    "Matching XiVO User not found"
                  )
                )
            token <- getXivoAuthToken(xivoUser.id, config).toTry(
              new AuthenticationException(
                AuthenticationError.UserNotFound,
                s"XiVO auth token cannot be retrieved for userId ${xivoUser.id}"
              )
            )
          } yield (
            AuthenticationInformation(
              login,
              now + config.Authentication.expires,
              now,
              AuthenticatedAction.ctiUserType,
              AuthenticatedAction.getAliasesFromAcls(token.acls),
              softwareType
            ),
            xivoUser
          )
        })
        .getOrElse {
          Failure(
            new AuthenticationException(
              AuthenticationError.OidcInvalidParameter,
              "Required access token parameter is not set"
            )
          )
        }
    }
  }
}

object OidcImpl extends Oidc {}
