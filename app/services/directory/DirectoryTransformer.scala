package services.directory

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import com.google.inject.Inject
import com.google.inject.name.Named
import models.{DirSearchResult, RichDirectoryResult, RichFavorites}
import services.ActorIds
import services.config.ConfigDispatcher.{MonitorPhoneHint, MonitorVideoStatus}
import services.directory.DirectoryTransformer.{
  EnrichDirectoryResult,
  RawDirectoryResult
}
import xivo.services.XivoDirectory.{
  DirLookupResult,
  FavoriteUpdated,
  Favorites,
  XivoDirectoryMsg
}
import xivo.websocket.WebSocketEvent
import xivo.websocket.WsBus.WsContent

object DirectoryTransformer {
  final val serviceName = "DirectoryTransformer"
  case class RawDirectoryResult(
      requester: ActorRef,
      result: XivoDirectoryMsg,
      requesterUserId: Long
  )
  case class EnrichDirectoryResult(
      dirResult: DirSearchResult,
      requesterUserId: Long
  )
}

class DirectoryTransformer @Inject() (
    transformer: DirectoryTransformerLogic,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef
) extends Actor
    with ActorLogging {

  def receive: Receive = {
    case RawDirectoryResult(ref, result: DirLookupResult, requesterUserId) =>
      val response: RichDirectoryResult =
        transformer.enrichDirResult(result.result, requesterUserId)
      log.debug(s"RichDirectoryResult: ${response.headers} ${response.entries}")
      configDispatcher ! MonitorPhoneHint(
        ref,
        transformer.getPhoneNumbers(result.result.results)
      )
      configDispatcher ! MonitorVideoStatus(
        ref,
        transformer.getUsernames(response.entries)
      )
      ref ! WsContent(WebSocketEvent.createEvent(response))
    case RawDirectoryResult(ref, result: Favorites, requesterUserId) =>
      val response: RichFavorites =
        transformer.enrichFavorites(result.result, requesterUserId)
      log.debug(s"RichDirectoryResult: ${response.headers} ${response.entries}")
      configDispatcher ! MonitorVideoStatus(
        ref,
        transformer.getUsernames(response.entries)
      )
      configDispatcher ! MonitorPhoneHint(
        ref,
        transformer.getPhoneNumbers(result.result.results)
      )
      ref ! WsContent(WebSocketEvent.createEvent(response))
    case RawDirectoryResult(ref, result: FavoriteUpdated, _) =>
      log.debug(s"XivoDirectoryResult: $result")
      ref ! WsContent(WebSocketEvent.createEvent(result))
    case EnrichDirectoryResult(result: DirSearchResult, requesterUserId) =>
      val response: RichDirectoryResult = {
        transformer.enrichDirResult(result, requesterUserId)
      }
      log.debug(
        s"Enrich RichDirectoryResult: ${response.headers} ${response.entries}"
      )
      sender() ! response
    case any =>
      log.debug(s"Received unprocessed message: $any")
  }
}
