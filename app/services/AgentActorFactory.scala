package services

import org.apache.pekko.actor._
import com.google.inject.Inject
import com.google.inject.name.Named
import play.api.libs.concurrent.InjectedActorSupport
import services.config.ConfigRepository
import xivo.models.Agent
import services.agent.AgentStatCollector
import xivo.phonedevices.QueueStatusCommand

import scala.collection.mutable.{Map => MMap}

class AgentActorFactory @Inject() (
    configRepository: ConfigRepository,
    @Named(ActorIds.DevicesTrackerId) devicesTracker: ActorRef,
    xucEventBus: XucEventBus,
    agentStatCollectorFactory: AgentStatCollector.Factory,
    @Named(ActorIds.AmiBusConnectorId) amiBusConnector: ActorRef
) extends InjectedActorSupport {

  val agFsms: MMap[Agent.Id, ActorRef]           = MMap()
  val agStatCollectors: MMap[Agent.Id, ActorRef] = MMap()

  private def startAgentStateFSM(
      id: Agent.Id,
      agentNumber: Agent.Number,
      context: ActorContext
  ): ActorRef =
    context.actorOf(
      AgentStateFSM
        .props(id, agentNumber, configRepository, devicesTracker, xucEventBus),
      s"AgentStateFSM$id"
    )
  private def createStatCollector(
      id: Agent.Id,
      context: ActorContext
  ): ActorRef =
    injectedChild(agentStatCollectorFactory(id), s"AgentStatCollector$id")(
      context
    )

  private def create(
      id: Agent.Id,
      agentNumber: Agent.Number,
      context: ActorContext
  ) = {
    agFsms += (id -> startAgentStateFSM(id, agentNumber, context))
    amiBusConnector ! QueueStatusCommand(s"Agent/$agentNumber")
    agFsms(id)
  }

  def getOrCreate(
      id: Agent.Id,
      agentNumber: Agent.Number,
      context: ActorContext
  ): ActorRef = {
    agFsms get id match {
      case Some(actorRef) => actorRef
      case None           => create(id, agentNumber, context)
    }
  }

  def getOrCreateAgentStatCollector(
      id: Agent.Id,
      context: ActorContext
  ): ActorRef = {
    agStatCollectors get id match {
      case Some(actorRef) => actorRef
      case None =>
        agStatCollectors += (id -> createStatCollector(id, context))
        agStatCollectors(id)
    }
  }
  def get(id: Agent.Id): Option[ActorRef] = agFsms.get(id)
}
