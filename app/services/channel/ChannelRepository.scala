package services.channel

import play.api.Logger
import services.channel.ChannelManager.Channels
import xivo.xucami.models.{CallerId, Channel, ChannelState}

trait ChannelMatchers {
  val logger: Logger                  = Logger(getClass.getName)
  def upChans(chan: Channel): Boolean = chan.state == ChannelState.UP
  def myCallerIdExceptAgentCallback(extension: String, chan: Channel): Boolean =
    chan.callerId.number == extension && !chan.isAgentCallback
  private def isMyInterface(name: String, interface: String): Boolean =
    name.startsWith(interface) && !name.contains("trunk")
  def myChans(interface: String, chans: List[Channel]): List[Channel] =
    chans filter (chan => isMyInterface(chan.name, interface))
  def myPhoneChans(phoneNb: String, chans: List[Channel]): List[Channel] =
    chans filter (chan => chan.callerId.number == phoneNb)
  def onlyOneChannel(chans: List[Channel]): Option[Channel] =
    if (chans.size == 1) chans.headOption else None
  def allButOnHold(chans: List[Channel]): List[Channel] =
    chans filter (_.state != ChannelState.HOLD)
}

trait HangupFinder {
  this: ChannelRepository with ChannelMatchers =>

  private def selectToHangup(
      interface: String,
      chans: List[Channel]
  ): List[Channel] =
    onlyOneChannel(chans) match {
      case Some(channel) => List(channel)
      case _             => chans filter upChans
    }
  def toHangup(interface: String, extension: String): List[Channel] = {
    if (log.isDebugEnabled)
      log.debug(s"${myChans(interface, channels.values.toList)}")
    selectToHangup(
      interface,
      allButOnHold(myChans(interface, channels.values.toList))
    )
  }
}

trait ChannelFinder {
  this: ChannelRepository =>
  def byAgentNumber(agentNumber: Option[String]): Option[Channel] =
    channels.values find (_.agentNumber.equals(agentNumber))

}

trait XferFinder {
  this: ChannelRepository with ChannelMatchers =>

  def upChannel(channel: Option[Channel]): Option[Channel] =
    channel filter (_.state == ChannelState.UP)

  def upChannels(channels: List[Channel]): List[Channel] =
    channels filter (_.state == ChannelState.UP)

  def toXfer(interface: String): Option[Channel] = {

    def onlyOneChan(chans: List[Channel]): Option[Channel] =
      upChannels(chans) match {
        case List()   => None
        case c :: Nil => Some(c)
        case c :: mChans if c.name.contains("Local") =>
          (c :: mChans).filter(c => rightPeerNumber(c.variables)) match {
            case ch :: List() => Some(ch)
            case _            => None
          }
        case c :: mChans =>
          (c :: mChans).filter(_.state == ChannelState.UP).headOption
      }

    def rightPeerNumber(variables: Map[String, String]): Boolean =
      variables.get("DIALEDPEERNUMBER") match {
        case None         => false
        case Some(peerNb) => interface.contains(peerNb.replace("/n", ""))
      }

    upChannel(onlyOneChan(myChans(interface, channels.values.toList))) match {
      case Some(channel) => Some(channel)
      case None =>
        log.error(
          s"Unable to transfer : No channel of more than one channel for $interface in ${myChans(interface, channels.values.toList)} list ${channels.values.toList}"
        )
        None
    }
  }

}

trait CompleteXferFinder {
  this: ChannelRepository with ChannelMatchers =>

  def holdChannel(chans: List[Channel]): List[Channel] =
    chans filter (_.state == ChannelState.HOLD)

  def toCompleteXfer(interface: String): Option[Channel] = {
    if (log.isDebugEnabled)
      log.debug(s"${myChans(interface, channels.values.toList)}")

    onlyOneChannel(
      holdChannel(myChans(interface, channels.values.toList))
    ) match {
      case Some(channel) => Some(channel)
      case None =>
        log.error(
          s"Unable to find only one channel on Hold to complete transfer for $interface in ${myChans(interface, channels.values.toList)}"
        )
        None
    }
  }
}

trait CancelXferFinder {
  this: ChannelRepository with ChannelMatchers =>

  def toCancelXfer(interface: String, extension: String): Option[Channel] = {
    val myChannels = myChans(interface, channels.values.toList)
    allButOnHold(myChannels) match {
      case List(channel) => Some(channel)
      case List(channel, _*) =>
        log.error(
          s"Unable to find only one channel to cancel transfer for $interface in $myChannels"
        )
        None
      case List() =>
        log.error(
          s"Unable to find any channel other than on hold to cancel transfer for $interface in $myChannels"
        )
        None
    }
  }
}

trait SetDataFinder {
  this: ChannelRepository with ChannelMatchers =>

  def toSetData(phoneNb: String): List[Channel] = {
    def addLnkChans(chan: Channel) =
      channels.get(chan.linkedChannelId) match {
        case Some(lkChan) if chan.id != chan.linkedChannelId =>
          List(chan, lkChan)
        case _ => List(chan)
      }

    myPhoneChans(phoneNb, channels.values.toList).flatMap(addLnkChans(_))
  }

}

case class ChannelRepository(channels: Channels = new Channels())
    extends ChannelFinder
    with HangupFinder
    with XferFinder
    with CompleteXferFinder
    with CancelXferFinder
    with SetDataFinder
    with ChannelMatchers {

  val log: Logger = Logger(ChannelRepository.getClass.getName)

  def apply(id: String): Channel = channels(id)

  log.trace({
    val stack = new RuntimeException().getStackTrace.view.drop(1)
    stack
      .filter(!_.getClassName.matches("(scala|pekko)\\..*"))
      .map { ste =>
        ste.getFileName.stripSuffix(".scala") + "@" + ste.getLineNumber
      }
      .filter(!_.startsWith("Logger@"))
      .take(8)
      .mkString(" <- ") +
      channels
        .map { case (id, channel) =>
          s"$id -> $channel"
        }
        .mkString(" channels=", " | ", "")
  })

  def get(id: String): Option[Channel] = channels.get(id)

  def getByNumbers(callerNo: String, connectedLineNb: String): Option[Channel] =
    channels.values.filter { channel =>
      channel.callerId.number == callerNo && channel.connectedLineNb.isDefined &&
      channel.connectedLineNb.get == connectedLineNb
    }.toList match {
      case List()   => None
      case c :: Nil => Some(c)
      case chans    => chans.find(_.name.contains("Local"))
    }

  def getByChannelName(name: String): Option[Channel] =
    channels.values.find(_.name == name)

  def getTransferer(chan: Channel): Option[Channel] =
    for {
      xFerName  <- chan.variables.get(Channel.VarNames.TransfererName)
      xTrsferer <- channels.values.find(_.name.contains(xFerName))
    } yield xTrsferer

  def addNewChannel(channel: Channel): ChannelRepository =
    channels.get(channel.id) match {
      case None =>
        val chan = myChans(channel.interface(), channels.values.toList) match {
          case x :: xs => channel.addUserVariables(x.variables)
          case _       => channel
        }
        log.debug(s"Adding New Channel : $chan")
        ChannelRepository(channels + (chan.id -> chan))
      case _ =>
        log.debug(s"Channel : $channel already exists")
        this
    }

  def updateChannelsLinkedId(
      oldLinkedId: String,
      newLinkedId: String
  ): Channels = {
    channels.map {
      case (id, channel) if channel.linkedChannelId == oldLinkedId =>
        id -> channel.withLinkedId(newLinkedId)
      case (id, channel) => (id, channel)
    }
  }

  def updateXFerCIdOnChanL2(cId: CallerId, chan: Channel): Channel =
    if (chan.isL2onAttXfer) chan.withCallerId(cId) else chan

  def updateChanUserDataOnXFer(chan: Channel): Channel =
    getTransferer(chan)
      .map(trferer =>
        updateXFerCIdOnChanL2(
          trferer.callerId,
          chan.addUserVariables(trferer.variables)
        )
      )
      .getOrElse(chan)

  def updateLinkedId(fromId: String, toId: String): ChannelRepository = {
    val chans = for {
      initChan: Channel <- channels.get(fromId)
      destChan: Channel <- channels.get(toId)
    } yield {
      val upChans = updateChannelsLinkedId(
        destChan.linkedChannelId,
        initChan.linkedChannelId
      )
      upChans + (destChan.id -> destChan.withLinkedId(initChan.linkedChannelId))
    }

    ChannelRepository(chans.getOrElse(channels))
  }

  def updateChannel(
      channel: Channel,
      f: Channel => Channel = identity
  ): ChannelRepository =
    ChannelRepository(
      channels + (channel.id -> f(updateChanUserDataOnXFer(channel)))
    )

  def propagateVars(fromId1: String, toId2: String): ChannelRepository = {
    if (fromId1 != toId2) {
      log.debug(s"prop vars from $fromId1 to $toId2 repo : $channels")

      val chans = for {
        fromChannel <- channels.get(fromId1)
        toChannel   <- channels.get(toId2)
        newChannelTo = updateChanUserDataOnXFer(
          toChannel.addVariables(fromChannel.variables)
        )
        newChannelFrom = fromChannel.addOriginatePartyState(toChannel)
      } yield channels + (toChannel.id -> newChannelTo) + (fromChannel.id -> newChannelFrom)

      log.debug(s"variables updated ${chans.getOrElse(channels).get(toId2)}")
      ChannelRepository(chans.getOrElse(channels))
    } else {
      this
    }
  }
}
