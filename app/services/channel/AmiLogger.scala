package services.channel

import org.asteriskjava.manager.event._
import play.api.Logger
import services.XucAmiBus.AmiEvent
import xivo.xucami.userevents.{UserEventAgentLogin, UserEventAgentLogoff}

object AmiLogger {
  val log: Logger = Logger("amievents")

  def prChan(e: ManagerEvent, f: String): Unit = {
    log.info(s"Chan-> $f")
    prChanDebug(e, duplicate = true)
  }

  def prChanDebug(e: ManagerEvent, duplicate: Boolean): Unit = {
    val duplicateStr = if (duplicate) "see ↑" else "uniq."
    log.debug(s"RawAmi($duplicateStr)-> $e")
  }

  def pIds(evt: AbstractChannelEvent): String =
    s"${evt.getUniqueId} ${evt.getChannel}"
  def cId(me: ManagerEvent): String =
    s"Cid(${me.getCallerIdName},${me.getCallerIdNum})"

  def logEvent(event: AmiEvent): Unit = {
    val msg = event.message
    msg match {
      case nce: NewChannelEvent =>
        prChan(
          msg,
          s"NewChannelEvent    : ${pIds(nce)} ${nce.getChannelStateDesc} ${nce.getExten}"
        )
      case nci: NewCallerIdEvent =>
        prChan(msg, s"NewCallerIdEvent   : ${pIds(nci)} ${cId(nci)}")
      case lcb: LocalBridgeEvent =>
        prChan(
          msg,
          s"LocalBridgeEvent   : ${lcb.getLocalOneUniqueId} ${lcb.getLocalTwoUniqueid}"
        )
      case nse: NewStateEvent =>
        prChan(
          msg,
          s"NewStateEvent      : ${pIds(nse)} ${nse.getChannelStateDesc} ${cId(nse)} ${nse.getConnectedLineNum}"
        )
      case dia: DialEvent =>
        prChan(
          msg,
          s"DialEvent          : ${dia.getUniqueId} ${dia.getChannel} ${dia.getSubEvent} ${dia.getDestUniqueId} $dia"
        )
      case bri: BridgeEvent =>
        prChan(
          msg,
          s"BridgeEvent        : ${bri.getUniqueId1} ${bri.getUniqueId2} ${bri.isLink}"
        )
      case mae: MasqueradeEvent =>
        prChan(
          msg,
          s"MasqueradeEvent    : clone[${mae.getClone}] original[${mae.getOriginal}]"
        )
      case ren: RenameEvent =>
        prChan(
          msg,
          s"RenameEvent        : ${ren.getUniqueId} ${ren.getChannel} ${ren.getNewname}"
        )
      case hur: HangupRequestEvent =>
        prChan(msg, s"HangupRequestEvent : ${pIds(hur)}")
      case hue: HangupEvent =>
        prChan(
          msg,
          s"HangupEvent        : ${pIds(hue)} ${hue.getConnectedLineNum} ${hue.getCause}|${hue.getCauseTxt}"
        )
      case soh: SoftHangupRequestEvent =>
        prChan(msg, s"SoftHgupRequestEvt : ${pIds(soh)} ${soh.getCause}")

      case qcj: QueueCallerJoinEvent =>
        prChan(
          msg,
          s"QueueCallerJoinEvent    : ${qcj.getPosition} ${qcj.getDateReceived} ${cId(
            qcj
          )} ${qcj.getQueue} ${qcj.getUniqueId}"
        )
      case qcl: QueueCallerLeaveEvent =>
        prChan(
          msg,
          s"QueueCallerLeaveEvent   : ${qcl.getPosition} ${qcl.getDateReceived} ${cId(
            qcl
          )} ${qcl.getQueue} ${qcl.getUniqueId}"
        )
      case qca: QueueCallerAbandonEvent =>
        prChan(
          msg,
          s"QueueCallerAbandonEvent : ${qca.getPosition} ${qca.getDateReceived} ${cId(
            qca
          )} ${qca.getQueue} ${qca.getUniqueId} ${qca.getHoldTime}"
        )

      case aco: AgentConnectEvent =>
        prChan(
          msg,
          s"AgentConnectEvent       : $aco ${aco.getUniqueId} ${aco.getChannel} ${aco.getMemberName} ${aco.getQueue} ${aco.getBridgedChannel}"
        )
      case ace: AgentCompleteEvent =>
        prChan(msg, s"AgentCompleteEvent      : $ace")
      case ace: AgentCalledEvent =>
        prChan(
          msg,
          s"AgentCalledEvent        : $ace ${ace.getUniqueId} ${ace.getMemberName} ${ace.getQueue} ${cId(
            ace
          )} cal:${ace.getChannelCalling} dest:${ace.getDestinationChannel} ${ace.getConnectedLineNum}"
        )
      case qmp: QueueMemberPauseEvent =>
        prChan(
          msg,
          s"QueueMemberPauseEvent   : ${qmp.getMemberName} ${qmp.getQueue} ${qmp.getPaused} ${qmp.getPausedreason}"
        )
      case ual: UserEventAgentLogin =>
        prChan(
          msg,
          s"UserEventAgentLogin     : ${ual.getExtension()} ${ual.getAgentId()} ${ual.getAgentnumber}"
        )
      case uao: UserEventAgentLogoff =>
        prChan(
          msg,
          s"UserEventAgentLogoff    : ${uao.getAgentId()} ${uao.getAgentnumber}"
        )
      case vse: VarSetEvent =>
        prChan(
          msg,
          s"VarSetEvent             : ${vse.getUniqueId} : (${vse.getVariable} -> ${vse.getValue})"
        )
      case mse: MonitorStartEvent =>
        prChan(msg, s"MonitorStartEvent       : ${mse.getChannel}")
      case mso: MonitorStopEvent =>
        prChan(msg, s"MonitorStopEvent        : ${mso.getChannel}")

      case exs: ExtensionStatusEvent =>
        prChan(
          msg,
          s"ExtensionStatus         : ${exs.getExten} : ${exs.getStatus} / ${exs.getStatustext} (${exs.getHint})"
        )

      case mwe: MessageWaitingEvent =>
        prChan(
          msg,
          s"MessageWaiting          : ${mwe.getMailbox} new:${mwe.getNew}, wait:${mwe.getWaiting} old:${mwe.getOld}"
        )
      case vue: VoicemailUserDetailEvent =>
        prChan(
          msg,
          s"VoicemailUserDetail     : ${vue.getVoicemailbox}@${vue.getVmContext}  new:${vue.getNewMessageCount} old:${vue.getOldMessageCount}"
        )

      case _: ManagerEvent => prChanDebug(msg, duplicate = false)
    }
  }
}
