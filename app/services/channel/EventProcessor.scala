package services.channel

import org.asteriskjava.live.HangupCause
import org.asteriskjava.manager.action.GetVarAction
import org.asteriskjava.manager.event._
import play.api.Logger
import services.XucAmiBus
import services.XucAmiBus._
import xivo.xucami.models.{CallerId, Channel, ChannelState, MonitorState}
import xivo.xucami.userevents.{HangupEvent => HangupUserEvent}
trait EventProcessor {
  val processor: AmiEventProcessor
}

class AmiEventProcessor(amiBus: XucAmiBus) {
  val log: Logger          = Logger(this.getClass.getName)
  val loggerAmi: Logger    = Logger("amievents")
  val XivoAnsweredVariable = "XIVO_PICKEDUP"
  val XivoAnswerValue      = "1"

  def process(
      event: AmiEvent,
      channelRepo: ChannelRepository
  ): ChannelRepository = {
    event.message match {

      case hangup: HangupEvent =>
        processHangup(hangup.getUniqueId, channelRepo, Some(hangup))

      case hangup: HangupUserEvent =>
        log.debug(s"HangupUserEvent $hangup")
        processHangup(hangup.getUniqueId, channelRepo)

      case spyStarted: ChanSpyStartEvent =>
        log.debug(s"spyee ${spyStarted.getSpyeeChannel}")
        log.debug(s"spyer ${spyStarted.getSpyerChannel}")
        val channels = for {
          spyeeChannel <- channelRepo.channels.values.find(
            _.name == spyStarted.getSpyeeChannel
          )
          spyerChannel <- channelRepo.channels.values.find(
            _.name == spyStarted.getSpyerChannel
          )
        } yield {
          log.debug(
            s"spy started : spyee: $spyeeChannel <-> spyer : $spyerChannel"
          )
          amiBus.publish(SpyStarted(SpyChannels(spyerChannel, spyeeChannel)))
          channelRepo
            .updateChannel(spyerChannel)
            .updateChannelsLinkedId(spyerChannel.id, spyeeChannel.id)
        }
        channels.map(ChannelRepository(_)).getOrElse(channelRepo)

      case spyStopped: ChanSpyStopEvent =>
        log.debug(s"spy stopped: $spyStopped")
        channelRepo.channels.values.find(
          _.id == spyStopped.getSpyerUniqueId
        ) foreach { channel =>
          channelRepo.channels.values.find(
            _.id == channel.linkedChannelId
          ) foreach { linkedChannel =>
            log.debug(
              "notifying spy stop on spyee channel: " + linkedChannel.id
            )
            amiBus.publish(SpyStopped(linkedChannel))
          }
        }
        channelRepo

      case newState: NewStateEvent =>
        val newRepo = channelRepo
          .get(newState.getUniqueId)
          .map(c => c.withState(newState))
          .map(c =>
            channelRepo
              .updateChannel(c)
              .propagateVars(c.linkedChannelId, c.id)
          )
          .getOrElse(channelRepo)

        val channel = newRepo.get(newState.getUniqueId)
        val linkedChannel = channel
          .filter(c => c.linkedChannelId != c.id && c.state == ChannelState.UP)
          .flatMap(c => newRepo.get(c.linkedChannelId))

        channel.foreach(publish(_))
        linkedChannel.foreach(publish(_))

        newRepo

      case varSetEvent: VarSetEvent if varSetEvent.getValue != null =>
        channelRepo.get(varSetEvent.getUniqueId) match {
          case Some(channel) =>
            val updatedChannel = channel.updateVariable(
              varSetEvent.getVariable,
              varSetEvent.getValue
            )
            if (
              varSetEvent.getVariable == XivoAnsweredVariable && varSetEvent.getValue == XivoAnswerValue
            ) {
              amiBus.publish(DialAnswered(updatedChannel))
            }
            channelRepo.updateChannel(updatedChannel)
          case _ => channelRepo
        }

      case newCallerId: NewCallerIdEvent =>
        channelRepo.get(newCallerId.getUniqueId) match {
          case Some(channel) if channel.callerId.isNotFullySet =>
            log.debug(s"$channel")
            channelRepo.updateChannel(
              channel.withCallerId(
                CallerId(
                  newCallerId.getCallerIdName,
                  newCallerId.getCallerIdNum
                )
              )
            )
          case _ => channelRepo
        }

      case newConnectedLine: NewConnectedLineEvent =>
        channelRepo.get(newConnectedLine.getUniqueId) match {
          case Some(channel) =>
            val chanR = channelRepo.updateChannel(
              channel.withConnectedLine(
                newConnectedLine.getConnectedLineNum,
                newConnectedLine.getConnectedLineName
              )
            )
            log.debug(
              s"Setting connectedLineNb to ${newConnectedLine.getConnectedLineNum} for $channel"
            )
            publishAsResended(chanR(channel.id))
            chanR
          case _ => channelRepo
        }

      case dialEvent: DialEvent =>
        val cs = channelRepo.updateLinkedId(
          dialEvent.getUniqueId,
          dialEvent.getDestUniqueId
        )
        log.debug(s"${cs.channels}")
        cs

      case attendedTransfer: AttendedTransferEvent =>
        val transferredChannelId = attendedTransfer.getTransfereeUniqueid
        channelRepo.channels.get(transferredChannelId) match {
          case Some(channel) =>
            val newCaleeNum =
              attendedTransfer.getSecondTransfererConnectedLineNum
            val newCaleeName =
              attendedTransfer.getSecondTransfererConnectedLineName
            channelRepo.updateChannel(
              channel.withConnectedLine(newCaleeNum, newCaleeName),
              publish
            )
          case None => channelRepo
        }

      case localBridgeEvent: LocalBridgeEvent =>
        val cs = channelRepo.updateLinkedId(
          localBridgeEvent.getLocalOneUniqueId,
          localBridgeEvent.getLocalTwoUniqueid
        )
        log.debug(s"${cs.channels}")
        cs

      case agentCalled: AgentCalledEvent =>
        channelRepo.channels.get(agentCalled.getUniqueId) match {
          case Some(channel) =>
            channelRepo.updateChannel(
              channel.withAgentName(agentCalled.getMemberName),
              publish
            )
          case _ => channelRepo
        }

      case monitorStart: MonitorStartEvent =>
        processMonitoringSwitch(monitorStart.getUniqueId, true, channelRepo)

      case monitorStop: MonitorStopEvent =>
        processMonitoringSwitch(monitorStop.getUniqueId, false, channelRepo)

      case hold: HoldEvent => processHold(hold, channelRepo)

      case unhold: UnholdEvent => processUnhold(unhold, channelRepo)

      case coreShowChannelEvent: CoreShowChannelEvent =>
        channelRepo.addNewChannel(
          Channel(coreShowChannelEvent, event.sourceMds)
        )

      case any => channelRepo
    }
  }

  private def processMonitoringSwitch(
      channelId: String,
      isMonitored: Boolean,
      channelRepo: ChannelRepository
  ): ChannelRepository =
    channelRepo.get(channelId) match {
      case Some(channel) =>
        val newChannel = channel.copy(monitored =
          if (isMonitored) MonitorState.ACTIVE else MonitorState.DISABLED
        )
        amiBus.publish(ChannelEvent(newChannel))
        amiBus.publish(
          AmiAction(
            new GetVarAction(channel.name, "MONITOR_PAUSED"),
            Some(channel.id)
          )
        )
        channelRepo.updateChannel(newChannel)
      case None => channelRepo
    }

  private def processHangup(
      uniqueId: String,
      channelRepo: ChannelRepository,
      hangup: Option[HangupEvent] = None
  ): ChannelRepository = {
    channelRepo.get(uniqueId) match {
      case Some(channel) =>
        val peerId =
          if (
            channel.isLocal && channel.isSipAttTransfer && channel.isACallerIdChannel
            && hangup
              .map(h =>
                h.getCause == HangupCause.AST_CAUSE_NORMAL_CLEARING.getCode
              )
              .getOrElse(true)
          ) {
            log.debug(
              s"Channel $channel hangup - event not published because SIP att. transfer"
            )
            None
          } else {
            log.debug(s"Channel $channel hangup")
            publish(channel.withChannelState(ChannelState.HUNGUP))
            getLocalBridgedPeer(channel, channelRepo)
              .map(c => {
                log.debug(s"FOUND LOCAL PEER $c")
                publish(c.withChannelState(ChannelState.HUNGUP))
                c.id
              })
          }
        peerId match {
          case Some(id) =>
            ChannelRepository(channelRepo.channels - uniqueId - id)
          case None => ChannelRepository(channelRepo.channels - uniqueId)
        }
      case None =>
        log.warn("Got a Hangup event for unknown channel.")
        channelRepo
    }
  }

  private def getLocalBridgedPeer(
      channel: Channel,
      channelRepo: ChannelRepository
  ): Option[Channel] =
    channel.variables
      .get("BRIDGEPEER")
      .flatMap(channelRepo.getByChannelName(_))
      .filter(_.isLocal)
      .flatMap(c => channelRepo.getByChannelName(c.name.dropRight(1) + '2'))

  private def processHold(
      hold: HoldEvent,
      channelRepo: ChannelRepository
  ): ChannelRepository =
    channelRepo.get(hold.getUniqueId) match {
      case Some(channel) if channel.state != ChannelState.HOLD =>
        log.debug(s"Channel $channel hold")
        channelRepo.updateChannel(
          channel.withChannelState(ChannelState.HOLD),
          publish
        )
      case _ => processHoldOnLocal(hold, channelRepo)
    }

  private def processHoldOnLocal(
      hold: HoldEvent,
      channelRepo: ChannelRepository
  ): ChannelRepository =
    channelRepo
      .get(hold.getUniqueId)
      .flatMap(getLocalBridgedPeer(_, channelRepo)) match {
      case Some(channel) =>
        log.debug(s"Hold Local Channel $channel hold")
        channelRepo.updateChannel(
          channel.withChannelState(ChannelState.HOLD),
          publish
        )
      case None =>
        log.warn(s"Got a HoldEvent for the unknown channel $hold")
        channelRepo
    }

  private def processUnhold(
      unhold: UnholdEvent,
      channelRepo: ChannelRepository
  ): ChannelRepository = {
    channelRepo.get(unhold.getUniqueId) match {
      case Some(channel) =>
        getLocalBridgedPeer(channel, channelRepo) match {
          case Some(localChannel)
              if localChannel.state == ChannelState.HOLD && localChannel.isLocal =>
            channelRepo.updateChannel(
              localChannel.withChannelState(ChannelState.UP),
              publish
            )
          case _ =>
            channelRepo.updateChannel(
              channel.withChannelState(ChannelState.UP),
              publish
            )
        }
      case _ =>
        log.warn(s"Got an UnholdEvent for the unknown channel $unhold")
        channelRepo
    }
  }

  private def publishImpl(channel: Channel, resended: Boolean): Channel = {
    if (!channel.isAgentCallback) {
      loggerAmi.info(s"Pub: $channel")
      amiBus.publish(ChannelEvent(channel, resended))
    } else {
      loggerAmi.debug(s"Not: $channel")
    }
    channel
  }

  protected[channel] val publishAsResended: Channel => Channel = channel =>
    publishImpl(channel, resended = true)
  protected[channel] val publish: Channel => Channel = channel =>
    publishImpl(channel, resended = false)

}
