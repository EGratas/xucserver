package services.channel

import org.apache.pekko.actor.{Actor, ActorLogging, Props}
import org.asteriskjava.live.AsteriskChannel
import org.asteriskjava.manager.action.{
  GetVarAction,
  PauseMonitorAction,
  SetVarAction,
  UnpauseMonitorAction
}
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.{GetVarResponse, ManagerResponse}
import play.api.Logger
import services.XucAmiBus
import services.XucAmiBus._
import services.calltracking.SipDriver
import xivo.xuc.XucBaseConfig
import xivo.xucami.models._

import java.beans.PropertyChangeEvent
import scala.collection.immutable.HashMap

object ChannelManager {
  type Channels = HashMap[String, Channel]
  def props(amiBus: XucAmiBus, xucConfig: XucBaseConfig): Props =
    Props(
      new ChannelManager(amiBus, xucConfig = xucConfig)
        with ProductionEventProcessor
        with ProductionChannelRequestProcessor
    )
}

trait ProductionEventProcessor extends EventProcessor {
  def amiBus: XucAmiBus
  override val processor = new AmiEventProcessor(amiBus)
}

trait ProductionChannelRequestProcessor extends ChannelRequestProcessor {
  def amiBus: XucAmiBus
  override val channelProcessor = new ChannelRequestProc(amiBus)
}

class ChannelManager(
    val amiBus: XucAmiBus,
    channelRepo: ChannelRepository = ChannelRepository(
      new HashMap[String, Channel]()
    ),
    xucConfig: XucBaseConfig
) extends Actor
    with ActorLogging {
  this: EventProcessor with ChannelRequestProcessor =>

  amiBus.subscribe(self, AmiType.AmiEvent)
  amiBus.subscribe(self, AmiType.AmiResponse)
  amiBus.subscribe(self, AmiType.ChannelActionRequest)
  import ChannelManager.Channels

  val loggerAmi: Logger = Logger("amievents")

  def receive: Receive = process(channelRepo)

  def process(channelRepo: ChannelRepository): Receive = {

    case AmiEvent(newChanEvent: NewChannelEvent, sourceMds) =>
      context.become(
        process(onNewChannel(channelRepo, newChanEvent, sourceMds))
      )

    case event: AmiEvent =>
      context.become(process(processor.process(event, channelRepo)))

    case request: ChannelRequest =>
      log.debug(s"$request")
      channelProcessor.processChannelReq(request, channelRepo)

    case reply: AmiResponse =>
      context.become(
        process(ChannelRepository(processResponse(reply, channelRepo.channels)))
      )

    case propChange: PropertyChangeEvent =>
      context.become(
        process(
          ChannelRepository(
            processPropertyChangeEvent(propChange, channelRepo.channels)
          )
        )
      )

    case any =>
      log.debug(s"Received an unprocessed message: $any")
  }

  private def onNewChannel(
      channelRepo: ChannelRepository,
      newChanEvent: NewChannelEvent,
      sourceMds: String
  ) = {
    val channel = Channel(newChanEvent, sourceMds)
    val repo    = channelRepo.addNewChannel(channel)
    publish(repo.channels(channel.id))
    xucConfig.sipDriver match {
      case SipDriver.SIP
          if channel.name.toUpperCase.startsWith(SipDriver.SIP.toString) =>
        amiBus.publish(
          AmiAction(
            new GetVarAction(channel.name, "SIPCALLID"),
            Some(channel.id)
          )
        )
      case SipDriver.PJSIP
          if channel.name.toUpperCase.startsWith(SipDriver.PJSIP.toString) =>
        log.debug(
          s"Getting SIP CallId for channel: ${channel.name} from $sourceMds"
        )
        amiBus.publish(
          AmiAction(
            new GetVarAction(channel.name, "CHANNEL(pjsip,call-id)"),
            Some(channel.id),
            targetMds = Some(sourceMds)
          )
        )
      case _ =>
        log.debug(s"Channel $channel not requesting SIP call-id")
    }
    repo
  }

  private def publish(channel: Channel): Unit = {
    loggerAmi.info(s"Pub: $channel")
    amiBus.publish(ChannelEvent(channel))
  }

  private def processMonitorUpdate(
      channel: AsteriskChannel,
      newValue: Boolean,
      channels: Channels
  ): Channels = {
    val uniqueId = channel.getId
    channels.get(uniqueId) match {
      case Some(channel) =>
        if (newValue) {
          channel.monitored = MonitorState.ACTIVE
          log.debug(s"Channel $channel is monitored")
        } else {
          channel.monitored = MonitorState.DISABLED
          log.debug(s"Channel $channel is not monitored")
        }
        publish(channel)
        channels - uniqueId + (uniqueId -> channel)
      case None =>
        log.debug(s"Got a MonitorUpdate for an unknown channel: $channel.")
        channels
    }
  }

  case class AmiR(
      response: ManagerResponse,
      action: Option[AmiAction],
      channels: Channels
  )

  type AmiProcessRespFunction = scala.PartialFunction[AmiR, Channels]

  def processPauseMonitorResp: AmiProcessRespFunction = {
    case AmiR(response: ManagerResponse, Some(action), channels) =>
      action.message match {
        case pause: PauseMonitorAction =>
          channels.get(action.reference.getOrElse("")) match {
            case Some(channel) =>
              channel.monitored = MonitorState.PAUSED
              publish(channel)
              channels - channel.id + (channel.id -> channel)
            case None =>
              log.error(s"Got response for unknown channel!")
              channels
          }

        case unpause: UnpauseMonitorAction =>
          channels.get(action.reference.getOrElse("")) match {
            case Some(channel) =>
              channel.monitored = MonitorState.ACTIVE
              publish(channel)
              channels - channel.id + (channel.id -> channel)
            case None =>
              log.error(s"Got response for unknown channel!")
              channels
          }
        case any =>
          log.debug(
            s"Reply $any to action $action is not currently processed ${channels
              .get(action.reference.getOrElse(""))}"
          )
          channels

      }
  }

  def processGetVarResp: AmiProcessRespFunction = {
    case AmiR(response: GetVarResponse, Some(action), channels) =>
      log.debug(s"$response   \n\t${response.getAttributes}  \n\t$channels")
      channels.get(action.reference.getOrElse("")) match {
        case Some(channel) =>
          log.debug(
            s""" ${response.getAttribute("variable")} : ${response.getAttribute(
              "value"
            )}"""
          )
          response.getAttribute("variable") match {
            case "MONITOR_PAUSED" =>
              if (response.getAttribute("value") == "true") {
                log.debug("Monitor started in pause state")
                channel.monitored = MonitorState.PAUSED
                publish(channel)
                channels - channel.id + (channel.id -> channel)
              } else
                channels
            case "SIPCALLID" =>
              val updated = channel.addVariables(
                Map("SIPCALLID" -> response.getAttribute("value"))
              )
              publish(updated)
              channels - channel.id + (channel.id -> updated)
            case "DIALSTATUS" =>
              val direction = channel.getDirection(response.getValue)
              val updated: Channel = channel
                .addVariables(Map("DIALSTATUS" -> response.getValue))
                .withDirection(direction)
              publish(updated)
              channels - channel.id + (channel.id -> updated)
            case "CHANNEL(pjsip,call-id)" =>
              log.debug(
                s"Got SIP CallId ${response.getAttribute("value")} for channel ${channel.name}"
              )
              setSipcallidChannelVar(response, channel)
              val updated = channel.addVariables(
                Map("SIPCALLID" -> response.getAttribute("value"))
              )
              publish(updated)
              channels - channel.id + (channel.id -> updated)
            case _ => channels
          }
        case None => channels
      }
  }

  private def setSipcallidChannelVar(
      response: GetVarResponse,
      channel: Channel
  ): Unit = {
    val setvar = new SetVarAction()
    setvar.setChannel(channel.name)
    setvar.setVariable("XIVO_SIPCALLID")
    setvar.setValue(response.getAttribute("value"))
    amiBus.publish(AmiAction(setvar))
  }

  def processAny: AmiProcessRespFunction = {
    case AmiR(response, Some(action), channels) => channels
  }

  private def processResponse(
      reply: AmiResponse,
      channels: Channels
  ): Channels = {
    val attr = reply.message._1.getAttributes
    log.debug(s"ami response: $reply : $attr")
    reply.message match {
      case (response, Some(action)) =>
        response.getResponse match {
          case "Success" =>
            (processGetVarResp orElse processPauseMonitorResp orElse processAny)(
              AmiR(response, Some(action), channels)
            )
          case _ =>
            log.error(s"Ami action $action failed: $response")
            if (channels.get(response.getUniqueId) != None) {
              checkPJSIPCallId(reply) match {
                case Some(a) =>
                  amiBus.publish(a)
                case None =>
              }
            }
            channels
        }
      case (response, None) =>
        log.debug(s"Not processing replies without action: $response")
        channels
    }
  }

  private def checkPJSIPCallId(reply: AmiResponse): Option[AmiAction] = {
    reply match {
      case AmiResponse(
            (
              evt: GetVarResponse,
              Some(AmiAction(action: GetVarAction, _, _, actionTargetMds))
            )
          ) =>
        if (
          evt.getResponse != "Success" && action.getVariable == "CHANNEL(pjsip,call-id)"
        ) {
          log.warning(
            "Get SIP CallId for channel {} failed, retrying, failure: {}",
            action.getChannel,
            evt
          )
          Some(
            AmiAction(
              new GetVarAction(action.getChannel, "CHANNEL(pjsip,call-id)"),
              Some(action.getActionId),
              targetMds = actionTargetMds
            )
          )
        } else {
          None
        }
      case _ => None
    }
  }

  private def processPropertyChangeEvent(
      propChange: PropertyChangeEvent,
      channels: Channels
  ): Channels = {
    propChange.getSource match {
      case channel: AsteriskChannel =>
        propChange.getPropertyName match {
          case AsteriskChannel.PROPERTY_MONITORED =>
            amiBus.publish(
              AmiAction(
                new GetVarAction(channel.getName, "MONITOR_PAUSED"),
                Some(channel.getId)
              )
            )
            processMonitorUpdate(
              channel,
              propChange.getNewValue.asInstanceOf[Boolean],
              channels
            )
          case _ =>
            log.debug(
              s"Received unprocessed property change event: $propChange"
            )
            channels
        }
      case _ =>
        log.error(
          s"Received wrong source in propertyChangeEvent for monitor property: $propChange"
        )
        channels
    }
  }
}
