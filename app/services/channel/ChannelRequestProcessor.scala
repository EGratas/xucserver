package services.channel

import org.asteriskjava.live.HangupCause
import org.asteriskjava.manager.action._
import play.api.Logger
import services.XucAmiBus
import services.XucAmiBus.{AmiAction, ChannelRequest}
import services.channel.ChannelRequestProc._

trait ChannelRequestProcessor {
  val channelProcessor: ChannelRequestProc
}

trait ActionBuilder {
  def pauseAction(channelName: String, channelId: String): AmiAction = {
    val action = new PauseMonitorAction()
    action.setChannel(channelName)
    AmiAction(action, Some(channelId))
  }
  def unPauseAction(channelName: String, channelId: String): AmiAction = {
    val action = new UnpauseMonitorAction()
    action.setChannel(channelName)
    AmiAction(action, Some(channelId))
  }

  def hangupAction(channelName: String, channelId: String): List[AmiAction] = {
    val setvar = new SetVarAction()
    setvar.setChannel(channelName)
    setvar.setVariable("CHANNEL(hangupsource)")
    setvar.setValue(channelName)
    val action = new HangupAction()
    action.setChannel(channelName)
    action.setCause(HangupCause.AST_CAUSE_NORMAL_CLEARING.getCode)
    List(AmiAction(setvar, Some(channelId)), AmiAction(action, Some(channelId)))
  }

  def atxFerAction(
      channelName: String,
      destination: String,
      context: String,
      channelId: String
  ): AmiAction = {
    val action = new AtxferAction()
    action.setChannel(channelName)
    action.setContext(context)
    action.setExten(destination)
    action.setPriority(1)
    AmiAction(action, Some(channelId))
  }

  def blindTransferAction(
      channelName: String,
      destination: String,
      context: String,
      channelId: String
  ): AmiAction = {
    val action = new BlindTransferAction()
    action.setChannel(channelName)
    action.setContext(context)
    action.setExten(destination)
    action.setPriority(1)
    AmiAction(action, Some(channelId))
  }

  def setVarAction(
      channelName: String,
      key: String,
      value: String,
      channelId: String
  ): AmiAction = {
    val action = new SetVarAction()
    action.setChannel(channelName)
    action.setValue(value)
    action.setVariable(key)
    AmiAction(action, Some(channelId))
  }

}

trait Hangup {
  this: ChannelRequestProc with ActionBuilder =>

  def hangup(
      interface: String,
      extension: String,
      repository: ChannelRepository
  ): Any =
    repository.toHangup(interface, extension) match {
      case List() => log.error(s"No channel found to hangup for $interface")
      case channels =>
        channels
          .flatMap(c => hangupAction(c.name, c.id))
          .map(amiBus.publish)
    }

}
trait AtXfer {
  this: ChannelRequestProc with ActionBuilder =>

  def atXfer(
      interface: String,
      destination: String,
      context: String,
      repository: ChannelRepository
  ): Option[Unit] =
    repository.toXfer(interface) map (chan =>
      amiBus.publish(
        atxFerAction(chan.name, destination + dialEnd, context, chan.id)
      )
    )

}

trait CompleteXfer {
  this: ChannelRequestProc with ActionBuilder =>

  def completeXfer(interface: String, repository: ChannelRepository): Any =
    repository.toCompleteXfer(interface) match {
      case Some(channel) =>
        log.debug(s"Complete transfer for $interface will hangup $channel")
        hangupAction(channel.name, channel.id).map(amiBus.publish)
      case any =>
        log.error(s"No channel found to complete transfer for $interface")
    }
}

trait CancelXfer {
  this: ChannelRequestProc with ActionBuilder =>

  def cancelXfer(
      interface: String,
      extension: String,
      repository: ChannelRepository
  ): Any =
    repository.toCancelXfer(interface, extension) match {
      case Some(channel) =>
        log.debug(s"Cancel transfer for $interface will hangup $channel")
        hangupAction(channel.name, channel.id).map(amiBus.publish)
      case any =>
        log.error(s"No channel found to cancel transfer for $interface")

    }
}

trait DirectXfer {
  this: ChannelRequestProc with ActionBuilder =>

  def directXfer(
      interface: String,
      destination: String,
      context: String,
      repository: ChannelRepository
  ): Unit =
    repository.toXfer(interface) match {
      case Some(chan) =>
        amiBus.publish(
          blindTransferAction(chan.name, destination, context, chan.id)
        )
      case _ => log.error(s"No channel found to direct transfer for $interface")
    }
}

trait SetDataToChannels {
  this: ChannelRequestProc with ActionBuilder =>

  def setData(
      phoneNb: String,
      variables: Map[String, String],
      repository: ChannelRepository
  ): Unit = {
    val chans = repository.toSetData(phoneNb)
    log.debug(s"$phoneNb my channels $chans")
    chans.foreach { channel =>
      {
        log.debug(s"Attach data $variables to $channel")
        variables.foreach { case (key, value) =>
          amiBus.publish(setVarAction(channel.name, key, value, channel.id))
        }
      }
    }
  }
}

object ChannelRequestProc {
  class ChannelActionRequest()

  case class HangupActionReq(val interface: String, val extension: String)
      extends ChannelActionRequest
  case class AtxFerActionReq(
      val interface: String,
      val destination: String,
      val context: String
  ) extends ChannelActionRequest
  case class CompleteXferActionReq(val interface: String)
      extends ChannelActionRequest
  case class CancelXferActionReq(val interface: String, val extension: String)
      extends ChannelActionRequest
  case class DirectXferActionReq(
      val interface: String,
      val destination: String,
      val context: String
  ) extends ChannelActionRequest
  case class SetDataActionReq(phoneNb: String, variables: Map[String, String])
      extends ChannelActionRequest

}

class ChannelRequestProc(val amiBus: XucAmiBus)
    extends ActionBuilder
    with Hangup
    with AtXfer
    with CompleteXfer
    with CancelXfer
    with DirectXfer
    with SetDataToChannels {

  val log: Logger = Logger(this.getClass.getName)
  val dialEnd     = "#"

  def processChannelReq(
      request: ChannelRequest,
      repository: ChannelRepository
  ): Any =
    request.message match {
      case HangupActionReq(interface, extension) =>
        hangup(interface, extension, repository)
      case AtxFerActionReq(interface, destination, context) =>
        atXfer(interface, destination, context, repository)
      case CompleteXferActionReq(interface) =>
        completeXfer(interface, repository)
      case CancelXferActionReq(interface, extension) =>
        cancelXfer(interface, extension, repository)
      case DirectXferActionReq(interface, destination, context) =>
        directXfer(interface, destination, context, repository)
      case SetDataActionReq(phoneNb, variables) =>
        setData(phoneNb, variables, repository)

      case any =>
    }

}
