package services.video

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef, Props}
import helpers.JmxActorSingletonMonitor
import models.XivoUser
import services.VideoChat
import services.chat.ChatService.Username
import services.config.ConfigRepository
import services.video.VideoService.{
  ConnectVideoUser,
  DisconnectVideoUser,
  VideoUser,
  VideoUserState
}
import services.video.model._
import services.VideoChat.{VideoChatStates, VideoChatUser, VideoStatus}

import javax.inject.Inject
import helpers.JmxLongMetric
import scala.util.Try

class VideoService @Inject() (configRepo: ConfigRepository)
    extends Actor
    with ActorLogging
    with JmxActorSingletonMonitor
    with VideoChat[VideoUserState, VideoStatus, VideoUser] {

  var users
      : Map[Username, VideoChatStates[VideoUserState, VideoStatus, VideoUser]] =
    Map.empty
  val jmxUsersCount: Try[JmxLongMetric] =
    jmxBean.addLong("Users", 0L, Some("Number of video users"))

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))

    context.become(processVideoRequest)
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
  }

  def processVideoRequest: Receive = {

    case ConnectVideoUser(xivoUser) =>
      val ctiRouterRef = sender()
      log.debug(s"Connect Video User : $xivoUser, $ctiRouterRef")

      users = disconnectXivoUser(xivoUser, VideoStatus.Unavailable)(users)
      users = connectXivoUser(
        xivoUser,
        newState(xivoUser, ctiRouterRef, VideoStatus.Available)
      )(users)

      jmxUsersCount.set(users.size)

    case DisconnectVideoUser(xivoUser) =>
      log.debug(s"Disconnect Video Text User : $xivoUser")
      users = disconnectXivoUser(xivoUser, VideoStatus.Unavailable)(users)
      jmxUsersCount.set(users.size)

    case mri: MeetingRoomInvite =>
      log.debug(
        s"Meeting room invite from: ${mri.username}, to ${mri.destUsername}"
      )
      val invitationSent =
        sendToRouter(mri.destUsername, mri, VideoStatus.Available)(users)
      log.debug(s"Meeting room invite from: $invitationSent")
      if (!invitationSent) {
        sender() ! MeetingRoomInviteAckReply(
          mri.requestId,
          mri.username,
          configRepo
            .getCtiUserDisplayName(mri.destUsername)
            .getOrElse(mri.destUsername),
          VideoInviteAck.NACK
        )
      }

    case ack: MeetingRoomInviteAckReply =>
      log.debug(s"Meeting room invite ack: ${ack.destUsername}")
      sendToRouter(ack.destUsername, ack, VideoStatus.Available).apply(users)

    case r: MeetingRoomInviteResponse =>
      log.debug(s"Meeting room invite response: ${r.destUsername}")
      sendToRouter(r.destUsername, r, VideoStatus.Available).apply(users)

    case other =>
      log.debug(s"Unexpected message received, $other")
  }

  def receive: Receive = Actor.emptyBehavior

  private def newState(
      xivoUser: XivoUser,
      cti: ActorRef,
      status: VideoStatus = VideoStatus.Available
  )(username: String) =
    VideoUserState(
      VideoUser(username, Some(xivoUser.fullName)),
      status,
      Some(cti),
      xivoUser
    )
}

object VideoService {
  final val serviceName = "VideoService"
  type Username = String

  sealed trait VideoRequest
  case class ConnectVideoUser(xivoUser: XivoUser)    extends VideoRequest
  case class DisconnectVideoUser(xivoUser: XivoUser) extends VideoRequest

  case class VideoUserState(
      user: VideoUser,
      status: VideoStatus,
      ctiRouterRef: Option[ActorRef],
      xivoUser: XivoUser
  ) extends VideoChatStates[VideoUserState, VideoStatus, VideoUser] {

    override def disconnect: VideoUserState = this

    override def withStatus(status: VideoStatus): VideoUserState =
      this.copy(status = status)
  }

  case class VideoUser(username: String, displayName: Option[String])
      extends VideoChatUser

  def props(configRepo: ConfigRepository): Props =
    Props(new VideoService(configRepo))
}
