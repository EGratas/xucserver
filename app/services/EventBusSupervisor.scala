package services

import org.apache.pekko.actor.{Actor, ActorRef, ActorSystem, Props, Terminated}
import org.apache.pekko.event.ActorEventBus
import services.EventBusSupervisor.Monitor

trait EventBusSupervision extends ActorEventBus {

  implicit val system: ActorSystem

  lazy val supervisor: ActorRef = system.actorOf(EventBusSupervisor.props(this))

  abstract override def publish(event: Event): Unit = super.publish(event)
  abstract override def unsubscribe(
      subscriber: Subscriber,
      from: Classifier
  ): Boolean = super.unsubscribe(subscriber, from)
  abstract override def unsubscribe(subscriber: Subscriber): Unit =
    super.unsubscribe(subscriber)

  abstract override def subscribe(
      subscriber: Subscriber,
      to: Classifier
  ): Boolean = {
    if (super.subscribe(subscriber, to)) {
      supervisor ! Monitor(subscriber)
      true
    } else false
  }

}

private[services] class EventBusSupervisor(bus: EventBusSupervision)
    extends Actor {

  def receive: PartialFunction[Any, Unit] = {
    case Monitor(ref) =>
      context.watch(ref)

    case Terminated(ref) =>
      bus.unsubscribe(ref)
  }

}

private[services] object EventBusSupervisor {
  case class Monitor(ref: ActorRef)

  def props(bus: EventBusSupervision): Props =
    Props(new EventBusSupervisor(bus))
}
