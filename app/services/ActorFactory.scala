package services

import org.apache.pekko.actor.{ActorPath, ActorRef}
import com.google.inject.ImplementedBy
import services.ActorIds.ctiRouterActorName
import xivo.directory.PersonalContactRepository

@ImplementedBy(classOf[ActorIds])
trait ActorIdsFactory {
  def personalContactRepositoryPath(
      ctiRouterFactory: ActorRef,
      username: String
  ): ActorPath

  def ctiRouterPath(
      ctiRouterFactory: ActorRef,
      username: String
  ): ActorPath

  def queueDispatcherPath(
      queueDispatcherFactory: ActorRef
  ): ActorPath
}

class ActorIds extends ActorIdsFactory {
  def personalContactRepositoryPath(
      ctiRouterFactory: ActorRef,
      username: String
  ): ActorPath =
    ctiRouterFactory.path / ctiRouterActorName(username) / {
      PersonalContactRepository.personalContactRepoURI
    }
  def ctiRouterPath(
      ctiRouterFactory: ActorRef,
      username: String
  ): ActorPath =
    ctiRouterFactory.path / ctiRouterActorName(username)

  def queueDispatcherPath(
      queueDispatcherFactory: ActorRef
  ): ActorPath = queueDispatcherFactory.path
}

object ActorIds {
  // Please Alphabetical order
  final val AgentConfigId          = "AgentConfig"
  final val AgentDeviceManagerId   = "AgentDeviceManager"
  final val AgentGroupSetterId     = "AgentGroupSetter"
  final val AgentManagerId         = "AgentManager"
  final val AmiBusConnectorId      = "AmiBusConnector"
  final val AsteriskGraphTrackerId = "AsteriskGraphTracker"
  final val CallHistoryManagerId   = "CallHistoryManager"
  final val CallbackMgrInterfaceId = "CallbackMgrInterface"
  final val ChannelTrackerId       = "ChannelTracker"
  final val ClientLogger           = "ClientLogger"
  final val ConferenceTrackerId    = "ConferenceTracker"
  final val ConfigDispatcherId     = "ConfigDispatcher"
  final val ConfigManagerId        = "ConfigManager"
  final val ConfigServiceManagerId = "ConfigServiceManager"
  final val CtiRouterFactoryId     = "CtiRouterFactory"
  final val CtiStatsAggregatorId   = "CtiStatsAggregator"
  final val DefaultQueueMembershipRepositoryIds =
    "DefaultQueueMembershipRepository"
  final val DevicesTrackerId         = "DevicesTracker"
  final val DirectoryTransformerId   = "DirectoryTransformer"
  final val EventPublisherId         = "EventPublisher"
  final val ExtensionManager         = "ExtensionManager"
  final val GlobalAggregatorId       = "GlobalAggregator"
  final val MainRunnerId             = "MainRunner"
  final val QLogDispatcherId         = "QLogDispatcher"
  final val QLogTransferId           = "QLogTransfer"
  final val QStatCacheId             = "QStatCache"
  final val QuartzActorId            = "QuartzActor"
  final val QueueDispatcherId        = "QueueDispatcher"
  final val QueueEventDispatcherId   = "QueueEventDispatcherId"
  final val StatusPublishId          = "StatusPublish"
  final val UsageEventsId            = "UsageEvents"
  final val UserPreferenceService    = "UserPreferenceService"
  final val VideoEventManager        = "VideoEventManager"
  final val VoiceMailManagerId       = "VoiceMailManager"
  final val XivoAuthenticationId     = "XivoAuthentication"
  final val XivoDirectoryInterfaceId = "XivoDirectoryInterface"
  final val XivoRabbitManagerId      = "XivoEventManager"
  final val XucAmiSupervisor         = "XucAmiSupervisor"
  // Please Alphabetical order

  def ctiFilterActorName(username: String): String = s"${username}CtiFilter"
  def ctiRouterActorName(username: String): String = s"${username}CtiRouter"
  def ctiLinkActorName(username: String): String   = s"${username}CtiLink"
  def ctiRouterPath(ctiRouterFactory: ActorRef, username: String): ActorPath =
    ctiRouterFactory.path / ctiRouterActorName(username)
  def chatLinkActorName(username: String): String = s"${username}ChatLink"
  def queueDispatcherPath(queueDispatcherFactory: ActorRef): ActorPath =
    queueDispatcherFactory.path
}
