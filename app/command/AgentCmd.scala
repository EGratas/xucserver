package command

import org.joda.time.DateTime
import xivo.models.Agent

case class AgentInitLoggedIn(
    id: Agent.Id,
    number: Agent.Number,
    phoneNumber: String,
    loginDate: DateTime
)

case class AgentInitLoggedOut(
    id: Agent.Id,
    number: Agent.Number,
    logoutDate: DateTime
)
