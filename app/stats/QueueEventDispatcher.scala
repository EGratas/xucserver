package stats

import org.apache.pekko.actor.{Actor, ActorLogging}
import com.google.inject.Inject
import models.QueueLog._
import stats.Statistic.{RequestStat, ResetStat}

class QueueEventDispatcher @Inject() (queueStatRepository: QueueStatRepository)
    extends Actor
    with ActorLogging {

  def receive: PartialFunction[Any, Unit] = {
    case EnterQueue(queueName) =>
      queueStatRepository.getQueueStat(queueName) match {
        case Some(actorRef) => actorRef ! EnterQueue(queueName)
        case None =>
          log.info(s"unknown queueName $queueName: creating statistic")
          queueStatRepository.createQueueStat(queueName, context) ! EnterQueue(
            queueName
          )
      }

    case Abandonned(queueName, waitingTime) =>
      queueStatRepository
        .getQueueStat(queueName)
        .foreach(_ ! Abandonned(queueName, waitingTime))

    case Complete(queueName, agentNumber, waitTime, callTime) =>
      queueStatRepository
        .getQueueStat(queueName)
        .foreach(_ ! Complete(queueName, agentNumber, waitTime, callTime))

    case Connect(queueName, agentNumber, waitTime, callId) =>
      queueStatRepository
        .getQueueStat(queueName)
        .foreach(_ ! Connect(queueName, agentNumber, waitTime, callId))

    case Closed(queueName) =>
      queueStatRepository.getQueueStat(queueName).foreach(_ ! Closed(queueName))

    case Timeout(queueName, waitingTime) =>
      queueStatRepository
        .getQueueStat(queueName)
        .foreach(_ ! Timeout(queueName, waitingTime))

    case ExitWithKey(queueName) =>
      queueStatRepository
        .getQueueStat(queueName)
        .foreach(_ ! ExitWithKey(queueName))

    case LeaveEmpty(queueName) =>
      queueStatRepository
        .getQueueStat(queueName)
        .foreach(_ ! LeaveEmpty(queueName))

    case requestStat: RequestStat =>
      log.debug(
        s"Statistics request $requestStat ${queueStatRepository.getAllStats.size}"
      )
      queueStatRepository.getAllStats.foreach(_ ! requestStat)

    case ResetStat =>
      log.info(
        s"Statistics reset requested ${queueStatRepository.getAllStats.size}"
      )
      queueStatRepository.getAllStats.foreach(_ ! ResetStat)
  }
}
