package stats

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import com.codahale.metrics.*
import models.QueueLog.*
import services.XucStatsEventBus.{Stat, StatUpdate}
import stats.Statistic.StatValue
import xivo.models.XivoObject.ObjectDefinition

import java.util.concurrent.TimeUnit

object Statistic {
  type Accumulator = (Long, Long)
  type StatValue   = Double
  type StatCalculator =
    (ObjectEvent, Accumulator) => Option[(Accumulator, StatValue)]

  object StatName extends Enumeration {
    type StatName = Value
    val CallEntered: Statistic.StatName.Value = Value
  }

  private[stats] def statTreeRef(objectDef: ObjectDefinition, statRef: String) =
    s"${objectDef.objectType}.${objectDef.objectRef.get}.$statRef"

  private def TotalNumber(nb: Long): Option[(Accumulator, StatValue)] =
    Some(((nb + 1, 0L), nb.toDouble + 1))
  protected[stats] def Percent(num: Long, denom: Long): Double =
    denom match {
      case 0     => 0
      case demom => num.toDouble / denom.toDouble * 100
    }

  def TotalNumberCallsEntered: StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case evt: EnterQueue => TotalNumber(acc._1)
        case _               => None
      }
  def TotalNumberCallsAbandonned: StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case evt: Abandonned => TotalNumber(acc._1)
        case _               => None
      }
  def TotalNumberCallsAnswered: StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case evt: Connect => TotalNumber(acc._1)
        case _            => None
      }

  def TotalNumberCallsAnsweredBefore(before: Long): StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case Connect(_, _, waitTime, _) if waitTime < before =>
          TotalNumber(acc._1)
        case _ => None
      }
  def TotalNumberCallsAbandonnedAfter(after: Long): StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case Abandonned(_, waitTime) if waitTime > after =>
          TotalNumber(acc._1)
        case _ => None
      }
  def TotalNumberCallsClosed: StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case evt: Closed => TotalNumber(acc._1)
        case _           => None
      }
  def TotalNumberCallsTimeout: StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case evt: Timeout => TotalNumber(acc._1)
        case _            => None
      }

  def TotalNumberCallsNotAnswered: StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case evt: Timeout     => TotalNumber(acc._1)
        case evt: LeaveEmpty  => TotalNumber(acc._1)
        case evt: ExitWithKey => TotalNumber(acc._1)
        case _                => None
      }
  object FractionNumber extends Enumeration {
    type FractionNumber = Value
    val IncDenominator: Statistic.FractionNumber.Value = Value
    val IncNumerator: Statistic.FractionNumber.Value   = Value
    val IncBoth: Statistic.FractionNumber.Value        = Value
  }
  import FractionNumber._
  def RelativeNumberPercentage(
      acc: Accumulator,
      nb: FractionNumber
  ): Option[(Accumulator, StatValue)] = {
    def updateAcc(nb: FractionNumber) =
      nb match {
        case IncBoth        => (acc._1 + 1, acc._2 + 1)
        case IncNumerator   => (acc._1 + 1, acc._2)
        case IncDenominator => (acc._1, acc._2 + 1)
      }
    val (nbOk, totalNb) = updateAcc(nb)
    if (nbOk > totalNb) None
    else Some(((nbOk, totalNb), Percent(nbOk, totalNb)))
  }
  def RelativeNumberPercentageAbandonnedAfter(after: Long): StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case Abandonned(_, waitTime) if waitTime > after =>
          RelativeNumberPercentage(acc, IncNumerator)
        case EnterQueue(_) => RelativeNumberPercentage(acc, IncDenominator)
        case _             => None
      }

  def TotalNumberPercentageAbandonned: StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case Abandonned(_, _) =>
          RelativeNumberPercentage(acc, IncNumerator)
        case EnterQueue(_) => RelativeNumberPercentage(acc, IncDenominator)
        case _             => None
      }
  def RelativeNumberPercentageAnsweredBefore(before: Long): StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case Connect(_, _, waitTime, _) if waitTime < before =>
          RelativeNumberPercentage(acc, IncNumerator)
        case EnterQueue(_) => RelativeNumberPercentage(acc, IncDenominator)
        case _             => None
      }

  def TotalNumberPercentageAnswered: StatCalculator =
    (event: ObjectEvent, acc: Accumulator) =>
      event match {
        case Connect(_, _, _, _) =>
          RelativeNumberPercentage(acc, IncNumerator)
        case EnterQueue(_) => RelativeNumberPercentage(acc, IncDenominator)
        case _             => None
      }

  case class RequestStat(requester: ActorRef, objDef: ObjectDefinition)
  case object ResetStat
  case object PublishSlidingStats

}
import stats.Statistic.StatCalculator

trait SlidingStatistic {
  this: Statistic =>

  val SlidingTime: (Int, TimeUnit) = (60, java.util.concurrent.TimeUnit.MINUTES)

  private def register[T <: Metric](name: String, metric: T): T = {
    metricRegistry.remove(name)
    metricRegistry.register(name, metric)
  }

  private[stats] val slidingWindow = new Histogram(
    new SlidingTimeWindowReservoir(SlidingTime._1, SlidingTime._2)
  )
  private[stats] val windowHistory = register(
    s"${Statistic.statTreeRef(objDef, statRef)}.slidingHour",
    slidingWindow
  )
  private[stats] val lastHour = register(
    s"${Statistic.statTreeRef(objDef, statRef)}.lastHour",
    new DoubleGauge()
  )

}

trait HistogramCalculation {
  def UpdateWindow(value: StatValue): Unit
  def getHistoValue: StatValue
  def valueChanged: Boolean
  def updateHisto: Unit
}

trait HistoSize extends HistogramCalculation {
  this: SlidingStatistic with Statistic =>

  def UpdateWindow(value: StatValue): Unit = {
    windowHistory.update(1)
    lastHour.setValue(windowHistory.getSnapshot.size())
  }
  def getHistoValue: Double = windowHistory.getSnapshot.size()

  def valueChanged: Boolean = getHistoValue != lastHour.getValue.toInt

  def updateHisto: Unit = lastHour.setValue(windowHistory.getSnapshot.size())
}

trait HistoValue extends HistogramCalculation {
  this: Statistic with SlidingStatistic =>

  def UpdateWindow(value: StatValue): Unit = {
    log.debug(s"update window $value : ${value.toLong}")
    windowHistory.update(value.toLong)
    lastHour.setValue(windowHistory.getSnapshot.getMean)
  }
  def getHistoValue: StatValue = windowHistory.getSnapshot.getMean

  def valueChanged: Boolean = getHistoValue != lastHour.getValue

  def updateHisto: Unit = lastHour.setValue(windowHistory.getSnapshot.getMean)

}

class Statistic(
    val statRef: String,
    val objDef: ObjectDefinition,
    calculator: StatCalculator,
    aggregator: ActorRef,
    val metricRegistry: MetricRegistry
) extends Actor
    with ActorLogging
    with SlidingStatistic {
  this: HistogramCalculation =>
  import Statistic._

  private val statValue = metricRegistry.register(
    s"${statTreeRef(objDef, statRef)}.count",
    new DoubleGauge()
  )

  protected[stats] var acc: Accumulator = (0, 0)

  def receive: PartialFunction[Any, Unit] = {

    case event: ObjectEvent =>
      calculator(event, acc) match {
        case Some((newAcc, value)) =>
          log.debug(s"$value $statValue")
          statValue.setValue(value)
          UpdateWindow(value)
          acc = newAcc
          val statUpdate =
            StatUpdate(objDef, List[Stat](Stat(statRef, statValue.getValue)))
          log.debug("SendingToAggregator " + aggregator.path)
          aggregator ! statUpdate
          val statHistoUpdate = StatUpdate(
            objDef,
            List[Stat](Stat(s"${statRef}LastHour", getHistoValue))
          )
          aggregator ! statHistoUpdate
        case unknown =>
      }

    case ResetStat =>
      log.info(s"$statRef:${statValue.getValue} reset value requested")
      statValue.setValue(0.0)
      acc = (0, 0)
      aggregator ! StatUpdate(
        objDef,
        List[Stat](Stat(statRef, statValue.getValue))
      )

    case PublishSlidingStats =>
      if (valueChanged) {
        log.debug(s"$statRef:${statValue.getValue} value changed")
        updateHisto
        val statHistoUpdate = StatUpdate(
          objDef,
          List[Stat](Stat(s"${statRef}LastHour", getHistoValue))
        )
        aggregator ! statHistoUpdate
      }

    case unknown =>
  }

}
