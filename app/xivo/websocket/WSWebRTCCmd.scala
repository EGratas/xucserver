package xivo.websocket

import play.api.libs.json._
import xivo.websocket.WSWebRTCCmd.toJson

abstract sealed trait WSWebRTCCmd {
  val name: String
  val sipCallId: Option[String]
}

object WSWebRTCCmd {
  def CtiCommand(cmd: String): JsObject = Json.obj("command" -> JsString(cmd))
  implicit val writes: Writes[WSWebRTCCmd] = new Writes[WSWebRTCCmd] {
    def writes(cmd: WSWebRTCCmd): JsValue = {
      cmd match {
        case cmd: WebRTCSendDtmfCmd =>
          WSWebRTCCmd.CtiCommand(cmd.name) ++ Json.obj(
            "key" -> JsString(cmd.key.toString)
          )
        case _ =>
          cmd.sipCallId
            .map(c =>
              WSWebRTCCmd.CtiCommand(cmd.name) ++ Json.obj(
                "sipCallId" -> JsString(c)
              )
            )
            .getOrElse(WSWebRTCCmd.CtiCommand(cmd.name))
      }
    }
  }
  def toJson(cmd: WSWebRTCCmd): JsValue = Json.toJson(cmd)
}

case class WebRTCHoldCmd(sipCallId: Option[String]) extends WSWebRTCCmd {
  override val name = "Hold"
}
object WebRTCHoldCmd {
  implicit val cmdWrites: Writes[WebRTCHoldCmd] = (cmd: WebRTCHoldCmd) =>
    toJson(cmd)
}

case class WebRTCAnswerCmd(sipCallId: Option[String]) extends WSWebRTCCmd {
  override val name = "Answer"
}
object WebRTCAnswerCmd {
  implicit val cmdWrites: Writes[WebRTCAnswerCmd] = (cmd: WebRTCAnswerCmd) =>
    toJson(cmd)
}

case class WebRTCSendDtmfCmd(key: Char) extends WSWebRTCCmd {
  override val name                      = "SendDtmf"
  override val sipCallId: Option[String] = None
}
object WebRTCSendDtmfCmd {
  implicit val cmdWrites: Writes[WebRTCSendDtmfCmd] =
    (cmd: WebRTCSendDtmfCmd) => toJson(cmd)
}

case class WebRTCMuteCmd(sipCallId: Option[String]) extends WSWebRTCCmd {
  override val name = "ToggleMicrophone"
}
object WebRTCMuteCmd {
  implicit val cmdWrites: Writes[WebRTCMuteCmd] = (cmd: WebRTCMuteCmd) =>
    toJson(cmd)
}
