package xivo.websocket

import models._
import services.XucStatsEventBus.AggregatedStatEvent
import services.agent.AgentStatistic
import services.config.ConfigDispatcher._
import services.request._
import services.video.model.{VideoInvitationEvent, VideoStatusEvent}
import xivo.events.{AgentState, PhoneHintStatusEvent}
import xivo.models.{
  Agent,
  AgentQueueMember,
  CallHistory,
  QueueConfigUpdate,
  RichCallHistory
}
import xivo.websocket.WsBus.WsContent
import xivo.services.AuthenticationToken

class WsEncoder {

  def encode(msg: Any): Option[WsContent] =
    msg match {
      case agentState: AgentState =>
        Some(WsContent(WebSocketEvent.createEvent(agentState)))

      case agent: Agent =>
        Some(WsContent(WebSocketEvent.createEvent(agent)))

      case agentQM: AgentQueueMember =>
        Some(WsContent(WebSocketEvent.createEvent(agentQM)))

      case queue: QueueConfigUpdate =>
        Some(WsContent(WebSocketEvent.createEvent(queue)))

      case QueueList(queues) =>
        Some(WsContent(WebSocketEvent.createQueuesEvent(queues)))

      case AgentList(agents) =>
        Some(WsContent(WebSocketEvent.createAgentListEvent(agents)))

      case AgentGroupList(agentGroups) =>
        Some(WsContent(WebSocketEvent.createAgentGroupsEvent(agentGroups)))

      case AgentQueueMemberList(agentQueueMembers) =>
        Some(
          WsContent(
            WebSocketEvent.createAgentQueueMembersEvent(agentQueueMembers)
          )
        )

      case ad: AgentDirectory =>
        Some(WsContent(WebSocketEvent.createEvent(ad)))

      case aggregatedStat: AggregatedStatEvent =>
        Some(WsContent(WebSocketEvent.createEvent(aggregatedStat)))

      case MeetmeList(meetmeList) =>
        Some(WsContent(WebSocketEvent.createConferenceListEvent(meetmeList)))

      case agentStat: AgentStatistic =>
        Some(WsContent(WebSocketEvent.createEvent(agentStat)))

      case queueCalls: QueueCallList =>
        Some(WsContent(WebSocketEvent.createEvent(queueCalls)))

      case history: CallHistory =>
        Some(WsContent(WebSocketEvent.createEvent(history)))

      case richHistory: RichCallHistory =>
        Some(WsContent(WebSocketEvent.createEvent(richHistory)))

      case cbLists: CallbackLists =>
        Some(WsContent(WebSocketEvent.createEvent(cbLists)))

      case cbrList: FindCallbackResponseWithId =>
        Some(WsContent(WebSocketEvent.createEvent(cbrList)))

      case cbUpdate: CallbackRequestUpdated =>
        Some(WsContent(WebSocketEvent.createEvent(cbUpdate)))

      case periodLists: PreferredCallbackPeriodList =>
        Some(WsContent(WebSocketEvent.createEvent(periodLists)))

      case cbTaken: CallbackTaken =>
        Some(WsContent(WebSocketEvent.createEvent(cbTaken)))

      case cbReleased: CallbackReleased =>
        Some(WsContent(WebSocketEvent.createEvent(cbReleased)))

      case cbStarted: CallbackStarted =>
        Some(WsContent(WebSocketEvent.createEvent(cbStarted)))

      case cbClotured: CallbackClotured =>
        Some(WsContent(WebSocketEvent.createEvent(cbClotured)))

      case lineCfg: LineConfig =>
        Some(WsContent(WebSocketEvent.createEvent(lineCfg)))

      case uqm: UserQueueDefaultMembership =>
        Some(WsContent(WebSocketEvent.createEvent(uqm)))

      case uqm: UsersQueueDefaultMembership =>
        Some(WsContent(WebSocketEvent.createEvent(uqm)))

      case o: PhoneHintStatusEvent =>
        Some(WsContent(WebSocketEvent.createEvent(o)))

      case v: VideoStatusEvent =>
        Some(WsContent(WebSocketEvent.createEvent(v)))

      case v: VideoInvitationEvent =>
        Some(WsContent(WebSocketEvent.createEvent(v)))

      case o: AuthenticationToken =>
        Some(WsContent(WebSocketEvent.createEvent(o)))

      case cchResp: CustomerCallHistoryResponseWithId =>
        Some(WsContent(WebSocketEvent.createEvent(cchResp)))

      case udn: UserDisplayName =>
        Some(WsContent(WebSocketEvent.createEvent(udn)))

      case iceCfg: IceConfig =>
        Some(WsContent(WebSocketEvent.createEvent(iceCfg)))

      case unknown => None
    }

}
