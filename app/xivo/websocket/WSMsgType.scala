package xivo.websocket

import org.xivo.cti.message.Sheet
import play.api.libs.json.*
import xivo.websocket

object WSMsgType extends Enumeration {

  type WSMsgType = Value
  val AgentLogin: websocket.WSMsgType.Value                        = Value
  val AgentConfig: websocket.WSMsgType.Value                       = Value
  val AgentDirectory: websocket.WSMsgType.Value                    = Value
  val AgentError: websocket.WSMsgType.Value                        = Value
  val AgentGroupList: websocket.WSMsgType.Value                    = Value
  val AgentList: websocket.WSMsgType.Value                         = Value
  val AgentListen: websocket.WSMsgType.Value                       = Value
  val AgentLogout: websocket.WSMsgType.Value                       = Value
  val AgentPaused: websocket.WSMsgType.Value                       = Value
  val AgentReady: websocket.WSMsgType.Value                        = Value
  val AgentStateEvent: websocket.WSMsgType.Value                   = Value
  val AgentStatistics: websocket.WSMsgType.Value                   = Value
  val AuthenticationToken: websocket.WSMsgType.Value               = Value
  val CallbackClotured: websocket.WSMsgType.Value                  = Value
  val CallbackLists: websocket.WSMsgType.Value                     = Value
  val CallbackReleased: websocket.WSMsgType.Value                  = Value
  val CallbackRequestUpdated: websocket.WSMsgType.Value            = Value
  val CallbackStarted: websocket.WSMsgType.Value                   = Value
  val CallbackTaken: websocket.WSMsgType.Value                     = Value
  val CallHistory: websocket.WSMsgType.Value                       = Value
  val RichCallHistory: websocket.WSMsgType.Value                   = Value
  val ConferenceCommandError: websocket.WSMsgType.Value            = Value
  val ConferenceEvent: websocket.WSMsgType.Value                   = Value
  val ConferenceList: websocket.WSMsgType.Value                    = Value
  val ConferenceParticipantEvent: websocket.WSMsgType.Value        = Value
  val CurrentCallsPhoneEvents: websocket.WSMsgType.Value           = Value
  val CustomerCallHistoryResponseWithId: websocket.WSMsgType.Value = Value
  val DirectoryResult: websocket.WSMsgType.Value                   = Value
  val Error: websocket.WSMsgType.Value                             = Value
  val Favorites: websocket.WSMsgType.Value                         = Value
  val FavoriteUpdated: websocket.WSMsgType.Value                   = Value
  val FindCallbackResponseWithId: websocket.WSMsgType.Value        = Value
  val FlashTextEvent: websocket.WSMsgType.Value                    = Value
  val IceConfig: websocket.WSMsgType.Value                         = Value
  val LineConfig: websocket.WSMsgType.Value                        = Value
  val LineStateEvent: websocket.WSMsgType.Value                    = Value
  val LinkStatusUpdate: websocket.WSMsgType.Value                  = Value
  val LoggedOn: websocket.WSMsgType.Value                          = Value
  val PhoneAvailable: websocket.WSMsgType.Value                    = Value
  val PhoneCalling: websocket.WSMsgType.Value                      = Value
  val PhoneEvent: websocket.WSMsgType.Value                        = Value
  var PhoneHintStatusEvent: websocket.WSMsgType.Value              = Value
  val PhoneRinging: websocket.WSMsgType.Value                      = Value
  val PhoneStatusUpdate: websocket.WSMsgType.Value                 = Value
  val PreferredCallbackPeriodList: websocket.WSMsgType.Value       = Value
  val QueueCalls: websocket.WSMsgType.Value                        = Value
  val QueueConfig: websocket.WSMsgType.Value                       = Value
  val QueueList: websocket.WSMsgType.Value                         = Value
  val QueueMember: websocket.WSMsgType.Value                       = Value
  val QueueMemberList: websocket.WSMsgType.Value                   = Value
  val QueueStatistics: websocket.WSMsgType.Value                   = Value
  val RightProfile: websocket.WSMsgType.Value                      = Value
  val Sheet: Value                                                 = Value(classOf[Sheet].getSimpleName)
  val UserConfigUpdate: websocket.WSMsgType.Value                  = Value
  val UserDisplayName: websocket.WSMsgType.Value                   = Value
  val UserPreference: websocket.WSMsgType.Value                    = Value
  val UserQueueDefaultMembership: websocket.WSMsgType.Value        = Value
  val UsersQueueDefaultMembership: websocket.WSMsgType.Value       = Value
  val UsersStatuses: websocket.WSMsgType.Value                     = Value
  val CtiStatuses: websocket.WSMsgType.Value                       = Value
  val UserStatusUpdate: websocket.WSMsgType.Value                  = Value
  val VoiceMailStatusUpdate: websocket.WSMsgType.Value             = Value
  val WebRTCCmd: websocket.WSMsgType.Value                         = Value
  val VideoStatusEvent: websocket.WSMsgType.Value                  = Value
  val MeetingRoomInvite: websocket.WSMsgType.Value                 = Value
  val MeetingRoomInviteAck: websocket.WSMsgType.Value              = Value
  val MeetingRoomInviteResponse: websocket.WSMsgType.Value         = Value

  implicit val wsMsgTypeWrites: Writes[WSMsgType] = (WSMsgType: WSMsgType) =>
    JsString(WSMsgType.toString)
}

object LinkState extends Enumeration {
  type LinkState = Value
  val down, up = Value
}
import xivo.websocket.LinkState._

case class LinkStatusUpdate(status: LinkState)

object LinkStatusUpdate {

  implicit val linkStatusWrites: Writes[LinkStatusUpdate] =
    (linkStatus: LinkStatusUpdate) =>
      Json.obj(
        "status" -> linkStatus.status.toString
      )
  def toJson(linkStatus: LinkStatusUpdate): JsValue = Json.toJson(linkStatus)
}
