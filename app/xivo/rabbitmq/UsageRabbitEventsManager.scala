package xivo.rabbitmq

import org.apache.pekko.actor.{Actor, ActorLogging}
import com.google.inject.Inject
import models.usm.LoginEvent

class UsageRabbitEventsManager @Inject() (
    producer: UsageRabbitEventsProducer
) extends Actor
    with ActorLogging {

  override def preStart(): Unit = {
    try {
      log.debug(s"Creating connection to XiVO RabbitMQ")
      producer.createConnection()
    } catch {
      case e: Exception => log.error(s"Exception in preStart: ${e.getMessage}")
    }
  }

  override def receive: Receive = {
    case event: LoginEvent =>
      producer.publishMessage(event)
    case _ =>
  }

  override def postStop(): Unit = {
    try {
      log.debug("Closing connection to XiVO RabbitMQ")
      producer.shutdownConnection()
    } catch {
      case e: Exception => log.error(s"Exception in postStop: ${e.getMessage}")
    }
  }
}
