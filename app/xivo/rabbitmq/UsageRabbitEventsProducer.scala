package xivo.rabbitmq

import com.google.inject.Inject
import com.rabbitmq.client.ConnectionFactory
import models.usm.LoginEvent
import play.api.libs.json.Json
import play.api.libs.json.Json.toBytes
import xivo.xuc.RabbitConfigGenerator

import scala.util.{Failure, Try}
import xivo.xuc.RabbitConfig

class UsageRabbitEventsProducer @Inject() (
    eventBusConnectionFactory: ConnectionFactory,
    eventBusConfigGenerator: RabbitConfigGenerator
) extends AbstractRabbitEventsFactory
    with RabbitPublish {

  val connection: ConnectionFactory = eventBusConnectionFactory
  val config: RabbitConfig          = eventBusConfigGenerator.rabbitUsageConfig()
  this.configureBus(config)

  val queue: String            = config.queueName.get
  val queueDurability: Boolean = config.queueDurability.get
  val queueRestricted: Boolean = config.queueRestricted.get
  val queueDelete: Boolean     = config.queueDelete.get

  override def createConnection(): Try[Unit] = {
    for {
      c  <- Try(connection.newConnection)
      ch <- Try(c.createChannel)
      qn = ch
        .queueDeclare(
          queue,
          queueDurability,
          queueRestricted,
          queueDelete,
          null
        )
        .getQueue
    } yield {
      this.busConnection = c
      this.channel = ch
      this.queueName = qn
    }
  }

  override def shutdownConnection(): Unit = {
    this.channel.queueDelete(this.queueName)
    this.channel.close()
    this.busConnection.close()
  }

  override def publishMessage(message: LoginEvent): Unit = {
    val msg: Array[Byte] = toBytes(
      Json.obj(
        LoginEvent.getClass.getSimpleName.split('$').head -> Json.toJson(
          message
        )
      )
    )
    Try {
      this.channel.basicPublish(
        "",
        "usage.event_login",
        null,
        msg
      )
    } match {
      case Failure(e) =>
        log.error(
          s"""Exception while writting into rabbitmq: ${e.getMessage}"""
        )
      case _ =>
    }
  }
}
