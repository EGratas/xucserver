package xivo.rabbitmq

import com.rabbitmq.client.{Channel, Connection, ConnectionFactory}
import models.usm.LoginEvent
import play.api.Logger
import xivo.rabbitmq.RabbitEventsFactory.QueueName
import xivo.xuc.RabbitConfig

import scala.util.Try

private object RabbitEventsFactory {
  type QueueName = String
}

abstract class AbstractRabbitEventsFactory {

  val connection: ConnectionFactory
  val log: Logger = Logger(getClass.getName)

  def configureBus(rabbitConfig: RabbitConfig): Unit = {
    connection.setUsername(rabbitConfig.username)
    connection.setPassword(rabbitConfig.password)
    connection.setVirtualHost(rabbitConfig.rabbitVirtualHost)
    connection.setHost(rabbitConfig.rabbitHost)
    connection.setPort(rabbitConfig.rabbitPort)
  }

  protected[rabbitmq] var busConnection: Connection = _
  protected[rabbitmq] var channel: Channel          = _
  protected[rabbitmq] var queueName: QueueName      = _

  def createConnection(): Try[Unit]
  def shutdownConnection(): Unit
}

trait RabbitPublish {
  def publishMessage(message: LoginEvent): Unit
}
