package xivo.rabbitmq

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef, Cancellable}
import com.google.inject.Inject
import com.google.inject.name.Named
import services.config.ConfigDispatcher.RefreshLine
import services.config.ConfigInitializer.LoadAgentQueueMembers
import services.config.ConfigServerRequester
import services.config.ConfigServiceManager.GetCtiStatuses
import services.request.{
  MobilePushTokenAdded,
  MobilePushTokenDeleted,
  UserPreferenceCreated,
  UserPreferenceDeleted,
  UserPreferenceEdited
}
import services.{ActorIds, XucEventBus}
import xivo.events.cti.models.{CtiStatusLegacyMapWrapper, CtiStatusMapWrapper}
import xivo.models._
import xivo.rabbitmq.XivoRabbitEventsManager.{Connect, LoginTimeout}
import xivo.xucami.AmiSupervisor

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.util.{Failure, Success}

object XivoRabbitEventsManager {
  case object Connect
  case object LoginTimeout
  case object LoginTimeoutException extends Exception

}

class XivoRabbitEventsManager @Inject() (
    configRequester: ConfigServerRequester,
    rabbitFactory: XivoRabbitEventsFactory,
    bus: XucEventBus,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
    @Named(ActorIds.XucAmiSupervisor) amiSupervisor: ActorRef,
    @Named(ActorIds.ConfigServiceManagerId) configServiceManager: ActorRef
) extends Actor
    with ActorLogging {

  private[rabbitmq] var loginTimeout: Cancellable                   = null
  private[rabbitmq] val initialLoginTimeoutDuration: FiniteDuration = 5.seconds
  private[rabbitmq] var loginTimeoutDuration: FiniteDuration =
    initialLoginTimeoutDuration

  override def preStart(): Unit = {
    self ! Connect
  }

  override def receive: Receive = {
    case Connect =>
      loginTimeout = context.system.scheduler.scheduleOnce(
        loginTimeoutDuration,
        self,
        LoginTimeout
      )
      Future(rabbitFactory.createConnection()).map {
        case Failure(e) =>
          log.warning(
            s"Failed to initialize connection to RabbitMQ. ${e.getMessage}"
          )
        case Success(_) =>
          log.info(s"Successfully connected to RabbitMQ")
          loginTimeout.cancel()
          loginTimeoutDuration = initialLoginTimeoutDuration
          rabbitFactory.addShutdownListener(self)
          new XivoRabbitEventsConsumer(self, rabbitFactory)
      }

    case LoginTimeout =>
      log.error(
        s"RabbitMQ connection attempt timed out after ${loginTimeoutDuration.toString()}, retrying..."
      )
      if (loginTimeoutDuration < initialLoginTimeoutDuration * 6)
        loginTimeoutDuration += initialLoginTimeoutDuration
      self ! Connect

    case RabbitEventConnectionClosed =>
      log.warning(
        s"RabbitMQ connection was closed unexpectedly, trying to reconnect."
      )
      rabbitFactory.removeShutdownListener()
      self ! Connect

    case event: RabbitEventQueueCreated =>
      val action = "getQueueConfigCreated"
      configRequester
        .getQueueConfig(event.data.id)
        .onComplete({
          case Success(queueConfig) =>
            configDispatcher ! queueConfig
            configDispatcher ! LoadAgentQueueMembers
          case Failure(e) => log.error(s"Error $action: ${e.getMessage}")
        })
    case event: RabbitEventQueueEdited =>
      val action = "getQueueConfigUpdate"
      configRequester
        .getQueueConfig(event.data.id)
        .onComplete({
          case Success(queueConfig) =>
            configDispatcher ! queueConfig
            configDispatcher ! LoadAgentQueueMembers
          case Failure(e) => log.error(s"Error $action: ${e.getMessage}")
        })
    case event: RabbitEventAgentEdited =>
      val action = "getAgentConfigUpdate"
      configRequester
        .getAgentConfig(event.data.id)
        .onComplete({
          case Success(agentConfigUpdate) =>
            configDispatcher ! agentConfigUpdate
          case Failure(e) => log.error(s"Error $action: ${e.getMessage}")
        })
    case event: RabbitEventAgentDeleted =>
      configDispatcher ! RemoveAgentQueueMember(event.data.id)
    case event: RabbitEventAgentCreated =>
      val action = "getAgentConfigUpdate"
      configRequester
        .getAgentConfig(event.data.id)
        .onComplete({
          case Success(agentConfigUpdate) =>
            configDispatcher ! agentConfigUpdate
          case Failure(e) => log.error(s"Error $action: ${e.getMessage}")
        })
    case e: RabbitEventMediaServerCreated =>
      amiSupervisor ! AmiSupervisor.LoadOrReloadMds(e.data.id)
    case e: RabbitEventMediaServerEdited =>
      amiSupervisor ! AmiSupervisor.LoadOrReloadMds(e.data.id)
    case e: RabbitEventMediaServerDeleted =>
      amiSupervisor ! AmiSupervisor.DeleteMds(e.data.id)
    case event: RabbitEventSipEndpointEdited =>
      configDispatcher ! RefreshLine(SipEndpoint(event.data.id))

    case event: RabbitEventUserServicesEdited =>
      log.debug(s"Received $event")
      configRequester
        .getUserServices(event.data.id)
        .onComplete({
          case Success(userServices) =>
            log.debug(s"Received $userServices")
            configDispatcher ! UserServicesUpdated(
              event.data.id.toInt,
              userServices
            )
          case Failure(e) =>
            log.error(s"Error EventUserServiceUpdated: ${e.getMessage}")
        })

    case event: RabbitEventSipConfigEdited =>
      log.debug(s"Received $event")
      configRequester.getIceServer
        .onComplete({
          case Success(iceCfg) =>
            configDispatcher ! iceCfg
          case Failure(e) =>
            log.error(
              s"Failed to retrieve ICE configuration from configuration server: ${e.getMessage}"
            )
        })

    case event: RabbitEventUserConfigEdited =>
      log.debug(s"Received $event")
      configDispatcher ! UserConfigUpdated(event.data.id)

    case event: RabbitEventUserPreferenceCreated =>
      log.debug(s"Received $event")
      configDispatcher ! UserPreferenceCreated(event.data.id)

    case event: RabbitEventUserPreferenceEdited =>
      log.debug(s"Received $event")
      configDispatcher ! UserPreferenceEdited(event.data.id)

    case event: RabbitEventUserPreferenceDeleted =>
      log.debug(s"Received $event")
      configDispatcher ! UserPreferenceDeleted(event.data.id)

    case event: RabbitEventMobilePushTokenAdded =>
      log.debug(s"Received $event")
      configDispatcher ! MobilePushTokenAdded(event.data.id)

    case event: RabbitEventMobilePushTokenDeleted =>
      log.debug(s"Received $event")
      configDispatcher ! MobilePushTokenDeleted(event.data.id)

    case event: RabbitEventWebserviceUserCreated =>
      log.debug(s"Received $event")
      configDispatcher ! WebserviceUserActionCreated(event.data.login)

    case event: RabbitEventWebserviceUserEdited =>
      log.debug(s"Received $event")
      configDispatcher ! WebserviceUserActionEdited(event.data.login)

    case event: RabbitEventWebserviceUsersReload =>
      log.debug(s"Received $event")
      configDispatcher ! WebserviceUsersActionReload
    // Reception groupped with the RabbitEventCtiStatusReload , RabbitEventCtiStatusReload ask for the reload to the configServiceManager
    // and CtiStatusMapWrapper send the answer to the configDispatcher with the new data
    case event: RabbitEventCtiStatusReload =>
      log.debug(s"Received $event")
      configServiceManager ! GetCtiStatuses
    // Reception groupped with the RabbitEventCtiStatusReload , RabbitEventCtiStatusReload ask for the reload to the configServiceManager
    // and CtiStatusMapWrapper send the answer to the configDispatcher with the new data
    case event: CtiStatusMapWrapper =>
      configDispatcher ! event
    // Reception groupped with the RabbitEventCtiStatusReload , RabbitEventCtiStatusReload ask for the reload to the configServiceManager
    // and CtiStatusMapWrapper send the answer to the configDispatcher with the new data
    // Deprecated
    case event: CtiStatusLegacyMapWrapper =>
      configDispatcher ! event

    case unknown =>
      log.debug(s"Unknown $unknown message received")
  }

  override def postStop(): Unit = {
    try {
      log.debug(s"Closing connection to XiVO RabbitMQ")
      rabbitFactory.shutdownConnection()
    } catch {
      case e: Exception => log.error(s"Exception in postStop: ${e.getMessage}")
    }
  }
}
