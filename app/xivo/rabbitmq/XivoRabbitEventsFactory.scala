package xivo.rabbitmq

import org.apache.pekko.actor.ActorRef
import com.google.inject.Inject
import com.rabbitmq.client.{
  Connection,
  ConnectionFactory,
  ShutdownListener,
  ShutdownSignalException
}
import play.api.Logger
import xivo.models.RabbitEventConnectionClosed
import xivo.xuc.RabbitConfigGenerator

import scala.util.Try
import xivo.xuc.RabbitConfig

class ShutdownListenerWithRef(ref: ActorRef, currentConnection: Connection)
    extends ShutdownListener {
  val log: Logger = Logger(getClass.getName)
  def apply: ShutdownListenerWithRef =
    new ShutdownListenerWithRef(ref: ActorRef, currentConnection: Connection)
  override def shutdownCompleted(cause: ShutdownSignalException): Unit =
    shutdownCallback(cause, ref)

  def shutdownCallback(cause: ShutdownSignalException, ref: ActorRef): Unit = {
    log.error(cause.getMessage)
    if (!cause.isInitiatedByApplication)
      ref ! RabbitEventConnectionClosed
  }
}

class XivoRabbitEventsFactory @Inject() (
    eventBusConnectionFactory: ConnectionFactory,
    eventBusConfigGenerator: RabbitConfigGenerator
) extends AbstractRabbitEventsFactory {

  val connection: ConnectionFactory                     = eventBusConnectionFactory
  val config: RabbitConfig                              = eventBusConfigGenerator.rabbitXivoConfig()
  var shutdownListener: Option[ShutdownListenerWithRef] = None
  this.configureBus(config)

  val exchangeName: String      = config.exchange.get
  val routingKeys: List[String] = config.routingKeys.get
  val consumerTag: String       = config.consumerTag.get

  def createConnection(): Try[Unit] = {
    for {
      c  <- Try(connection.newConnection)
      ch <- Try(c.createChannel)
      qn = ch.queueDeclare().getQueue
    } yield {
      this.busConnection = c
      this.channel = ch
      this.queueName = qn
      routingKeys.foreach { routingKey =>
        channel.queueBind(queueName, exchangeName, routingKey, null) match {
          case e: Exception =>
            log.error(
              s"Failed to bind the queue $queueName to routing key $routingKey: ${e.getMessage}"
            )
          case _ =>
        }
      }
    }
  }

  def addShutdownListener(ref: ActorRef): Unit = {
    shutdownListener match {
      case None =>
        val listener = new ShutdownListenerWithRef(ref, this.busConnection)
        shutdownListener = Some(listener)
        this.busConnection.addShutdownListener(listener)
      case Some(_) =>
        log.warn(
          s"Trying to add shutdown listener on RabbitMQ connection but it is already registered"
        )
    }
  }

  def removeShutdownListener(): Unit = {
    shutdownListener match {
      case Some(listener: ShutdownListenerWithRef) =>
        this.busConnection.removeShutdownListener(listener)
        shutdownListener = None
      case None =>
        log.warn(
          s"Trying to remove shutdown listener on RabbitMQ connection but none were set previously"
        )
    }
  }

  def shutdownConnection(): Unit = {
    routingKeys.foreach(routingKey =>
      this.channel.queueUnbind(queueName, exchangeName, routingKey)
    )
    this.channel.queueDelete(queueName)
    this.channel.close()
    this.busConnection.close()
  }
}
