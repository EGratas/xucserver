package xivo.data

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import com.google.inject.Inject
import com.google.inject.name.Named
import models.QueueLog
import services.ActorIds
import stats.Statistic.ResetStat

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object QLogTransfer {
  case class LoadQueueLogs(clause: String)
}

class QLogTransfer @Inject() (
    queueLog: QueueLog,
    @Named(ActorIds.QLogDispatcherId) qLogDispatcher: ActorRef
) extends Actor
    with ActorLogging {
  import QLogTransfer.LoadQueueLogs

  protected[data] var nextClause: String = queueLog.defaultClause

  log.info(s"starting transfert with clause : $nextClause")

  override def preStart(): Unit = {
    qLogDispatcher ! ResetStat
    context.system.scheduler.scheduleOnce(
      1.second,
      self,
      LoadQueueLogs(nextClause)
    )
  }

  def receive: PartialFunction[Any, Unit] = { case LoadQueueLogs(clause) =>
    val qLogs = queueLog.getAll(clause)
    qLogs.foreach(qd => log.debug(s"$qd"))
    qLogs.foreach(qLogDispatcher ! _)
    if (qLogs.size > 0) nextClause = s"'${qLogs.last.queuetime}'"
    context.system.scheduler.scheduleOnce(
      1.second,
      self,
      LoadQueueLogs(nextClause)
    )

  }

}
