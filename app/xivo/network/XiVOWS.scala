package xivo.network

import org.apache.pekko.stream.Materializer
import com.google.inject.{ImplementedBy, Inject}
import play.api.http.ContentTypeOf
import play.api.libs.json.JsValue
import play.api.libs.ws._
import xivo.xuc.XucBaseConfig

import scala.concurrent.duration._

@ImplementedBy(classOf[XiVOWSImpl])
trait XiVOWS {
  def WS: WSClient
  def xucConfig: XucBaseConfig
  def getWsUrl(resource: String): String
  def withWS(resource: String): WSRequest
  def post(
      host: String,
      uri: String,
      payload: Option[JsValue] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest
  def genericPost(
      host: String,
      uri: String,
      payload: Option[String] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest
  def get(
      host: String,
      uri: String,
      payload: Option[JsValue] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest
  def put(
      host: String,
      uri: String,
      payload: Option[JsValue] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest
  def del(
      host: String,
      uri: String,
      payload: Option[JsValue] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest
}

class XiVOWSImpl @Inject() (val xucConfig: XucBaseConfig, wSClient: WSClient)(
    implicit mat: Materializer
) extends XiVOWS {

  val WS: WSClient = wSClient

  def getWsUrl(resource: String): String =
    s"https://${xucConfig.XivoWs_host}:${xucConfig.XivoWs_port}/1.1/$resource"

  def withWS(resource: String): WSRequest = {
    xucConfig.XivoWs_confdToken match {
      case Some(token) =>
        WS.url(getWsUrl(resource) + "")
          .withHttpHeaders(
            ("Content-Type", "application/json"),
            ("Accept", "application/json"),
            ("X-Auth-Token", token)
          )
          .withRequestTimeout(xucConfig.WsRequestTimeout.millis)
      case None =>
        WS.url(getWsUrl(resource))
          .withHttpHeaders(
            ("Content-Type", "application/json"),
            ("Accept", "application/json")
          )
          .withAuth(
            xucConfig.XivoWs_wsUser,
            xucConfig.XivoWs_wsPwd,
            WSAuthScheme.DIGEST
          )
          .withRequestTimeout(xucConfig.WsRequestTimeout.millis)
    }
  }

  def post(
      host: String,
      uri: String,
      payload: Option[JsValue] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest = {
    request(host, uri, payload, user, password, headers, port, protocol)
      .withMethod("POST")
  }

  def genericPost(
      host: String,
      uri: String,
      payload: Option[String] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest = {
    request(host, uri, payload, user, password, headers, port, protocol)
      .withMethod("POST")
  }

  def get(
      host: String,
      uri: String,
      payload: Option[JsValue] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest = {
    request(host, uri, payload, user, password, headers, port, protocol)
      .withMethod("GET")
  }

  def put(
      host: String,
      uri: String,
      payload: Option[JsValue] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest = {
    request(host, uri, payload, user, password, headers, port, protocol)
      .withMethod("PUT")
  }

  def del(
      host: String,
      uri: String,
      payload: Option[JsValue] = None,
      user: Option[String] = None,
      password: Option[String] = None,
      headers: Map[String, String] = Map(),
      port: Option[Int] = None,
      protocol: String = xucConfig.defaultProtocol
  ): WSRequest = {
    request(host, uri, payload, user, password, headers, port, protocol)
      .withMethod("DELETE")
  }

  private def request[T](
      host: String,
      uri: String,
      payload: Option[T],
      user: Option[String],
      password: Option[String],
      headers: Map[String, String],
      port: Option[Int],
      protocol: String
  )(implicit w: BodyWritable[T], c: ContentTypeOf[T]): WSRequest = {

    var request = WS.url(s"$protocol://$host${port.fold("")(":" + _)}/$uri")

    headers.foreach(header => request = request.addHttpHeaders(header))

    request = user.fold(request)(
      request.withAuth(_, password.getOrElse(""), xucConfig.defaultWSAuthScheme)
    )

    payload.fold(request)(request.withBody(_))
  }
}
