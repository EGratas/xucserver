package xivo.network

import org.apache.pekko.actor._
import org.json.JSONObject
import org.xivo.cti.MessageFactory
import xivo.network.CtiLinkKeepAlive.{StartKeepAlive, StopKeepALive}
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.duration._

object CtiLinkKeepAlive {
  case class StartKeepAlive(userId: String, link: ActorRef)
  case object StopKeepALive

  def props(userName: String): Props = Props(new CtiLinkKeepAlive(userName))
}

class CtiLinkKeepAlive(userName: String) extends Actor with ActorLogging {
  val KeepAliveInterval: FiniteDuration = 10.minutes

  override def receive: Receive = {

    case StartKeepAlive(userId, link) =>
      val scheduler = context.system.scheduler.scheduleAtFixedRate(
        initialDelay = KeepAliveInterval,
        interval = KeepAliveInterval,
        receiver = self,
        message = new MessageFactory().createGetUserStatus(userId)
      )

      context.become(keepAlive(link, scheduler))
    case message =>
      log.debug(s"received message : $message in context receive")
  }

  def keepAlive(link: ActorRef, scheduler: Cancellable): Receive = {
    case getUserStatus: JSONObject =>
      log.info(s"[$userName] keepalive : $getUserStatus")
      link ! getUserStatus

    case StopKeepALive =>
      scheduler.cancel()
      context.become(receive)
    case message =>
      log.debug(s"received message : $message in context keepAlive")
  }

  override def postStop(): Unit = {
    self ! StopKeepALive
  }
}
