package xivo.network

import org.apache.pekko.actor.{Actor, Props}
import models.XucUser
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{LoginCapasAck, LoginIdAck, LoginPassAck}
import play.api.Logger
object CtiLoginStep {
  def props(xivoCtiVersion: String): Props =
    Props(new CtiLoginStep(new MessageFactory(xivoCtiVersion)))
  val XivoCtiIdentitySuffix = "Xuc"
}
class CtiLoginStep(val messageFactory: MessageFactory) extends Actor {
  protected[network] var user: XucUser = null
  def identity: String                 = user.username + CtiLoginStep.XivoCtiIdentitySuffix

  val log: Logger = Logger(getClass.getName + "." + self.path.name)

  def receive: PartialFunction[Any, Unit] = {
    case StartLogin(updatedUser) =>
      user = updatedUser
      log.info(s"Logging on user: ${updatedUser.username}")
      val message = messageFactory.createLoginId(user.username, identity)
      sender() ! message

    case loginIdAck: LoginIdAck =>
      log.debug(
        "Sending loginPass message: " + user.username + " sessionId: " + loginIdAck.sesssionId
      )
      val message = messageFactory.createLoginPass(
        user.xivoUser.password.getOrElse(""),
        loginIdAck.sesssionId
      )
      sender() ! message

    case loginPassAck: LoginPassAck =>
      val capaId = loginPassAck.capalist.get(0)
      log.debug(
        "Sending loginCapas message: " + user.username + " capaId: " + capaId
      )
      if (user.phoneNumber.isDefined) {
        val message =
          messageFactory.createLoginCapas(capaId, user.phoneNumber.get)
        sender() ! message
      } else {
        val message = messageFactory.createLoginCapas(capaId)
        sender() ! message
      }

    case loginCapasAck: LoginCapasAck =>
      log.debug(
        "User: " + user.username + " successfully logged in, loginCapasAck received"
      )
      sender() ! LoggedOn(
        user,
        loginCapasAck.userId
      )

    case unknown =>
      log.warn(s"Uknown message received: $unknown from : ${sender()}")

  }
}

case class StartLogin(user: XucUser)
case class LoggedOn(
    user: XucUser,
    userId: String
)
