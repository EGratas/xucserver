package xivo.network

import org.apache.pekko.actor.{Actor, ActorRef, PoisonPill, Props, Scheduler}
import org.apache.pekko.io.Tcp.{apply => _, _}
import org.apache.pekko.io.{IO, Tcp}
import org.apache.pekko.util.{ByteString, Timeout}
import com.codahale.metrics.{Counter, SharedMetricRegistries}
import org.json.JSONObject
import org.xivo.cti.message.{
  CtiResponseMessage,
  LoginCapasAck,
  LoginIdAck,
  LoginPassAck
}
import play.api.Logger
import services.Start
import xivo.network.CtiLink._
import xivo.websocket.{LinkState, LinkStatusUpdate}
import xivo.xuc.XucBaseConfig

import java.net.InetSocketAddress
import scala.concurrent.duration.{
  Duration,
  DurationInt,
  FiniteDuration,
  SECONDS
}
import com.codahale.metrics.MetricRegistry

object CtiLink {
  val DefaultReconnectTimer: FiniteDuration = 20.seconds

  case object ConnectionRequest

  case class Restart(reason: Exception)

  class RemoteConnectionClosedException
      extends Exception("Remote connection closed")
  class ConnectionClosedException extends Exception("Connection closed")
  class ConnectionFailedException extends Exception("Connection failed")
  class WriteFailedException      extends Exception("Write failed")

  def props(username: String, config: XucBaseConfig): Props =
    Props(
      new CtiLink(username, new MessageProcessor(username), config)
        with OnTcpFailedKill
    )
  def xucprops(username: String, config: XucBaseConfig): Props =
    Props(
      new CtiLink(username, new MessageProcessor(username), config)
        with OnTcpFailedRestart
    )

}

trait CtiLinkLog {
  this: CtiLink =>
  val logger: Logger = Logger(
    getClass.getPackage.getName + ".CtiLink." + self.path.name
  )

  private def logMessage(msg: String): String = s"[$username] $msg"
  object log {
    def debug(msg: String): Unit = logger.debug(logMessage(msg))
    def info(msg: String): Unit  = logger.info(logMessage(msg))
    def error(msg: String): Unit = logger.error(logMessage(msg))
    def warn(msg: String): Unit  = logger.warn(logMessage(msg))
  }
}

trait OnTcpFailed {
  def onRemoteConnectionClosed(): Unit
  def onConnectionClosed(): Unit
  def onConnectFailed(): Unit
  def onWriteFailed(): Unit
}

trait OnTcpFailedKill extends OnTcpFailed {
  this: CtiLink =>
  override def onRemoteConnectionClosed(): Unit = {
    logError(
      s"CTI remote server closed connection sending message to ${context.parent}"
    )
    context.parent ! LinkStatusUpdate(LinkState.down)
    self ! PoisonPill
  }

  override def onConnectionClosed(): Unit = {
    logError(s"CTI socket closed")
    self ! PoisonPill
  }

  override def onConnectFailed(): Unit = {
    logError(s"Tcp Connection failed")
    self ! PoisonPill
  }

  override def onWriteFailed(): Unit = {
    logError(s"Tcp Write failed")
    self ! PoisonPill
  }

  private def logError(msg: String): Unit = {
    log.error(s"$msg restarting $self")
  }

}

trait OnTcpFailedRestart extends OnTcpFailed {
  this: CtiLink =>
  import context.dispatcher
  override def onRemoteConnectionClosed(): Unit = {
    logError(
      s"CTI remote server closed connection sending message to ${context.parent}"
    )
    context.parent ! LinkStatusUpdate(LinkState.down)
    closing = true
    scheduleRestart(new RemoteConnectionClosedException)
  }

  override def onConnectionClosed(): Unit = {
    logError(s"CTI socket closed")
    scheduleRestart(new ConnectionClosedException)
  }

  override def onConnectFailed(): Unit = {
    logError(s"Connection failed")
    scheduleRestart(new ConnectionFailedException)
  }

  override def onWriteFailed(): Unit = {
    logError(s"Write failed")
    scheduleRestart(new WriteFailedException)
  }

  private def logError(msg: String): Unit = {
    log.error(s"$msg restarting $self")
  }

  private def scheduleRestart(ex: Exception) =
    scheduler.scheduleOnce(DefaultReconnectTimer, self, Restart(ex))
}

class CtiLink(
    val username: String,
    val messageProcessor: MessageProcessor,
    xucConfig: XucBaseConfig
) extends Actor
    with CtiLinkLog {
  this: OnTcpFailed =>

  import xivo.network.CtiLink._
  case object Ack extends Event

  import context.system
  val endpoint: InetSocketAddress =
    new InetSocketAddress(xucConfig.XIVOCTI_HOST, xucConfig.XIVOCTI_PORT.toInt)
  implicit val timeout: Timeout = Timeout(
    Duration(xucConfig.XivoCtiTimeout, SECONDS)
  )
  protected[network] var connection: ActorRef = null

  val registry: MetricRegistry =
    SharedMetricRegistries.getOrCreate(xucConfig.metricsRegistryName)

  def scheduler: Scheduler = context.system.scheduler

  val startCount: Counter =
    registry.counter(s"CtiLink.$username.startConnection")
  val stopCount: Counter      = registry.counter(s"CtiLink.$username.stopConnection")
  val stopTotalCount: Counter = registry.counter(s"CtiLink.stopConnection")

  val manager: ActorRef = IO(Tcp)

  val logCtiOut: Logger = Logger("xucevents.cti.out." + username)
  val logCtiOutPretty   = false

  log.info(s"........Starting CtiLink actor for user $username")

  protected[network] var ctiLoginStep: ActorRef = null

  override def preStart(): Unit = {
    log.debug(s"preStart CtiLink actor for user $username ")
    ctiLoginStep = context.actorOf(
      CtiLoginStep.props(xucConfig.XivoCtiVersion),
      username + "CtiLoginStep"
    )
    self ! ConnectionRequest
  }

  def processDecodedMessage(message: CtiResponseMessage[_]): Unit = {
    message match {
      case message: LoginCapasAck =>
        ctiLoginStep ! message

      case message: LoginIdAck =>
        ctiLoginStep ! message

      case message: LoginPassAck =>
        ctiLoginStep ! message

      case _ =>
        context.parent ! message
    }
  }

  def commonReceive: Receive = runningReceive orElse onFailed

  def runningReceive: Receive = {

    case Start(user) =>
      log.info(s"Starting link  user : ${user.username}")
      ctiLoginStep ! StartLogin(user)

    case ConnectionRequest =>
      log.info(
        "Connection request to " + xucConfig.XIVOCTI_HOST + " on port " + xucConfig.XIVOCTI_PORT
      )
      manager ! Tcp.Connect(endpoint)

    case _: Tcp.Connected =>
      log.info(s"Connection for user: [$username] established")
      startCount.inc()
      connection = sender()
      context.become(receiveNoBuffer)
      connection ! Register(self, keepOpenOnPeerClosed = false)
      context.parent ! LinkStatusUpdate(LinkState.up)

    case ln: LoggedOn => onCtiServerLoggedOn(ln)

    case Tcp.Received(data) => processData(data)
    case Restart(reason)    => throw reason
    case Ack                => acknowledge()

  }

  def onFailed: Receive = {

    case Tcp.CommandFailed(w: Write) => onWriteFailed()

    case Tcp.CommandFailed(_: Connect) => onConnectFailed()

    case Tcp.PeerClosed =>
      stopCount.inc()
      stopTotalCount.inc()
      context.become(receiveOnStart)
      connection = null
      onRemoteConnectionClosed()

    case _: Tcp.ConnectionClosed =>
      stopCount.inc()
      stopTotalCount.inc()
      context.become(receiveOnStart)
      connection = null
      onConnectionClosed()
  }

  def receiveAndBufferize: Receive = {
    case ctiMessage: JSONObject =>
      log.debug(">>>Buffering : %s\n".format(ctiMessage.toString))
      buffer(ByteString("%s\n".format(ctiMessage.toString)))

    case unknown =>
      log.error(s"receiveAndBufferize Uknown message received: $unknown")

  }

  def receiveNoBufferize: Receive = {
    case ctiMessage: JSONObject =>
      val data = ByteString("%s\n".format(ctiMessage.toString))
      log.debug(">>> %s\n".format(ctiMessage.toString))
      logCtiOut.debug {
        if (logCtiOutPretty) ctiMessage.toString(2) else ctiMessage.toString
      }
      buffer(data)
      connection ! Tcp.Write(data, Ack)
      context.become(receiveWithBuffer)

    case unknown =>
      log.error(s"receiveNoBufferize Uknown message received: $unknown ")

  }

  def receive: Receive = receiveOnStart

  def receiveOnStart: Receive    = commonReceive
  def receiveWithBuffer: Receive = commonReceive orElse receiveAndBufferize
  def receiveNoBuffer: Receive   = commonReceive orElse receiveNoBufferize

  override def postStop(): Unit = {
    log.info(s"Post stop cleaning tcp connection $connection")
    if (connection != null) connection ! Tcp.Close
  }

  private def onCtiServerLoggedOn(ln: LoggedOn): Unit = {
    log.info(s"Logged on ${ln.user.username} ${ln.userId}")
    context.parent ! ln
  }

  private def processData(data: ByteString): Unit = {
    val receivedString = data.decodeString("US-ASCII")
    log.debug("<<< " + receivedString)
    val messages = messageProcessor.processBuffer(receivedString)
    messages.foreach((message: CtiResponseMessage[_]) => {
      log.debug(s"$message")
      processDecodedMessage(message)
    })
  }

  var storage: Vector[ByteString] = Vector.empty[ByteString]
  var stored                      = 0L
  var transferred                 = 0L
  var closing                     = false

  val maxStored           = 100000000L
  val highWatermark: Long = maxStored * 5 / 10
  val lowWatermark: Long  = maxStored * 3 / 10
  var suspended           = false

  //#simple-helpers
  private def buffer(data: ByteString): Unit = {
    storage :+= data
    stored += data.size

    if (stored > maxStored) {
      log.error(s"drop connection (buffer overrun)")
      context stop self

    } else if (stored > highWatermark) {
      log.error(s"suspending reading")
      connection ! SuspendReading
      suspended = true
    }
  }

  private def acknowledge(): Unit = {
    require(storage.nonEmpty, "storage was empty")

    val size = storage(0).size
    stored -= size
    transferred += size

    storage = storage drop 1

    if (suspended && stored < lowWatermark) {
      log.debug("resuming reading")
      connection ! ResumeReading
      suspended = false
    }

    if (storage.isEmpty) {
      if (!closing) context.become(receiveNoBuffer)
    } else connection ! Write(storage(0), Ack)
  }

}
