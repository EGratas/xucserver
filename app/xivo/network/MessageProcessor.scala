package xivo.network

import java.security.InvalidParameterException

import org.json.JSONException
import org.xivo.cti.MessageParser
import org.xivo.cti.message.CtiResponseMessage
import play.api.Logger
import play.libs.Json

class MessageProcessor(userName: String) {
  val log: Logger = Logger(getClass.getName)

  val MsgSeparator   = "\n"
  var partialMessage = ""

  var messageParser: MessageParser = new MessageParser

  val logCtiIn: Logger = Logger("xucevents.cti.in." + userName)
  val logCtiInPretty   = false

  def parseMessage(message: String): Option[CtiResponseMessage[_]] = {

    try {
      messageParser.parseBuffer(message) match {
        case msg if msg != null => Some(msg)
        case unknown =>
          log.debug(s"unable to parse message $unknown")
          None
      }
    } catch {
      case e: JSONException =>
        log.debug("Invalid JSON in message : " + message)
        None
      case e: InvalidParameterException =>
        log.debug("Invalid parameter in message : " + message)
        None
      case e: IllegalArgumentException =>
        log.debug("Illegal argument in message : " + message)
        None
    }
  }

  def processBuffer(buffer: String): Array[CtiResponseMessage[_]] = {
    var msgs = (partialMessage + buffer).split(MsgSeparator)

    if (!buffer.endsWith(MsgSeparator)) {
      partialMessage = msgs.takeRight(1)(0)
      msgs = msgs.dropRight(1)
    } else partialMessage = ""

    for {
      message <- msgs
      decoded <- parseMessage(message)
    } yield {
      logCtiIn.debug({
        val parsed = Json.parse(message)
        val json =
          if (logCtiInPretty) Json.prettyPrint(parsed)
          else Json.stringify(parsed)
        json + " -> " + decoded
      })
      decoded
    }

  }

}
