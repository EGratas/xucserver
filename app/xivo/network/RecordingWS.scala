package xivo.network

import com.google.inject.Inject
import models.{CallbackTicket, CallbackTicketPatch}
import org.slf4j.LoggerFactory
import play.api.http.Status._
import play.api.libs.json._
import play.api.libs.ws.{WSClient, WSResponse}
import services.request.{HistoryDays, HistoryParam, HistorySize}
import xivo.models.{
  FindCustomerCallHistoryRequest,
  FindCustomerCallHistoryResponse
}
import xivo.xuc.RecordingConfig
import play.api.libs.ws.DefaultBodyWritables.writeableOf_String
import play.api.libs.ws.WSBodyWritables.writeableOf_String
import play.api.libs.ws.writeableOf_String
import play.api.libs.ws.JsonBodyWritables.writeableOf_JsValue
import play.api.libs.ws.WSBodyWritables.writeableOf_JsValue
import play.api.libs.ws.writeableOf_JsValue
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.slf4j.Logger

abstract class XucWS(val host: String, val port: Int, val token: String)

trait XucWSResponse {
  this: XucWS =>

  val ws: WSClient

  private def url(size: Int) =
    s"http://$host:$port/recording/records/search?page=1&pageSize=$size"

  def postWs(size: Int, json: JsObject): Future[WSResponse] =
    ws.url(url(size)).withHttpHeaders("X-Auth-Token" -> token).post(json)

  def processResp(response: WSResponse): HistoryServerResponse =
    response.status match {
      case OK =>
        HistoryServerResponse((Json.parse(response.body) \ "records").get)
      case FORBIDDEN => throw new ForbiddenException
      case _ =>
        throw new WebServiceException(
          s"Request to history web service failed with status ${response.status} and message ${response.body}"
        )
    }
}

sealed trait HistoryEndpoint
case object AgentHistoryEndpoint extends HistoryEndpoint {
  override def toString: String = "/agent"
}
case object DefaultHistoryEndpoint extends HistoryEndpoint {
  override def toString: String = ""
}

class RecordingWS @Inject() (config: RecordingConfig, val ws: WSClient)
    extends XucWS(
      config.recordingHost,
      config.recordingPort,
      config.recordingToken
    )
    with XucWSResponse {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def getQueueCallHistory(
      queue: String,
      size: Int
  ): Future[HistoryServerResponse] = {
    val json = Json.obj("queue" -> queue)
    postWs(size, json).map(processResp(_))
  }

  def getAgentCallHistory(
      param: HistoryParam,
      agentNumber: String
  ): Future[HistoryServerResponse] = {
    val json = Json.obj("agentNum" -> agentNumber)

    ws.url(callHistoryUrl(param, AgentHistoryEndpoint))
      .withHttpHeaders("X-Auth-Token" -> token)
      .post(json)
      .map(response => {
        if (response.status == 200)
          HistoryServerResponse(Json.parse(response.body))
        else if (response.status == 403) throw new ForbiddenException
        else
          throw new WebServiceException(
            s"Request to history web service failed with status ${response.status} and message ${response.body}"
          )
      })
  }

  def findCustomerCallHistory(
      criteria: FindCustomerCallHistoryRequest
  ): Future[FindCustomerCallHistoryResponse] = {
    val json = Some(Json.toJson(criteria))

    logger.debug(s"findCustomerCallHistory $json")
    ws.url(s"http://$host:$port/recording/history/customer")
      .withHttpHeaders("X-Auth-Token" -> token)
      .post(json.get)
      .map(response => {
        processPotentialError(response)
        response.json.validate[FindCustomerCallHistoryResponse] match {
          case e: JsError =>
            logger
              .error(s"Request to history web service failed with message $e")
            throw new WebServiceException("Non understandable JSON returned")
          case JsSuccess(resp, _) =>
            logger.debug(s"findCustomerCallHistory success $resp")
            resp
        }
      })
  }

  def getUserCallHistory(
      param: HistoryParam,
      interface: String
  ): Future[HistoryServerResponse] = {
    val json = Json.obj("interface" -> interface)

    ws.url(callHistoryUrl(param))
      .withHttpHeaders("X-Auth-Token" -> token)
      .post(json)
      .map(response => {
        if (response.status == 200)
          HistoryServerResponse(Json.parse(response.body))
        else if (response.status == 403) throw new ForbiddenException
        else
          throw new WebServiceException(
            s"Request to history web service failed with status ${response.status} and message ${response.body}"
          )
      })
  }

  def createCallbackTicket(ticket: CallbackTicket): Future[CallbackTicket] =
    ws.url(s"http://$host:$port/recording/callback_tickets")
      .withHttpHeaders("X-Auth-Token" -> token)
      .post(Json.toJson(ticket))
      .map(response => {
        response.json.validate[CallbackTicket] match {
          case e: JsError =>
            logger.error(s"Could not read from config server : $e")
            throw new WebServiceException("Non understandable JSON returned")
          case s: JsSuccess[CallbackTicket] => s.get
        }
      })

  def updateCallbackTicket(
      uuid: String,
      patch: CallbackTicketPatch
  ): Future[Unit] =
    ws.url(s"http://$host:$port/recording/callback_tickets/$uuid")
      .withHttpHeaders("X-Auth-Token" -> token)
      .put(Json.toJson(patch))
      .map(response => {
        if (response.status == 200) ()
        else throw new WebServiceException(response.body)
      })

  def exportTicketsCSV(listUuid: String): Future[String] =
    ws.url(
      s"http://$host:$port/recording/callback_tickets/search?listUuid=$listUuid"
    ).withHttpHeaders("X-Auth-Token" -> token)
      .post("")
      .map(response => {
        if (response.status == 200) response.body
        else throw new WebServiceException(response.body)
      })

  def getCallbackTicket(uuid: String): Future[CallbackTicket] =
    ws.url(s"http://$host:$port/recording/callback_tickets/$uuid")
      .withHttpHeaders("X-Auth-Token" -> token)
      .get()
      .map(response => {
        response.json.validate[CallbackTicket] match {
          case e: JsError =>
            logger.error(s"Could not read from config server : $e")
            throw new WebServiceException("Non understandable JSON returned")
          case s: JsSuccess[CallbackTicket] => s.get
        }
      })

  private def processPotentialError(response: WSResponse): Unit = {
    if (response.status >= 400)
      throw new WebServiceException(
        s"""Web service call failed with code ${response.status} and message "${response.body}""""
      )
  }

  private def callHistoryUrl(
      param: HistoryParam,
      historyEndpoint: HistoryEndpoint = DefaultHistoryEndpoint
  ): String = {
    val historyBaseUrl = s"http://$host:$port/recording/history"

    param match {
      case HistorySize(size) => s"$historyBaseUrl$historyEndpoint?size=$size"
      case HistoryDays(days) => s"$historyBaseUrl$historyEndpoint?days=$days"
    }
  }
}

case class HistoryServerResponse(json: JsValue)

class ForbiddenException
    extends Exception(
      "Cannot login to history server, probably wrong login/password"
    )

class WebServiceException(message: String) extends Exception(message: String) {
  override def equals(o: Any): Boolean =
    o match {
      case e: WebServiceException => e.getMessage == message
      case _                      => false
    }
}
