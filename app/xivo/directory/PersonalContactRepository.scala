package xivo.directory

import org.apache.pekko.actor.Actor
import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import models._
import models.ws.auth.ApiUser
import services.config.{RepositoryMap, SetEntry}
import xivo.directory.PersonalContactRepository.{
  DeletePersonalContact,
  PersonalContactMap,
  PersonalContacts,
  SetPersonalContact
}
import xivo.models.PersonalContactResult

import scala.concurrent.{ExecutionContext, Future}

object PersonalContactRepository {
  type PersonalContactMap = Map[String, PersonalContactResult]
  case class PersonalContacts(map: PersonalContactMap)

  case class SetPersonalContact(pc: PersonalContactResult)
  case class DeletePersonalContact(pcId: String)

  val personalContactRepoURI: String = "personalContactRepo"

  trait Factory {
    def apply(user: XivoUser): Actor
  }
}

class PersonalContactRepository @Inject() (
    @Assisted user: XivoUser,
    dirdRequester: DirdRequester
) extends RepositoryMap[String, PersonalContactResult] {

  def buildCacheEntry(pc: PersonalContactResult): PersonalContactMap = {
    List(pc.number, pc.mobile, pc.fax).flatten.map(s => (s, pc)).toMap
  }

  def deleteEntry(pcId: String): Unit = {
    repository = repository.filter(_._2.id != pcId)
  }

  override def load(implicit
      ec: ExecutionContext
  ): Future[PersonalContactMap] = {
    log.debug(s"creating personal contact name cache ")
    dirdRequester
      .list(ApiUser(user.username, user.id))
      .map(_.flatten(buildCacheEntry).toMap)
  }

  override def receiveCommand: PartialFunction[Any, Unit] = {

    case SetPersonalContact(pc) =>
      log.debug(s"${sender()} ! SetPersonalContact ${pc.id}")
      deleteEntry(pc.id)
      buildCacheEntry(pc).foreach { case (k, v) => self ! SetEntry(k, v) }

    case DeletePersonalContact(pcId) =>
      log.debug(s"${sender()} ! DeletePersonalContact $pcId")
      deleteEntry(pcId)
  }

  override def mapResponse(m: PersonalContactMap): Any = PersonalContacts(m)
}
