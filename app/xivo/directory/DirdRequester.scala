package xivo.directory

import org.apache.pekko.actor.ActorRef
import org.apache.pekko.pattern.ask
import com.google.inject.Inject
import com.google.inject.name.Named
import models._
import models.ws.auth.ApiUser
import models.ws.{ErrorType, JsonParsingError, NotHandledError}
import play.api.Logger
import play.api.http.Status._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.libs.ws.{WSRequest, WSResponse}
import services._
import services.directory.DirectoryTransformer.EnrichDirectoryResult
import xivo.directory.DirdRequester._
import xivo.models._
import xivo.network.XiVOWS
import xivo.services.XivoAuthentication
import xivo.xuc.XucConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

object DirdRequester {
  implicit val timeout: org.apache.pekko.util.Timeout = 5.seconds
  val log: Logger                                     = Logger(getClass.getName)
  val CsvLineSeparator                                = "\r\n"
  val CsvSeparator                                    = ','
  val UTF8_BOM                                        = "\uFEFF"

  def createError[T](
      username: Option[String],
      requestAction: String,
      message: String,
      code: ErrorType = NotHandledError
  ): Future[T] = {
    Future.failed(
      new DirdRequesterException(
        code,
        s"Impossible to $requestAction personal contact for $username. $message"
      )
    )
  }

  def logRequest(username: Option[String], requestAction: String): Unit = {
    log.info(
      s"Executing dird $requestAction personal contacts request for $username"
    )
  }
}

class DirdRequester @Inject() (
    @Named(ActorIds.XivoAuthenticationId) xivoAuthentication: ActorRef,
    @Named(ActorIds.DirectoryTransformerId) transformerRef: ActorRef,
    xivoWS: XiVOWS,
    config: XucConfig
) {

  private def auth(userId: Long) =
    XivoAuthentication.getCtiTokenHelper(xivoAuthentication, userId)

  def richList(
      user: ApiUser
  ): Future[RichDirectoryResult] = {
    val actionRequest = "richList"
    logRequest(user.username, actionRequest)

    def request(t: Token) =
      xivoWS.get(
        config.xivoHost,
        s"${config.XivoDir.directoryURI}/${config.XivoDir.defaultProfile}",
        headers = Map("X-Auth-Token" -> t.token),
        port = Some(config.XivoDir.port)
      )

    auth(user.userId)
      .flatMap(t =>
        request(t)
          .execute()
          .map(resp => DirSearchResult.parse(resp.json))
      )
      .flatMap(r => transformerRef ? EnrichDirectoryResult(r, user.userId))
      .collect { case r: RichDirectoryResult => r }
      .recoverWith { case e =>
        createError(user.username, actionRequest, e.getMessage)
      }
  }

  def list(
      user: ApiUser
  ): Future[List[PersonalContactResult]] = {
    val actionRequest = "list"
    logRequest(user.username, actionRequest)

    def request(t: Token) =
      xivoWS.get(
        config.xivoHost,
        s"${config.XivoDir.personalURI}",
        headers = Map("X-Auth-Token" -> t.token),
        port = Some(config.XivoDir.port)
      )

    execute(user, actionRequest, request, List(OK)).flatMap(
      responseToPersonalContactList
    )
  }

  def get(
      user: ApiUser
  )(contactId: String): Future[PersonalContactResult] = {
    val actionRequest = "get"
    logRequest(user.username, actionRequest)

    def request(t: Token) =
      xivoWS.get(
        config.xivoHost,
        s"${config.XivoDir.personalURI}/$contactId",
        headers = Map("X-Auth-Token" -> t.token),
        port = Some(config.XivoDir.port)
      )

    execute(user, actionRequest, request, List(OK)).flatMap(
      responseToPersonalContact
    )
  }

  def add(
      user: ApiUser,
      contact: PersonalContactRequest
  ): Future[PersonalContactResult] = {
    val actionRequest = "add"
    logRequest(user.username, actionRequest)

    def request(t: Token) =
      xivoWS.post(
        config.xivoHost,
        config.XivoDir.personalURI,
        payload = Some(Json.toJson(contact)),
        headers = Map("X-Auth-Token" -> t.token),
        port = Some(config.XivoDir.port)
      )

    execute(user, actionRequest, request, List(CREATED)).flatMap(
      responseToPersonalContact
    )
  }

  def edit(
      user: ApiUser,
      contact: PersonalContactRequest
  )(
      contactId: String
  ): Future[PersonalContactResult] = {
    val actionRequest = "edit"
    logRequest(user.username, actionRequest)

    def request(t: Token) =
      xivoWS.put(
        config.xivoHost,
        s"${config.XivoDir.personalURI}/$contactId",
        payload = Some(Json.toJson(contact)),
        headers = Map("X-Auth-Token" -> t.token),
        port = Some(config.XivoDir.port)
      )

    execute(user, actionRequest, request, List(OK)).flatMap(
      responseToPersonalContact
    )
  }

  def delete(user: ApiUser)(
      contactId: String
  ): Future[WSResponse] = {
    val actionRequest = "delete"
    logRequest(user.username, actionRequest)

    def request(t: Token) =
      xivoWS.del(
        config.xivoHost,
        s"${config.XivoDir.personalURI}/$contactId",
        headers = Map("X-Auth-Token" -> t.token),
        port = Some(config.XivoDir.port)
      )

    execute(user, actionRequest, request, List(NO_CONTENT))
  }

  def deleteAll(user: ApiUser): Future[WSResponse] = {
    val actionRequest = "deleteAll"
    logRequest(user.username, actionRequest)

    def request(t: Token) =
      xivoWS.del(
        config.xivoHost,
        s"${config.XivoDir.personalURI}",
        headers = Map("X-Auth-Token" -> t.token),
        port = Some(config.XivoDir.port)
      )

    execute(user, actionRequest, request, List(NO_CONTENT))
  }

  def exportCsv(user: ApiUser): Future[String] = {
    val actionRequest = "export"
    logRequest(user.username, actionRequest)

    def request(t: Token) =
      xivoWS.get(
        config.xivoHost,
        s"${config.XivoDir.personalURI}?format=text%2Fcsv",
        headers = Map("X-Auth-Token" -> t.token),
        port = Some(config.XivoDir.port)
      )

    execute(user, actionRequest, request, List(NO_CONTENT, OK))
      .flatMap(
        responseToCSV
      )
  }

  def importCsv(
      user: ApiUser
  )(csv: Option[String]): Future[PersonalContactImportResult] = {
    val actionRequest = "import"
    logRequest(user.username, actionRequest)

    def request(t: Token) =
      xivoWS.genericPost(
        config.xivoHost,
        s"${config.XivoDir.personalURI}/import",
        payload = csv,
        headers = Map(
          "X-Auth-Token" -> t.token,
          "Content-Type" -> "text/csv;charset=UTF-8"
        ),
        port = Some(config.XivoDir.port)
      )

    execute(user, actionRequest, request, List(CREATED)).flatMap(
      responseToPersonalContactImport
    )
  }

  def responseToPersonalContactImport(
      resp: WSResponse
  ): Future[PersonalContactImportResult] = {
    resp.json.validate[PersonalContactImportResult] match {
      case JsSuccess(pci, _) => Future.successful(pci)
      case JsError(_) =>
        createError(
          None,
          "read",
          "Fail to parse response from dird while importing contacts",
          JsonParsingError
        )
    }
  }

  def responseToPersonalContact(
      resp: WSResponse
  ): Future[PersonalContactResult] = {
    resp.json.validate[PersonalContactResult] match {
      case JsSuccess(pc, _) => Future.successful(pc)
      case JsError(_) =>
        createError(
          None,
          "read",
          "Fail to parse response from dird while getting contact",
          JsonParsingError
        )
    }
  }

  def responseToPersonalContactList(
      resp: WSResponse
  ): Future[List[PersonalContactResult]] = {
    resp.json.validate[PersonalContactList] match {
      case JsSuccess(list, _) => Future.successful(list.items)
      case JsError(_) =>
        createError(
          None,
          "read",
          "Fail to parse response from dird while listing contacts",
          JsonParsingError
        )
    }
  }

  def responseToCSV(resp: WSResponse): Future[String] = {
    def dropColumn(col: String, csv: String, delim: Char = CsvSeparator) = {

      def drop(line: Array[String], rmIndex: Int) =
        (line.take(rmIndex) ++ line.drop(rmIndex + 1)).mkString(delim.toString)
      val csvArr  = csv.split(CsvLineSeparator)
      val header  = csvArr.head.split(delim).map(_.trim)
      val rmIndex = header.indexOf(col)

      drop(header, rmIndex) + CsvLineSeparator + csvArr.tail
        .map { r =>
          drop(r.split(delim.toString, -1), rmIndex)
        }
        .mkString(CsvLineSeparator)
    }

    Future(DirdRequester.UTF8_BOM.concat(dropColumn("id", resp.body)))
  }

  def execute(
      user: ApiUser,
      actionRequest: String,
      request: Token => WSRequest,
      awaitedStatus: List[Int]
  ): Future[WSResponse] = {
    def dirdError(msg: String, code: ErrorType = NotHandledError) =
      createError(user.username, actionRequest, msg, code)

    auth(user.userId)
      .flatMap(t => request(t).execute())
      .recoverWith { case e =>
        dirdError(s"Fail to contact dird: ${e.getMessage}", Unreachable)
      }
      .flatMap(resp => {
        if (awaitedStatus contains resp.status) {
          Future.successful(resp)
        } else {
          resp.json.validate[DirdError] match {
            case JsSuccess(de: DirdError, _) =>
              val code = de.status_code match {
                case 400 => InvalidContact
                case 404 => ContactNotFound
                case 409 => DuplicateContact
                case 503 => Unreachable
                case _   => NotHandledError
              }
              dirdError(de.reason.head, code)
            case _ => dirdError("Unknown error from dird")
          }
        }
      })
  }
}
