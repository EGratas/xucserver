package xivo.services

import java.net.URLEncoder
import org.apache.pekko.actor.{
  Actor,
  ActorLogging,
  ActorRef,
  Cancellable,
  Props
}
import com.google.inject.Inject
import com.google.inject.name.Named
import models.{DirSearchResult, Token, XivoUser}
import play.api.libs.json.*
import play.api.libs.ws.{WSRequest, WSResponse}
import services.ActorIds
import services.config.ConfigRepository
import services.directory.DirectoryTransformer.RawDirectoryResult
import services.request.*
import xivo.network.XiVOWS
import xivo.services.XivoDirectory.Action.Action
import xivo.services.XivoDirectory.*
import xivo.xuc.XucConfig

import scala.concurrent.Await

object XivoDirectory {
  sealed trait XivoDirectoryMsg
  case class DirLookupResult(result: DirSearchResult)
      extends XucRequest
      with XivoDirectoryMsg
  case class Favorites(result: DirSearchResult)
      extends XucRequest
      with XivoDirectoryMsg

  object Action extends Enumeration {
    type Action = Value
    val Added: XivoDirectory.Action.Value      = Value
    val AddFail: XivoDirectory.Action.Value    = Value
    val Removed: XivoDirectory.Action.Value    = Value
    val RemoveFail: XivoDirectory.Action.Value = Value
    implicit val actionWrites: Writes[Action] = new Writes[Action] {
      def writes(`enum`: Action): JsValue = JsString(`enum`.toString)
    }
  }

  case class FavoriteUpdated(action: Action, contactId: String, source: String)
      extends XucRequest
      with XivoDirectoryMsg
  object FavoriteUpdated {
    implicit val writes: Writes[FavoriteUpdated] = new Writes[FavoriteUpdated] {
      def writes(f: FavoriteUpdated): JsValue =
        JsObject(
          Seq(
            "action"     -> JsString(f.action.toString),
            "contact_id" -> JsString(f.contactId),
            "source"     -> JsString(f.source)
          )
        )
    }
  }

  case object TokenRetrievalTimeout extends XucRequest with XivoDirectoryMsg
  case object XivoAuthTimeout       extends XucRequest with XivoDirectoryMsg
  case object XivoAuthError         extends XucRequest with XivoDirectoryMsg
  case object XivoDirdTimeout       extends XucRequest with XivoDirectoryMsg
}

class XivoDirectory @Inject() (
    @Named(ActorIds.XivoAuthenticationId) xauth: ActorRef,
    @Named(ActorIds.DirectoryTransformerId) dirTransformer: ActorRef,
    configRepo: ConfigRepository,
    xivoWS: XiVOWS,
    config: XucConfig
) extends Actor
    with ActorLogging {

  def receive: Receive = {
    case UserBaseRequest(ref, request: DirectoryRequest, user) =>
      configRepo.getCtiUser(user.username) match {
        case None =>
          log.error(s"Got directory look up for unknown user ${user.username}")
        case Some(user) =>
          log.debug(s"Received $request by user ${user.id}")
          getToken(user, BaseRequest(ref, request))
      }

    case (
          t: Token,
          BaseRequest(ref, DirectoryLookUp(term)),
          requesterUserId: Long
        ) =>
      search(term, t) match {
        case r: DirLookupResult =>
          dirTransformer ! RawDirectoryResult(ref, r, requesterUserId)
        case other =>
          ref ! other
      }

    case (
          t: Token,
          BaseRequest(ref, GetFavorites),
          requesterUserId: Long
        ) =>
      getFavorites(t) match {
        case f: Favorites =>
          dirTransformer ! RawDirectoryResult(ref, f, requesterUserId)
        case other =>
          ref ! other
      }

    case (
          t: Token,
          BaseRequest(ref, AddFavorite(contactId, directory)),
          requesterUserId: Long
        ) =>
      setFavorite(contactId, directory, t) match {
        case r: FavoriteUpdated =>
          log.debug(s"sending $r")
          dirTransformer ! RawDirectoryResult(ref, r, requesterUserId)
        case other =>
          log.debug(s"sending $other")
          ref ! other
      }

    case (
          t: Token,
          BaseRequest(ref, RemoveFavorite(contactId, directory)),
          requesterUserId: Long
        ) =>
      removeFavorite(contactId, directory, t) match {
        case r: FavoriteUpdated =>
          dirTransformer ! RawDirectoryResult(ref, r, requesterUserId)
        case other =>
          ref ! other
      }

    case any =>
      log.debug(s"Received unprocessed message $any")
  }

  private def getToken(user: XivoUser, baseRequest: BaseRequest): Unit = {
    val replyTo = self
    context.actorOf(Props(new Actor() {
      xauth ! XivoAuthentication.GetCtiToken(user.id)

      def receive: Receive = {
        case TokenRetrievalTimeout =>
          log.error("Get token timeout")
          baseRequest.requester ! XivoAuthTimeout
          context.stop(self)

        case t: Token =>
          log.debug(s"Got token, searching")
          replyTo ! ((t, baseRequest, user.id))
          timeout.cancel()
          context.stop(self)

        case any =>
          log.error(s"Received unexpected response $any")
          baseRequest.requester ! XivoAuthError
          context.stop(self)
      }

      import context.dispatcher

      val timeout: Cancellable =
        context.system.scheduler.scheduleOnce(config.defaultWSTimeout) {
          self ! TokenRetrievalTimeout
        }
    }))
  }

  private def search(term: String, t: Token): XivoDirectoryMsg = {
    val searchURI =
      s"${config.XivoDir.searchURI}/${config.XivoDir.defaultProfile}${config.XivoDir.searchArg}" + URLEncoder
        .encode(term, "UTF-8")
    val request = xivoWS.get(
      config.xivoHost,
      searchURI,
      headers = Map("X-Auth-Token" -> t.token),
      port = Some(config.XivoDir.port)
    )
    executeSearchRequest(request, t)
  }

  private def getFavorites(t: Token): XivoDirectoryMsg = {
    val requestURI =
      s"${config.XivoDir.favoriteURI}/${config.XivoDir.defaultProfile}"
    val request = xivoWS.get(
      config.xivoHost,
      requestURI,
      headers = Map("X-Auth-Token" -> t.token),
      port = Some(config.XivoDir.port)
    )
    executeSearchRequest(request, t, true)
  }

  private def executeSearchRequest(
      request: WSRequest,
      t: Token,
      favorites: Boolean = false
  ): XivoDirectoryMsg = {
    import context.dispatcher
    val wsResult =
      request.execute().map(resp => DirSearchResult.parse(resp.json))
    val searchResult = Await.result(wsResult, config.defaultWSTimeout)
    log.debug(s"Got dird result: $searchResult")
    if (!favorites) {
      DirLookupResult(searchResult)
    } else {
      Favorites(searchResult)
    }
  }

  private def setFavorite(
      contactId: String,
      directory: String,
      t: Token
  ): XivoDirectoryMsg = {
    val requestURI = s"${config.XivoDir.favoriteURI}/$directory/$contactId"
    val request = xivoWS.put(
      config.xivoHost,
      requestURI,
      headers = Map("X-Auth-Token" -> t.token),
      port = Some(config.XivoDir.port)
    )
    processFavoriteRequest(
      request,
      contactId,
      directory,
      Action.Added,
      Action.AddFail
    )
  }

  private def removeFavorite(
      contactId: String,
      directory: String,
      t: Token
  ): XivoDirectoryMsg = {
    val requestURI = s"${config.XivoDir.favoriteURI}/$directory/$contactId"
    val request = xivoWS.del(
      config.xivoHost,
      requestURI,
      headers = Map("X-Auth-Token" -> t.token),
      port = Some(config.XivoDir.port)
    )
    processFavoriteRequest(
      request,
      contactId,
      directory,
      Action.Removed,
      Action.RemoveFail
    )
  }

  private def processFavoriteRequest(
      request: WSRequest,
      contactId: String,
      directory: String,
      actionSuccess: Action,
      actionFail: Action
  ): XivoDirectoryMsg = {
    val wsResult     = request.execute()
    val searchResult = Await.result(wsResult, config.defaultWSTimeout)
    log.debug(s"Got set favorite result: ${searchResult.status}")
    if (searchResult.status == 204) {
      FavoriteUpdated(actionSuccess, contactId, directory)
    } else {
      FavoriteUpdated(actionFail, contactId, directory)
    }
  }
}
