package xivo.services

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef, Props}
import org.apache.pekko.pattern.ask
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.{RequestError, RequestSuccess, RequestUnauthorized}
import models.*
import play.api.libs.json.{JsPath, Json, Writes}
import services.ActorIds
import services.config.ConfigRepository
import xivo.services.TokenRetriever.TokenByLogin
import xivo.services.XivoAuthentication.{
  AuthTimeSynchronizationError,
  AuthTimeout,
  AuthUnknownUser
}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt

object TokenRetriever {
  def props(xivoAuthentication: ActorRef, repo: ConfigRepository): Props =
    Props(new TokenRetriever(xivoAuthentication, repo))
  case class TokenByLogin(login: String)

  trait Factory {
    def apply(): Actor
  }
}

case class AuthenticationToken(token: String)

object AuthenticationToken {
  implicit val writes: Writes[AuthenticationToken] =
    (JsPath \ "token").write[String].contramap(o => o.token)
}

class TokenRetriever @Inject() (
    @Named(ActorIds.XivoAuthenticationId) xivoAuthentication: ActorRef,
    repo: ConfigRepository
) extends Actor
    with ActorLogging {

  implicit val timeout: org.apache.pekko.util.Timeout = 2.seconds

  override def receive: Receive = { case TokenByLogin(login) =>
    val theSender = sender()
    repo.getCtiUser(login) match {
      case Some(u) =>
        val res = xivoAuthentication ? XivoAuthentication.GetCtiToken(u.id)
        res.map({
          case Token(token, _, _, _, _, _) =>
            theSender ! RequestSuccess(token)
          case AuthTimeout =>
            theSender ! RequestError("Token retrieval timeout")
          case AuthTimeSynchronizationError =>
            theSender ! RequestError("Token time synchronization error")
          case AuthUnknownUser =>
            theSender ! RequestUnauthorized(
              s"User $login unknown by the token server"
            )
        })
      case None =>
        theSender ! RequestUnauthorized(s"User $login unknown by the Xuc")
    }
  }
}
