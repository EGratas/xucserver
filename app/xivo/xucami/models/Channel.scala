package xivo.xucami.models

import org.asteriskjava.live.AsteriskChannel
import org.asteriskjava.manager.event.{
  CoreShowChannelEvent,
  NewChannelEvent,
  NewStateEvent
}
import services.calltracking.SipDriver
import xivo.xucami.models.Channel.VarNames
import xivo.xucami.models.ChannelDirection.ChannelDirection
import scala.util.matching.Regex

case class CallerId(name: String, number: String) {
  def isNotFullySet: Boolean =
    this.name == "" || this.name == null || this.number == "" || this.number == null
  def nameOption: Option[String]   = Option(name).filterNot(_.isEmpty)
  def numberOption: Option[String] = Option(number).filterNot(_.isEmpty)
}

object CallerId {
  val callerIdRegexp: Regex =
    """^(?:"(.+)"|([a-zA-Z0-9\-\.\!%\*_\+`\'\~]+)) ?(?:<(\+?[0-9\*#]+)>)?$""".r
  val extenRegexp: Regex = """^\+?[0-9\*#]+$""".r
  def parse(callerid: String): CallerId = {
    callerid match {
      case callerIdRegexp(quotedName, unquotedName, exten)
          if quotedName != null =>
        CallerId(quotedName, exten)
      case callerIdRegexp(quotedName, unquotedName, exten)
          if unquotedName != null =>
        CallerId(unquotedName, exten)
      case extenRegexp(exten) => CallerId("", exten)
      case _                  => CallerId("", callerid)
    }
  }

  def from(optname: Option[String], optnumber: Option[String]): CallerId = {
    (optname, optnumber) match {
      case (Some(name), Some(number)) =>
        CallerId(name.stripPrefix("\"").stripSuffix("\""), number)
      case (Some(name), None) =>
        CallerId(name.stripPrefix("\"").stripSuffix("\""), "")
      case (None, Some(number)) => CallerId("", number)
      case (None, None)         => empty
    }
  }

  val empty: CallerId = CallerId("", "")

}

object ChannelState extends Enumeration(-2) {
  type ChannelState = Value
  val UNITIALIZED, HUNGUP, DOWN, RSRVD, OFFHOOK, DIALING, ORIGINATING, RINGING,
      UP, BUSY, DIALING_OFFHOOK, PRERING, HOLD = Value
}
import xivo.xucami.models.ChannelState.ChannelState

object ChannelDirection extends Enumeration {
  type ChannelDirection = Value
  val INCOMING, OUTGOING = Value

  def fromChannelState(state: ChannelState): Option[ChannelDirection] =
    state match {
      case ChannelState.RINGING     => Some(ChannelDirection.INCOMING)
      case ChannelState.DIALING     => Some(ChannelDirection.OUTGOING)
      case ChannelState.ORIGINATING => Some(ChannelDirection.OUTGOING)
      case _                        => None
    }
}

object MonitorState extends Enumeration {
  type MonitorState = Value
  val DISABLED, ACTIVE, PAUSED, UNKNOWN = Value
}
import xivo.xucami.models.MonitorState.MonitorState

object Channel {

  object VarNames {
    val AttendedTransfer           = "ATTENDEDTRANSFER"
    val TransfererName             = "TRANSFERERNAME"
    val XiVOContext                = "XIVO_CONTEXT"
    val xucCallType                = "XUC_CALLTYPE"
    val xucCallSource              = "XUC_CALLSOURCE"
    val XiVOAnsweredVariable       = "XIVO_PICKEDUP"
    val AsteriskDialStatusVariable = "DIALSTATUS"
    val ConferenceRoleVariable     = "XIVO_MEETMEROLE"
    val ConferenceNumberVariable   = "XIVO_MEETMENUMBER"
    val ConferencePinVariable      = "XIVO_MEETMEPIN"
    val ConferenceInvitation       = "_XUC_MEETMEINVITATION_ORIGINATE"
    val RecordingMode              = "__XIVO_RECORDING_MODE"
    val XiVOFwdReferer             = "__XIVO_FWD_REFERER"
    val XiVOSwitchBoardRetrieve    = "XIVO_SWITCHBOARD_ORIGINATE"
    val XiVOSipDomain              = "_XIVO_SIPDOMAIN"
  }

  val AgentCallBackMark            = "agentcallback"
  val UserDataPrefix               = "USR_"
  val callTypeValOriginate         = "Originate"
  val callTypeValOutboundOriginate = "OutboundOriginate"
  val callSourceMobile             = "MOBILE_APPLICATION"
  val callAnsweredValue            = "1"
  val callAnsweredDialStatus       = "ANSWER"

  def getChanId(astChannel: AsteriskChannel): String =
    astChannel.getLinkedChannel.getId

  def intStateToChannelState(s: Integer): ChannelState.Value =
    if (s != null) ChannelState(s) else ChannelState.UNITIALIZED

  def apply(newChanEvent: NewChannelEvent, mdsName: String): Channel = {
    Channel(
      id = newChanEvent.getUniqueId,
      name = newChanEvent.getChannel,
      callerId =
        CallerId(newChanEvent.getCallerIdName, newChanEvent.getCallerIdNum),
      linkedChannelId = newChanEvent.getLinkedid,
      state = intStateToChannelState(newChanEvent.getChannelState),
      direction = ChannelDirection.fromChannelState(
        ChannelState(newChanEvent.getChannelState)
      ),
      exten = Option(newChanEvent.getExten),
      mdsName = mdsName
    )
  }

  def apply(newChanEvent: CoreShowChannelEvent, mdsName: String): Channel = {
    Channel(
      id = newChanEvent.getUniqueid,
      name = newChanEvent.getChannel,
      callerId =
        CallerId(newChanEvent.getCallerIdName, newChanEvent.getCallerIdNum),
      linkedChannelId = newChanEvent.getLinkedid,
      state = intStateToChannelState(newChanEvent.getChannelState),
      exten = Option(newChanEvent.getExten),
      connectedLineNb = Option(newChanEvent.getConnectedLineNum),
      connectedLineName = Option(newChanEvent.getConnectedLineName),
      mdsName = mdsName
    )
  }
}

case class Channel(
    id: String,
    name: String,
    callerId: CallerId,
    linkedChannelId: String,
    state: ChannelState = ChannelState.UNITIALIZED,
    var monitored: MonitorState = MonitorState.DISABLED,
    exten: Option[String] = None,
    connectedLineNb: Option[String] = None,
    connectedLineName: Option[String] = None,
    var agentNumber: Option[String] = None,
    variables: Map[String, String] = Map(),
    remoteState: Option[ChannelState] = None,
    queueHistory: List[QueueHistoryEvent] = List.empty,
    direction: Option[ChannelDirection.ChannelDirection] = None,
    mdsName: String = "default",
    isGroupPickupInitiator: Boolean = false
) {
  def updateVariable(name: String, value: String): Channel = {
    val newDirection = direction.orElse(
      if (
        List(Channel.VarNames.xucCallType, "__" + Channel.VarNames.xucCallType)
          .contains(name)
        && List(
          Channel.callTypeValOriginate,
          Channel.callTypeValOutboundOriginate
        ).contains(value)
      )
        Some(ChannelDirection.OUTGOING)
      else
        None
    )
    this.copy(
      variables = this.variables + (name -> value),
      direction = newDirection
    )
  }

  def addVariables(newVariables: Map[String, String]): Channel =
    this.copy(variables = this.variables ++ newVariables.filterNot {
      case (key, value) =>
        key == Channel.VarNames.xucCallType && value == Channel.callTypeValOriginate
    })

  def addOriginatePartyState(remote: Channel): Channel = {
    if (isOriginate) {
      this.copy(remoteState = Some(remote.state))
    } else {
      this
    }
  }

  def addUserVariables(newVariables: Map[String, String]): Channel =
    this.copy(variables =
      this.variables ++ newVariables.view
        .filterKeys(_.matches(s"${Channel.UserDataPrefix}.*"))
        .toMap
    )

  def withLinkedId(channelId: String): Channel =
    this.copy(linkedChannelId = channelId)
  def withCallerId(newCallerId: CallerId): Channel =
    this.copy(callerId = newCallerId)
  def withChannelState(newState: ChannelState): Channel =
    this.copy(state = newState)
  def withState(newStEvt: NewStateEvent): Channel = {
    val newDirection = direction
      .orElse(
        ChannelDirection.fromChannelState(
          ChannelState(newStEvt.getChannelState)
        )
      )
      .orElse(if (isOriginate) Some(ChannelDirection.OUTGOING) else None)

    if (this.isAgentCallback) {
      this.copy(
        callerId = CallerId(newStEvt.getCallerIdName, newStEvt.getCallerIdNum),
        state = ChannelState(newStEvt.getChannelState),
        connectedLineNb = Option(newStEvt.getConnectedLineNum),
        connectedLineName = Option(newStEvt.getConnectedLineName),
        direction = newDirection
      )
    } else {
      this.copy(
        state = ChannelState(newStEvt.getChannelState),
        connectedLineNb = Option(newStEvt.getConnectedLineNum),
        connectedLineName = Option(newStEvt.getConnectedLineName),
        direction = newDirection
      )
    }
  }
  def withAgentName(memberName: String): Channel =
    this.copy(agentNumber = memberName.split("/").lastOption)
  def withConnectedLine(num: String, name: String): Channel =
    this.copy(connectedLineNb = Option(num), connectedLineName = Option(name))

  def withQueueHistory(event: QueueHistoryEvent): Channel =
    this.copy(queueHistory = event :: this.queueHistory)

  def withDirection(direction: Option[ChannelDirection]): Channel =
    this.copy(direction = direction)

  def withInterception: Channel =
    this.copy(isGroupPickupInitiator = true)

  def isAgentCallback: Boolean = this.name.contains(Channel.AgentCallBackMark)
  def isLocal: Boolean         = name.startsWith("Local/")
  def isSipAttTransfer: Boolean =
    variables
      .get(VarNames.AttendedTransfer)
      .exists(v =>
        v.startsWith(s"${SipDriver.SIP.toString}/") || v.startsWith(
          s"${SipDriver.PJSIP.toString}/"
        )
      )
  def isACallerIdChannel: Boolean = {
    val channelFormat = """[^/]+/([^@]+)@.+""".r
    name match {
      case channelFormat(number) if callerId.number == number => true
      case _                                                  => false
    }
  }

  def isPickupGroupCallRetrieval: Boolean =
    isGroupPickupInitiator

  def isWaitingInQueue: Boolean = {
    queueHistory.lastOption.fold(false)(_.isInstanceOf[EnterQueue])
  }

  def isOriginate: Boolean =
    variables
      .get(Channel.VarNames.xucCallType)
      .contains(Channel.callTypeValOriginate)

  def isSwitchboardOriginate: Boolean =
    variables.keySet.contains(Channel.VarNames.XiVOSwitchBoardRetrieve)

  def isOutboundOriginate: Boolean =
    variables
      .get(Channel.VarNames.xucCallType)
      .orElse(variables.get("__" + Channel.VarNames.xucCallType))
      .contains(Channel.callTypeValOutboundOriginate)

  def isOriginatePartyUp: Boolean =
    isOriginate && remoteState.contains(ChannelState.UP)

  def isSetAsAnsweredByAsterisk: Boolean =
    variables
      .get(Channel.VarNames.AsteriskDialStatusVariable)
      .contains(Channel.callAnsweredDialStatus)

  def isSetAsAnsweredByXiVOPickUp: Boolean =
    variables
      .get(Channel.VarNames.XiVOAnsweredVariable)
      .contains(Channel.callAnsweredValue)

  def isL2onAttXfer: Boolean =
    variables.contains(VarNames.XiVOContext) && isLocal

  def interface(): String = this.name.split("-").headOption.getOrElse("")

  def lastQueueName: Option[String] =
    queueHistory
      .collect({ case EnterQueue(queueName, _, _, _) => queueName })
      .headOption

  def getDirection(dialstatus: String): Option[ChannelDirection.Value] = {
    val direction: ChannelDirection.Value = dialstatus match {
      case "ANSWER" => ChannelDirection.OUTGOING
      case _        => ChannelDirection.INCOMING
    }

    this.state match {
      case ChannelState.UP          => Some(direction)
      case ChannelState.RINGING     => Some(ChannelDirection.INCOMING)
      case ChannelState.DIALING     => Some(ChannelDirection.OUTGOING)
      case ChannelState.ORIGINATING => Some(ChannelDirection.OUTGOING)
      case _                        => None
    }
  }
}
