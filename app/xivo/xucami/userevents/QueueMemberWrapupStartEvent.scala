package xivo.xucami.userevents

import org.asteriskjava.manager.event.QueueMemberPausedEvent

class QueueMemberWrapupStartEvent(source: Object)
    extends QueueMemberPausedEvent(source)
