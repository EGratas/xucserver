package xivo.models

import anorm.SqlParser.{get => aGet}
import anorm._
import com.google.inject.{ImplementedBy, Inject}
import play.api.Logger
import play.api.db.Database

case class UserLineNumber(userId: Long, lineId: Line.Id, number: String)

@ImplementedBy(classOf[UserLineNumberFactoryImpl])
trait UserLineNumberFactory {
  def get(userId: Long): Option[UserLineNumber]
}

class UserLineNumberFactoryImpl @Inject() (db: Database)
    extends UserLineNumberFactory {
  type Id = Long

  val logger: Logger = Logger(getClass.getName)

  def query(id: Long): String = s"""
    select
      user_line.user_id, user_line.line_id, extensions.exten
    from
      user_line, extensions
    where
      user_line.extension_id=extensions.id and user_line.user_id=$id"""

  val simple: RowParser[UserLineNumber] = {
    aGet[Long]("user_line.user_id") ~
      aGet[Line.Id]("user_line.line_id") ~
      aGet[String]("extensions.exten") map {
        case user_id ~ line_id ~ extension =>
          UserLineNumber(user_id, line_id, extension)
      }
  }

  def get(userId: Long): Option[UserLineNumber] = {
    db.withConnection { implicit c =>
      SQL(query(userId)).as(simple.*).headOption
    } match {
      case Some(userLineNumber) =>
        logger.debug(
          s"Got user line number link from database: $userLineNumber"
        )
        Some(userLineNumber)
      case None =>
        logger.info(
          s"Unable to get user line number link from database for userId: $userId"
        )
        None
    }
  }
}
