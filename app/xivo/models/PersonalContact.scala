package xivo.models

import models.ws.{ErrorResult, ErrorType}
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Format, JsPath, Json, Reads, Writes}
import play.api.mvc.{Result, Results}
import play.api.libs.functional.syntax._

trait PersonalContact {}

case class PersonalContactRequest(
    firstname: Option[String],
    lastname: Option[String],
    number: Option[String],
    mobile: Option[String],
    fax: Option[String],
    email: Option[String],
    company: Option[String]
) extends PersonalContact {}

case class PersonalContactResult(
    id: String,
    firstname: Option[String],
    lastname: Option[String],
    number: Option[String],
    mobile: Option[String],
    fax: Option[String],
    email: Option[String],
    company: Option[String]
) extends PersonalContact

case class PersonalContactList(items: List[PersonalContactResult])

case class PersonalContactImportResult(
    created: Option[List[PersonalContactResult]],
    failed: Option[List[PersonalContactImportFailure]]
)
case class PersonalContactImportFailure(
    line: Option[Int],
    errors: Option[List[String]]
)

object PersonalContactImportResult {
  implicit val format: Format[PersonalContactImportResult] = (
    (JsPath \ "created").formatNullable[List[PersonalContactResult]] and
      (JsPath \ "failed").formatNullable[List[PersonalContactImportFailure]]
  )(PersonalContactImportResult.apply, o => (o.created, o.failed))
}

object PersonalContactImportFailure {
  implicit val format: Format[PersonalContactImportFailure] = (
    (JsPath \ "line").formatNullable[Int] and
      (JsPath \ "errors").formatNullable[List[String]]
  )(PersonalContactImportFailure.apply, o => (o.line, o.errors))
}

object PersonalContactRequest {

  val nonEmptyStringValidation
      : PartialFunction[Option[String], Option[String]] = {
    case Some(s) if s.trim.nonEmpty => Some(s)
    case Some(_)                    => None
    case None                       => None
  }

  implicit val personalContactReads: Reads[PersonalContactRequest] = {
    (
      (JsPath \ "firstname")
        .readNullable[String]
        .map(nonEmptyStringValidation) and
        (JsPath \ "lastname")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "number")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "mobile")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "fax")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "email")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "company")
          .readNullable[String]
          .map(nonEmptyStringValidation)
    )(PersonalContactRequest.apply _)
      .filter(p =>
        (p.firstname.exists(_.trim.nonEmpty) || p.lastname
          .exists(_.trim.nonEmpty))
          && (p.mobile.exists(_.trim.nonEmpty) || p.fax
            .exists(_.trim.nonEmpty) || p.number.exists(_.trim.nonEmpty))
      )
  }

  implicit val personalContactWrites: Writes[PersonalContactRequest] = (
    (JsPath \ "firstname").writeNullable[String] and
      (JsPath \ "lastname").writeNullable[String] and
      (JsPath \ "number").writeNullable[String] and
      (JsPath \ "mobile").writeNullable[String] and
      (JsPath \ "fax").writeNullable[String] and
      (JsPath \ "email").writeNullable[String] and
      (JsPath \ "company").writeNullable[String]
  )(o =>
    (o.firstname, o.lastname, o.number, o.mobile, o.fax, o.email, o.company)
  )
}

object PersonalContactResult {
  implicit val format: Format[PersonalContactResult] = (
    (JsPath \ "id").format[String] and
      (JsPath \ "firstname").formatNullable[String] and
      (JsPath \ "lastname").formatNullable[String] and
      (JsPath \ "number").formatNullable[String] and
      (JsPath \ "mobile").formatNullable[String] and
      (JsPath \ "fax").formatNullable[String] and
      (JsPath \ "email").formatNullable[String] and
      (JsPath \ "company").formatNullable[String]
  )(
    PersonalContactResult.apply,
    o =>
      (
        o.id,
        o.firstname,
        o.lastname,
        o.number,
        o.mobile,
        o.fax,
        o.email,
        o.company
      )
  )
}

object PersonalContactList {
  implicit val format: Format[PersonalContactList] = (JsPath \ "items")
    .format[List[PersonalContactResult]]
    .inmap(i => PersonalContactList(i), o => o.items)

}

case class PersonalContactError(error: ErrorType, message: String)
    extends ErrorResult {

  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case Unreachable => Results.ServiceUnavailable(body)
      case ContactNotFound | DuplicateContact | InvalidContact =>
        Results.BadRequest(body)
      case _ => Results.InternalServerError(body)
    }
  }
}

object PersonalContactError {
  implicit val PersonalContactErrorWrites: Writes[PersonalContactError] = (
    (JsPath \ "error").write[ErrorType] and
      (JsPath \ "message").write[String]
  )(o => (o.error, o.message))
}

trait DirdErrorType     extends ErrorType
case object Unreachable extends DirdErrorType { val error = "Unreachable" }
case object ContactNotFound extends DirdErrorType {
  val error = "ContactNotFound"
}
case object DuplicateContact extends DirdErrorType {
  val error = "DuplicateContact"
}
case object InvalidContact extends DirdErrorType {
  val error = "InvalidContact"
}

class DirdRequesterException(val errorType: ErrorType, val message: String)
    extends Exception(message)

case class DirdError(
    timestamp: List[Double],
    reason: List[String],
    status_code: Int
)
object DirdError {
  implicit val dirdErrorReads: Reads[DirdError] = (
    (JsPath \ "timestamp").read[List[Double]] and
      (JsPath \ "reason").read[List[String]] and
      (JsPath \ "status_code").read[Int]
  )(DirdError.apply)
}
