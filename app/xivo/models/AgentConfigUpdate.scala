package xivo.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class QueueMember(
    queue_name: String,
    queue_id: Long,
    interface: String,
    penalty: Int,
    commented: Int,
    usertype: String,
    userid: Int,
    channel: String,
    category: String,
    position: Int
)

object QueueMember {
  implicit val format: Format[QueueMember] = (
    (JsPath \ "queue_name").format[String] and
      (JsPath \ "queue_id").format[Long] and
      (JsPath \ "interface").format[String] and
      (JsPath \ "penalty").format[Int] and
      (JsPath \ "commented").format[Int] and
      (JsPath \ "usertype").format[String] and
      (JsPath \ "userid").format[Int] and
      (JsPath \ "channel").format[String] and
      (JsPath \ "category").format[String] and
      (JsPath \ "position").format[Int]
  )(
    QueueMember.apply,
    qm =>
      (
        qm.queue_name,
        qm.queue_id,
        qm.interface,
        qm.penalty,
        qm.commented,
        qm.usertype,
        qm.userid,
        qm.channel,
        qm.category,
        qm.position
      )
  )
}

case class AgentConfigUpdate(
    id: Long,
    firstname: String,
    lastname: String,
    number: String,
    context: String,
    member: List[QueueMember],
    numgroup: Long,
    userid: Option[Long]
)

object AgentConfigUpdate {
  implicit val AgentConfigUpdateTmpReads: Reads[AgentConfigUpdate] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "firstname").read[String] and
      (JsPath \ "lastname").read[String] and
      (JsPath \ "number").read[String] and
      (JsPath \ "context").read[String] and
      (JsPath \ "member").read[List[QueueMember]] and
      (JsPath \ "numgroup").read[Long] and
      (JsPath \ "userid").readNullable[Long]
  )(AgentConfigUpdate.apply _)

  implicit val AgentConfigUpdateWrites: Writes[AgentConfigUpdate] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "firstname").write[String] and
      (JsPath \ "lastname").write[String] and
      (JsPath \ "number").write[String] and
      (JsPath \ "context").write[String] and
      (JsPath \ "member").write[List[QueueMember]] and
      (JsPath \ "numgroup").write[Long] and
      (JsPath \ "userid").writeNullable[Long]
  )(acu =>
    (
      acu.id,
      acu.firstname,
      acu.lastname,
      acu.number,
      acu.context,
      acu.member,
      acu.numgroup,
      acu.userid
    )
  )
}
