package xivo.models

import anorm.SqlParser._
import anorm._
import play.api.db.Database

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

case class ExtensionName(value: String) extends AnyVal
object ExtensionName {
  val ProgrammableKey: ExtensionName      = ExtensionName("phoneprogfunckey")
  val DND: ExtensionName                  = ExtensionName("enablednd")
  val UnconditionalForward: ExtensionName = ExtensionName("fwdunc")
  val NoAnswerForward: ExtensionName      = ExtensionName("fwdrna")
  val BusyForward: ExtensionName          = ExtensionName("fwdbusy")

  val all: List[ExtensionName] = List(
    ProgrammableKey,
    DND,
    UnconditionalForward,
    NoAnswerForward,
    BusyForward
  )

  def fromName(name: String): Option[ExtensionName] =
    name match {
      case ProgrammableKey.value      => Some(ProgrammableKey)
      case DND.value                  => Some(DND)
      case UnconditionalForward.value => Some(UnconditionalForward)
      case NoAnswerForward.value      => Some(NoAnswerForward)
      case BusyForward.value          => Some(BusyForward)
      case _                          => None
    }
}

case class ExtensionPattern(name: ExtensionName, pattern: String) {
  def exten(): String = pattern.replaceAll("[\\[NXZ!._]{1}", "")
}

object ExtensionPattern {
  def userService(
      userId: String,
      phoneprogfunckey: ExtensionPattern,
      service: ExtensionPattern,
      destination: Option[String],
      state: FunctionState
  ): FunctionKey = {
    val progExten = phoneprogfunckey.exten()
    val svcExten  = cleanupStar(service.exten(), 2)
    val destExten = destination.map(cleanupStar(_, 3))

    FunctionKey(
      progExten + createService(userId, svcExten, destExten)
        .mkString("*"),
      state
    )
  }

  private def cleanupStar(exten: String, pos: Int): String =
    if (exten.startsWith("*"))
      "**" + pos.toString + exten.stripPrefix("*")
    else
      exten

  private def createService(
      userId: String,
      svcExten: String,
      destination: Option[String]
  ): List[String] =
    List(Some(userId), Some(svcExten), destination).flatten

  private def disableOldServices(
      userId: String,
      previousServices: UserServices,
      services: UserServices,
      phoneprogPattern: ExtensionPattern,
      fwdbusyPattern: ExtensionPattern,
      fwdrnaPattern: ExtensionPattern,
      fwduncPattern: ExtensionPattern
  ): List[FunctionKey] = {

    def compare(
        oldDest: String,
        newDest: String,
        ext: ExtensionPattern
    ): Option[FunctionKey] = {
      if (newDest != oldDest) {
        Some(
          userService(
            userId,
            phoneprogPattern,
            ext,
            Some(oldDest),
            KeyNotInUse
          )
        )
      } else None
    }

    compare(
      previousServices.busy.destination,
      services.busy.destination,
      fwdbusyPattern
    ).toList ++
      compare(
        previousServices.noanswer.destination,
        services.noanswer.destination,
        fwdrnaPattern
      ).toList ++
      compare(
        previousServices.unconditional.destination,
        services.unconditional.destination,
        fwduncPattern
      ).toList
  }

  def userServices(
      userId: String,
      previousServices: Option[UserServices],
      services: UserServices,
      phoneprogPattern: ExtensionPattern,
      dndPattern: ExtensionPattern,
      fwdbusyPattern: ExtensionPattern,
      fwdrnaPattern: ExtensionPattern,
      fwduncPattern: ExtensionPattern
  ): List[FunctionKey] = {
    previousServices
      .map(oldServices =>
        disableOldServices(
          userId,
          oldServices,
          services,
          phoneprogPattern,
          fwdbusyPattern,
          fwdrnaPattern,
          fwduncPattern
        )
      )
      .getOrElse(List()) ++
      List(
        userService(
          userId,
          phoneprogPattern,
          dndPattern,
          None,
          FunctionState.inUse(services.dndEnabled)
        ),
        userService(
          userId,
          phoneprogPattern,
          fwdbusyPattern,
          Some(services.busy.destination),
          FunctionState.inUse(services.busy.enabled)
        ),
        userService(
          userId,
          phoneprogPattern,
          fwdbusyPattern,
          None,
          FunctionState.inUse(services.busy.enabled)
        ),
        userService(
          userId,
          phoneprogPattern,
          fwdrnaPattern,
          Some(services.noanswer.destination),
          FunctionState.inUse(services.noanswer.enabled)
        ),
        userService(
          userId,
          phoneprogPattern,
          fwdrnaPattern,
          None,
          FunctionState.inUse(services.noanswer.enabled)
        ),
        userService(
          userId,
          phoneprogPattern,
          fwduncPattern,
          Some(services.unconditional.destination),
          FunctionState.inUse(services.unconditional.enabled)
        ),
        userService(
          userId,
          phoneprogPattern,
          fwduncPattern,
          None,
          FunctionState.inUse(services.unconditional.enabled)
        )
      )
  }

}

class ExtensionPatternFactory @Inject() (db: Database) {

  implicit val context: ExecutionContext =
    scala.concurrent.ExecutionContext.Implicits.global

  val extensionPattern: RowParser[Option[ExtensionPattern]] =
    str("typeval") ~ str("exten") map { case name ~ exten =>
      ExtensionName.fromName(name).map(ExtensionPattern(_, exten))
    }

  def get(name: ExtensionName): Future[ExtensionPattern] =
    Future(
      db.withConnection { implicit c =>
        val pattern =
          SQL("""SELECT exten FROM extensions WHERE typeval={name}""")
            .on("name" -> name.value)
            .as(SqlParser.str("exten").single)

        ExtensionPattern(name, pattern)
      }
    )

  def getList(names: List[ExtensionName]): Future[List[ExtensionPattern]] =
    Future(
      db.withConnection { implicit c =>
        SQL(
          """SELECT typeval, exten FROM extensions WHERE typeval in ({names})"""
        )
          .on("names" -> names.map(_.value))
          .as(extensionPattern.*)
          .flatten
      }
    )

  def getAll(): Future[List[ExtensionPattern]] =
    getList(ExtensionName.all)
}
