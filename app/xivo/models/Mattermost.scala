package xivo.models

import java.time.{OffsetDateTime, ZoneOffset}
import play.api.libs.json.{
  __,
  Format,
  JsObject,
  JsPath,
  JsValue,
  Json,
  OWrites,
  Reads,
  Writes
}
import xivo.network.ChatBackendWS
import play.api.libs.functional.syntax.*

case class MattermostTimeZone(
    automaticTimezone: String,
    manualTimezone: String,
    useAutomaticTimezone: String
)
case class MattermostGuid(
    guid: String,
    username: String,
    displayName: Option[String]
)

case class UpdateUserWithGuid(guid: MattermostGuid)
case class MattermostParties(from: MattermostGuid, to: MattermostGuid)

case class NoChannelsFoundError(message: String, status_code: Int)

sealed trait MattermostRequest
trait Sequence {
  val sequence: Long
}
case class SendDirectMattermostMessage(
    userFrom: MattermostGuid,
    userTo: MattermostGuid,
    message: String,
    sequence: Long
) extends MattermostRequest
    with Sequence
case class GetDirectMattermostMessagesHistory(
    from: MattermostGuid,
    to: MattermostGuid,
    sequence: Long
) extends MattermostRequest
    with Sequence
case class MarkMattermostMessagesAsRead(
    from: MattermostGuid,
    to: MattermostGuid,
    sequence: Long
) extends MattermostRequest
    with Sequence

case class MattermostDirectChannel(
    id: String,
    team_id: String,
    name: String,
    `type`: String
)
object MattermostDirectChannel {
  implicit val format: Format[MattermostDirectChannel] = (
    (JsPath \ "id").format[String] and
      (JsPath \ "team_id").format[String] and
      (JsPath \ "name").format[String] and
      (JsPath \ "type").format[String]
  )(MattermostDirectChannel.apply, o => (o.id, o.team_id, o.name, o.`type`))
}

case class NewMattermostChannelPost(
    channel_id: String,
    message: String,
    seq: Long,
    from: String,
    to: String
)
object NewMattermostChannelPost {
  implicit val format: Format[NewMattermostChannelPost] = (
    (JsPath \ "channel_id").format[String] and
      (JsPath \ "message").format[String] and
      (JsPath \ "seq").format[Long] and
      (JsPath \ "from").format[String] and
      (JsPath \ "to").format[String]
  )(
    NewMattermostChannelPost.apply,
    o => (o.channel_id, o.message, o.seq, o.from, o.to)
  )
}

case class MattermostDirectMessage(
    id: String,
    channel_id: String,
    message: String,
    create_at: Long,
    user_id: String
) {
  def offsetDateTime: OffsetDateTime =
    new java.util.Date(create_at).toInstant.atOffset(ZoneOffset.UTC)
}
object MattermostDirectMessage {
  implicit val format: Format[MattermostDirectMessage] = (
    (JsPath \ "id").format[String] and
      (JsPath \ "channel_id").format[String] and
      (JsPath \ "message").format[String] and
      (JsPath \ "create_at").format[Long] and
      (JsPath \ "user_id").format[String]
  )(
    MattermostDirectMessage.apply,
    o => (o.id, o.channel_id, o.message, o.create_at, o.user_id)
  )
}

case class MattermostDirectMessageAck(
    id: String,
    channel_id: String,
    message: String,
    create_at: Long,
    user_id: String,
    seq: Long,
    usernameFrom: String,
    usernameTo: String
) {
  def offsetDateTime: OffsetDateTime =
    new java.util.Date(create_at).toInstant.atOffset(ZoneOffset.UTC)
}
object MattermostDirectMessageAck {
  implicit val format: Format[MattermostDirectMessageAck] = (
    (JsPath \ "id").format[String] and
      (JsPath \ "channel_id").format[String] and
      (JsPath \ "message").format[String] and
      (JsPath \ "create_at").format[Long] and
      (JsPath \ "user_id").format[String] and
      (JsPath \ "seq").format[Long] and
      (JsPath \ "usernameFrom").format[String] and
      (JsPath \ "usernameTo").format[String]
  )(
    MattermostDirectMessageAck.apply,
    o =>
      (
        o.id,
        o.channel_id,
        o.message,
        o.create_at,
        o.user_id,
        o.seq,
        o.usernameFrom,
        o.usernameTo
      )
  )
}

case class MattermostUser(
    id: String,
    create_at: Long,
    update_at: Long,
    delete_at: Long,
    username: String,
    auth_data: String,
    email: String,
    nickname: String,
    first_name: String,
    last_name: String,
    position: String,
    roles: String,
    locale: String,
    timezone: MattermostTimeZone
)
object MattermostUser {
  implicit val timezoneFormat: Format[MattermostTimeZone] = (
    (JsPath \ "automaticTimezone").format[String] and
      (JsPath \ "manualTimezone").format[String] and
      (JsPath \ "useAutomaticTimezone").format[String]
  )(
    MattermostTimeZone.apply,
    o => (o.automaticTimezone, o.manualTimezone, o.useAutomaticTimezone)
  )

  implicit val format: Format[MattermostUser] = (
    (JsPath \ "id").format[String] and
      (JsPath \ "create_at").format[Long] and
      (JsPath \ "update_at").format[Long] and
      (JsPath \ "delete_at").format[Long] and
      (JsPath \ "username").format[String] and
      (JsPath \ "auth_data").format[String] and
      (JsPath \ "email").format[String] and
      (JsPath \ "nickname").format[String] and
      (JsPath \ "first_name").format[String] and
      (JsPath \ "last_name").format[String] and
      (JsPath \ "position").format[String] and
      (JsPath \ "roles").format[String] and
      (JsPath \ "locale").format[String] and
      (JsPath \ "timezone").format[MattermostTimeZone]
  )(
    MattermostUser.apply,
    o =>
      (
        o.id,
        o.create_at,
        o.update_at,
        o.delete_at,
        o.username,
        o.auth_data,
        o.email,
        o.nickname,
        o.first_name,
        o.last_name,
        o.position,
        o.roles,
        o.locale,
        o.timezone
      )
  )
}

case class NewMattermostUser(
    email: String,
    username: String,
    password: String,
    first_name: String,
    last_name: String
)
object NewMattermostUser {
  implicit val tjs: Writes[NewMattermostUser] = (
    (JsPath \ "email").write[String] and
      (JsPath \ "username").write[String] and
      (JsPath \ "password").write[String] and
      (JsPath \ "first_name").write[String] and
      (JsPath \ "last_name").write[String]
  )(o => (o.email, o.username, o.password, o.first_name, o.last_name))
  def apply(
      id: Long,
      secret: String,
      first_name: String,
      last_name: String
  ): NewMattermostUser =
    NewMattermostUser(
      ChatBackendWS.getEmailFromId(id),
      ChatBackendWS.getUsernameFromId(id),
      ChatBackendWS.getPasswordFromId(id, secret),
      first_name,
      last_name
    )
}

case class MattermostTeam(id: String, name: String)
object MattermostTeam {
  implicit val format: Format[MattermostTeam] = (
    (JsPath \ "id").format[String] and
      (JsPath \ "name").format[String]
  )(MattermostTeam.apply, o => (o.id, o.name))
}

case class MattermostChannelMember(channel_id: String, user_id: String)
object MattermostChannelMember {
  implicit val format: Format[MattermostChannelMember] = (
    (JsPath \ "channel_id").format[String] and
      (JsPath \ "user_id").format[String]
  )(MattermostChannelMember.apply, o => (o.channel_id, o.user_id))
}

object NoChannelsFoundError {
  implicit val reads: Reads[NoChannelsFoundError] = (
    (JsPath \ "message")
      .read[String]
      .filter(m => m == "No channels were found") and
      (JsPath \ "status_code").read[Int]
  )(NoChannelsFoundError.apply _)
  implicit val writes: OWrites[NoChannelsFoundError] = (
    (JsPath \ "message").write[String] and
      (JsPath \ "status_code").format[Int]
  )(o => (o.message, o.status_code))
}

case class MattermostChannelPosts(
    order: List[String],
    posts: List[MattermostDirectMessage]
)
object MattermostChannelPosts {
  implicit val reads: Reads[MattermostChannelPosts] = (
    (JsPath \ "order").read[List[String]] and
      (__ \ "posts").read[JsValue].map {
        case j: JsObject =>
          j.values.flatMap(r => r.asOpt[MattermostDirectMessage]).toList
        case _ => List()
      }
  )(MattermostChannelPosts.apply _)
}

case class MattermostUnreadCounter(
    team_id: String,
    channel_id: String,
    msg_count: Int
)
object MattermostUnreadCounter {
  implicit val format: Format[MattermostUnreadCounter] = (
    (JsPath \ "team_id").format[String] and
      (JsPath \ "channel_id").format[String] and
      (JsPath \ "msg_count").format[Int]
  )(MattermostUnreadCounter.apply, o => (o.team_id, o.channel_id, o.msg_count))
}

case class MattermostViewChannelResponse(
    status: String,
    last_viewed_at_times: JsObject
)
object MattermostViewChannelResponse {
  implicit val format: Format[MattermostViewChannelResponse] = (
    (JsPath \ "status").format[String] and
      (JsPath \ "last_viewed_at_times").format[JsObject]
  )(
    MattermostViewChannelResponse.apply,
    o => (o.status, o.last_viewed_at_times)
  )
}

case class MattermostViewChannel(channel_id: String)
object MattermostViewChannel {
  implicit val format: Format[MattermostViewChannel] = (JsPath \ "channel_id")
    .format[String]
    .inmap(str => MattermostViewChannel(str), mvc => mvc.channel_id)
}

case class MattermostUnreadNotification(
    direction: MattermostMessageDirection,
    usernameFrom: String,
    userIdTo: Long,
    message: MattermostDirectMessage,
    offsetDateTime: OffsetDateTime,
    seq: Long
)

sealed trait MattermostMessageDirection
case object DirectionIsFromUser extends MattermostMessageDirection
case object DirectionIsToUser   extends MattermostMessageDirection

case class MattermostNoUnreadMessages(channel: MattermostDirectChannel)
    extends Exception
