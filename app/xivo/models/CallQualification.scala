package xivo.models

import models.ws.{ErrorResult, ErrorType}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc.{Result, Results}

case class CallQualificationAnswer(
    sub_qualification_id: Int,
    time: String,
    callid: String,
    agent: Int,
    queue: Int,
    firstName: String,
    lastName: String,
    comment: String,
    customData: String
)
case class CallQualification(
    id: Option[Long],
    name: String,
    subQualification: List[SubQualification]
) {
  def toJson: JsValue =
    Json.obj(
      "id"                -> id,
      "qualificationName" -> name
    )
}
case class SubQualification(id: Option[Long], name: String) {
  def toJson: JsValue =
    Json.obj(
      "id"                   -> id,
      "subQualificationName" -> name
    )
}

object CallQualificationAnswer {

  implicit val format: Format[CallQualificationAnswer] = (
    (JsPath \ "sub_qualification_id").format[Int] and
      (JsPath \ "time").format[String] and
      (JsPath \ "callid").format[String] and
      (JsPath \ "agent").format[Int] and
      (JsPath \ "queue").format[Int] and
      (JsPath \ "first_name").format[String] and
      (JsPath \ "last_name").format[String] and
      (JsPath \ "comment").format[String] and
      (JsPath \ "custom_data").format[String]
  )(
    CallQualificationAnswer.apply,
    o =>
      (
        o.sub_qualification_id,
        o.time,
        o.callid,
        o.agent,
        o.queue,
        o.firstName,
        o.lastName,
        o.comment,
        o.customData
      )
  )
}

object SubQualification {

  implicit val subQualificationWrites: Writes[SubQualification] =
    (subQualification: SubQualification) =>
      Json.obj(
        "id"   -> subQualification.id,
        "name" -> subQualification.name
      )

  implicit val subQualificationReads: Reads[SubQualification] = (
    (JsPath \ "id").readNullable[Long] and
      (JsPath \ "name").read[String]
  )(SubQualification.apply _)
}

object CallQualification {

  implicit val callQualificationWrites: Writes[CallQualification] =
    (qualification: CallQualification) =>
      Json.obj(
        "id"                -> qualification.id,
        "name"              -> qualification.name,
        "subQualifications" -> qualification.subQualification
      )

  implicit val callQualificationReads: Reads[CallQualification] = (
    (JsPath \ "id").readNullable[Long] and
      (JsPath \ "name").read[String] and
      (JsPath \ "subQualifications").read[List[SubQualification]]
  )(CallQualification.apply _)
}

class CallQualificationException(val errorType: ErrorType, val message: String)
    extends Exception

trait CallQualificationErrorType extends ErrorType
case object CallQualificationJsonBodyNotFound
    extends CallQualificationErrorType {
  val error = "JsonBodyNotFound"
}
case object CallQualificationJsonErrorDecoding
    extends CallQualificationErrorType {
  val error = "JsonErrorDecoding"
}

case class CallQualificationError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case CallQualificationJsonBodyNotFound  => Results.BadRequest(body)
      case CallQualificationJsonErrorDecoding => Results.BadRequest(body)
      case _                                  => Results.InternalServerError(body)
    }
  }
}
object CallQualificationError {
  implicit val CallQualificationErrorWrites: Writes[CallQualificationError] = (
    (JsPath \ "error").write[ErrorType] and
      (JsPath \ "message").write[String]
  )(o => (o.error, o.message))
}
