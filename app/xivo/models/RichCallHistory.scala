package xivo.models

import org.joda.time.format.{DateTimeFormat, PeriodFormatterBuilder}
import org.joda.time.{DateTime, Period}
import org.xivo.cti.model.PhoneHintStatus
import play.api.libs.json.{JsArray, JsValue, Json, Writes}
import services.video.model.VideoEvents
import services.video.model.VideoEvents.Event
import org.joda.time.format.{DateTimeFormatter, PeriodFormatter}

trait RichCallDetailTrait extends CallDetailTrait {
  def srcUsername: String
  def dstUsername: String

  def srcPhoneStatus: PhoneHintStatus
  def srcVideoStatus: VideoEvents.Event
  def dstPhoneStatus: PhoneHintStatus
  def dstVideoStatus: VideoEvents.Event
}

case class RichCallDetail(
    start: DateTime,
    duration: Option[Period],
    srcUsername: String,
    dstUsername: String,
    status: CallStatus.CallStatus,
    srcPhoneStatus: PhoneHintStatus,
    srcVideoStatus: VideoEvents.Event,
    dstPhoneStatus: PhoneHintStatus,
    dstVideoStatus: VideoEvents.Event,
    srcNum: String,
    dstNum: Option[String],
    srcFirstName: Option[String] = None,
    srcLastName: Option[String] = None,
    dstFirstName: Option[String] = None,
    dstLastName: Option[String] = None
) extends RichCallDetailTrait
object RichCallDetail {
  val format: DateTimeFormatter =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val periodFormat: PeriodFormatter = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2)
    .printZeroAlways()
    .appendHours()
    .appendSeparator(":")
    .appendMinutes()
    .appendSeparator(":")
    .appendSeconds()
    .toFormatter

  def apply(
      cd: CallDetail,
      phoneNbToUserName: String => String,
      phoneNbToPhoneStatus: String => PhoneHintStatus,
      phoneNbToVideoStatus: String => Event
  ): RichCallDetail = RichCallDetail(
    cd.start,
    cd.duration,
    phoneNbToUserName(cd.srcNum),
    phoneNbToUserName(cd.dstNum.getOrElse("")),
    cd.status,
    phoneNbToPhoneStatus(cd.srcNum),
    phoneNbToVideoStatus(cd.srcNum),
    phoneNbToPhoneStatus(cd.dstNum.getOrElse("")),
    phoneNbToVideoStatus(cd.dstNum.getOrElse("")),
    cd.srcNum,
    cd.dstNum,
    cd.srcFirstName,
    cd.srcLastName,
    cd.dstFirstName,
    cd.dstLastName
  )

  implicit val writes: Writes[RichCallDetail] = new Writes[RichCallDetail] {
    def writes(call: RichCallDetail): JsValue =
      Json.obj(
        "start"          -> call.start.toString(format),
        "duration"       -> call.duration.map(_.toString(periodFormat)),
        "srcUsername"    -> call.srcUsername,
        "dstUsername"    -> call.dstUsername,
        "status"         -> call.status.toString,
        "srcPhoneStatus" -> call.srcPhoneStatus.getHintStatus(),
        "srcVideoStatus" -> call.srcVideoStatus.toString,
        "dstPhoneStatus" -> call.dstPhoneStatus.getHintStatus(),
        "dstVideoStatus" -> call.dstVideoStatus.toString,
        "srcNum"         -> call.srcNum,
        "dstNum"         -> call.dstNum,
        "srcFirstName"   -> call.srcFirstName,
        "srcLastName"    -> call.srcLastName,
        "dstFirstName"   -> call.dstFirstName,
        "dstLastName"    -> call.dstLastName
      )
  }
}

case class RichCallHistory(calls: List[RichCallDetail])

object RichCallHistory {
  def apply(
      history: CallHistory,
      phoneNbToUserName: String => String,
      phoneNbToPhoneStatus: String => PhoneHintStatus,
      phoneNbToVideoStatus: String => Event
  ): RichCallHistory =
    RichCallHistory(
      history.calls.map(
        RichCallDetail.apply(
          _,
          phoneNbToUserName: String => String,
          phoneNbToPhoneStatus: String => PhoneHintStatus,
          phoneNbToVideoStatus: String => Event
        )
      )
    )

  implicit val writes: Writes[RichCallHistory] = new Writes[RichCallHistory] {
    override def writes(h: RichCallHistory): JsValue =
      JsArray(h.calls.map(Json.toJson(_)))
  }
}
