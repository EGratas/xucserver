package xivo.models

import play.api.libs.json.{Format, JsPath, Json, Reads, Writes}
import play.api.libs.functional.syntax._

case class IceServer(
    stunAddress: Option[String]
)

object IceServer {
  implicit val format: Format[IceServer] =
    (JsPath \ "stunAddress")
      .formatNullable[String]
      .inmap(s => IceServer(s), i => i.stunAddress)
}
