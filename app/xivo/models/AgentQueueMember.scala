package xivo.models

import anorm.SqlParser._
import anorm._
import com.google.inject.{ImplementedBy, Inject}
import play.api.Logger
import play.api.db.Database
import play.api.http.Status._
import play.api.libs.functional.syntax._
import play.api.libs.json.Writes._
import play.api.libs.json.{Json, _}
import services.config.ConfigServerRequester
import xivo.network.XiVOWS
import play.api.libs.ws.JsonBodyWritables.writeableOf_JsValue
import play.api.libs.ws.WSBodyWritables.writeableOf_JsValue
import play.api.libs.ws.writeableOf_JsValue
import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.DurationInt
import scala.concurrent.duration.FiniteDuration

case class AgentQueueMember(agentId: Agent.Id, queueId: Long, penalty: Int)

object AgentQueueMember {

  implicit val agqmWrites: OWrites[AgentQueueMember] =
    ((__ \ "agentId").write[Long] and
      (__ \ "queueId").write[Long] and
      (__ \ "penalty").write[Int])(aw =>
      (
        aw.agentId,
        aw.queueId,
        aw.penalty
      )
    )

  implicit val agqmReads: Reads[AgentQueueMember] = (
    (JsPath \ "agent_id").read[Agent.Id] and
      (JsPath \ "queue_id").read[Long] and
      (JsPath \ "penalty").read[Int]
  )(AgentQueueMember.apply _)

  def toJson(agentQueueMember: AgentQueueMember): JsValue =
    Json.toJson(agentQueueMember)
  def toJson(agentQueueMembers: List[AgentQueueMember]): JsValue =
    Json.toJson(agentQueueMembers)
}

@ImplementedBy(classOf[AgentQueueMemberFactoryImpl])
trait AgentQueueMemberFactory {
  def removeAgentFromQueue(
      agentId: Agent.Id,
      queueId: Long
  ): Option[AgentQueueMember]
  def setAgentQueue(
      agentId: Agent.Id,
      queueId: Long,
      penalty: Int
  ): Option[AgentQueueMember]
  def all(): List[AgentQueueMember]
  def getQueueMember(
      agentNumber: String,
      queueName: String
  ): Option[AgentQueueMember]
}

class AgentQueueMemberFactoryImpl @Inject() (
    db: Database,
    xivoWs: XiVOWS,
    configServerRequester: ConfigServerRequester
)(implicit ec: ExecutionContext)
    extends AgentQueueMemberFactory {

  val logger: Logger             = Logger(getClass.getName)
  val waitResult: FiniteDuration = 10.seconds

  def removeAgentFromQueue(
      agentId: Agent.Id,
      queueId: Long
  ): Option[AgentQueueMember] = {
    val responseFuture =
      xivoWs.withWS(s"queues/$queueId/members/agents/$agentId").delete()
    val resultFuture = responseFuture map { response =>
      response.status match {
        case NO_CONTENT => Some(AgentQueueMember(agentId, queueId, -1))
        case _ =>
          logger.error(
            s"Webservice unable to remove agent  $agentId from queue $queueId"
          )
          None
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def associateAgentToQueue(
      agentId: Agent.Id,
      queueId: Long,
      penalty: Int
  ): Option[AgentQueueMember] = {
    val data = Json.obj(
      "agent_id" -> agentId,
      "penalty"  -> penalty
    )
    val responseFuture = xivoWs
      .withWS(s"queues/$queueId/members/agents")
      .post(data)
    val resultFuture = responseFuture map { response =>
      response.status match {
        case CREATED => getAgentAssociation(agentId, queueId)
        case _ =>
          logger.error(
            s"Webservice unable to set agent association for agent: $agentId queue $queueId failed."
          )
          None
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def setAgentQueue(
      agentId: Agent.Id,
      queueId: Long,
      penalty: Int
  ): Option[AgentQueueMember] = {

    val data = Json.obj(
      "penalty" -> penalty
    )

    val responseFuture =
      xivoWs.withWS(s"queues/$queueId/members/agents/$agentId").put(data)
    val resultFuture = responseFuture map { response =>
      response.status match {
        case NOT_FOUND  => associateAgentToQueue(agentId, queueId, penalty)
        case NO_CONTENT => getAgentAssociation(agentId, queueId)
        case _ =>
          logger.error(
            s"Webservice set agent association for agent: $agentId queue $queueId failed."
          )
          None
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def getAgentAssociation(
      agentId: Agent.Id,
      queueId: Long
  ): Option[AgentQueueMember] = {
    val futRes = configServerRequester
      .getAgentConfig(agentId)
      .map(
        _.member
          .filter(_.queue_id == queueId)
          .headOption
          .map(_.penalty)
          .map(AgentQueueMember(agentId, queueId, _))
      )
    Await.result(futRes, waitResult)
  }

  val simple: RowParser[AgentQueueMember] = {
    get[Long]("agentid") ~
      get[Long]("queueid") ~
      get[Int]("penalty") map { case agentId ~ queueId ~ penalty =>
        AgentQueueMember(agentId, queueId, penalty)
      }
  }

  def all(): List[AgentQueueMember] = {
    db.withConnection { implicit c =>
      SQL(
        """select userid as agentid, id as queueid, penalty from queuemember, queuefeatures
          where queuemember.usertype='agent' and queuefeatures.name = queuemember.queue_name"""
      )
        .as(simple.*)
    }
  }

  private def query(agentNumber: String, queueName: String) = s"""
      select userid as agentid, id as queueid, penalty from queuemember, queuefeatures
      where split_part(interface,'/',2) = '$agentNumber' and queue_name = '$queueName'
      and queuefeatures.name = queuemember.queue_name
      """

  def getQueueMember(
      agentNumber: String,
      queueName: String
  ): Option[AgentQueueMember] = {
    db.withConnection { implicit c =>
      SQL(query(agentNumber, queueName)).as(simple.singleOpt)
    }
  }
}
