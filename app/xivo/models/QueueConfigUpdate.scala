package xivo.models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.*

case class QueueConfigUpdate(
    id: Long,
    name: String,
    displayName: String,
    number: String,
    context: Option[String],
    data_quality: Int,
    retries: Int,
    ring: Int,
    transfer_user: Int,
    transfer_call: Int,
    write_caller: Int,
    write_calling: Int,
    url: String,
    announceoverride: String,
    timeout: Option[Int],
    preprocess_subroutine: Option[String],
    announce_holdtime: Int,
    waittime: Option[Int],
    waitratio: Option[Double],
    ignore_forward: Int,
    recording_mode: String,
    recording_activated: Int
)

object QueueConfigUpdate {
  implicit val format: Format[QueueConfigUpdate] = (
    (JsPath \ "id").format[Long] and
      (JsPath \ "name").format[String] and
      (JsPath \ "displayName").format[String] and
      (JsPath \ "number").format[String] and
      (JsPath \ "context").formatNullable[String] and
      (JsPath \ "data_quality").format[Int] and
      (JsPath \ "retries").format[Int] and
      (JsPath \ "ring").format[Int] and
      (JsPath \ "transfer_user").format[Int] and
      (JsPath \ "transfer_call").format[Int] and
      (JsPath \ "write_caller").format[Int] and
      (JsPath \ "write_calling").format[Int] and
      (JsPath \ "url").format[String] and
      (JsPath \ "announceoverride").format[String] and
      (JsPath \ "timeout").formatNullable[Int] and
      (JsPath \ "preprocess_subroutine").formatNullable[String] and
      (JsPath \ "announce_holdtime").format[Int] and
      (JsPath \ "waittime").formatNullable[Int] and
      (JsPath \ "waitratio").formatNullable[Double] and
      (JsPath \ "ignore_forward").format[Int] and
      (JsPath \ "recording_mode").format[String] and
      (JsPath \ "recording_activated").format[Int]
  )(
    QueueConfigUpdate.apply,
    o =>
      (
        o.id,
        o.name,
        o.displayName,
        o.number,
        o.context,
        o.data_quality,
        o.retries,
        o.ring,
        o.transfer_user,
        o.transfer_call,
        o.write_caller,
        o.write_calling,
        o.url,
        o.announceoverride,
        o.timeout,
        o.preprocess_subroutine,
        o.announce_holdtime,
        o.waittime,
        o.waitratio,
        o.ignore_forward,
        o.recording_mode,
        o.recording_activated
      )
  )
}
