package xivo.models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{
  Format,
  JsPath,
  JsResult,
  JsValue,
  Json,
  Reads,
  Writes
}

case class UserForward(
    enabled: Boolean,
    destination: String,
    domain: String = "default"
)

case class UserServices(
    dndEnabled: Boolean,
    busy: UserForward,
    noanswer: UserForward,
    unconditional: UserForward
) {
  def update(partial: PartialUserServices): UserServices = {
    this.copy(
      dndEnabled = partial.dndEnabled.getOrElse(dndEnabled),
      busy = partial.busy.getOrElse(busy),
      noanswer = partial.noanswer.getOrElse(noanswer),
      unconditional = partial.unconditional.getOrElse(unconditional)
    )
  }

}

case class PartialUserServices(
    dndEnabled: Option[Boolean],
    busy: Option[UserForward],
    noanswer: Option[UserForward],
    unconditional: Option[UserForward]
)

case class UserServicesUpdated(userId: Int, services: UserServices)

object UserForward {
  def validate(json: JsValue): JsResult[UserForward] =
    json.validate[UserForward]

  implicit val reads: Reads[UserForward] =
    ((JsPath \ "enabled").read[Boolean] and
      (JsPath \ "destination").read[String] and
      (JsPath \ "domain")
        .readNullable[String]
        .map(_.getOrElse("default")))(UserForward.apply _)
  implicit val writes: Writes[UserForward] = (
    (JsPath \ "enabled").write[Boolean] and
      (JsPath \ "destination").write[String] and
      (JsPath \ "domain").write[String]
  )(o => (o.enabled, o.destination, o.domain))
}

object UserServices {
  implicit val format: Format[UserServices] = (
    (JsPath \ "dndEnabled").format[Boolean] and
      (JsPath \ "busy").format[UserForward] and
      (JsPath \ "noanswer").format[UserForward] and
      (JsPath \ "unconditional").format[UserForward]
  )(
    UserServices.apply,
    o => (o.dndEnabled, o.busy, o.noanswer, o.unconditional)
  )
}

object PartialUserServices {
  implicit val format: Format[PartialUserServices] = (
    (JsPath \ "dndEnabled").formatNullable[Boolean] and
      (JsPath \ "busy").formatNullable[UserForward] and
      (JsPath \ "noanswer").formatNullable[UserForward] and
      (JsPath \ "unconditional").formatNullable[UserForward]
  )(
    PartialUserServices.apply,
    o => (o.dndEnabled, o.busy, o.noanswer, o.unconditional)
  )
}
