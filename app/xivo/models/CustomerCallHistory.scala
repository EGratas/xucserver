package xivo.models

import models.DynamicFilter
import org.joda.time.format.{DateTimeFormat, PeriodFormatterBuilder}
import org.joda.time.{DateTime, Period}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import org.joda.time.format.{DateTimeFormatter, PeriodFormatter}

case class CustomerCallDetail(
    start: DateTime,
    duration: Option[Period],
    waitTime: Option[Period],
    agentName: Option[String],
    agentNum: Option[String],
    queueName: Option[String],
    queueNum: Option[String],
    status: CallStatus.CallStatus
)

object CustomerCallDetail {

  val format: DateTimeFormatter =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val periodFormat: PeriodFormatter = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2)
    .printZeroAlways()
    .appendHours()
    .appendSeparator(":")
    .appendMinutes()
    .appendSeparator(":")
    .appendSeconds()
    .toFormatter

  def validate(json: JsValue): JsResult[CustomerCallDetail] =
    json.validate[CustomerCallDetail]

  implicit val reads: Reads[CustomerCallDetail] =
    ((JsPath \ "start").read[String].map(format.parseDateTime) and
      (JsPath \ "duration")
        .readNullable[String]
        .map(sOpt => sOpt.map(periodFormat.parsePeriod)) and
      (JsPath \ "wait_time")
        .readNullable[String]
        .map(sOpt => sOpt.map(periodFormat.parsePeriod)) and
      (JsPath \ "agent_name").readNullable[String] and
      (JsPath \ "agent_num").readNullable[String] and
      (JsPath \ "queue_name").readNullable[String] and
      (JsPath \ "queue_num").readNullable[String] and
      (JsPath \ "status")
        .readNullable[String]
        .map(s => CallStatus.withName(s.getOrElse("answered"))))(
      CustomerCallDetail.apply _
    )

  implicit val writes: Writes[CustomerCallDetail] =
    new Writes[CustomerCallDetail] {
      def writes(call: CustomerCallDetail): JsValue =
        Json.obj(
          "start"     -> call.start.toString(format),
          "duration"  -> call.duration.map(_.toString(periodFormat)),
          "waitTime"  -> call.waitTime.map(_.toString(periodFormat)),
          "agentName" -> call.agentName,
          "agentNum"  -> call.agentNum,
          "queueName" -> call.queueName,
          "queueNum"  -> call.queueNum,
          "status"    -> call.status.toString
        )
    }
}

case class FindCustomerCallHistoryRequest(
    filters: List[DynamicFilter],
    size: Int = 10
)
case class FindCustomerCallHistoryResponse(
    total: Long,
    list: List[CustomerCallDetail]
)

object FindCustomerCallHistoryRequest {
  implicit val format: Format[FindCustomerCallHistoryRequest] = (
    (JsPath \ "filters").format[List[DynamicFilter]] and
      (JsPath \ "size").formatWithDefault[Int](10)
  )(FindCustomerCallHistoryRequest.apply, o => (o.filters, o.size))

  def validate(json: JsValue): JsResult[FindCustomerCallHistoryRequest] =
    json.validate[FindCustomerCallHistoryRequest]
}

object FindCustomerCallHistoryResponse {
  implicit val format: Format[FindCustomerCallHistoryResponse] = (
    (JsPath \ "total").format[Long] and
      (JsPath \ "list").format[List[CustomerCallDetail]]
  )(FindCustomerCallHistoryResponse.apply, o => (o.total, o.list))
}
