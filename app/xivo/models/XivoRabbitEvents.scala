package xivo.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

object RabbitEventType extends Enumeration {
  type RabbitType = String

  val QueueCreated: RabbitType           = "queue_created"
  val QueueEdited: RabbitType            = "queue_edited"
  val AgentEdited: RabbitType            = "agent_edited"
  val AgentDeleted: RabbitType           = "agent_deleted"
  val AgentCreated: RabbitType           = "agent_created"
  val MediaServerCreated: RabbitType     = "mediaserver_created"
  val MediaServerEdited: RabbitType      = "mediaserver_edited"
  val MediaServerDeleted: RabbitType     = "mediaserver_deleted"
  val SipEndpointEdited: RabbitType      = "sip_endpoint_edited"
  val UserServicesEdited: RabbitType     = "user_services_edited"
  val SipConfigEdited: RabbitType        = "sip_config_edited"
  val UserConfigEdited: RabbitType       = "user_edited"
  val UserPreferenceCreated: RabbitType  = "user_preference_created"
  val UserPreferenceEdited: RabbitType   = "user_preference_edited"
  val UserPreferenceDeleted: RabbitType  = "user_preference_deleted"
  val MobilePushTokenAdded: RabbitType   = "mobile_push_token_added"
  val MobilePushTokenDeleted: RabbitType = "mobile_push_token_deleted"
  val WebserviceUserCreated: RabbitType  = "webservice_user_created"
  val WebserviceUserEdited: RabbitType   = "webservice_user_edited"
  val WebserviceUsersReload: RabbitType  = "webservice_users_reload"
  val CtiStatusReload: RabbitType        = "ctistatus_reload"
}

abstract class RabbitEvent
case class RabbitEventData(id: Long, value: Option[String])
case class RabbitEventQueueEdited(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventData {
  implicit val eventDataReads: Reads[RabbitEventData] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "value").readNullable[String]
  )(RabbitEventData.apply _)
}

object DefaultRabbitEventReader {
  def read[T] = (JsPath \ "data").read[RabbitEventData] and
    (JsPath \ "name").read[String]
}

object RabbitEventQueueEdited {
  implicit val eventQueueEditedReads: Reads[RabbitEventQueueEdited] =
    DefaultRabbitEventReader.read(RabbitEventQueueEdited.apply _)
}

case class RabbitEventQueueCreated(data: RabbitEventData, name: String)
    extends RabbitEvent
object RabbitEventQueueCreated {
  implicit val rabbitEventRead: Reads[RabbitEventQueueCreated] =
    DefaultRabbitEventReader.read(RabbitEventQueueCreated.apply)
}

case class RabbitEventAgentDeletedData(id: Long)
case class RabbitEventAgentDeleted(
    data: RabbitEventAgentDeletedData,
    name: String
) extends RabbitEvent

object RabbitEventAgentDeletedData {
  implicit val eventAgentDeletedDataReads: Reads[RabbitEventAgentDeletedData] =
    (JsPath \ "id").read[Long].map(id => RabbitEventAgentDeletedData.apply(id))
}
object RabbitEventAgentDeleted {
  implicit val eventAgentDeletedReads: Reads[RabbitEventAgentDeleted] = (
    (JsPath \ "data").read[RabbitEventAgentDeletedData] and
      (JsPath \ "name").read[String]
  )(RabbitEventAgentDeleted.apply)
}

case class RabbitEventAgentCreatedData(id: Long)
case class RabbitEventAgentCreated(
    data: RabbitEventAgentCreatedData,
    name: String
) extends RabbitEvent

object RabbitEventAgentCreatedData {
  implicit val eventAgentAddedDataReads: Reads[RabbitEventAgentCreatedData] =
    (JsPath \ "id").read[Long].map(id => RabbitEventAgentCreatedData.apply(id))
}
object RabbitEventAgentCreated {
  implicit val eventAgentAddedReads: Reads[RabbitEventAgentCreated] = (
    (JsPath \ "data").read[RabbitEventAgentCreatedData] and
      (JsPath \ "name").read[String]
  )(RabbitEventAgentCreated.apply)
}

case class RabbitEventAgentEditedData(id: Long)
case class RabbitEventAgentEdited(
    data: RabbitEventAgentEditedData,
    name: String
) extends RabbitEvent

object RabbitEventAgentEditedData {
  implicit val eventAgentEditedDataReads: Reads[RabbitEventAgentEditedData] =
    (JsPath \ "id").read[Long].map(id => RabbitEventAgentEditedData.apply(id))
}

object RabbitEventAgentEdited {
  implicit val eventAgentEditedReads: Reads[RabbitEventAgentEdited] = (
    (JsPath \ "data").read[RabbitEventAgentEditedData] and
      (JsPath \ "name").read[String]
  )(RabbitEventAgentEdited.apply _)
}

case class RabbitEventMediaServerCreated(data: RabbitEventData, name: String)
    extends RabbitEvent
case class RabbitEventMediaServerEdited(data: RabbitEventData, name: String)
    extends RabbitEvent
case class RabbitEventMediaServerDeleted(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventMediaServerCreated {
  implicit val rabbitEventRead: Reads[RabbitEventMediaServerCreated] =
    DefaultRabbitEventReader.read(RabbitEventMediaServerCreated.apply)
}

object RabbitEventMediaServerEdited {
  implicit val rabbitEventRead: Reads[RabbitEventMediaServerEdited] =
    DefaultRabbitEventReader.read(RabbitEventMediaServerEdited.apply)
}

object RabbitEventMediaServerDeleted {
  implicit val rabbitEventRead: Reads[RabbitEventMediaServerDeleted] =
    DefaultRabbitEventReader.read(RabbitEventMediaServerDeleted.apply)
}

case class RabbitEventSipEndpointEdited(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventSipEndpointEdited {
  implicit val rabbitEventRead: Reads[RabbitEventSipEndpointEdited] =
    DefaultRabbitEventReader.read(RabbitEventSipEndpointEdited.apply)
}

case class RabbitEventUserServicesEdited(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventUserServicesEdited {
  implicit val rabbitEventRead: Reads[RabbitEventUserServicesEdited] =
    DefaultRabbitEventReader.read(RabbitEventUserServicesEdited.apply)
}

case class RabbitEventSipConfigEdited(name: String) extends RabbitEvent

object RabbitEventSipConfigEdited {
  implicit val rabbitEventRead: Reads[RabbitEventSipConfigEdited] =
    (JsPath \ "name")
      .read[String]
      .map(name => RabbitEventSipConfigEdited.apply(name))
}

case class RabbitEventUserConfigEdited(data: RabbitEventData)
    extends RabbitEvent

object RabbitEventUserConfigEdited {
  implicit val rabbitEventRead: Reads[RabbitEventUserConfigEdited] =
    (JsPath \ "data")
      .read[RabbitEventData]
      .map(data => RabbitEventUserConfigEdited.apply(data))
}

case class RabbitEventUserPreferenceCreated(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventUserPreferenceCreated {
  implicit val rabbitEventRead: Reads[RabbitEventUserPreferenceCreated] =
    DefaultRabbitEventReader.read(RabbitEventUserPreferenceCreated.apply)
}

case class RabbitEventUserPreferenceEdited(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventUserPreferenceEdited {
  implicit val rabbitEventRead: Reads[RabbitEventUserPreferenceEdited] =
    DefaultRabbitEventReader.read(RabbitEventUserPreferenceEdited.apply)
}
case class RabbitEventUserPreferenceDeleted(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventUserPreferenceDeleted {
  implicit val rabbitEventRead: Reads[RabbitEventUserPreferenceDeleted] =
    DefaultRabbitEventReader.read(RabbitEventUserPreferenceDeleted.apply)
}

case class RabbitEventMobilePushTokenDeleted(
    data: RabbitEventData,
    name: String
) extends RabbitEvent

object RabbitEventMobilePushTokenDeleted {
  implicit val rabbitEventRead: Reads[RabbitEventMobilePushTokenDeleted] =
    DefaultRabbitEventReader.read(RabbitEventMobilePushTokenDeleted.apply)
}

case class RabbitEventMobilePushTokenAdded(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventMobilePushTokenAdded {
  implicit val rabbitEventRead: Reads[RabbitEventMobilePushTokenAdded] =
    DefaultRabbitEventReader.read(RabbitEventMobilePushTokenAdded.apply)
}

case class RabbitEventWebserviceUserData(login: String)
object RabbitEventWebserviceUserData {
  implicit val eventWebserviceUserDataReads
      : Reads[RabbitEventWebserviceUserData] = (JsPath \ "login")
    .read[String]
    .map(login => RabbitEventWebserviceUserData.apply(login))
}

case class RabbitEventWebserviceUserCreated(
    data: RabbitEventWebserviceUserData,
    name: String
) extends RabbitEvent

object RabbitEventWebserviceUserCreated {
  implicit val rabbitEventRead: Reads[RabbitEventWebserviceUserCreated] = (
    (JsPath \ "data").read[RabbitEventWebserviceUserData] and
      (JsPath \ "name").read[String]
  )(RabbitEventWebserviceUserCreated.apply _)
}

case class RabbitEventWebserviceUserEdited(
    data: RabbitEventWebserviceUserData,
    name: String
) extends RabbitEvent

object RabbitEventWebserviceUserEdited {
  implicit val rabbitEventRead: Reads[RabbitEventWebserviceUserEdited] = (
    (JsPath \ "data").read[RabbitEventWebserviceUserData] and
      (JsPath \ "name").read[String]
  )(RabbitEventWebserviceUserEdited.apply _)
}

case class RabbitEventWebserviceUsersReload(name: String) extends RabbitEvent

object RabbitEventWebserviceUsersReload {
  implicit val rabbitEventRead: Reads[RabbitEventWebserviceUsersReload] =
    (JsPath \ "name")
      .read[String]
      .map(name => RabbitEventWebserviceUsersReload.apply(name))
}

case class RabbitEventConnectionClosed() extends RabbitEvent

case class RabbitEventCtiStatusReload(name: String) extends RabbitEvent

object RabbitEventCtiStatusReload {
  implicit val rabbitEventRead: Reads[RabbitEventCtiStatusReload] =
    (JsPath \ "name")
      .read[String]
      .map(name => RabbitEventCtiStatusReload.apply(name))
}
