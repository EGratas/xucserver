package xivo.ami

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import com.google.inject.Inject
import org.xivo.cti.model.{Counter, StatName}
import com.google.inject.name.Named
import org.asteriskjava.manager.event.{
  OriginateResponseEvent,
  QueueEntryEvent,
  QueueMemberEvent,
  QueueMemberPauseEvent,
  QueueSummaryEvent
}
import org.xivo.cti.message.QueueStatistics
import play.api.libs.json.Writes._
import play.api.libs.json._
import services.XucAmiBus._
import services.channel.ChannelRequestProc._
import services.config.ConfigRepository
import services.line.LineState
import services.line.LineState.LineState
import services.request.KeyLightRequest
import services.{ActorIds, XucAmiBus, XucEventBus}
import xivo.ami.AmiBusConnector.{
  AgentListenStarted,
  AgentListenStopped,
  AgentSpyStart,
  AgentSpyStop
}
import xivo.events.PhoneEventType.EventFailure
import xivo.events._
import xivo.models.Agent
import xivo.models.Agent.Id
import xivo.phonedevices._
import xivo.xuc.ChanSpyNotificationConfig
import xivo.xucami.models.MonitorState.MonitorState
import xivo.xucami.models._
import xivo.xucami.userevents.{
  QueueMemberWrapupStartEvent,
  UserEventAgentLogin,
  UserEventAgentLogoff
}

object AmiBusConnector {

  case class CallData(
      linkedId: String,
      uniqueId: String,
      state: LineState,
      callerIdNb: String,
      variables: Map[String, String] = Map()
  )
  implicit val callDataWrites: Writes[CallData] = (cd: CallData) =>
    Json.obj(
      "callId"    -> cd.linkedId,
      "lineState" -> cd.state.toString
    )
  object CallData {
    def apply(channel: Channel): CallData =
      CallData(
        channel.linkedChannelId,
        channel.id,
        LineState(channel.state.id),
        channel.callerId.number,
        channel.variables
      )
  }

  abstract class AgentListenNotification(
      val phoneNumber: String,
      val agentId: Option[Agent.Id]
  )

  case class AgentListenStarted(
      override val phoneNumber: String,
      override val agentId: Option[Agent.Id]
  ) extends AgentListenNotification(phoneNumber, agentId)
  implicit val agentListenStartedWrites: Writes[AgentListenStarted] =
    (als: AgentListenStarted) =>
      Json.obj(
        "started"     -> true,
        "phoneNumber" -> als.phoneNumber,
        "agentId"     -> als.agentId
      )

  case class AgentListenStopped(
      override val phoneNumber: String,
      override val agentId: Option[Agent.Id]
  ) extends AgentListenNotification(phoneNumber, agentId)
  implicit val agentListenStoppedWrites: Writes[AgentListenStopped] =
    (als: AgentListenStopped) =>
      Json.obj(
        "started"     -> false,
        "phoneNumber" -> als.phoneNumber,
        "agentId"     -> als.agentId
      )

  abstract class AgentSpyNotification(val id: Agent.Id, val phoneNb: String)
  case class AgentSpyStart(
      override val id: Agent.Id,
      override val phoneNb: String
  ) extends AgentSpyNotification(id, phoneNb)
  case class AgentSpyStop(
      override val id: Agent.Id,
      override val phoneNb: String
  ) extends AgentSpyNotification(id, phoneNb)
}

case class AgentCallUpdate(
    agentId: Agent.Id,
    monitorState: MonitorState
)

trait PhoneActionProcessor {
  this: AmiBusConnector =>

  def processPhoneRequest: Receive = {
    case HangupCommand(line, phoneNb) =>
      amiBus.publish(ChannelRequest(HangupActionReq(line.interface, phoneNb)))

    case AttendedTransferCommand(line, destination) =>
      amiBus.publish(
        ChannelRequest(
          AtxFerActionReq(line.interface, destination, line.context)
        )
      )

    case CompleteTransferCommand(line) =>
      amiBus.publish(ChannelRequest(CompleteXferActionReq(line.interface)))

    case CancelTransferCommand(line, phoneNb) =>
      amiBus.publish(
        ChannelRequest(CancelXferActionReq(line.interface, phoneNb))
      )

    case DirectTransferCommand(line, destination) =>
      amiBus.publish(
        ChannelRequest(
          DirectXferActionReq(line.interface, destination, line.context)
        )
      )

    case SetDataCommand(phoneNb, variables) =>
      def updateKey(key: String) =
        if (key.startsWith(UserData.incPrefix)) key
        else s"${UserData.incPrefix}$key"

      amiBus.publish(
        ChannelRequest(
          SetDataActionReq(phoneNb, variables.map(v => (updateKey(v._1), v._2)))
        )
      )
  }
}

trait SpyActionProcessor {
  this: AmiBusConnector =>

  def processSpyRequest: Receive = {
    case SpyStarted(SpyChannels(spyerChannel, spyeeChannel)) =>
      log.debug(s"spy started $spyerChannel, $spyeeChannel")
      val agentLoggedOnPhoneNumber =
        configRepository.getAgentLoggedOnPhoneNumber(
          spyeeChannel.callerId.number
        )
      notifyAgentSpyStarted(
        spyerChannel,
        spyeeChannel,
        agentLoggedOnPhoneNumber
      )
      eventBus.publish(
        AgentListenStarted(
          spyeeChannel.callerId.number,
          agentLoggedOnPhoneNumber
        )
      )

    case SpyStopped(
          Channel(
            _,
            _,
            CallerId(_, number),
            _,
            _,
            _,
            _,
            _,
            _,
            _,
            _,
            _,
            _,
            _,
            _,
            _
          )
        ) =>
      val agentLoggedOnPhoneNumber =
        configRepository.getAgentLoggedOnPhoneNumber(number)
      notifyAgentSpyStopped(number, agentLoggedOnPhoneNumber)
      eventBus.publish(
        AgentListenStopped(
          number,
          configRepository.getAgentLoggedOnPhoneNumber(number)
        )
      )
  }

  private def notifyAgentSpyStarted(
      spyerChannel: Channel,
      spyeeChannel: Channel,
      agentLoggedOnPhoneNumber: Option[Id]
  ) = {
    if (
      !spyerChannel.variables.contains(
        "XIVO_CHANNEL_TO_BEEP"
      ) && spyConfig.enableChanSpyBeep
    ) {
      amiBus.publish(BeepRequest(BeepActionRequest(spyeeChannel.name)))
      agentLoggedOnPhoneNumber.map(id =>
        agentDeviceManager ! AgentSpyStart(id, spyeeChannel.callerId.number)
      )
    }
  }

  private def notifyAgentSpyStopped(
      number: String,
      agentLoggedOnPhoneNumber: Option[Id]
  ) = {
    agentLoggedOnPhoneNumber.map(id =>
      agentDeviceManager ! AgentSpyStop(id, number)
    )
  }
}

class AmiBusConnector @Inject() (
    val configRepository: ConfigRepository,
    @Named(ActorIds.AgentManagerId) val agentManager: ActorRef,
    @Named(ActorIds.ConfigDispatcherId) val configDispatcher: ActorRef,
    @Named(ActorIds.AgentDeviceManagerId) val agentDeviceManager: ActorRef,
    val amiBus: XucAmiBus,
    val eventBus: XucEventBus,
    val spyConfig: ChanSpyNotificationConfig
) extends Actor
    with ActorLogging
    with PhoneActionProcessor
    with SpyActionProcessor {

  amiBus.subscribe(self, AmiType.ChannelEvent)
  amiBus.subscribe(self, AmiType.AmiAgentEvent)
  amiBus.subscribe(self, AmiType.AmiEvent)

  def receive: Receive =
    processPhoneRequest orElse processSpyRequest orElse myReceive

  def myReceive: Receive = {

    case ChannelEvent(channel, false) =>
      processChannel(channel)

    case DialAnswered(channel) =>
      processChannel(channel)

    case AmiAgentEvent(event: UserEventAgentLogin) =>
      agentManager ! EventAgentLogin(event)

    case AmiAgentEvent(event: UserEventAgentLogoff) =>
      agentManager ! EventAgentLogout(event)

    case AmiAgentEvent(event: QueueMemberPauseEvent) =>
      getAgentId(event.getMemberName).foreach(id => {
        if (event.getPaused()) {
          agentManager ! EventAgentPause(
            id,
            Option(event.getPausedreason).filter(_.nonEmpty)
          )
        } else {
          agentManager ! EventAgentUnPause(id)
        }
      })

    case AmiEvent(evt: OriginateResponseEvent, _) =>
      configRepository
        .getPhoneNbfromInterface(evt.getChannel)
        .foreach(srcNum =>
          if (!evt.isSuccess) {
            configDispatcher ! PhoneEvent(
              EventFailure,
              srcNum,
              evt.getCallerIdNum,
              evt.getCallerIdName,
              evt.getUniqueId,
              evt.getUniqueId
            )
          }
        )

    case AmiAgentEvent(event: QueueMemberWrapupStartEvent) =>
      getAgentId(event.getMemberName).foreach(id => {
        agentManager ! EventAgentWrapup(id)
      })

    case AmiAgentEvent(event: QueueMemberEvent) =>
      if (event.getPaused) {
        getAgentId(event.getName).foreach(id => {
          log.debug(s"EventAgentPause $id, ${event.getPausedreason}")
          agentManager ! EventAgentPause(id, Some(event.getPausedreason))
        })
      }

    case klReq: KeyLightRequest =>
      log.debug(s"$klReq")
      amiBus.publish(klReq.toAmi)

    case listenRequest: ListenRequest =>
      log.debug(s"$listenRequest")
      amiBus.publish(listenRequest)

    case queuePauseRequest: QueuePauseRequest =>
      log.debug(s"$queuePauseRequest")
      amiBus.publish(AmiRequest(queuePauseRequest.message))

    case queueUnpauseRequest: QueueUnpauseRequest =>
      log.debug(s"$queueUnpauseRequest")
      amiBus.publish(AmiRequest(queueUnpauseRequest.message))

    case HangupChannelCommand(channel) =>
      log.debug(s"Hanging up channel $channel")
      amiBus.publish(
        AmiRequest(
          SetChannelVarRequest(channel, "CHANNEL(hangupsource)", channel)
        )
      )
      amiBus.publish(AmiRequest(HangupActionRequest(channel)))

    case AmiAgentEvent(event: QueueEntryEvent) =>
      configDispatcher ! event

    case request: RequestToAmi =>
      request.toAmi() match {
        case Some(amiRequest) =>
          log.info(s"ami request $amiRequest")
          amiBus.publish(AmiRequest(amiRequest))
        case None =>
          log.debug("AMI Request could not be sent")
      }

    case RequestToMds(request, targetMds) =>
      request.toAmi() match {
        case Some(amiRequest) =>
          log.debug(s"MDS AMI request $amiRequest to $targetMds")
          amiBus.publish(AmiRequest(amiRequest, targetMds))
        case None =>
          log.debug(
            s"AMI Request $request to MDS to $targetMds could not be sent, trying to unpack"
          )
          self forward request
      }

    case AmiEvent(event: QueueSummaryEvent, _) =>
      log.info(
        s"Transforming and publishing a QueueSummaryEvent fot queue: ${event.getQueue}"
      )
      configRepository
        .getQueue(event.getQueue)
        .map(queue => {
          val queueStatistics = new QueueStatistics;
          queueStatistics.setQueueId(
            queue.id.toInt
          )
          queueStatistics.addCounter(
            new Counter(StatName.AvailableAgents, event.getAvailable)
          )
          queueStatistics.addCounter(
            new Counter(StatName.TalkingAgents, event.getCallers)
          )
          queueStatistics.addCounter(
            new Counter(StatName.EWT, event.getHoldTime)
          )
          queueStatistics.addCounter(
            new Counter(StatName.LongestWaitTime, event.getLongestHoldTime)
          )
          configDispatcher ! queueStatistics
        })
        .getOrElse(
          log.debug(
            s"Queue ${event.getQueue} is not found, maybe it's a group ?"
          )
        )

    case any =>
      log.debug(s"Received unprocessed message: $any")
  }

  private def getAgentId(memberName: String): Option[Long] =
    Option(memberName)
      .filter(_.startsWith("Agent/"))
      .map(_.drop("Agent/".length))
      .flatMap(name => configRepository.getAgent(name))
      .map(_.id)

  private def processChannel(channel: Channel): Unit =
    for {
      number <- channel.agentNumber
      agent  <- configRepository.getAgent(number)
    } agentManager ! AgentCallUpdate(agent.id.toInt, channel.monitored)

}
