package xivo.phonedevices

import play.api.Logger
import org.apache.pekko.stream.Materializer
import javax.inject.Inject
import play.api.libs.ws.WSClient
import xivo.models.Line
import xivo.xuc.XucConfig

class DeviceAdapterFactory @Inject() (ws: WSClient, config: XucConfig)(implicit
    mat: Materializer
) {

  val logger: Logger = Logger(getClass.getName)

  def getAdapter(line: Line, phoneNb: String): DeviceAdapter =
    lineAdapter(line, phoneNb).getOrElse(defaultAdapter(line, phoneNb))

  private def lineAdapter(line: Line, phoneNb: String): Option[DeviceAdapter] =
    (for {
      device <- line.dev
      ip     <- device.ip
    } yield vendorAdapter(device.vendor, ip, line, phoneNb)).flatten

  private def vendorAdapter(
      vendor: String,
      ip: String,
      line: Line,
      phoneNb: String
  ): Option[DeviceAdapter] =
    vendor match {
      case "Snom"    => Some(new SnomDevice(ip, line, phoneNb, ws, config))
      case "Polycom" => Some(new PolycomDevice(ip, line, phoneNb, ws, config))
      case "Yealink" => Some(new YealinkDevice(ip, line, phoneNb, config))
      case "Aastra"  => Some(new AastraDevice(ip, line, phoneNb, config))
      case _         => None
    }

  private def defaultAdapter(line: Line, phoneNb: String) = {
    if (line.webRTC) {
      new WebRTCDevice(line, phoneNb, config)
    } else {
      new CtiDevice(line, phoneNb, config)
    }
  }
}
