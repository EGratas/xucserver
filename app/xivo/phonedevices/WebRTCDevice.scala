package xivo.phonedevices

import org.apache.pekko.actor.ActorRef
import play.api.libs.json.{Format, Reads, Writes}
import services.calltracking.DeviceCall
import xivo.models.Line
import xivo.websocket._
import xivo.xuc.XucConfig

class WebRTCDevice(line: Line, lineNumber: String, config: XucConfig)
    extends CtiDevice(line, lineNumber, config) {

  logger.info(s"Created WebRTC device for line number $lineNumber")

  def getSipCallId(call: Option[DeviceCall]): Option[String] = {
    call.flatMap(c =>
      c.channel.flatMap(ch => { ch.variables.get("SIPCALLID").orElse(None) })
    )
  }

  override def answer(call: Option[DeviceCall], sender: ActorRef): Unit = {
    val callId = getSipCallId(call)
    logger.info(
      s"Sending a webrtc answer command to the websocket for channel $callId"
    )
    sender ! WebSocketEvent.createWebRTCCommand(WebRTCAnswerCmd(callId))
  }

  override def hold(call: Option[DeviceCall], sender: ActorRef): Unit = {
    val callId = getSipCallId(call)
    logger.info(
      s"Sending a webrtc hold command to the websocket for callId $callId"
    )
    sender ! WebSocketEvent.createWebRTCCommand(WebRTCHoldCmd(callId))
  }

  override def sendDtmf(key: Char, sender: ActorRef): Unit = {
    logger.info(s"Sending a webrtc Dtmf command with $key to the websocket")
    sender ! WebSocketEvent.createWebRTCCommand(WebRTCSendDtmfCmd(key))
  }

  override def toggleMicrophone(
      call: Option[DeviceCall],
      sender: ActorRef
  ): Unit = {
    val callId = getSipCallId(call)
    logger.info(
      s"Sending a webrtc toggleMicrophone command to the websocket for callId $callId"
    )
    sender ! WebSocketEvent.createWebRTCCommand(WebRTCMuteCmd(callId))
  }
}

sealed abstract class WebRTCDeviceBearer(val name: String)
object WebRTCDeviceBearer {
  case object MobileApp extends WebRTCDeviceBearer("MobileApp")
  case object WebApp    extends WebRTCDeviceBearer("WebApp")
  val All: Seq[WebRTCDeviceBearer] = List(MobileApp, WebApp)

  implicit val format: Format[WebRTCDeviceBearer] = Format(
    Reads.StringReads.map { x =>
      All.find(_.name == x) match {
        case Some(r) => r
        case None =>
          throw new IllegalArgumentException(
            s"Can't parse '$x' as WebRTCDeviceBearer"
          )
      }
    },
    Writes.StringWrites.contramap(_.name)
  )
}
