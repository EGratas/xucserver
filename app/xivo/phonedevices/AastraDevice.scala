package xivo.phonedevices

import org.apache.pekko.actor.ActorRef
import services.calltracking.DeviceCall
import xivo.models.Line
import xivo.phonedevices.AastraDevice.Keys
import xivo.xuc.XucConfig

object AastraDevice {
  object Keys {
    val Line1 = "Line1"
    val hold  = "Hold"
  }
}

class AastraDevice(
    ip: String,
    line: Line,
    lineNumber: String,
    config: XucConfig
) extends CtiDevice(line, lineNumber, config) {

  private val contentType = Map("Content-type" -> "application/xml")
  private val event       = Map("Event" -> "aastra-xml")

  def content(key: String): String =
    s"""<AastraIPPhoneExecute> <ExecuteItem URI='Key:$key'/> </AastraIPPhoneExecute>""".stripMargin

  def xml(key: String): Map[String, String] =
    Map("content" -> content(key)) ++ contentType ++ event

  override def answer(call: Option[DeviceCall], sender: ActorRef): Unit =
    sender ! SipNotifyCommand(line.interface, xml(Keys.Line1), line.driver)

  override def hold(call: Option[DeviceCall], sender: ActorRef): Unit =
    sender ! SipNotifyCommand(line.interface, xml(Keys.hold), line.driver)

}
