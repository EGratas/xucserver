package xivo.phonedevices

import org.apache.pekko.actor.ActorRef
import play.api.Logger
import services.XucAmiBus._
import services.calltracking.DeviceCall
import services.calltracking.SipDriver.SipDriver
import xivo.models.{Agent, Line}
import xivo.xucami.models.QueueCall

sealed trait RequestToAmi {
  def toAmi(): Option[AmiActionRequest]
}

case class RequestToMds(request: RequestToAmi, targetMds: Option[String])

case class OutboundDial(
    destination: String,
    agentId: Agent.Id,
    queueNumber: String,
    variables: Map[String, String],
    xivoHost: String,
    xivoUserId: Long,
    driver: SipDriver
) extends RequestToAmi {

  def toAmi(): Some[OutBoundDialActionRequest] =
    Some(
      OutBoundDialActionRequest(
        destination,
        s"select_agent(agent=agent_$agentId)",
        queueNumber,
        variables,
        xivoHost,
        xivoUserId,
        driver
      )
    )
}

case class StandardDial(
    line: Line,
    destination: String,
    variables: Map[String, String],
    xivoHost: String,
    driver: SipDriver
) extends RequestToAmi {

  def toAmi(): Some[DialActionRequest] =
    Some(
      DialActionRequest(
        s"${line.interface}",
        line.context,
        line.callerId,
        destination,
        variables,
        xivoHost,
        driver
      )
    )
}

case class ListenCallbackMessage(
    line: Line,
    voiceMessageRef: String,
    variables: Map[String, String],
    xivoHost: String,
    driver: SipDriver
) extends RequestToAmi {
  def toAmi(): Some[ListenCallbackMessageActionRequest] =
    Some(
      ListenCallbackMessageActionRequest(
        s"${line.interface}",
        voiceMessageRef,
        variables,
        xivoHost,
        driver
      )
    )
}

case class DialFromMobile(
    mobileNumber: String,
    callee: String,
    callerNumber: String,
    callerName: String,
    variables: Map[String, String],
    xivoHost: String,
    xivoUserId: Long
) extends RequestToAmi {
  def toAmi(): Some[DialFromMobileActionRequest] =
    Some(
      DialFromMobileActionRequest(
        mobileNumber,
        callee,
        callerNumber,
        callerName,
        variables,
        xivoHost,
        xivoUserId
      )
    )
}

case class DialWithLocalChannelCommand(
    callerInterface: String,
    callerName: String,
    callerNum: String,
    destination: String,
    device: Option[WebRTCDeviceBearer],
    xivoHost: String,
    xivoUserId: Long,
    userContext: String,
    variables: Map[String, String] = Map.empty,
    driver: SipDriver
) extends RequestToAmi {
  def toAmi(): Some[DialWithLocalChannelRequest] =
    Some(
      DialWithLocalChannelRequest(
        callerInterface,
        callerName,
        callerNum,
        destination,
        device,
        xivoHost,
        xivoUserId,
        userContext,
        variables,
        driver
      )
    )
}

case class HangupCommand(line: Line, phoneNb: String) extends RequestToAmi {
  def toAmi(): Option[AmiActionRequest] = None
}

case class AttendedTransferCommand(line: Line, destination: String)
    extends RequestToAmi {
  def toAmi(): Option[AmiActionRequest] = None
}

case class CompleteTransferCommand(line: Line) extends RequestToAmi {
  def toAmi(): Option[AmiActionRequest] = None
}

case class CancelTransferCommand(line: Line, phoneNb: String)
    extends RequestToAmi {
  def toAmi(): Option[AmiActionRequest] = None
}

case class DirectTransferCommand(line: Line, destination: String)
    extends RequestToAmi {
  def toAmi(): Option[AmiActionRequest] = None
}

object SipNotifyCommand {
  def apply(
      channel: String,
      variables: Map[String, String],
      driver: SipDriver
  ) = new SipNotifyCommand(channel, variables, driver)
}

case class SipNotifyCommand(
    channel: String,
    variables: Map[String, String],
    driver: SipDriver
) extends RequestToAmi {
  def toAmi(): Some[SipNotifyRequest] = Some(
    SipNotifyRequest(channel, variables, driver)
  )
}

case class SetDataCommand(phoneNb: String, variables: Map[String, String])
    extends RequestToAmi {
  def toAmi(): Option[AmiActionRequest] = None
}

case class SetVarCommand(channel: String, variable: String, value: String)
    extends RequestToAmi {
  def toAmi(): Some[SetChannelVarRequest] = Some(
    SetChannelVarRequest(channel, variable, value)
  )
}

case class RedirectCommand(
    channel: String,
    context: String,
    exten: String,
    priority: Int
) extends RequestToAmi {
  def toAmi(): Some[RedirectRequest] = Some(
    RedirectRequest(channel, context, exten, priority)
  )
}

case class MultipleRedirectCommand(
    channel: String,
    context: String,
    exten: String,
    priority: Int,
    extraChannel: String,
    extraContext: String,
    extraExten: String,
    extraPriority: Int
) extends RequestToAmi {
  def toAmi(): Some[MultipleRedirectRequest] =
    Some(
      MultipleRedirectRequest(
        channel,
        context,
        exten,
        priority,
        extraChannel,
        extraContext,
        extraExten,
        extraPriority
      )
    )
}

case class BridgeChannelCommand(channel1: String, channel2: String)
    extends RequestToAmi {
  def toAmi(): Some[BridgeActionRequest] = Some(
    BridgeActionRequest(channel1, channel2)
  )
}

case class HangupChannelCommand(channel: String) extends RequestToAmi {
  def toAmi(): Some[HangupActionRequest] = Some(HangupActionRequest(channel))
}

case class LocalOptimizeAwayCommand(channel: String) extends RequestToAmi {
  def toAmi(): Some[LocalOptimizeAwayRequest] = Some(
    LocalOptimizeAwayRequest(channel)
  )
}

case class QueueLogEventCommand(
    queue: String,
    event: String,
    uniqueId: String,
    message: String
) extends RequestToAmi {
  def toAmi(): Some[QueueLogEventRequest] = Some(
    QueueLogEventRequest(queue, event, uniqueId, message)
  )
}

case class QueueStatusCommand(member: String) extends RequestToAmi {
  def toAmi(): Some[QueueStatusRequest] = Some(QueueStatusRequest(member))
}

case class QueueSummaryCommand(queue: String) extends RequestToAmi {
  def toAmi(): Option[QueueSummaryRequest] = Some(QueueSummaryRequest(queue))
}

case class RetrieveQueueCall(
    sourceChannel: String,
    sourceNumber: Option[String],
    sourceName: String,
    callerChannel: String,
    callerNumber: String,
    callerName: Option[String],
    variables: Map[String, String],
    xivoHost: String,
    callId: String,
    queueName: Option[String],
    agentNum: Option[String],
    autoAnswer: Boolean,
    driver: SipDriver
) extends RequestToAmi {
  def toAmi(): Some[RetrieveQueueCallActionRequest] =
    Some(
      RetrieveQueueCallActionRequest(
        sourceChannel,
        sourceNumber,
        sourceName,
        callerChannel,
        callerNumber,
        callerName,
        variables,
        xivoHost,
        callId,
        queueName,
        agentNum,
        autoAnswer,
        driver
      )
    )
}

object QueueLogEventCommand {
  val QueueLogTransferEvent = "XIVO_QUEUE_TRANSFER"
}

trait DeviceAdapter {
  val logger: Logger = Logger(getClass.getName)
  def dial(
      destination: String,
      variables: Map[String, String],
      sender: ActorRef
  ): Unit
  def dialFromMobile(
      mobileNumber: String,
      destination: String,
      callerNumber: String,
      callerName: String,
      variables: Map[String, String],
      sender: ActorRef,
      xivoUserid: Long
  ): Unit
  def listenCallbackMessage(
      voiceMessageRef: String,
      variables: Map[String, String],
      sender: ActorRef
  ): Unit
  def answer(call: Option[DeviceCall], sender: ActorRef): Unit
  def hangup(sender: ActorRef): Unit
  def attendedTransfer(destination: String, sender: ActorRef): Unit
  def completeTransfer(sender: ActorRef): Unit
  def completeTransferAmi(
      channel1: String,
      channel2: String,
      sender: ActorRef
  ): Unit
  def cancelTransfer(sender: ActorRef): Unit
  def directTransfer(s: String, self: ActorRef): Unit = {}
  def conference(sender: ActorRef): Unit = {
    logger.info("Not implemented for this device")
  }
  def hold(call: Option[DeviceCall], sender: ActorRef): Unit = {}
  def odial(
      destination: String,
      agentId: Agent.Id,
      queueNumber: String,
      variables: Map[String, String],
      xivoUserId: Long,
      sender: ActorRef,
      driver: SipDriver
  ): Unit
  def sendDtmf(key: Char, sender: ActorRef): Unit = {
    logger.info("Not implemented for this device")
  }
  def retrieveQueueCall(
      sourceChannel: String,
      sourceNumber: Option[String],
      sourceName: String,
      queueCall: QueueCall,
      variables: Map[String, String],
      sender: ActorRef,
      callId: String,
      queueName: Option[String],
      agentNum: Option[String],
      autoAnswer: Boolean,
      driver: SipDriver
  ): Unit
  def toggleMicrophone(call: Option[DeviceCall], sender: ActorRef): Unit = {
    logger.info("Not implemented for this device")
  }
}
