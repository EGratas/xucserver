package xivo.xuc

import com.google.inject.{ImplementedBy, Inject, Singleton}
import play.api.Configuration
import play.api.libs.ws.WSAuthScheme
import services.calltracking.SipDriver

import scala.concurrent.duration._
import scala.util.Random

@ImplementedBy(classOf[XucConfig])
trait TransferConfig {
  def enableAmiTransfer: Boolean
  def amiTransferTimeoutSeconds: Int
  def conferenceRefreshTimerSeconds: Int
}

@ImplementedBy(classOf[XucConfig])
trait ChannelTrackerConfig {
  def enableChannelTrackerThrottling: Boolean
  def channelTrackerThrottleTimeoutMs: Int
  def retryGetCurrentChannelsDelay: Int
}

@ImplementedBy(classOf[XucConfig])
trait DeviceTrackerConfig {
  def stopRecordingUponExternalXfer: Boolean
  def enableRecordingRules: Boolean
}

@ImplementedBy(classOf[XucConfig])
trait KerberosSsoConfig {
  def kerberosPrincipal: Option[String]
  def kerberosKeytab: Option[String]
  def kerberosPassword: Option[String]
  def kerberosDebug: Boolean
}

@ImplementedBy(classOf[XucConfig])
trait CasSsoConfig {
  def casServerUrl: Option[String]
}

@ImplementedBy(classOf[XucConfig])
trait OidcSsoConfig {
  def oidcEnable: Boolean
  def oidcServerUrl: Option[String]
  def oidcClientId: Option[String]
  def oidcAudience: List[String]
  def oidcUsernameField: Option[String]
  def oidcAdditionalTrustedServersUrl: List[String]
  def oidcAdditionalTrustedClientIDs: List[String]
}

@ImplementedBy(classOf[XucConfig])
trait RecordingConfig {
  def recordingHost: String
  def recordingPort: Int
  def recordingToken: String
}

@ImplementedBy(classOf[XucConfig])
trait IceServerConfig {
  def stunEnable: Boolean
  def turnEnable: Boolean
  def turnSecret: String
  def turnSecretTtl: Long
}

@ImplementedBy(classOf[XucConfig])
trait ChatConfig {
  def chatEnable: Boolean
  def chatHost: String
  def chatPort: String
  def chatAdminToken: String
  def secret: Option[String]
}
@ImplementedBy(classOf[XucConfig])
trait IPFilterConfig {
  def ipAccepted: List[String]
}

@ImplementedBy(classOf[XucConfig])
trait SnomDeviceConfig {
  def SnomUser: String
  def SnomPassword: String
}

@ImplementedBy(classOf[XucConfig])
trait PolycomDeviceConfig {
  def PolycomUser: String
  def PolycomPassword: String
}

@ImplementedBy(classOf[XucConfig])
trait ChanSpyNotificationConfig {
  def enableChanSpyBeep: Boolean
}

@ImplementedBy(classOf[XucConfig])
trait SipConfig {
  def sipDriver: SipDriver.SipDriver

  def sipPort: Option[String]
}

class ConfigServerConfig @Inject() (configuration: Configuration) {
  def configHost: String =
    configuration.getOptional[String]("config.host").getOrElse("")
  def configPort: Int =
    configuration.getOptional[Int]("config.port").getOrElse(0)
  def configToken: String =
    configuration.getOptional[String]("config.token").getOrElse("")
  def configHttpContext: String =
    configuration.getOptional[String]("config.httpContext").getOrElse("")
  def retryCount: Int =
    configuration.getOptional[Int]("config.retryCount").getOrElse(100)
  def retryDelay: FiniteDuration =
    configuration
      .getOptional[Int]("config.retryDelaySeconds")
      .map(_.seconds)
      .getOrElse(2.minutes)
}

@ImplementedBy(classOf[XucConfig])
trait XucBaseConfig
    extends TransferConfig
    with ChannelTrackerConfig
    with DeviceTrackerConfig
    with KerberosSsoConfig
    with CasSsoConfig
    with OidcSsoConfig
    with RecordingConfig
    with SnomDeviceConfig
    with PolycomDeviceConfig
    with ChatConfig
    with ChanSpyNotificationConfig
    with IceServerConfig
    with IPFilterConfig
    with SipConfig
    with WebServiceUserConfig
    with CtiUserConfig {

  def configuration: Configuration
  val DefaultCtiVersion = "2.1"

  def xivoHost: String =
    configuration.getOptional[String]("xivohost").getOrElse("")

  def outboundLength: Int =
    configuration.getOptional[Int]("xuc.outboundLength").getOrElse(6)

  val XivoRestApiKey = "authentication.xivo.restapi"
  def EventUser: String =
    configuration.getOptional[String]("xivocti.eventUser").getOrElse("")
  def XivoCtiTimeout: Int =
    configuration.getOptional[String]("xivocti.timeout").getOrElse("1").toInt
  def XivoCtiVersion: String =
    configuration
      .getOptional[String]("xivocti.version")
      .getOrElse(DefaultCtiVersion)

  def XivoWs_host: String =
    configuration.getOptional[String]("XivoWs.host").getOrElse("")
  def XivoWs_port: String =
    configuration.getOptional[String]("XivoWs.port").getOrElse("")
  def XivoWs_wsUser: String =
    configuration.getOptional[String]("XivoWs.wsUser").getOrElse("")
  def XivoWs_wsPwd: String =
    configuration.getOptional[String]("XivoWs.wsPwd").getOrElse("")
  def XivoWs_confdToken: Option[String] =
    if (configuration.keys.contains(s"$XivoRestApiKey.token")) {
      configuration.getOptional[String](s"$XivoRestApiKey.token")
    } else { None }

  def WsSslHandshakeTimeout: Int =
    configuration.getOptional[Int]("Ws.sslHandshakeTimeout").getOrElse(50000)
  def WsRequestTimeout: Long =
    configuration.getOptional[Long]("Ws.requestTimeout").getOrElse(50000L)

  def EVENT_URL: String =
    configuration.getOptional[String]("api.eventUrl").getOrElse("")

  def XIVOCTI_HOST: String =
    configuration.getOptional[String]("xivocti.host").getOrElse("localhost")
  def XIVOCTI_PORT: String =
    configuration.getOptional[String]("xivocti.port").getOrElse("5003")

  def ipAccepted: List[String] = {
    configuration
      .getOptional[String]("api.ipaccepted")
      .map(_.split(",").map(_.trim).toList)
      .getOrElse(List[String]())
  }

  def SnomUser: String =
    configuration.getOptional[String]("Snom.user").getOrElse("")
  def SnomPassword: String =
    configuration.getOptional[String]("Snom.password").getOrElse("")

  def PolycomUser: String =
    configuration.getOptional[String]("Polycom.user").getOrElse("")
  def PolycomPassword: String =
    configuration.getOptional[String]("Polycom.password").getOrElse("")

  def statsMetricsRegistryName: String =
    configuration.getOptional[String]("metrics.name").getOrElse("Stats")
  def metricsRegistryName: String =
    configuration
      .getOptional[String]("techMetrics.name")
      .getOrElse("techMetrics")
  def metricsRegistryJVM: Boolean =
    configuration.getOptional[Boolean]("techMetrics.jvm").getOrElse(true)
  def metricsLogReporter: Boolean =
    configuration
      .getOptional[Boolean]("techMetrics.logReporter")
      .getOrElse(true)
  def metricsJmxReporter: Boolean =
    configuration
      .getOptional[Boolean]("techMetrics.jmxReporter")
      .getOrElse(true)
  def metricsLogReporterPeriod: Long =
    configuration
      .getOptional[Long]("techMetrics.logReporterPeriod")
      .getOrElse(5)

  def xucPeers: List[String] =
    configuration
      .getOptional[Seq[String]]("xuc.peers")
      .getOrElse(List[String]())
      .toList

  def recordingHost: String =
    configuration.getOptional[String]("recording.host").getOrElse("localhost")
  def recordingPort: Int =
    configuration.getOptional[Int]("recording.port").getOrElse(9000)
  def recordingToken: String =
    configuration.getOptional[String]("recording.token").getOrElse("")

  def chatEnable: Boolean =
    configuration.getOptional[Boolean]("chatServer.enable").getOrElse(false)
  def chatHost: String =
    configuration.getOptional[String]("chatServer.host").getOrElse("mattermost")
  def chatPort: String =
    configuration.getOptional[String]("chatServer.port").getOrElse("80")
  def chatAdminToken: String =
    configuration.getOptional[String]("chatServer.adminToken").getOrElse("")
  def secret: Option[String] =
    configuration.getOptional[String]("chatServer.secret")

  def enableAmiTransfer: Boolean =
    configuration.getOptional[Boolean]("enableAmiTransfer").getOrElse(true)
  def amiTransferTimeoutSeconds: Int =
    configuration.getOptional[Int]("amiTransferTimeoutSeconds").getOrElse(180)
  def conferenceRefreshTimerSeconds: Int =
    configuration
      .getOptional[Int]("conferenceRefreshTimerSeconds")
      .getOrElse(300)

  def stopRecordingUponExternalXfer: Boolean =
    configuration
      .getOptional[Boolean]("stopRecordingUponExternalXfer")
      .getOrElse(true)
  def enableRecordingRules: Boolean =
    configuration.getOptional[Boolean]("enableRecordingRules").getOrElse(true)

  def defaultWebServiceExpires: Int = configuration
    .getOptional[Int]("authentication.webservice.defaultExpires")
    .getOrElse(86400)

  def wsMaxExpires: Int = configuration
    .getOptional[Int]("authentication.webservice.maxExpires")
    .getOrElse(86400)

  def ctiExpires: Int = {
    val threeWeeks = 86400 * 21
    configuration
      .getOptional[Int]("authentication.expires") match {
      case Some(expiration) if expiration < threeWeeks && expiration > 30 =>
        expiration
      case Some(_) => threeWeeks
      case _       => 3600
    }
  }

  val defaultWSAuthScheme: WSAuthScheme = WSAuthScheme.BASIC
  val defaultProtocol                   = "https"
  val defaultWSTimeout: FiniteDuration  = 3.seconds
  object XivoAuth {
    def getConfig(backend: String, defaultExpires: Int): XivoAuthConfig =
      XivoAuthConfig(backend, defaultExpires)

  }

  case class XivoAuthConfig(
      backend: String,
      defaultExpires: Int,
      getTokenURI: String = "0.1/token",
      port: Int =
        configuration.getOptional[Int]("xivo.authPort").getOrElse(9497)
  )

  object XivoDir {
    def defaultProfile: String =
      configuration
        .getOptional[String]("xivo.dirdDefaultProfile")
        .getOrElse("default")
    val version              = 0.1
    val personalURI: String  = s"$version/personal"
    val pathDir: String      = s"$version/directories"
    val searchURI: String    = s"$pathDir/lookup"
    val favoriteURI: String  = s"$pathDir/favorites"
    val directoryURI: String = s"$pathDir/personal"
    val searchArg            = "?term="
    def port: Int =
      configuration.getOptional[Int]("xivo.dirdPort").getOrElse(9489)
  }

  object Authentication {
    lazy val secret: String = configuration
      .getOptional[String]("authentication.secret")
      .getOrElse(Random.alphanumeric.take(32).mkString)

    def expires: Long =
      configuration
        .getOptional[Long]("authentication.expires")
        .getOrElse(3600)

    def preventXucUserLogin: Boolean =
      configuration
        .getOptional[Boolean]("authentication.preventXucUserLogin")
        .getOrElse(true)
  }

  def enableChannelTrackerThrottling: Boolean =
    configuration
      .getOptional[Boolean]("enableChannelTrackerThrottling")
      .getOrElse(true)
  def channelTrackerThrottleTimeoutMs: Int =
    configuration
      .getOptional[Int]("channelTrackerThrottleTimeoutMs")
      .getOrElse(50)
  def retryGetCurrentChannelsDelay: Int = 5

  def casServerUrl: Option[String] =
    configuration.getOptional[String]("casServerUrl")

  def oidcEnable: Boolean =
    configuration.getOptional[Boolean]("oidc.enable").getOrElse(false)
  def oidcServerUrl: Option[String] =
    configuration.getOptional[String]("oidc.trustedServerUrl")
  def oidcClientId: Option[String] =
    configuration
      .getOptional[String]("oidc.clientId")

  def oidcAdditionalTrustedClientIDs: List[String] =
    configuration
      .getOptional[String]("oidc.additionalTrustedClientIDs")
      .map(_.split(",").map(_.trim).toList)
      .getOrElse(List.empty)
  def oidcAudience: List[String] =
    configuration
      .getOptional[String]("oidc.audience")
      .map(_.split(",").map(_.trim).toList)
      .getOrElse(oidcClientId match {
        case Some(clientID) => List(clientID)
        case None           => List.empty
      })
  def oidcUsernameField: Option[String] =
    configuration.getOptional[String]("oidc.usernameField")
  def oidcAdditionalTrustedServersUrl: List[String] = {
    configuration
      .getOptional[String]("oidc.additionalTrustedServersUrl")
      .map(_.split(",").map(_.trim).toList)
      .getOrElse(List.empty)
  }

  def kerberosPrincipal: Option[String] =
    configuration.getOptional[String]("secured.krb5.principal")
  def kerberosKeytab: Option[String] =
    configuration.getOptional[String]("secured.krb5.keyTab")
  def kerberosPassword: Option[String] =
    configuration.getOptional[String]("secured.krb5.password")
  def kerberosDebug: Boolean =
    configuration.getOptional[Boolean]("secured.krb5.debug").getOrElse(false)

  def enableChanSpyBeep: Boolean =
    configuration.getOptional[Boolean]("enableChanSpyBeep").getOrElse(false)

  def stunEnable: Boolean =
    configuration.getOptional[Boolean]("ice.stun.enable").getOrElse(true)

  def turnEnable: Boolean =
    configuration.getOptional[Boolean]("ice.turn.enable").getOrElse(true)

  def turnSecret: String =
    configuration.getOptional[String]("ice.turn.secret").getOrElse("")

  override def turnSecretTtl: Long =
    configuration.getOptional[Long]("ice.turn.expiry").getOrElse(3600)

  def sipDriver: SipDriver.SipDriver =
    configuration
      .getOptional[String]("sipDriver")
      .map(_.toUpperCase)
      .flatMap(SipDriver.toEnum)
      .getOrElse(SipDriver.SIP)

  def sipPort: Option[String] =
    configuration.getOptional[String]("sipPort");
}

case class RabbitConfig(
    username: String,
    password: String,
    exchange: Option[String],
    routingKeys: Option[List[String]],
    consumerTag: Option[String],
    queueName: Option[String],
    queueDurability: Option[Boolean],
    queueRestricted: Option[Boolean],
    queueDelete: Option[Boolean],
    rabbitVirtualHost: String,
    rabbitHost: String,
    rabbitPort: Int
)

@ImplementedBy(classOf[RabbitConfiguration])
trait RabbitXivoConfig {
  def rabbitXivoConfig(): RabbitConfig
}

@ImplementedBy(classOf[RabbitConfiguration])
trait RabbitUsageConfig {
  def rabbitUsageConfig(): RabbitConfig
}

@ImplementedBy(classOf[RabbitConfiguration])
trait RabbitConfigGenerator extends RabbitXivoConfig with RabbitUsageConfig {

  def configuration: Configuration

  def rabbitXivoConfig(): RabbitConfig = {
    RabbitConfig(
      configuration.getOptional[String]("RabbitMQ.username").getOrElse(""),
      configuration.getOptional[String]("RabbitMQ.password").getOrElse(""),
      Option(
        configuration.getOptional[String]("RabbitMQ.exchange").getOrElse("")
      ),
      Option(
        configuration
          .getOptional[Seq[String]]("RabbitMQ.routingkeys")
          .getOrElse(List[String]())
          .toList
      ),
      Option(
        configuration.getOptional[String]("RabbitMQ.consumertag").getOrElse("")
      ),
      None,
      None,
      None,
      None,
      configuration.getOptional[String]("RabbitMQ.virtualhost").getOrElse("/"),
      configuration.getOptional[String]("RabbitMQ.xivohost").getOrElse(""),
      configuration.getOptional[Int]("RabbitMQ.xivoport").getOrElse(5672)
    )
  }

  def rabbitUsageConfig(): RabbitConfig = {
    RabbitConfig(
      configuration.getOptional[String]("Usage.username").getOrElse(""),
      configuration.getOptional[String]("Usage.password").getOrElse(""),
      None,
      None,
      None,
      Option(
        configuration.getOptional[String]("Usage.queueName").getOrElse("")
      ),
      Option(
        configuration
          .getOptional[Boolean]("Usage.queueDurability")
          .getOrElse(false)
      ),
      Option(
        configuration
          .getOptional[Boolean]("Usage.queueRestricted")
          .getOrElse(true)
      ),
      Option(
        configuration.getOptional[Boolean]("Usage.queueDelete").getOrElse(true)
      ),
      configuration.getOptional[String]("RabbitMQ.virtualhost").getOrElse("/"),
      configuration.getOptional[String]("RabbitMQ.xivohost").getOrElse(""),
      configuration.getOptional[Int]("RabbitMQ.xivoport").getOrElse(5672)
    )
  }

}

@ImplementedBy(classOf[XucConfig])
trait WebServiceUserConfig {
  def defaultWebServiceExpires: Int
  def wsMaxExpires: Int
}

@ImplementedBy(classOf[XucConfig])
trait CtiUserConfig {
  def ctiExpires: Int
}

@Singleton
class RabbitConfiguration @Inject() (val configuration: Configuration)
    extends RabbitConfigGenerator {}

@Singleton
class XucConfig @Inject() (val configuration: Configuration)
    extends XucBaseConfig {}
