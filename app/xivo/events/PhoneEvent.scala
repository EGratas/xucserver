package xivo.events

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.*
import xivo.events.CallDirection.CallDirection
import xivo.events.PhoneEventType.PhoneEventType
import xivo.xucami.models.{Channel, ChannelDirection}
import xivo.xucami.models.ChannelDirection.ChannelDirection

object PhoneEventType extends Enumeration {
  type PhoneEventType = Value
  val EventReleased, EventDialing, EventRinging, EventEstablished, EventOnHold,
      EventFailure =
    Value

  implicit def enumWrites: Writes[PhoneEventType.PhoneEventType] =
    (value: PhoneEventType.PhoneEventType) => JsString(value.toString)
}

object CallDirection extends Enumeration {
  type CallDirection = Value
  val Incoming, Outgoing, DirectionUnknown = Value

  implicit def enumWrites: Writes[CallDirection.CallDirection] =
    (value: CallDirection.CallDirection) => JsString(value.toString)

  def apply(direction: Option[ChannelDirection]): CallDirection = {
    direction match {
      case Some(ChannelDirection.INCOMING) => Incoming
      case Some(ChannelDirection.OUTGOING) => Outgoing
      case None                            => DirectionUnknown
      case _                               => DirectionUnknown
    }
  }

}

object UserData {
  val QueueNameKey = "XIVO_QUEUENAME"
  type userData = Map[String, String]
  type filter   = userData => userData
  val incPrefix = "USR_"
  val includeKeys: List[String] = List(
    "XIVO_USERID",
    "XIVO_SRCNUM",
    "XIVO_CONTEXT",
    "XIVO_EXTENPATTERN",
    "XIVO_DSTNUM",
    "XIVO_REVERSE_LOOKUP",
    Channel.VarNames.xucCallType,
    "XIVO_DST_FIRSTNAME",
    "XIVO_DST_LASTNAME",
    "SIPCALLID"
  )
  def filterData(variables: userData): userData =
    variables.view
      .filterKeys(key => includeKeys.contains(key))
      .toMap ++ variables.view.filterKeys(_.startsWith(incPrefix)).toMap
}

object PhoneEvent {
  implicit val writes: Writes[PhoneEvent] = (
    (JsPath \ "eventType").write[PhoneEventType] and
      (JsPath \ "DN").write[String] and
      (JsPath \ "otherDN").write[String] and
      (JsPath \ "otherDName").write[String] and
      (JsPath \ "linkedId").write[String] and
      (JsPath \ "uniqueId").write[String] and
      (JsPath \ "queueName").writeNullable[String] and
      (JsPath \ "userData").write[Map[String, String]] and
      (JsPath \ "callDirection").write[CallDirection] and
      (JsPath \ "username").writeNullable[String]
  )(pe =>
    (
      pe.eventType,
      pe.DN,
      pe.otherDN,
      pe.otherDName,
      pe.linkedId,
      pe.uniqueId,
      pe.queueName,
      pe.userData,
      pe.callDirection,
      pe.username
    )
  )

}

case class PhoneEvent(
    eventType: PhoneEventType,
    DN: String,
    otherDN: String,
    otherDName: String,
    linkedId: String,
    uniqueId: String,
    queueName: Option[String] = None,
    userData: Map[String, String] = Map(),
    callDirection: CallDirection = CallDirection.DirectionUnknown,
    username: Option[String] = None
)
