package xivo.events
package cti.models
import anorm.SqlParser.get
import anorm.{~, RowParser}
import play.api.libs.json.{Format, JsPath, Json, Reads, Writes}
import play.api.libs.functional.syntax._

/**  Object that replace the UserStatus object which is now Deprecated
  */
trait CtiStatusTraits {
  type CtiStatusMap = Map[String, List[CtiStatus]]
  type CtiStatusLegacyMap =
    Map[String, List[CtiStatusLegacy]] // Deprecated It will be removed

}

case class CtiStatusMapWrapper(map: Map[String, List[CtiStatus]])
object CtiStatusMapWrapper {
  implicit val format: Format[CtiStatusMapWrapper] =
    (JsPath \ "map")
      .format[Map[String, List[CtiStatus]]]
      .inmap(
        map => CtiStatusMapWrapper(map),
        ctiWrapper => ctiWrapper.map
      )
}

// Deprecated It will be removed
case class CtiStatusLegacyMapWrapper(map: Map[String, List[CtiStatusLegacy]])
object CtiStatusLegacyMapWrapper {
  implicit val format: Format[CtiStatusLegacyMapWrapper] =
    (JsPath \ "map")
      .format[Map[String, List[CtiStatusLegacy]]]
      .inmap(
        map => CtiStatusLegacyMapWrapper(map),
        ctiLegacyWrapper => ctiLegacyWrapper.map
      )
}

case class CtiStatus(
    name: Option[String],
    displayName: Option[String],
    status: Option[Int]
)

object CtiStatus {
  implicit val format: Format[CtiStatus] = (
    (JsPath \ "name").formatNullable[String] and
      (JsPath \ "displayName").formatNullable[String] and
      (JsPath \ "status").formatNullable[Int]
  )(CtiStatus.apply, cs => (cs.name, cs.displayName, cs.status))

  val simple: RowParser[CtiStatus] = {
    get[Option[String]]("name") ~
      get[Option[String]]("displayName") ~
      get[Option[Int]]("status") map { case name ~ displayName ~ status =>
        CtiStatus(name, displayName, status)
      }
  }
}

// Deprecated It will be removed
case class CtiStatusActionLegacy(name: String, parameters: Option[String])

// Deprecated It will be removed
object CtiStatusActionLegacy {
  implicit val format: Format[CtiStatusActionLegacy] = (
    (JsPath \ "name").format[String] and
      (JsPath \ "parameters").formatNullable[String]
  )(CtiStatusActionLegacy.apply, csal => (csal.name, csal.parameters))

  implicit val rowParser: RowParser[CtiStatusActionLegacy] = {
    get[String]("name") ~
      get[Option[String]]("parameters") map { case name ~ parameters =>
        new CtiStatusActionLegacy(name, parameters)
      }
  }

}
// Deprecated It will be removed
case class CtiStatusLegacy(
    name: Option[String],
    longName: Option[String],
    color: Option[String],
    actions: List[CtiStatusActionLegacy]
)
// Deprecated It will be removed
object CtiStatusLegacy {
  implicit val format: Format[CtiStatusLegacy] = (
    (JsPath \ "name").formatNullable[String] and
      (JsPath \ "longName").formatNullable[String] and
      (JsPath \ "color").formatNullable[String] and
      (JsPath \ "actions").format[List[CtiStatusActionLegacy]]
  )(
    CtiStatusLegacy.apply,
    csl => (csl.name, csl.longName, csl.color, csl.actions)
  )

  val simple: RowParser[CtiStatusLegacy] = {
    get[Option[String]]("name") ~
      get[Option[String]]("longName") ~
      get[Option[String]]("color") ~
      get[Option[String]]("actions") map {
        case name ~ longName ~ color ~ actions =>
          CtiStatusLegacy(
            name,
            longName,
            color,
            Json.parse(actions.getOrElse("[]")).as[List[CtiStatusActionLegacy]]
          )
      }
  }
}
