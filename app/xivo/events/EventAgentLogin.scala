package xivo.events

import xivo.models.Agent
import xivo.xucami.userevents.UserEventAgentLogin

object EventAgentLogin {
  def apply(userEventAgentLogin: UserEventAgentLogin) =
    new EventAgentLogin(
      userEventAgentLogin.getAgentId().toLong,
      userEventAgentLogin.getExtension(),
      userEventAgentLogin.getAgentnumber
    )
}

case class EventAgentLogin(
    agentId: Agent.Id,
    phoneNb: String,
    agentNb: Agent.Number
) extends EventAgent
