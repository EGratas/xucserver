package xivo.events

import xivo.models.Agent

case class EventAgentWrapup(agentId: Agent.Id) extends EventAgent
