package xivo.events

import xivo.models.Agent

case class EventAgentUnPause(agentId: Agent.Id) extends EventAgent
