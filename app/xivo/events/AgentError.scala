package xivo.events

import org.xivo.cti.message.IpbxCommandResponse

trait AgentError

case class AgentLoginError(cause: String) extends AgentError

object AgentError {
  def create(agentError: IpbxCommandResponse): AgentLoginError = {
    agentError.getError_string match {
      case "agent_login_invalid_exten" =>
        AgentLoginError("PhoneNumberForAgentUnknownOrUnexistingPhoneNumber")
      case "agent_login_exten_in_use" =>
        AgentLoginError("PhoneNumberAlreadyInUse")
      case _ => AgentLoginError(s"Unknown:${agentError.getError_string}")
    }
  }
}
