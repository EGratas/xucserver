package xivo.events

import xivo.models.Agent

sealed trait AgentLogTransition {
  val agentId: Agent.Id
}
case class AgentLoggingIn(agentId: Agent.Id, phoneNumber: String)
    extends AgentLogTransition
case class AgentLoggingOut(agentId: Agent.Id) extends AgentLogTransition
