package controllers.config

import com.google.inject.Inject
import controllers.helpers.AuthenticatedAction.getApiUser
import controllers.helpers.{AuthenticatedAction, AuthenticatedRequest}
import play.api.Logger
import play.api.http.HttpEntity
import play.api.libs.json.Json
import play.api.mvc._
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.models.{
  MeetingRoom,
  PersonalMeetingRoom,
  StaticMeetingRoom,
  TemporaryMeetingRoom
}
import xivo.xuc.XucBaseConfig

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

class MeetingRooms @Inject() (implicit
    configRequester: ConfigServerRequester,
    repo: ConfigRepository,
    config: XucBaseConfig,
    ec: ExecutionContext,
    parsers: PlayBodyParsers,
    cc: ControllerComponents
) extends AbstractController(cc) {
  val log: Logger = Logger(getClass.getName)

  implicit val contentParser: BodyParser[AnyContent] = parsers.anyContent

  def getTokenByIdStatic(id: String): Action[AnyContent] =
    getTokenById(id, StaticMeetingRoom)
  def getTokenByIdPersonal(id: String): Action[AnyContent] =
    getTokenById(id, PersonalMeetingRoom)
  def getTokenByIdTemporary(id: String): Action[AnyContent] =
    getTokenById(id, TemporaryMeetingRoom)

  def getTokenById(
      room: String,
      roomType: MeetingRoom
  ): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      val user = getApiUser(request.user)
      log.info(
        s"Forward meetingRoom token request for $room to configmgt for ${user.username}"
      )
      configRequester
        .getMeetingRoomToken(room, Some(user.userId), roomType)
        .map(meetingRoomToken =>
          Results
            .Ok(Json.toJson(meetingRoomToken))
            .withHeaders("Content-Type" -> "application/json")
        )
    }

  def getPersonalRoomById(id: Long): Action[AnyContent] = {
    AuthenticatedAction(config.Authentication.secret).async { request =>
      val user = getApiUser(request.user)
      val url  = s"meetingrooms/personal/$id?userId=${user.userId}"
      forwardToConfigMgt(request, url)
    }
  }

  def addPersonalRoom(): Action[AnyContent] = {
    AuthenticatedAction(config.Authentication.secret).async { request =>
      val user = getApiUser(request.user)
      val url  = s"meetingrooms/personal?userId=${user.userId}"
      forwardToConfigMgt(request, url)
    }
  }

  def editPersonalRoom(): Action[AnyContent] = {

    AuthenticatedAction(config.Authentication.secret).async { request =>
      val user = getApiUser(request.user)
      val url  = s"meetingrooms/personal?userId=${user.userId}"
      forwardToConfigMgt(request, url)
    }
  }

  def deletePersonalRoom(id: Long): Action[AnyContent] = {
    AuthenticatedAction(config.Authentication.secret).async { request =>
      val user = getApiUser(request.user)
      val url  = s"meetingrooms/personal/$id?userId=${user.userId}"
      forwardToConfigMgt(request, url)
    }
  }

  def forwardToConfigMgt(
      request: AuthenticatedRequest[AnyContent],
      url: String
  ): Future[Result] = {
    configRequester
      .forward(
        url,
        request.method,
        request.body.asJson,
        "2.0",
        None
      )
      .map { resp =>
        val headers = resp.headers.map { h =>
          (h._1, h._2.head)
        }
        Result(
          ResponseHeader(resp.status, headers),
          HttpEntity.Strict(resp.bodyAsBytes, None)
        )
      }
  }
}
