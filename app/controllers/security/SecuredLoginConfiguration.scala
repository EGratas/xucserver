package controllers.security

import javax.security.auth.login.{AppConfigurationEntry, Configuration}
import xivo.xuc.KerberosSsoConfig

import scala.jdk.CollectionConverters._

class SecuredLoginConfiguration(config: KerberosSsoConfig)
    extends Configuration {

  type AppConfigurationOptions = Map[String, String]

  def getPrincipal(): String = {
    if (config.kerberosPrincipal.isEmpty)
      throw new Exception("Principal cannot be null")
    config.kerberosPrincipal.get
  }

  def getOptions(): Option[AppConfigurationOptions] =
    for {
      keyTab    <- config.kerberosKeytab
      principal <- config.kerberosPrincipal
      password  <- config.kerberosPassword
      debug = config.kerberosDebug
    } yield Map(
      "keyTab"         -> keyTab,
      "principal"      -> getPrincipal(),
      "password"       -> password,
      "useKeyTab"      -> "true",
      "storeKey"       -> "true",
      "doNotPrompt"    -> "true",
      "useTicketCache" -> "false",
      "renewTGT"       -> "false",
      "isInitiator"    -> "false",
      "debug"          -> debug.toString
    )

  lazy val appConfigurationEntry: Array[AppConfigurationEntry] = Array(
    new AppConfigurationEntry(
      "com.sun.security.auth.module.Krb5LoginModule",
      AppConfigurationEntry.LoginModuleControlFlag.REQUIRED,
      getOptions().getOrElse(Map.empty).asJava
    )
  )

  def getAppConfigurationEntry(name: String): Array[AppConfigurationEntry] =
    appConfigurationEntry
}
