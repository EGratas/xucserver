package controllers

import com.google.inject.Inject
import configuration.AuthConfig
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents}
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.xuc.{ConfigServerConfig, XucBaseConfig}
import xivo.xucami.XucBaseAmiConfig
import xivo.xucstats.XucBaseStatsConfig

import scala.concurrent.ExecutionContext
import play.api.mvc

class XucDefault @Inject() (
    authConfig: AuthConfig,
    repo: ConfigRepository,
    xucConfig: XucBaseConfig,
    xucStatsConfig: XucBaseStatsConfig,
    xucAmiConfig: XucBaseAmiConfig,
    configServerConfig: ConfigServerConfig,
    configServer: ConfigServerRequester,
    cc: ControllerComponents
)(implicit ec: ExecutionContext)
    extends AbstractController(cc) {

  def index: mvc.Action[AnyContent] =
    Action.async {
      configServer.getMediaServerConfigAll
        .map(mdsServers =>
          Ok(
            views.html.default.index(
              "Xuc",
              authConfig,
              repo,
              xucConfig,
              xucStatsConfig,
              xucAmiConfig,
              mdsServers,
              configServerConfig
            )
          )
        )
    }
}
