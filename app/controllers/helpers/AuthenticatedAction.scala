package controllers.helpers

import org.apache.pekko.actor.ActorRef
import org.apache.pekko.stream.Materializer
import org.apache.pekko.stream.scaladsl.Flow
import controllers.helpers.AuthenticatedAction.{aclFromRequest, validateACL}
import controllers.helpers.XivoAuthenticationHelper.getXivoAuthToken
import controllers.security.KerberosAuthentication
import models.ws.auth.SoftwareType.SoftwareType
import models.ws.auth._
import models.{Token, WebServiceUser, XivoUser}
import org.joda.time.DateTime
import play.api.libs.typedmap.TypedMap
import play.api.libs.ws.WSClient
import play.api.mvc.WebSocket.MessageFlowTransformer
import play.api.mvc._
import play.api.mvc.request.{RemoteConnection, RequestTarget}
import services.auth.{CasWSImpl, OidcImpl}
import services.config.ConfigRepository
import xivo.network.XiVOWS
import xivo.services.XivoAuthentication
import xivo.xuc.XucBaseConfig

import scala.collection.immutable.HashMap
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.matching.Regex
import scala.util.{Either, Failure, Success, Try}
class AuthenticatedRequest[A](
    val auth: AuthenticationInformation,
    val user: Either[XivoUser, WebServiceUser],
    request: Request[A]
) extends WrappedRequest[A](request)

class AuthenticatedRequestHeader(
    val auth: AuthenticationInformation,
    val user: Either[XivoUser, WebServiceUser],
    request: RequestHeader
) extends RequestHeader {
  override def connection: RemoteConnection = request.connection
  override def method: String               = request.method
  override def target: RequestTarget        = request.target
  override def version: String              = request.version
  override def headers: Headers             = request.headers
  override def attrs: TypedMap              = request.attrs
}

object AuthenticatedRequestParser {

  import helpers.OptionToTry._

  def extractBearer[A](auth: String): Try[String] = {
    auth.split("Bearer ") match {
      case Array(_, bearer) => Success(bearer)
      case _ =>
        Failure(
          new AuthenticationException(
            AuthenticationError.BearerNotFound,
            "Bearer information not found"
          )
        )
    }
  }

  def extractToken(request: RequestHeader): Try[String] = {
    val qsToken = request.queryString
      .get("token")
      .flatMap(_.headOption)
      .filterNot(_.isEmpty())
      .toTry(
        new AuthenticationException(
          AuthenticationError.AuthorizationHeaderNotFound,
          s"Authorization querystring not found for ${request.queryString}"
        )
      )

    qsToken.recoverWith({ case _ =>
      request.headers
        .get("Authorization")
        .filterNot(header => header.isEmpty)
        .toTry(
          new AuthenticationException(
            AuthenticationError.AuthorizationHeaderNotFound,
            s"Authorization header not found for ${request.headers} and query ${request.queryString}"
          )
        )
        .flatMap(extractBearer)
    })
  }

  def apply(request: RequestHeader, secret: String)(implicit
      repo: ConfigRepository
  ): Try[(AuthenticationInformation, Either[XivoUser, WebServiceUser])] = {
    val userType =
      extractBearer(request.headers.get("Authorization").getOrElse(""))
        .flatMap(AuthenticationInformation.decode(_, secret))
        .map(_.userType)
        .getOrElse(AuthenticatedAction.ctiUserType)

    if (userType == AuthenticatedAction.ctiUserType) {
      for {
        token <- extractToken(request)
        auth <-
          AuthenticationInformation.decode(token, secret).flatMap(_.validate)
        xivoUser <-
          repo
            .getCtiUser(auth.login)
            .toTry(
              new AuthenticationException(
                AuthenticationError.UserNotFound,
                "Matching XiVO user not found"
              )
            )
      } yield (auth, Left(xivoUser))

    } else {
      for {
        token <- extractToken(request)
        auth <-
          AuthenticationInformation.decode(token, secret).flatMap(_.validate)
        webServiceUser <-
          repo
            .getWebServiceUser(auth.login)
            .toTry(
              new AuthenticationException(
                AuthenticationError.UserNotFound,
                "Matching web service user not found"
              )
            )
      } yield (auth, Right(webServiceUser))

    }
  }

  def ssoAuthentication(
      request: RequestHeader,
      expires: Long,
      softwareType: Option[SoftwareType],
      config: XucBaseConfig
  )(implicit
      krb: KerberosAuthentication,
      repo: ConfigRepository,
      ec: ExecutionContext,
      xivoAuthentication: ActorRef
  ): Try[(AuthenticationInformation, XivoUser)] = {
    val now = AuthenticatedAction.now()
    for {
      login <-
        krb
          .performKerberosAuthentication(request)
          .toTry(
            new AuthenticationException(
              AuthenticationError.SsoAuthenticationFailed,
              "SSO Authentication Failed"
            )
          )
      xivoUser <-
        repo
          .getCtiUser(login)
          .toTry(
            new AuthenticationException(
              AuthenticationError.UserNotFound,
              s"Matching XiVO user not found for login $login"
            )
          )
      token <- getXivoAuthToken(xivoUser.id, config).toTry(
        new AuthenticationException(
          AuthenticationError.UserNotFound,
          s"XiVO auth token cannot be retrieved for userId ${xivoUser.id}"
        )
      )

    } yield (
      AuthenticationInformation(
        login,
        now + expires,
        now,
        AuthenticatedAction.ctiUserType,
        AuthenticatedAction.getAliasesFromAcls(token.acls),
        softwareType
      ),
      xivoUser
    )
  }
}

class AuthenticatedWebsocket(secret: String)(implicit repo: ConfigRepository) {
  def apply[In, Out](
      f: AuthenticatedRequestHeader => Future[Either[Result, Flow[In, Out, _]]]
  )(implicit
      transformer: MessageFlowTransformer[In, Out],
      mat: Materializer
  ): WebSocket = {
    WebSocket.acceptOrResult[In, Out] { request =>
      AuthenticatedRequestParser(request, secret) match {
        case Success((auth, xivoUser)) =>
          val userType = auth.userType
          if (userType.equals(AuthenticatedAction.ctiUserType))
            f(new AuthenticatedRequestHeader(auth, xivoUser, request))
          else
            Future.successful(
              Left(
                AuthenticationFailure
                  .from(
                    new AuthenticationException(
                      AuthenticationError.WrongRequester,
                      s"User does not have the right type to open CTI websocket (current: $userType, expected: ${AuthenticatedAction.ctiUserType})"
                    )
                  )
                  .toResult
              )
            )
        case Failure(t) =>
          Future.successful(Left(AuthenticationFailure.from(t).toResult))
      }
    }
  }
}

object AuthenticatedWebsocket {
  def apply(secret: String)(implicit repo: ConfigRepository) =
    new AuthenticatedWebsocket(secret)
}

class AuthenticatedAction(secret: String)(implicit
    repo: ConfigRepository,
    ec: ExecutionContext,
    bodyParser: BodyParser[AnyContent]
) extends ActionBuilder[AuthenticatedRequest, AnyContent] {
  def invokeBlock[A](
      request: Request[A],
      block: AuthenticatedRequest[A] => Future[Result]
  ): Future[Result] = {
    AuthenticatedRequestParser(request, secret) match {
      case Success((auth, xivoUser)) =>
        val requiredACL: Option[String] =
          aclFromRequest(request.path, request.method)
        if (validateACL(request.path, requiredACL, auth.acls))
          block(new AuthenticatedRequest(auth, xivoUser, request))
        else
          Future.successful(
            AuthenticationFailure
              .from(
                new AuthenticationException(
                  AuthenticationError.Forbidden,
                  s"Required ACL for this endpoint (${requiredACL
                    .getOrElse("")}) is missing from user ACLs"
                )
              )
              .toResult
          )
      case Failure(t) =>
        Future.successful(AuthenticationFailure.from(t).toResult)
    }
  }
  override def parser: BodyParser[AnyContent] = bodyParser

  override protected def executionContext: ExecutionContext = ec
}

object AuthenticatedAction {
  val ctiUserType: String        = AuthUserType.Cti.toString
  val webServiceUserType: String = AuthUserType.Webservice.toString
  val apiPath2                   = "/xuc/api/2.0"
  val apiPath1                   = "/xuc/api/1.0"
  val aclAliasRepo: Map[String, List[String]] = HashMap(
    "alias.ctiuser" -> List(
      "xuc.rest.contact.personal.read",
      "xuc.rest.contact.personal.create",
      "xuc.rest.contact.personal.delete",
      "xuc.rest.contact.personal.*.read",
      "xuc.rest.contact.personal.*.update",
      "xuc.rest.contact.personal.*.delete",
      "xuc.rest.contact.export.personal.read",
      "xuc.rest.contact.import.personal.create",
      "xuc.rest.contact.display.personal.read",
      "xuc.rest.call_qualification.queue.*.read",
      "xuc.rest.call_qualification.csv.#.read",
      "xuc.rest.call_qualification.create",
      "xuc.rest.mobile.push.register.create",
      "xuc.rest.config.meetingrooms.static.token.*.read",
      "xuc.rest.config.meetingrooms.personal.token.*.read",
      "xuc.rest.config.meetingrooms.temporary.token.*.read",
      "xuc.rest.config.meetingrooms.personal.*.read",
      "xuc.rest.config.meetingrooms.personal.create",
      "xuc.rest.config.meetingrooms.personal.update",
      "xuc.rest.config.meetingrooms.personal.*.delete",
      "xuc.rest.config.#.read",
      "xuc.rest.config.#.create",
      "xuc.rest.config.#.update"
    )
  )
  def now(): Long = new DateTime().getMillis / 1000
  def apply(secret: String)(implicit
      repo: ConfigRepository,
      ec: ExecutionContext,
      bodyParser: BodyParser[AnyContent]
  ) = new AuthenticatedAction(secret)

  def getAliasesFromAcls(acls: List[String]): List[String] = {
    acls.filter(_.startsWith("alias."))
  }

  def validateACL(
      path: String,
      requiredACL: Option[String],
      authACLs: List[String]
  ): Boolean = {
    val mergedAclsWithAlias = authACLs
      .flatMap(a => aclAliasRepo.getOrElse(a, List.empty))
      .concat(authACLs)
    if (path.startsWith(s"$apiPath2/auth/") || path.startsWith(s"$apiPath1/")) {
      true
    } else {
      requiredACL.exists(acl => mergedAclsWithAlias.contains(acl))
    }
  }

  def aclFromRequest(path: String, method: String): Option[String] = {
    val regex = new Regex(
      "((?:dial\\.user\\.)|" +
        "(?:dial\\.fromqueue\\.)|" +
        "(?:dial\\.number\\.)|" +
        "(?:dnd\\.)|" +
        "(?:forward\\.unconditional\\.)|" +
        "(?:forward\\.noanswer\\.)|" +
        "(?:forward\\.busy\\.)|" +
        "(?:history\\.)|" +
        "(?:contact\\.personal\\.)|" +
        "(?:call\\_qualification\\.queue\\.)|" +
        "(?:config\\.meetingrooms\\.(?:(?:[a-zA-Z]+\\.token\\.)|" +
        "(?:personal\\.)))).+$"
    )
    val aclPath =
      path.replaceAll(apiPath2, "xuc.rest").replaceAll("/", ".") match {
        case p if p.contains("call_qualification.csv") =>
          "xuc.rest.call_qualification.csv.#"
        case p
            if p.contains("xuc.rest.config") && !p.startsWith(
              "xuc.rest.config.meetingrooms"
            ) =>
          "xuc.rest.config.#"
        case p if p.contains("xuc.rest.history.days.") =>
          "xuc.rest.history.days.*"
        case p => regex.replaceAllIn(p, m => s"${m.group(1)}\\*").toLowerCase()
      }
    method match {
      case "GET"    => Some(s"$aclPath.read")
      case "POST"   => Some(s"$aclPath.create")
      case "PUT"    => Some(s"$aclPath.update")
      case "DELETE" => Some(s"$aclPath.delete")
      case _        => None
    }
  }

  def forbiddenAction(implicit ec: ExecutionContext): Result =
    AuthenticationFailure
      .from(
        new AuthenticationException(
          AuthenticationError.Forbidden,
          "You do not have sufficient rights to execute this action"
        )
      )
      .toResult

  def getApiUser(user: Either[XivoUser, WebServiceUser]): ApiUser = {
    user match {
      case Left(u)  => ApiUser(u.username, u.id)
      case Right(u) => ApiUser(Option(u.name))
    }
  }
}

class SsoAuthenticatedAction(
    expires: Long
)(implicit
    krb: KerberosAuthentication,
    repo: ConfigRepository,
    ec: ExecutionContext,
    bodyParser: BodyParser[AnyContent],
    config: XucBaseConfig,
    xivoAuthentication: ActorRef
) extends ActionBuilder[AuthenticatedRequest, AnyContent] {
  def invokeBlock[A](
      request: Request[A],
      block: AuthenticatedRequest[A] => Future[Result]
  ): Future[Result] = {
    val softwareType = request.getQueryString("softwareType")
    AuthenticatedRequestParser.ssoAuthentication(
      request,
      expires,
      SoftwareType.decode(softwareType),
      config
    ) match {
      case Success((auth, xivoUser)) =>
        block(new AuthenticatedRequest(auth, Left(xivoUser), request))
      case Failure(t) =>
        Future.successful(AuthenticationFailure.from(t).toResult)
    }
  }

  override def parser: BodyParser[AnyContent] = bodyParser

  override protected def executionContext: ExecutionContext = ec
}

object SsoAuthenticatedAction {
  def apply(expires: Long)(implicit
      krb: KerberosAuthentication,
      repo: ConfigRepository,
      ec: ExecutionContext,
      bodyParser: BodyParser[AnyContent],
      config: XucBaseConfig,
      xivoAuthentication: ActorRef
  ) = new SsoAuthenticatedAction(
    expires
  )
}

class CasAuthenticatedAction(xiVOWS: XiVOWS, config: XucBaseConfig)(implicit
    repo: ConfigRepository,
    ec: ExecutionContext,
    bodyParser: BodyParser[AnyContent],
    xivoAuthentication: ActorRef
) extends ActionBuilder[AuthenticatedRequest, AnyContent] {
  def invokeBlock[A](
      request: Request[A],
      block: AuthenticatedRequest[A] => Future[Result]
  ): Future[Result] = {
    val r = for {
      service <- request.getQueryString("service")
      ticket  <- request.getQueryString("ticket")
    } yield {
      CasWSImpl
        .validate(service, ticket, xiVOWS, config)
        .flatMap(resp => {
          repo.getCtiUser(resp.user.username) match {
            case Some(xivoUser) =>
              val softwareType = request.getQueryString("softwareType")
              val now          = AuthenticatedAction.now()
              getXivoAuthToken(xivoUser.id, config) match {
                case Some(token) =>
                  block(
                    new AuthenticatedRequest(
                      AuthenticationInformation(
                        resp.user.username,
                        now + config.Authentication.expires,
                        now,
                        AuthenticatedAction.ctiUserType,
                        AuthenticatedAction.getAliasesFromAcls(token.acls),
                        SoftwareType.decode(softwareType)
                      ),
                      Left(xivoUser),
                      request
                    )
                  )
                case None =>
                  val error = new AuthenticationException(
                    AuthenticationError.UserNotFound,
                    s"XiVO auth token cannot be retrieved for userId ${xivoUser.id}"
                  )
                  Future.successful(AuthenticationFailure.from(error).toResult)
              }
            case None =>
              val error = new AuthenticationException(
                AuthenticationError.UserNotFound,
                "Matching XiVO user not found"
              )
              Future.successful(AuthenticationFailure.from(error).toResult)
          }
        })(ec)
        .recoverWith({ case t =>
          Future.successful(AuthenticationFailure.from(t).toResult)
        })
    }
    r.getOrElse {
      val error = new AuthenticationException(
        AuthenticationError.CasServerInvalidParameter,
        "Required parameters are not set"
      )
      Future.successful(AuthenticationFailure.from(error).toResult)
    }
  }

  override def parser: BodyParser[AnyContent] = bodyParser

  override protected def executionContext: ExecutionContext = ec
}

object CasAuthenticatedAction {
  def apply(xiVOWS: XiVOWS, config: XucBaseConfig)(implicit
      repo: ConfigRepository,
      ec: ExecutionContext,
      bodyParser: BodyParser[AnyContent],
      xivoAuthentication: ActorRef
  ) = new CasAuthenticatedAction(xiVOWS, config)
}

class OidcAuthenticatedAction(ws: WSClient, config: XucBaseConfig)(implicit
    repo: ConfigRepository,
    ec: ExecutionContext,
    bodyParser: BodyParser[AnyContent],
    xivoAuthentication: ActorRef
) extends ActionBuilder[AuthenticatedRequest, AnyContent] {
  def invokeBlock[A](
      request: Request[A],
      block: AuthenticatedRequest[A] => Future[Result]
  ): Future[Result] = {

    val softwareType = request.getQueryString("softwareType")

    AuthenticatedRequestParser
      .extractToken(request)
      .flatMap(token =>
        OidcImpl.authenticate(
          Some(token),
          config,
          repo,
          ws,
          SoftwareType.decode(softwareType)
        )
      ) match {
      case Success((auth, xivoUser)) =>
        block(new AuthenticatedRequest(auth, Left(xivoUser), request))
      case Failure(t) =>
        Future.successful(AuthenticationFailure.from(t).toResult)
    }
  }

  override def parser: BodyParser[AnyContent]               = bodyParser
  override protected def executionContext: ExecutionContext = ec
}

object OidcAuthenticatedAction {
  def apply(ws: WSClient, config: XucBaseConfig)(implicit
      repo: ConfigRepository,
      ec: ExecutionContext,
      bodyParser: BodyParser[AnyContent],
      xivoAuthentication: ActorRef
  ) = new OidcAuthenticatedAction(ws, config)
}

object XivoAuthenticationHelper {
  def getXivoAuthToken(
      xivoUserId: Long,
      config: XucBaseConfig
  )(implicit
      ec: ExecutionContext,
      xivoAuthentication: ActorRef
  ): Option[Token] = {
    val result = XivoAuthentication
      .getCtiTokenHelper(
        xivoAuthentication,
        xivoUserId
      )
    Try(Await.result(result, config.defaultWSTimeout)).toOption
  }
}
