package controllers.helpers

import play.api.Logger
import play.api.mvc.Result
import play.api.mvc.Results._
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

sealed trait RequestResult {
  val reason: String
}

case class RequestTimeout(reason: String) extends RequestResult

case class RequestSuccess(reason: String) extends RequestResult

case class RequestError(reason: String) extends RequestResult

case class RequestUnauthorized(reason: String) extends RequestResult

case class RequestBlocked(reason: String) extends RequestResult

trait RequestResultHelper {
  val log: Logger = Logger(getClass.getName)

  def processResult(
      result: Future[RequestResult],
      username: String = ""
  ): Future[Result] = {
    result.map {
      case r: RequestSuccess =>
        Ok(r.reason)
      case r: RequestUnauthorized =>
        log.info(s"Unauthorized $username")
        Unauthorized(r.reason)
      case r: RequestBlocked =>
        log.info(s"Request blocked (${r.reason})")
        ServiceUnavailable(s"The API endpoint is disabled (${r.reason})")
      case t: RequestTimeout =>
        log.error(s"Process Result timeout $username")
        BadRequest(t.reason)
      case e: RequestError =>
        log.info(s"Process Result Bad Request(${e.reason}) $username")
        BadRequest(e.reason)
    }
  }
}
