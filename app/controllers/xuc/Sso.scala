package controllers.xuc

import org.apache.pekko.actor.{ActorRef, ActorSystem, Props}
import org.apache.pekko.pattern.{ask, AskTimeoutException}
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.{
  RequestBlocked,
  RequestError,
  RequestResult,
  RequestSuccess,
  RequestUnauthorized,
  RequestTimeout => XucRequestTimeout
}
import controllers.security.KerberosAuthentication
import models.ws.sso.AuthenticationResult
import play.api.libs.json.Json
import play.api.mvc.{
  AbstractController,
  AnyContent,
  ControllerComponents,
  InjectedController
}
import play.mvc.Http
import services.ActorIds
import services.config.ConfigRepository
import xivo.services.TokenRetriever

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import play.api.mvc

class Sso @Inject() (
    kerberos: KerberosAuthentication,
    system: ActorSystem,
    @Named(ActorIds.XivoAuthenticationId) xivoAuthentication: ActorRef,
    configRepo: ConfigRepository,
    cc: ControllerComponents
) extends AbstractController(cc) {
  val tokenRetriever: ActorRef =
    system.actorOf(Props(new TokenRetriever(xivoAuthentication, configRepo)))
  implicit val timeout: org.apache.pekko.util.Timeout = 2.seconds

  def createToken(orig: String): mvc.Action[AnyContent] =
    Action.async { implicit request =>
      kerberos.performKerberosAuthentication(request) match {
        case Some(login) =>
          generateToken(login)
            .map({
              case RequestSuccess(token)       => Redirect(s"$orig?token=$token")
              case RequestError(reason)        => InternalServerError(reason)
              case RequestUnauthorized(reason) => Unauthorized(reason)
              case XucRequestTimeout(reason)   => InternalServerError(reason)
              case RequestBlocked(reason)      => InternalServerError(reason)
            })
            .recover({ case e: AskTimeoutException =>
              InternalServerError("Timeout while getting the token")
            })
        case None =>
          Future.successful(
            Unauthorized("You are unauthorized").withHeaders(
              Http.HeaderNames.WWW_AUTHENTICATE -> "Negotiate"
            )
          )
      }
    }

  def login: mvc.Action[AnyContent] =
    Action { implicit request =>
      val resp = kerberos.performKerberosAuthentication(request) match {
        case Some(login) =>
          configRepo.getCtiUser(login) match {
            case Some(user) =>
              Ok(Json.toJson(AuthenticationResult(true, Some(login))))
            case None =>
              Unauthorized(Json.toJson(AuthenticationResult(false, None)))
                .withHeaders(Http.HeaderNames.WWW_AUTHENTICATE -> "Negotiate")
          }

        case None =>
          Unauthorized(Json.toJson(AuthenticationResult(false, None)))
            .withHeaders(Http.HeaderNames.WWW_AUTHENTICATE -> "Negotiate")
      }
      resp
        .withHeaders(
          "Access-Control-Allow-Origin" -> request.headers
            .get(ORIGIN)
            .getOrElse("*")
        )
        .withHeaders(ACCESS_CONTROL_ALLOW_CREDENTIALS -> "true")
    }

  private def generateToken(login: String): Future[RequestResult] =
    (tokenRetriever ? TokenRetriever.TokenByLogin(login)).mapTo[RequestResult]
}
