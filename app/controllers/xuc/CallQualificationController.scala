package controllers.xuc

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.AuthenticatedAction.getApiUser
import controllers.helpers.{AuthenticatedAction, RequestResult}
import models.ws.{GenericError, NotHandledError}
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import services.ActorIds
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.models._
import xivo.network.WebServiceException
import xivo.xuc.XucBaseConfig
import xivo.xuc.api.Requester

import scala.concurrent.{ExecutionContext, Future}

class CallQualificationController @Inject() (implicit
    system: ActorSystem,
    configRepository: ConfigRepository,
    configRequester: ConfigServerRequester,
    @Named(ActorIds.CallbackMgrInterfaceId) callbackManager: ActorRef,
    @Named(ActorIds.ConfigServiceManagerId) configServiceManager: ActorRef,
    config: XucBaseConfig,
    parsers: PlayBodyParsers,
    ec: ExecutionContext,
    cc: ControllerComponents
) extends AbstractController(cc) {
  val logger: Logger                                 = Logger(getClass.getName)
  val serviceName                                    = "CallQualification"
  implicit val contentParser: BodyParser[AnyContent] = parsers.anyContent

  def logRequest(username: Option[String], requestAction: String): Unit = {
    logger.info(s"[$username] Req: <$serviceName - $requestAction>")
  }

  def genericError(e: Throwable, requestAction: String): Result = {
    GenericError(
      NotHandledError,
      s"Error $requestAction call qualification: ${e.getMessage}"
    ).toResult
  }

  def callQualificationError(e: CallQualificationException): Result = {
    CallQualificationError(e.errorType, e.message).toResult
  }

  def handleException(
      requestAction: String
  ): PartialFunction[Throwable, Result] = {
    case ce: CallQualificationException => callQualificationError(ce)
    case we: WebServiceException        => genericError(we, requestAction)
    case e                              => genericError(e, requestAction)
  }

  def get(queueId: Long): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      val requestAction = "get"
      val user          = getApiUser(request.user)

      def result(resp: List[CallQualification]): Result = {
        Results.Ok(Json.toJson(resp))
      }

      logRequest(user.username, requestAction)
      configRequester
        .getCallQualifications(queueId)
        .map(response => result(response))
        .recover(handleException(requestAction))
    }

  def exportQualificationAnswers(
      queueId: Long,
      from: String,
      to: String
  ): Action[AnyContent] =
    Action.async {

      def result(resp: RequestResult): Result = {
        Results
          .Ok(resp.reason)
          .withHeaders(
            "Access-Control-Allow-Origin" -> "*",
            "Content-Type"                -> "text/csv",
            "Content-Disposition"         -> s"attachment;filename=$queueId-$from-$to.csv"
          )
      }

      val requestAction = "exportQualificationAnswers"
      logger.info(s"Req: <$serviceName - $requestAction>")

      Requester
        .exportQualificationsCsv(configServiceManager, queueId, from, to)
        .map(response => result(response))
        .recover(handleException(requestAction))
    }

  def create(): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      val requestAction = "create"
      val user          = getApiUser(request.user)

      def result(resp: Long): Result = {
        Results.Created(Json.toJson(resp))
      }

      logRequest(user.username, requestAction)
      request.body.asJson match {
        case Some(json) =>
          json.validate[CallQualificationAnswer] match {
            case JsSuccess(callQualificationAnswer, _) =>
              configRequester
                .createCallQualificationAnswer(callQualificationAnswer)
                .map(response => result(response))
                .recover(handleException(requestAction))
            case JsError(e) =>
              Future
                .failed(
                  new CallQualificationException(
                    CallQualificationJsonErrorDecoding,
                    "Error decoding JSON."
                  )
                )
                .recover(handleException(requestAction))
          }
        case None =>
          Future
            .failed(
              new CallQualificationException(
                CallQualificationJsonBodyNotFound,
                "No JSON could be decoded."
              )
            )
            .recover(handleException(requestAction))
      }
    }
}
