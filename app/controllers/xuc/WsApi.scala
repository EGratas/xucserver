package controllers.xuc

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.util.Timeout.durationToTimeout
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.api.IPFilter
import controllers.helpers.{
  AuthenticatedAction,
  RequestResult,
  RequestResultHelper
}
import play.api.Logger
import play.api.libs.functional.syntax.*
import play.api.libs.json.Reads.*
import play.api.libs.json.Writes.*
import play.api.libs.json.*
import play.api.libs.ws.WSClient
import play.api.mvc.*
import services.config.ConfigRepository
import services.request.{DialFromQueue, HistoryDays, HistorySize}
import services.{ActorIds, XucEventBus}
import xivo.xuc.XucBaseConfig
import xivo.xuc.api.{CtiApi, Requester}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}

class WsApi @Inject() (
    config: XucBaseConfig,
    ws: WSClient,
    ctiApi: CtiApi,
    @Named(ActorIds.ConfigManagerId) configManager: ActorRef,
    @Named(ActorIds.CallbackMgrInterfaceId) callbackManager: ActorRef,
    @Named(ActorIds.CtiRouterFactoryId) ctiRouterFactory: ActorRef,
    @Named(ActorIds.CallHistoryManagerId) callHistoryManager: ActorRef,
    eventBus: XucEventBus,
    parsers: PlayBodyParsers,
    iPFilter: IPFilter,
    cc: ControllerComponents
)(implicit
    system: ActorSystem,
    ec: ExecutionContext,
    configRepo: ConfigRepository
) extends AbstractController(cc)
    with RequestResultHelper {

  override val log: Logger                            = Logger(getClass.getName)
  implicit val timeout: org.apache.pekko.util.Timeout = 1.seconds
  implicit val contentParser: BodyParser[AnyContent]  = parsers.anyContent

  def handShake(domain: String): Action[AnyContent] =
    iPFilter { implicit request =>
      log.info(s"handShake domain $domain ")
      Requester.handShake(configManager)
      Ok("")
    }

  private val lgfs = (__ \ Symbol("password")).format[String]

  def connect(domain: String, username: String): Action[AnyContent] =
    iPFilter.async { implicit request =>
      log.info(s"connect $username domain $domain " + request.body.asJson)
      request.body.asJson
        .map { json =>
          json
            .validate[String](lgfs)
            .map { (_: String) =>
              log.debug(s"Connecting user : username")
              val result =
                Requester.connect(configRepo, ctiRouterFactory, username)
              processResult(result, username)
            }
            .recoverTotal { e =>
              log.error(s"Error connect " + JsError.toJson(e))
              Future(BadRequest(s"Error connect " + JsError.toJson(e)))
            }
        }
        .getOrElse {
          log.error(s"Error Error connect Expecting Json data")
          Future(BadRequest(s"Error Error connect Expecting Json data"))
        }

    }

  def isForMe(username: String): Boolean =
    configRepo.getCtiUser(username).isDefined

  case class UserRequest[A](username: String, request: Request[A])
      extends WrappedRequest(request)

  case class UserFromRequest(domain: String, username: String, action: String)(
      implicit ec: ExecutionContext
  ) extends ActionBuilder[UserRequest, AnyContent] {
    def executionContext: ExecutionContext      = ec
    override def parser: BodyParser[AnyContent] = parsers.anyContent

    def invokeBlock[A](
        request: Request[A],
        block: UserRequest[A] => Future[Result]
    ): Future[Result] = {
      if (isForMe(username)) {
        block(UserRequest(username, request))
      } else {
        ForwardRequester.forwardRequest(
          domain,
          username,
          action,
          config.xucPeers
        )(request, ws)
      }
    }

  }

  private val rds = (__ \ Symbol("number")).format[String]

  def dial(domain: String, username: String): Action[AnyContent] =
    UserFromRequest(domain, username, "dial").async { implicit request =>
      log.info(
        s"dial $username domain $domain ${request.headers.get("Peer")}" + request.body.asJson
      )
      request.body.asJson
        .map { json =>
          json
            .validate[String](rds)
            .map(number =>
              processResult(ctiApi.dial(username, number), username)
            )
            .recoverTotal { e =>
              log.error(s"Error $username dial " + JsError.toJson(e))
              Future(BadRequest(s"Error $username dial " + JsError.toJson(e)))
            }
        }
        .getOrElse {
          log.error(s"Error $username dial Expecting Json data")
          Future(BadRequest(s"Error $username dial Expecting Json data"))
        }
    }

  private val usernameReads = (__ \ Symbol("username")).format[String]

  def dialByUsername(domain: String, username: String): Action[AnyContent] =
    UserFromRequest(domain, username, "dialByUsername").async {
      implicit request =>
        log.info(
          s"dial $username domain $domain ${request.headers.get("Peer")}" + request.body.asJson
        )
        request.body.asJson
          .map { json =>
            json
              .validate[String](usernameReads)
              .map(user2 =>
                configRepo.phoneNumberForUser(user2) match {
                  case Some(number) =>
                    processResult(ctiApi.dial(username, number), username)
                  case None =>
                    Future(BadRequest(s"Phone number for $user2 not found"))
                }
              )
              .recoverTotal { e =>
                log.error(s"Error $username dial " + JsError.toJson(e))
                Future(BadRequest(s"Error $username dial " + JsError.toJson(e)))
              }
          }
          .getOrElse {
            log.error(s"Error $username dial Expecting Json data")
            Future(BadRequest(s"Error $username dial Expecting Json data"))
          }
    }

  def dialFromQueue(domain: String, username: String): Action[AnyContent] =
    UserFromRequest(domain, username, "dialFromQueue").async {
      implicit request =>
        log.info(s"dial from queue $username domain $domain ${request.headers
          .get("Peer")}" + request.body.asJson)
        request.body.asJson
          .map { json =>
            json
              .validate[DialFromQueue]
              .map { dialFromQueue =>
                processResult(ctiApi.dialFromQueue(username, dialFromQueue))
              }
              .recoverTotal { e =>
                log.error(
                  s"Error $username dial from queue " + JsError.toJson(e)
                )
                Future.successful(
                  BadRequest(
                    s"Error $username dial from queue " + JsError.toJson(e)
                  )
                )
              }
          }
          .getOrElse {
            log.error(s"Error $username dial from queue Expecting Json data")
            Future.successful(
              BadRequest(s"Error $username dial from queue Expecting Json data")
            )
          }
    }

  def dnd(domain: String, username: String): Action[AnyContent] =
    UserFromRequest(domain, username, "dnd").async { implicit request =>
      log.info(s"Dnd $username domain $domain " + request.body.asJson)
      request.body.asJson
        .map { json =>
          val dndState: JsResult[Boolean] = (json \ "state").validate[Boolean]
          dndState
            .map { case state =>
              val result = ctiApi.dnd(username, state)
              processResult(result, username)
            }
            .recoverTotal { e =>
              log.error(s"Error $username dnd " + JsError.toJson(e))
              Future(BadRequest(s"Error $username dnd " + JsError.toJson(e)))
            }
        }
        .getOrElse {
          log.error(s"Error $username dnd Expecting Json data")
          Future(BadRequest(s"Error $username dnd Expecting Json data"))
        }
    }

  val fwdf: OFormat[(Boolean, String)] = ((__ \ Symbol("state"))
    .format[Boolean] and (__ \ Symbol("destination")).format[String]).tupled

  def uncForward(domain: String, username: String): Action[AnyContent] =
    forward(domain, username, "uncForward", ctiApi.uncForward)

  def naForward(domain: String, username: String): Action[AnyContent] =
    forward(domain, username, "naForward", ctiApi.naForward)

  def busyForward(domain: String, username: String): Action[AnyContent] =
    forward(domain, username, "busyForward", ctiApi.busyForward)

  def getRichCallHistory(
      domain: String,
      username: String,
      size: Int
  ): Action[AnyContent] =
    iPFilter.async { request =>
      log.info(s"Call history request for user $username with size $size")
      Requester
        .richCallHistory(
          username,
          HistorySize(size),
          configRepo,
          callHistoryManager
        )
        .map(history => Ok(Json.toJson(history)))
        .recover({ case e: Exception =>
          log.error(s"Error getting call history for $username: $e")
          InternalServerError(s"Error getting call history for $username: $e")
        })
    }

  def getRichCallHistoryByDays(
      domain: String,
      username: String,
      days: Int
  ): Action[AnyContent] =
    iPFilter.async { request =>
      log.info(
        s"Call history by days request for user $username with $days days"
      )
      Requester
        .richCallHistoryByDays(
          username,
          HistoryDays(days),
          configRepo,
          callHistoryManager
        )
        .map(history => Ok(Json.toJson(history)))
        .recover({ case e: Exception =>
          log.error(s"Error getting call history by days for $username: $e")
          InternalServerError(
            s"Error getting call history by days for $username: $e"
          )
        })
    }

  private def forward(
      domain: String,
      username: String,
      action: String,
      forwardRequest: (String, Boolean, String) => Future[RequestResult]
  ) =
    UserFromRequest(domain, username, action).async { implicit request =>
      log.info(s"forward $username " + request.body.asJson)

      request.body.asJson
        .map { json =>
          json
            .validate[(Boolean, String)](fwdf)
            .map { case (state, destination) =>
              val result = forwardRequest(username, state, destination)
              processResult(result, username)
            }
            .recoverTotal { e =>
              log.error(s"Error $username forward " + JsError.toJson(e))
              Future(
                BadRequest(s"Error $username forward " + JsError.toJson(e))
              )
            }
        }
        .getOrElse {
          log.error(s"Error $username forward Expecting Json data")
          Future(BadRequest(s"Error $username forward Expecting Json data"))
        }
    }

  def importRequests(listUuid: String): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async {
      implicit request =>
        log.info(s"import callback requests for list $listUuid")
        (request.body.asText match {
          case None =>
            Future.successful(BadRequest("Content should not be empty"))
          case Some(text) =>
            processResult(
              Requester.importCsvCallback(callbackManager, listUuid, text)
            )
        }).map(_.withHeaders("Access-Control-Allow-Origin" -> "*"))
    }

  def exportTickets(listUuid: String): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async {
      log.info(s"export callback tickets for list $listUuid")
      processResult(Requester.exportTicketsCsv(callbackManager, listUuid)).map(
        _.withHeaders(
          "Access-Control-Allow-Origin" -> "*",
          "Content-Type"                -> "text/csv",
          "Content-Disposition"         -> s"attachment;filename=$listUuid.csv"
        )
      )
    }

}
