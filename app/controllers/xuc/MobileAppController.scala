package controllers.xuc

import com.google.inject.Inject
import controllers.helpers.AuthenticatedAction
import controllers.helpers.AuthenticatedAction.getApiUser
import models.ws.{GenericError, JsonParsingError, NotHandledError}
import models.{MobileAppError, MobileAppPushToken}
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess}
import play.api.libs.ws.WSClient
import play.api.mvc._
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.xuc.XucBaseConfig

import scala.concurrent.{ExecutionContext, Future}

class MobileAppController @Inject() (implicit
    configRepository: ConfigRepository,
    configRequester: ConfigServerRequester,
    config: XucBaseConfig,
    parsers: PlayBodyParsers,
    ec: ExecutionContext,
    ws: WSClient,
    cc: ControllerComponents
) extends AbstractController(cc) {
  val log: Logger = Logger(getClass.getName)
  val serviceName = "MobileApp"

  implicit val contentParser: BodyParser[AnyContent] = parsers.anyContent

  def logRequest(username: Option[String], requestAction: String): Unit = {
    log.info(s"[$username] Req: <$serviceName - $requestAction>")
  }

  def add(): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      val user = getApiUser(request.user)
      logRequest(user.username, "create")

      request.body.asJson match {
        case Some(json) =>
          json.validate[MobileAppPushToken] match {
            case JsSuccess(token, _) =>
              configRequester
                .setMobileAppPushToken(
                  user.username,
                  token
                )
                .flatMap(_ => Future(Results.Created))
                .recover({ case _ =>
                  GenericError(
                    NotHandledError,
                    s"Error while adding mobile push notification token"
                  ).toResult
                })

            case JsError(_) =>
              Future(
                MobileAppError(JsonParsingError, "Error decoding JSON").toResult
              )
          }
        case None =>
          Future(
            MobileAppError(
              JsonParsingError,
              "No JSON could be decoded"
            ).toResult
          )
      }
    }
}
