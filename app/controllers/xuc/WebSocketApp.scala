package controllers.xuc

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.pattern.ask
import org.apache.pekko.stream.Materializer
import org.apache.pekko.stream.scaladsl.Flow
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.AuthenticatedAction.forbiddenAction
import controllers.helpers.AuthenticatedWebsocket
import controllers.security.KerberosAuthentication
import models._
import models.authentication.AuthenticationProvider
import play.api.Logger
import play.api.libs.json._
import play.api.mvc._
import play.api.routing.JavaScriptReverseRouter
import play.mvc.Http
import services.ActorIds
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.services.{TokenRetriever, XivoAuthentication}
import xivo.websocket.{ActorFlowOut, WsActor, WsBus}
import xivo.xuc.{ConfigServerConfig, XucBaseConfig}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

class WebSocketApp @Inject() (
    kerberos: KerberosAuthentication,
    authProvider: AuthenticationProvider,
    configRepository: ConfigRepository,
    configServerRequester: ConfigServerRequester,
    wsBus: WsBus,
    @Named(ActorIds.CtiRouterFactoryId) ctiRouterFactory: ActorRef,
    @Named(ActorIds.XivoAuthenticationId) xivoAuthentication: ActorRef,
    tokenRetrieverFactory: TokenRetriever.Factory,
    configServerConfig: ConfigServerConfig,
    config: XucBaseConfig,
    cc: ControllerComponents
)(implicit system: ActorSystem, mat: Materializer)
    extends AbstractController(cc) {
  val log: Logger = Logger(getClass.getName)

  import WebSocket.MessageFlowTransformer.jsonMessageFlowTransformer
  implicit val timeout: org.apache.pekko.util.Timeout = 2.seconds

  def javascriptChannel: Action[AnyContent] =
    Action { implicit request =>
      Ok(
        JavaScriptReverseRouter("xucRoutes")(
          routes.javascript.WebSocketApp.ctiChannel
        )
      ).as("text/javascript")
    }

  def failWebSocket(message: String): Future[Left[Result, Nothing]] =
    Future.successful(
      Left(
        Unauthorized(message).withHeaders(
          Http.HeaderNames.WWW_AUTHENTICATE -> "Negotiate"
        )
      )
    )

  def startWebSocket(
      xivoUser: XivoUser,
      phoneNumber: String
  ): Future[Right[Nothing, Flow[Any, Nothing, Any]]] = {
    log.info(s"[${xivoUser.username}] authentication success.")
    wsActor(xivoUser, phoneNumber)
  }

  def startWebSocket(
      login: String,
      phoneNumber: String
  ): Future[Either[Result, Flow[JsValue, JsValue, _]]] = {
    configRepository.getCtiUser(login) match {
      case Some(xivoUser) => startWebSocket(xivoUser, phoneNumber)
      case None =>
        failWebSocket(
          s"[$login] Authentication failed, please check your identifiers"
        )
    }
  }

  def ctiChannel(
      username: Option[String],
      phoneNumber: Option[String],
      password: Option[String]
  ): WebSocket =
    WebSocket.acceptOrResult[JsValue, JsValue] { implicit request =>
      log.info(s"[$username] websocket requested")
      kerberos.performKerberosAuthentication(request) match {
        case Some(login) => startWebSocket(login, phoneNumber.getOrElse("0"))
        case None =>
          username match {
            case None | Some("") =>
              log.info(
                s"Cannot create cti websocket username empty from host ${request.remoteAddress}"
              )
              failWebSocket("Username cannot be empty")
            case Some(username) =>
              configRepository.getCtiUser(
                username,
                password.getOrElse("")
              ) match {
                case Some(xivoUser) =>
                  startWebSocket(xivoUser, phoneNumber.getOrElse("0"))

                case None =>
                  try {
                    authProvider.authenticate(
                      username,
                      password.getOrElse("")
                    ) match {
                      case Some(user) =>
                        startWebSocket(username, phoneNumber.getOrElse("0"))
                      case None =>
                        log.info(s"[$username] authentication failed.")
                        failWebSocket(
                          s"[$username] Authentication failed, please check your identifiers"
                        )
                    }
                  } catch {
                    case e: Exception =>
                      log.error(
                        s"[$username] Unexpected authentication error ${e.getMessage}"
                      )
                      Future.successful(
                        Left(
                          InternalServerError(
                            s"[$username] Authentication error, please contact your administrator"
                          )
                        )
                      )
                  }
              }
          }
      }
    }

  def ctiChannelWithToken(token: String): WebSocket =
    WebSocket.acceptOrResult[JsValue, JsValue] { implicit request =>
      log.info(s"Token authentication : [$token] websocket requested")
      token match {
        case "" =>
          log.info(
            s"Cannot create cti websocket token empty from host ${request.remoteAddress}"
          )
          Future.successful(Left(Forbidden("Token cannot be empty")))
        case _ =>
          val userOpt =
            (xivoAuthentication ? XivoAuthentication.UserByToken(token))
              .mapTo[Option[XivoUser]]
          userOpt.flatMap(userOpt =>
            userOpt match {
              case None =>
                Future.successful(
                  Left(
                    Forbidden(
                      s"[$token] Authentication failed, please check your token is valid"
                    )
                  )
                )
              case Some(user) =>
                wsActor(user, "0")
            }
          )
      }
    }

  def ctiChannelAuthenticated: WebSocket =
    AuthenticatedWebsocket(config.Authentication.secret)(configRepository) {
      request =>
        request.user match {
          case Left(u) => wsActor(u, "0")
          case Right(_) =>
            Future(
              Left(
                forbiddenAction
              )
            )
        }
    }

  private def wsActor(
      xivoUser: XivoUser,
      phoneNumber: String
  ) =
    WsActor
      .props(
        getXucUser(xivoUser, phoneNumber),
        configServerRequester,
        wsBus,
        ctiRouterFactory,
        tokenRetrieverFactory,
        configRepository,
        configServerConfig
      )
      .map(ActorFlowOut.actorRef(_))
      .map(Right(_))

  private def getXucUser(
      user: XivoUser,
      phoneNumber: String
  ) = {
    if (phoneNumber == "0")
      XucUser(user.username.getOrElse(""), user)
    else
      XucUser(user.username.getOrElse(""), user, Some(phoneNumber))
  }
}
