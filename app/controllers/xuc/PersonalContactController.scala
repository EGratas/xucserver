package controllers.xuc

import org.apache.pekko.actor.{ActorRef, ActorSelection, ActorSystem}
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.AuthenticatedAction.getApiUser
import controllers.helpers.{AuthenticatedAction, AuthenticatedRequest}
import models.RichDirectoryResult
import models.ws.auth.ApiUser
import models.ws.{GenericError, JsonParsingError, NotHandledError}
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.libs.ws.WSResponse
import play.api.mvc._
import services.config.{ConfigRepository, DeleteAllEntries}
import services.{ActorIds, ActorIdsFactory}
import xivo.directory.DirdRequester
import xivo.directory.PersonalContactRepository.{
  DeletePersonalContact,
  SetPersonalContact
}
import xivo.models._
import xivo.xuc.XucBaseConfig

import java.text.SimpleDateFormat
import java.util.Calendar
import scala.concurrent.{ExecutionContext, Future}
import org.apache.pekko.actor.ActorPath

class PersonalContactController @Inject() (implicit
    @Named(ActorIds.CtiRouterFactoryId) ctiRouterFactory: ActorRef,
    system: ActorSystem,
    configRepository: ConfigRepository,
    dirdRequester: DirdRequester,
    config: XucBaseConfig,
    actorIdFactory: ActorIdsFactory,
    parsers: PlayBodyParsers,
    ec: ExecutionContext,
    cc: ControllerComponents
) extends AbstractController(cc) {
  val log: Logger                                    = Logger(getClass.getName)
  val serviceName                                    = "PersonalContact"
  implicit val contentParser: BodyParser[AnyContent] = parsers.anyContent

  def logRequest(username: Option[String], requestAction: String): Unit = {
    log.info(s"[$username] Req: <$serviceName - $requestAction>")
  }

  def dirdResultError(e: DirdRequesterException): Result = {
    PersonalContactError(e.errorType, e.message).toResult
  }

  def genericError(
      username: Option[String],
      requestAction: String,
      e: Throwable
  ): Result = {
    GenericError(
      NotHandledError,
      s"Error $requestAction personal contact for $username: ${e.getMessage}"
    ).toResult
  }

  def jsonValidationError(
      username: Option[String],
      requestAction: String,
      requestBody: JsValue
  ): Future[Result] = {
    Future(
      GenericError(
        JsonParsingError,
        s"Error $requestAction personal contact for $username: Invalid JSON $requestBody"
      ).toResult
    )
  }

  def returnError(
      requestUserName: Option[String],
      requestAction: String
  ): PartialFunction[Throwable, Result] = {
    case de: DirdRequesterException => dirdResultError(de)
    case e                          => genericError(requestUserName, requestAction, e)
  }

  def getPersonalContactRepoPath(
      ctiRouterFactory: ActorRef,
      username: Option[String]
  ): ActorPath = {
    actorIdFactory.personalContactRepositoryPath(
      ctiRouterFactory,
      username.getOrElse("")
    )
  }

  def getPersonalContactRepoActor(username: Option[String]): ActorSelection = {
    system.actorSelection(
      getPersonalContactRepoPath(ctiRouterFactory, username)
    )
  }

  def richList: Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      def result(resp: RichDirectoryResult): Result = {
        Results.Ok(Json.toJson(resp))
      }

      execute[RichDirectoryResult](
        request,
        "richList",
        dirdRequester.richList,
        result
      )
    }

  def list: Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      def result(resp: List[PersonalContactResult]): Result = {
        Results.Ok(Json.toJson(resp))
      }

      execute[List[PersonalContactResult]](
        request,
        "list",
        dirdRequester.list,
        result
      )
    }

  def get(id: String): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      def result(resp: PersonalContactResult): Result = {
        Results.Ok(Json.toJson(resp))
      }

      execute[PersonalContactResult](
        request,
        "get",
        dirdRequester.get(_)(id),
        result
      )
    }

  def delete(id: String): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      def result(resp: WSResponse): Result = {
        val user = getApiUser(request.user)
        getPersonalContactRepoActor(
          user.username
        ) ! DeletePersonalContact(id)
        Results.NoContent
      }

      execute[WSResponse](
        request,
        "delete",
        dirdRequester.delete(_)(id),
        result
      )
    }

  def deleteAll: Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      def result(resp: WSResponse): Result = {
        val user = getApiUser(request.user)
        getPersonalContactRepoActor(
          user.username
        ) ! DeleteAllEntries
        Results.NoContent
      }

      execute[WSResponse](
        request,
        "deleteAll",
        dirdRequester.deleteAll,
        result
      )
    }

  def add: Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      request =>
        def result(resp: PersonalContactResult): Result = {
          val user = getApiUser(request.user)
          getPersonalContactRepoActor(
            user.username
          ) ! SetPersonalContact(resp)
          Results.Created(Json.toJson(resp))
        }

        executeWithPc(request, "add", dirdRequester.add, result)
    }

  def edit(id: String): Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      request =>
        def result(resp: PersonalContactResult): Result = {
          val user = getApiUser(request.user)
          getPersonalContactRepoActor(
            user.username
          ) ! SetPersonalContact(resp)
          Results.Ok(Json.toJson(resp))
        }

        executeWithPc(request, "edit", dirdRequester.edit(_, _)(id), result)
    }

  def exportCsv: Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      def result(resp: String): Result = {
        val now       = Calendar.getInstance.getTime
        val formatter = new SimpleDateFormat("yyyy-MM-dd")
        Results
          .Ok(resp)
          .withHeaders(
            ACCESS_CONTROL_ALLOW_ORIGIN -> "*",
            CONTENT_TYPE                -> "text/csv;charset=UTF-8",
            CONTENT_DISPOSITION         -> s"attachment;filename=export-${formatter.format(now)}.csv"
          )
      }

      execute[String](request, "export", dirdRequester.`exportCsv`, result)
    }

  def importCsv: Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret)
      .async(parsers.anyContent) { request =>
        def result(resp: PersonalContactImportResult): Result = {
          val user = getApiUser(request.user)
          val pcRepoActor =
            getPersonalContactRepoActor(user.username)
          for {
            list <- resp.created
            pc   <- list
          } pcRepoActor ! SetPersonalContact(pc)
          Results
            .Created(Json.toJson(resp))
            .withHeaders(CONTENT_TYPE -> "application/json;charset=UTF-8")
        }

        execute[PersonalContactImportResult](
          request,
          "import",
          dirdRequester.importCsv(_)(request.body.asText),
          result
        )
      }

  private def executeWithPc(
      request: AuthenticatedRequest[JsValue],
      requestAction: String,
      dirdRequest: (
          ApiUser,
          PersonalContactRequest
      ) => Future[PersonalContactResult],
      result: PersonalContactResult => Result
  ) = {
    val user = getApiUser(request.user)
    logRequest(user.username, requestAction)

    request.body.validate[PersonalContactRequest] match {
      case JsSuccess(pc, _) =>
        dirdRequest(user, pc)
          .map(response => result(response))
          .recover {
            returnError(user.username, requestAction)
          }
      case JsError(_) =>
        jsonValidationError(
          user.username,
          requestAction,
          request.body
        )
    }
  }

  private def execute[T](
      request: AuthenticatedRequest[AnyContent],
      requestAction: String,
      dirdRequest: ApiUser => Future[T],
      result: T => Result
  ) = {
    val user = getApiUser(request.user)
    logRequest(user.username, requestAction)

    dirdRequest(user)
      .map(response => result(response))
      .recover {
        returnError(user.username, requestAction)
      }
  }
}
