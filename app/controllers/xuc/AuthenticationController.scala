package controllers.xuc

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.pattern._
import org.apache.pekko.util.Timeout
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers._
import controllers.security.KerberosAuthentication
import models.XucUser
import models.authentication.AuthenticationProvider
import models.usm.LoginEvent
import models.ws.auth.AuthType.AuthType
import models.ws.auth.SoftwareType.SoftwareType
import models.ws.auth._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.Logger
import play.api.libs.Codecs
import play.api.libs.json.{JsError, JsSuccess}
import play.api.mvc._
import services.auth.WebService
import services.config.ConfigRepository
import services.{ActorIds, GetRouter, Router}
import xivo.network.XiVOWS
import xivo.services.TokenRetriever
import xivo.xuc.XucBaseConfig

import java.security.MessageDigest
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import scala.util.Success
import scala.util.Failure
import play.api.libs.json.JsValue

class AuthenticationController @Inject() (
    system: ActorSystem,
    authProvider: AuthenticationProvider,
    @Named(ActorIds.CtiRouterFactoryId) ctiRouterFactory: ActorRef,
    @Named(ActorIds.UsageEventsId) usageEvents: ActorRef,
    xivoWs: XiVOWS,
    parsers: PlayBodyParsers,
    ws: WebService,
    cc: ControllerComponents
)(implicit
    krb: KerberosAuthentication,
    config: XucBaseConfig,
    configRepository: ConfigRepository,
    ec: ExecutionContext,
    @Named(ActorIds.XivoAuthenticationId) xivoAuthentication: ActorRef
) extends AbstractController(cc) {

  import helpers.OptionToTry._
  val md: MessageDigest = MessageDigest.getInstance("SHA-1")
  val log: Logger       = Logger(getClass.getName)
  val tokenRetriever: ActorRef =
    system.actorOf(TokenRetriever.props(xivoAuthentication, configRepository))
  implicit val contentParser: BodyParser[AnyContent] = parsers.anyContent

  def webService(): Action[JsValue] =
    Action.async(parsers.json) { request =>
      log.info("Received web service authentication request")
      request.body.validate[AuthenticationRequest] match {
        case JsSuccess(r, _) =>
          checkWebServiceCredentials(
            r.login,
            r.password,
            r.expiration
          )
            .recover { case e: AuthenticationException =>
              AuthenticationFailure(
                e.error,
                e.getMessage
              )
            }
            .map(_.toResult)
        case JsError(e) =>
          Future.successful(
            AuthenticationFailure(
              AuthenticationError.InvalidJson,
              "Invalid JSON: " + e.toString()
            ).toResult
          )
      }
    }

  def login(): Action[JsValue] =
    Action.async(parsers.json) { request =>
      request.body.validate[AuthenticationRequest] match {
        case JsSuccess(r, _) =>
          checkCredentials(
            r.login,
            r.password,
            r.softwareType
          ).map(_.toResult)
        case JsError(e) =>
          Future.successful(
            AuthenticationFailure(
              AuthenticationError.InvalidJson,
              "Invalid JSON: " + e.toString()
            ).toResult
          )
      }
    }

  private def allowLogin(
      login: String,
      authType: AuthType,
      softwareType: Option[SoftwareType],
      config: XucBaseConfig,
      acls: List[String]
  ): AuthenticationSuccess = {
    val softType = softwareType.getOrElse(SoftwareType.unknown)
    logLoginType(
      login,
      authType,
      softType
    )
    AuthenticationSuccess(
      login,
      AuthenticationInformation.encodeFromLogin(
        login,
        config.Authentication.secret,
        config.Authentication.expires,
        softwareType,
        acls
      ),
      config.Authentication.expires
    )
  }

  def getExpiry(requestedExpiry: Int, userType: String): Int = {
    AuthUserType.decode(userType) match {
      case AuthUserType.Cti =>
        limitExpiration(requestedExpiry, config.ctiExpires)
      case AuthUserType.Webservice =>
        limitExpiration(requestedExpiry, config.wsMaxExpires)
      case AuthUserType.Unknown => config.defaultWebServiceExpires
    }
  }

  private def limitExpiration(expiration: Int, maxExpiration: Int): Int = {
    if (expiration > maxExpiration) maxExpiration
    else expiration
  }

  private def mobileSetupAuthError = new AuthenticationException(
    AuthenticationError.WrongMobileSetup,
    "Missing mobile push token key on XiVO"
  )

  private def checkInvalidMobileLogin(
      login: String,
      softwareType: Option[SoftwareType]
  ): Boolean = {
    val softType = softwareType.getOrElse(SoftwareType.unknown)
    if (
      softType.equals(
        SoftwareType.mobile
      ) && !configRepository.mobileConfigIsValid
    ) {
      log.warn(
        s"Connection refused to $login from mobile as push token key is not configured on XiVO"
      )
      true
    } else false
  }

  private def checkCredentials(
      login: String,
      password: String,
      softwareType: Option[SoftwareType]
  ): Future[AuthenticationResult] = {
    log.debug(s"Checking credentials for $login")
    implicit val timeout: Timeout = Timeout(1.second)

    val result = if (login.isEmpty) {
      Future.failed(
        new AuthenticationException(
          AuthenticationError.EmptyLogin,
          "Login is required"
        )
      )
    } else if (
      config.Authentication.preventXucUserLogin && config.EventUser == login
    ) {
      log.warn(
        s"Preventing application access to xuc user $login. Set authentication.preventXucUserLogin to false in application.conf to allow access."
      )
      Future.failed(
        new AuthenticationException(
          AuthenticationError.InvalidCredentials,
          "Invalid credentials"
        )
      )
    } else {
      configRepository.getCtiUser(login) match {
        case Some(user) =>
          log.debug(s"Found $user")
          if (authProvider.authenticate(login, password).isDefined) {
            if (
              checkInvalidMobileLogin(
                login,
                softwareType
              )
            ) {
              Future.failed(
                mobileSetupAuthError
              )
            } else {
              for {
                router <- (ctiRouterFactory ? GetRouter(
                  XucUser(user.username.getOrElse(""), user)
                )).mapTo[Router]
                token <- XivoAuthenticationHelper.getXivoAuthToken(
                  user.id,
                  config
                ) match {
                  case Some(token) => Future.successful(token)
                  case None =>
                    Future.failed(
                      new AuthenticationException(
                        AuthenticationError.UserNotFound,
                        s"XiVO auth token cannot be retrieved for userId ${user.id}"
                      )
                    )
                }
              } yield allowLogin(
                login,
                AuthType.basic,
                softwareType,
                config,
                token.acls
              )
            }
          } else {
            log.warn(s"Invalid password for $login")
            Future.failed(
              new AuthenticationException(
                AuthenticationError.InvalidCredentials,
                "Invalid credentials"
              )
            )
          }
        case None =>
          log.warn(s"User $login not found in local repository")
          Future.failed(
            new AuthenticationException(
              AuthenticationError.InvalidCredentials,
              "Invalid credentials"
            )
          )
      }
    }

    result.recover({ case t => AuthenticationFailure.from(t) })

  }

  def loginSso(softwareType: Option[String]): Action[AnyContent] =
    SsoAuthenticatedAction(config.Authentication.expires).apply { request =>
      if (
        checkInvalidMobileLogin(
          request.auth.login,
          request.auth.softwareType
        )
      ) {
        AuthenticationFailure
          .from(
            mobileSetupAuthError
          )
          .toResult
      } else {
        allowLogin(
          request.auth.login,
          AuthType.kerberos,
          request.auth.softwareType,
          config,
          request.auth.acls
        ).toResult
      }
    }

  def loginCas(
      service: String,
      ticket: String,
      softwareType: Option[String]
  ): Action[AnyContent] =
    CasAuthenticatedAction(xivoWs, config).apply { request =>
      if (
        checkInvalidMobileLogin(
          request.auth.login,
          request.auth.softwareType
        )
      ) {
        AuthenticationFailure
          .from(
            mobileSetupAuthError
          )
          .toResult
      } else {
        allowLogin(
          request.auth.login,
          AuthType.cas,
          request.auth.softwareType,
          config,
          request.auth.acls
        ).toResult
      }
    }

  def loginOidc(softwareType: Option[String]): Action[AnyContent] =
    OidcAuthenticatedAction(xivoWs.WS, config).apply { request =>
      if (
        checkInvalidMobileLogin(
          request.auth.login,
          request.auth.softwareType
        )
      ) {
        AuthenticationFailure
          .from(
            mobileSetupAuthError
          )
          .toResult
      } else {
        allowLogin(
          request.auth.login,
          AuthType.oidc,
          request.auth.softwareType,
          config,
          request.auth.acls
        ).toResult
      }
    }

  def check(): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).apply { request =>
      val tokenExpiration = request.auth.expiresAt - request.auth.issuedAt
      val expiration = getExpiry(
        tokenExpiration.toInt,
        request.auth.userType
      )
      val tryAcls: Try[List[String]] =
        if (request.auth.userType == AuthenticatedAction.webServiceUserType) {
          configRepository
            .getWebServiceUser(
              request.auth.login
            )
            .map(_.acls)
            .toTry(
              new AuthenticationException(
                AuthenticationError.UserNotFound,
                "Matching XiVO user not found"
              )
            )
        } else {
          Success(request.auth.acls)
        }

      tryAcls match {
        case Success(acls) =>
          AuthenticationSuccess(
            request.auth.login,
            request.auth
              .refresh(expiration, acls)
              .encode(config.Authentication.secret),
            expiration
          ).toResult
        case Failure(e) => AuthenticationFailure.from(e).toResult
      }
    }

  def anonymize(s: String): String = {
    Codecs.sha1(md.digest(s.getBytes))
  }

  def logLoginType(
      login: String,
      authType: AuthType,
      softType: SoftwareType
  ): Unit = {
    val connectionType = ConnectionType(
      softType,
      authType,
      configRepository
        .getLineForUser(login)
        .map(LineType.lineToLineType)
    )
    val loginEvent: LoginEvent = LoginEvent(
      connectionType.softwareType.toString,
      "undefined",
      connectionType.lineType.map(_.toString).getOrElse("undefined"),
      configRepository
        .getCtiUser(login)
        .map(u => anonymize(u.id.toString))
        .getOrElse("undefined"),
      DateTimeFormat
        .forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
        .print(new DateTime())
    )
    usageEvents ! loginEvent
    log.info(
      s"""Login user $login
           | -- auth type: ${connectionType.authType}
           | -- software type: ${connectionType.softwareType}
           | -- line type: ${connectionType.lineType
        .getOrElse("unknown")}""".stripMargin
    )
  }

  private def checkWebServiceCredentials(
      login: String,
      password: String,
      expiration: Option[Int]
  ): Future[AuthenticationSuccess] = {

    log.debug(s"Checking credentials for web service user $login")

    implicit val timeout: Timeout = Timeout(1.second)

    val finalExpiration = {
      getExpiry(
        expiration.getOrElse(config.defaultWebServiceExpires),
        AuthUserType.Webservice.toString
      )
    }

    ws
      .authenticate(
        login,
        password,
        finalExpiration
      )
      .map(authToken => ws.getAuthenticationInformation(login, authToken))
      .map(authenticationInformation =>
        AuthenticationSuccess(
          login,
          ws.encodeToJWT(authenticationInformation),
          finalExpiration
        )
      )
  }

}
