package models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Format, JsPath, Json, Reads, Writes}

case class IceConfig(
    stunConfig: Option[StunConfig] = None,
    turnConfig: Option[TurnConfig] = None
)

object IceConfig {
  implicit val format: Format[IceConfig] =
    (
      (JsPath \ "stunConfig").formatNullable[StunConfig] and
        (JsPath \ "turnConfig").formatNullable[TurnConfig]
    )(IceConfig.apply, o => (o.stunConfig, o.turnConfig))

}
