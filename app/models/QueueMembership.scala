package models

import play.api.libs.json._
import play.api.libs.functional.syntax.toFunctionalBuilderOps

case class QueueMembership(queueId: Long, penalty: Int)

object QueueMembership {
  implicit val format: Format[QueueMembership] =
    (
      (JsPath \ "queueId").format[Long] and
        (JsPath \ "penalty").format[Int]
    )(QueueMembership.apply, o => (o.queueId, o.penalty))
}

case class UserQueueDefaultMembership(
    userId: Long,
    membership: List[QueueMembership]
)

object UserQueueDefaultMembership {
  implicit val format: Format[UserQueueDefaultMembership] =
    (
      (JsPath \ "userId").format[Long] and
        (JsPath \ "membership").format[List[QueueMembership]]
    )(UserQueueDefaultMembership.apply, o => (o.userId, o.membership))
}

case class UsersQueueDefaultMembership(
    memberships: List[UserQueueDefaultMembership]
)

object UsersQueueDefaultMembership {

  val reads: Reads[UsersQueueDefaultMembership] =
    (JsPath \ "memberships").read[List[UserQueueDefaultMembership]].map { o =>
      UsersQueueDefaultMembership(o)
    }

  val writes: Writes[UsersQueueDefaultMembership] =
    (JsPath \ "memberships")
      .write[List[UserQueueDefaultMembership]]
      .contramap(o => o.memberships)

  implicit val format: Format[UsersQueueDefaultMembership] =
    Format(reads, writes)
}
