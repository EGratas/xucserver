package models

import play.api.libs.json.{JsString, JsValue, Json, Writes}
import xivo.models.Line

case class LineConfig(
    id: String,
    phoneNb: String,
    line: Option[Line] = None,
    sipPort: Option[String] = None
)
case object LineConfig {
  implicit val writes: Writes[LineConfig] = new Writes[LineConfig] {
    def writes(lc: LineConfig): JsValue =
      lc.line match {
        case Some(line) =>
          val common = Json.obj(
            "id"        -> lc.id,
            "name"      -> lc.line.fold("")(l => l.name),
            "xivoIp"    -> lc.line.fold("")(l => l.xivoIp),
            "hasDevice" -> lc.line.exists(l => l.dev.nonEmpty),
            "isUa"      -> lc.line.exists(l => l.ua),
            "webRtc"    -> lc.line.fold(false)(l => l.webRTC),
            "vendor"    -> lc.line.flatMap(_.dev.map(_.vendor)),
            "number"    -> lc.phoneNb,
            "mobileApp" -> lc.line.exists(_.mobileApp)
          )
          var eventuallyWithPassword = line.dev match {
            case Some(_) => common
            case None =>
              common + (("password", JsString(line.password.getOrElse(""))))
          }
          line.webRTC match {
            case true =>
              eventuallyWithPassword = eventuallyWithPassword + (
                (
                  "sipProxyName",
                  JsString(s"${line.registrar.getOrElse("default")}")
                )
              )
            case false => eventuallyWithPassword
          }
          lc.sipPort match {
            case Some(sp: String) =>
              eventuallyWithPassword = eventuallyWithPassword +
                (
                  (
                    "sipPort",
                    JsString(s"$sp")
                  )
                )
            case None => eventuallyWithPassword
          }
          eventuallyWithPassword
        case None =>
          Json.obj("id" -> lc.id)
      }
  }
}
