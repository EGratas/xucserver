package models

import org.joda.time.DateTime
import play.api.libs.json._
import play.api.libs.functional.syntax._

sealed trait XivoAuth

/*
 * compatible with XiVO auth swagger specification v0.1
 */

case class CreateTokenParameters(backend: String, expires: Option[Integer])
    extends XivoAuth

case class TokenRequest(backend: String, expiration: Long) extends XivoAuth
object TokenRequest {
  implicit val format: Format[TokenRequest] =
    (
      (JsPath \ "backend").format[String] and
        (JsPath \ "expiration").format[Long]
    )(TokenRequest.apply, o => (o.backend, o.expiration))
}

case class XivoAuthToken(
    token: String,
    expiresAt: DateTime,
    issuedAt: DateTime,
    authId: String,
    xivoUserUuid: Option[String],
    acls: List[String]
)

object XivoAuthToken {
  val fmt: String = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
  implicit val dateReads: Reads[DateTime] =
    JodaReads.jodaDateReads(fmt)
  implicit val reads: Reads[XivoAuthToken] = (
    (JsPath \ "data" \ "token").read[String] and
      (JsPath \ "data" \ "expires_at")
        .read[DateTime] and
      (JsPath \ "data" \ "issued_at").read[DateTime] and
      (JsPath \ "data" \ "auth_id").read[String] and
      (JsPath \ "data" \ "xivo_user_uuid")
        .readNullable[String] and
      (JsPath \ "data" \ "acls").read[List[String]]
  )(XivoAuthToken.apply _)
}
case class Token(
    token: String,
    expiresAt: DateTime,
    issuedAt: DateTime,
    userType: String,
    xivoUserUuid: Option[String],
    acls: List[String]
)
object Token {
  val fmt: String = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
  implicit val dateReads: Reads[DateTime] =
    JodaReads.jodaDateReads(fmt)
  implicit val dateWrites: Writes[DateTime] =
    JodaWrites.jodaDateWrites(fmt)
  implicit val writes: Writes[Token] =
    (
      (JsPath \ "token").write[String] and
        (JsPath \ "expiresAt").write[DateTime] and
        (JsPath \ "issuedAt").write[DateTime] and
        (JsPath \ "userType").write[String] and
        (JsPath \ "xivoUserUuid").writeNullable[String] and
        (JsPath \ "acls").write[List[String]]
    )(o =>
      (o.token, o.expiresAt, o.issuedAt, o.userType, o.xivoUserUuid, o.acls)
    )
}
case class Error(message: String, status: Integer)
