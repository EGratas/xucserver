package models

import anorm.SqlParser.get
import anorm.{~, SQL}
import com.google.inject.{ImplementedBy, Inject}
import play.api.db.Database
import play.api.libs.json.*
import xivo.network.XiVOWS
import anorm.as

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import anorm.RowParser
import play.api.libs.functional.syntax.toFunctionalBuilderOps

import scala.concurrent.duration.FiniteDuration

case class WebServiceUser(
    login: String,
    password: String,
    name: String,
    acls: List[String] = List.empty
)

@ImplementedBy(classOf[WebServiceUserDaoImpl])
trait WebServiceUserDao {
  def getWebServiceUser(login: String): Future[Option[WebServiceUser]]
  def getWebServiceUsers(): Future[List[WebServiceUser]]
}

class WebServiceUserDaoImpl @Inject() (db: Database, ws: XiVOWS)
    extends WebServiceUserDao {
  val timeout: FiniteDuration = 10.seconds

  implicit val context: ExecutionContext =
    scala.concurrent.ExecutionContext.Implicits.global
  implicit val format: Format[WebServiceUser] =
    (
      (JsPath \ "login").format[String] and
        (JsPath \ "password").format[String] and
        (JsPath \ "name").format[String] and
        (JsPath \ "acls").format[List[String]]
    )(WebServiceUser.apply, o => (o.login, o.password, o.name, o.acls))

  val queryWebServiceUsers =
    "select login, passwd, name, acl from accesswebservice where login is not null and passwd is not null and disable != 1"

  def queryWebServiceUser(login: String): String =
    s"$queryWebServiceUsers and login='$login'"

  val webServiceUserMapping: RowParser[WebServiceUser] = {
    get[String]("login") ~
      get[String]("passwd") ~
      get[String]("name") ~
      get[List[String]]("acl") map { case login ~ password ~ name ~ acls =>
        WebServiceUser(
          login,
          password,
          name,
          acls
        )
      }
  }

  def getWebServiceUser(login: String): Future[Option[WebServiceUser]] = {
    Future {
      db.withConnection { implicit c =>
        SQL(queryWebServiceUser(login)).as(webServiceUserMapping.*).headOption
      }
    }
  }

  def getWebServiceUsers(): Future[List[WebServiceUser]] = {
    Future {
      db.withConnection { implicit c =>
        SQL(queryWebServiceUsers).as(webServiceUserMapping.*)
      }
    }
  }

}
