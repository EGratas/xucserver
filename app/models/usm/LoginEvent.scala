package models.usm

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, Json, Writes}

case class LoginEvent(
    softwareType: String,
    applicationType: String,
    lineType: String,
    userId: String,
    eventTime: String
)

object LoginEvent {
  implicit val loginEventWrites: Writes[LoginEvent] =
    ((JsPath \ "softwareType").write[String] and
      (JsPath \ "applicationType").write[String] and
      (JsPath \ "lineType").write[String] and
      (JsPath \ "userId").write[String] and
      (JsPath \ "eventTime").write[String])(o =>
      (
        o.softwareType,
        o.applicationType,
        o.lineType,
        o.userId,
        o.eventTime
      )
    )
}
