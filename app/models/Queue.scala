package models

import play.api.libs.json.{JsValue, Json, Writes}

case class Queue(id: Option[Int], name: String, displayName: String)

object Queue {
  implicit val writes: Writes[Queue] = new Writes[Queue] {
    def writes(queue: Queue): JsValue =
      Json.obj(
        "id"          -> queue.id,
        "name"        -> queue.name,
        "displayName" -> queue.displayName
      )
  }
}
