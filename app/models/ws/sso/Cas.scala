package models.ws.sso

import models.ws.auth.AuthenticationError
import scala.util.Try
import scala.xml._
import scala.util.{Failure, Success}

object CasAuthenticationError extends Enumeration {
  val InvalidRequest: Value           = Value("INVALID_REQUEST")
  val InvalidTicketSpec: Value        = Value("INVALID_TICKET_SPEC")
  val UnauthorizedServiceProxy: Value = Value("UNAUTHORIZED_SERVICE_PROXY")
  val InvalidProxyCallback: Value     = Value("INVALID_PROXY_CALLBACK")
  val InvalidTicket: Value            = Value("INVALID_TICKET")
  val InvalidService: Value           = Value("INVALID_SERVICE")
  val InternalError: Value            = Value("INTERNAL_ERROR")

  def decode(s: String): Try[Value] = Try(withName(s))

  def toAuthenticationError(e: Value): AuthenticationError.Value = {
    import AuthenticationError._

    e match {
      case InvalidRequest           => CasServerInvalidRequest
      case InvalidTicketSpec        => CasServerInvalidTicketSpec
      case UnauthorizedServiceProxy => CasServerUnauthorizedServiceProxy
      case InvalidProxyCallback     => CasServerInvalidProxyCallback
      case InvalidTicket            => CasServerInvalidTicket
      case InvalidService           => CasServerInvalidService
      case InternalError            => CasServerInternalError
    }
  }
}

sealed trait CasServiceResponseType
case class CasAuthenticationFailure(
    code: CasAuthenticationError.Value,
    description: Option[String]
) extends CasServiceResponseType

object CasAuthenticationFailure {
  def decode(e: Elem): Try[CasAuthenticationFailure] =
    if (e.label == "authenticationFailure") {
      val description = Option(e.text.trim).filterNot(_.isEmpty)
      val codeStr     = e \@ "code"
      CasAuthenticationError
        .decode(codeStr)
        .map(code => CasAuthenticationFailure(code, description))
    } else {
      Failure(new Exception("Invalid authentication failure node"))
    }
}

case class CasUser(username: String)

object CasUser {
  def decode(e: Elem): Try[CasUser] = {
    if (e.label == "user") {
      Success(CasUser(e.text.toLowerCase()))
    } else {
      Failure(new Exception("Invalid user node"))
    }
  }
}

case class CasAttribute(name: String, value: String)

object CasAttribute {
  def decode(e: Elem): Try[CasAttribute] = {
    Success(CasAttribute(e.label, e.text))
  }

  def decodeAttributes(e: Elem): Try[List[CasAttribute]] = {
    if (e.label == "attributes") {
      Success(
        e.child
          .collect { case e: Elem =>
            decode(e)
          }
          .map(_.toOption)
          .toList
          .flatten
      )
    } else {
      Failure(new Exception("Invalid attributes node"))
    }
  }
}

case class CasAuthenticationSuccess(
    user: CasUser,
    attributes: List[CasAttribute]
) extends CasServiceResponseType

object CasAuthenticationSuccess {
  def decode(e: Elem): Try[CasAuthenticationSuccess] = {
    if (e.label == "authenticationSuccess") {
      val userNode = (e \ "user").headOption
        .collect { case e: Elem =>
          CasUser.decode(e)
        }
        .getOrElse(Failure(new Exception("User node not found")))

      val attributes = (e \ "attributes").headOption
        .collect { case e: Elem =>
          CasAttribute.decodeAttributes(e).toOption
        }
        .flatten
        .getOrElse(List.empty)
      userNode.map(CasAuthenticationSuccess(_, attributes))
    } else {
      Failure(new Exception("Invalid authentication success node"))
    }

  }
}

case class CasServiceResponse(response: CasServiceResponseType)

object CasServiceResponse {
  def decode(s: String): Try[CasServiceResponse] = {
    Try(XML.loadString(s)).flatMap(xml =>
      if (xml.label == "serviceResponse") {
        xml.child
          .filter(isElem(_))
          .map(decodeInnerResponse(_))
          .headOption
          .getOrElse(
            Failure(new NoSuchElementException("Service response is empty"))
          )
          .map(CasServiceResponse(_))
      } else
        Failure(
          new IllegalArgumentException(
            s"Not a valid service response ${xml.label}"
          )
        )
    )
  }

  private def isElem(n: Node): Boolean =
    n match {
      case _: Elem => true
      case _       => false
    }

  private def decodeInnerResponse(n: Node): Try[CasServiceResponseType] = {
    n match {
      case e: Elem if e.label == "authenticationFailure" =>
        CasAuthenticationFailure.decode(e)
      case e: Elem if e.label == "authenticationSuccess" =>
        CasAuthenticationSuccess.decode(e)
      case _ => Failure(new Exception(s"Unexpected node ${n.label}"))
    }
  }
}
