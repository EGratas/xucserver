package models.ws
import play.api.libs.functional.syntax._

import play.api.Logger
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.*
import play.api.mvc.{Result, Results}

trait ErrorResult {
  val log: Logger = Logger("ErrorResult")
  def toResult: Result
}

trait ErrorType { def error: String }
case object JsonParsingError extends ErrorType {
  val error = "JsonParsingError"
}
case object NotHandledError extends ErrorType { val error = "NotHandledError" }

object ErrorType {
  implicit val writes: Writes[ErrorType] = new Writes[ErrorType] {
    def writes(e: ErrorType): JsValue = JsString(e.error)
  }
}

object GenericError {
  implicit val genericErrorWrites: Writes[GenericError] =
    ((JsPath \ "code").write[ErrorType] and
      (JsPath \ "message").write[String])(o => (o.code, o.message))
}

case class GenericError(code: ErrorType, message: String) extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    Results.InternalServerError(Json.toJson(this))
  }
}
