package models.ws.auth

import models.ws.auth.AuthType.AuthType
import models.ws.auth.LineType.LineType
import models.ws.auth.SoftwareType.SoftwareType
import pdi.jwt.{JwtAlgorithm, JwtJson, JwtJsonImplicits}
import play.api.Logger
import play.api.libs.json.*
import play.api.mvc.{Result, Results}
import play.mvc.Http
import xivo.models.Line

import scala.util.{Failure, Success, Try}
import controllers.helpers.AuthenticatedAction
import models.ws.auth
import play.api.libs.functional.syntax.toFunctionalBuilderOps

case class ConnectionType(
    softwareType: SoftwareType,
    authType: AuthType,
    lineType: Option[LineType]
)

object AuthType extends Enumeration {
  type AuthType = Value

  val basic: Value    = Value("basic")
  val kerberos: Value = Value("kerberos")
  val cas: Value      = Value("cas")
  val oidc: Value     = Value("oidc")
}

object SoftwareType extends Enumeration {
  implicit val format: Format[SoftwareType] = Json.formatEnum(this)

  type SoftwareType = Value

  val electron: Value   = Value("electron")
  val webBrowser: Value = Value("webBrowser")
  val mobile: Value     = Value("mobile")
  val unknown: Value    = Value("unknown")

  def decode(s: Option[String]): Option[Value] = Try(
    withName(s.getOrElse(unknown.toString))
  ).toOption
}

object AuthUserType extends Enumeration {
  type AuthUserType = Value

  val Cti: Value        = Value("cti")
  val Webservice: Value = Value("webservice")
  val Unknown: Value    = Value("unknown")

  def decode(s: String): Value =
    values.find(_.toString.toLowerCase() == s.toLowerCase()).getOrElse(Unknown)
}

object LineType extends Enumeration {

  implicit val format: Format[LineType] = Json.formatEnum(this)

  type LineType = Value

  val phone: Value  = Value("phone")
  val webRTC: Value = Value("webRTC")
  val ua: Value     = Value("ua")

  def lineToLineType(line: Line): LineType = {
    if (line.ua) LineType.ua
    else if (line.webRTC) LineType.webRTC
    else LineType.phone
  }
}

case class AuthenticationRequest(
    login: String,
    password: String,
    expiration: Option[Int],
    entity: Option[String],
    softwareType: Option[SoftwareType]
)

object AuthenticationRequest {
  implicit val format: Format[AuthenticationRequest] =
    ((JsPath \ "login").format[String] and
      (JsPath \ "password").format[String] and
      (JsPath \ "expiration").formatNullable[Int] and
      (JsPath \ "entity").formatNullable[String] and
      (JsPath \ "softwareType").formatNullable[SoftwareType])(
      AuthenticationRequest.apply,
      c => (c.login, c.password, c.expiration, c.entity, c.softwareType)
    )

  implicit def enumWrites: Writes[SoftwareType.SoftwareType] =
    (value: SoftwareType.SoftwareType) => JsString(value.toString)
}

sealed trait AuthenticationResult {
  val log: Logger = Logger("AuthenticationResult")
  def toResult: Result
}

object AuthenticationSuccess {
  implicit val format: Format[AuthenticationSuccess] =
    ((JsPath \ "login").format[String] and
      (JsPath \ "token").format[String] and
      (JsPath \ "TTL").format[Long])(
      AuthenticationSuccess.apply,
      c => (c.login, c.token, c.TTL)
    )
}

case class AuthenticationSuccess(login: String, token: String, TTL: Long)
    extends AuthenticationResult {
  def toResult: Result = {
    log.debug("Successful authentication")
    Results.Ok(Json.toJson(this))
  }
}

object AuthenticationError extends Enumeration {
  type AuthenticationError = Value
  val EmptyLogin: auth.AuthenticationError.Value                        = Value
  val Forbidden: auth.AuthenticationError.Value                         = Value
  val UserNotFound: auth.AuthenticationError.Value                      = Value
  val InvalidCredentials: auth.AuthenticationError.Value                = Value
  val InvalidToken: auth.AuthenticationError.Value                      = Value
  val InvalidJson: auth.AuthenticationError.Value                       = Value
  val BearerNotFound: auth.AuthenticationError.Value                    = Value
  val AuthorizationHeaderNotFound: auth.AuthenticationError.Value       = Value
  val TokenExpired: auth.AuthenticationError.Value                      = Value
  val UnhandledError: auth.AuthenticationError.Value                    = Value
  val SsoAuthenticationFailed: auth.AuthenticationError.Value           = Value
  val CasServerUrlNotSet: auth.AuthenticationError.Value                = Value
  val CasServerInvalidResponse: auth.AuthenticationError.Value          = Value
  val CasServerInvalidParameter: auth.AuthenticationError.Value         = Value
  val CasServerInvalidRequest: auth.AuthenticationError.Value           = Value
  val CasServerInvalidTicketSpec: auth.AuthenticationError.Value        = Value
  val CasServerUnauthorizedServiceProxy: auth.AuthenticationError.Value = Value
  val CasServerInvalidProxyCallback: auth.AuthenticationError.Value     = Value
  val CasServerInvalidTicket: auth.AuthenticationError.Value            = Value
  val CasServerInvalidService: auth.AuthenticationError.Value           = Value
  val CasServerInternalError: auth.AuthenticationError.Value            = Value
  val OidcNotEnabled: auth.AuthenticationError.Value                    = Value
  val OidcInvalidParameter: auth.AuthenticationError.Value              = Value
  val OidcAuthenticationFailed: auth.AuthenticationError.Value          = Value
  val WrongMobileSetup: auth.AuthenticationError.Value                  = Value
  val WrongRequester: auth.AuthenticationError.Value                    = Value

  implicit val format: Format[AuthenticationError] =
    new Format[AuthenticationError] {
      def writes(o: AuthenticationError): JsValue = JsString(o.toString)
      def reads(json: JsValue): JsResult[AuthenticationError] =
        JsSuccess(AuthenticationError.withName(json.as[String]))
    }
}

class AuthenticationException(
    val error: AuthenticationError.AuthenticationError,
    message: String
) extends Exception(message)

object AuthenticationFailure {
  implicit val format: Format[AuthenticationFailure] =
    ((JsPath \ "error").format[AuthenticationError.AuthenticationError] and
      (JsPath \ "message")
        .format[String])(AuthenticationFailure.apply, c => (c.error, c.message))

  def from(t: Throwable): AuthenticationFailure =
    t match {
      case e: AuthenticationException =>
        AuthenticationFailure(e.error, e.getMessage)
      case _ =>
        AuthenticationFailure(AuthenticationError.UnhandledError, t.getMessage)
    }
}

case class AuthenticationFailure(
    error: AuthenticationError.AuthenticationError,
    message: String
) extends AuthenticationResult {
  import AuthenticationError._
  def toResult: Result = {
    log.warn(s"Failed authentication: $this")
    val body = Json.toJson(this)
    error match {
      case BearerNotFound | AuthorizationHeaderNotFound | InvalidJson |
          EmptyLogin | OidcInvalidParameter =>
        Results.BadRequest(body)
      case UnhandledError => Results.InternalServerError(body)
      case SsoAuthenticationFailed =>
        Results
          .Unauthorized(body)
          .withHeaders(Http.HeaderNames.WWW_AUTHENTICATE -> "Negotiate")
      case InvalidToken | InvalidCredentials => Results.Unauthorized(body)
      case _                                 => Results.Forbidden(body)
    }
  }
}

case class AuthenticationInformation(
    login: String,
    expiresAt: Long,
    issuedAt: Long,
    userType: String,
    acls: List[String],
    softwareType: Option[SoftwareType]
) {
  def refresh(expires: Long, acls: List[String]): AuthenticationInformation = {
    val now = AuthenticatedAction.now()
    this.copy(expiresAt = now + expires, issuedAt = now, acls = acls)
  }
  def encode(secret: String): String =
    AuthenticationInformation.encode(this, secret)
  def validate: Try[AuthenticationInformation] =
    if (expiresAt > AuthenticatedAction.now())
      Success(this)
    else
      Failure(
        new AuthenticationException(
          AuthenticationError.TokenExpired,
          "Token expired"
        )
      )

}

object AuthenticationInformation extends JwtJsonImplicits {
  implicit val format: Format[AuthenticationInformation] =
    ((JsPath \ "login").format[String] and
      (JsPath \ "expiresAt").format[Long] and
      (JsPath \ "issuedAt").format[Long] and
      (JsPath \ "userType").format[String] and
      (JsPath \ "acls").format[List[String]] and
      (JsPath \ "softwareType").formatNullable[SoftwareType])(
      AuthenticationInformation.apply,
      c =>
        (c.login, c.expiresAt, c.issuedAt, c.userType, c.acls, c.softwareType)
    )

  def encode(
      info: AuthenticationInformation,
      secret: String
  ): String =
    JwtJson.encode(Json.toJson(info).as[JsObject], secret, JwtAlgorithm.HS256)
  def decode(
      token: String,
      secret: String
  ): Try[AuthenticationInformation] =
    JwtJson
      .decodeJson(token, secret, Seq(JwtAlgorithm.HS256))
      .map(_.as[AuthenticationInformation])
      .recoverWith({ case t =>
        Failure(
          new AuthenticationException(
            AuthenticationError.InvalidToken,
            t.getMessage
          )
        )
      })

  def encodeFromLogin(
      login: String,
      secret: String,
      expires: Long,
      softwareType: Option[SoftwareType],
      acls: List[String]
  ): String = {
    val now = AuthenticatedAction.now()
    encode(
      AuthenticationInformation(
        login,
        now + expires,
        now,
        AuthenticatedAction.ctiUserType,
        AuthenticatedAction.getAliasesFromAcls(acls),
        softwareType
      ),
      secret
    )
  }
}
