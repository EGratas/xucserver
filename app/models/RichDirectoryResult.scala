package models

import models.RichDirectoryEntries.toJson
import org.xivo.cti.model.PhoneHintStatus
import services.video.model.VideoEvents
import play.api.libs.json._
import play.api.libs.functional.syntax._

import scala.collection.mutable.Buffer

case class RichEntry(
    status: PhoneHintStatus,
    fields: RichEntry.Fields,
    videoStatus: Option[VideoEvents.Event],
    contactId: Option[String] = None,
    source: Option[String] = None,
    favorite: Option[Boolean] = None,
    username: Option[String] = None,
    url: Option[String] = None,
    personal: Boolean = false
)
object RichEntry {
  type Fields = Buffer[Any]
  implicit val fieldsWrites: Writes[Fields] = new Writes[Fields] {
    def writes(fields: Fields): JsValue = {
      val jsvalues = fields.map {
        case s: String  => JsString(s)
        case b: Boolean => JsBoolean(b)
        case _          => JsNull
      }
      JsArray(jsvalues)
    }
  }

  implicit val richEntryWrites: OWrites[RichEntry] = (
    (__ \ "status").write[Int].contramap { (a: PhoneHintStatus) =>
      a.getHintStatus
    } and
      (__ \ "entry").write[Fields] and
      (__ \ "videoStatus").writeNullable[VideoEvents.Event] and
      (__ \ "contact_id").writeNullable[String] and
      (__ \ "source").writeNullable[String] and
      (__ \ "favorite").writeNullable[Boolean] and
      (__ \ "username").writeNullable[String] and
      (__ \ "url").writeNullable[String] and
      (__ \ "personal").write[Boolean]
  )(re =>
    (
      re.status,
      re.fields,
      re.videoStatus,
      re.contactId,
      re.source,
      re.favorite,
      re.username,
      re.url,
      re.personal
    )
  )

}

class RichDirectoryEntries(val headers: List[String]) {
  var entries: List[RichEntry]      = List()
  def getEntries(): List[RichEntry] = entries
  def add(entry: RichEntry): Unit = {
    entries = entries :+ entry
  }
}

object RichDirectoryEntries {
  implicit val rdrWrites: Writes[RichDirectoryEntries] =
    new Writes[RichDirectoryEntries] {
      def writes(rdr: RichDirectoryEntries): JsValue =
        JsObject(
          Seq(
            "headers" -> Json.toJson(rdr.headers),
            "entries" -> Json.toJson(rdr.entries)
          )
        )
    }

  def toJson(rdResult: RichDirectoryEntries): JsValue = Json.toJson(rdResult)

  val defaultHeaders: List[String] = List(
    "name",
    "number",
    "mobile",
    "external_number",
    "favorite",
    "email"
  )

}

class RichDirectoryResult(headers: List[String])
    extends RichDirectoryEntries(headers)
class RichFavorites(headers: List[String]) extends RichDirectoryEntries(headers)

object RichDirectoryResult {
  implicit val richDirectoryResultsWrites: Writes[RichDirectoryResult] =
    new Writes[RichDirectoryResult] {
      def writes(richDirectoryResults: RichDirectoryResult): JsValue =
        toJson(richDirectoryResults)
    }
}

object RichFavorites {
  implicit val richFavoritesWrites: Writes[RichFavorites] =
    new Writes[RichFavorites] {
      def writes(richFavorites: RichFavorites): JsValue = toJson(richFavorites)
    }
}
