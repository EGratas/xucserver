package models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, JsValue, Json, OFormat, OWrites, Writes}

sealed trait XucConfiguration

case class ConfigurationEntry(name: String, value: String)
    extends XucConfiguration
case class ConfigurationIntSetEntry(name: String, value: Set[Int])
    extends XucConfiguration

object ConfigurationEntry {
  implicit val format: OFormat[ConfigurationEntry] =
    (
      (JsPath \ "name").format[String] and
        (JsPath \ "value").format[String]
    )(ConfigurationEntry.apply, o => (o.name, o.value))
}

object ConfigurationIntSetEntry {
  implicit val format: OWrites[ConfigurationIntSetEntry] =
    (
      (JsPath \ "name").write[String] and
        (JsPath \ "value").write[Set[Int]]
    )(o => (o.name, o.value))
}

object XucConfiguration {
  implicit val writes: Writes[XucConfiguration] = new Writes[XucConfiguration] {
    override def writes(conf: XucConfiguration): JsValue = {
      val (_, data) = conf match {
        case c: ConfigurationEntry =>
          (c, Json.toJson(c)(ConfigurationEntry.format))
        case c: ConfigurationIntSetEntry =>
          (c, Json.toJson(c)(ConfigurationIntSetEntry.format))
      }
      data
    }
  }
}
