package models

import models.ws.{ErrorResult, ErrorType}
import play.api.libs.json.*
import play.api.mvc.{Result, Results}
import play.api.libs.functional.syntax.toFunctionalBuilderOps

case class MobileAppPushToken(token: Option[String])

object MobileAppPushToken {
  val reads: Reads[MobileAppPushToken] =
    (__ \ "token").readNullable[String].map { o => MobileAppPushToken(o) }
  val writes: Writes[MobileAppPushToken] =
    (__ \ "token").writeNullable[String].contramap { (o) => o.token }
  implicit val format: Format[MobileAppPushToken] =
    Format(reads, writes)

}

case class MobileAppError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case _ => Results.BadRequest(body)
    }
  }
}
object MobileAppError {
  implicit val mobileAppErrorFormat: Writes[MobileAppError] =
    (
      (JsPath \ "error").write[ErrorType] and
        (JsPath \ "message").write[String]
    )(o => (o.error, o.message))
}
