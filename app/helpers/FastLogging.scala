package helpers

import org.apache.pekko.actor.Actor
import org.apache.pekko.event.LoggingAdapter

trait FastLogging { this: Actor =>
  private var _fastLog: FastLoggingAdapter = _

  def log: FastLoggingAdapter = {
    // only used in Actor, i.e. thread safe
    if (_fastLog eq null)
      _fastLog = new FastLoggingAdapter(
        org.apache.pekko.event.Logging(context.system, this)
      )
    _fastLog
  }
}

class FastLoggingAdapter(log: LoggingAdapter) {
  def debug(message: => String): Unit = {
    if (log.isDebugEnabled) log.debug(message)
  }
  def debug(message: => String, arg1: => Any): Unit = {
    if (log.isDebugEnabled) log.debug(message, arg1)
  }
  def debug(message: => String, arg1: => Any, arg2: => Any): Unit = {
    if (log.isDebugEnabled) log.debug(message, arg1, arg2)
  }
  def debug(
      message: => String,
      arg1: => Any,
      arg2: => Any,
      arg3: => Any
  ): Unit = { if (log.isDebugEnabled) log.debug(message, arg1, arg2, arg3) }

  def warning(message: => String): Unit = {
    if (log.isWarningEnabled) log.warning(message)
  }
  def warning(message: => String, arg1: => Any): Unit = {
    if (log.isWarningEnabled) log.warning(message, arg1)
  }
  def warning(message: => String, arg1: => Any, arg2: => Any): Unit = {
    if (log.isWarningEnabled) log.warning(message, arg1, arg2)
  }

  def error(cause: Throwable, message: String): Unit = log.error(cause, message)

  def info(message: => String): Unit               = log.info(message)
  def info(message: => String, arg1: => Any): Unit = log.info(message, arg1)
  def info(message: => String, arg1: => Any, arg2: => Any): Unit =
    log.info(message, arg1, arg2)
  def info(message: => String, arg1: => Any, arg2: => Any, arg3: => Any): Unit =
    log.info(message, arg1, arg3)
}
