package helpers

import scala.util.{Failure, Success, Try}

object OptionToTry {
  implicit class OptionToTry[T](val option: Option[T]) extends AnyVal {
    def toTry(exception: => Throwable = new NoSuchElementException()): Try[T] =
      option match {
        case Some(a) => Success(a)
        case None    => Failure(exception)
      }

    def toEither[L](leftValue: => L): Either[L, T] =
      option match {
        case Some(a) => Right(a)
        case None    => Left(leftValue)
      }
  }

}
