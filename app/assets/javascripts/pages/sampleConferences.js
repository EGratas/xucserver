function conferencesHandler(conferences) {
  console.log("conferences Handler " + JSON.stringify(conferences));
  $('#conferences').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(conferences) + '</code></pre></li>');
}
$('#xuc_clean_conferencesEvent').click(function(event) {
  $('#conferencesEvents').empty();
});
function ConferenceEventsHandler(conferenceEvent) {
  console.log("conference Event Handler " + JSON.stringify(conferenceEvent));
  $('#conferencesEvents').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(conferenceEvent) + '</code></pre></li>');
}

$('#xuc_clean_conferences').click(function(event) {
  $('#conferences').empty();
});
$('#xuc_get_config_conferences').click(function(event) {
  Cti.getConferenceRooms();
});
$('#xuc_conference_muteme').click(function(event) {
  Cti.conferenceMuteMe($("#xuc_numconf").val());
});
$('#xuc_conference_unmuteme').click(function(event) {
  Cti.conferenceUnmuteMe($("#xuc_numconf").val());
});
$('#xuc_conference_muteall').click(function(event) {
  Cti.conferenceMuteAll($("#xuc_numconf").val());
});
$('#xuc_conference_unmuteall').click(function(event) {
  Cti.conferenceUnmuteAll($("#xuc_numconf").val());
});
$('#xuc_conference_mute').click(function(event) {
  Cti.conferenceMute($("#xuc_numconf").val(), parseInt($("#xuc_conf_index").val()));
});
$('#xuc_conference_unmute').click(function(event) {
  Cti.conferenceUnmute($("#xuc_numconf").val(), parseInt($("#xuc_conf_index").val()));
});
$('#xuc_conference_kick').click(function(event) {
  Cti.conferenceKick($("#xuc_numconf").val(), parseInt($("#xuc_conf_index").val()));
});
$('#xuc_conference_close').click(function(event) {
  Cti.conferenceClose($("#xuc_numconf").val());
});
$('#xuc_conference_deafen').click(function(event) {
  Cti.conferenceDeafen($("#xuc_numconf").val(), parseInt($("#xuc_conf_index").val()));
});
$('#xuc_conference_undeafen').click(function(event) {
  Cti.conferenceUndeafen($("#xuc_numconf").val(), parseInt($("#xuc_conf_index").val()));
});
$('#xuc_conference_invite').click(function(event) {
  var numConf = $("#xuc_numconf").val();
  var exten = $("#xuc_conf_exten").val();
  var role = $("input[name='xuc_conf_role']:checked").val();
  var earlyJoin = $("#xuc_conf_earlyjoin").prop( "checked" );
  var variables = extractVariables($("#xuc_conf_variables").val());
  Cti.conferenceInvite(numConf, exten, role, earlyJoin, variables);
});

function initConferences() {
  Cti.setHandler(Cti.MessageType.CONFERENCES, conferencesHandler);
  Cti.setHandler(Cti.MessageType.CONFERENCEEVENT, ConferenceEventsHandler);
  Cti.setHandler(Cti.MessageType.CONFERENCEPARTICIPANTEVENT, ConferenceEventsHandler);
  Cti.setHandler(Cti.MessageType.CONFERENCECOMMANDERROR, ConferenceEventsHandler);
}
