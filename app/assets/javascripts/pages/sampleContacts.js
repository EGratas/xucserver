function directoryResultHandler(directoryResult) {
  console.log("Directory result" + JSON.stringify(directoryResult));
  DirectoryDisplay.prepareTable();
  DirectoryDisplay.displayHeaders(directoryResult.headers);
  DirectoryDisplay.displayEntries(directoryResult.entries);
  $('#raw_directory_result').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(directoryResult) + '</pre></code></li>');
}

function favoriteUpdatedHandler(updateResult) {
  console.log("FavoriteUpdated: " + JSON.stringify(updateResult));
  $('#favorite_update_result').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(updateResult) + '</pre></code></li>');
}

function resetDisplay() {
  $('#directoryresult').empty();
  $('#raw_directory_result').empty();
}

$('#xuc_lookup_btn').click(function(event) {
    Cti.directoryLookUp($("#xuc_directory").val());
});
$('#xuc_directory').keypress(function(e) {
    if (e.which == $.ui.keyCode.ENTER) {
        Cti.directoryLookUp($("#xuc_directory").val());
    }
});
$('#xuc_favorites_btn').click(function(event) {
    Cti.getFavorites();
});
$('#xuc_personal_list_btn').click(function(event) {
   makeAuthCall('GET', "/xuc/api/2.0/contact/display/personal").success(function(data){
       directoryResultHandler(data);
     }).fail(function(sender, message, details){
       generateMessage("Impossible to retrieve personal contacts for this user", true);
   });
});
$('#xuc_personal_export_btn').click(function(event) {
   event.originalEvent.currentTarget.href = '/xuc/api/2.0/contact/export/personal?token='+$('#xuc_token').val();
});
$('#xuc_personal_import_btn').on('change', function() {
  makeAuthCsvUpload('/xuc/api/2.0/contact/import/personal',
  $('#xuc_personal_import_btn')[0].files[0]).success(function(data){
      generateMessage(data);
    }).fail(function(response, message, details){
      generateMessage(extractErrorMessageFromResponse(response), true);
  });
});
$('#xuc_personal_del_all_btn').click(function(event) {
   makeAuthCall('DELETE', "/xuc/api/2.0/contact/personal").success(function(){
        generateMessage('All personal contacts have been deleted successfully');
      }).fail(function(response, message, details){
        generateMessage(extractErrorMessageFromResponse(response), true);
   });
});
$('#xuc_personal_get_btn').click(function(event) {
   makeAuthCall('GET', "/xuc/api/2.0/contact/personal/"+$("#xuc_personal_contact_id").val()).success(function(data){
      generateMessage('Personal contact '+$("#xuc_personal_contact_id").val()+' has been retrieved successfully');
         $("#xuc_personal_firstname").val(data.firstname);
         $("#xuc_personal_lastname").val(data.lastname);
         $("#xuc_personal_number").val(data.number);
         $("#xuc_personal_mobile").val(data.mobile);
         $("#xuc_personal_fax").val(data.fax);
         $("#xuc_personal_email").val(data.email);
         $("#xuc_personal_company").val(data.company);
      }).fail(function(response, message, details){
        generateMessage(extractErrorMessageFromResponse(response), true);
   });
});
$('#xuc_personal_del_btn').click(function(event) {
   makeAuthCall('DELETE', "/xuc/api/2.0/contact/personal/"+$("#xuc_personal_contact_id").val()).success(function(){
        generateMessage('Personal contact '+$("#xuc_personal_contact_id").val()+' has been deleted successfully');
      }).fail(function(response, message, details){
        generateMessage(extractErrorMessageFromResponse(response), true);
   });
});
$('#xuc_personal_add_btn').click(function(event) {
   var contact = {
     "firstname": $("#xuc_personal_firstname").val(),
     "lastname": $("#xuc_personal_lastname").val(),
     "number": $("#xuc_personal_number").val(),
     "mobile": $("#xuc_personal_mobile").val(),
     "fax": $("#xuc_personal_fax").val(),
     "email": $("#xuc_personal_email").val(),
     "company": $("#xuc_personal_company").val()
   };
   makeAuthCall('POST', "/xuc/api/2.0/contact/personal",contact).success(function(data){
       generateMessage('Personal contact has been created successfully');
     }).fail(function(response, message, details){
       generateMessage(extractErrorMessageFromResponse(response), true);
   });
});
$('#xuc_personal_edit_btn').click(function(event) {
   var contact = {
     "firstname": $("#xuc_personal_firstname").val(),
     "lastname": $("#xuc_personal_lastname").val(),
     "number": $("#xuc_personal_number").val(),
     "mobile": $("#xuc_personal_mobile").val(),
     "fax": $("#xuc_personal_fax").val(),
     "email": $("#xuc_personal_email").val(),
     "company": $("#xuc_personal_company").val()
   };
   makeAuthCall('PUT', "/xuc/api/2.0/contact/personal/"+$("#xuc_personal_contact_id").val(),contact).success(function(data){
       generateMessage('Personal contact '+$("#xuc_personal_contact_id").val()+' has been updated successfully');
     }).fail(function(response, message, details){
       generateMessage(extractErrorMessageFromResponse(response), true);
   });
});
$('#xuc_fav_add_btn').click(function(event) {
    Cti.addFavorite($("#xuc_fav_contact_id").val(), $("#xuc_fav_source").val());
});
$('#xuc_fav_rm_btn').click(function(event) {
    Cti.removeFavorite($("#xuc_fav_contact_id").val(), $("#xuc_fav_source").val());
});
$('#xuc_clean_directory').click(function (){
    resetDisplay();
});
$('#xuc_clean_favorite').click(function (){
    resetDisplay();
});
$('#xuc_clean_personal').click(function (){
    resetDisplay();
});

function initContacts() {
  Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, directoryResultHandler);
  Cti.setHandler(Cti.MessageType.FAVORITES, directoryResultHandler);
  Cti.setHandler(Cti.MessageType.FAVORITEUPDATED, favoriteUpdatedHandler);

  DirectoryDisplay.init("#directoryresult");
}



