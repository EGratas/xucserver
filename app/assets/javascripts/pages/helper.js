var DirectoryDisplay = {

    init : function(tableName) {
        this.tableName = tableName;
    },

    prepareTable : function() {
        $(this.tableName).empty();
        $(this.tableName).append('<table class="table"/>' );
    },

    displayHeaders : function(headers) {
        var tableElement = this.tableName + " table";
        $(tableElement).append( '<tr>');
        $(tableElement+ ' tr').append( '<th></th>' );
        $(tableElement+ ' tr').append( '<th>Contact id</th>' );
        for(var i=0 ; i < headers.length; i++){
           $(tableElement+ ' tr').append( '<th>' + headers[i] + '</th>' );
        }
        $(tableElement+ ' tr').append( '<th>Username</th>' );
        $(tableElement).append( '</tr>');
        this.length = headers.length;
    },
    displayEntries : function(entries) {
        var tableElement = this.tableName + " table";
        for(var i=0; i < entries.length;i++){
            var entry = '<tr><td bgcolor="'+Cti.PhoneStatusColors[entries[i].status]+'"><span class="glyphicon glyphicon-earphone glyph-white"></span></td>';
            entry += '<td>'+entries[i].contact_id+'</td>';
            for ( var j = 0; j < entries[i].entry.length; j++) {
                var currentField = entries[i].entry[j];
                entry = entry + '<td >' +  this.formatField(currentField) + '</td>';
            }
            for(; j < this.length; j++) {
                entry += '<td/>';
            }
            entry += '<td>'+entries[i].username+'</td>';
            entry = entry + '</tr>';
            $(tableElement).append(entry);
        }
    },

    formatField : function(field) {
        if ($.isNumeric(field)) {
            return '<a href="#">'+field+'</a>';
        }
        else {
            return field;
        }
    }
};

function extractVariables(str) {
    var res = {};
    if(str) {
        var keyValues = str.split('&');
        for(var i=0; i<keyValues.length; i++) {
            var kv = keyValues[i].split('=');
            if(kv.length == 2) {
                var k = kv[0];
                var v = kv[1];
                res[k] = v;
            } else {
                console.log('Invalid key value pair');
                console.log(kv);
            }
        }
    }
    return res;
}

function makeAuthCall(type, url, data) {

    var req = {
        type: type,
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", 'Bearer ' + $('#xuc_token').val());
            },
        url: url
        };

    if (data) {
        req.data = JSON.stringify(data);
        req.dataType = 'json';
        req.contentType = 'application/json;charset=utf-8';
    }
    return $.ajax(req);
}

function makeAuthCsvUpload(url, csv_data) {
    return $.ajax({
        type : "POST",
        beforeSend: function(request) {
          request.setRequestHeader("Authorization", 'Bearer ' + $('#xuc_token').val());
        },
        contentType: "text/plain;charset=utf-8",
        data: csv_data,
        url : url,
        processData: false
    });
}

function extractErrorMessageFromResponse(response) {
    if (response.getResponseHeader('content-type').startsWith('application/json')) {
      return response.responseJSON.message + '(' + response.responseJSON.error + ')';
    } else {
      return response.status + ' ' + response.statusText;
    }
}
