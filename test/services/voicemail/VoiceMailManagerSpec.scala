package services.voicemail

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestKit, TestProbe}
import pekkotest.TestKitSpec
import models.XivoUser
import org.asteriskjava.manager.event.{
  MessageWaitingEvent,
  VoicemailUserDetailEvent
}
import org.mockito.Mockito.{spy, timeout, verify}
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.ArgumentMatchers.any
import services.XucAmiBus
import services.XucAmiBus.{AmiAction, AmiEvent, AmiType}
import xivo.models.{VoiceMail, VoiceMailFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Try}

class VoiceMailManagerSpec
    extends TestKitSpec("VoiceMailManagerSpec")
    with MockitoSugar {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class DummyVoiceMailFactory extends VoiceMailFactory {
    override def all(): Future[List[VoiceMail]] = Future.successful(List.empty)
    override def get(username: String): Future[Option[VoiceMail]] =
      Future.successful(getUserVoiceMail(username))

    def getUserVoiceMail(username: String): Option[VoiceMail] =
      if (username == "nomailbox") None
      else Some(VoiceMail(4, s"john@xivo", "john", "xivo"))
  }

  class Helper {
    val ctiRouter: TestProbe        = TestProbe()
    val anotherCtiRouter: TestProbe = TestProbe()
    val xucAmiBus: XucAmiBus        = mock[XucAmiBus]
    val voiceMailFactory: DummyVoiceMailFactory = spy(new DummyVoiceMailFactory)

    val xivoUserWithVoiceMail: XivoUser =
      XivoUser(1, None, None, "John", Some("Doe"), Some("john"), None, None, None)
    val xivoUserWithSameVoiceMail: XivoUser =
      XivoUser(
        2,
        None,
        None,
        "John",
        Some("Almost-Doe"),
        Some("nhoj"),
        None,
        None,
        None
      )
    val xivoUserWithoutVoiceMail: XivoUser =
      XivoUser(
        10,
        None,
        None,
        "Alice",
        Some("Sample"),
        Some("nomailbox"),
        None,
        None,
        None
      )

    def actor(
        mockUsers: Map[VoiceMailManager.UserName, VoiceMail] = Map.empty,
        mockVoiceMails: Map[VoiceMailManager.VoiceMailName, Set[VoiceMailRef]] =
          Map.empty
    ): (TestActorRef[VoiceMailManager], VoiceMailManager) = {

      val a = TestActorRef[VoiceMailManager](
        Props(new VoiceMailManager(xucAmiBus, voiceMailFactory) {
          users = mockUsers
          voiceMails = mockVoiceMails
        })
      )
      (a, a.underlyingActor)
    }
  }

  "VoiceMailManager" should {
    "subscribe to AmiEvent" in new Helper {

      val (ref, _) = actor()
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiEvent)
    }

    "subscribe a user to its voicemail and add it if doesn't exist yet" in new Helper {
      var (ref, a) = actor()
      a.voiceMails.size should be(0)

      ref ! SubscribeToVoiceMail(xivoUserWithVoiceMail)
      Await.result(
        a.getAndAddVoiceMail(xivoUserWithVoiceMail, ctiRouter.ref),
        3.seconds
      ) should be(
        AddVoiceMail(
          xivoUserWithVoiceMail,
          VoiceMail(4, "john@xivo", "john", "xivo")
        )
      )
      a.voiceMails.size should be(1)
      a.voiceMails("john@xivo") shouldBe an[Set[VoiceMailRef]]
    }

    "subscribe multiple users to one voicemail" in new Helper {
      var (ref, a) = actor()

      Await.result(
        a.getAndAddVoiceMail(xivoUserWithVoiceMail, ctiRouter.ref),
        3.seconds
      ) should be(
        AddVoiceMail(
          xivoUserWithVoiceMail,
          VoiceMail(4, "john@xivo", "john", "xivo")
        )
      )
      Await.result(
        a.getAndAddVoiceMail(xivoUserWithSameVoiceMail, anotherCtiRouter.ref),
        3.seconds
      ) should be(
        AddVoiceMail(
          xivoUserWithSameVoiceMail,
          VoiceMail(4, "john@xivo", "john", "xivo")
        )
      )
      a.voiceMails.size should be(1)
      a.voiceMails("john@xivo").size should be(2)
    }

    "do not subscribe a user if its voicemail does not exists" in new Helper {
      var (ref, a) = actor()
      a.voiceMails.size should be(0)

      ref ! SubscribeToVoiceMail(xivoUserWithoutVoiceMail)
      Try(
        Await.result(
          a.getAndAddVoiceMail(xivoUserWithoutVoiceMail, ctiRouter.ref),
          3.seconds
        )
      ) shouldBe an[Failure[_]]
      a.voiceMails.size should be(0)
    }

    "add a subscription to users voicemail repo and ask voicemail status" in new Helper {
      var (ref, a) = actor()
      ref ! AddVoiceMail(
        xivoUserWithVoiceMail,
        VoiceMail(4, "john@xivo", "john", "xivo")
      )

      a.users("john") should be(VoiceMail(4, "john@xivo", "john", "xivo"))
      verify(xucAmiBus, timeout(500)).publish(any[AmiAction]())
    }

    "add multiple subscription to same voicemail" in new Helper {
      var (ref, a) = actor()
      ref ! AddVoiceMail(
        xivoUserWithVoiceMail,
        VoiceMail(4, "john@xivo", "john", "xivo")
      )
      ref ! AddVoiceMail(
        xivoUserWithSameVoiceMail,
        VoiceMail(4, "john@xivo", "john", "xivo")
      )

      a.users("john") should be(VoiceMail(4, "john@xivo", "john", "xivo"))
      a.users("nhoj") should be(VoiceMail(4, "john@xivo", "john", "xivo"))
    }

    "unsubscribe a user to its voicemail" in new Helper {
      val v: Map[String, Set[VoiceMailRef]] = Map("john@xivo" -> Set(VoiceMailRef(None, xivoUserWithVoiceMail)))
      val u: Map[String, VoiceMail] = Map("john" -> VoiceMail(4, "john@xivo", "john", "xivo"))
      var (ref, a) = actor(u, v)

      a.users.size should be(1)
      a.voiceMails("john@xivo").size should be(1)
      ref ! UnsubscribeFromVoiceMail(xivoUserWithVoiceMail)
      a.users should be(Map.empty)
      a.voiceMails("john@xivo") should be(Set.empty)
    }

    "notify voicemail status when start request" in new Helper {
      val v: Map[String, Set[VoiceMailRef]] = Map(
        "john@xivo" -> Set(
          VoiceMailRef(Some(ctiRouter.ref), xivoUserWithVoiceMail)
        )
      )
      val u: Map[String, VoiceMail] = Map("john" -> VoiceMail(4, "john@xivo", "john", "xivo"))
      val vue = new VoicemailUserDetailEvent("test")
      vue.setVoicemailbox("john")
      vue.setVmContext("xivo")
      vue.setNewMessageCount(1)
      vue.setOldMessageCount(0)

      val event: AmiEvent = AmiEvent(vue, "main")

      var (ref, a) = actor(u, v)
      ref ! event

      ctiRouter.expectMsg(VoiceMailStatusUpdate(4, 1))
    }

    "notify voicemail status when start request only when having new message" in new Helper {
      val v: Map[String, Set[VoiceMailRef]] = Map(
        "john@xivo" -> Set(
          VoiceMailRef(Some(ctiRouter.ref), xivoUserWithVoiceMail)
        )
      )
      val u: Map[String, VoiceMail] = Map("john" -> VoiceMail(4, "john@xivo", "john", "xivo"))
      val vue = new VoicemailUserDetailEvent("test")
      vue.setVoicemailbox("john")
      vue.setVmContext("xivo")
      vue.setNewMessageCount(0)
      vue.setOldMessageCount(1)

      val event: AmiEvent = AmiEvent(vue, "main")

      var (ref, a) = actor(u, v)
      ref ! event

      ctiRouter.expectNoMessage()
    }

    "forward voicemail status when received from AMI" in new Helper {
      val v: Map[String, Set[VoiceMailRef]] = Map(
        "john@xivo" -> Set(
          VoiceMailRef(Some(ctiRouter.ref), xivoUserWithVoiceMail),
          VoiceMailRef(Some(anotherCtiRouter.ref), xivoUserWithSameVoiceMail)
        )
      )
      val u: Map[String, VoiceMail] = Map(
        "john" -> VoiceMail(4, "john@xivo", "john", "xivo"),
        "nhoj" -> VoiceMail(4, "john@xivo", "john", "xivo")
      )
      val mwe = new MessageWaitingEvent("test")
      mwe.setMailbox("john@xivo")
      mwe.setNew(1)
      val event: AmiEvent = AmiEvent(mwe, "main")

      var (ref, a) = actor(u, v)
      ref ! event

      ctiRouter.expectMsg(VoiceMailStatusUpdate(4, 1))
      anotherCtiRouter.expectMsg(VoiceMailStatusUpdate(4, 1))
    }

    "not forward voicemail from AMI if not subscribed" in new Helper {
      val v: Map[String, Set[VoiceMailRef]] = Map(
        "john@xivo" -> Set(
          VoiceMailRef(Some(ctiRouter.ref), xivoUserWithVoiceMail)
        )
      )
      val mwe = new MessageWaitingEvent("test")
      mwe.setMailbox("john@xivo")
      val event: AmiEvent = AmiEvent(mwe, "main")

      var (ref, a) = actor(mockVoiceMails = v)
      ref ! event

      ctiRouter.expectNoMessage()
    }
  }
}
