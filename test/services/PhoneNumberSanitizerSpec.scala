package services

import pekkotest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar

class PhoneNumberSanitizerSpec
    extends TestKitSpec("PhoneNumberSanitizerSpec")
    with MockitoSugar {

  "a phone number sanitizer" should {

    "remove whitespaces" in {
      val phoneNumber = "01 02  03"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("010203")
    }

    "remove dots" in {
      val phoneNumber = ".01.02..03."

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("010203")
    }

    "remove dashes" in {
      val phoneNumber = "-01-02--03-"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("010203")
    }

    "remove slashes" in {
      val phoneNumber = "/01/02//03/"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("010203")
    }

    "replace plus-prefixed country code by 00-prefixed code" in {
      val phoneNumber = "+33123456789"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("0033123456789")
    }

    "replace plus-prefixed country code with 0 between parenthesis by 00-prefixed code" in {
      val phoneNumber = "+33(0)123456789"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("0033123456789")
    }

    "replace plus-prefixed country code with leading 0 by 00-prefixed code" in {
      val phoneNumber = "+330123456789"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("00330123456789")
    }

    "handle a number with spaces and a leading zero that we have to keep" in {
      val phoneNumber = "0 687 654 321"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("0687654321")
    }

    "handle a number with spaces and plus-prefixed country code with zero between parentheses" in {
      val phoneNumber = "+33(0) 6 87 65 43 21"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("0033687654321")
    }

    "handle a number with a 3-digits country code" in {
      val phoneNumber = "+353 06 87 50 43 21"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("003530687504321")
    }

    "remove non-digits characters" in {
      val phoneNumber = "01abc02|03"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("010203")
    }

    "keep the leading zero when Italian country code as in ticket #14434" in {
      val phoneNumber = "00390458251111"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("00390458251111")
    }

    "allow star in phone number" in {
      val phoneNumber = "*98"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be("*98")
    }

    "allow dialing sip peer directly" in {
      val phoneNumber = "jpthomasset@xivo.avencall.com"

      PhoneNumberSanitizer.sanitize(phoneNumber) should be(phoneNumber)
    }
  }
}
