package services.callhistory

import org.apache.pekko.actor.{ActorRef, Props}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.{DynamicFilter, OperatorEq}
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.{JsArray, Json}
import services.config.ConfigRepository
import services.request.*
import xivo.models.*
import xivo.network.*

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.joda.time.format.DateTimeFormatter
import org.mockito.Mockito.when

class CallHistoryManagerSpec
    extends TestKitSpec("CallHistoryManagerSpec")
    with MockitoSugar
    with BeforeAndAfterEach {

  val format: DateTimeFormatter                   = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  var requester: TestProbe     = null
  var requesterRef: ActorRef   = null
  var recordingWs: RecordingWS = null
  var historyMgr: ActorRef     = null
  var repo: ConfigRepository   = null

  override def beforeEach(): Unit = {
    requester = TestProbe()
    requesterRef = requester.ref
    recordingWs = mock[RecordingWS]
    repo = mock[ConfigRepository]
    historyMgr = TestActorRef[CallHistoryManager](
      Props(new CallHistoryManager(recordingWs, repo))
    )
  }

  "A CallHistoryManager" should {
    "send agent call history to the requester" in {
      val agent = Agent(12, "John", "Doe", "1002", "default")
      val rq    = BaseRequest(requesterRef, AgentCallHistoryRequest(10, "toto"))
      when(repo.agentFromUsername("toto")).thenReturn(Some(agent))
      val json = JsArray(
        List(
          Json.obj(
            "start"    -> "2014-01-01 08:00:00",
            "duration" -> "00:14:23",
            "agent"    -> "1002",
            "queue"    -> "test_queue",
            "src_num"  -> "3354789",
            "dst_num"  -> "44485864"
          )
        )
      )
      when(recordingWs.getAgentCallHistory(HistorySize(10), "1002"))
        .thenReturn(Future(HistoryServerResponse(json)))

      historyMgr ! rq

      requester.expectMsg(
        CallHistory(
          List(
            CallDetail(
              format.parseDateTime("2014-01-01 08:00:00"),
              Some(new Period(0, 14, 23, 0)),
              "3354789",
              Some("44485864"),
              CallStatus.Answered
            )
          )
        )
      )
    }

    "send user call history to the requester" in {
      val param     = HistorySize(10)
      val rq        = BaseRequest(requesterRef, UserCallHistoryRequest(param, "toto"))
      val interface = "SIP/agbef"
      when(repo.interfaceFromUsername("toto")).thenReturn(Some(interface))
      val json = JsArray(
        List(
          Json.obj(
            "start"    -> "2014-01-01 08:00:00",
            "duration" -> "00:14:23",
            "status"   -> "emitted",
            "src_num"  -> "3354789",
            "dst_num"  -> "44485864"
          )
        )
      )
      when(recordingWs.getUserCallHistory(param, interface))
        .thenReturn(Future(HistoryServerResponse(json)))

      historyMgr ! rq

      requester.expectMsg(
        CallHistory(
          List(
            CallDetail(
              format.parseDateTime("2014-01-01 08:00:00"),
              Some(new Period(0, 14, 23, 0)),
              "3354789",
              Some("44485864"),
              CallStatus.Emitted
            )
          )
        )
      )
    }

    "send customer call history to the requester" in {
      val criteria = FindCustomerCallHistoryRequest(
        List(DynamicFilter("src_num", Some(OperatorEq), Some("1234")))
      )
      val rq = BaseRequest(
        requesterRef,
        CustomerCallHistoryRequestWithId(22, criteria)
      )

      var response = FindCustomerCallHistoryResponse(
        1,
        List(
          CustomerCallDetail(
            format.parseDateTime("2014-01-01 08:00:00"),
            Some(new Period(0, 14, 23, 0)),
            Some(new Period(0, 10, 0, 0)),
            Some("James Bond"),
            Some("1000"),
            Some("Services"),
            Some("3000"),
            CallStatus.Answered
          )
        )
      )

      when(recordingWs.findCustomerCallHistory(criteria))
        .thenReturn(Future(response))

      historyMgr ! rq

      requester.expectMsg(CustomerCallHistoryResponseWithId(22, response))
    }

    "send queue call history to the requester" in {
      val queue = "bl"
      val size  = 8
      val rq    = BaseRequest(requesterRef, GetQueueCallHistory(queue, size))

      val json = JsArray(
        List(
          Json.obj(
            "start"    -> "2014-01-01 08:00:00",
            "duration" -> "00:14:23",
            "agent"    -> "1002",
            "queue"    -> "test_queue",
            "src_num"  -> "3354789",
            "dst_num"  -> "44485864"
          )
        )
      )
      when(recordingWs.getQueueCallHistory(queue, size))
        .thenReturn(Future(HistoryServerResponse(json)))

      historyMgr ! rq

      requester.expectMsg(
        CallHistory(
          List(
            CallDetail(
              format.parseDateTime("2014-01-01 08:00:00"),
              Some(new Period(0, 14, 23, 0)),
              "3354789",
              Some("44485864"),
              CallStatus.Answered
            )
          )
        )
      )
    }
  }
}
