package services.callhistory
import org.apache.pekko.actor.{ActorRef, Cancellable, Scheduler}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.joda.time.{DateTime, Period}
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.model.PhoneHintStatus
import services.config.ConfigRepository
import services.request.HistorySize
import services.video.model.VideoEvents
import xivo.directory.PersonalContactRepository.PersonalContacts
import xivo.models._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class CallHistoryEnricherSpec
    extends TestKitSpec("CallHistoryEnricherSpec")
    with MockitoSugar {

  "A CallHistoryEnricher" should {

    "stops when asked" in new Helper {
      val userName                   = "testStop"
      val (ref, callHistoryEnricher) = actor(userName)

      val testProbe: TestProbe = TestProbe()
      testProbe watch ref

      ref ! CallHistoryEnricher.Stop
      testProbe.expectTerminated(ref)
    }

    "check that timeout is scheduled and calls stop" in new Helper {
      val userName                   = "testTimeout"
      val (ref, callHistoryEnricher) = actor(userName)

      val testProbe: TestProbe = TestProbe()
      testProbe watch ref

      verify(callHistoryEnricher.scheduler).scheduleOnce(
        3.seconds,
        ref,
        CallHistoryEnricher.Stop
      )(scala.concurrent.ExecutionContext.Implicits.global, ref)
    }

    "check that timeout is not canceled if not all answers are received" in new Helper {
      val userName                   = "testTimeout"
      val (ref, callHistoryEnricher) = actor(userName)

      ref ! CallHistory(List.empty)

      verify(callHistoryEnricher.timeoutTasks, never()).cancel()
    }

    """cancel timeout, when getting
      | CallHistory from CallHistoryManager
      | getAllEntries from personalContactRepository""".stripMargin in new Helper {
      val userName                   = "testCallHistoryEnricher"
      val (ref, callHistoryEnricher) = actor(userName)

      val testProbe: TestProbe = TestProbe()
      testProbe watch ref

      ref ! CallHistory(List.empty)
      ref ! PersonalContacts(Map.empty)
      verify(callHistoryEnricher.timeoutTasks).cancel()

    }

    """enrich call history then stop, when getting
      | CallHistory from CallHistoryManager
      | getAllEntries from personalContactRepository""".stripMargin in new Helper {
      val userName                   = "testCallHistoryEnricher"
      val (ref, callHistoryEnricher) = actor(userName)

      val testProbe: TestProbe = TestProbe()
      testProbe watch ref

      val num1 = "1000"
      val num2 = "2000"
      val num3 = "3000"

      val date   = new DateTime
      val period: Some[Period] = Some(new Period)

      ref ! CallHistory(
        List(
          // enrichment
          createCallDetail(date, period, num1, num2, outgoing = false),
          createCallDetail(date, period, num1, num2, outgoing = true),
          // no enrichment
          createCallDetail(date, period, num1, num3, outgoing = true),
          createCallDetail(date, period, num3, num1, outgoing = false)
        )
      )
      ref ! PersonalContacts(Map("1000" -> pc1, "2000" -> pc2))

      respondTo.expectMsg(
        RichCallHistory(
          List(
            createRichCallDetail(date, period, num1, num2, outgoing = false),
            createRichCallDetail(date, period, num1, num2, outgoing = true),
            // no enrichment
            createRichCallDetail(date, period, num1, num3, outgoing = true),
            createRichCallDetail(date, period, num3, num1, outgoing = false)
          )
        )
      )

      testProbe.expectTerminated(ref)
    }
  }

  class Helper {
    val callHistoryMgr: TestProbe = TestProbe()
    val respondTo: TestProbe      = TestProbe()
    val pcRepository: TestProbe   = TestProbe()

    val pc1: PersonalContactResult = PersonalContactResult(
      "1",
      Some("first1000"),
      Some("last1000"),
      Some("1000"),
      None,
      None,
      None,
      None
    )
    val pc2: PersonalContactResult = PersonalContactResult(
      "2",
      Some("first2000"),
      Some("last2000"),
      Some("2000"),
      None,
      None,
      None,
      None
    )

    val myScheduler: Scheduler = mock[Scheduler]
    when(
      myScheduler.scheduleOnce(
        any[FiniteDuration],
        any[ActorRef],
        CallHistoryEnricher.Stop
      )(any[ExecutionContext])
    ).thenReturn(new Cancellable {
      override def cancel(): Boolean    = true
      override def isCancelled: Boolean = true
    })

    val configRepositoryMock: ConfigRepository = mock[ConfigRepository]
    val configDispatcherMock: ActorRef = TestProbe().ref

    when(configRepositoryMock.userNameFromPhoneNb(any[String])).thenReturn(Some(""))
    when(configRepositoryMock.getUserVideoStatus(any[String])).thenReturn(Some(VideoEvents.Available))
    when(configRepositoryMock.richPhones).thenReturn(
      Map(
        "4000" -> PhoneHintStatus.BUSY,
        "4001" -> PhoneHintStatus.BUSY
      )
    )
    class Wrapper(username: String)
        extends CallHistoryEnricher(
          callHistoryMgr.ref,
          respondTo.ref,
          pcRepository.ref,
          HistorySize(10),
          username,
          configRepositoryMock,
          configDispatcherMock
        ) {
      override def scheduler: Scheduler      = myScheduler
      override val timeoutTasks: Cancellable = mock[Cancellable]
    }

    def actor(username: String): (TestActorRef[Wrapper], Wrapper) = {
      val a = TestActorRef(new Wrapper(username))
      (a, a.underlyingActor)
    }

    def createCallDetail(
        date: DateTime,
        period: Option[Period],
        srcNum: String,
        dstNum: String,
        outgoing: Boolean
    ): CallDetail = {
      val status = if (outgoing) CallStatus.Emitted else CallStatus.Answered
      CallDetail(
        date,
        period,
        srcNum,
        Some(dstNum),
        status,
        Some("first" + srcNum),
        Some("last" + srcNum),
        Some("first" + dstNum),
        Some("last" + dstNum)
      )
    }
    def createRichCallDetail(
        date: DateTime,
        period: Option[Period],
        srcNum: String,
        dstNum: String,
        outgoing: Boolean
    ): RichCallDetail = {
      val status = if (outgoing) CallStatus.Emitted else CallStatus.Answered
      RichCallDetail(
        date,
        period,
        "",
        "",
        status,
        PhoneHintStatus.UNEXISTING,
        VideoEvents.Available,
        PhoneHintStatus.UNEXISTING,
        VideoEvents.Available,
        srcNum,
        Some(dstNum),
        Some("first" + srcNum),
        Some("last" + srcNum),
        Some("first" + dstNum),
        Some("last" + dstNum)
      )
    }
  }
}
