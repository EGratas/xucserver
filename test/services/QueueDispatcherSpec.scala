package services

import org.apache.pekko.testkit.TestActorRef
import pekkotest.TestKitSpec
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.{verify, when}
import org.scalatestplus.mockito.MockitoSugar
import services.XucAmiBus.AmiAction
import services.config.QueueRepository
import services.request.{DialFromQueue, DialToQueue}
import xivo.models.QueueConfigUpdate
import xivo.xuc.XucBaseConfig
import xivo.xucami.models.Channel

class QueueDispatcherSpec
    extends TestKitSpec("CtiDeviceSpec")
    with MockitoSugar {

  class Helper {
    val queueRepository: QueueRepository = mock[QueueRepository]
    val xucAmiBus: XucAmiBus       = mock[XucAmiBus]
    val config: XucBaseConfig          = mock[XucBaseConfig]

    when(config.xivoHost).thenReturn("xivoHost")

    def actor(): (TestActorRef[QueueDispatcher], QueueDispatcher) = {
      val a = TestActorRef[QueueDispatcher](
        QueueDispatcher.props(queueRepository, xucAmiBus, config)
      )
      (a, a.underlyingActor)
    }
  }

  "QueueDispatcher" should {
    "on DialFromQueue publish AmiAction with Originate message to XucAmiBus" in new Helper() {
      val msg: DialFromQueue = DialFromQueue(
        destination = "123456789",
        queueId = 123,
        callerIdName = "Jane",
        callerIdNumber = "999999",
        variables = Map("foo" -> "bar"),
        "default"
      )
      val qcu: QueueConfigUpdate = QueueConfigUpdate(
        1,
        "q1",
        "Queue One",
        "333",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      when(queueRepository.getQueue(msg.queueId)).thenReturn(Some(qcu))

      val (ref, _) = actor()
      ref ! msg

      val arg: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(xucAmiBus).publish(arg.capture)
      arg.getValue.message.getAction shouldEqual Channel.callTypeValOriginate
      arg.getValue.targetMds shouldEqual Some("default")
    }

    "on DialToQueue publish AmiAction with Originate message to XucAmiBus" in new Helper() {
      val msg: DialToQueue = DialToQueue(
        destination = "0123456789",
        queueName = "std_notaire",
        callerIdNumber = "0472727272",
        variables = Map("varname1" -> "varval1", "varname2" -> "varval2")
      )
      val qcu: QueueConfigUpdate = QueueConfigUpdate(
        1,
        "q1",
        "Queue One",
        "333",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      when(queueRepository.getQueue(msg.queueName)).thenReturn(Some(qcu))

      val (ref, _) = actor()
      ref ! msg

      val arg: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(xucAmiBus).publish(arg.capture)
      arg.getValue.message.getAction shouldEqual Channel.callTypeValOriginate
      arg.getValue.targetMds shouldEqual Some("default")
    }
  }

}
