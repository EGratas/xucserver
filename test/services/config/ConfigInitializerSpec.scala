package services.config

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import command.{AgentInitLoggedIn, AgentInitLoggedOut}
import models.{WebServiceUserDao, XivoUser, XivoUserDao}
import org.joda.time.DateTime
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.{never, verify, when}
import org.mockito.{Mockito}
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.request.UsersAdded
import org.xivo.cti.message.{AgentConfigUpdate as _, QueueConfigUpdate as _, *}
import services.XucEventBus.XucEvent
import services.config.ObjectType
import services.config.ConfigInitializer.{LoadAgentQueueMembers, UpdateAgent, UpdateQueue}
import services.config.ConfigManager.InitConfig
import services.config.ConfigServiceManager.{GetAgentConfigAll, GetIceServer, GetQueueConfigAll}
import services.{XucAmiBus, XucEventBus}
import xivo.events.AgentState.AgentReady
import xivo.models.*
import xivo.xuc.XucBaseConfig
import org.mockito.ArgumentMatchers.{eq => mockitoEq}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.*
import models.WebServiceUser
import org.json.JSONObject

class ConfigInitializerSpec
    extends TestKitSpec("ConfigInitializerSpec")
    with MockitoSugar
    with ScalaFutures
    with GuiceOneAppPerSuite {

  class Helper {
    val configRepository: ConfigRepository        = mock[ConfigRepository]
    val lineManager: TestProbe             = TestProbe()
    val eventBus: XucEventBus                = mock[XucEventBus]
    val defaultMembershipRepo: TestProbe   = TestProbe()
    val amiBus: XucAmiBus                  = mock[XucAmiBus]
    val factory                 = new MessageFactory
    val myFactory: MessageFactory               = mock[MessageFactory]
    val agentQueueMemberFactory: AgentQueueMemberFactory = mock[AgentQueueMemberFactory]
    val xivoUserDao: XivoUserDao             = mock[XivoUserDao]
    val webServiceUserDao: WebServiceUserDao       = mock[WebServiceUserDao]
    val agentLoginStatusDao: AgentLoginStatusDao     = mock[AgentLoginStatusDao]
    val parent: TestProbe                  = TestProbe()
    val agentManager: TestProbe            = TestProbe()
    val statAgreggator: TestProbe          = TestProbe()
    val configServiceManager: TestProbe    = TestProbe()
    val agentGroupFactory: AgentGroupFactory       = mock[AgentGroupFactory]
    val userLineNumberFactory: UserLineNumberFactory   = mock[UserLineNumberFactory]
    val devicesTracker: TestProbe          = TestProbe()
    val statusPublish: TestProbe           = TestProbe()
    val extensionsManager: TestProbe       = TestProbe()
    val extensionPattern: ExtensionPatternFactory        = mock[ExtensionPatternFactory]
    val xucConfig: XucBaseConfig               = app.injector.instanceOf[XucBaseConfig]
    val configServerRequester: ConfigServerRequester   = mock[ConfigServerRequester]
    val userPreferenceService: TestProbe   = TestProbe()
    val ctiRouterFactory: TestProbe        = TestProbe()
    val amiBusConnector: TestProbe         = TestProbe()

    when(extensionPattern.getAll()).thenReturn(Future.successful(List.empty))
    when(webServiceUserDao.getWebServiceUsers())
      .thenReturn(Future.successful(List.empty))
    when(configRepository.webServiceUsers).thenReturn(Map.empty)

    trait AgentLoginStatusDaoTest extends AgentLoginStatusDao {
      override def getLoginStatus(id: Agent.Id): Option[AgentLoginStatus] =
        agentLoginStatusDao.getLoginStatus(id)
    }

    def actor: (TestActorRef[ConfigDispatcher with ConfigInitializer], ConfigDispatcher with ConfigInitializer) = {
      val a = TestActorRef[ConfigDispatcher with ConfigInitializer](
        Props(
          new ConfigDispatcher(
            configRepository,
            agentManager.ref,
            defaultMembershipRepo.ref,
            statAgreggator.ref,
            xivoUserDao,
            webServiceUserDao,
            eventBus,
            amiBus,
            agentQueueMemberFactory,
            agentGroupFactory,
            userLineNumberFactory,
            devicesTracker.ref,
            configServiceManager.ref,
            statusPublish.ref,
            extensionsManager.ref,
            agentLoginStatusDao,
            extensionPattern,
            xucConfig,
            userPreferenceService.ref,
            ctiRouterFactory.ref,
            amiBusConnector.ref
          ) with ConfigInitializer {
            override def updateOrCreateMetric(
                queueId: Int,
                statName: String,
                statValue: Double
            ): Unit = mock[(Int, String, Double) => Unit]
          }
        ),
        parent.ref,
        "ConfigInitializer"
      )
      a.underlyingActor.requests = List()
      (a, a.underlyingActor)
    }
  }

  "Config initializer" should {
    "send request to link on init config" in new Helper {
      val (ref, cInit) = actor
      val ctiLink: TestProbe = TestProbe()
      when(xivoUserDao.getCtiUsers()).thenReturn(Future(List()))
      val requests: List[JSONObject] =
        List(factory.createGetAgents(), factory.createGetPhonesList())
      cInit.requestsToSend = requests

      ref ! InitConfig(ctiLink.ref)

      requests.foreach(r => ctiLink.expectMsg(r))
    }
    "load queue members on init config" in new Helper {
      val (ref, cInit) = actor
      val ctiLink: TestProbe = TestProbe()
      when(xivoUserDao.getCtiUsers()).thenReturn(Future(List()))

      ref ! InitConfig(ctiLink.ref)

      configServiceManager.expectMsgAllOf(GetQueueConfigAll, GetAgentConfigAll)
    }

    "load users on InitConfig" in new Helper {
      val (ref, cInit) = actor
      val ctiLink: TestProbe = TestProbe()

      val userList: Future[List[XivoUser]] = Future(
        List(
          XivoUser(
            1,
            None,
            None,
            "Jack",
            Some("Band"),
            Some("user1"),
            Some("0000"),
            None,
            None
          )
        )
      )
      when(xivoUserDao.getCtiUsers()).thenReturn(userList)

      ref ! InitConfig(ctiLink.ref)

      verify(xivoUserDao).getCtiUsers()
      configServiceManager.expectMsgAllOf(GetQueueConfigAll, GetAgentConfigAll)
      verify(configRepository, never()).updateWebServiceUser(
        any[WebServiceUser]
      )
      verify(configRepository, Mockito.timeout(1000)).updateUser(
        XivoUser(
          1,
          None,
          None,
          "Jack",
          Some("Band"),
          Some("user1"),
          Some("0000"),
          None,
          None
        )
      )
    }

    "load stun address on InitConfig" in new Helper {
      val (ref, _) = actor
      val ctiLink: TestProbe = TestProbe()

      when(xivoUserDao.getCtiUsers()).thenReturn(Future(List()))

      ref ! InitConfig(ctiLink.ref)

      configServiceManager.expectMsgAllOf(
        GetQueueConfigAll,
        GetAgentConfigAll,
        GetIceServer
      )
    }

    "send subsequent requests to link" in new Helper {
      val (ref, cInit) = actor
      val ctiLink: TestProbe = TestProbe()
      when(xivoUserDao.getCtiUsers()).thenReturn(Future(List()))
      when(configRepository.ctiUsers).thenReturn(Map.empty)
      val requests: List[JSONObject] = List(factory.createGetAgents())
      cInit.link = ctiLink.ref
      cInit.requestsToSend = requests

      ref ! InitConfig(ctiLink.ref)

      val subseqrequests: List[JSONObject] = List(factory.createGetAgentConfig("12"))
      cInit.requests = subseqrequests

      (requests ++ subseqrequests).foreach(r => ctiLink.expectMsg(r))
    }

    "load users and creates outgoing requests on UsersAdded" in new Helper {
      val (ref, cInit) = actor
      val ctiLink: TestProbe = TestProbe()
      cInit.link = ctiLink.ref
      val xivoUser111: XivoUser =
        XivoUser(
          111,
          None,
          None,
          "Jack",
          Some("Band"),
          Some("user1"),
          Some("0000"),
          None,
          None
        )
      val xivoUser222: XivoUser =
        XivoUser(
          222,
          None,
          None,
          "Ann",
          Some("BandX"),
          Some("user2"),
          Some("1111"),
          None,
          None
        )

      when(xivoUserDao.getCtiUser(xivoUser111.id))
        .thenReturn(Future(Option(xivoUser111)))
      when(xivoUserDao.getCtiUser(xivoUser222.id))
        .thenReturn(Future(Option(xivoUser222)))
      when(configRepository.ctiUsers).thenReturn(Map.empty)

      when(configRepository.configServerRequester).thenReturn(
        configServerRequester
      )
      when(configServerRequester.getUserServices(xivoUser111.id))
        .thenReturn(Future.successful(mock[UserServices]))
      when(configServerRequester.getUserServices(xivoUser222.id))
        .thenReturn(Future.successful(mock[UserServices]))
      when(userLineNumberFactory.get(any[Long]))
        .thenReturn(Some(UserLineNumber(232, 35, "1000")))
      when(configRepository.getUserServices(any[Int]))
        .thenReturn(Some(mock[UserServices]))
      when(configRepository.getCtiUser(any[Long])).thenReturn(None)

      val msg = new UsersAdded
      msg.add(xivoUser111.id.toInt)
      msg.add(xivoUser222.id.toInt)
      ref ! msg

      val received: Seq[String] = ctiLink.receiveN(3, 2.seconds).map(_.toString)
      assert(
        received(0).contains(
          """"tipbxid":"xivo","function":"listid","listname":"phones","class":"getlist""""
        )
      )
      assert(
        received(1).contains(
          """"tipbxid":"xivo","function":"updatestatus","listname":"users","class":"getlist""""
        )
      )
      assert(received(1).contains(""""tid":"111""""))
      assert(
        received(2).contains(
          """"tipbxid":"xivo","function":"updatestatus","listname":"users","class":"getlist""""
        )
      )
      assert(received(2).contains(""""tid":"222""""))

      verify(xivoUserDao).getCtiUser(xivoUser111.id)
      verify(xivoUserDao).getCtiUser(xivoUser222.id)

      verify(configRepository, Mockito.timeout(1000)).updateUser(xivoUser111)
      verify(configRepository, Mockito.timeout(2500)).updateUser(xivoUser222)
    }

    "request phones configuration on phone ids" in new Helper {
      val (ref, cInit) = actor
      cInit.requests = List()
      val phoneIds           = new PhoneIdsList
      val phoneConfigRequest: JSONObject = factory.createGetPhoneConfig("4")
      when(configRepository.onPhoneIds(phoneIds))
        .thenReturn(List(phoneConfigRequest))

      ref ! phoneIds

      verify(configRepository).onPhoneIds(phoneIds)
      cInit.requests.head shouldBe phoneConfigRequest
    }

    "request queues configuration on queue ids" in new Helper {
      val (ref, cInit) = actor
      cInit.requests = List()
      val queueIds           = new QueueIds
      val queueConfigRequest: JSONObject = factory.createGetQueueConfig("72")
      when(configRepository.onQueueIds(queueIds))
        .thenReturn(List(queueConfigRequest))

      ref ! queueIds

      verify(configRepository).onQueueIds(queueIds)
      cInit.requests.head shouldBe queueConfigRequest
    }
    "request queue member configuration on queue member ids" in new Helper {
      val (ref, cInit)             = actor
      val queueMemberIds           = new QueueMemberIds
      val queueMemberConfigRequest: JSONObject = factory.createGetQueueMemberConfig("65")
      when(configRepository.onQueueMemberIds(queueMemberIds))
        .thenReturn(List(queueMemberConfigRequest))

      ref ! queueMemberIds

      verify(configRepository).onQueueMemberIds(queueMemberIds)
      cInit.requests.head shouldBe queueMemberConfigRequest

    }
    "not crash on receiving other id list" in new Helper {
      val (ref, cInit) = actor
      cInit.requests = List()
      val agentIds = new AgentIds
      cInit.receive(agentIds)
    }

    "update config repository on initialization" in new Helper {
      val (ref, cInit) = actor
      val ctiLink: TestProbe = TestProbe()

      val userList: Future[List[Nothing]] = Future(List())
      when(xivoUserDao.getCtiUsers()).thenReturn(userList)

      ref ! InitConfig(ctiLink.ref)

      configServiceManager.expectMsg(GetQueueConfigAll)
    }

    "update config repository on queue configuration received" in new Helper {
      val (ref, _) = actor
      val queueConfig: QueueConfigUpdate = QueueConfigUpdate(
        123,
        "q1",
        "Queue One",
        "111",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      ref ! UpdateQueue(queueConfig)

      verify(configRepository).updateQueueConfig(queueConfig)
    }

    "update agents config repository on LoadAgents" in new Helper {
      val (ref, _) = actor
      val queueConfig: QueueConfigUpdate = QueueConfigUpdate(
        123,
        "q1",
        "Queue One",
        "111",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      ref ! LoadAgentQueueMembers

      configServiceManager.expectMsg(GetAgentConfigAll)
    }

    """on user config updated message received
        get user username
        update config repository with username
        update config repository with line
        update config repository
        request user status""" in new Helper {

      val (ref, configInitializer) = actor
      val link: TestProbe = TestProbe()
      configInitializer.link = link.ref
      configInitializer.messageFactory = myFactory
      val xivoUser: XivoUser =
        XivoUser(
          232,
          None,
          None,
          "Jack",
          Some("Band"),
          Some("user1"),
          Some("0000"),
          None,
          None
        )

      when(xivoUserDao.getCtiUser(xivoUser.id))
        .thenReturn(Future(Option(xivoUser)))
      when(configRepository.ctiUsers).thenReturn(Map.empty)

      when(configRepository.configServerRequester).thenReturn(
        configServerRequester
      )
      when(configServerRequester.getUserServices(xivoUser.id))
        .thenReturn(Future.successful(mock[UserServices]))
      when(userLineNumberFactory.get(any[Long]))
        .thenReturn(Some(UserLineNumber(232, 35, "1000")))
      when(configRepository.getUserServices(any[Int]))
        .thenReturn(Some(mock[UserServices]))
      when(configRepository.getCtiUser(232)).thenReturn(Some(xivoUser))

      val userConfigUpdate: UserConfigUpdated = UserConfigUpdated(232)
      ref ! userConfigUpdate

      verify(configRepository, Mockito.timeout(1000)).updateUser(xivoUser)
      verify(configServerRequester).getUserServices(232)
      verify(xivoUserDao).getCtiUser(232)
      verify(configRepository).onUserServicesUpdated(
        mockitoEq(232),
        any[UserServices]
      )
      verify(configRepository).onUserConfigUpdate(userConfigUpdate)

      verify(configRepository).loadUserLine(232, 35)

      verify(configRepository).getCtiUser(232)
      verify(configRepository).getUserServices(232)
    }

    """ on user config update
        load agent configuration if agent id is not 0
        and send self message with new agent loaded
        """ in new Helper {
      val (ref, configInit) = actor
      when(xivoUserDao.getCtiUser(232)).thenReturn(
        Future(
          Some(
            XivoUser(
              232,
              Some(45),
              None,
              "Alf",
              Some("Foul"),
              Some("afoul"),
              Some("0000"),
              None,
              None
            )
          )
        )
      )

      val agentId          = 45
      val agent: Agent = Agent(agentId, "Marc", "Aurèle", "3345", "default")
      val userConfigUpdate: UserConfigUpdated = UserConfigUpdated(232)

      when(configRepository.configServerRequester).thenReturn(
        configServerRequester
      )
      when(configServerRequester.getUserServices(232))
        .thenReturn(Future.successful(mock[UserServices]))
      when(userLineNumberFactory.get(232))
        .thenReturn(Some(UserLineNumber(232, 35, "1000")))
      when(configRepository.getAgent(agentId)).thenReturn(Some(agent))

      ref ! userConfigUpdate

      verify(configRepository, Mockito.timeout(1000)).loadAgent(agentId)
      verify(configRepository).getAgent(agentId)

    }

    """ on UpdateAgent received
        - if agent state is not available
        - get login status from db and send agent init login to agent manager if exists
        - request status
        - request queue member config update
        - publish agent on bus""" in new Helper {
      val (ref, cInit) = actor
      cInit.messageFactory = myFactory
      val ctiLink: TestProbe = TestProbe()
      cInit.link = ctiLink.ref
      val agentId     = 45
      val agentNumber = "56778"
      val agent: Agent = Agent(agentId, "Bob", "Dash", agentNumber, "default")
      val phoneNb     = "1200"

      val statusRequest: JSONObject = factory.createGetAgentStatus(agentId.toString)
      val qmemberConfigRequest: JSONObject =
        factory.createGetQueueMemberConfig(s"Agent/$agentNumber,queue")

      when(configRepository.getAgentState(agentId)).thenReturn(None)
      when(myFactory.createGetAgentStatus(agentId.toString))
        .thenReturn(statusRequest)
      when(configRepository.getAgentQueueMemberRequest(agentNumber))
        .thenReturn(List(qmemberConfigRequest))

      val agentLoginStatus =
        new AgentLoginStatus(agentId, phoneNb, new DateTime())
      when(agentLoginStatusDao.getLoginStatus(agentId))
        .thenReturn(Some(agentLoginStatus))

      ref ! UpdateAgent(agent)

      verify(myFactory).createGetAgentStatus(agentId.toString)
      ctiLink.expectMsgAllOf(statusRequest, qmemberConfigRequest)

      agentManager.expectMsg(
        AgentInitLoggedIn(
          agent.id,
          agent.number,
          phoneNb,
          agentLoginStatus.loginDate
        )
      )
      agentManager.expectNoMessage()
      verify(eventBus).publish(
        XucEvent(XucEventBus.configTopic(ObjectType.TypeAgent), agent)
      )

    }

    """ on UpdateAgent received
        - if agent state is available
        - get login status from db and send agent init login to agent manager if exists
        - request status
        - request queue member config update
        - publish agent on bus""" in new Helper {
      val (ref, cInit) = actor
      cInit.messageFactory = myFactory
      val ctiLink: TestProbe = TestProbe()
      cInit.link = ctiLink.ref
      val agentId     = 45
      val agentNumber = "56778"
      val agent: Agent = Agent(agentId, "Bob", "Dash", agentNumber, "default")
      val phoneNb     = "1200"

      val statusRequest: JSONObject = factory.createGetAgentStatus(agentId.toString)
      val qmemberConfigRequest: JSONObject =
        factory.createGetQueueMemberConfig(s"Agent/$agentNumber,queue")

      when(configRepository.getAgentState(agentId)).thenReturn(
        Some(
          AgentReady(
            agentId,
            new DateTime(),
            phoneNb,
            List(),
            None,
            agentNumber
          )
        )
      )
      when(myFactory.createGetAgentStatus(agentId.toString))
        .thenReturn(statusRequest)
      when(configRepository.getAgentQueueMemberRequest(agentNumber))
        .thenReturn(List(qmemberConfigRequest))

      val agentLoginStatus =
        new AgentLoginStatus(agentId, phoneNb, new DateTime())
      when(agentLoginStatusDao.getLoginStatus(agentId))
        .thenReturn(Some(agentLoginStatus))

      ref ! UpdateAgent(agent)

      verify(myFactory).createGetAgentStatus(agentId.toString)
      ctiLink.expectMsgAllOf(statusRequest, qmemberConfigRequest)

      agentManager.expectNoMessage(250.milliseconds)
      verify(eventBus).publish(
        XucEvent(XucEventBus.configTopic(ObjectType.TypeAgent), agent)
      )

    }

    """ on UpdateAgent received
        - if no login status from db send agent init logout to agent manager
        - request queue member
        - request queue member config update
        - publish agent on bus""" in new Helper {

      val (ref, cInit) = actor
      cInit.messageFactory = myFactory
      val ctiLink: TestProbe = TestProbe()
      cInit.link = ctiLink.ref
      val agentId     = 45
      val agentNumber = "56778"
      val agent: Agent = Agent(agentId, "Bob", "Dash", agentNumber, "default")

      val statusRequest: JSONObject = factory.createGetAgentStatus(agentId.toString)
      val qmemberConfigRequest: JSONObject =
        factory.createGetQueueMemberConfig(s"Agent/$agentNumber,queue")

      when(myFactory.createGetAgentStatus(agentId.toString))
        .thenReturn(statusRequest)
      when(configRepository.getAgentQueueMemberRequest(agentNumber))
        .thenReturn(List(qmemberConfigRequest))

      when(agentLoginStatusDao.getLoginStatus(agentId)).thenReturn(None)

      ref ! UpdateAgent(agent)

      verify(myFactory).createGetAgentStatus(agentId.toString)
      ctiLink.expectMsgAllOf(statusRequest, qmemberConfigRequest)

      agentManager.expectMsgPF()({
        case AgentInitLoggedOut(agent.id, agent.number, _) =>
      })
      verify(eventBus).publish(
        XucEvent(XucEventBus.configTopic(ObjectType.TypeAgent), agent)
      )

    }
  }
}
