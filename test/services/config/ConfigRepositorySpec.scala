package services.config

import models.*
import org.joda.time.{DateTime, Period}
import org.json.JSONObject
import org.mockito.Mockito.{never, verify, when}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{AgentIds, PhoneConfigUpdate, PhoneIdsList, PhoneStatusUpdate, QueueIds, QueueMemberConfigUpdate, QueueMemberIds, QueueConfigUpdate as _}
import org.xivo.cti.model.{Meetme, MeetmeMember, PhoneHintStatus}
import services.agent.{AgentStatistic, StatPeriod, Statistic}
import services.config.ConfigDispatcher.{LineConfigQueryById, LineConfigQueryByNb, TypeDefaultDevice, TypePhoneDevice}
import services.config.ConfigRepository.WaitingCallsStatistics
import xivo.events.AgentState.{AgentLoggedOut, AgentOnCall, AgentReady}
import xivo.events.CallDirection.*
import xivo.models.*
import xivo.xuc.{IceServerConfig, SipConfig}
import xivo.xucami.models.QueueCall

import java.util.Date
import scala.jdk.CollectionConverters.*
import scala.language.reflectiveCalls

class ConfigRepositorySpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterEach {

  import xivo.models.LineHelper.makeLine
  var configRepository: ConfigRepository               = _
  var factory: MessageFactory                          = _
  var lineFactory: LineFactory                         = _
  var agentFactory: AgentFactory                       = _
  var agentQueueMemberFactory: AgentQueueMemberFactory = _
  var configServerRequester: ConfigServerRequester     = _
  var turnServerConfig: IceServerConfig                = _
  var sipConfig: SipConfig                             = _

  override def beforeEach(): Unit = {
    factory = mock[MessageFactory]
    lineFactory = mock[LineFactory]
    agentFactory = mock[AgentFactory]
    agentQueueMemberFactory = mock[AgentQueueMemberFactory]
    configServerRequester = mock[ConfigServerRequester]
    turnServerConfig = mock[IceServerConfig]
    sipConfig = mock[SipConfig]

    when(sipConfig.sipPort).thenReturn(None)
    configRepository = new ConfigRepository(
      factory,
      lineFactory,
      agentFactory,
      agentQueueMemberFactory,
      configServerRequester,
      turnServerConfig,
      sipConfig)
  }

  def fixture(phoneNb: String, lineId: Int = 34): fixture =
    new fixture(phoneNb, lineId)
  class fixture(phoneNb: String, lineId: Int = 34) {
      val LineId: Int = lineId
      val userId      = 52
      val phoneNumber: String = phoneNb
      val line: Line = makeLine(
        LineId,
        "default",
        "sip",
        "ojhf",
        Some(
          XivoDevice(
            "ecbe7520c44c481cbf7bbc196294f27c",
            Some("10.50.2.104"),
            "Snom"
          )
        ),
        None,
        "ip"
      )
      val status: String = PhoneHintStatus.AVAILABLE.getHintStatus.toString
      val phoneStatus: PhoneStatusUpdate = {
        val phoneStatus = new PhoneStatusUpdate
        phoneStatus.setLineId(LineId)
        phoneStatus.setHintStatus(status)
        phoneStatus
      }
      val phoneConfig: PhoneConfigUpdate = {
        val phoneConfig = new PhoneConfigUpdate
        phoneConfig.setId(LineId)
        phoneConfig.setNumber(phoneNb)
        phoneConfig.setUserId(userId)
        phoneConfig
      }
      val emptyPhoneStatus: PhoneStatusUpdate = {
        val phoneStatus = new PhoneStatusUpdate
        phoneStatus.setLineId(LineId)
        phoneStatus.setHintStatus("")
        phoneStatus
      }
    }
  "A line repository" should {
    "update user line entry with phone device" in {
      val testFixture = fixture("44500")

      when(lineFactory.get(987L)).thenReturn(
        Some(makeLine(987, "default", "sip", "kjjkh", None, None, "ip"))
      )

      configRepository.loadUserLine(56, 987)

      verify(lineFactory).get(987L)
      configRepository.getLineForUser(56) should be(
        Some(makeLine(987, "default", "sip", "kjjkh", None, None, "ip"))
      )
      configRepository.getDeviceForLine(987L) should be(Some(TypePhoneDevice))

    }

    "update user line entry with webrtc device " in {
      val testFixture = fixture("44500")

      when(lineFactory.get(987L)).thenReturn(
        Some(
          makeLine(
            987,
            "default",
            "sip",
            "kjjkh",
            None,
            None,
            "ip",
            webRTC = true
          )
        )
      )

      configRepository.loadUserLine(56, 987)

      verify(lineFactory).get(987L)
      configRepository.getLineForUser(56) should be(
        Some(
          makeLine(
            987,
            "default",
            "sip",
            "kjjkh",
            None,
            None,
            "ip",
            webRTC = true
          )
        )
      )
      configRepository.getDeviceForLine(987L) should be(Some(TypeDefaultDevice))

    }

    "do not persist user line upon startup because we use default" in {
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
      val line = makeLine(112, "default", "SIP", "ajkoas", None, None, "ip")
      val nb   = "44580"
      configRepository.updateUser(user)
      configRepository.updateLineUser(line.id, user.id)
      configRepository.updateLinePhoneNb(line.id, nb)

      configRepository.updateLineDevice(line.id, TypePhoneDevice)
      verify(configServerRequester, never()).setUserPreference(
        user.id,
        UserPreferenceKey.PreferredDevice,
        TypePhoneDevice.name,
        UserPreferenceType.StringType
      )

    }

    "persist user line entry when changed " in {
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
      val line = makeLine(112, "default", "SIP", "ajkoas", None, None, "ip")
      val nb   = "44580"
      configRepository.updateUser(user)
      configRepository.updateLineUser(line.id, user.id)
      configRepository.updateLinePhoneNb(line.id, nb)

      configRepository.updateLineDevice(line.id, TypeDefaultDevice)
      configRepository.updateLineDevice(line.id, TypePhoneDevice)
      verify(configServerRequester).setUserPreference(
        user.id,
        UserPreferenceKey.PreferredDevice,
        TypePhoneDevice.name,
        UserPreferenceType.StringType
      )

    }

    "return line for a username" in {
      val line = mock[Line]
      configRepository.updateUser(
        XivoUser(3, None, None, "john", None, Some("johnLogin"), None, None, None)
      )
      configRepository.updateUserLine(3, line)

      configRepository.getLineForUser("johnLogin") should be(Some(line))
    }

    "return a user for a give line id" in {
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
      val line = makeLine(112, "default", "SIP", "ajkoas", None, None, "ip")
      val nb   = "44580"
      configRepository.updateUser(user)
      configRepository.updateLineUser(line.id, user.id)
      configRepository.updateLinePhoneNb(line.id, nb)

      configRepository.getLineUser(line.id) should be(Some(user))
    }

    "return phone number of a line for a given line interface" in {
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
      val nb = "44580"
      val line =
        makeLine(
          112,
          "default",
          "SIP",
          "m8r7oi95",
          None,
          None,
          "ip",
          webRTC = true,
          Some(nb)
        )
      configRepository.updateUser(user)
      configRepository.updateUserLine(user.id, line)

      configRepository.getPhoneNbfromInterface("SIP/m8r7oi95") should be(
        Some(nb)
      )
    }
  }

  "A config repository on phone status update" should {

    "update phone status with username" in {

      configRepository.updateUser(
        XivoUser(
          32,
          None,
          None,
          "jack",
          Some("Bal"),
          Some("jack"),
          Some("pwd"),
          None,
          None
        )
      )
      val (phoneNumber, status) =
        ("4000", PhoneHintStatus.RINGING.getHintStatus)
      val phoneConfig = new PhoneConfigUpdate
      phoneConfig.setId(45)
      phoneConfig.setNumber(phoneNumber)
      phoneConfig.setUserId(32)
      configRepository.onPhoneConfigUpdate(phoneConfig)

      configRepository.updatePhoneStatus(phoneNumber, status)

      val userPhoneStatus =
        configRepository.getUserPhoneStatus(phoneNumber, status).get

      userPhoneStatus.username should be("jack")
      userPhoneStatus.status should be(PhoneHintStatus.RINGING)
    }

    "not get any user phone status if phone status is not known (ERROR)" in {
      val (phoneNumber, status) = ("4000", 9999)

      configRepository.updateUser(
        XivoUser(
          32,
          None,
          None,
          "jack",
          Some("Bal"),
          Some("jack"),
          Some("pwd"),
          None,
          None
        )
      )
      val phoneConfig = new PhoneConfigUpdate
      phoneConfig.setId(43)
      phoneConfig.setNumber(phoneNumber)
      phoneConfig.setUserId(32)
      configRepository.onPhoneConfigUpdate(phoneConfig)

      configRepository.updatePhoneStatus(phoneNumber, status)

      configRepository.getUserPhoneStatus(phoneNumber, status) should be(
        None
      )
    }

    "get all user phone statuses" in {
      val (phoneNumber, status) =
        ("1000", PhoneHintStatus.AVAILABLE.getHintStatus)
      configRepository.updateUser(
        XivoUser(
          32,
          None,
          None,
          "jack",
          Some("Bal"),
          Some("jack"),
          Some("pwd"),
          None,
          None
        )
      )
      val phoneConfig = new PhoneConfigUpdate
      phoneConfig.setId(45)
      phoneConfig.setNumber(phoneNumber)
      phoneConfig.setUserId(32)
      configRepository.onPhoneConfigUpdate(phoneConfig)

      configRepository.updatePhoneStatus(phoneNumber, status)

      val uph = UserPhoneStatus("jack", PhoneHintStatus.getHintStatus(status))

      val uphs = configRepository.getAllUserPhoneStatuses

      uphs should contain(uph)
    }

    "can retreive phone status" in {
      val testFixture = fixture("1256", 78)
      val status      = PhoneHintStatus.AVAILABLE.getHintStatus

      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)

      configRepository.updatePhoneStatus(testFixture.phoneNumber, status)

      configRepository.getPhoneStatus(78).get.getHintStatus should be(
        status.toString
      )
      configRepository.getPhoneStatus(78).get.getLineId should be(78)

    }
  }

  "A config repository" should {
    "update internal line config repo from phoneConfigUpdate" in {
      val testFixture = fixture("44500")
      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)
      configRepository.linePhoneNbs
        .filter(_._2 == "44500")
        .keys
        .toList should be(List(testFixture.phoneConfig.getId))

    }
    "update internal line config repo directly" in {
      configRepository.updatePhoneLine(14L, "44500", 140L)
      configRepository.linePhoneNbs
        .filter(_._2 == "44500")
        .keys
        .toList should be(List(14))
      configRepository.linesUser should be(Map(14 -> 140))
    }
    "replace line configuration" in {
      val testFixture = fixture("44500")
      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)
      testFixture.phoneConfig.setId(58)
      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)
      configRepository.linePhoneNbs
        .filter(_._2 == "44500")
        .keys
        .toList should be(List(testFixture.phoneConfig.getId))

    }

    "requests phoneConfig for each phoneId upon reception of phoneList" in {

      val expectedRequest = new JSONObject()
      when(factory.createGetPhoneConfig("1")).thenReturn(expectedRequest)
      when(factory.createGetPhoneConfig("3")).thenReturn(expectedRequest)

      val phoneIdsList = new PhoneIdsList()
      phoneIdsList.add(1)
      phoneIdsList.add(3)

      val result = configRepository.onPhoneIds(phoneIdsList)

      result.size should be(2)

      result.foreach(request => {
        request should be(expectedRequest)
      })
      verify(factory).createGetPhoneConfig("1")
      verify(factory).createGetPhoneConfig("3")
    }

    "get a user from id" in {
      val user =
        XivoUser(
          45,
          None,
          None,
          "Bob",
          Some("Health"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      configRepository.updateUser(user)

      configRepository.getCtiUser(45) should be(Some(user))
    }
    "get a user from username" in {
      val user =
        XivoUser(
          45,
          None,
          None,
          "Bob",
          Some("Health"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      configRepository.updateUser(user)

      configRepository.getCtiUser("bhealth") should be(Some(user))
    }
    "cannot get a user without username" in {
      val user =
        XivoUser(
          45,
          None,
          None,
          "Bob",
          Some("Health"),
          Some(""),
          Some("pwd"),
          None,
          None
        )
      configRepository.updateUser(user)

      configRepository.getCtiUser("") should be(None)
    }
    "get a user from username and password" in {
      val user =
        XivoUser(
          45,
          None,
          None,
          "Bob",
          Some("Health"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      configRepository.updateUser(
        XivoUser(
          45,
          None,
          None,
          "Bob",
          Some("Health"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      )
      configRepository.updateUser(
        XivoUser(
          46,
          None,
          None,
          "John",
          Some("Als"),
          Some("jals"),
          Some("pwdals"),
          None,
          None
        )
      )

      configRepository.getCtiUser("bhealth", "pwd") should be(Some(user))
      configRepository.getCtiUser("bhealth", "xcvds") should be(None)

    }

    "when receiving a new user remove existing user with same cti username and update with new user" in {
      val user =
        XivoUser(
          45,
          None,
          None,
          "Bob",
          Some("Health"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      val newUserWithSameCtiUsername =
        XivoUser(
          47,
          None,
          None,
          "Bob",
          Some("Health"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      configRepository.updateUser(user)
      configRepository.updateUser(newUserWithSameCtiUsername)

      configRepository.getCtiUser(
        newUserWithSameCtiUsername.username.get
      ) should be(Some(newUserWithSameCtiUsername))
      configRepository.getCtiUser(user.id) should be(None)
    }

    "not remove users without username while updating other user" in {
      val user1WithoutLogin =
        XivoUser(
          50,
          None,
          None,
          "Bob",
          Some("50"),
          None,
          None,
          None,
          None
        )
      val user2WithoutLogin =
        XivoUser(
          51,
          None,
          None,
          "Bob",
          Some("51"),
          None,
          None,
          None,
          None
        )
      val user =
        XivoUser(
          52,
          None,
          None,
          "Bob",
          Some("NoName"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      val userUpdate =
        XivoUser(
          52,
          None,
          None,
          "Bob",
          Some("52"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      configRepository.updateUser(user)
      configRepository.updateUser(user1WithoutLogin)
      configRepository.updateUser(user2WithoutLogin)
      configRepository.updateUser(userUpdate)

      configRepository.getCtiUser(50) should be(Some(user1WithoutLogin))
      configRepository.getCtiUser(51) should be(Some(user2WithoutLogin))
      configRepository.getCtiUser("bhealth") should be(Some(userUpdate))
    }

    "not remove users with empty username while updating other user" in {
      val user1WithoutLogin =
        XivoUser(
          50,
          None,
          None,
          "Bob",
          Some("50"),
          Some(""),
          None,
          None,
          None
        )
      val user2WithoutLogin =
        XivoUser(
          51,
          None,
          None,
          "Bob",
          Some("51"),
          Some(""),
          None,
          None,
          None
        )
      val user =
        XivoUser(
          52,
          None,
          None,
          "Bob",
          Some("NoName"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      val userUpdate =
        XivoUser(
          52,
          None,
          None,
          "Bob",
          Some("52"),
          Some("bhealth"),
          Some("pwd"),
          None,
          None
        )
      configRepository.updateUser(user)
      configRepository.updateUser(user1WithoutLogin)
      configRepository.updateUser(user2WithoutLogin)
      configRepository.updateUser(userUpdate)

      configRepository.getCtiUser(50) should be(Some(user1WithoutLogin))
      configRepository.getCtiUser(51) should be(Some(user2WithoutLogin))
      configRepository.getCtiUser("bhealth") should be(Some(userUpdate))
    }

    "get a user display name from username" in {
      val user =
        XivoUser(
          45,
          None,
          None,
          "James",
          Some("Bond"),
          Some("jbond"),
          Some("pwd"),
          None,
          None
        )
      configRepository.updateUser(user)

      configRepository.getCtiUserDisplayName("jbond") should be(
        Some("James Bond")
      )
    }

    "requests queue configuration for each queue id received" in {

      val expectedRequest = new JSONObject()
      when(factory.createGetQueueConfig("11")).thenReturn(expectedRequest)
      when(factory.createGetQueueConfig("31")).thenReturn(expectedRequest)

      val queueIds = new QueueIds
      queueIds.add(11)
      queueIds.add(31)

      val result = configRepository.onQueueIds(queueIds)

      result.size should be(2)
      verify(factory).createGetQueueConfig("11")
      verify(factory).createGetQueueConfig("31")
    }

    "requests agent status for each agent id received" in {

      val expectedRequest = new JSONObject()
      when(factory.createGetAgentStatus("27")).thenReturn(expectedRequest)
      when(factory.createGetAgentStatus("41")).thenReturn(expectedRequest)

      val agentIds = new AgentIds
      agentIds.add(27)
      agentIds.add(41)

      val result = configRepository.requestStatusOnAgentIds(agentIds)

      result.size should be(2)
      verify(factory).createGetAgentStatus("27")
      verify(factory).createGetAgentStatus("41")
    }

    "request agent from db on id" in {
      val agentId = 45
      val albert  = Agent(agentId, "Albert", "Jul", "4567", "default")

      when(agentFactory.getById(agentId)).thenReturn(Some(albert))

      configRepository.loadAgent(agentId)

      configRepository.getAgent(agentId) should be(Some(albert))
    }

    "request agent by userId" in {
      val userId = 12
      val albert = Agent(45L, "Albert", "Jul", "4567", "default", 0, userId)

      configRepository.addAgent(albert)

      configRepository.getAgentByUserId(userId) should be(Some(albert))
    }

    "creates agent queue member config update from agent number" in {
      val queueMember = new QueueMemberConfigUpdate
      queueMember.setId("Agent/7654,queueone")
      queueMember.setAgentNumber("7654")
      queueMember.setQueueName("queueone")
      putAgentInQueue(agentId = 25L, queueId = 32, penalty = 5)
      addAgent(Agent(25L, "Alain", "Ficelle", "7654", "default", 32))
      addQueue(32, "queueone")

      val expectedRequest = new JSONObject()

      when(factory.createGetQueueMemberConfig("Agent/7654,queueone"))
        .thenReturn(expectedRequest)

      configRepository.getAgentQueueMemberRequest("7654") should be(
        List(expectedRequest)
      )

      verify(factory).createGetQueueMemberConfig("Agent/7654,queueone")

    }
    "request queue member configuration on each queue member id received" in {
      val expectedRequest = new JSONObject()
      when(factory.createGetQueueMemberConfig("Agent/3456,queueone"))
        .thenReturn(expectedRequest)
      when(factory.createGetQueueMemberConfig("Agent/3400,queuetwo"))
        .thenReturn(expectedRequest)

      val qmIds = new QueueMemberIds
      qmIds.add("Agent/3456,queueone")
      qmIds.add("Agent/3400,queuetwo")

      val result = configRepository.onQueueMemberIds(qmIds)

      result.size should be(2)
      verify(factory).createGetQueueMemberConfig("Agent/3456,queueone")
      verify(factory).createGetQueueMemberConfig("Agent/3400,queuetwo")
    }
    "should update of add queue member" in {
      putAgentInQueue(45L, 32, 1)

      configRepository.updateOrAddQueueMembers(AgentQueueMember(45L, 32, 7))

      val agQMembers = configRepository.getAgentQueueMembers()
      agQMembers.length should be(1)
      agQMembers should contain(AgentQueueMember(45L, 32, 7))

    }
    "should check if queue member exists" in {
      putAgentInQueue(32L, 12, 3)

      configRepository.queueMemberExists(AgentQueueMember(32L, 12, 3))
    }

    "update phone status with username on request" in {

      val userId = 54321
      val lineId = 5678
      configRepository.updateUser(
        XivoUser(
          userId,
          None,
          None,
          "joe",
          Some("bal"),
          Some("joe"),
          Some("pwd"),
          None,
          None
        )
      )
      val phoneConfig = new PhoneConfigUpdate
      phoneConfig.setId(lineId)
      phoneConfig.setNumber("1000")
      phoneConfig.setUserId(54321)
      configRepository.onPhoneConfigUpdate(phoneConfig)

      val status = new PhoneStatusUpdate
      status.setLineId(lineId)
      status.setHintStatus("99")
      val userPhoneStatus: UserPhoneStatus =
        configRepository.getUserPhoneStatus(status).get

      userPhoneStatus.username should be("joe")
    }

    "not update any phone status when no username updated" in {

      val status = new PhoneStatusUpdate
      status.setLineId(32)
      status.setHintStatus("6")
      val userPhoneStatus = configRepository.getUserPhoneStatus(status)

      userPhoneStatus should be(None)
    }

    "get line config on config request by number when found" in {
      val phoneNumberToFind = "44500"

      val testFixture = fixture(phoneNumberToFind)
      when(lineFactory.get(testFixture.LineId)).thenReturn(Some(testFixture.line))

      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)

      val lineConfig = LineConfig(
        testFixture.LineId.toString,
        phoneNumberToFind,
        Some(testFixture.line)
      )

      configRepository.getLineConfig(
        LineConfigQueryByNb(phoneNumberToFind)
      ) should be(Some(lineConfig))
    }

    "get None on config request by number when not found" in {
      val testFixture = fixture("44500")

      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)

      configRepository.getLineConfig(LineConfigQueryByNb("777")) should be(None)
    }

    "get line config on config request by id when found" in {
      val tf = fixture("2000", 57)
      val line = makeLine(
        tf.LineId,
        "default",
        "sip",
        "ojhf",
        Some(
          XivoDevice(
            "ecbe7520c44c481cbf7bbc196294f27c",
            Some("10.50.2.104"),
            "Snom"
          )
        ),
        None,
        "ip"
      )
      when(lineFactory.get(tf.LineId, deviceType = TypeDefaultDevice))
        .thenReturn(Some(line))
      configRepository.onPhoneConfigUpdate(tf.phoneConfig)

      val lineConfig = LineConfig(tf.LineId.toString, "2000", Some(line))

      configRepository.getLineConfig(LineConfigQueryById(tf.LineId)) should be(
        Some(lineConfig)
      )
    }

    "get line config on config request by id with enrich ua" in {
      val tf = fixture("2000", 57)
      val line = makeLine(
        tf.LineId,
        "default",
        "sip",
        "ojhf",
        Some(
          XivoDevice(
            "ecbe7520c44c481cbf7bbc196294f27c",
            Some("10.50.2.104"),
            "Snom"
          )
        ),
        None,
        "ip",
        webRTC = true,
        ua = true
      )
      configRepository.lineDevice = Map(tf.LineId.toLong -> TypeDefaultDevice)

      val expected = line.copy(name = line.name + "_w", dev = None)

      when(lineFactory.get(tf.LineId, deviceType = TypeDefaultDevice))
        .thenReturn(Some(expected))
      configRepository.onPhoneConfigUpdate(tf.phoneConfig)

      val lineConfig = LineConfig(tf.LineId.toString, "2000", Some(expected))

      configRepository.getLineConfig(LineConfigQueryById(tf.LineId)) should be(
        Some(lineConfig)
      )
    }

    "get line config on config request by id with un-enrich ua" in {
      val tf = fixture("2000", 57)
      val line = makeLine(
        tf.LineId,
        "default",
        "sip",
        "ojhf",
        Some(
          XivoDevice(
            "ecbe7520c44c481cbf7bbc196294f27c",
            Some("10.50.2.104"),
            "Snom"
          )
        ),
        None,
        "ip",
        webRTC = true,
        ua = true
      )
      configRepository.lineDevice = Map(tf.LineId.toLong -> TypePhoneDevice)

      val expected = line.copy(name = line.name + "_w", dev = None)

      when(lineFactory.get(tf.LineId, deviceType = TypePhoneDevice))
        .thenReturn(Some(expected))
      configRepository.onPhoneConfigUpdate(tf.phoneConfig)

      val lineConfig = LineConfig(tf.LineId.toString, "2000", Some(expected))

      configRepository.getLineConfig(LineConfigQueryById(tf.LineId)) should be(
        Some(lineConfig)
      )
    }

    "update device line with phone type" in {
      val tf = fixture("2000", 57)
      configRepository.lineDevice = Map(tf.LineId.toLong -> TypeDefaultDevice)
      configRepository.updateLineDevice(tf.LineId.toLong, TypePhoneDevice)

      configRepository.getDeviceForLine(tf.LineId.toLong) shouldBe Some(
        TypePhoneDevice
      )
    }

    "update device line with webrtc type" in {
      val tf = fixture("2000", 57)
      configRepository.lineDevice = Map(tf.LineId.toLong -> TypePhoneDevice)
      configRepository.updateLineDevice(tf.LineId.toLong, TypeDefaultDevice)

      configRepository.getDeviceForLine(tf.LineId.toLong) shouldBe Some(
        TypeDefaultDevice
      )
    }

    "get line by sip endpoint id" in {
      val tf = fixture("2000", 57)
      val line = makeLine(
        tf.LineId,
        "default",
        "sip",
        "ojhf",
        Some(
          XivoDevice(
            "ecbe7520c44c481cbf7bbc196294f27c",
            Some("10.50.2.104"),
            "Snom"
          )
        ),
        None,
        "ip",
        webRTC = true,
        ua = true
      )
      val endpoint = SipEndpoint(1L)

      val expected = line

      when(lineFactory.get(endpoint)).thenReturn(Some(line))

      configRepository.getLineByEndpoint(endpoint) should be(Some(expected))
    }

    "send agent directory on request" in {
      val agentId = 34L
      val agent   = Agent(agentId, "John", "Doe", "2000", "default")
      val agentState =
        AgentReady(agentId, new DateTime(), "4460", List(), agentNb = "2200")

      configRepository.addAgent(agent)
      configRepository.onAgentState(agentState)

      val ad = configRepository.getAgentDirectory

      ad should be(List(AgentDirectoryEntry(agent, agentState)))
    }

    "not send agent on directory if not status received" in {

      val agent = Agent(34L, "John", "Doe", "2000", "default")

      configRepository.addAgent(agent)
      val ad = configRepository.getAgentDirectory

      ad should be(List())

    }

    "send agent states on request" in {
      val agentStateA =
        AgentReady(34L, new DateTime(), "3200", List(), agentNb = "2000")
      val agentStateB = AgentOnCall(
        22L,
        new DateTime(),
        true,
        Incoming,
        "3211",
        List(),
        onPause = false,
        agentNb = "2000"
      )

      configRepository.onAgentState(agentStateA)
      configRepository.onAgentState(agentStateB)

      val as = configRepository.getAgentStates

      as should be(List(agentStateA, agentStateB))
    }
    "retreive agent state" in {
      val agentStateA =
        AgentReady(34L, new DateTime(), "3200", List(), agentNb = "2000")
      val agentStateB = AgentOnCall(
        22L,
        new DateTime(),
        true,
        Incoming,
        "3211",
        List(),
        onPause = false,
        agentNb = "2000"
      )

      configRepository.onAgentState(agentStateA)
      configRepository.onAgentState(agentStateB)

      val ags = configRepository.getAgentState(22L)
      ags should be(Some(agentStateB))
    }
    "get agent id logged on a phone number" in {
      val loggedId = 354
      val agentStateA =
        AgentReady(loggedId, new DateTime(), "3200", List(), agentNb = "2000")
      val agentStateB = AgentOnCall(
        22L,
        new DateTime(),
        true,
        Incoming,
        "3211",
        List(),
        onPause = false,
        agentNb = "2000"
      )

      configRepository.onAgentState(agentStateA)
      configRepository.onAgentState(agentStateB)

      val agentId = configRepository.getAgentLoggedOnPhoneNumber("3200")

      agentId should be(Some(loggedId))
    }
    "do not get agent id for a phone number if agent is logged out" in {
      val loggedOutId = 354
      val agentStateA =
        AgentLoggedOut(loggedOutId, new DateTime(), "3200", List(), "")

      configRepository.onAgentState(agentStateA)

      val agentId = configRepository.getAgentLoggedOnPhoneNumber("3200")

      agentId should be(None)
    }
    "do not get agent id if phone number is empty" in {
      val loggedId = 354
      val agentStateA =
        AgentReady(loggedId, new DateTime(), "", List(), agentNb = "2000")

      configRepository.onAgentState(agentStateA)

      val agentId = configRepository.getAgentLoggedOnPhoneNumber("")

      agentId should be(None)
    }
    "update user line and agent line on agent login received" in {
      val agentLogin =
        AgentReady(54L, new DateTime(), "3300", List(), agentNb = "2000")
      configRepository.linePhoneNbs = Map(874.toLong -> "3300")
      configRepository.userAgents = Map(77L -> 54)

      when(lineFactory.get(874L)).thenReturn(
        Some(makeLine(874, "default", "sip", "sdlkf", None, None, "ip"))
      )
      configRepository.onAgentState(agentLogin)

      configRepository.getLineForAgent(54L) should be(
        Some(makeLine(874, "default", "sip", "sdlkf", None, None, "ip"))
      )
      configRepository.getLineForUser(77) should be(
        Some(makeLine(874, "default", "sip", "sdlkf", None, None, "ip"))
      )
    }

    "saves queue configuration" in {
      val qConfig = QueueConfigUpdate(
        34,
        "q1",
        "Queue One",
        "111",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      configRepository.updateQueueConfig(qConfig)
      configRepository.queues.get(34) shouldBe Some(qConfig)
    }

    "retrieve queue configuration by given queue id" in {
      val qConfig = QueueConfigUpdate(
        34,
        "q1",
        "Queue One",
        "111",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      configRepository.updateQueueConfig(qConfig)
      configRepository.getQueue(34) shouldBe Some(qConfig)
    }

    "retrieve all queue configurations" in {
      val qConfigs = List(
        QueueConfigUpdate(
          34,
          "q1",
          "Queue One",
          "111",
          Some("default"),
          1,
          1,
          1,
          1,
          1,
          1,
          1,
          "url",
          "",
          Some(1),
          Some("preprocess_subroutine"),
          1,
          Some(1),
          Some(1),
          1,
          "recorded",
          1
        ),
        QueueConfigUpdate(
          35,
          "q2",
          "Queue Two",
          "222",
          Some("default"),
          1,
          1,
          1,
          1,
          1,
          1,
          1,
          "url",
          "",
          Some(1),
          Some("preprocess_subroutine"),
          1,
          Some(1),
          Some(1),
          1,
          "recorded",
          1
        )
      )
      configRepository.updateQueueConfig(qConfigs.head)
      configRepository.updateQueueConfig(qConfigs(1))
      configRepository.getQueues() shouldBe qConfigs
    }

    "return outbound queue number" in {
      addQueue(34, "out_grp1", "4000")
      addQueue(35, "otherqueue", "4010")
      addQueue(36, "out_grp2", "4020")

      val queue = configRepository.getQueue(36L)

      configRepository.getOutboundQueue(List(35, 36)) should be(queue)
    }

    "send back agents for groupId, queueId, penaly" in {
      val Alain    = Agent(4L, "Alain", "Chabot", "5236", "sales", 5)
      val Bernard  = Agent(5L, "Bernard", "Dante", "5237", "sales", 5)
      val Caroline = Agent(6L, "Caroline", "Eniac", "5238", "marketing", 1)
      addAgent(Alain)
      addAgent(Bernard)
      addAgent(Caroline)

      putAgentInQueue(4L, 1, 0)
      putAgentInQueue(5L, 1, 2)
      putAgentInQueue(6L, 2, 0)

      val agents =
        configRepository.getAgents(groupId = 5, queueId = 1, penalty = 0)

      agents should contain(Alain)
      agents should contain.noneOf(Bernard, Caroline)
    }

    "send back agents for groupId, not in queueId" in {
      import AgentBuilderP._
      val Daniel =
        addInConfig("Daniel", "Fan", "5239").inGroup(780).inQueue(100).build()
      val Eric =
        addInConfig("Eric", "Gore", "5240").inGroup(780).inQueue(225).build()
      val Fanny =
        addInConfig("Fany", "Hotel", "5241").inGroup(121).inQueue(230).build()

      putAgent(Daniel) inQueue 115

      val agents =
        configRepository.getAgentsNotInQueue(groupId = 780, queueId = 100)

      agents should contain(Eric)
      agents should contain.noneOf(Daniel, Fanny)
    }

    "send meetmes on request" in {
      val meetmeA = new Meetme(
        "test",
        "4000",
        true,
        new Date(),
        List[MeetmeMember]().asJava
      )
      val meetmeB = new Meetme(
        "test2",
        "4002",
        true,
        new Date(),
        List[MeetmeMember]().asJava
      )

      configRepository.onMeetmeUpdate(List(meetmeA, meetmeB))

      configRepository.getMeetmeList should be(List(meetmeA, meetmeB))
    }

    "add agent statistic" in {
      val agStat = AgentStatistic(34L, List(Statistic("stat1", StatPeriod(1))))

      configRepository.updateAgentStatistic(agStat)

      configRepository.agentStatistics(34L) should be(agStat)
    }

    "add a new agent statistic on update" in {
      val agStat1 = AgentStatistic(34L, List(Statistic("stat1", StatPeriod(1))))
      val agStat2 =
        AgentStatistic(34L, List(Statistic("stat2", StatPeriod(34))))

      configRepository.updateAgentStatistic(agStat1)
      configRepository.updateAgentStatistic(agStat2)

      configRepository
        .agentStatistics(34L)
        .statistics should contain theSameElementsAs List(
        Statistic("stat1", StatPeriod(1)),
        Statistic("stat2", StatPeriod(34))
      )

    }
    "update agent statistic" in {

      val agStat1 =
        AgentStatistic(34L, List(Statistic("stat1", StatPeriod(100))))
      val agStat2 =
        AgentStatistic(34L, List(Statistic("stat1", StatPeriod(2500))))

      configRepository.updateAgentStatistic(agStat1)
      configRepository.updateAgentStatistic(agStat2)

      configRepository.agentStatistics(34L) should be(agStat2)
    }

    "return a list of statistics" in {
      val agStat1 = AgentStatistic(34L, List(Statistic("stat1", StatPeriod(1))))
      val agStat2 =
        AgentStatistic(52L, List(Statistic("stat2", StatPeriod(34))))

      configRepository.updateAgentStatistic(agStat1)
      configRepository.updateAgentStatistic(agStat2)

      configRepository.getAgentStatistics should be(List(agStat1, agStat2))
    }

    "add a call to a queue" in {
      val queue    = "foo"
      val uniqueId = "123456.789"
      val queueCall = QueueCall(
        2,
        Some("foo"),
        "3345678",
        new DateTime(),
        "SIP/abcd-000001",
        "main"
      )

      configRepository.onQueueCallReceived(queue, uniqueId, queueCall)

      configRepository.queueCalls shouldEqual Map(
        queue -> Map(uniqueId -> queueCall)
      )
    }

    "add a call to the existing ones if the queue is already registered" in {
      val queue     = "foo"
      val uniqueId1 = "123456.789"
      val uniqueId2 = "123456.999"
      val queueCall1 = QueueCall(
        2,
        Some("foo"),
        "3345678",
        new DateTime(),
        "SIP/abcd-000001",
        "main"
      )
      val queueCall2 = QueueCall(
        3,
        Some("bar"),
        "44445678",
        new DateTime(),
        "SIP/abcd-000002",
        "main"
      )

      configRepository.onQueueCallReceived(queue, uniqueId1, queueCall1)
      configRepository.onQueueCallReceived(queue, uniqueId2, queueCall2)

      configRepository.queueCalls shouldEqual Map(
        queue -> Map(uniqueId1 -> queueCall1, uniqueId2 -> queueCall2)
      )
    }

    "update queue call caller id on queue entry received" in {
      val queue       = "foo"
      val queueConfig = mock[QueueConfigUpdate]
      val uniqueId1   = "123456.789"
      val uniqueId2   = "123456.999"
      val queueCall1 = QueueCall(
        2,
        Some("foo"),
        "3345678",
        new DateTime(),
        "SIP/abcd-000001",
        "main"
      )
      val queueCall2 = QueueCall(
        3,
        Some("bar"),
        "44445678",
        new DateTime(),
        "SIP/abcd-000002",
        "main"
      )

      when(queueConfig.name).thenReturn(queue)
      when(queueConfig.id).thenReturn(1L)

      configRepository.updateQueueConfig(queueConfig)
      configRepository.onQueueCallReceived(queue, uniqueId1, queueCall1)
      configRepository.onQueueCallReceived(queue, uniqueId2, queueCall2)

      configRepository.updateQueueCallCallerId(
        queue,
        "SIP/abcd-000001",
        "123456.789",
        "0000",
        "John"
      )

      val expected = queueCall1.copy(name = Some("John"), number = "0000")

      configRepository.queueCalls shouldEqual Map(
        queue -> Map(uniqueId1 -> expected, uniqueId2 -> queueCall2)
      )
    }

    "remove a call from a queue" in {
      val queue     = "foo"
      val uniqueId1 = "123456.789"
      val uniqueId2 = "123456.999"
      val queueCall1 = QueueCall(
        2,
        Some("foo"),
        "3345678",
        new DateTime(),
        "SIP/abcd-000001",
        "main"
      )
      val queueCall2 = QueueCall(
        3,
        Some("bar"),
        "44445678",
        new DateTime(),
        "SIP/abcd-000002",
        "main"
      )

      configRepository.queueCalls =
        Map(queue -> Map(uniqueId1 -> queueCall1, uniqueId2 -> queueCall2))
      configRepository.onQueueCallFinished(queue, uniqueId2)

      configRepository.queueCalls shouldEqual Map(
        queue -> Map(uniqueId1 -> queueCall1)
      )
    }

    "remove oldest call from a queue when uniqueid is not found in list" in {
      val queueName        = "foo"
      val oldestUniqueId   = "000000.000"
      val newestUniqueId   = "999999.999"
      val notFoundUniqueId = "123456.789"
      val oldestCall = QueueCall(
        4,
        Some("foo"),
        "3345678",
        new DateTime(2017, 9, 10, 11, 24),
        "SIP/abcd-000001",
        "main"
      )
      val newestCall = QueueCall(
        2,
        Some("bar"),
        "44445678",
        new DateTime(2017, 9, 10, 12, 12),
        "SIP/abcd-000002",
        "main"
      )

      configRepository.queueCalls = Map(
        queueName -> Map(
          oldestUniqueId -> oldestCall,
          newestUniqueId -> newestCall
        )
      )
      configRepository.onQueueCallFinished(queueName, notFoundUniqueId)

      configRepository.queueCalls shouldEqual Map(
        queueName -> Map(newestUniqueId -> newestCall)
      )
    }

    "update transfered call in a queue" in {
      val queue     = "foo"
      val uniqueId1 = "123456.789"
      val uniqueId2 = "123456.999"
      val uniqueId3 = "123456.123"
      val queueCall1 = QueueCall(
        2,
        Some("Aaa"),
        "3345678",
        new DateTime(),
        "SIP/abcd-000001",
        "main"
      )
      val queueCall2 = QueueCall(
        3,
        Some("Bbb"),
        "44445678",
        new DateTime(),
        "SIP/abcd-000002",
        "main"
      )

      configRepository.queueCalls =
        Map(queue -> Map(uniqueId1 -> queueCall1, uniqueId2 -> queueCall2))
      configRepository.onQueueCallTransferred(
        uniqueId2,
        uniqueId3
      ) shouldEqual List("foo")

      configRepository.queueCalls =
        Map(queue -> Map(uniqueId1 -> queueCall1, uniqueId3 -> queueCall2))
    }

    "remove all calls from all queues" in {

      val queue1    = "foo"
      val queue2    = "bar"
      val uniqueId1 = "123456.789"
      val uniqueId2 = "123456.999"
      val queueCall1 = QueueCall(
        2,
        Some("foo"),
        "3345678",
        new DateTime(),
        "SIP/abcd-000001",
        "main"
      )
      val queueCall2 = QueueCall(
        3,
        Some("bar"),
        "44445678",
        new DateTime(),
        "SIP/abcd-000002",
        "main"
      )

      configRepository.queueCalls = Map(
        queue1 -> Map(uniqueId1 -> queueCall1),
        queue2 -> Map(uniqueId2 -> queueCall2)
      )
      configRepository.removeQueueCallsFromMds("main")

      configRepository.queueCalls shouldEqual Map()

    }

    "remove calls from failing mds" in {

      val queue1    = "foo"
      val queue2    = "bar"
      val uniqueId1 = "123456.789"
      val uniqueId2 = "123456.999"
      val queueCall1 = QueueCall(
        2,
        Some("foo"),
        "3345678",
        new DateTime(),
        "SIP/abcd-000001",
        "main"
      )
      val queueCall2 = QueueCall(
        3,
        Some("bar"),
        "44445678",
        new DateTime(),
        "SIP/abcd-000002",
        "mds1"
      )

      configRepository.queueCalls = Map(
        queue1 -> Map(uniqueId1 -> queueCall1),
        queue2 -> Map(uniqueId2 -> queueCall2)
      )
      configRepository.removeQueueCallsFromMds("mds1")

      configRepository.queueCalls shouldEqual Map(
        queue1 -> Map(uniqueId1 -> queueCall1)
      )

    }

    "not fail if the uniqueId is missing" in {
      val queue = "foo"
      configRepository.queueCalls = Map(queue -> Map())

      configRepository.onQueueCallFinished(queue, "123456.789")
      configRepository.queueCalls shouldEqual Map(queue -> Map())
    }

    "not fail if the queue is missing" in {
      configRepository.onQueueCallFinished("foo", "123456.789")
      configRepository.queueCalls shouldEqual Map()
    }

    "retrieve the calls on a queue" in {
      val queueName   = "foo"
      val queueId     = 10L
      val queueConfig = mock[QueueConfigUpdate]
      when(queueConfig.name).thenReturn(queueName)
      val queueCall = QueueCall(
        1,
        Some("bar"),
        "334578",
        new DateTime(),
        "123456789.123",
        "main"
      )

      configRepository.queues = Map(queueId -> queueConfig)
      configRepository.queueCalls =
        Map(queueName -> Map("123456.89" -> queueCall))

      configRepository.getQueueCalls(queueId) shouldEqual QueueCallList(
        queueId,
        List(queueCall)
      )
    }

    "retrieve the calls on a queue by name" in {
      val queueName   = "foo"
      val queueId     = 10L
      val queueConfig = mock[QueueConfigUpdate]
      when(queueConfig.id).thenReturn(queueId)
      when(queueConfig.name).thenReturn(queueName)
      val queueCall = QueueCall(
        1,
        Some("bar"),
        "334578",
        new DateTime(),
        "SIP/abcd-000001",
        "main"
      )

      configRepository.queues = Map(queueId -> queueConfig)
      configRepository.queueCalls =
        Map(queueName -> Map("123456.89" -> queueCall))

      configRepository.getQueueCalls(queueName) shouldEqual Some(
        QueueCallList(queueId, List(queueCall))
      )
    }

    "find longest waiting call in queue" in {

      val queue1    = "foo"
      val queue2    = "bar"
      val queue3    = "without_calls"
      val uniqueId1 = "123456.666"
      val uniqueId2 = "123456.777"
      val uniqueId3 = "123456.888"
      val uniqueId4 = "123456.999"

      val ts = new DateTime(2005, 3, 26, 12, 0, 0, 0)
      val queueCall1 = QueueCall(
        1,
        Some(queue1),
        "111111",
        ts.minus(Period.hours(2)),
        "SIP/abcd-000001",
        "main"
      )
      val queueCall2 = QueueCall(
        2,
        Some(queue1),
        "222222",
        ts.minus(Period.hours(3)),
        "SIP/abcd-000002",
        "main"
      )
      val queueCall3 = QueueCall(
        3,
        Some(queue1),
        "333333",
        ts.minus(Period.hours(1)),
        "SIP/abcd-000003",
        "main"
      )
      val queueCall4 = QueueCall(
        1,
        Some(queue2),
        "444444",
        ts.minus(Period.hours(4)),
        "SIP/abcd-000004",
        "main"
      )

      configRepository.queueCalls = Map(
        queue1 -> Map(
          uniqueId1 -> queueCall1,
          uniqueId2 -> queueCall2,
          uniqueId3 -> queueCall3
        ),
        queue2 -> Map(uniqueId4 -> queueCall4),
        queue3 -> Map()
      )

      configRepository
        .findWaitingCallsStatistics(queue1)
        .contains(
          WaitingCallsStatistics(3, ts.minus(Period.hours(3)))
        ) shouldBe true
      configRepository
        .findWaitingCallsStatistics(queue2)
        .contains(
          WaitingCallsStatistics(1, ts.minus(Period.hours(4)))
        ) shouldBe true
      configRepository.findWaitingCallsStatistics(queue3) shouldEqual None
    }

    "get the agent number by cti username" in {
      val ctiUserName = "toto"
      val agent       = Agent(54L, "John", "Doe", "1000", "default")
      val userId      = 21L
      configRepository.ctiUsers = Map(
        userId -> XivoUser(
          userId,
          None,
          None,
          "Toto",
          Some("Doe"),
          Some(ctiUserName),
          None,
          None,
          None
        )
      )
      configRepository.userAgents = Map(userId -> agent.id)
      configRepository.agents = Map(agent.id -> agent)

      configRepository.agentFromUsername(ctiUserName) shouldEqual Some(agent)
    }

    "get the device interface by cti username" in {
      val ctiUserName = "toto"
      val lineName    = "aasfeg"
      val userId      = 21L
      configRepository.ctiUsers = Map(
        userId -> XivoUser(
          userId,
          None,
          None,
          "Toto",
          Some("Doe"),
          Some(ctiUserName),
          None,
          None,
          None
        )
      )
      configRepository.userLines = Map(
        userId -> makeLine(12, "default", "sip", lineName, None, None, "ip")
      )

      configRepository.interfaceFromUsername(ctiUserName) shouldEqual Some(
        s"SIP/$lineName"
      )
    }

    "get the device interface name without _w if unique account peer" in {
      val ctiUserName = "matt"
      val lineName    = "line_w"
      val userId      = 22L
      configRepository.ctiUsers = Map(
        userId -> XivoUser(
          userId,
          None,
          None,
          "Matt",
          Some("Duff"),
          Some(ctiUserName),
          None,
          None,
          None
        )
      )
      configRepository.userLines = Map(
        userId -> makeLine(
          12,
          "default",
          "sip",
          lineName,
          None,
          None,
          "ip",
          ua = true
        )
      )

      configRepository.interfaceFromUsername(ctiUserName) shouldEqual Some(
        s"SIP/line"
      )
    }

    "get the device interface name if not unique account peer" in {
      val ctiUserName = "matt"
      val lineName    = "line_w"
      val userId      = 22L
      configRepository.ctiUsers = Map(
        userId -> XivoUser(
          userId,
          None,
          None,
          "Matt",
          Some("Duff"),
          Some(ctiUserName),
          None,
          None,
          None
        )
      )
      configRepository.userLines = Map(
        userId -> makeLine(12, "default", "sip", lineName, None, None, "ip")
      )

      configRepository.interfaceFromUsername(ctiUserName) shouldEqual Some(
        s"SIP/$lineName"
      )
    }

    "return the groupId of the agent" in {
      val agent1 = Agent(1L, "Toto", "Doe", "1000", "default", 10)
      val agent2 = Agent(2L, "John", "Luke", "1001", "default", 20)
      configRepository.agents = Map(agent1.id -> agent1, agent2.id -> agent2)

      configRepository.groupIdForAgent(agent1.id) shouldEqual Some(10)
    }

    "return the phone number of the user" in {
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
      val line = makeLine(112, "default", "SIP", "ajkoas", None, None, "ip")
      val nb   = "44580"
      configRepository.updateUser(user)
      configRepository.updateUserLine(user.id, line)
      configRepository.updateLinePhoneNb(line.id, nb)

      configRepository.phoneNumberForUser(user.username.get) shouldEqual Some(
        nb
      )
    }
  }
  "a user repository" should {
    "find a user by phone number" in {
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
      val line = makeLine(112, "default", "SIP", "ajkoas", None, None, "ip")
      val nb   = "44580"
      configRepository.updateUser(user)
      configRepository.updateLineUser(line.id, user.id)
      configRepository.updateLinePhoneNb(line.id, nb)

      configRepository.userNameFromPhoneNb(nb) shouldEqual user.username
    }

    "find a user by agent Id" in {
      val agentId: Agent.Id = 24L

      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
      val agent = Agent(
        agentId,
        user.firstname,
        user.lastname.get,
        "2300",
        "default",
        12,
        user.id
      )

      configRepository.updateUser(user)
      configRepository.addAgent(agent)

      configRepository.getCtiUserByAgentId(agentId) should be(Some(user))
    }

    "get the queues for an agent" in {
      val agentId = 12
      configRepository.updateOrAddQueueMembers(AgentQueueMember(agentId, 3, 0))
      configRepository.updateOrAddQueueMembers(AgentQueueMember(agentId, 4, 0))
      configRepository.updateOrAddQueueMembers(
        AgentQueueMember(agentId + 1, 5, 0)
      )

      configRepository.getQueuesForAgent(agentId) should contain.allOf(
        AgentQueueMember(agentId, 3, 0),
        AgentQueueMember(agentId, 4, 0)
      )
    }

    "convert AgentConfigUpdate to Agent" in {
      val qm = QueueMember(
        "queue1",
        1L,
        "Agent/1001",
        1,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        1
      )
      val acu = AgentConfigUpdate(
        1L,
        "firstname",
        "lastname",
        "1001",
        "default",
        List(qm),
        2L,
        Some(3L)
      )

      val expected =
        Agent(1L, "firstname", "lastname", "1001", "default", 2L, 3L)

      configRepository.convertAgentConfigUpdateToAgent(acu) shouldEqual expected
    }

    "convert AgentConfigUpdate to AgentQueueMember" in {
      val qm1 = QueueMember(
        "queue1",
        queue_id = 1L,
        "Agent/1001",
        penalty = 0,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        1
      )
      val qm2 = QueueMember(
        "queue2",
        queue_id = 2L,
        "Agent/1001",
        penalty = 3,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        1
      )
      val acu = AgentConfigUpdate(
        id = 1L,
        "firstname",
        "lastname",
        "1001",
        "default",
        List(qm1, qm2),
        2L,
        Some(3L)
      )

      val expected =
        List(AgentQueueMember(1L, 1L, 0), AgentQueueMember(1L, 2L, 3))

      configRepository.convertAgentConfigUpdateToQueueMember(
        acu
      ) shouldEqual expected
    }

    "fetch all AgentQueueMember of given agent" in {
      val agentId = 12
      val aqm1    = AgentQueueMember(agentId, 3, 0)
      val aqm2    = AgentQueueMember(agentId, 4, 0)
      val aqm3    = AgentQueueMember(agentId + 1, 5, 0)
      configRepository.updateOrAddQueueMembers(aqm1)
      configRepository.updateOrAddQueueMembers(aqm2)
      configRepository.updateOrAddQueueMembers(aqm3)

      configRepository
        .filterOutQueueMember(agentId) should contain.allOf(aqm1, aqm2)
    }

    "remove queue member on queue member removed" in {
      val agentId = 12
      val aqm1    = AgentQueueMember(agentId, 3, 0)
      val aqm2    = AgentQueueMember(agentId, 4, 0)
      val aqm3    = AgentQueueMember(agentId + 1, 5, 0)
      configRepository.updateOrAddQueueMembers(aqm1)
      configRepository.updateOrAddQueueMembers(aqm2)
      configRepository.updateOrAddQueueMembers(aqm3)

      configRepository.removeQueueMember(aqm1)

      val agQMembers = configRepository.getAgentQueueMembers()

      agQMembers should not contain AgentQueueMember(12L, 3, 0)
      agQMembers should contain(AgentQueueMember(12L, 4, 0))
      agQMembers should contain(AgentQueueMember(13L, 5, 0))
    }

    "get agent queue members to remove" in {
      val agentId = 12
      val aqm1    = AgentQueueMember(agentId, 3, 0)
      val aqm2    = AgentQueueMember(agentId, 4, 0)
      val aqm3    = AgentQueueMember(agentId, 5, 0)
      configRepository.updateOrAddQueueMembers(aqm1)
      configRepository.updateOrAddQueueMembers(aqm2)
      configRepository.updateOrAddQueueMembers(aqm3)

      val updatedAgentQueueMemberList = List(aqm1)

      configRepository.getAgentQueueMembersToRemove(
        updatedAgentQueueMemberList
      ) should contain.allOf(aqm2, aqm3)
    }

    "store user services" in {
      val services = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )

      configRepository.onUserServicesUpdated(123, services)
      configRepository.getUserServices(123) shouldBe Some(services)
    }

    "store extension pattern" in {
      val patterns = List(
        ExtensionPattern(ExtensionName.ProgrammableKey, "_*735."),
        ExtensionPattern(ExtensionName.DND, "*25"),
        ExtensionPattern(ExtensionName.UnconditionalForward, "_*21."),
        ExtensionPattern(ExtensionName.NoAnswerForward, "_*22."),
        ExtensionPattern(ExtensionName.BusyForward, "_*23.")
      )

      patterns.foreach(p => {
        configRepository.updateExtensionPattern(p)
        configRepository.getExtensionPattern(p.name) shouldBe Some(p)
      })
    }
  }

  "ICE repository" should {

    "create and save an IceConfig with stun and turn from an IceServer" in {
      val ttl    = 84600
      val secret = "secret"
      when(turnServerConfig.turnSecretTtl).thenReturn(ttl.toLong)
      when(turnServerConfig.turnSecret).thenReturn(secret)
      when(turnServerConfig.turnEnable).thenReturn(true)
      when(turnServerConfig.stunEnable).thenReturn(true)

      configRepository.updateIceConfig(IceServer(Some("host:3478")))
      val iceConfig = configRepository.getIceConfig

      iceConfig.stunConfig shouldBe Some(StunConfig(List("stun:host:3478")))
      val turnConfig = iceConfig.turnConfig.get
      turnConfig.urls shouldBe List("turn:host:3478", "turns:host:3478")
      turnConfig.ttl shouldBe ttl
      configRepository.createPassword(
        turnConfig.username,
        secret
      ) shouldBe turnConfig.credential
    }

    "create and save an IceConfig with stun only from an IceServer" in {
      val ttl    = 84600
      val secret = "secret"
      when(turnServerConfig.turnSecretTtl).thenReturn(ttl.toLong)
      when(turnServerConfig.turnSecret).thenReturn(secret)
      when(turnServerConfig.turnEnable).thenReturn(false)
      when(turnServerConfig.stunEnable).thenReturn(true)

      configRepository.updateIceConfig(IceServer(Some("host:3478")))
      val iceConfig = configRepository.getIceConfig

      iceConfig.stunConfig shouldBe Some(StunConfig(List("stun:host:3478")))
      iceConfig.turnConfig shouldBe None
    }

    "create and save an IceConfig with turn only from an IceServer" in {
      val ttl    = 84600
      val secret = "secret"
      when(turnServerConfig.turnSecretTtl).thenReturn(ttl.toLong)
      when(turnServerConfig.turnSecret).thenReturn(secret)
      when(turnServerConfig.turnEnable).thenReturn(true)
      when(turnServerConfig.stunEnable).thenReturn(false)

      configRepository.updateIceConfig(IceServer(Some("host:3478")))
      val iceConfig = configRepository.getIceConfig

      iceConfig.stunConfig shouldBe None
      val turnConfig = iceConfig.turnConfig.get
      turnConfig.urls shouldBe List("turn:host:3478", "turns:host:3478")
      turnConfig.ttl shouldBe ttl
      configRepository.createPassword(
        turnConfig.username,
        secret
      ) shouldBe turnConfig.credential
    }

    "create and save an IceConfig without stun and turn from an IceServer" in {
      val ttl    = 84600
      val secret = "secret"
      when(turnServerConfig.turnSecretTtl).thenReturn(ttl.toLong)
      when(turnServerConfig.turnSecret).thenReturn(secret)
      when(turnServerConfig.turnEnable).thenReturn(false)
      when(turnServerConfig.stunEnable).thenReturn(false)

      configRepository.updateIceConfig(IceServer(Some("host:3478")))
      val iceConfig = configRepository.getIceConfig

      iceConfig.stunConfig shouldBe None
      iceConfig.turnConfig shouldBe None
    }

    "create and save an empty IceConfig if empty IceServer" in {
      val ttl    = 84600
      val secret = "secret"
      when(turnServerConfig.turnSecretTtl).thenReturn(ttl.toLong)
      when(turnServerConfig.turnSecret).thenReturn(secret)
      when(turnServerConfig.turnEnable).thenReturn(true)
      when(turnServerConfig.stunEnable).thenReturn(true)

      configRepository.updateIceConfig(IceServer(None))
      configRepository.getIceConfig shouldBe IceConfig(None, None)
    }

    "updates mobile app config status" in {
      val valid = true
      configRepository.mobileConfigIsValid shouldBe !valid
      configRepository.updateMobileConfigStatus(valid)

      configRepository.mobileConfigIsValid shouldBe valid
    }
  }

  private def addAgent(ag: Agent): Unit = configRepository.addAgent(ag)

  private def addQueue(id: Int, name: String, number: String = ""): Unit = {
    val qcf = QueueConfigUpdate(
      id,
      name,
      "Queue One",
      number,
      Some("default"),
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      "url",
      "",
      Some(1),
      Some("preprocess_subroutine"),
      1,
      Some(1),
      Some(1),
      1,
      "recorded",
      1
    )

    configRepository.updateQueueConfig(qcf)
  }

  private def putAgentInQueue(
      agentId: Agent.Id,
      queueId: Long,
      penalty: Int
  ): Unit = {
    configRepository.agentQueueMembers = AgentQueueMember(
      agentId,
      queueId,
      penalty
    ) :: configRepository.agentQueueMembers
  }

  object AgentBuilderP {
    var agentId: Agent.Id = 0L
    val context           = "default"
    class AgentBuilder(
        firstName: String,
        lastName: String,
        number: String,
        groupId: Option[Long],
        queueId: Option[Long],
        penalty: Option[Int]
    ) {
      def inGroup(id: Long) =
        new AgentBuilder(
          firstName,
          lastName,
          number,
          Some(id),
          queueId,
          penalty
        )
      def inQueue(id: Long) =
        new AgentBuilder(
          firstName,
          lastName,
          number,
          groupId,
          Some(id),
          penalty
        )
      def withPenalty(pen: Int) =
        new AgentBuilder(
          firstName,
          lastName,
          number,
          groupId,
          queueId,
          Some(pen)
        )

      def build(): Agent = {
        agentId = agentId + 1
        val agent = Agent(
          agentId,
          firstName,
          lastName,
          number,
          context,
          groupId.getOrElse(0)
        )
        configRepository.addAgent(agent)
        configRepository.agentQueueMembers = AgentQueueMember(
          agent.id,
          queueId.get,
          penalty.getOrElse(0)
        ) :: configRepository.agentQueueMembers
        agent
      }
    }

    def addInConfig(firstName: String, lastName: String, number: String) =
      new AgentBuilder(firstName, lastName, number, None, None, None)

    class AgentB(agent: Agent) {
      def inQueue(queueId: Long): Unit =
        configRepository.agentQueueMembers = AgentQueueMember(
          agent.id,
          queueId,
          0
        ) :: configRepository.agentQueueMembers
    }
    def putAgent(agent: Agent) = new AgentB(agent)
  }
}
