package services.config

import org.apache.pekko.testkit.TestActorRef
import pekkotest.TestKitSpec
import models.{QueueMembership, UserQueueDefaultMembership, UsersQueueMembership}
import org.mockito.Mockito.{reset, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import services.XucEventBus
import services.XucEventBus.XucEvent
import services.config.ObjectType
import services.request.SetUsersDefaultMembership

import scala.concurrent.Future

/**
  */
class DefaultQueueMembershipRepositorySpec
    extends TestKitSpec("DefaultQueueMembershipRepository")
    with MockitoSugar {

  val requester: ConfigServerRequester = mock[ConfigServerRequester]
  val bus: XucEventBus                 = mock[XucEventBus]
  val topic: XucEventBus.Topic                            = XucEventBus.configTopic(ObjectType.TypeBaseQueueMember)

  def actor: (TestActorRef[DefaultQueueMembershipRepository], DefaultQueueMembershipRepository) = {
    val a = TestActorRef[DefaultQueueMembershipRepository](
      new DefaultQueueMembershipRepository(requester, bus)
    )
    (a, a.underlyingActor)
  }

  "DefaultQueueMembershipRepository" should {
    "load base configuration upon startup" in {
      val defaultMembership = List(
        UserQueueDefaultMembership(
          1,
          List(QueueMembership(1, 2), QueueMembership(2, 3))
        ),
        UserQueueDefaultMembership(
          3,
          List(QueueMembership(1, 4), QueueMembership(2, 5))
        )
      )

      when(requester.getAllDefaultMembership).thenReturn(
        Future.successful(defaultMembership)
      )
      val (ref, repo) = actor
      verify(requester).getAllDefaultMembership
    }

    "get base configuration of user" in {
      val defaultMembership = List(
        UserQueueDefaultMembership(
          1,
          List(QueueMembership(1, 2), QueueMembership(2, 3))
        ),
        UserQueueDefaultMembership(
          3,
          List(QueueMembership(1, 4), QueueMembership(2, 5))
        )
      )

      when(requester.getAllDefaultMembership).thenReturn(
        Future.successful(defaultMembership)
      )
      val (ref, repo) = actor
      ref ! GetEntry[Long, List[QueueMembership]](1)
      expectMsg(
        UserQueueDefaultMembership(
          1,
          List(QueueMembership(1, 2), QueueMembership(2, 3))
        )
      )
    }

    "set base configuration of user and publish update to bus" in {
      reset(bus)
      when(requester.getAllDefaultMembership).thenReturn(
        Future.successful(List.empty)
      )
      val membership = List(QueueMembership(1, 2), QueueMembership(2, 3))
      when(requester.setUserDefaultMembership(1, membership))
        .thenReturn(Future.successful(()))
      val (ref, repo) = actor

      ref ! SetEntry[Long, List[QueueMembership]](1, membership)
      ref ! GetEntry[Long, List[QueueMembership]](1)
      expectMsg(UserQueueDefaultMembership(1, membership))

      verify(bus).publish(
        XucEvent(topic, UserQueueDefaultMembership(1, membership))
      )

    }

    "replace base configuration and publish update to bus" in {
      reset(bus)
      when(requester.getAllDefaultMembership).thenReturn(
        Future.successful(List.empty)
      )
      val membership  = List(QueueMembership(1, 2), QueueMembership(2, 3))
      val membership2 = List(QueueMembership(2, 3))
      when(requester.setUserDefaultMembership(1, membership))
        .thenReturn(Future.successful(()))
      val (ref, repo) = actor

      ref ! SetEntry[Long, List[QueueMembership]](1, membership)
      ref ! GetEntry[Long, List[QueueMembership]](1)
      expectMsg(UserQueueDefaultMembership(1, membership))

      verify(bus).publish(
        XucEvent(topic, UserQueueDefaultMembership(1, membership))
      )

      ref ! SetEntry[Long, List[QueueMembership]](1, membership2)
      ref ! GetEntry[Long, List[QueueMembership]](1)
      expectMsg(UserQueueDefaultMembership(1, membership2))

      verify(bus).publish(
        XucEvent(topic, UserQueueDefaultMembership(1, membership2))
      )

    }

    "set base configuration of several users and publish update to bus" in {
      when(requester.getAllDefaultMembership).thenReturn(
        Future.successful(List.empty)
      )
      val membership = List(QueueMembership(1, 2), QueueMembership(2, 3))
      when(
        requester.setUsersDefaultMembership(
          UsersQueueMembership(List(1, 3), membership)
        )
      ).thenReturn(Future.successful(()))
      reset(bus)

      val (ref, repo) = actor
      ref ! SetUsersDefaultMembership(List(1, 3), membership)
      ref ! GetEntry[Long, List[QueueMembership]](1)
      expectMsg(UserQueueDefaultMembership(1, membership))
      ref ! GetEntry[Long, List[QueueMembership]](3)
      expectMsg(UserQueueDefaultMembership(3, membership))

      verify(bus).publish(
        XucEvent(topic, UserQueueDefaultMembership(1, membership))
      )
      verify(bus).publish(
        XucEvent(topic, UserQueueDefaultMembership(3, membership))
      )
    }

  }
}
