package services.config

import org.apache.pekko.actor.{PoisonPill, Props}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.asteriskjava.manager.action.{
  ExtensionStateAction,
  ExtensionStateListAction
}
import org.asteriskjava.manager.response.ExtensionStateResponse
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.{reset, verify}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.duration._
import services.XucAmiBus._
import services.config.ExtensionManager.GetExtensionStatus
import services.XucAmiBus
import org.scalatest.matchers.should.Matchers

class ExtensionManagerSpec
    extends TestKitSpec("ExtensionManagerSpec")
    with MockitoSugar
    with Matchers {

  class Helper {
    val xucAmiBus: XucAmiBus = mock[XucAmiBus]
    val configDispatcher: TestProbe     = TestProbe()

    def actor: (TestActorRef[ExtensionManager], ExtensionManager) = {
      val a = TestActorRef[ExtensionManager](
        Props(new ExtensionManager(xucAmiBus, configDispatcher.ref))
      )
      (a, a.underlyingActor)
    }

  }

  "Extension manager" should {
    "subscribe to ami bus on start" in new Helper {
      val (ref, _) = actor
      verify(xucAmiBus).subscribe(ref, AmiType.AmiResponse)
      verify(xucAmiBus).subscribe(ref, AmiType.AmiService)
    }

    "request all extension state when connected to a MDS" in new Helper {
      val (ref, _) = actor
      ref ! AmiConnected("mds2")
      val arg: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(xucAmiBus).publish(arg.capture)
      val action: AmiAction = arg.getValue
      action.message shouldBe a[ExtensionStateListAction]
      action.requester shouldBe Some(ref)
      action.targetMds shouldBe Some("mds2")
    }

    "unsubscribe from ami bus on stop" in new Helper {
      val (ref, _) = actor
      ref ! PoisonPill
      verify(xucAmiBus).unsubscribe(ref)
    }

    "publish extension state event on AMI bus" in new Helper {
      val (ref, _) = actor
      reset(xucAmiBus)

      val number = "1000"

      ref ! GetExtensionStatus("1000")

      val arg: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(xucAmiBus).publish(arg.capture)
      val action: AmiAction = arg.getValue
      action.message shouldBe a[ExtensionStateAction]
      val message: ExtensionStateAction = action.message.asInstanceOf[ExtensionStateAction]
      message.getExten shouldBe "1000"
      action.requester shouldBe Some(ref)
    }

    "send extension status to config dispatcher" in new Helper {
      val (ref, _) = actor

      val action = new ExtensionStateAction()
      val resp   = new ExtensionStateResponse()
      resp.setHint("")
      resp.setExten("1000")
      resp.setStatus(0)
      resp.setStatusText("Idle")

      ref ! AmiResponse((resp, Some(AmiAction(action))))

      val msg: AmiExtensionStatusEvent =
        configDispatcher.expectMsgType[AmiExtensionStatusEvent]
      msg.message.getHint should be("")
      msg.message.getStatus should be(0)
      msg.message.getStatustext should be("Idle")
      msg.message.getExten should be("1000")
    }

    "ignore extension status less than 0 to filter not existing extension on mds" in new Helper {
      val (ref, _) = actor

      val action = new ExtensionStateAction()
      val resp   = new ExtensionStateResponse()
      resp.setHint("")
      resp.setExten("1000")
      resp.setStatus(-1)
      resp.setStatusText("")

      ref ! AmiResponse((resp, Some(AmiAction(action))))

      configDispatcher.expectNoMessage(200.millis)
    }
  }
}
