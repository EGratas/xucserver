package services.config

import org.apache.pekko.pattern.ask
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import org.apache.pekko.util.Timeout
import pekkotest.TestKitSpec
import controllers.helpers.{RequestError, RequestResult, RequestSuccess}
import org.mockito.Mockito.{timeout => mtimeout}
import org.mockito.Mockito.{verify, when}
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import play.api.libs.ws.WSResponse
import services.config.ConfigInitializer.UpdateQueue
import services.config.ConfigServiceManager.{
  ExportQualificationsCsv,
  GetAgentConfigAll,
  GetIceServer,
  GetMobileConfig,
  GetQueueConfigAll
}
import xivo.models.{
  AgentConfigUpdate,
  IceServer,
  MobileAppConfig,
  QueueConfigUpdate,
  QueueMember
}
import xivo.network.WebServiceException
import xivo.xuc.ConfigServerConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class ConfigServiceManagerSpec
    extends TestKitSpec("ConfigServiceManagerSpec")
    with MockitoSugar
    with ScalaFutures {

  implicit val defaultPatience: PatienceConfig =
    PatienceConfig(timeout = Span(2, Seconds), interval = Span(5, Millis))

  class Helper {
    val configMgtMock: ConfigServerRequester   = mock[ConfigServerRequester]
    val configServerConfig: ConfigServerConfig = mock[ConfigServerConfig]
    val wsResponse: WSResponse                 = mock[WSResponse]
    val queueId: Long                          = 1L
    val configInitializer: ConfigInitializer   = mock[ConfigInitializer]

    val fromRefTime: String = "2016-01-01 00:00:00"
    val toRefTime: String   = "2018-12-12 00:00:00"

    val configRepo: ConfigRepository = mock[ConfigRepository]
    val router: TestProbe     = TestProbe()

    def actor(retryDelay: FiniteDuration = 2.minutes, retryCount: Int = 1): (TestActorRef[ConfigServiceManager], ConfigServiceManager) = {
      when(configServerConfig.retryDelay).thenReturn(retryDelay)
      when(configServerConfig.retryCount).thenReturn(retryCount)
      val a = TestActorRef(
        new ConfigServiceManager(configMgtMock, configServerConfig)
      )
      (a, a.underlyingActor)
    }

  }

  "ConfigServiceManager actor " should {
    "ask for the CSV of qualification answers" in new Helper {
      implicit val timeout: Timeout = Timeout(100.milliseconds)
      val result: String   = "csv content"
      when(
        configMgtMock.exportQualificationsCsv(queueId, fromRefTime, toRefTime)
      ).thenReturn(Future(result))

      var (ref, a) = actor()

      val res: Future[RequestResult] =
        (ref ? ExportQualificationsCsv(queueId, fromRefTime, toRefTime))
          .mapTo[RequestResult]
      res.futureValue shouldEqual RequestSuccess(result)
    }

    "ask for the CSV of qualification answers and return RequestError in case of failure" in new Helper {
      implicit val timeout: Timeout = Timeout(100.milliseconds)
      val result: String   = "csv content"
      when(
        configMgtMock.exportQualificationsCsv(queueId, fromRefTime, toRefTime)
      ).thenReturn(Future.failed(new WebServiceException("Error message")))

      var (ref, a) = actor()

      val res: Future[RequestResult] =
        (ref ? ExportQualificationsCsv(queueId, fromRefTime, toRefTime))
          .mapTo[RequestResult]

      res.futureValue shouldEqual RequestError("Error message")
    }

    "retrieve all QueueConfigUpdate and tell the sender" in new Helper {
      implicit val timeout: Timeout = Timeout(100.milliseconds)
      val result: List[QueueConfigUpdate] =
        List(mock[QueueConfigUpdate], mock[QueueConfigUpdate])
      when(configMgtMock.getQueueConfigAll).thenReturn(Future(result))

      var (ref, a) = actor()

      ref ! GetQueueConfigAll
      expectMsg(UpdateQueue(result(0)))
      expectMsg(UpdateQueue(result(1)))
    }

    "retry to get all QueueConfigUpdate when first attempt fails" in new Helper {
      implicit val timeout: Timeout = Timeout(100.milliseconds)
      val result: List[QueueConfigUpdate] =
        List(mock[QueueConfigUpdate], mock[QueueConfigUpdate])
      when(configMgtMock.getQueueConfigAll).thenReturn(
        Future.failed(new WebServiceException("Uh oh !"))
      )

      var (ref, a) = actor(1.millisecond, 2)

      ref ! GetQueueConfigAll

      verify(configMgtMock, mtimeout(500).times(2)).getQueueConfigAll
    }

    "retrieve all AgentConfigUpdate and tell the repository" in new Helper {
      implicit val timeout: Timeout = Timeout(100.milliseconds)

      val agentMember: QueueMember = QueueMember(
        "queue1",
        1L,
        "Agent/1001",
        1,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        1
      )
      val agCfg1: AgentConfigUpdate = AgentConfigUpdate(
        1L,
        "Isabel",
        "Smith",
        "1010",
        "default",
        List(agentMember),
        1L,
        Some(0L)
      )
      val agCfg2: AgentConfigUpdate = AgentConfigUpdate(
        2L,
        "John",
        "Doe",
        "1011",
        "default",
        List(agentMember),
        1L,
        Some(0L)
      )

      when(configMgtMock.getAgentConfigAll).thenReturn(
        Future(List(agCfg1, agCfg2))
      )

      var (ref, a) = actor()

      ref ! GetAgentConfigAll
      expectMsgAllOf(agCfg1, agCfg2)
    }

    "retry to get all AgentConfigUpdate when first attempt fails" in new Helper {
      implicit val timeout: Timeout = Timeout(100.milliseconds)
      when(configMgtMock.getAgentConfigAll).thenReturn(
        Future.failed(new WebServiceException("Uh Oh!"))
      )

      var (ref, a) = actor(1.millisecond, 2)

      ref ! GetAgentConfigAll
      verify(configMgtMock, mtimeout(500).times(2)).getAgentConfigAll
    }

    "retrieve IceServer and tell the repository" in new Helper {
      implicit val timeout: Timeout  = Timeout(100.milliseconds)
      val iceSrv: IceServer = IceServer(Some("host:3478"))
      when(configMgtMock.getIceServer).thenReturn(Future(iceSrv))

      val (ref, _) = actor()

      ref ! GetIceServer
      expectMsgAllOf(iceSrv)
    }

    "retry to get IceServer when first attempt fails" in new Helper {
      implicit val timeout: Timeout = Timeout(100.milliseconds)
      when(configMgtMock.getIceServer).thenReturn(
        Future.failed(new WebServiceException("Errrrrr !"))
      )

      val (ref, _) = actor(1.millisecond, 2)

      ref ! GetIceServer
      verify(configMgtMock, mtimeout(500).times(2)).getIceServer
    }

    "retrieve mobile App configuration status and tell the repository" in new Helper {
      implicit val timeout: Timeout                     = Timeout(100.milliseconds)
      val mobileConfigIsValid: MobileAppConfig = MobileAppConfig(true)
      when(configMgtMock.checkValidMobileAppSetup).thenReturn(
        Future(mobileConfigIsValid)
      )

      val (ref, _) = actor()

      ref ! GetMobileConfig
      expectMsgAllOf(mobileConfigIsValid)
    }

    "retry to get mobile App configuration when first attempt fails" in new Helper {
      implicit val timeout: Timeout = Timeout(100.milliseconds)
      when(configMgtMock.checkValidMobileAppSetup).thenReturn(
        Future.failed(new WebServiceException("Errrrrr !"))
      )

      val (ref, _) = actor(1.millisecond, 2)

      ref ! GetMobileConfig
      verify(configMgtMock, mtimeout(500).times(2)).checkValidMobileAppSetup
    }
  }
}
