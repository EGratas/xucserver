package services.config

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import command.{AgentInitLoggedIn, AgentInitLoggedOut}
import org.joda.time.DateTime
import org.mockito.Mockito.{verify, when}
import org.scalatestplus.mockito.MockitoSugar
import services.AgentActorFactory
import xivo.ami.AgentCallUpdate
import xivo.events.*
import xivo.models.Agent
import xivo.xucami.models.MonitorState

import scala.concurrent.duration.*

class AgentManagerSpec
    extends TestKitSpec("AgentManagerSpec")
    with MockitoSugar {

  class Helper {
    val agFSMFactory: AgentActorFactory = mock[AgentActorFactory]
    def actor: (TestActorRef[AgentManager], AgentManager) = {
      val a = TestActorRef[AgentManager](Props(new AgentManager(agFSMFactory)))
      (a, a.underlyingActor)
    }
    def actorWithFsm(agentId: Agent.Id): (TestActorRef[AgentManager], AgentManager, TestProbe) = {
      val (ref, agentManager) = actor
      val agentFSM            = TestProbe()
      agentManager.agFsms += (agentId -> agentFSM.ref)
      (ref, agentManager, agentFSM)
    }
  }

  "Agent Manager" should {

    "send agent call update to the FSM" in new Helper {
      val agentId: Agent.Id   = 665
      val (ref, agentManager) = actor
      val agentFSM: TestProbe = TestProbe()
      when(agFSMFactory.get(agentId)).thenReturn(Some(agentFSM.ref))

      val acu: AgentCallUpdate = AgentCallUpdate(agentId, MonitorState.ACTIVE)

      ref ! acu

      agentFSM.expectMsg(acu)
    }

    "forward EventAgentLogin to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54
      val agentEvent: EventAgentLogin = EventAgentLogin(agentId, phoneNumber, agentNumber)
      val agentFSM: TestProbe = TestProbe()

      when(agFSMFactory.get(agentId)).thenReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }

    "forward EventAgentPause to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54L
      val agentEvent: EventAgentPause = EventAgentPause(agentId)
      val agentFSM: TestProbe = TestProbe()

      when(agFSMFactory.get(agentId)).thenReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }
    "forward EventAgentUnPause to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54L
      val agentEvent: EventAgentUnPause = EventAgentUnPause(agentId)
      val agentFSM: TestProbe = TestProbe()

      when(agFSMFactory.get(agentId)).thenReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }
    "forward EventAgentWrapup to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54L
      val agentEvent: EventAgentWrapup = EventAgentWrapup(agentId)
      val agentFSM: TestProbe = TestProbe()

      when(agFSMFactory.get(agentId)).thenReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }

    "forward EventAgentLogout to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val agentNumber         = "2001"
      val agentId             = 55
      val agentEvent: EventAgentLogout = EventAgentLogout(agentId, agentNumber)
      val agentFSM: TestProbe = TestProbe()

      when(agFSMFactory.get(agentId)).thenReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }

    "get or create agent FSM and agent stat on init login state received and send event" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54
      val agentLoggedIn: AgentInitLoggedIn =
        AgentInitLoggedIn(agentId, agentNumber, phoneNumber, new DateTime())
      val loginEvent: EventAgentLogin = EventAgentLogin(agentId, phoneNumber, agentNumber)
      val agentFSM: TestProbe = TestProbe()
      val agentStatCollector: TestProbe = TestProbe()

      when(agFSMFactory.getOrCreate(agentId, agentNumber, agentManager.context))
        .thenReturn(agentFSM.ref)
      when(
        agFSMFactory.getOrCreateAgentStatCollector(
          agentId,
          agentManager.context
        )
      ).thenReturn(agentStatCollector.ref)

      ref ! agentLoggedIn

      verify(agFSMFactory).getOrCreate(
        agentId,
        agentNumber,
        agentManager.context
      )
      agentFSM.expectMsg(loginEvent)
    }
    "get or create agent FSM and agent stat on init logout state received and send event" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54
      val agentInitLoggedOut: AgentInitLoggedOut =
        AgentInitLoggedOut(agentId, agentNumber, new DateTime())
      val agentFSM: TestProbe = TestProbe()
      val agentStatCollector: TestProbe = TestProbe()

      when(agFSMFactory.getOrCreate(agentId, agentNumber, agentManager.context))
        .thenReturn(agentFSM.ref)
      when(
        agFSMFactory.getOrCreateAgentStatCollector(
          agentId,
          agentManager.context
        )
      ).thenReturn(agentStatCollector.ref)

      ref ! agentInitLoggedOut

      verify(agFSMFactory).getOrCreate(
        agentId,
        agentNumber,
        agentManager.context
      )
      agentFSM.expectNoMessage(200.millis)
    }

  }
}
