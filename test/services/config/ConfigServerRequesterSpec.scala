package services.config
import cats.data.Writer
import pekkotest.TestKitSpec
import models.*
import org.apache.pekko.stream.scaladsl.Source
import org.joda.time.{DateTime, LocalDate, LocalTime}
import org.mockito.Mockito.*
import org.mockito.ArgumentMatchers.*
import org.mockito.stubbing.OngoingStubbing
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.{JsObject, JsValue, Json, Writes}
import play.api.libs.ws.{BodyWritable, InMemoryBody, WSAuthScheme, WSBody, WSClient, WSCookie, WSProxyServer, WSRequest, WSRequestFilter, WSResponse, WSSignatureCalculator, writeableOf_String}
import play.api.mvc.{Cookie, MultipartFormData}
import play.api.test.FakeRequest
import play.api.test.Helpers.*
import xivo.models.*
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}
import xivo.network.{WebServiceException, XiVOWS}
import xivo.xuc.{ConfigServerConfig, XucBaseConfig}
import play.api.libs.ws.JsonBodyWritables.writeableOf_JsValue
import org.mockito.ArgumentMatchers.eq as mockitoEq
import org.mockito.MockingDetails
import org.apache.pekko.util.ByteString

import java.io.File
import java.net.URI
import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.Duration

class DummyWsReq(resp: WSResponse)(implicit ec: ExecutionContext) extends WSRequest with MockitoSugar {
  override def withBody[T: BodyWritable](body: T): Self =
    this

  override def execute(): Future[Response] =
    Future.successful {resp}

  override def withHeaders(headers: (String, String)*): WSRequest = ???
  override def withHttpHeaders(headers: (String, String)*): WSRequest = ???
  override def withQueryString(parameters: (String, String)*): WSRequest = ???
  override def withQueryStringParameters(parameters: (String, String)*): WSRequest = ???
  override def withCookies(cookie: WSCookie*): WSRequest = ???
  override def method: String = ???
  override def body: WSBody = ???
  override def headers: Map[String, Seq[String]] = ???
  override def queryString: Map[String, Seq[String]] = ???
  override def calc: Option[WSSignatureCalculator] = ???
  override def auth: Option[(String, String, WSAuthScheme)] = ???
  override def followRedirects: Option[Boolean] = ???
  override def requestTimeout: Option[Duration] = ???
  override def virtualHost: Option[String] = ???
  override def proxyServer: Option[WSProxyServer] = ???
  override def sign(calc: WSSignatureCalculator): WSRequest = ???
  override def withAuth(username: String, password: String, scheme: WSAuthScheme): WSRequest = ???
  override def withFollowRedirects(follow: Boolean): WSRequest = ???
  override def withRequestTimeout(timeout: Duration): WSRequest = ???
  override def withRequestFilter(filter: WSRequestFilter): WSRequest = ???
  override def withVirtualHost(vh: String): WSRequest = ???
  override def withProxyServer(proxyServer: WSProxyServer): WSRequest = ???
  override def withMethod(method: String): WSRequest = ???
  override def get(): Future[WSResponse] = ???
  override def post[T: BodyWritable](body: T): Future[WSResponse] = ???
  override def post(body: File): Future[WSResponse] = ???
  override def post(body: Source[MultipartFormData.Part[Source[ByteString, _]], _]): Future[WSResponse] = ???
  override def patch[T: BodyWritable](body: T): Future[WSResponse] = ???
  override def patch(body: File): Future[WSResponse] = ???
  override def patch(body: Source[MultipartFormData.Part[Source[ByteString, _]], _]): Future[WSResponse] = ???
  override def put[T: BodyWritable](body: T): Future[WSResponse] = ???
  override def put(body: File): Future[WSResponse] = ???
  override def put(body: Source[MultipartFormData.Part[Source[ByteString, _]], _]): Future[WSResponse] = ???
  override def delete(): Future[WSResponse] = ???
  override def head(): Future[WSResponse] = ???
  override def options(): Future[WSResponse] = ???
  override def execute(method: String): Future[WSResponse] = ???
  override def url: String = ???
  override def uri: URI = ???
  override def contentType: Option[String] = ???
  override def cookies: Seq[WSCookie] = ???
  override def withDisableUrlEncoding(disableUrlEncoding: Boolean): WSRequest = ???
  override def withUrl(url: String): WSRequest = ???
  override def stream(): Future[WSResponse] = ???
  override def addCookies(cookies: WSCookie*): WSRequest = ???
}

class ConfigServerRequesterSpec
    extends TestKitSpec("ConfigServerRequesterSpec")
    with MockitoSugar
    with ScalaFutures {

  val appliConfig: Map[String, Any] = {
    Map(
      "config.host"  -> "configIP",
      "config.port"  -> 9000,
      "config.token" -> "abcdef12456"
    )
  }

  implicit val defaultPatience: PatienceConfig =
    PatienceConfig(timeout = Span(2, Seconds), interval = Span(5, Millis))

  class Helper(conf: Map[String, Any] = appliConfig) {
    implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global
    val xivoWS: XiVOWS                   = mock[XiVOWS]
    val configWS: ConfigWS               = mock[ConfigWS]
    val requester: ConfigServerRequester = new ConfigServerRequester(configWS)
    val cookie: Cookie                   = Cookie("_eid", "g4jg34u9khf8vcu9e49keut")

    val wsReq: WSRequest   = mock[WSRequest]
    val wsResp: WSResponse = mock[WSResponse]

    when(configWS.ApiRelease2_0).thenReturn("2.0")
  }

  "A ReponseWS" should {
    "return an error with unparsable JSON" in new Helper with ResponseWS {

      when(wsResp.json).thenReturn(Json.parse("{}"))

      a[WebServiceException] should be thrownBy {
        processResponse[String](wsResp)
      }
    }
    "return an error with invalid response from config server" in new Helper
      with ResponseWS {

      when(wsResp.status).thenReturn(INTERNAL_SERVER_ERROR)

      a[WebServiceException] should be thrownBy {
        checkError(wsResp)
      }
    }
  }

  "A ConfigWS" should {
    class ConfigHelper(httpContext: String) {
      val xucConfig: XucBaseConfig   = mock[XucBaseConfig]
      val config: ConfigServerConfig = mock[ConfigServerConfig]
      val xivoWS: XiVOWS             = mock[XiVOWS]
      val wsClient: WSClient = mock[WSClient]
      val wsRequest: WSRequest = mock[WSRequest]

      val jsonBody: JsValue = Json.parse("""{"key":"value"}""")
      val username = "jbond"

      when(config.configHost).thenReturn("configIP")
      when(config.configPort).thenReturn(9000)
      when(config.configHttpContext).thenReturn(httpContext)
      when(xivoWS.WS).thenReturn(wsClient)
      when(wsClient.url(any[String])).thenReturn(wsRequest)
      when(wsRequest.withHttpHeaders(any[(String, String)]))
        .thenReturn(wsRequest)
      when(wsRequest.withRequestTimeout(any[Duration])).thenReturn(wsRequest)
      when(wsRequest.withMethod(any[String])).thenReturn(wsRequest)
      when(wsRequest.withBody(jsonBody)).thenReturn(wsRequest)
      when(wsRequest.addQueryStringParameters(("username", username)))
        .thenReturn(wsRequest)

      val configWs = new ConfigWS(xivoWS, config, xucConfig)

    }
    "generate URL when httpContext is empty" in new ConfigHelper("") {

      configWs.getConfigWsUrl(
        "test"
      ) shouldEqual "http://configIP:9000/api/1.0/test"
    }
    "generate URL when httpContext contains a path without /" in new ConfigHelper(
      "cfg"
    ) {

      configWs.getConfigWsUrl(
        "test"
      ) shouldEqual "http://configIP:9000/cfg/api/1.0/test"
    }
    "generate URL when httpContext contains a path with /" in new ConfigHelper(
      "cfgWith/"
    ) {

      configWs.getConfigWsUrl(
        "test"
      ) shouldEqual "http://configIP:9000/cfgWith/api/1.0/test"
    }
  }

  "A ConfigServerRequester" should {
    "list CallbackLists" in new Helper {
      val listUuid: UUID = UUID.randomUUID()
      val cbUuid: UUID = UUID.randomUUID()
      val periodUuid: UUID = UUID.randomUUID()
      val jsonResult: JsValue = Json.parse(s"""[{
             |"uuid":"$listUuid","name":"The List","queueId":3,
             |"callbacks":[{
             | "uuid": "$cbUuid","listUuid":"$listUuid", "phoneNumber": "1000", "company": "The company",
             | "preferredPeriod": {
             |   "uuid": "$periodUuid", "name": "morning", "periodStart": "08:00:00", "periodEnd": "11:00:00", "default": true
             | },
             | "dueDate": "2015-01-10"
             |}]
             |}]""".stripMargin)
      val period: PreferredCallbackPeriod = PreferredCallbackPeriod(
        Some(periodUuid),
        "morning",
        new LocalTime(8, 0, 0),
        new LocalTime(11, 0, 0),
        true
      )
      val result: List[CallbackList] = List(
        CallbackList(
          Some(listUuid),
          "The List",
          3L,
          List(
            CallbackRequest(
              Some(cbUuid),
              listUuid,
              Some("1000"),
              None,
              None,
              None,
              Some("The company"),
              None,
              preferredPeriod = Some(period),
              dueDate = new LocalDate(2015, 1, 10)
            )
          )
        )
      )

      when(configWS.request("callback_lists", "GET", null)).thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.json).thenReturn(jsonResult)

      val fValue: Future[List[CallbackList]] = requester.getCallbackLists

      whenReady(fValue) { _ =>
        fValue.futureValue shouldEqual result

      }
    }

    "post a csv to the config server and return Unit" in new Helper {
      val listUuid: String = UUID.randomUUID().toString
      val csv              = "213333|John"

      when(
        configWS.request(
          s"callback_lists/$listUuid/callback_requests/csv",
          "POST",
          null
        )
      ).thenReturn(new DummyWsReq(wsResp))
      when(wsResp.status).thenReturn(CREATED)

      whenReady(requester.importCsvCallback(listUuid, csv)) { res =>
        res shouldEqual ((): Unit)
      }
    }

    "send a take callback request" in new Helper {
      val uuid: String = UUID.randomUUID().toString
      val agent: Agent = Agent(12L, "John", "Doe", "1000", "default")

      when(configWS.request(s"callback_requests/$uuid/take", "POST", null))
        .thenReturn(new DummyWsReq(wsResp))
      when(wsResp.status).thenReturn(OK)

      whenReady(requester.takeCallback(uuid, agent)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/take", "POST")
      }
    }

    "send a release callback request" in new Helper {
      val uuid: String = UUID.randomUUID().toString

      when(configWS.request(s"callback_requests/$uuid/release", "POST", null))
        .thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)

      whenReady(requester.releaseCallback(uuid)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/release", "POST")
      }
    }

    "send a get all pending callback request for an agent" in new Helper {
      val uuid: UUID = UUID.randomUUID()
      val agentId = 12L

      when(
        configWS.request(
          s"callback_requests/agent/$agentId/taken",
          "GET",
          null
        )
      ).thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.parse(s"""{"ids": ["$uuid"]}"""))

      whenReady(requester.getTakenCallbacks(agentId)) { res =>
        res shouldEqual List(uuid)
        verify(configWS).request(
          s"callback_requests/agent/$agentId/taken",
          "GET"
        )
      }
      requester.getTakenCallbacks(agentId).futureValue shouldEqual List(uuid)
    }

    "retrieve a CallbackRequest by uuid" in new Helper {
      val uuid: UUID = UUID.randomUUID()
      val listUuid: UUID = UUID.randomUUID()

      when(configWS.request(s"callback_requests/$uuid", "GET", null))
        .thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.parse(s"""{
             | "uuid": "$uuid",
             | "listUuid":"$listUuid",
             | "phoneNumber": "1000",
             | "company": "The company",
             | "queueId": 23
             |}""".stripMargin))

      whenReady(requester.getCallbackRequest(uuid.toString)) { res =>
        res shouldEqual CallbackRequest(
          Some(uuid),
          listUuid,
          Some("1000"),
          None,
          None,
          None,
          Some("The company"),
          None,
          queueId = Some(23)
        )
      }
    }

    "find list of CallbackRequest matching criteria" in new Helper {

      val uuid1: UUID = UUID.randomUUID()
      val uuid2: UUID = UUID.randomUUID()
      val listUuid: UUID = UUID.randomUUID()

      val criteria: FindCallbackRequest = FindCallbackRequest(
        List(
          DynamicFilter("listUuid", Some(OperatorEq), Some(listUuid.toString))
        )
      )
      val payload: JsValue = Json.toJson(criteria)

      val expected: FindCallbackResponse = FindCallbackResponse(
        2,
        List(
          CallbackRequest(
            Some(uuid1),
            listUuid,
            Some("1000"),
            None,
            None,
            None,
            Some("The company"),
            None,
            queueId = Some(23)
          ),
          CallbackRequest(
            Some(uuid2),
            listUuid,
            Some("1000"),
            None,
            None,
            None,
            Some("The company"),
            None,
            queueId = Some(23)
          )
        )
      )

      val jsonResponse: JsValue = Json.toJson(expected)

      when(configWS.request(s"callback_requests/find", "POST", null))
        .thenReturn(new DummyWsReq(wsResp))

      when(wsResp.json).thenReturn(jsonResponse)
      when(wsResp.status).thenReturn(OK)

      val f: Future[FindCallbackResponse] = requester.findCallbackRequest(criteria)

      whenReady(f)(res => {
        res.list should contain(expected.list.head)
        verify(configWS).request(s"callback_requests/find", "POST")
      })
    }

    "cloture a CallbackRequest" in new Helper {
      val uuid: UUID = UUID.randomUUID()

      when(configWS.request(s"callback_requests/$uuid/cloture", "POST", null))
        .thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)

      whenReady(requester.clotureRequest(uuid)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/cloture", "POST")
      }
    }

    "uncloture a CallbackRequest" in new Helper {
      val uuid: UUID = UUID.randomUUID()

      when(configWS.request(s"callback_requests/$uuid/uncloture", "POST", null))
        .thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)

      whenReady(requester.unclotureRequest(uuid)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/uncloture", "POST")
      }
    }

    "reschedule a CallbackRequest" in new Helper {
      import RescheduleCallback._

      val uuid: UUID = UUID.randomUUID()
      val reschedule: RescheduleCallback = RescheduleCallback(
        DateTime.parse("2016-08-09"),
        "268ce207-7619-4d79-bcf7-27215e8636eb"
      )
      val json: JsValue = Json.toJson(reschedule)

      when(
        configWS.request(s"callback_requests/$uuid/reschedule", "POST", null)
      ).thenReturn(new DummyWsReq(wsResp))
      when(wsResp.status).thenReturn(OK)

      whenReady(requester.rescheduleCallback(uuid, reschedule)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/reschedule", "POST")
      }
    }

    "Get preferred callback period" in new Helper {
      val uuid: UUID = UUID.randomUUID()
      val listUuid: UUID = UUID.randomUUID()
      val pid1: UUID = UUID.randomUUID()
      val pid2: UUID = UUID.randomUUID()

      val morning: PreferredCallbackPeriod = PreferredCallbackPeriod(
        Some(pid1),
        "morning",
        new LocalTime(8, 0, 0),
        new LocalTime(11, 0, 0),
        default = true
      )
      val afternoon: PreferredCallbackPeriod = PreferredCallbackPeriod(
        Some(pid2),
        "afternoon",
        new LocalTime(14, 0, 0),
        new LocalTime(17, 0, 0),
        default = false
      )

      val jsonResult: JsValue = Json.parse(
        s"""[{"uuid": "$pid1", "name": "morning", "periodStart": "08:00:00.000", "periodEnd": "11:00:00.000", "default": true },
             |{"uuid": "$pid2", "name": "afternoon", "periodStart": "14:00:00.000", "periodEnd": "17:00:00.000", "default": false }
             |]""".stripMargin
      )

      val periods: List[PreferredCallbackPeriod] = List(morning, afternoon)

      when(configWS.request(s"preferred_callback_periods", "GET", null))
        .thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(jsonResult)

      whenReady(requester.getPreferredCallbackPeriods()) { res =>
        res shouldEqual periods
      }

    }

    "retrieve all default membership" in new Helper {
      import UserQueueDefaultMembership._

      override val requester: ConfigServerRequester = new ConfigServerRequester(configWS)

      val expected: List[UserQueueDefaultMembership] = List(
        UserQueueDefaultMembership(
          10,
          List(QueueMembership(1, 7), QueueMembership(3, 4))
        ),
        UserQueueDefaultMembership(
          12,
          List(QueueMembership(2, 5), QueueMembership(3, 8))
        )
      )

      val jsonResponse: JsValue = Json.toJson(expected)

      when(configWS.request(s"user_membership/", "GET", null)).thenReturn(wsReq)

      when(wsResp.json).thenReturn(jsonResponse)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)

      val f: Future[List[UserQueueDefaultMembership]] = requester.getAllDefaultMembership
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"user_membership/", "GET")
      })
    }

    "set a User default membership" in new Helper {
      import QueueMembership._

      val userId     = 1234
      val membership: List[QueueMembership] = List(QueueMembership(1, 5), QueueMembership(3, 0))
      val json: JsValue = Json.toJson(membership)

      when(configWS.request(s"user_membership/$userId", "POST", null))
        .thenReturn(new DummyWsReq(wsResp))
      when(wsResp.status).thenReturn(OK)

      val f: Future[Unit] = requester.setUserDefaultMembership(userId, membership)
      whenReady(f)(res => {
        res shouldEqual ((): Unit)
        verify(configWS).request(s"user_membership/$userId", "POST")
      })
    }

    "set default membership for a list of users" in new Helper {
      import UsersQueueMembership._

      val userIds: List[Long] = List(1L, 2L, 3L, 4L)
      val membership: UsersQueueMembership = UsersQueueMembership(
        userIds,
        List(QueueMembership(1, 5), QueueMembership(3, 0))
      )
      val json: JsValue = Json.toJson(membership)

      when(configWS.request(s"user_membership/bulk", "POST", null))
        .thenReturn(new DummyWsReq(wsResp))
      when(wsResp.status).thenReturn(OK)

      val f: Future[Unit] = requester.setUsersDefaultMembership(membership)
      whenReady(f)(res => {
        res shouldEqual ((): Unit)
        verify(configWS).request(s"user_membership/bulk", "POST")
      })
    }

    "retrieve a CallQualification by queue" in new Helper {
      val queueId = 1L

      val sq: List[SubQualification] = List(SubQualification(Some(1), "subqualif1"))
      val expected: List[CallQualification] = List(
        CallQualification(Some(1), "qualif1", sq),
        CallQualification(Some(2), "qualif2", sq)
      )
      val json: JsValue = Json.toJson(expected)

      when(configWS.request(s"call_qualification/queue/$queueId", "GET", null))
        .thenReturn(new DummyWsReq(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[List[CallQualification]] =
        requester.getCallQualifications(queueId)
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"call_qualification/queue/$queueId", "GET")
      })
    }

    "request a csv with CallQualificationAnswer" in new Helper {
      val queueId             = 1L
      val fromRefTime: String = "2016-01-01 00:00:00"
      val toRefTime: String   = "2018-12-12 00:00:00"

      when(
        configWS.request(
          s"call_qualification_answer/$queueId/$fromRefTime/$toRefTime",
          "GET",
          null
        )
      ).thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.body).thenReturn("csv")

      val f: Future[String] =
        requester.exportQualificationsCsv(queueId, fromRefTime, toRefTime)
      whenReady(f)(res => {
        res shouldEqual "csv"
        verify(configWS).request(
          s"call_qualification_answer/$queueId/$fromRefTime/$toRefTime",
          "GET"
        )
      })
    }

    "create a CallQualificationAnswer" in new Helper {
      val qualificationAnswer: CallQualificationAnswer = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "some custom data"
      )
      val json: JsValue = Json.toJson(qualificationAnswer)
      val expected = 1L

      when(configWS.request(s"call_qualification_answer", "POST"))
        .thenReturn(new DummyWsReq(wsResp))

      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.toJson(expected))

      val f: Future[Long] = requester.createCallQualificationAnswer(qualificationAnswer)
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS, times(2)).request(s"call_qualification_answer", "POST")
      })
    }

    "retrieve a single QueueConfigUpdate by queue id" in new Helper {
      val queueId = 1L

      val expected: QueueConfigUpdate = QueueConfigUpdate(
        1,
        "queue",
        "queue",
        "1010",
        Some("context"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "announce",
        Some(1),
        Some("fs"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      val json: JsValue = Json.toJson(expected)

      when(configWS.request(s"queue_config/$queueId", "GET", null))
        .thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[QueueConfigUpdate] = requester.getQueueConfig(queueId)
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"queue_config/$queueId", "GET")
      })
    }

    "retrieve all QueueConfigUpdate" in new Helper {
      val qf1: QueueConfigUpdate = QueueConfigUpdate(
        1,
        "q1",
        "Queue One",
        "1000",
        Some("context"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "announce",
        Some(1),
        Some("fs"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      val qf2: QueueConfigUpdate = QueueConfigUpdate(
        2,
        "q2",
        "Queue Two",
        "2000",
        Some("context"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "announce",
        Some(1),
        Some("fs"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      val expected: List[QueueConfigUpdate] = List(qf1, qf2)

      val json: JsValue = Json.toJson(expected)

      when(configWS.request(s"queue_config", "GET", null)).thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[List[QueueConfigUpdate]] = requester.getQueueConfigAll
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"queue_config", "GET")
      })
    }

    "retrieve a single AgentConfig" in new Helper {
      val agentId = 1L

      val member: QueueMember = QueueMember(
        "queue1",
        1L,
        "Agent/1001",
        1,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        8
      )
      val expected: AgentConfigUpdate = AgentConfigUpdate(
        1L,
        "firstname",
        "lastname",
        "1001",
        "default",
        List(member),
        1L,
        Some(1L)
      )

      val json: JsValue = Json.toJson(expected)

      when(configWS.request(s"agent_config/$agentId", "GET", null))
        .thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[AgentConfigUpdate] = requester.getAgentConfig(agentId)
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"agent_config/$agentId", "GET")
      })
    }

    "retrieve all AgentConfig" in new Helper {

      val m1: QueueMember = QueueMember(
        "queue1",
        1L,
        "Agent/1001",
        1,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        1
      )
      val m2: QueueMember = QueueMember(
        "queue2",
        2L,
        "Agent/1002",
        2,
        0,
        "agent",
        2,
        "Agent",
        "queue",
        2
      )

      val a1: AgentConfigUpdate = AgentConfigUpdate(
        1L,
        "firstname",
        "lastname",
        "1001",
        "default",
        List(m1),
        1L,
        Some(1L)
      )
      val a2: AgentConfigUpdate = AgentConfigUpdate(
        2L,
        "firstname",
        "lastname",
        "1001",
        "default",
        List(m2),
        2L,
        Some(2L)
      )
      val expected: List[AgentConfigUpdate] = List(a1, a2)

      val json: JsValue = Json.toJson(expected)

      when(configWS.request(s"agent_config", "GET", null)).thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[List[AgentConfigUpdate]] = requester.getAgentConfigAll
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"agent_config", "GET")
      })
    }

    "retrieve all MediaServerConfig" in new Helper {
      private val mds1 = MediaServerConfig(
        1,
        "mds1",
        "MDS 1",
        "123.123.123.123",
        read_only = false
      )
      private val mds2 = MediaServerConfig(
        2,
        "mds2",
        "MDS 2",
        "123.123.123.123",
        read_only = false
      )
      private val mdsList = List(mds1, mds2)

      private val json = Json.toJson(mdsList)

      when(configWS.request(s"mediaserver", "GET", null)).thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[List[MediaServerConfig]] = requester.getMediaServerConfigAll
      whenReady(f)(res => {
        res shouldEqual mdsList
        verify(configWS).request(s"mediaserver", "GET")
      })

    }

    "retrieve a MediaServerConfig" in new Helper {
      private val mds: MediaServerConfig = MediaServerConfig(
        1,
        "mds1",
        "MDS 1",
        "123.123.123.123",
        read_only = false
      )
      private val json: JsValue = Json.toJson(mds)

      when(configWS.request(s"mediaserver/1", "GET", null)).thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[MediaServerConfig] = requester.getMediaServerConfig(1)
      whenReady(f)(res => {
        res shouldEqual mds
        verify(configWS).request(s"mediaserver/1", "GET")
      })

    }

    "forward a request with body to configmgt" in new Helper {
      private val json: JsValue = Json.parse("""
          |{
          | "fileName": "queue1_fileName"
          |}
        """.stripMargin)

      private val returnedJson: JsValue = Json.parse("""
          |{
          | "id": 3,
          | "fileName" : "queue1_fileName"
          |}
        """.stripMargin)

      when(
        configWS.request(
          s"queue/3/dissuasion/sound_file",
          "PUT",
          Some(json),
          "1.0",
          None
        )
      ).thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(returnedJson)

      val f: Future[WSResponse] =
        requester.forward(
          "queue/3/dissuasion/sound_file",
          "PUT",
          Some(json),
          "1.0",
          None
        )
      whenReady(f)(res => {
        verify(configWS)
          .request(
            s"queue/3/dissuasion/sound_file",
            "PUT",
            Some(json),
            "1.0",
            None
          )
      })
    }

    "get a user preference" in new Helper {
      val userId  = 1234
      val key     = "SOME_KEY"
      val value   = "some value"
      val pref: UserPreference = UserPreference(userId, key, value, "String")
      val payload: UserPreferencePayload = UserPreferencePayload(None, pref.value, pref.valueType)
      val json: JsValue = Json.toJson(payload)

      when(
        configWS.request(
          s"users/$userId/preferences/$key",
          "GET",
          None,
          "2.0",
          None
        )
      ).thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[UserPreference] =
        requester.getUserPreference(userId, pref.key)
      whenReady(f)(res => {
        res shouldEqual pref
        verify(configWS)
          .request(s"users/$userId/preferences/$key", "GET", None, "2.0", None)
      })

    }

    "get all user preferences" in new Helper {
      val userId = 1234
      val userPreferences: List[UserPreference] = List(
        UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"),
        UserPreference(userId, "Mobile_info", "true", "String")
      )
      val payload: List[UserPreferencePayload] = userPreferences.map(up =>
        UserPreferencePayload(Some(up.key), up.value, up.valueType)
      )
      val json: JsValue = Json.toJson(payload)

      when(
        configWS.request(s"users/$userId/preferences", "GET", None, "2.0", None)
      ).thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[List[UserPreference]] =
        requester.getUserPreferences(userId)
      whenReady(f)(res => {
        res shouldEqual userPreferences
        verify(configWS)
          .request(s"users/$userId/preferences", "GET", None, "2.0", None)
      })
    }

    "update a user preference" in new Helper {
      val userId  = 1234
      val key     = "SOME_KEY"
      val value   = "some value"
      val pref: UserPreference = UserPreference(userId, key, value, "String")
      val payload: UserPreferencePayload = UserPreferencePayload(None, pref.value, pref.valueType)
      val json: JsValue = Json.toJson(payload)

      when(
        configWS.request(
          s"users/$userId/preferences/$key",
          "PUT",
          Some(json),
          "2.0",
          None
        )
      ).thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)

      val f: Future[Unit] = requester.setUserPreference(pref)
      whenReady(f)(_ => {
        verify(configWS)
          .request(
            s"users/$userId/preferences/$key",
            "PUT",
            Some(json),
            "2.0",
            None
          )
      })

    }

    "create a user preference when update returns 404" in new Helper {
      val userId  = 1234
      val key     = "SOME_KEY"
      val value   = "some value"
      val pref: UserPreference = UserPreference(userId, key, value, "String")
      val payload: UserPreferencePayload = UserPreferencePayload(None, pref.value, pref.valueType)
      val json: JsValue = Json.toJson(payload)

      when(
        configWS.request(
          s"users/$userId/preferences/$key",
          "PUT",
          Some(json),
          "2.0",
          None
        )
      ).thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(NOT_FOUND)

      val wsReq2: WSRequest   = mock[WSRequest]
      val wsResp2: WSResponse = mock[WSResponse]
      when(
        configWS.request(
          s"users/$userId/preferences/$key",
          "POST",
          Some(json),
          "2.0",
          None
        )
      ).thenReturn(wsReq2)
      when(wsReq2.execute()).thenReturn(Future.successful(wsResp2))
      when(wsResp2.status).thenReturn(NO_CONTENT)

      val f: Future[Unit] = requester.setUserPreference(pref)
      whenReady(f)(_ => {
        verify(configWS)
          .request(
            s"users/$userId/preferences/$key",
            "PUT",
            Some(json),
            "2.0",
            None
          )
        verify(configWS)
          .request(
            s"users/$userId/preferences/$key",
            "POST",
            Some(json),
            "2.0",
            None
          )
      })

    }

    "get a user services" in new Helper {
      val userId = 1234
      val payload: UserServices = UserServices(
        false,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )

      val json: JsValue = Json.toJson(payload)

      when(
        configWS.request(s"users/$userId/services", "GET", None, "2.0", None)
      )
        .thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[UserServices] = requester.getUserServices(userId)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS)
          .request(s"users/$userId/services", "GET", None, "2.0", None)
      })

    }

    "update user services" in new Helper {
      val userId   = 1234
      val services: PartialUserServices = PartialUserServices(Some(true), None, None, None)
      val json: JsValue = Json.toJson(services)
      val response: UserServices = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )
      val jsonResp: JsValue = Json.toJson(response)

      when(
        configWS.request(
          s"users/$userId/services",
          "PUT",
          Some(json),
          "2.0",
          None
        )
      ).thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(jsonResp)

      val f: Future[PartialUserServices] = requester.setUserServices(userId, services)
      whenReady(f)(_ => {
        verify(configWS)
          .request(s"users/$userId/services", "PUT", Some(json), "2.0", None)
      })

    }

    "retrieve Ice configuration" in new Helper {
      val expected: IceServer = IceServer(Some("host:3478"))
      val json: JsValue = Json.toJson(expected)

      when(configWS.request(s"sip/ice_servers", "GET", null)).thenReturn(wsReq)

      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[IceServer] = requester.getIceServer
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"sip/ice_servers", "GET")
      })
    }

    "get token information for a static meeting room" in new Helper {
      val roomId = "42"
      val payload: MeetingRoomToken = MeetingRoomToken(
        "f1def7a5-546c-43d4-a8a1-6e2c9a9f7a1f",
        """eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
          |eyJuYW1lIjoiaGVsbG9yIiwidXVpZCI6ImYxZGVmN2E1LTU0NmMtNDNkNC1hOGExLTZlMmM5YTlmN2ExZiJ9.
          |L6SJEhXVfokLd2X1GBp_2-66QD7H0ajr6XfC88JshLY""".stripMargin
      )

      val json: JsValue = Json.toJson(payload)

      when(
        configWS.request(
          s"meetingrooms/token/$roomId",
          "GET",
          None,
          "2.0",
          None
        )
      )
        .thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[MeetingRoomToken] =
        requester.getMeetingRoomToken(roomId, Some(1L), StaticMeetingRoom)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS)
          .request(s"meetingrooms/token/$roomId", "GET", None, "2.0", None)
      })
    }

    "get token information for a personal meeting room" in new Helper {
      val roomId = "42"
      val userId = 1L
      val payload: MeetingRoomToken = MeetingRoomToken(
        "f1def7a5-546c-43d4-a8a1-6e2c9a9f7a1f",
        """eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
          |eyJuYW1lIjoiaGVsbG9yIiwidXVpZCI6ImYxZGVmN2E1LTU0NmMtNDNkNC1hOGExLTZlMmM5YTlmN2ExZiJ9.
          |L6SJEhXVfokLd2X1GBp_2-66QD7H0ajr6XfC88JshLY""".stripMargin
      )

      val json: JsValue = Json.toJson(payload)

      when(
        configWS.request(
          s"meetingrooms/token/$roomId?userId=$userId",
          "GET",
          None,
          "2.0",
          None
        )
      )
        .thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[MeetingRoomToken] =
        requester.getMeetingRoomToken(roomId, Some(1), PersonalMeetingRoom)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS)
          .request(
            s"meetingrooms/token/$roomId?userId=$userId",
            "GET",
            None,
            "2.0",
            None
          )
      })
    }

    "get token information for a temporary meeting room" in new Helper {
      val room = "User One"
      val payload: MeetingRoomToken = MeetingRoomToken(
        "f1def7a5-546c-43d4-a8a1-6e2c9a9f7a1f",
        """eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
          |eyJuYW1lIjoiaGVsbG9yIiwidXVpZCI6ImYxZGVmN2E1LTU0NmMtNDNkNC1hOGExLTZlMmM5YTlmN2ExZiJ9.
          |L6SJEhXVfokLd2X1GBp_2-66QD7H0ajr6XfC88JshLY""".stripMargin
      )

      val json: JsValue = Json.toJson(payload)

      when(
        configWS.request(
          s"meetingrooms/temporary/token/$room",
          "GET",
          None,
          "2.0",
          None
        )
      )
        .thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[MeetingRoomToken] =
        requester.getMeetingRoomToken(room, Some(1), TemporaryMeetingRoom)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS)
          .request(
            s"meetingrooms/temporary/token/$room",
            "GET",
            None,
            "2.0",
            None
          )
      })
    }

    "get alias information for a meeting room" in new Helper {
      val roomId = "42"
      val payload: MeetingRoomAlias = MeetingRoomAlias(
        Some("f1de-546c")
      )

      val json: JsValue = Json.toJson(payload)

      when(
        configWS.request(
          s"meetingrooms/alias/$roomId",
          "GET",
          None,
          "2.0",
          None
        )
      )
        .thenReturn(wsReq)
      when(wsReq.execute()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[MeetingRoomAlias] =
        requester.getMeetingRoomAlias(roomId)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS)
          .request(s"meetingrooms/alias/$roomId", "GET", None, "2.0", None)
      })
    }
  }

  "set mobile push notification token " in new Helper {
    val username  = "jbond"
    val pushToken: MobileAppPushToken = MobileAppPushToken(Some("myToken"))
    val json: JsValue = Json.toJson(pushToken)

    when(
      configWS.request(
        s"mobile/push/register",
        "POST",
        Some(json),
        "2.0",
        Some(username)
      )
    ).thenReturn(wsReq)
    when(wsReq.execute()).thenReturn(Future.successful(wsResp))
    when(wsResp.status).thenReturn(OK)

    val f: Future[Unit] = requester.setMobileAppPushToken(Some(username), pushToken)
    whenReady(f)(_ => {
      verify(configWS)
        .request(
          s"mobile/push/register",
          "POST",
          Some(json),
          "2.0",
          Some(username)
        )
    })

  }

  "delete mobile push notification token " in new Helper {
    val username = "jdoe"

    when(
      configWS.request(
        s"mobile/push/register",
        "DELETE",
        None,
        "2.0",
        Some(username)
      )
    )
      .thenReturn(wsReq)
    when(wsReq.execute()).thenReturn(Future.successful(wsResp))
    when(wsResp.status).thenReturn(NO_CONTENT)

    val f: Future[Unit] = requester.deleteMobileAppPushToken(Some(username))
    whenReady(f)(_ => {
      verify(configWS)
        .request(s"mobile/push/register", "DELETE", None, "2.0", Some(username))
    })

  }

  "checks that mobile application setup is valid for 204 HTTP response" in new Helper {

    when(
      configWS.request(
        s"mobile/push/check",
        "GET",
        None,
        "2.0",
        None
      )
    ).thenReturn(wsReq)

    when(wsReq.execute()).thenReturn(Future.successful(wsResp))
    when(wsResp.status).thenReturn(NO_CONTENT)

    whenReady(requester.checkValidMobileAppSetup) { res =>
      res shouldEqual MobileAppConfig(true)
      verify(configWS).request(
        s"mobile/push/check",
        "GET",
        None,
        "2.0",
        None
      )
    }
    requester.checkValidMobileAppSetup.futureValue shouldEqual MobileAppConfig(
      true
    )
  }

  "checks that mobile application setup is invalid for other HTTP response" in new Helper {

    when(
      configWS.request(
        s"mobile/push/check",
        "GET",
        None,
        "2.0",
        None
      )
    ).thenReturn(wsReq)

    when(wsReq.execute()).thenReturn(Future.successful(wsResp))
    when(wsResp.status).thenReturn(INTERNAL_SERVER_ERROR)

    whenReady(requester.checkValidMobileAppSetup) { res =>
      res shouldEqual MobileAppConfig(false)
      verify(configWS).request(
        s"mobile/push/check",
        "GET",
        None,
        "2.0",
        None
      )
    }
    requester.checkValidMobileAppSetup.futureValue shouldEqual MobileAppConfig(
      false
    )
  }
}
