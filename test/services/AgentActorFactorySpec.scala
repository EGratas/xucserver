package services

import org.apache.pekko.actor.*
import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito.{times, verify, when}
import org.mockito.ArgumentMatchers.*
import services.agent.AgentStatCollector
import xivo.models.Agent
import services.config.ConfigRepository
import xivo.phonedevices.QueueStatusCommand

class AgentActorFactorySpec
    extends TestKitSpec("AgentActorFactorySpec")
    with MockitoSugar {

  class Helper {
    val repo: ConfigRepository           = mock[ConfigRepository]
    val devicesTracker: TestProbe = TestProbe()
    val xucBus: XucEventBus         = mock[XucEventBus]
    val amiBus: TestProbe         = TestProbe()
    val agentStatFactory: AgentStatCollector.Factory = new AgentStatCollector.Factory {
      def apply(id: Agent.Id): Actor =
        new Actor {
          def receive: Receive = Actor.emptyBehavior
        }
    }
    val agaf = new AgentActorFactory(
      repo,
      devicesTracker.ref,
      xucBus,
      agentStatFactory,
      amiBus.ref
    )
    val context: ActorContext      = mock[ActorContext]
    val createdActor: ActorRef = TestProbe().ref

    when(context.actorOf(any(), anyString()))
      .thenReturn(createdActor)
  }

  "an agent actor factory" should {
    "return a new actor and ask for agent state while creating a new agent actor" in new Helper {
      val act: ActorRef = agaf.getOrCreate(32L, "1200", context)
      act should be(createdActor)
      amiBus.expectMsg(QueueStatusCommand(s"Agent/1200"))
    }

    "return a previously created actor on getOrCreate" in new Helper {
      agaf.getOrCreate(32L, "1200", context)
      agaf.getOrCreate(32L, "1200", context) should be(createdActor)
      verify(context, times(1)).actorOf(any[Props], anyString())

    }

    "return a previously created actor on get" in new Helper {
      agaf.getOrCreate(32L, "1200", context)
      agaf.get(32L) should be(Some(createdActor))
    }

    "return a new agent stat collector" in new Helper {
      agaf.getOrCreateAgentStatCollector(22L, context) should be(createdActor)
    }

    "return a previously created agent stat collector" in new Helper {
      val agentStatCollectorActor: ActorRef = TestProbe().ref
      agaf.agStatCollectors += (22L -> agentStatCollectorActor)

      agaf.getOrCreateAgentStatCollector(22L, context) should be(
        agentStatCollectorActor
      )
      verify(context, times(0)).actorOf(any[Props], anyString())

    }
  }

}
