package services.user

import org.apache.pekko.actor.{ActorRef, Props}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.json.JSONObject
import org.mockito.Mockito.{verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.xivo.cti.MessageFactory
import services.agent.AgentAction
import services.config.{ConfigRepository, ConfigServerRequester}
import services.line.PhoneController
import services.request.*
import services.{CtiFilter, CtiRouter, XucAmiBus, XucEventBus, XucStatsEventBus}
import xivo.directory.PersonalContactRepository
import xivo.models.{PartialUserServices, UserForward}
import xivo.websocket.WsBus
import xivo.xuc.XucBaseConfig
import xuctest.XucUserHelper

import scala.concurrent.ExecutionContext

class UserActionSpec
    extends TestKitSpec("CtiUserAction")
    with MockitoSugar
    with GuiceOneAppPerSuite
    with XucUserHelper {

  implicit lazy val executionContext: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  val testCtiLink: TestProbe        = TestProbe()
  val messageFactory: MessageFactory     = mock[MessageFactory]
  val eventBus: XucEventBus           = mock[XucEventBus]
  val statEventBus: XucStatsEventBus       = mock[XucStatsEventBus]
  val wsBus: WsBus              = mock[WsBus]
  val agentActionService: TestProbe = TestProbe()

  val phoneControllerFactory: PhoneController.Factory     = mock[PhoneController.Factory]
  val ctiFilterFactory: CtiFilter.Factory           = mock[CtiFilter.Factory]
  val personalContactRepoFactory: PersonalContactRepository.Factory = mock[PersonalContactRepository.Factory]
  val configRepository: ConfigRepository           = mock[ConfigRepository]
  val agentActionFactory: AgentAction.Factory         = mock[AgentAction.Factory]
  val callbackManager: TestProbe            = TestProbe()
  val configDispatcher: TestProbe           = TestProbe()
  val callHistory: TestProbe                = TestProbe()
  val xivoDirectory: TestProbe              = TestProbe()
  val amiBusConnector: TestProbe            = TestProbe()
  val queueDispatcher: TestProbe            = TestProbe()
  val agentConfig: TestProbe                = TestProbe()
  val voiceMailManager: TestProbe           = TestProbe()
  val flashTextService: TestProbe           = TestProbe()
  val xucConfig: XucBaseConfig                  = mock[XucBaseConfig]
  val clientLogger: TestProbe               = TestProbe()
  val videoEventManager: TestProbe          = TestProbe()
  val configServerRequester: ConfigServerRequester      = mock[ConfigServerRequester]
  val videoService: TestProbe               = TestProbe()
  val userPreferenceService: TestProbe      = TestProbe()

  class Helper {
    when(xucConfig.metricsRegistryName).thenReturn("TestRegistry")
    def actor(): (TestActorRef[CtiRouter], CtiRouter) = {
      val a = TestActorRef[CtiRouter](
        Props(
          new CtiRouter(
            wsBus,
            eventBus,
            statEventBus,
            messageFactory,
            phoneControllerFactory,
            ctiFilterFactory,
            personalContactRepoFactory,
            configRepository,
            agentActionFactory,
            configServerRequester,
            callbackManager.ref,
            configDispatcher.ref,
            callHistory.ref,
            xivoDirectory.ref,
            amiBusConnector.ref,
            queueDispatcher.ref,
            voiceMailManager.ref,
            flashTextService.ref,
            videoService.ref,
            userPreferenceService.ref,
            agentConfig.ref,
            clientLogger.ref,
            videoEventManager.ref,
            xucConfig,
            getXucUser("test", "pwd")
          ) {
            override def createCtiLink(username: String): ActorRef =
              testCtiLink.ref
          }
        )
      )
      a.underlyingActor.agentActionService = agentActionService.ref
      (a, a.underlyingActor)
    }
  }

  "a Cti user action" should {
    "send unconditionnal forward to ConfigServerRequester" in new Helper {
      val (ref, _) = actor()

      ref ! BaseRequest(TestProbe().ref, UncForward("3125", true))

      verify(configServerRequester).setUserServices(
        1,
        PartialUserServices(
          None,
          None,
          None,
          Some(UserForward(enabled = true, "3125"))
        )
      )
    }
    "Send na forward to ConfigServerRequester" in new Helper {
      val (ref, _) = actor()

      ref ! BaseRequest(TestProbe().ref, NaForward("2356", false))

      verify(configServerRequester).setUserServices(
        1,
        PartialUserServices(
          None,
          None,
          Some(UserForward(enabled = false, "2356")),
          None
        )
      )

    }

    "Send busy forward to ConfigServerRequester" in new Helper {
      val (ref, _) = actor()

      ref ! BaseRequest(TestProbe().ref, BusyForward("5699", true))

      verify(configServerRequester).setUserServices(
        1,
        PartialUserServices(
          None,
          Some(UserForward(enabled = true, "5699")),
          None,
          None
        )
      )
    }

    "send user status update request to ctilink" in new Helper {
      val userId           = 456
      val status           = "away"
      val (ref, ctiRouter) = actor()

      val json = new JSONObject()
      json.append("test", "userstatusupdate")

      when(messageFactory.createUserAvailState(userId.toString, status))
        .thenReturn(json)

      ref ! BaseRequest(
        TestProbe().ref,
        UserStatusUpdateReq(Some(userId), status)
      )

      verify(messageFactory).createUserAvailState(userId.toString, status)
      testCtiLink.expectMsg(json)

    }

    "send dnd request to ConfigServerRequester" in new Helper {
      val (ref, _) = actor()

      ref ! BaseRequest(TestProbe().ref, DndReq(true))

      verify(configServerRequester).setUserServices(
        1,
        PartialUserServices(Some(true), None, None, None)
      )
    }
  }

}
