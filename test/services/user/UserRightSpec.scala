package services.user

import org.joda.time.DateTime
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.MessageFactory
import play.api.libs.json.{JsString, JsValue, Json}
import play.api.libs.ws.WSResponse
import play.api.test.Helpers._
import services.XucStatsEventBus.AggregatedStatEvent
import services.agent.AgentStatistic
import services.config.ConfigDispatcher.{
  AgentGroupList,
  AgentList,
  AgentQueueMemberList,
  QueueList
}
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.events.AgentState.AgentReady
import xivo.models.XivoObject.{ObjectDefinition, ObjectType}
import xivo.models._
import xivo.xuc.{ConfigServerConfig, IceServerConfig, SipConfig}
import xuctest.BaseTest

import scala.concurrent.Future

class UserRightSpec extends BaseTest with MockitoSugar with ScalaFutures {

  class Helper {
    val (queueId1, queueId2, queueId3) = (4, 78, 6)
    val (groupId1, groupId2)           = (7, 14)
    val supervisorRight: SupervisorRight = SupervisorRight(
      List(queueId1, queueId3),
      List(groupId1),
      recordingAccess = true,
      dissuasionAccess = false
    )
    val wsResp: WSResponse               = mock[WSResponse]
    val requester: ConfigServerRequester = mock[ConfigServerRequester]

    val factory: MessageFactory    = mock[MessageFactory]
    val lineFactory: LineFactory   = mock[LineFactory]
    val agentFactory: AgentFactory = mock[AgentFactory]
    val agentQueueMemberFactory: AgentQueueMemberFactory =
      mock[AgentQueueMemberFactory]
    val configServer: ConfigServerRequester     = mock[ConfigServerRequester]
    val turnServerConfig: IceServerConfig = mock[IceServerConfig]
    val sipConfig: SipConfig        = mock[SipConfig]
    val configRepository = new ConfigRepository(
      factory,
      lineFactory,
      agentFactory,
      agentQueueMemberFactory,
      configServer,
      turnServerConfig,
      sipConfig
    )
    val configServerConfig: ConfigServerConfig = mock[ConfigServerConfig]
    when(configServerConfig.configHost).thenReturn("configHost")
    when(configServerConfig.configPort).thenReturn(9100)

    when(requester.getRights("username")).thenReturn(Future.successful(wsResp))
  }

  "The Right singleton" should {
    "parse admin right from JSON" in {
      val json = Json.obj(
        "type" -> "admin",
        "data" -> Json.obj()
      )

      UserRight.validate(json).asOpt shouldEqual Some(AdminRight())
    }

    "parse supervisor right from JSON" in {
      val json = Json.obj(
        "type" -> "supervisor",
        "data" -> Json.obj(
          "queueIds"         -> List(1, 2),
          "groupIds"         -> List(3, 4),
          "recordingAccess"  -> true,
          "dissuasionAccess" -> false
        )
      )

      UserRight.validate(json).asOpt shouldEqual Some(
        SupervisorRight(
          List(1, 2),
          List(3, 4),
          recordingAccess = true,
          dissuasionAccess = false
        )
      )
    }

    "parse supervisor right with dissuasion from JSON" in {
      val json = Json.obj(
        "type" -> "supervisor",
        "data" -> Json.obj(
          "queueIds"         -> List(1, 2),
          "groupIds"         -> List(3, 4),
          "recordingAccess"  -> false,
          "dissuasionAccess" -> true
        )
      )

      UserRight.validate(json).asOpt shouldEqual Some(
        SupervisorRight(
          List(1, 2),
          List(3, 4),
          recordingAccess = false,
          dissuasionAccess = true
        )
      )
    }

    "get admin right for user" in new Helper {
      val json: JsValue = Json.parse("{\"type\":\"admin\",\"data\":{}}")

      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[UserRight] =
        UserRight.getRights("username", requester, configServerConfig)
      whenReady(f)(res => {
        res shouldEqual AdminRight()
      })

    }

    "get supervisor right for user" in new Helper {

      val json: JsValue = Json.parse(
        "{\"type\":\"supervisor\",\"data\":{\"queueIds\":[3],\"groupIds\":[],\"incallIds\":[],\"recordingAccess\":true,\"dissuasionAccess\":false}}"
      )

      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[UserRight] =
        UserRight.getRights("username", requester, configServerConfig)
      whenReady(f)(res => {
        res shouldEqual SupervisorRight(
          List(3L),
          List(),
          recordingAccess = true,
          dissuasionAccess = false
        )
      })

    }

    "get supervisor right with dissuasion for user" in new Helper {

      val json: JsValue = Json.parse(
        "{\"type\":\"supervisor\",\"data\":{\"queueIds\":[3],\"groupIds\":[],\"incallIds\":[],\"recordingAccess\":false,\"dissuasionAccess\":true}}"
      )

      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[UserRight] =
        UserRight.getRights("username", requester, configServerConfig)
      whenReady(f)(res => {
        res shouldEqual SupervisorRight(
          List(3L),
          List(),
          recordingAccess = false,
          dissuasionAccess = true
        )
      })

    }

    "get no right if user type is unknown" in new Helper {

      val json: JsValue = Json.parse("{\"type\":\"unknown\",\"data\":{}}")

      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[UserRight] =
        UserRight.getRights("username", requester, configServerConfig)
      whenReady(f)(res => {
        res shouldEqual NoRightForUser()
      })

    }

    "get no right if user not found" in new Helper {

      when(wsResp.status).thenReturn(NOT_FOUND)

      val f: Future[UserRight] =
        UserRight.getRights("username", requester, configServerConfig)
      whenReady(f)(res => {
        res shouldEqual NoRightForUser()
      })

    }

    "get no right if unexpected response code received" in new Helper {
      when(wsResp.status).thenReturn(FORBIDDEN)

      val f: Future[UserRight] =
        UserRight.getRights("username", requester, configServerConfig)
      whenReady(f)(res => {
        res shouldEqual NoRightForUser()
      })
    }

    "get no right if user type is missing" in new Helper {
      val json: JsValue = Json.parse("{\"data\":{}}")

      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(json)

      val f: Future[UserRight] =
        UserRight.getRights("username", requester, configServerConfig)
      whenReady(f)(res => {
        res shouldEqual NoRightForUser()
      })
    }

    "serialize to JSON" in {
      Json
        .toJson(AdminRight())
        .shouldEqual(
          Json.obj(
            "profile" -> JsString("Admin"),
            "rights"  -> Json.arr("recording")
          )
        )
      Json
        .toJson(
          SupervisorRight(
            List(),
            List(),
            recordingAccess = false,
            dissuasionAccess = false
          )
        )
        .shouldEqual(
          Json.obj("profile" -> JsString("Supervisor"), "rights" -> Json.arr())
        )
      Json
        .toJson(NoRightForUser())
        .shouldEqual(
          Json.obj(
            "profile" -> JsString("NoRightForUser"),
            "rights"  -> Json.arr()
          )
        )
    }

    "serialize to JSON supervisor with dissuasion right" in {
      Json
        .toJson(
          SupervisorRight(
            List(),
            List(),
            recordingAccess = false,
            dissuasionAccess = true
          )
        )
        .shouldEqual(
          Json.obj(
            "profile" -> JsString("Supervisor"),
            "rights"  -> Json.arr("dissuasion")
          )
        )
    }

    "serialize to JSON supervisor with recording right" in {
      Json
        .toJson(
          SupervisorRight(
            List(),
            List(),
            recordingAccess = true,
            dissuasionAccess = false
          )
        )
        .shouldEqual(
          Json.obj(
            "profile" -> JsString("Supervisor"),
            "rights"  -> Json.arr("recording")
          )
        )
    }

    "serialize to JSON supervisor with all rights" in {
      Json
        .toJson(
          SupervisorRight(
            List(),
            List(),
            recordingAccess = true,
            dissuasionAccess = true
          )
        )
        .shouldEqual(
          Json.obj(
            "profile" -> JsString("Supervisor"),
            "rights"  -> Json.arr("recording", "dissuasion")
          )
        )
    }
  }

  "A SupervisorRight" should {

    "filter QueueList" in new Helper {
      val (q1, q2, q3) = (
        mock[QueueConfigUpdate],
        mock[QueueConfigUpdate],
        mock[QueueConfigUpdate]
      )
      when(q1.id).thenReturn(queueId1.toLong)
      when(q2.id).thenReturn(queueId2.toLong)
      when(q3.id).thenReturn(queueId3.toLong)
      val queueList: QueueList = QueueList(List(q1, q2, q3))

      supervisorRight.filter(queueList) shouldEqual Some(
        QueueList(List(q1, q3))
      )
    }

    "filter QueueMemberList based on the queue ids" in new Helper {
      val (qm1, qm2, qm3) = (
        AgentQueueMember(12L, queueId1, 0),
        AgentQueueMember(13L, queueId2, 0),
        AgentQueueMember(14L, queueId3, 0)
      )
      val qmList: AgentQueueMemberList = AgentQueueMemberList(List(qm1, qm2, qm3))
      val ag1: Agent = Agent(12L, "", "", "", "", groupId1)
      val ag3: Agent = Agent(14L, "", "", "", "", groupId1)
      configRepository.addAgent(ag1)
      configRepository.addAgent(ag3)

      supervisorRight.filter(qmList, configRepository) shouldEqual Some(
        AgentQueueMemberList(List(qm1, qm3))
      )
    }

    "filter QueueMemberList based on the group ids" in new Helper {
      val (qm1, qm2) =
        (AgentQueueMember(12L, queueId1, 0), AgentQueueMember(14L, queueId1, 0))
      val qmList: AgentQueueMemberList = AgentQueueMemberList(List(qm1, qm2))
      val ag1: Agent = Agent(12L, "", "", "", "", groupId1)
      val ag2: Agent = Agent(14L, "", "", "", "", groupId2)
      configRepository.addAgent(ag1)
      configRepository.addAgent(ag2)
      supervisorRight.filter(qmList, configRepository) shouldEqual Some(
        AgentQueueMemberList(List(qm1))
      )
    }

    "filter AggregatedStatEvent" in new Helper {
      val (stat1, stat2, stat3) = (
        AggregatedStatEvent(
          ObjectDefinition(ObjectType.Queue, Some(queueId1)),
          List()
        ),
        AggregatedStatEvent(
          ObjectDefinition(ObjectType.Queue, Some(queueId2)),
          List()
        ),
        AggregatedStatEvent(
          ObjectDefinition(ObjectType.Queue, Some(queueId3)),
          List()
        )
      )

      supervisorRight.filter(stat1) shouldEqual Some(stat1)
      supervisorRight.filter(stat2) shouldEqual None
      supervisorRight.filter(stat3) shouldEqual Some(stat3)
    }

    "filter AgentState" in new Helper {
      val (state1, state2) = (
        AgentReady(48L, new DateTime, "1234", List(4, 58), agentNb = "2000"),
        AgentReady(49L, new DateTime, "7895", List(12), agentNb = "2000")
      )
      val ag1: Agent = Agent(state1.id, "", "", "", "", groupId1)
      val ag2: Agent = Agent(state2.id, "", "", "", "", groupId2)
      configRepository.addAgent(ag1)
      configRepository.addAgent(ag2)

      supervisorRight.filter(state1, configRepository) shouldEqual Some(state1)
      supervisorRight.filter(state2, configRepository) shouldEqual None
    }

    "filter AgentStatistics" in new Helper {
      val stat1: AgentStatistic = AgentStatistic(14L, List())
      val stat2: AgentStatistic = AgentStatistic(15L, List())
      val ag1: Agent = Agent(stat1.agentId, "", "", "", "", groupId1)
      val ag2: Agent = Agent(stat2.agentId, "", "", "", "", groupId2)
      configRepository.addAgent(ag1)
      configRepository.addAgent(ag2)

      supervisorRight.filter(stat1, configRepository) shouldEqual Some(stat1)
      supervisorRight.filter(stat2, configRepository) shouldEqual None
    }

    "filter AgentGroupList" in new Helper {
      val list: AgentGroupList = AgentGroupList(
        List(
          AgentGroup(Some(groupId1), "john"),
          AgentGroup(Some(groupId2), "doe")
        )
      )
      supervisorRight.filter(list) shouldEqual Some(
        AgentGroupList(List(AgentGroup(Some(groupId1), "john")))
      )
    }

    "filter AgentList" in new Helper {
      val a1: Agent = Agent(25L, "John", "Doe", "1000", "default", groupId1)
      val a2: Agent = Agent(26L, "Lucky", "Luke", "1002", "default", groupId2)

      supervisorRight.filter(
        AgentList(List(a1, a2)),
        configRepository
      ) shouldEqual Some(AgentList(List(a1)))
    }

    "have recording rights if set" in new Helper {
      supervisorRight.rights shouldEqual List(RecordingAccess)
      val rightWithoutRec: SupervisorRight = SupervisorRight(
        List(queueId1, queueId3),
        List(groupId1),
        recordingAccess = false,
        dissuasionAccess = false
      )
      rightWithoutRec.rights shouldEqual List()
    }

    "have dissuasion rights if set" in new Helper {
      val rightWithDissuasion: SupervisorRight = SupervisorRight(
        List(queueId1, queueId3),
        List(groupId1),
        recordingAccess = false,
        dissuasionAccess = true
      )
      rightWithDissuasion.rights shouldEqual List(DissuasionAccess)
    }

    "have all rights if set" in new Helper {
      val rightWithDissuasion: SupervisorRight = SupervisorRight(
        List(queueId1, queueId3),
        List(groupId1),
        recordingAccess = true,
        dissuasionAccess = true
      )
      rightWithDissuasion.rights should contain.allOf(
        DissuasionAccess,
        RecordingAccess
      )
    }
  }

  "An AdminRight" should {
    "return the object unchanged" in new Helper {
      val o = "dummy"
      AdminRight().filter(o, configRepository) shouldEqual Some(o)
    }
  }

  "User right" should {
    "be admin right for user if there is no right server configured" in new Helper {

      when(configServerConfig.configHost).thenReturn("")
      when(configServerConfig.configPort).thenReturn(0)

      val f: Future[UserRight] =
        UserRight.getRights("username", requester, configServerConfig)
      whenReady(f)(res => {
        res shouldEqual AdminRight()
      })

    }
  }

}
