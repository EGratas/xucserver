package services.user

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.joda.time.{DateTime, Period}
import org.scalatestplus.mockito.MockitoSugar
import services.request.{
  BaseRequest,
  HistoryDays,
  HistorySize,
  UserCallHistoryRequest,
  UserCallHistoryRequestByDays
}
import xivo.models.{CallDetail, CallHistory, CallStatus}

class CallHistoryProxySpec
    extends TestKitSpec("CallHistoryProxySpec")
    with MockitoSugar {

  val callHistoryManager: TestProbe = TestProbe()

  class Helper {
    def actor: (TestActorRef[CallHistoryProxy], CallHistoryProxy) = {
      val a = TestActorRef[CallHistoryProxy](
        Props(new CallHistoryProxy(callHistoryManager.ref))
      )
      (a, a.underlyingActor)
    }
  }

  "A CallHistoryProxy" should {
    "forward UserCallHistoryRequest with size to the CallHistoryManager" in new Helper {
      val (ref, proxy) = actor
      ref ! UserCallHistoryRequest(HistorySize(7), "jdoe")
      callHistoryManager.expectMsg(
        BaseRequest(ref, UserCallHistoryRequest(HistorySize(7), "jdoe"))
      )
      proxy.requester.isDefined shouldBe true
    }

    "forward UserCallHistoryRequest with days to the CallHistoryManager" in new Helper {
      val (ref, proxy) = actor
      ref ! UserCallHistoryRequestByDays(HistoryDays(7), "jdoe")
      callHistoryManager.expectMsg(
        BaseRequest(ref, UserCallHistoryRequestByDays(HistoryDays(7), "jdoe"))
      )
      proxy.requester.isDefined shouldBe true
    }

    "send the CallHistory back to the requester" in new Helper {
      val (ref, proxy) = actor
      val requester: TestProbe = TestProbe()
      proxy.requester = Some(requester.ref)
      val history: CallHistory = CallHistory(
        List(
          CallDetail(
            new DateTime,
            Some(new Period),
            "4000",
            Some("4001"),
            CallStatus.Answered
          )
        )
      )

      ref ! history

      requester.expectMsg(history)
    }
  }

}
