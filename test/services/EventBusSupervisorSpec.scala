package services

import org.apache.pekko.actor.PoisonPill
import org.apache.pekko.event.{ActorEventBus, LookupClassification}
import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import org.mockito.Mockito.{spy, timeout, verify}
import org.scalatest.BeforeAndAfter
import org.scalatestplus.mockito.MockitoSugar
import org.apache.pekko.actor.ActorSystem

final case class TestMessage(topic: String, payload: String)

class EventBusSupervisorSpec
    extends TestKitSpec("EventBusSupervisorSpec")
    with MockitoSugar
    with BeforeAndAfter {

  class TestBus
      extends ActorEventBus
      with LookupClassification
      with EventBusSupervision {
    type Event      = TestMessage
    type Classifier = String

    implicit val system: ActorSystem = EventBusSupervisorSpec.this.system

    override protected def classify(event: Event): Classifier = event.topic

    override protected def publish(
        event: Event,
        subscriber: Subscriber
    ): Unit = {
      subscriber ! event.payload
    }

    override protected def mapSize(): Int = 5

  }

  var eventBus: TestBus = null
  var actor: TestProbe  = null

  before {
    eventBus = spy(new TestBus())
    actor = TestProbe()
  }

  "EventBusSupervisor" should {

    "Unsubscribe ActorRef when actor dies" in {
      eventBus.subscribe(actor.ref, "SomeTopic")

      actor.ref ! PoisonPill

      verify(eventBus, timeout(1000)).unsubscribe(actor.ref)
    }

  }

}
