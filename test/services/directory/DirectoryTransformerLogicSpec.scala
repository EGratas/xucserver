package services.directory

import models._
import org.mockito.Mockito.{verify, verifyNoInteractions, when}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.PhoneConfigUpdate
import org.xivo.cti.model.PhoneHintStatus
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.models.{
  AgentFactory,
  AgentQueueMemberFactory,
  LineFactory,
  MeetingRoomAlias
}
import xivo.xuc.{IceServerConfig, SipConfig, XucConfig}

import scala.collection.mutable
import scala.concurrent.Future
import scala.language.reflectiveCalls
import services.video.model.UserVideoEvent
import services.video.model.VideoEvents

/** Created by jirka on 03/09/15.
  */
class DirectoryTransformerLogicSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar {

  class Helper() {

    def fixture(phoneNumber: String, lineId: Int = 34): fixture =
      new fixture(phoneNumber, lineId)
    class fixture(phoneNumber: String, lineId: Int = 34) {
        val LineId: Int = lineId
        val userId = 52
        val status: Int = PhoneHintStatus.AVAILABLE.getHintStatus

        val phoneConfig: PhoneConfigUpdate = {
          val phoneConfig = new PhoneConfigUpdate
          phoneConfig.setId(LineId)
          phoneConfig.setNumber(phoneNumber)
          phoneConfig.setUserId(userId)
          phoneConfig
        }
        val xivoDirectoryResult: DirSearchResult = {
          DirSearchResult(
            List("Name", "Number", "Mobile", "Other number", "Favorite"),
            List("name", "name", "number", "number", "favorite"),
            List(
              ResultItem(
                DefaultDisplayViewItem(
                  "peter pan",
                  Some("44500"),
                  Some("87"),
                  Some("7"),
                  None,
                  false,
                  false
                ),
                Relations(
                  None,
                  None,
                  Some(userId),
                  Some("xivo-id1"),
                  None
                ),
                "xivo-directory"
              )
            )
          )
        }
      }

    val repo = new ConfigRepository(
      mock[MessageFactory],
      mock[LineFactory],
      mock[AgentFactory],
      mock[AgentQueueMemberFactory],
      mock[ConfigServerRequester],
      mock[IceServerConfig],
      mock[SipConfig]
    )
    val configMgtMock: ConfigServerRequester = mock[ConfigServerRequester]
    val xucConfig: XucConfig                 = mock[XucConfig]
    val logic                                = new DirectoryTransformerLogic(repo, configMgtMock, xucConfig)
    val testFixture: fixture = fixture("44500")
  }

  "DirectoryTransformerLogic" should {
    "update and transform xivo directory entries" in new Helper() {
      repo.onPhoneConfigUpdate(testFixture.phoneConfig)
      repo.updatePhoneStatus(
        testFixture.phoneConfig.getNumber,
        testFixture.status
      )

      val richDirectoryResult: RichDirectoryResult =
        logic.enrichDirResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries().head

      richEntry.status should be(PhoneHintStatus.AVAILABLE)
      richEntry.favorite should be(Some(false))
      richDirectoryResult.headers shouldBe RichDirectoryEntries.defaultHeaders
      richDirectoryResult.getEntries().head shouldEqual
        RichEntry(
          PhoneHintStatus.AVAILABLE,
          mutable.Buffer("peter pan", "44500", "87", "7", false, ""),
          None,
          None,
          Some("xivo-directory"),
          Some(false)
        )
    }

    "update xivo directory entries with UNEXISTING when line not found" in new Helper() {
      val richDirectoryResult: RichDirectoryResult =
        logic.enrichDirResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries().head

      richEntry.status should be(PhoneHintStatus.UNEXISTING)
      richDirectoryResult.headers shouldBe RichDirectoryEntries.defaultHeaders
    }

    "update xivo directory entries with UNEXISTING when status not found" in new Helper() {
      repo.onPhoneConfigUpdate(testFixture.phoneConfig)

      val richDirectoryResult: RichDirectoryResult =
        logic.enrichDirResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries().head

      richEntry.status should be(PhoneHintStatus.UNEXISTING)
      richDirectoryResult.headers shouldBe RichDirectoryEntries.defaultHeaders
    }

    "update and transform xivo favorites entries" in new Helper() {
      repo.onPhoneConfigUpdate(testFixture.phoneConfig)
      repo.updatePhoneStatus(
        testFixture.phoneConfig.getNumber,
        testFixture.status
      )

      val favorites: RichFavorites =
        logic.enrichFavorites(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = favorites.getEntries().head

      richEntry.status should be(PhoneHintStatus.AVAILABLE)
      richEntry.favorite should be(Some(false))
      favorites.headers shouldBe RichDirectoryEntries.defaultHeaders
    }

    "update results with username" in new Helper() {
      val xivoUser: XivoUser =
        XivoUser(52, None, None, "foo", None, Some("foobar"), None, None, None)
      repo.updateUser(xivoUser)

      val richDirectoryResult: RichDirectoryResult =
        logic.enrichDirResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries().head

      richEntry.username should be(Some("foobar"))

    }

    "update results with video status" in new Helper() {
      val xivoUser: XivoUser =
        XivoUser(52, None, None, "foo", None, Some("foobar"), None, None, None)
      repo.updateUser(xivoUser)
      repo.updateVideoStatus(e = UserVideoEvent("foobar", "videoStart"))

      val richDirectoryResult: RichDirectoryResult =
        logic.enrichDirResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries().head

      richEntry.videoStatus should be(Some(VideoEvents.Busy))
    }

    "username should be None if there is no XivoUser associated to the line" in new Helper {
      val richDirectoryResult: RichDirectoryResult =
        logic.enrichDirResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries().head

      richEntry.username should be(None)
    }

    "update results with personal meeting room link if source is meeting room" in new Helper() {
      val meetingRoomAlias: MeetingRoomAlias = MeetingRoomAlias(
        Some("abcd-1234")
      )
      when(configMgtMock.getMeetingRoomAlias("1"))
        .thenReturn(Future.successful(meetingRoomAlias))

      val xivoMeetingDirectoryResult: DirSearchResult = {
        DirSearchResult(
          List(
            "Name",
            "Number",
            "Mobile",
            "Other number",
            "Favorite",
            "Personal"
          ),
          List("name", "number", "number", "number", "favorite", "personal"),
          List(
            ResultItem(
              DefaultDisplayViewItem(
                "Meeting room",
                Some("4004"),
                None,
                None,
                None,
                favorite = true,
                true
              ),
              Relations(
                None,
                None,
                None,
                Some("xivo-id1"),
                Some("1")
              ),
              "xivo_meetingroom"
            )
          )
        )
      }

      val richDirectoryResult: RichDirectoryResult =
        logic.enrichDirResult(xivoMeetingDirectoryResult, 1)

      verify(configMgtMock).getMeetingRoomAlias(
        "1"
      )

      richDirectoryResult.getEntries().head.fields shouldEqual
        mutable.Buffer(
          "Meeting room",
          "4004",
          "",
          "",
          true,
          ""
        )

      richDirectoryResult.getEntries().head.url shouldBe Some(
        "/meet?id=abcd-1234"
      )
      richDirectoryResult.getEntries().head.personal shouldBe true
    }

    "update results with static meeting room link if source is meeting room" in new Helper() {
      val meetingRoomAlias: MeetingRoomAlias = MeetingRoomAlias(
        Some("abcd-1234")
      )
      when(configMgtMock.getMeetingRoomAlias("1"))
        .thenReturn(Future.successful(meetingRoomAlias))

      val xivoMeetingDirectoryResult: DirSearchResult = {
        DirSearchResult(
          List("Name", "Number", "Mobile", "Other number", "Favorite"),
          List("name", "number", "number", "number", "favorite"),
          List(
            ResultItem(
              DefaultDisplayViewItem(
                "Meeting room",
                Some("404"),
                None,
                None,
                None,
                favorite = true,
                false
              ),
              Relations(
                None,
                None,
                None,
                Some("xivo-id1"),
                Some("1")
              ),
              "xivo_meetingroom"
            )
          )
        )
      }

      val richDirectoryResult: RichDirectoryResult =
        logic.enrichDirResult(xivoMeetingDirectoryResult, 1)

      verify(configMgtMock).getMeetingRoomAlias("1")

      richDirectoryResult.getEntries().head.fields shouldEqual
        mutable.Buffer(
          "Meeting room",
          "404",
          "",
          "",
          true,
          ""
        )

      richDirectoryResult.getEntries().head.url shouldBe Some(
        "/meet?id=abcd-1234"
      )
      richDirectoryResult.getEntries().head.personal shouldBe false
    }

    "update results with email if source is different than meeting room" in new Helper() {
      val meetingRoomAlias: MeetingRoomAlias = MeetingRoomAlias(
        Some("abcd-1234")
      )
      when(configMgtMock.getMeetingRoomAlias("1"))
        .thenReturn(Future.successful(meetingRoomAlias))

      val xivoMeetingDirectoryResult: DirSearchResult = {
        DirSearchResult(
          List("Name", "Number", "Mobile", "Other number", "Favorite", "email"),
          List("name", "number", "number", "number", "favorite", "email"),
          List(
            ResultItem(
              DefaultDisplayViewItem(
                "Meeting room",
                Some("4004"),
                None,
                None,
                Some("email@email.com"),
                favorite = true,
                false
              ),
              Relations(
                None,
                None,
                None,
                Some("xivo-id1"),
                Some("1")
              ),
              "xivo_source"
            )
          )
        )
      }

      val richDirectoryResult: RichDirectoryResult =
        logic.enrichDirResult(xivoMeetingDirectoryResult, 1)

      richDirectoryResult.getEntries().head.fields shouldEqual
        mutable.Buffer(
          "Meeting room",
          "4004",
          "",
          "",
          true,
          "email@email.com"
        )

      verifyNoInteractions(configMgtMock)
    }
  }
}
