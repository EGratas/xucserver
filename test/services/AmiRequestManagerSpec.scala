package services

import org.apache.pekko.testkit.TestActorRef
import pekkotest.TestKitSpec
import org.asteriskjava.manager.action.{OriginateAction, SetVarAction}
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*
import org.scalatestplus.mockito.MockitoSugar
import services.XucAmiBus.*

class AmiRequestManagerSpec
    extends TestKitSpec("SetVarManagerSpec")
    with MockitoSugar {
  class Helper() {
    val amiBus: XucAmiBus = mock[XucAmiBus]

    def actor(): (TestActorRef[AmiRequestManager], AmiRequestManager) = {
      val sa = TestActorRef(new AmiRequestManager(amiBus))
      (sa, sa.underlyingActor)
    }
  }

  "AmiRequestManager" should {

    "subscribe to the ami bus for set var action requests and listen requests" in new Helper() {
      val (ref, _) = actor()
      verify(amiBus).subscribe(ref, AmiType.AmiRequest)
    }

    "send set variable request to ami bus" in new Helper {
      val (ref, _) = actor()

      ref ! SetVarRequest(SetVarActionRequest("VarName", "VarValue"))
      val arg: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case setvar: SetVarAction =>
          setvar.getVariable shouldBe "VarName"
          setvar.getValue shouldBe "VarValue"
        case any =>
          fail(s"Should get SetVarAction, got: $any")
      }
    }
    "send listen request to ami bus" in new Helper {
      val (ref, _) = actor()

      val (listener, listened) = ("SIP/lkkl", "SIP/khkhjd")

      val action = new ListenActionRequest(listener, listened)

      ref ! ListenRequest(action)

      val arg: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case originate: OriginateAction =>
          originate.getApplication shouldBe "ChanSpy"
          originate.getAsync shouldBe true
          originate.getChannel shouldBe listener
          originate.getData shouldBe s"$listened,bdqsS"
          originate.getCallerId shouldBe "Listen"
      }
    }

    "send beep request to ami bus" in new Helper {
      val (ref, _) = actor()

      val listened = "SIP/khkhjd"

      val action = new BeepActionRequest(listened)

      ref ! BeepRequest(action)

      val arg: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case originate: OriginateAction =>
          originate.getChannel shouldBe "Local/s@xivo-play-beep-to-agent"
          originate.getAsync shouldBe true
          originate.getData shouldBe s"beep"
          originate.getApplication shouldBe "Playback"
          originate.getVariables.get("XIVO_CHANNEL_TO_BEEP") shouldBe listened
      }
    }

    "publish ami action from request" in new Helper {
      val (ref, _)         = actor()
      val amiActionRequest: AmiActionRequest = mock[AmiActionRequest]
      val amiRealAction: XucManagerAction = mock[XucManagerAction]

      when(amiActionRequest.buildAction()).thenReturn(amiRealAction)

      ref ! AmiRequest(amiActionRequest)

      verify(amiBus).publish(AmiAction(amiRealAction))

    }

    "publish ami action from request with targetMds when defined" in new Helper {
      val (ref, _)         = actor()
      val amiActionRequest: AmiActionRequest = mock[AmiActionRequest]
      val amiRealAction: XucManagerAction = mock[XucManagerAction]

      when(amiActionRequest.buildAction()).thenReturn(amiRealAction)

      ref ! AmiRequest(amiActionRequest, Some("mds1"))

      verify(amiBus).publish(AmiAction(amiRealAction, targetMds = Some("mds1")))

    }
  }

}
