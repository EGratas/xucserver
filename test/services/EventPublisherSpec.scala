package services

import org.apache.pekko.testkit.TestActorRef
import pekkotest.TestKitSpec
import org.mockito.Mockito.{verify, when}
import org.scalatestplus.mockito.MockitoSugar
import services.config.ConfigRepository
import services.video.model.{VideoEvents, VideoStatusEvent}
import xivo.events.{PhoneEvent, PhoneEventType}
import xivo.websocket.WebSocketEvent

class EventPublisherSpec
    extends TestKitSpec("EventPublisher")
    with MockitoSugar {

  class Helper {
    val restPublisher: RestPublisher = mock[RestPublisher]
    val eventBus: XucEventBus      = mock[XucEventBus]
    val userRepo: ConfigRepository      = mock[ConfigRepository]
    def actor(): (TestActorRef[EventPublisher], EventPublisher) = {
      val sa = TestActorRef(
        new EventPublisher(restPublisher, eventBus, userRepo)
      )
      (sa, sa.underlyingActor)
    }
  }

  "An event publisher" should {
    "subscribe to event bus phone events" in new Helper {
      var (ref, a) = actor()
      verify(eventBus).subscribe(ref, XucEventBus.allPhoneEventsTopic)
    }
    "publish phone event for a user" in new Helper {
      var (ref, a) = actor()
      when(userRepo.userNameFromPhoneNb("1001")).thenReturn(Some("jlang"))

      val phoneEvent: PhoneEvent = PhoneEvent(
        PhoneEventType.EventRinging,
        "1001",
        "0675432130",
        "Billy The Kid",
        "1003456.7",
        "1003456.1"
      )

      ref ! phoneEvent

      verify(restPublisher).publish(
        "jlang",
        WebSocketEvent.createEvent(phoneEvent)
      )
    }
    "subscribe to event bus video events" in new Helper {
      var (ref, a) = actor()
      verify(eventBus).subscribe(ref, XucEventBus.allVideoStatusTopic)
    }
    "publish video event for a user" in new Helper {
      var (ref, a) = actor()

      val videoStatusEvent: VideoStatusEvent = VideoStatusEvent("ahonnet", VideoEvents.Busy)

      ref ! videoStatusEvent

      verify(restPublisher).publish(
        "ahonnet",
        WebSocketEvent.createEvent(videoStatusEvent)
      )
    }
  }
}
