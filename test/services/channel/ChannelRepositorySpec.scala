package services.channel

import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import xivo.xucami.models.{CallerId, Channel, ChannelState, MonitorState}
import xuctest.{BaseTest, ChannelGen}

import scala.collection.immutable.HashMap

class ChannelRepositorySpec extends BaseTest with MockitoSugar with ChannelGen {

  "A channel repo" should {

    "Add a channel" in {
      val chan = bchan("SIP/ossfd")
      ChannelRepository().addNewChannel(chan).get(chan.id) shouldBe Some(chan)

    }

    "Add Local channels with '***'" in {
      val chanSip   = bchan("SIP/asdfas")
      val chanLocal = bChanLocal("***341030")
      ChannelRepository()
        .addNewChannel(chanSip)
        .addNewChannel(chanLocal)
        .get(chanLocal.id) shouldBe Some(chanLocal)
    }

    "Update user data from last peer channel when adding a new channel" in {
      val interface = "SIP/kssd"
      val peerChan =
        bchan(interface).copy(variables = Map("USR_DATA1" -> "val1"))
      val secChan = bchan(interface)

      ChannelRepository()
        .addNewChannel(peerChan)
        .addNewChannel(secChan)
        .get(secChan.id) shouldBe Some(
        secChan.copy(variables = Map("USR_DATA1" -> "val1"))
      )

    }

    "Do not Update user data from last peer channel when device is a trunk" in {
      val interface = "SIP/trunk-from-somewhere"
      val peerChan =
        bchan(interface).copy(variables = Map("USR_DATA1" -> "val1"))
      val secChan = bchan(interface)

      ChannelRepository()
        .addNewChannel(peerChan)
        .addNewChannel(secChan)
        .get(secChan.id)
        .map(_.variables) shouldBe Some(Map.empty)

    }

    "Update a channel" in {
      val initialChannel = Channel(
        "13216545.12",
        "SIP/wred",
        CallerId("tony", "2016"),
        linkedChannelId = "13216545.12",
        agentNumber = Some("2500")
      )

      ChannelRepository(HashMap(initialChannel.id -> initialChannel))
        .updateChannel(initialChannel.withChannelState(ChannelState.BUSY))
        .channels(initialChannel.id) should be(
        initialChannel.withChannelState(ChannelState.BUSY)
      )
    }
    "On transfer update a channel with transferer user data" in {
      val txfererInterface = "SIP/iufdsqyu"
      val otherInt         = "SIP/other"
      val transfererChan =
        bchan(txfererInterface).updateVariable("USR_DATA1", "BIG DATA")
      val destXferChan = bchan(otherInt).updateVariable(
        Channel.VarNames.TransfererName,
        transfererChan.name.replace(";1", "")
      )

      val repo = ChannelRepository()
        .addNewChannel(transfererChan)
        .updateChannel(destXferChan)

      repo.get(destXferChan.id) shouldBe Some(
        destXferChan.updateVariable("USR_DATA1", "BIG DATA")
      )
    }
    "Update a channel and apply function" in {
      val doWithChannel = mock[Channel => Channel]
      val initialChannel = Channel(
        "13216545.12",
        "SIP/wred",
        CallerId("tony", "2016"),
        linkedChannelId = "13216545.12",
        agentNumber = Some("2500")
      )

      ChannelRepository(HashMap(initialChannel.id -> initialChannel))
        .updateChannel(
          initialChannel.withChannelState(ChannelState.BUSY),
          doWithChannel
        )

      verify(doWithChannel)(initialChannel.withChannelState(ChannelState.BUSY))

    }
    "Update linkedid of a channel" in {

      val initialChannel = Channel(
        "13216545.12",
        "SIP/wred",
        CallerId("tony", "2016"),
        linkedChannelId = "13216545.12",
        agentNumber = Some("2500")
      )
      val destChannel = Channel(
        "14665454.54",
        "SIP/1k4yj2-00000038",
        CallerId("", ""),
        linkedChannelId = "14665454.54"
      )

      val chans = HashMap(
        initialChannel.id -> initialChannel,
        destChannel.id    -> destChannel
      )

      ChannelRepository(chans)
        .updateLinkedId(initialChannel.id, destChannel.id)
        .channels(destChannel.id)
        .linkedChannelId should be(initialChannel.linkedChannelId)

    }
    "update linkedid of a channel and its linked channels" in {
      val initialChannel = Channel(
        "13216545.12",
        "SIP/wred",
        CallerId("tony", "2016"),
        linkedChannelId = "13216545.04",
        agentNumber = Some("2500")
      )
      val destChannel = Channel(
        "14665454.54",
        "SIP/1k4yj2-00000038",
        CallerId("", ""),
        linkedChannelId = "14665454.54"
      )
      val destChannellinked = Channel(
        "14665454.32",
        "SIP/1k4yj2-00000044",
        CallerId("", ""),
        linkedChannelId = "14665454.54"
      )
      val chans = HashMap(
        initialChannel.id    -> initialChannel,
        destChannel.id       -> destChannel,
        destChannellinked.id -> destChannellinked
      )

      ChannelRepository(chans)
        .updateLinkedId(initialChannel.id, destChannel.id)
        .channels(destChannellinked.id)
        .linkedChannelId should be(initialChannel.linkedChannelId)

    }

    "propagate variables" in {
      val channel1 = bchan("Local/id-58").updateVariable("varch1", "valuech1")
      val channel2 = bchan("Local/id-58").updateVariable("varch2", "valuech2")

      val chans = HashMap(channel1.id -> channel1, channel2.id -> channel2)

      val updatedChan = ChannelRepository(chans)
        .propagateVars(channel1.id, channel2.id)
        .channels(channel2.id)

      updatedChan.variables("varch1") should be("valuech1")
      updatedChan.variables("varch2") should be("valuech2")

    }
    "update user data on propagate variable for transferred channels" in {
      val mainInterface = "SIP/kjsfd"
      val mainChan      = bchan(mainInterface).updateVariable("USR_1", "DATA1")
      val channel1 = bchan("Local/id-58")
        .updateVariable("varch1", "valuech1")
        .updateVariable(
          Channel.VarNames.TransfererName,
          mainChan.name.replace(":", "")
        )
      val channel2 = bchan("Local/id-58").updateVariable("varch2", "valuech2")

      val chans = HashMap(
        mainChan.id -> mainChan,
        channel1.id -> channel1,
        channel2.id -> channel2
      )

      val updatedChan = ChannelRepository(chans)
        .propagateVars(channel1.id, channel2.id)
        .channels(channel2.id)

      updatedChan.variables("varch1") should be("valuech1")
      updatedChan.variables("varch2") should be("valuech2")
      updatedChan.variables("USR_1") should be("DATA1")

    }

    "propagate remote party state information" in {
      val channel1 = bchan("SIP/abcd").updateVariable(
        Channel.VarNames.xucCallType,
        Channel.callTypeValOriginate
      )
      val channel2 = bchan("Local/id-58", linkedId = channel1.id)

      val chans = HashMap(channel1.id -> channel1, channel2.id -> channel2)

      val updatedChan = ChannelRepository(chans)
        .propagateVars(channel1.id, channel2.id)
        .channels(channel1.id)

      updatedChan.isOriginatePartyUp should be(true)

    }

    class AgentChannels {
      val agentNb = "3450"
      val cid: CallerId = CallerId("eddie", "2014")
      val channel = new Channel(
        "444332.22",
        "SIP/uwert",
        cid,
        "444332.10",
        monitored = MonitorState.ACTIVE,
        agentNumber = Some(agentNb)
      )

      val repo: ChannelRepository = ChannelRepository(HashMap(channel.id -> channel))

    }
    "find a channel by agent number" in new AgentChannels {

      repo.byAgentNumber(Some(agentNb)) should be(Some(channel))

    }

    "find a channel by callerId number and connectedLine number" in {
      val chan1 = bChanNumbers("1001", "1002")
      val chan2 = bChanNumbers("1001", "1100")
      val chan3 = bChanNumbers("1001", "1003")
      val repo = ChannelRepository()
        .addNewChannel(chan1)
        .addNewChannel(chan2)
        .addNewChannel(chan3)
      repo.getByNumbers("1001", "1100").shouldEqual(Some(chan2))
    }

    """when searching for a channel by callerIdNo and connectedLineNo return first local channel when two or more are
       present""" in {
      val chan1 = bChanNumbers("1001", "1002")
      val chan2 = bChanNumbers("1001", "1100", "SIP-bnsdfd5")
      val chan3 = bChanNumbers("1001", "1100", "Local/1100")
      val chan4 = bChanNumbers("1001", "1003")
      val repo = ChannelRepository()
        .addNewChannel(chan1)
        .addNewChannel(chan2)
        .addNewChannel(chan3)
        .addNewChannel(chan4)
      repo.getByNumbers("1001", "1100").shouldEqual(Some(chan3))
    }
  }
  /*
  Attended xfer scenario

   deviceC1 main channel on device established with deviceC2

   Attended transfer using CTI

   - deviceL1 : Local channel connected to device (placeholder for ami attendted xfer)
   - deviceL2 : Local channel connected to the outside channel
   - L1 & L2 are bridged
   - device C3 : Outside channel,

   (On complete L1 & L2 are removed and C3 bridged to C2

   */
  "On attented transfer fix #935" should {
    class AXferChannels {
      val phoneInterface    = "SIP/ivhbur05"
      val localInterface    = "Local/0664489834"
      val sipTrunkInterface = "SIP/0033972521691"
      val deviceC1: Channel =
        bchan(phoneInterface, callerId = new CallerId("Bruce", "1000"))
      var deviceL1: Channel =
        bchan(localInterface, callerId = new CallerId(null, "0033972521691"))
          .addVariables(Map(Channel.VarNames.TransfererName -> deviceC1.name))
      var deviceL2: Channel = bchan(
        localInterface,
        callerId = new CallerId("223344556677", "223344556677")
      ).addVariables(
        Map(
          Channel.VarNames.TransfererName -> deviceC1.name,
          "XIVO_CONTEXT"                  -> "default"
        )
      )
      var deviceC3: Channel = bchan(
        sipTrunkInterface,
        callerId = new CallerId("223344556677", "223344556677")
      ).addVariables(
        Map(
          Channel.VarNames.TransfererName -> deviceC1.name,
          "XIVO_CONTEXT"                  -> "default"
        )
      )
    }
    "update local 2nd channel caller id from original channel" in new AXferChannels {

      val repo: ChannelRepository = ChannelRepository()
        .addNewChannel(deviceC1)
        .updateChannel(deviceL2)

      repo.get(deviceL2.id) shouldBe Some(
        deviceL2.withCallerId(deviceC1.callerId)
      )

    }
    "Do not update L1 channel" in new AXferChannels {
      val repo: ChannelRepository = ChannelRepository()
        .addNewChannel(deviceC1)
        .updateChannel(deviceL1)

      repo.get(deviceL1.id) shouldBe Some(deviceL1)

    }
    "Do not update outgoing channel on attended Xfer fix #935" in new AXferChannels {

      val repo: ChannelRepository = ChannelRepository()
        .addNewChannel(deviceC1)
        .updateChannel(deviceC3)

      repo.get(deviceC3.id) shouldBe Some(deviceC3)

    }
  }
  "Hangup Finder" should {
    val interface     = "SIP/uwert"
    val lineExtension = "1000"

    trait HangupChannels {
      val chinterface: Channel = bchan(interface, ChannelState.UP)

      val repo: ChannelRepository = new ChannelRepository(HashMap())
        .addNewChannel(chinterface)
        .addNewChannel(bchan("SIP/lsdkjf", ChannelState.DIALING))

    }

    "find a channel to hangup" in new HangupChannels {

      repo.toHangup(interface, lineExtension) should be(List(chinterface))

    }

    class upChannels {

      val upChans: ChannelRepository = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, ChannelState.UP))
        .addNewChannel(bchan(interface, ChannelState.UP))

      val repo: ChannelRepository = new ChannelRepository(upChans.channels)
        .addNewChannel(bchan(interface, ChannelState.HOLD))

    }
    "All up channels have to be hanged up" in new upChannels {

      repo.toHangup(
        interface,
        lineExtension
      ) should equal (upChans.channels.values.toList)

    }

    "Only one channel found, selected for hangup" in {

      val repo = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, ChannelState.RINGING))

      repo.toHangup(
        interface,
        lineExtension
      ) should contain only repo.channels.values.toList.head
    }

    "Hold channel are never hanged up even if alone" in {

      val repo = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, ChannelState.HOLD))

      repo.toHangup(interface, lineExtension) shouldBe empty

    }

    "Channel found except on holds" in {

      val toHangup = bchan(interface, ChannelState.RINGING)
      val repo = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, ChannelState.HOLD))
        .addNewChannel(toHangup)

      repo.toHangup(interface, lineExtension) should contain only toHangup

    }

  }

  "Xfer transfer finder " should {
    val interface = "SIP/uwert"
    class ToXFerChannel {

      val toXfer: Channel = bchan(interface, ChannelState.UP)
      val repo: ChannelRepository = new ChannelRepository(HashMap())
        .addNewChannel(toXfer)

    }
    "retreive a Up channel" in new ToXFerChannel {
      repo.toXfer(interface) should be(Some(toXfer))
    }

    class XferChannels {

      val upChan: Channel = bchan(interface, ChannelState.UP)
      val repo: ChannelRepository = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, ChannelState.HOLD))
        .addNewChannel(upChan)

    }

    "Retreive channel if more than one channel" in new XferChannels {
      repo.toXfer(interface) should be(Some(upChan))
    }

    "Retreive only up channel" in {
      val repo = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, ChannelState.HOLD))

      repo.toXfer(interface) should be(None)
    }

    "Retreive channel for local interface with same dialed peer number(remote agent)" in {
      val localInterface = "Local/00664489834@default"
      val lchan1 = bchan(
        localInterface,
        ChannelState.UP,
        callerId = CallerId("agent", "id-1")
      ).copy(variables = Map("DIALEDPEERNUMBER" -> "00664489834@default"))
      val lchan2 = bchan(
        localInterface,
        ChannelState.UP,
        callerId = CallerId("other", "06785433")
      ).copy(variables =
        Map("DIALEDPEERNUMBER" -> "xivo-bazile-vip/0619232652")
      )
      val repo = new ChannelRepository(HashMap())
        .addNewChannel(lchan1)
        .addNewChannel(lchan2)

      repo.toXfer(localInterface) should be(Some(lchan1))
    }
    "Retreive channel for local interface with same dialed peer number(remote agent) even with no optimize (/n)" in {
      val localInterface = "Local/00664489834@default"
      val lchan1 = bchan(
        localInterface,
        ChannelState.UP,
        callerId = CallerId("agent", "id-1")
      ).copy(variables = Map("DIALEDPEERNUMBER" -> "00664489834@default/n"))
      val lchan2 = bchan(
        localInterface,
        ChannelState.UP,
        callerId = CallerId("other", "06785433")
      ).copy(variables =
        Map("DIALEDPEERNUMBER" -> "xivo-bazile-vip/0619232652")
      )
      val repo = new ChannelRepository(HashMap())
        .addNewChannel(lchan1)
        .addNewChannel(lchan2)

      repo.toXfer(localInterface) should be(Some(lchan1))
    }

  }

  "Complete Xfer finder" should {
    val interface = "SIP/sodif"
    class CompleteXferHoldchannel {

      val toCompleteXfer: Channel = bchan(interface, ChannelState.HOLD)
      val repo: ChannelRepository = new ChannelRepository(HashMap())
        .addNewChannel(toCompleteXfer)

    }
    "retreive a Hold channel" in new CompleteXferHoldchannel {
      repo.toCompleteXfer(interface) should be(Some(toCompleteXfer))
    }

    class CompleteXferHoldchannels {

      val repo: ChannelRepository = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, ChannelState.HOLD))
        .addNewChannel(bchan(interface, ChannelState.HOLD))

    }

    "Do not retreveive any channel if more than one channel on Hold" in new CompleteXferHoldchannels {

      repo.toCompleteXfer(interface) should be(None)

    }

  }

  "Cancel Xfer finder" should {

    val interface = "SIP/sodif"

    class CancelXferChannels {
      val toCancelXfer: Channel = bchan(interface, ChannelState.DIALING)
      val repo: ChannelRepository = new ChannelRepository(HashMap())
        .addNewChannel(toCancelXfer)
        .addNewChannel(bchan(interface, ChannelState.HOLD))

    }
    "Retreive channel not on Hold" in new CancelXferChannels {
      repo.toCancelXfer(interface, cid.number) should be(Some(toCancelXfer))

    }

    "find nothing if more than one channel to cancel" in {
      val repo = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, ChannelState.DIALING))
        .addNewChannel(bchan(interface, ChannelState.UP))
        .addNewChannel(bchan(interface, ChannelState.HOLD))

      repo.toCancelXfer(interface, cid.number) should be(None)

    }

    "find nothing when no other channel than on hold" in {
      val repo = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, ChannelState.HOLD))

      repo.toCancelXfer(interface, cid.number) should be(None)

    }

  }

  "Set Data Finder" should {

    val interface = "SIP/sodif"
    val otherCid  = CallerId("John", "3232")

    class SetDataChannels {
      val myChan: Channel = bchan(interface)
      val repo: ChannelRepository = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, callerId = otherCid))
        .addNewChannel(myChan)

    }

    "find my channel" in new SetDataChannels {
      repo.toSetData(cid.number) shouldBe List(myChan)
    }
    class SetDataLinkedChannels {
      val lid    = "56455466.56"
      val myChan: Channel = bchan(interface, linkedId = lid)
      val lnkChan: Channel =
        Channel(lid, "SIPX/KSJ-0000", CallerId("external", "0123465"), lid)
      val repo: ChannelRepository = new ChannelRepository(HashMap())
        .addNewChannel(bchan(interface, callerId = otherCid))
        .addNewChannel(myChan)
        .addNewChannel(lnkChan)

    }

    "find my channel and my linked channel" in new SetDataLinkedChannels {
      repo.toSetData(cid.number) shouldBe List(myChan, lnkChan)

    }
  }

}
