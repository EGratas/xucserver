package services.channel

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.TestKit
import org.asteriskjava.live.HangupCause
import org.asteriskjava.manager.action.GetVarAction
import org.asteriskjava.manager.event.*
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import pekkotest.TestKitSpec
import services.{AmiEventHelper, XucAmiBus}
import services.XucAmiBus.*
import xivo.xucami.models.{CallerId, Channel, ChannelDirection, ChannelState, MonitorState}
import xivo.xucami.userevents.HangupEvent as HangupUserEvent
import xuctest.{BaseTest, ChannelGen}

import java.util
import scala.collection.immutable.HashMap

class EventProcessorSpec
    extends TestKitSpec("EventProcessor")
    with ChannelGen
    with MockitoSugar
    with AmiEventHelper
    with AnyWordSpecLike
    with Matchers {

  class Helper() {
    val amiBus: XucAmiBus       = mock[XucAmiBus]
    val mChannelRepo: ChannelRepository = mock[ChannelRepository]

    val eventProcessor = new AmiEventProcessor(amiBus)
  }

  "Ami event processor on new state" should {
    "when channel isn't agent callback, update channel state, connectedLineN* and variables (but not callerId) from linkedid and publish" in new Helper {
      val channelId = "5646546546.12"
      val linkedId  = "5646546546.10"

      val newState = new NewStateEvent("")
      newState.setUniqueId(channelId)
      newState.setCallerIdName("Bob")
      newState.setCallerIdNum("1099")
      newState.setChannelState(ChannelState.DIALING.id)
      newState.setConnectedLineNum("44200")
      newState.setConnectedLineName("Serge Gainsbourg")

      val channel: Channel = Channel(
        channelId,
        "SIP/1zupfz-00000054",
        CallerId("hawkeye", "1002"),
        linkedChannelId = linkedId,
        variables = HashMap("foo" -> "bar")
      )
      val linkedChannel: Channel = Channel(
        linkedId,
        "Local/id-28@agentcallback-0000003e;2",
        CallerId("", ""),
        linkedChannelId = linkedId,
        variables = HashMap("Var" -> "Value")
      )
      val channelRepo: ChannelRepository = ChannelRepository(
        HashMap(channelId -> channel, linkedId -> linkedChannel)
      )

      doNothing().when(amiBus).publish(any[ChannelEvent])

      val updatedChannelRepo: ChannelRepository =
        eventProcessor.process(AmiEvent(newState), channelRepo)

      verify(amiBus).publish(ChannelEvent(updatedChannelRepo(channelId)))
      val expectedChannel: Channel = channel.copy(
        state = ChannelState.DIALING,
        connectedLineNb = Some("44200"),
        connectedLineName = Some("Serge Gainsbourg"),
        variables = HashMap("Var" -> "Value", "foo" -> "bar"),
        direction = Some(ChannelDirection.OUTGOING)
      )
      updatedChannelRepo.get(channelId) shouldEqual Some(expectedChannel)
    }

    "when channel is agent callback, update channel callerId, state, connectedLineN* and variables from linkedid and do not publish" in new Helper {
      val channelId = "5646546546.12"
      val linkedId  = "5646546546.10"

      val newState = new NewStateEvent("")
      newState.setUniqueId(channelId)
      newState.setCallerIdName("Bob")
      newState.setCallerIdNum("1099")
      newState.setChannelState(ChannelState.DIALING.id)
      newState.setConnectedLineNum("44200")
      newState.setConnectedLineName("Serge Gainsbourg")

      val channel: Channel = Channel(
        channelId,
        "Local/id-28@agentcallback-0000003e;2",
        CallerId("hawkeye", "1002"),
        linkedChannelId = linkedId,
        variables = HashMap("foo" -> "bar")
      )
      val linkedChannel: Channel = Channel(
        linkedId,
        "SIP/1zupfz-00000054",
        CallerId("", ""),
        linkedChannelId = linkedId,
        variables = HashMap("Var" -> "Value")
      )
      val channelRepo: ChannelRepository = ChannelRepository(
        HashMap(channelId -> channel, linkedId -> linkedChannel)
      )

      val updatedChannelRepo: ChannelRepository =
        eventProcessor.process(AmiEvent(newState), channelRepo)

      verifyNoInteractions(amiBus)
      val expectedChannel: Channel = channel.copy(
        callerId = CallerId("Bob", "1099"),
        state = ChannelState.DIALING,
        connectedLineNb = Some("44200"),
        connectedLineName = Some("Serge Gainsbourg"),
        variables = HashMap("Var" -> "Value", "foo" -> "bar"),
        direction = Some(ChannelDirection.OUTGOING)
      )
      updatedChannelRepo.get(channelId) shouldEqual Some(expectedChannel)
    }

    "republish linked channel on state UP" in new Helper {
      val channelId = "5646546546.12"
      val linkedId  = "5646546546.10"

      val newState = new NewStateEvent("")
      newState.setUniqueId(channelId)
      newState.setCallerIdName("hawkeye")
      newState.setCallerIdNum("1002")
      newState.setChannelState(ChannelState.UP.id)
      newState.setConnectedLineNum("44200")

      val linkedChannel: Channel = Channel(
        linkedId,
        "SIP/1zupfz-00000054",
        CallerId("", ""),
        linkedChannelId = linkedId,
        variables = HashMap("Var" -> "Value")
      )
      val channel: Channel = Channel(
        channelId,
        "SIP/g2lhmi-00000053",
        CallerId("hawkeye", "1002"),
        linkedChannelId = linkedId
      )

      val chanRepo: ChannelRepository = eventProcessor.process(
        AmiEvent(newState),
        ChannelRepository(
          HashMap(channelId -> channel, linkedChannel.id -> linkedChannel)
        )
      )

      verify(amiBus).publish(ChannelEvent(chanRepo.get(channelId).get))

    }
    "Do not publish if linked channel not exists (start in traffic)" in new Helper {
      val channelId = "5646546546.12"
      val linkedId  = "5646546546.10"

      val newState = new NewStateEvent("")
      newState.setUniqueId(channelId)
      newState.setCallerIdName("hawkeye")
      newState.setCallerIdNum("1002")
      newState.setChannelState(6)
      newState.setConnectedLineNum("44200")

      val channel: Channel = Channel(
        channelId,
        "SIP/g2lhmi-00000053",
        CallerId("hawkeye", "1002"),
        linkedChannelId = linkedId
      )

      val chanRepo: ChannelRepository = eventProcessor.process(
        AmiEvent(newState),
        ChannelRepository(HashMap(channelId -> channel))
      )

      verify(amiBus).publish(ChannelEvent(chanRepo.get(channelId).get))
      verifyNoMoreInteractions(amiBus)
    }
  }

  "Ami event processor on set var event" should {
    "update channel variable on var set event" in new Helper {
      val channelId = "98798799.1"

      val channel: Channel = Channel(
        channelId,
        "Local/id-58@agentcallback-0000023e;1",
        CallerId("bob", "1204"),
        channelId
      )

      val varSetEvent = new VarSetEvent("")
      varSetEvent.setUniqueId(channelId)
      varSetEvent.setVariable("VARNAME")
      varSetEvent.setValue("VALUE")

      val chanRepo: ChannelRepository = eventProcessor.process(
        AmiEvent(varSetEvent),
        ChannelRepository(HashMap(channelId -> channel))
      )

      chanRepo.get(channelId).get.variables("VARNAME") should be("VALUE")

    }
    "do not update variable on null value" in new Helper {
      val channelId = "98798799.1"

      val channel: Channel = Channel(
        channelId,
        "Local/id-58@agentcallback-0000023e;1",
        CallerId("bob", "1204"),
        channelId
      )

      val varSetEvent = new VarSetEvent("")
      varSetEvent.setUniqueId(channelId)
      varSetEvent.setVariable("VARNAMENULL")
      varSetEvent.setValue(null)

      val chanRepo: ChannelRepository = eventProcessor.process(
        AmiEvent(varSetEvent),
        ChannelRepository(HashMap(channelId -> channel))
      )

      chanRepo.get(channelId).get.variables.get("VARNAMENULL") should be(None)
    }
    "send DialAnswered event when the variable XIVO_PICKEDUP is set to 1" in new Helper {
      val channelId = "98798799.1"
      val channel: Channel = Channel(
        channelId,
        "Local/id-58@agentcallback-0000023e;1",
        CallerId("bob", "1204"),
        channelId
      )

      val varSetEvent = new VarSetEvent("")
      varSetEvent.setUniqueId(channelId)
      varSetEvent.setVariable("XIVO_PICKEDUP")
      varSetEvent.setValue("1")

      val chanRepo: ChannelRepository = eventProcessor.process(
        AmiEvent(varSetEvent),
        ChannelRepository(HashMap(channelId -> channel))
      )

      verify(amiBus).publish(
        DialAnswered(
          chanRepo.get(channelId).get.updateVariable("XIVO_PICKEDUP", "1")
        )
      )
    }

  }

  trait SpyFixture {
    val spyeeChannel: Channel = Channel(
      "413313189.1",
      "SIP/çhdfg-00000002f",
      CallerId("Jack", "3000"),
      "413313189.1"
    )
    val spyerChannel: Channel = Channel(
      "123564464.1",
      "SIP/qgsdh-000000145",
      CallerId("Alice", "2000"),
      "123564464.1"
    )

  }

  "Ami event processor spy events" should {
    "publish a start spy event on spy channel started " in new Helper
      with SpyFixture {

      val spyStart = new ChanSpyStartEvent("")
      spyStart.setSpyeeChannel(spyeeChannel.name)
      spyStart.setSpyerChannel(spyerChannel.name)

      val result: ChannelRepository = eventProcessor.process(
        AmiEvent(spyStart),
        ChannelRepository(
          HashMap(
            spyeeChannel.id -> spyeeChannel,
            spyerChannel.id -> spyerChannel
          )
        )
      )

      verify(amiBus).publish(
        SpyStarted(SpyChannels(spyerChannel, spyeeChannel))
      )

      result.channels shouldBe
        HashMap(
          spyerChannel.id -> spyerChannel.withLinkedId(spyeeChannel.id),
          spyeeChannel.id -> spyeeChannel
        )
    }

    "publish a stop spy event on spy channel stopped" in new Helper
      with SpyFixture {

      val spyStopped = new ChanSpyStopEvent("")
      spyStopped.setSpyerUniqueId(spyeeChannel.id)

      eventProcessor.process(
        AmiEvent(spyStopped),
        ChannelRepository(
          HashMap(
            spyeeChannel.id -> spyeeChannel,
            spyerChannel.id -> spyerChannel
          )
        )
      )

      verify(amiBus).publish(SpyStopped(spyeeChannel))
    }
  }

  "Ami event processor monitor events" should {
    """upon reception of MonitorStartEvent:
      - publish GetVarAction(MONITOR_PAUSED) on the bus
      - set monitored to ACTIVE
    """ in new Helper {
      val channel: Channel = Channel(
        "413313189.1",
        "SIP/çhdfg-00000002f",
        CallerId("Jack", "3000"),
        "413313189.1"
      )
      val event = new MonitorStartEvent("")
      event.setUniqueId(channel.id)

      val res: ChannelRepository = eventProcessor.process(
        AmiEvent(event),
        ChannelRepository(HashMap(channel.id -> channel))
      )

      res.get(channel.id) shouldEqual Some(
        channel.copy(monitored = MonitorState.ACTIVE)
      )

      val captor1: ArgumentCaptor[ChannelEvent] = ArgumentCaptor.forClass(classOf[ChannelEvent])
      verify(amiBus).publish(captor1.capture())

      val captor2: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(amiBus).publish(captor2.capture())

      captor1.getAllValues.get(0) shouldEqual ChannelEvent(
        channel.copy(monitored = MonitorState.ACTIVE)
      )
      captor2.getAllValues.get(0).message.isInstanceOf[GetVarAction] shouldBe true
      val getVar: GetVarAction = captor2.getAllValues.get(0).message.asInstanceOf[GetVarAction]
      getVar.getVariable shouldEqual "MONITOR_PAUSED"
      getVar.getChannel shouldEqual channel.name
    }

    """upon reception of MonitorStopEvent:
      - publish GetVarAction(MONITOR_PAUSED) on the bus
      - set monitored to DISABLED
    """ in new Helper {
      val channel: Channel = Channel(
        "413313189.1",
        "SIP/çhdfg-00000002f",
        CallerId("Jack", "3000"),
        "413313189.1"
      )
      val event = new MonitorStopEvent("")
      event.setUniqueId(channel.id)

      val res: ChannelRepository = eventProcessor.process(
        AmiEvent(event),
        ChannelRepository(HashMap(channel.id -> channel))
      )

      res.get(channel.id) shouldEqual Some(
        channel.copy(monitored = MonitorState.DISABLED)
      )
      val captor1: ArgumentCaptor[ChannelEvent] = ArgumentCaptor.forClass(classOf[ChannelEvent])
      verify(amiBus).publish(captor1.capture())

      val captor2: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(amiBus).publish(captor2.capture())

      captor1.getAllValues.get(0) shouldEqual ChannelEvent(
        channel.copy(monitored = MonitorState.DISABLED)
      )
      captor2.getAllValues.get(0).message.isInstanceOf[GetVarAction] shouldBe true
      val getVar: GetVarAction = captor2.getAllValues.get(0).message.asInstanceOf[GetVarAction]
      getVar.getVariable shouldEqual "MONITOR_PAUSED"
      getVar.getChannel shouldEqual channel.name
    }
  }
  trait AgentConnectFixture {
    val agentNumber = "2500"
    val queueName   = "sales"
    val agentChannel = new Channel(
      "12334800.52",
      "SIP/uwert",
      CallerId("eddie", "2014"),
      "12334800.52",
      state = ChannelState.BUSY,
      monitored = MonitorState.ACTIVE
    )

    val agentConnectEvent = new AgentConnectEvent("")
    agentConnectEvent.setUniqueId(agentChannel.id)
    agentConnectEvent.setMemberName(s"Agent/$agentNumber")
    agentConnectEvent.setQueue(queueName)

  }
  "Ami event processor on new callerid event" should {
    "update caller id" in new Helper() {
      val uniqueId    = "1432212391.17"
      val newCallerId = new NewCallerIdEvent("")
      newCallerId.setCallerIdName("John")
      newCallerId.setCallerIdNum("1200")
      newCallerId.setUniqueId(uniqueId)
      val channelIn: Channel = Channel(
        uniqueId,
        "SIP/barometrix_jyldev-00000007",
        CallerId("", ""),
        uniqueId
      )
      val chans: HashMap[AmiObjectReference, Channel] = HashMap(uniqueId -> channelIn)

      val updatedChans: ChannelRepository =
        eventProcessor.process(AmiEvent(newCallerId), ChannelRepository(chans))

      updatedChans.get(uniqueId).get.callerId should be(
        CallerId("John", "1200")
      )
    }

    "do not update caller id if already fully set" in new Helper() {
      val uniqueId    = "1432212391.17"
      val newCallerId = new NewCallerIdEvent("")
      newCallerId.setCallerIdName("John")
      newCallerId.setCallerIdNum("1200")
      newCallerId.setUniqueId(uniqueId)
      val channelIn: Channel = Channel(
        uniqueId,
        "SIP/barometrix_jyldev-00000007",
        CallerId("Jim", "5555"),
        uniqueId
      )
      val chans: HashMap[AmiObjectReference, Channel] = HashMap(uniqueId -> channelIn)

      val updatedChans: ChannelRepository =
        eventProcessor.process(AmiEvent(newCallerId), ChannelRepository(chans))

      updatedChans.get(uniqueId).get.callerId should be(CallerId("Jim", "5555"))
    }

  }
  "Ami event processor on Hangup event" should {
    "publish a channel event and delete the channel" in new Helper() {
      val channel = new Channel(
        "13454445.32",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "13454445.28",
        state = ChannelState.OFFHOOK
      )
      val hangupEvent = new HangupEvent("")
      hangupEvent.setUniqueId(channel.id)

      val updatedChans: ChannelRepository = eventProcessor.process(
        AmiEvent(hangupEvent),
        ChannelRepository(HashMap(channel.id -> channel))
      )

      verify(amiBus).publish(
        ChannelEvent(channel.copy(state = ChannelState.HUNGUP))
      )

      updatedChans.get(channel.id) should be(None)

    }

    "when channel linked to a local channel is hung up, hangup the local channel" in new Helper() {
      val channel: Channel = new Channel(
        "13454445.321",
        "SIP/uwert-0000001",
        CallerId("eddie", "2014"),
        "13454445.28",
        state = ChannelState.OFFHOOK
      ).updateVariable("BRIDGEPEER", "Local/2014@default-000002;1")
      val lc1 = new Channel(
        "13454445.322",
        "Local/2014@default-000002;1",
        CallerId("Someone", "2015"),
        "13454445.323",
        state = ChannelState.UP
      )
      val lc2 = new Channel(
        "13454445.323",
        "Local/2014@default-000002;2",
        CallerId("eddie", "2014"),
        "13454445.322",
        state = ChannelState.UP
      )
      val hangupEvent = new HangupEvent("")
      hangupEvent.setUniqueId(channel.id)

      val channels: ChannelRepository = ChannelRepository(
        HashMap(
          channel.id -> channel,
          lc1.id     -> lc1,
          lc2.id     -> lc2
        )
      )

      val updatedChans: ChannelRepository = eventProcessor.process(AmiEvent(hangupEvent), channels)

      verify(amiBus).publish(
        ChannelEvent(channel.copy(state = ChannelState.HUNGUP))
      )
      verify(amiBus).publish(
        ChannelEvent(lc2.copy(state = ChannelState.HUNGUP))
      )

      updatedChans.get(channel.id) should be(None)
      updatedChans.get(lc2.id) should be(None)

    }
    "not publish zombi channels" in new Helper {
      val channel = new Channel(
        "1433244077.120",
        "Local/id-22@agentcallback-0000001c;1<ZOMBIE>",
        CallerId("eddie", "2014"),
        "1433244077.120",
        state = ChannelState.OFFHOOK
      )
      val hangupEvent = new HangupEvent("")
      hangupEvent.setUniqueId(channel.id)

      val updatedChans: ChannelRepository = eventProcessor.process(
        AmiEvent(hangupEvent),
        ChannelRepository(HashMap(channel.id -> channel))
      )

      verifyNoInteractions(amiBus)

      updatedChans.get(channel.id) should be(None)

    }
    "publish on local channel with SIP att transfer mark in variables and cause UserBusy" in new Helper() {
      val channel: Channel = bChanLocal(
        "2014",
        callerId = CallerId("eddie", "2014"),
        st = ChannelState.UP
      ).addVariables(Map("ATTENDEDTRANSFER" -> "SIP/xxxxxx"))
      val hangupEvent = new HangupEvent("")
      hangupEvent.setUniqueId(channel.id)
      hangupEvent.setCause(HangupCause.AST_CAUSE_USER_BUSY.getCode())

      val updatedChans: ChannelRepository = eventProcessor.process(
        AmiEvent(hangupEvent),
        ChannelRepository(HashMap(channel.id -> channel))
      )

      verify(amiBus).publish(
        ChannelEvent(channel.copy(state = ChannelState.HUNGUP))
      )

      updatedChans.get(channel.id) should be(None)
    }
    "NOT publish on local channel with SIP att transfer mark in variables and cause NormalCallClearing" in new Helper() {
      val channel: Channel = bChanLocal(
        "2014",
        callerId = CallerId("eddie", "2014"),
        st = ChannelState.UP
      ).addVariables(Map("ATTENDEDTRANSFER" -> "SIP/xxxxxx"))
      val hangupEvent = new HangupEvent("")
      hangupEvent.setUniqueId(channel.id)
      hangupEvent.setCause(HangupCause.AST_CAUSE_NORMAL_CLEARING.getCode)

      val updatedChans: ChannelRepository = eventProcessor.process(
        AmiEvent(hangupEvent),
        ChannelRepository(HashMap(channel.id -> channel))
      )

      verifyNoInteractions(amiBus)

      updatedChans.get(channel.id) should be(None)
    }
    "publish on local channel with SIP att transfer mark in variables which is not related to the channel caller id" in new Helper() {
      val channel: Channel = bChanLocal(
        "1168",
        callerId = CallerId("eddie", "2014"),
        st = ChannelState.UP
      ).addVariables(Map("ATTENDEDTRANSFER" -> "SIP/xxxxxx"))
      val hangupEvent = new HangupEvent("")
      hangupEvent.setUniqueId(channel.id)

      val updatedChans: ChannelRepository = eventProcessor.process(
        AmiEvent(hangupEvent),
        ChannelRepository(HashMap(channel.id -> channel))
      )

      verify(amiBus).publish(
        ChannelEvent(channel.copy(state = ChannelState.HUNGUP))
      )

      updatedChans.get(channel.id) should be(None)
    }
  }
  """Ami event processor on Hangup user event 
     Sometime native hangup event is not fired, see https://issues.asterisk.org/jira/browse/ASTERISK-19296""" should {
    "if Hangup UserEvent then publish a channel event and delete the channel" in new Helper() {
      val channel = new Channel(
        "13454445.32",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "13454445.28",
        state = ChannelState.OFFHOOK
      )
      val evt = new HangupUserEvent("")
      evt.setUniqueId(channel.id)

      val updatedChans: ChannelRepository = eventProcessor.process(
        AmiEvent(evt),
        ChannelRepository(HashMap(channel.id -> channel))
      )

      verify(amiBus).publish(
        ChannelEvent(channel.copy(state = ChannelState.HUNGUP))
      )

      updatedChans.get(channel.id) should be(None)

    }
  }
  "Ami event processor on dial event" should {
    "publish and update channel on dialevent" in new Helper {
      val initialChannel: Channel = Channel(
        "13216545.12",
        "SIP/wred",
        CallerId("tony", "2016"),
        linkedChannelId = "13216545.12",
        agentNumber = Some("2500")
      )
      val destChannel: Channel = Channel(
        "14665454.54",
        "SIP/1k4yj2-00000038",
        CallerId("", ""),
        linkedChannelId = "14665454.54"
      )

      val dialEvt = new DialEvent("")
      dialEvt.setUniqueId(initialChannel.id)
      dialEvt.setDestUniqueId(destChannel.id)

      when(mChannelRepo.updateLinkedId(initialChannel.id, destChannel.id))
        .thenReturn(ChannelRepository(HashMap()))
      val updatedChans: ChannelRepository = eventProcessor.process(AmiEvent(dialEvt), mChannelRepo)
      verify(mChannelRepo).updateLinkedId(initialChannel.id, destChannel.id)
    }
  }

  "Ami event processor on local bridge event" should {
    "propagate channel1 variables to channel2 on localbridge event and set linkedid" in new Helper {
      val channelId1 = "98798799.10"
      val channelId2 = "98798799.11"
      val channel1: Channel = Channel(
        channelId1,
        "Local/id-58@agentcallback-0000023e;1",
        CallerId("bob", "1204"),
        linkedChannelId = channelId1,
        variables = Map("varch1" -> "valuech1")
      )
      val channel2: Channel = Channel(
        channelId2,
        "Local/id-58@agentcallback-0000023e;2",
        CallerId("bob", "1204"),
        channelId2,
        variables = Map("varch2" -> "valuech2")
      )

      when(mChannelRepo.updateLinkedId(channel1.id, channel2.id))
        .thenReturn(ChannelRepository(HashMap()))

      var localBridgeEvent = new LocalBridgeEvent("")
      localBridgeEvent.setLocalOneUniqueId(channelId1)
      localBridgeEvent.setLocalTwoUniqueid(channelId2)

      val chans: ChannelRepository =
        eventProcessor.process(AmiEvent(localBridgeEvent), mChannelRepo)

      verify(mChannelRepo).updateLinkedId(channel1.id, channel2.id)
    }
  }

  "Ami event processor hold events" should {

    // Hold event contains the right channel
    // when only one cold to put on hold, or when receiving a call through a queue
    "set channel state to HOLD when receiving a a hold event" in new Helper {
      val channel: Channel = bchan("SIP/1k4yj2", st = ChannelState.UP)
      val hold    = new HoldEvent("")
      hold.setUniqueId(channel.id)

      when(mChannelRepo.get(channel.id)).thenReturn(Some(channel))

      eventProcessor.process(AmiEvent(hold), mChannelRepo)

      verify(mChannelRepo).updateChannel(
        channel.withChannelState(ChannelState.HOLD),
        eventProcessor.publish
      )

    }
    /*
    when transfering a call using ami
    Hold event is every time on the main channel of the device, instead of the local channel
    in this case we receive a hold event on a channel already on hold (as xfer is in progress)

    Device1000 --- C1 ---- Bridge --- C2 --- Device1001
                   |
                  LC1---Local-Bridge -- LC3---C3----Device1002

    Hold event is always received on channel C1.1 event if the second call is on hold.
     */
    "set channel state to HOLD when receiving HoldEvent on channel already on HOLD and publish Channel" in new Helper {
      val channel: Channel = new Channel(
        "13454445.321",
        "SIP/uwert-0000001",
        CallerId("eddie", "2014"),
        "13454445.28",
        state = ChannelState.HOLD
      ).updateVariable("BRIDGEPEER", "Local/2014@default-000002;1")
      val lc1 = new Channel(
        "13454445.322",
        "Local/2014@default-000002;1",
        CallerId("Someone", "2015"),
        "13454445.323",
        state = ChannelState.UP
      )
      val lc2 = new Channel(
        "13454445.323",
        "Local/2014@default-000002;2",
        CallerId("eddie", "2014"),
        "13454445.322",
        state = ChannelState.UP
      )

      val repo: ChannelRepository = ChannelRepository(
        HashMap(
          channel.id -> channel,
          lc1.id     -> lc1,
          lc2.id     -> lc2
        )
      )

      val hold = new HoldEvent("")
      hold.setUniqueId(channel.id)

      eventProcessor.process(AmiEvent(hold), repo)

      verify(amiBus).publish(ChannelEvent(lc2.copy(state = ChannelState.HOLD)))
    }

    // Simple case, one call, one channel on Hold to put up

    "set channel state to UP when receiving UnholdEvent and publish Channel" in new Helper {
      val channel: Channel = bchan("SIP/1k4yj2", st = ChannelState.HOLD)
      val unhold  = new UnholdEvent("")
      unhold.setUniqueId(channel.id)

      when(mChannelRepo.get(channel.id)).thenReturn(Some(channel))

      eventProcessor.process(AmiEvent(unhold), mChannelRepo)

      verify(mChannelRepo).updateChannel(
        channel.withChannelState(ChannelState.UP),
        eventProcessor.publish
      )
      /*
      val unhold = new UnholdEvent("")
      val channel = Channel("123456.789", "SIP/1k4yj2-00000038", CallerId("", "2001"), linkedChannelId = "",
        state = ChannelState.HOLD, connectedLineNb = Some("2011"))
      unhold.setCallerIdNum("2001")
      unhold.setConnectedLineNum("2011")
      when(mChannelRepo.getByNumbers("2001", "2011")).thenReturn(Some(channel))

      eventProcessor.process(AmiEvent(unhold), mChannelRepo)

      verify(mChannelRepo).updateChannel(channel.withChannelState(ChannelState.UP), eventProcessor.publish)
       */
    }
    /*
      Same scenario, Device1000 is transferring a call using asterisk XFer
      Push the hold key, to hold and un hold, in this case the hold/unhold event always refer to the main channel
      So in xuc memory, unhold the local channel instead
     */
    "When having a second local channel on hold, put on hold this local channel " in new Helper {
      val channel: Channel = new Channel(
        "13454445.321",
        "SIP/uwert-0000001",
        CallerId("eddie", "2014"),
        "13454445.28",
        state = ChannelState.HOLD
      ).updateVariable("BRIDGEPEER", "Local/2014@default-000002;1")
      val lc1 = new Channel(
        "13454445.322",
        "Local/2014@default-000002;1",
        CallerId("Someone", "2015"),
        "13454445.323",
        state = ChannelState.UP
      )
      val lc2 = new Channel(
        "13454445.323",
        "Local/2014@default-000002;2",
        CallerId("eddie", "2014"),
        "13454445.322",
        state = ChannelState.HOLD
      )

      val repo: ChannelRepository = ChannelRepository(
        HashMap(
          channel.id -> channel,
          lc1.id     -> lc1,
          lc2.id     -> lc2
        )
      )

      val evt = new UnholdEvent("")
      evt.setUniqueId(channel.id)

      eventProcessor.process(AmiEvent(evt), repo)

      verify(amiBus).publish(ChannelEvent(lc2.copy(state = ChannelState.UP)))

    }
    "When having a second channel up, up the first channel" in new Helper {
      val device1000 = "1000"
      val device1002 = "1002"
      val chanC1: Channel = bchan("SIP/1k4yj2", st = ChannelState.HOLD)
      val chanLocalC1: Channel = bChanLocal(
        "1000",
        callerId = CallerId("Jack", device1000),
        connectedLine = device1002,
        st = ChannelState.UP
      )
      val unHold = new UnholdEvent("")
      unHold.setCallerIdNum(device1000)
      unHold.setConnectedLineNum(device1002)
      unHold.setUniqueId(chanC1.id)

      when(mChannelRepo.get(chanC1.id)).thenReturn(Some(chanC1))
      when(mChannelRepo.getByNumbers(device1000, device1002))
        .thenReturn(Some(chanLocalC1))

      eventProcessor.process(AmiEvent(unHold), mChannelRepo)

      verify(mChannelRepo).updateChannel(
        chanC1.withChannelState(ChannelState.UP),
        eventProcessor.publish
      )

    }
    "When having a second channel not Local on Hold, up the channel from the hold event" in new Helper {
      val device1000 = "1000"
      val device1002 = "1002"
      val chanC1: Channel = bchan("SIP/1k4yj2", st = ChannelState.HOLD)
      val chanC2: Channel = bchan(
        "SIP/1k4yj2",
        callerId = CallerId("Jack", device1000),
        connectedLine = device1002,
        st = ChannelState.HOLD
      )
      val unHold = new UnholdEvent("")
      unHold.setCallerIdNum(device1000)
      unHold.setConnectedLineNum(device1002)
      unHold.setUniqueId(chanC1.id)

      when(mChannelRepo.get(chanC1.id)).thenReturn(Some(chanC1))
      when(mChannelRepo.getByNumbers(device1000, device1002))
        .thenReturn(Some(chanC2))

      eventProcessor.process(AmiEvent(unHold), mChannelRepo)

      verify(mChannelRepo).updateChannel(
        chanC1.withChannelState(ChannelState.UP),
        eventProcessor.publish
      )

    }
  }

  "Ami event processor on agent called" should {

    "update an existing channel with agent/queue info" in new Helper() {

      val agentCalled = new AgentCalledEvent("")
      agentCalled.setMemberName("Agent/2200")
      agentCalled.setUniqueId("1416910889.110")

      val channel: Channel = Channel(
        "1416910889.110",
        "SIP/8r1dcy-00000006",
        CallerId("hawkeye", "1002"),
        "1416910889.110"
      )

      var chans: HashMap[AmiObjectReference, Channel] = HashMap(channel.id -> channel)

      var updatedChannels: ChannelRepository =
        eventProcessor.process(AmiEvent(agentCalled), ChannelRepository(chans))

      updatedChannels(channel.id).agentNumber should be(Some("2200"))

      verify(amiBus).publish(ChannelEvent(updatedChannels(channel.id)))
    }
  }

  "Ami event processor on new connected line event" should {
    "update an existing channel with caller and calee info and publish" in new Helper() {

      val newConnectedLine = new NewConnectedLineEvent("")
      newConnectedLine.setChannel("SIP/thjuml-00000025")
      newConnectedLine.setUniqueId("1455802990.37")
      newConnectedLine.setConnectedLineNum("1003")
      newConnectedLine.setConnectedLineName("Aaron Lewi")
      newConnectedLine.setChannelState(6)

      val channel: Channel = Channel(
        "1455802990.37",
        "SIP/thjuml-00000025",
        CallerId("hawkeye", "1002"),
        "1455802990.37",
        connectedLineNb = Some("1001"),
        connectedLineName = scala.Some("John Doe")
      )

      var chans: HashMap[AmiObjectReference, Channel] = HashMap(channel.id -> channel)

      var updatedChannels: ChannelRepository = eventProcessor.process(
        AmiEvent(newConnectedLine),
        ChannelRepository(chans)
      )

      updatedChannels(channel.id).connectedLineNb should be(Some("1003"))
      updatedChannels(channel.id).connectedLineName should be(
        Some("Aaron Lewi")
      )

      verify(amiBus).publish(
        ChannelEvent(updatedChannels(channel.id), resended = true)
      )
    }
  }

  "Ami event processor on attendedTransfer event" should {
    "update an existing channel with new callee info" in new Helper() {

      val attendedTransfer = new AttendedTransferEvent("")
      attendedTransfer.setTransfereeUniqueid("2455802990.37")
      attendedTransfer.setSecondTransfererConnectedLineNum("1003")
      attendedTransfer.setSecondTransfererConnectedLineName("Django Reinhardt")

      val channel: Channel = Channel(
        "2455802990.37",
        "SIP/thjuml-00000025",
        CallerId("hawkeye", "1002"),
        "1455802990.37",
        connectedLineNb = Some("1001"),
        connectedLineName = scala.Some("John Doe")
      )

      var chans: HashMap[AmiObjectReference, Channel] = HashMap(channel.id -> channel)

      var updatedChannels: ChannelRepository = eventProcessor.process(
        AmiEvent(attendedTransfer),
        ChannelRepository(chans)
      )

      updatedChannels(channel.id).connectedLineNb should be(Some("1003"))
      updatedChannels(channel.id).connectedLineName should be(
        Some("Django Reinhardt")
      )
    }
  }

  "Ami event processor on coreShowChannel" should {
    "add a new channel for ongoing channels" in new Helper() {
      val coreShowChannelEvent = new CoreShowChannelEvent("")
      coreShowChannelEvent.setUniqueid("1455802990.11")
      coreShowChannelEvent.setChannel("SIP/thjuml-00000025")
      coreShowChannelEvent.setCallerIdName("hawkeye")
      coreShowChannelEvent.setCallerIdNum("1002")
      coreShowChannelEvent.setLinkedid("1455802990.37")

      var updatedChannels: ChannelRepository = eventProcessor.process(
        AmiEvent(coreShowChannelEvent),
        ChannelRepository(HashMap())
      )

      updatedChannels.channels.size shouldBe 1
      val (_, channel) = updatedChannels.channels.head

      channel.id should be("1455802990.11")
      channel.name should be("SIP/thjuml-00000025")
      channel.linkedChannelId should be("1455802990.37")
      channel.callerId shouldBe CallerId("hawkeye", "1002")
    }
  }
}
