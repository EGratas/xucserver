package services.request

import models.QueueMembership
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class AgentConfigRequestSpec extends AnyWordSpec with Matchers {

  "A MoveAgentInGroup" should {
    "be created from json" in {
      val expected = MoveAgentInGroup(1, 2, 3, 4, 5)
      val json = Json.parse(
        """{"groupId": 1, "fromQueueId": 2, "fromPenalty": 3, "toQueueId": 4, "toPenalty": 5}"""
      )
      MoveAgentInGroup.validate(json) match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(e)      => fail()
      }
    }
  }

  "A AddAgentInGroup" should {
    "be created from json" in {
      val expected = AddAgentInGroup(1, 2, 3, 4, 5)
      val json = Json.parse(
        """{"groupId": 1, "fromQueueId": 2, "fromPenalty": 3, "toQueueId": 4, "toPenalty": 5}"""
      )
      AddAgentInGroup.validate(json) match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(e)      => fail()
      }
    }
  }

  "A RemoveAgentGroupFromQueueGroup" should {
    "be created from json" in {
      val expected = RemoveAgentGroupFromQueueGroup(1, 2, 3)
      val json     = Json.parse("""{"groupId": 1, "queueId": 2, "penalty": 3}""")
      RemoveAgentGroupFromQueueGroup.validate(json) match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(e)      => fail()
      }
    }
  }

  "A AddAgentsNotInQueueFromGroupTo" should {
    "be created from json" in {
      val expected = AddAgentsNotInQueueFromGroupTo(1, 2, 3)
      val json     = Json.parse("""{"groupId": 1, "queueId": 2, "penalty": 3}""")
      AddAgentsNotInQueueFromGroupTo.validate(json) match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(e)      => fail()
      }
    }
  }

  "A SetAgentGroup" should {
    "be created from json" in {
      val expected = SetAgentGroup(1, 2)
      val json     = Json.parse("""{"agentId": 1, "groupId": 2}""")
      SetAgentGroup.validate(json) match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(e)      => fail()
      }
    }
  }

  "A GetUserDefaultMembership" should {
    "be created from json" in {
      val json: JsValue = Json.parse("""{"userId": 1234}""")
      GetUserDefaultMembership.validate(json) match {
        case JsSuccess(qm, _) => qm shouldEqual GetUserDefaultMembership(1234)
        case JsError(errors)  => fail()
      }
    }
  }

  "A SetUserDefaultMembership" should {
    "be created from json" in {
      val json: JsValue = Json.parse("""{
           "userId": 4321,
           "membership": [
              {"queueId": 8, "penalty": 1},
              {"queueId": 9, "penalty": 2},
              {"queueId": 18, "penalty": 3}]
        }""")
      val expected = SetUserDefaultMembership(
        4321,
        List(
          QueueMembership(8, 1),
          QueueMembership(9, 2),
          QueueMembership(18, 3)
        )
      )

      SetUserDefaultMembership.validate(json) match {
        case JsSuccess(qm, _) => qm shouldEqual expected
        case JsError(errors)  => fail()
      }
    }
  }

  "A SetUsersDefaultMembership" should {
    "be created from json" in {
      val json: JsValue = Json.parse("""{
           "userIds": [1,2,3,4],
           "membership": [
              {"queueId": 8, "penalty": 1},
              {"queueId": 9, "penalty": 2},
              {"queueId": 18, "penalty": 3}]
        }""")
      val expected = SetUsersDefaultMembership(
        List(1, 2, 3, 4),
        List(
          QueueMembership(8, 1),
          QueueMembership(9, 2),
          QueueMembership(18, 3)
        )
      )

      SetUsersDefaultMembership.validate(json) match {
        case JsSuccess(qm, _) => qm shouldEqual expected
        case JsError(errors)  => fail()
      }
    }
  }

  "An ApplyUsersDefaultMembership" should {
    "be created from json" in {
      val json: JsValue = Json.parse("""{"userIds": [1,2,5,9]}""")
      ApplyUsersDefaultMembership.validate(json) match {
        case JsSuccess(qm, _) =>
          qm shouldEqual ApplyUsersDefaultMembership(List(1, 2, 5, 9))
        case JsError(errors) => fail()
      }
    }
  }

}
