package services.request

import java.util.UUID
import models.{DynamicFilter, OperatorEq}
import models.{CallbackStatus, FindCallbackRequest, QueueMembership}
import org.joda.time.DateTime
import org.scalatest.BeforeAndAfterEach
import play.api.libs.json.Json
import services.config.ConfigDispatcher._
import services.request.PhoneRequest._
import xivo.models.FindCustomerCallHistoryRequest
import xivo.websocket.WsConferenceParticipantOrganizerRole
import xivo.xucami.models.QueueCall
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import services.video.model.{
  InviteToMeetingRoom,
  MeetingRoomInviteAccept,
  MeetingRoomInviteAck,
  MeetingRoomInviteReject,
  VideoEvent
}
import xivo.phonedevices.WebRTCDeviceBearer.{MobileApp, WebApp}
import play.api.libs.json.JsObject

class RequestDecoderSpec
    extends AnyWordSpec
    with Matchers
    with BeforeAndAfterEach {

  var decoder: RequestDecoder = null

  def browserMsg(cmd: String): JsObject =
    Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> cmd)

  override def beforeEach(): Unit = {
    decoder = new RequestDecoder()
  }

  "A decoder" should {

    "reject invalid message" in {
      val pingMsg = Json.toJson("invalid")

      val decoded = decoder.decode(pingMsg)

      decoded should be(
        InvalidRequest(XucRequest.errors.invalid, pingMsg.toString())
      )

    }
    "decode ping message" in {
      val pingMsg = Json.obj("claz" -> "ping")

      val decoded = decoder.decode(pingMsg)

      decoded should be(Ping)

    }

    "reject web class message without command" in {
      val invalidWeb = Json.obj("claz" -> XucRequest.WebClass)

      decoder.decode(invalidWeb) shouldBe an[InvalidRequest]
    }

    "decode moveAgentsInGroup command" in {
      val moveAgentInGroup = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "moveAgentsInGroup",
        "groupId"      -> 27,
        "fromQueueId"  -> 59,
        "fromPenalty"  -> 0,
        "toQueueId"    -> 157,
        "toPenalty"    -> 2
      )
      val decoded = decoder.decode(moveAgentInGroup)

      decoded should be(MoveAgentInGroup(27, 59, 0, 157, 2))
    }

    "decode addAgentsInGroup command" in {
      val addAgentInGroup = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "addAgentsInGroup",
        "groupId"      -> 23,
        "fromQueueId"  -> 42,
        "fromPenalty"  -> 1,
        "toQueueId"    -> 56,
        "toPenalty"    -> 2
      )

      val decoded = decoder.decode(addAgentInGroup)

      decoded should be(AddAgentInGroup(23, 42, 1, 56, 2))

    }
    "decode removeAgentGroupFromQueueGroup command" in {
      val groupId            = 131
      val (queueId, penalty) = (235, 2)

      val removeAgentGroupFromQueueGroup = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "removeAgentGroupFromQueueGroup",
        "groupId"      -> groupId,
        "queueId"      -> queueId,
        "penalty"      -> penalty
      )

      val decoded = decoder.decode(removeAgentGroupFromQueueGroup)

      decoded should be(
        RemoveAgentGroupFromQueueGroup(groupId, queueId, penalty)
      )

    }
    "decode addAgentsNotInQueueFromGroupTo command" in {
      val groupId            = 123
      val (queueId, penalty) = (342, 5)

      val addAgentsNotInQueueFromGroupTo = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "addAgentsNotInQueueFromGroupTo",
        "groupId"      -> groupId,
        "queueId"      -> queueId,
        "penalty"      -> penalty
      )
      val decoded = decoder.decode(addAgentsNotInQueueFromGroupTo)

      decoded should be(
        AddAgentsNotInQueueFromGroupTo(groupId, queueId, penalty)
      )

    }
    "decode agent listen command" in {
      val agentId = 675

      val agentListen = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "listenAgent",
        "agentid"      -> agentId
      )

      val decoded = decoder.decode(agentListen)

      decoded should be(AgentListen(agentId))
    }
    "decode subscribe to agent events" in {
      val decoder = new RequestDecoder()

      val subscribetoagents = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "subscribeToAgentEvents"
      )

      val decoded = decoder.decode(subscribetoagents)

      decoded should be(SubscribeToAgentEvents)

    }
    "decode get config request" in {
      import services.config.ObjectType._

      val getConfig = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getConfig",
        "objectType"   -> "queue"
      )

      val decoded = decoder.decode(getConfig)

      decoded should be(GetConfig(TypeQueue))
    }
    "decode get list request" in {
      import services.config.ObjectType._

      val getList = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getList",
        "objectType"   -> "queuemember"
      )

      val decoded = decoder.decode(getList)

      decoded should be(GetList(TypeQueueMember))
    }
    "decode get agent state request" in {
      val getAgentStates = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getAgentStates"
      )

      val decoded = decoder.decode(getAgentStates)

      decoded should be(GetAgentStates)

    }
    "decode get agent directory request" in {
      val getAgentDirectory = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getAgentDirectory"
      )

      val decoded = decoder.decode(getAgentDirectory)

      decoded should be(GetAgentDirectory)

    }

    "decode subscribeToQueueStats request" in {
      val subscribeToQueueStats = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "subscribeToQueueStats"
      )

      val decoded = decoder.decode(subscribeToQueueStats)

      decoded should be(SubscribeToQueueStats)
    }

    "decode monitorPause request" in {
      val monitorPause = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "monitorPause",
        "agentid"      -> 10
      )

      val decoded = decoder.decode(monitorPause)

      decoded should be(MonitorPause(10))
    }

    "decode monitorUnpause request" in {
      val monitorUnpause = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "monitorUnpause",
        "agentid"      -> 11
      )

      val decoded = decoder.decode(monitorUnpause)

      decoded should be(MonitorUnpause(11))
    }

    "decode agent login request" in {
      val agentLoginRequest =
        browserMsg("agentLogin") ++ Json.obj("agentid" -> 55)

      val decoded = decoder.decode(agentLoginRequest)

      decoded should be(AgentLoginRequest(Some(55), None))
    }

    "decode agent logout request" in {
      val agentLogoutRequest =
        browserMsg("agentLogout") ++ Json.obj("agentid" -> 787)

      val decoded = decoder.decode(agentLogoutRequest)

      decoded should be(AgentLogoutRequest(Some(787)))
    }
    "decode agent logout request without Agent Id" in {
      val agentLogoutRequest = browserMsg("agentLogout")

      val decoded = decoder.decode(agentLogoutRequest)

      decoded should be(AgentLogoutRequest(None))
    }

    "decode agent pause request without reason" in {
      val agentPauseRequest =
        browserMsg("pauseAgent") ++ Json.obj("agentid" -> 514)

      val decoded = decoder.decode(agentPauseRequest)

      decoded should be(AgentPauseRequest(Some(514)))
    }

    "decode agent pause request with reason" in {
      val agentPauseRequest =
        browserMsg("pauseAgent") ++ Json.obj("agentid" -> 514) ++ Json.obj(
          "reason" -> "testReason"
        )

      val decoded = decoder.decode(agentPauseRequest)

      decoded should be(AgentPauseRequest(Some(514), Some("testReason")))
    }

    "decode unpause agent request" in {
      val agentUnPauseRequest =
        browserMsg("unpauseAgent") ++ Json.obj("agentid" -> 514)

      val decoded = decoder.decode(agentUnPauseRequest)

      decoded should be(AgentUnPauseRequest(Some(514)))
    }

    "decode invite conference room request" in {
      val inviteConfRequest = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "inviteConferenceRoom",
        "userId"       -> 55
      )

      val decoded = decoder.decode(inviteConfRequest)

      decoded should be(InviteConferenceRoom(55))
    }

    "decode na forward request" in {
      val naForwardRequest = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "naFwd",
        "state"        -> true,
        "destination"  -> "1102"
      )

      val decoded = decoder.decode(naForwardRequest)

      decoded should be(NaForward("1102", true))
    }

    "decode unc forward request" in {
      val uncForwardRequest = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "uncFwd",
        "state"        -> false,
        "destination"  -> "2256"
      )

      val decoded = decoder.decode(uncForwardRequest)

      decoded should be(UncForward("2256", false))
    }

    "decode busy forward request" in {
      val busyForwardRequest = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "busyFwd",
        "state"        -> true,
        "destination"  -> "2233"
      )

      val decoded = decoder.decode(busyForwardRequest)

      decoded should be(BusyForward("2233", true))
    }

    "decode subscribe to agents stats" in {
      val subscribeToAgentStats = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "subscribeToAgentStats"
      )

      val decoded = decoder.decode(subscribeToAgentStats)

      decoded should be(SubscribeToAgentStats)

    }

    "decode subscribe to phone hints" in {
      val subscribeToPhoneHints = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "subscribeToPhoneHints",
        "phoneNumbers" -> List("1005", "1099")
      )

      decoder.decode(subscribeToPhoneHints) should be(
        SubscribeToPhoneHints(List("1005", "1099"))
      )
    }

    "decode unsubscribe from all phone hints" in {
      val unsubscribeFromAllPhoneHints = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "unsubscribeFromAllPhoneHints"
      )

      decoder.decode(unsubscribeFromAllPhoneHints) should be(
        UnsubscribeFromAllPhoneHints
      )
    }

    "decode subscribe to queue calls" in {
      val queueId = 5
      val subscribeToQueueCalls = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "subscribeToQueueCalls",
        "queueId"      -> queueId
      )

      decoder.decode(subscribeToQueueCalls) should be(
        SubscribeToQueueCalls(queueId)
      )
    }

    "decode unsubscribe to queue calls" in {
      val queueId = 5
      val unSubscribeToQueueCalls = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "unSubscribeToQueueCalls",
        "queueId"      -> queueId
      )

      decoder.decode(unSubscribeToQueueCalls) should be(
        UnSubscribeToQueueCalls(queueId)
      )
    }

    "decode get agent call history" in {
      val size = 7
      val getCallHistory = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getAgentCallHistory",
        "size"         -> size
      )

      decoder.decode(getCallHistory) should be(GetAgentCallHistory(size))
    }

    "decode get customer call history" in {
      val size    = 7
      val srcNum  = Some("1234")
      var filters = List(DynamicFilter("src_num", Some(OperatorEq), srcNum))
      var req     = Json.obj("filters" -> filters, "size" -> 100)
      val getCallHistory = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "findCustomerCallHistory",
        "id"           -> 1,
        "request"      -> req
      )

      decoder.decode(getCallHistory) should be(
        CustomerCallHistoryRequestWithId(
          1,
          FindCustomerCallHistoryRequest(filters, 100)
        )
      )
    }

    "throw an error if asked to get user call history with no parameters" in {
      val getCallHistory = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getUserCallHistory"
      )

      decoder.decode(getCallHistory).getClass shouldBe classOf[InvalidRequest]
    }

    "decode get user call history with size" in {
      val size = Some(7)
      val getCallHistory = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getUserCallHistory",
        "size"         -> size
      )

      decoder.decode(getCallHistory) should be(
        GetUserCallHistory(HistorySize(7))
      )
    }

    "decode get user call history with days" in {
      val days = Some(7)
      val getCallHistory = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getUserCallHistoryByDays",
        "days"         -> days
      )

      decoder.decode(getCallHistory) should be(
        GetUserCallHistoryByDays(HistoryDays(7))
      )
    }

    "decode get queue call history" in {
      val queue = "bl"
      val size  = 7
      val getQueueCallHistory = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getQueueCallHistory",
        "queue"        -> queue,
        "size"         -> size
      )

      decoder.decode(getQueueCallHistory) should be(
        GetQueueCallHistory(queue, size)
      )
    }
    "decode set agent group" in {
      val setAgentGroup = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "setAgentGroup",
        "groupId"      -> 7,
        "agentId"      -> 23
      )

      decoder.decode(setAgentGroup) shouldEqual SetAgentGroup(23, 7)
    }

    "decode directory look up" in {
      val directoryLookUp = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "directoryLookUp",
        "term"         -> "test"
      )

      decoder.decode(directoryLookUp) shouldEqual DirectoryLookUp("test")
    }

    "decode get favorites" in {
      val directoryLookUp = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getFavorites"
      )

      decoder.decode(directoryLookUp) shouldEqual GetFavorites
    }

    "decode set favorite" in {
      val setFavorite = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "addFavorite",
        "contactId"    -> "123",
        "source"       -> "xivou"
      )

      decoder.decode(setFavorite) shouldEqual AddFavorite("123", "xivou")
    }

    "decode remove favorite" in {
      val removeFavorite = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "removeFavorite",
        "contactId"    -> "123",
        "source"       -> "xivou"
      )

      decoder.decode(removeFavorite) shouldEqual RemoveFavorite("123", "xivou")
    }

    "decode GetCallbackLists" in {
      val getCallbacks = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getCallbackLists"
      )

      decoder.decode(getCallbacks) shouldEqual GetCallbackLists
    }

    "decode FindCallbackRequest" in {
      val getCallbacks = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "findCallbackRequest",
        "id"           -> 1,
        "request" -> Json.obj(
          "filters" -> List.empty[DynamicFilter],
          "offset"  -> 0,
          "limit"   -> 100
        )
      )

      decoder.decode(getCallbacks) shouldEqual FindCallbackRequestWithId(
        1,
        FindCallbackRequest(List.empty, 0, 100)
      )
    }

    "decode takeCallback" in {
      val uuid = UUID.randomUUID()
      val takeCallback = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "takeCallback",
        "uuid"         -> uuid.toString
      )

      decoder.decode(takeCallback) shouldEqual TakeCallback(uuid.toString)
    }

    "decode releaseCallback" in {
      val uuid = UUID.randomUUID()
      val releaseCallback = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "releaseCallback",
        "uuid"         -> uuid.toString
      )

      decoder.decode(releaseCallback) shouldEqual ReleaseCallback(uuid.toString)
    }

    "decode startCallback" in {
      val uuid = UUID.randomUUID()
      val startCallback = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "startCallback",
        "uuid"         -> uuid.toString,
        "number"       -> "233124"
      )

      decoder.decode(startCallback) shouldEqual StartCallback(
        uuid.toString,
        "233124"
      )
    }

    "decode dial with variables" in {
      val dial = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "dial",
        "destination"  -> "1000",
        "variables"    -> Json.obj("var1" -> "val1", "var2" -> "val2")
      )
      decoder.decode(dial) shouldEqual Dial(
        "1000",
        Map("var1" -> "val1", "var2" -> "val2")
      )
    }

    "decode dial without variables" in {
      val dial = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "dial",
        "destination"  -> "1000"
      )
      decoder.decode(dial) shouldEqual Dial("1000")
    }

    "decode dialFromMobile with variables" in {
      val dial = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "dialFromMobile",
        "destination"  -> "1000",
        "variables"    -> Json.obj("var1" -> "val1", "var2" -> "val2")
      )
      decoder.decode(dial) shouldEqual DialFromMobile(
        "1000",
        Map("var1" -> "val1", "var2" -> "val2")
      )
    }

    "decode dialFromMobile without variables" in {
      val dial = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "dialFromMobile",
        "destination"  -> "1000"
      )
      decoder.decode(dial) shouldEqual DialFromMobile("1000")
    }

    "decode dialByUsername with variables" in {
      val dialByUsername = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "dialByUsername",
        "username"     -> "userone",
        "variables"    -> Json.obj("var1" -> "val1", "var2" -> "val2")
      )
      decoder.decode(dialByUsername) shouldEqual DialByUsername(
        "userone",
        Map("var1" -> "val1", "var2" -> "val2"),
        "default"
      )
    }

    "decode updateCallbackTicket" in {
      val uuid = UUID.randomUUID()
      val updateCallbackTicket = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "updateCallbackTicket",
        "uuid"         -> uuid.toString,
        "status"       -> "Answered",
        "comment"      -> "A small comment"
      )

      decoder.decode(updateCallbackTicket) shouldEqual UpdateCallbackTicket(
        uuid.toString,
        Some(CallbackStatus.Answered),
        Some("A small comment")
      )
    }

    "decode updateCallbackTicket with reschedule information" in {
      val uuid       = UUID.randomUUID()
      val periodUuid = UUID.randomUUID().toString
      val updateCallbackTicket = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "updateCallbackTicket",
        "uuid"         -> uuid.toString,
        "status"       -> "Callback",
        "comment"      -> "A small comment",
        "dueDate"      -> "2016-08-09",
        "periodUuid"   -> periodUuid
      )

      decoder.decode(updateCallbackTicket) shouldEqual UpdateCallbackTicket(
        uuid.toString,
        Some(CallbackStatus.Callback),
        Some("A small comment"),
        Some(DateTime.parse("2016-08-09")),
        Some(periodUuid)
      )
    }

    "decode answer without uniqueId" in {
      val answer =
        Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "answer")
      decoder.decode(answer) shouldEqual Answer(None)
    }

    "decode answer with uniqueId" in {
      val hangup =
        Json.obj(
          "claz"         -> XucRequest.WebClass,
          XucRequest.Cmd -> "answer",
          "uniqueId"     -> "123456789.0"
        )
      decoder.decode(hangup) shouldEqual Answer(Some("123456789.0"))
    }

    "decode hangup without uniqueId" in {
      val hangup =
        Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "hangup")
      decoder.decode(hangup) shouldEqual Hangup(None)
    }

    "decode hangup with uniqueId" in {
      val hangup =
        Json.obj(
          "claz"         -> XucRequest.WebClass,
          XucRequest.Cmd -> "hangup",
          "uniqueId"     -> "123456789.0"
        )
      decoder.decode(hangup) shouldEqual Hangup(Some("123456789.0"))
    }

    "decode toggleMicrophone" in {
      val toggleMicrophone = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "toggleMicrophone",
        "uniqueId"     -> "123456789.0"
      )
      decoder.decode(toggleMicrophone) shouldEqual ToggleMicrophone(
        Some("123456789.0")
      )
    }

    "decode attendedTransfer" in {
      val attendedTransfer = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "attendedTransfer",
        "destination"  -> "1000"
      )
      decoder.decode(attendedTransfer) shouldEqual AttendedTransfer(
        "1000",
        None
      )

      val attendedTransferWithBearer1 = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "attendedTransfer",
        "destination"  -> "1000",
        "device"       -> "MobileApp"
      )
      decoder.decode(attendedTransferWithBearer1) shouldEqual AttendedTransfer(
        "1000",
        Some(MobileApp)
      )

      val attendedTransferWithBearer2 = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "attendedTransfer",
        "destination"  -> "1000",
        "device"       -> "WebApp"
      )
      decoder.decode(attendedTransferWithBearer2) shouldEqual AttendedTransfer(
        "1000",
        Some(WebApp)
      )

      val attendedTransferWithFakeBearer = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "attendedTransfer",
        "destination"  -> "1000",
        "device"       -> "Crap"
      )
      an[IllegalArgumentException] should be thrownBy decoder.decode(
        attendedTransferWithFakeBearer
      )
    }

    "decode completeTransfer" in {
      val completeTransfer = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "completeTransfer"
      )
      decoder.decode(completeTransfer) shouldEqual CompleteTransfer
    }

    "decode cancelTransfer" in {
      val answer = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "cancelTransfer"
      )
      decoder.decode(answer) shouldEqual CancelTransfer
    }

    "decode direct transfer" in {
      val attendedTransfer = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "directTransfer",
        "destination"  -> "1000"
      )
      decoder.decode(attendedTransfer) shouldEqual DirectTransfer("1000")
    }

    "decode conference" in {
      val answer =
        Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "conference")
      decoder.decode(answer) shouldEqual Conference
    }

    "decode hold without uniqueId" in {
      val hold =
        Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "hold")
      decoder.decode(hold) shouldEqual Hold(None)
    }

    "decode hold with uniqueId" in {
      val hangup =
        Json.obj(
          "claz"         -> XucRequest.WebClass,
          XucRequest.Cmd -> "hold",
          "uniqueId"     -> "123456789.0"
        )
      decoder.decode(hangup) shouldEqual Hold(Some("123456789.0"))
    }

    "decode setAgentQueue" in {
      val setAgentQueue = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "setAgentQueue",
        "agentId"      -> 33,
        "queueId"      -> 54,
        "penalty"      -> 2
      )
      decoder.decode(setAgentQueue) shouldEqual SetAgentQueue(33, 54, 2)
    }

    "decode getUserDefaultMembership" in {
      val json = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getUserDefaultMembership",
        "userId"       -> 1234
      )
      decoder.decode(json) shouldEqual GetUserDefaultMembership(1234)
    }

    "decode applyUsersDefaultMembership" in {
      val json = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "applyUsersDefaultMembership",
        "userIds"      -> List(1, 2, 3, 4)
      )
      decoder.decode(json) shouldEqual ApplyUsersDefaultMembership(
        List(1, 2, 3, 4)
      )
    }

    "decode setUserDefaultMembership" in {
      val json = Json.parse(s"""{
           "claz": "${XucRequest.WebClass}",
           "${XucRequest.Cmd}": "setUserDefaultMembership",
           "userId": 4321,
             "membership": [
                {"queueId": 8, "penalty": 1},
                {"queueId": 9, "penalty": 2},
                {"queueId": 18, "penalty": 3}]
        }""")

      val expected = SetUserDefaultMembership(
        4321,
        List(
          QueueMembership(8, 1),
          QueueMembership(9, 2),
          QueueMembership(18, 3)
        )
      )

      decoder.decode(json) shouldEqual expected
    }

    "decode setUsersDefaultMembership" in {
      val json = Json.parse(s"""{
           "claz": "${XucRequest.WebClass}",
           "${XucRequest.Cmd}": "setUsersDefaultMembership",
           "userIds": [1,2,3,4],
             "membership": [
                {"queueId": 8, "penalty": 1},
                {"queueId": 9, "penalty": 2},
                {"queueId": 18, "penalty": 3}]
        }""")

      val expected = SetUsersDefaultMembership(
        List(1, 2, 3, 4),
        List(
          QueueMembership(8, 1),
          QueueMembership(9, 2),
          QueueMembership(18, 3)
        )
      )

      decoder.decode(json) shouldEqual expected
    }
    "decode set Data" in {
      val dial = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "setData",
        "variables"    -> Json.obj("var1" -> "val1", "var2" -> "val2")
      )
      decoder.decode(dial) shouldEqual SetData(
        Map("var1" -> "val1", "var2" -> "val2")
      )
    }

    "decode remove agent" in {
      val removeAgent = browserMsg("removeAgentFromQueue") ++ Json.obj(
        "agentId" -> 32,
        "queueId" -> 21
      )

      decoder.decode(removeAgent) shouldBe RemoveAgentFromQueue(32, 21)
    }

    "decode change user status request" in {
      val changeUserStatus =
        browserMsg("userStatusUpdate") ++ Json.obj("status" -> "away")

      decoder.decode(changeUserStatus) shouldBe UserStatusUpdateReq(
        None,
        "away"
      )
    }

    "decode dnd request" in {
      val dndRequest = browserMsg("dnd") ++ Json.obj("state" -> true)

      decoder.decode(dndRequest) shouldBe DndReq(true)

    }

    "decode get queue statistics" in {
      val getQueueStatisticsRequest = browserMsg("getQueueStatistics") ++ Json
        .obj("queueId" -> 20, "window" -> 60, "xqos" -> 3600)

      decoder.decode(getQueueStatisticsRequest) shouldBe QueueStatisticsReq(
        20,
        60,
        3600
      )
    }

    "decode get current calls phone events" in {
      val getCurrentCallsPhoneEvents = browserMsg("getCurrentCallsPhoneEvents")

      decoder.decode(
        getCurrentCallsPhoneEvents
      ) shouldBe GetCurrentCallsPhoneEvents
    }

    "decode conferenceInvite command" in {
      val payload = Json.obj(
        "numConf"   -> "4000",
        "exten"     -> "0102030405",
        "role"      -> "Organizer",
        "earlyJoin" -> true
      )
      val m = browserMsg("conferenceInvite") ++ payload

      decoder.decode(m) shouldBe ConferenceInvite(
        "4000",
        "0102030405",
        WsConferenceParticipantOrganizerRole,
        true
      )
    }

    "decode conferenceMuteMe command" in {
      val m = browserMsg("conferenceMuteMe") ++ Json.obj("numConf" -> "4000")

      decoder.decode(m) shouldBe ConferenceMuteMe("4000")
    }

    "decode conferenceUnmuteMe command" in {
      val m = browserMsg("conferenceUnmuteMe") ++ Json.obj("numConf" -> "4000")

      decoder.decode(m) shouldBe ConferenceUnmuteMe("4000")
    }

    "decode conferenceMuteAll command" in {
      val m = browserMsg("conferenceMuteAll") ++ Json.obj("numConf" -> "4000")

      decoder.decode(m) shouldBe ConferenceMuteAll("4000")
    }

    "decode conferenceUnmuteAll command" in {
      val m = browserMsg("conferenceUnmuteAll") ++ Json.obj("numConf" -> "4000")

      decoder.decode(m) shouldBe ConferenceUnmuteAll("4000")
    }

    "decode conferenceKick command" in {
      val m = browserMsg("conferenceKick") ++ Json.obj(
        "numConf" -> "4000",
        "index"   -> 1
      )

      decoder.decode(m) shouldBe ConferenceKick("4000", 1)
    }

    "decode conferenceClose command" in {
      val m = browserMsg("conferenceClose") ++ Json.obj("numConf" -> "4000")

      decoder.decode(m) shouldBe ConferenceClose("4000")
    }

    "decode conferenceDeafen command" in {
      val m = browserMsg("conferenceDeafen") ++ Json.obj(
        "numConf" -> "4000",
        "index"   -> 1
      )

      decoder.decode(m) shouldBe ConferenceDeafen("4000", 1)
    }

    "decode conferenceUndeafen command" in {
      val m = browserMsg("conferenceUndeafen") ++ Json.obj(
        "numConf" -> "4000",
        "index"   -> 1
      )

      decoder.decode(m) shouldBe ConferenceUndeafen("4000", 1)
    }

    "decode conferenceReset command" in {
      val m = browserMsg("conferenceReset") ++ Json.obj(
        "numConf" -> "4000"
      )

      decoder.decode(m) shouldBe ConferenceReset("4000")
    }

    "decode sendDtmf request" in {
      val sendDtmf = browserMsg("sendDtmf") ++ Json.obj("key" -> "3")

      decoder.decode(sendDtmf) shouldBe SendDtmfRequest('3')
    }

    "fail to decode sendDtmf request with more characters or empty string" in {
      val sendDtmfTwoKeys = browserMsg("sendDtmf") ++ Json.obj("key" -> "36")
      decoder
        .decode(sendDtmfTwoKeys)
        .getClass shouldBe classOf[InvalidRequest]
      val sendDtmfEmpty = browserMsg("sendDtmf") ++ Json.obj("key" -> "")
      decoder
        .decode(sendDtmfEmpty)
        .getClass shouldBe classOf[InvalidRequest]
    }

    "decode retrieveQueueCall request" in {
      val now = DateTime.now
      val queueCall =
        QueueCall(1, Some("Graham Bell"), "12345", now, "SIP/defg", "main")

      val json = Json.parse(s"""{
           "claz": "${XucRequest.WebClass}",
           "${XucRequest.Cmd}": "retrieveQueueCall",
           "queueCall": {
              "position": 1,
              "name": "Graham Bell",
              "number": "12345",
              "queueTime": "$now",
              "channel": "SIP/defg",
              "mdsName": "main"
           }
        }""")

      decoder.decode(json) shouldBe RetrieveQueueCall(queueCall)
    }

    "decode retrieveQueueCall with null name request" in {
      val now       = DateTime.now
      val queueCall = QueueCall(1, None, "12345", now, "SIP/defg", "main")

      val json = Json.parse(s"""{
           "claz": "${XucRequest.WebClass}",
           "${XucRequest.Cmd}": "retrieveQueueCall",
           "queueCall": {
              "position": 1,
              "name": null,
              "number": "12345",
              "queueTime": "$now",
              "channel": "SIP/defg",
              "mdsName": "main"
           }
        }""")

      decoder.decode(json) shouldBe RetrieveQueueCall(queueCall)
    }

    "decode toggle phone device request" in {
      val toggleDevice = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "toggleUniqueAccountDevice",
        "deviceType"   -> "phone"
      )

      val decoded = decoder.decode(toggleDevice)

      decoded should be(ToggleUniqueAccountDevice(None, TypePhoneDevice))
    }

    "decode toggle webrtc device request" in {
      val toggleDevice = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "toggleUniqueAccountDevice",
        "deviceType"   -> "webrtc"
      )

      val decoded = decoder.decode(toggleDevice)

      decoded should be(ToggleUniqueAccountDevice(None, TypeDefaultDevice))
    }

    "decode toggle unknown device request fallback to webrtc" in {
      val toggleDevice = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "toggleUniqueAccountDevice",
        "deviceType"   -> "unknown"
      )

      val decoded = decoder.decode(toggleDevice)

      decoded should be(ToggleUniqueAccountDevice(None, TypeDefaultDevice))
    }

    "decode display name lookup request" in {
      val displayNameLookup = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "displayNameLookup",
        "username"     -> "jbond"
      )

      val decoded = decoder.decode(displayNameLookup)

      decoded should be(DisplayNameLookup("jbond"))
    }

    "decode add error to log" in {
      val pushLogToServer = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "pushLogToServer",
        "level"        -> "error",
        "log"          -> "The squirrels are starving !"
      )

      val decoded = decoder.decode(pushLogToServer)
      decoded should be(
        PushLogToServer("error", "The squirrels are starving !")
      )
    }

    "decode get ice config request" in {
      val getIceConfig = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "getIceConfig"
      )
      val decoded = decoder.decode(getIceConfig)
      decoded should be(GetIceConfig)
    }

    "decode video event" in {
      val videoStartRequest = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "videoEvent",
        "status"       -> "videoStart"
      )
      val decodedStart = decoder.decode(videoStartRequest)
      decodedStart should be(VideoEvent("videoStart"))

      val videoEndRequest = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "videoEvent",
        "status"       -> "videoEnd"
      )
      val decodedEnd = decoder.decode(videoEndRequest)
      decodedEnd should be(VideoEvent("videoEnd"))
    }

    "decode subscribe to video status" in {
      val subscribeToVideoStatus = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "subscribeToVideoStatus",
        "usernames"    -> List("ahonnet", "alder")
      )

      decoder.decode(subscribeToVideoStatus) should be(
        SubscribeToVideoStatus(List("ahonnet", "alder"))
      )
    }

    "decode unsubscribe from all video status" in {
      val unsubscribeFromAllVideoStatus = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "unsubscribeFromAllVideoStatus"
      )

      decoder.decode(unsubscribeFromAllVideoStatus) should be(
        UnsubscribeFromAllVideoStatus
      )
    }

    "decode inviteToMeetingRoom" in {
      val inviteToMeetingRoom = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "inviteToMeetingRoom",
        "requestId"    -> 42,
        "token"        -> "my.token",
        "username"     -> "johnDoe"
      )

      decoder.decode(inviteToMeetingRoom) should be(
        InviteToMeetingRoom(42, "my.token", "johnDoe")
      )
    }

    "decode meetingRoomInviteAck" in {
      val meetingRoomInviteAck = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "meetingRoomInviteAck",
        "requestId"    -> 42,
        "username"     -> "johnDoe"
      )

      decoder.decode(meetingRoomInviteAck) should be(
        MeetingRoomInviteAck(42, "johnDoe")
      )
    }

    "decode meetingRoomInviteAccept" in {
      val meetingRoomInviteAccept = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "meetingRoomInviteAccept",
        "requestId"    -> 42,
        "username"     -> "johnDoe"
      )

      decoder.decode(meetingRoomInviteAccept) should be(
        MeetingRoomInviteAccept(42, "johnDoe")
      )
    }

    "decode meetingRoomInviteReject" in {
      val meetingRoomInviteReject = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "meetingRoomInviteReject",
        "requestId"    -> 42,
        "username"     -> "johnDoe"
      )

      decoder.decode(meetingRoomInviteReject) should be(
        MeetingRoomInviteReject(42, "johnDoe")
      )
    }

    "decode setUserPreference request" in {
      val setUserPreferenceRequest = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "setUserPreference",
        "key"          -> "PREFERRED_DEVICE",
        "value"        -> "TypePhoneDevice",
        "value_type"   -> "String"
      )

      decoder.decode(setUserPreferenceRequest) should be(
        SetUserPreferenceRequest(
          None,
          "PREFERRED_DEVICE",
          "TypePhoneDevice",
          "String"
        )
      )
    }

    "decode unregisterMobileApp request" in {
      val unregisterMobileAppRequest = Json.obj(
        "claz"         -> XucRequest.WebClass,
        XucRequest.Cmd -> "unregisterMobileApp",
        "requestId"    -> 42,
        "username"     -> "johnDoe"
      )

      decoder.decode(unregisterMobileAppRequest) should be(
        UnregisterMobileApp(Some("johnDoe"))
      )
    }
  }
}
