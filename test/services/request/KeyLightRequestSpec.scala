package services.request

import services.XucAmiBus.{SetVarActionRequest, SetVarRequest}
import xuctest.BaseTest

class KeyLightRequestSpec extends BaseTest {

  "Turning light on" should {

    "be transformed to ami request for pause key" in {
      val phoneNb     = "24300"
      val turnOnPause = TurnOnKeyLight(phoneNb, PhoneKey.Pause)

      turnOnPause.toAmi should be(
        SetVarRequest(
          SetVarActionRequest(s"DEVICE_STATE(Custom:***34$phoneNb)", "INUSE")
        )
      )
    }
    "be transformed to ami request for logon key" in {
      val phoneNb     = "27300"
      val turnOnPause = TurnOnKeyLight(phoneNb, PhoneKey.Logon)

      turnOnPause.toAmi should be(
        SetVarRequest(
          SetVarActionRequest(s"DEVICE_STATE(Custom:***30$phoneNb)", "INUSE")
        )
      )
    }
    "be transformed to ami request for spy key" in {
      val phoneNb   = "24300"
      val turnOnSpy = TurnOnKeyLight(phoneNb, PhoneKey.Spied)

      turnOnSpy.toAmi should be(
        SetVarRequest(
          SetVarActionRequest(s"DEVICE_STATE(Custom:***35$phoneNb)", "INUSE")
        )
      )
    }
  }
  "Turning light off" should {

    "be transformed to ami request for pause key" in {
      val phoneNb      = "77300"
      val turnOffPause = TurnOffKeyLight(phoneNb, PhoneKey.Pause)

      turnOffPause.toAmi should be(
        SetVarRequest(
          SetVarActionRequest(
            s"DEVICE_STATE(Custom:***34$phoneNb)",
            "NOT_INUSE"
          )
        )
      )
    }
    "be transformed to ami request for logon key" in {
      val phoneNb      = "27300"
      val turnOffLogon = TurnOffKeyLight(phoneNb, PhoneKey.Logon)

      turnOffLogon.toAmi should be(
        SetVarRequest(
          SetVarActionRequest(
            s"DEVICE_STATE(Custom:***30$phoneNb)",
            "NOT_INUSE"
          )
        )
      )
    }
    "be transformed to ami request for spy key" in {
      val phoneNb    = "27300"
      val turnOffSpy = TurnOffKeyLight(phoneNb, PhoneKey.Spied)

      turnOffSpy.toAmi should be(
        SetVarRequest(
          SetVarActionRequest(
            s"DEVICE_STATE(Custom:***35$phoneNb)",
            "NOT_INUSE"
          )
        )
      )
    }
  }

  "Turning light off for key with Agent number" should {

    "be transformed to ami request for login key with agent number" in {
      val phoneNb = "77300"
      val agentNb = "8000"
      val turnOffWAgNum =
        TurnOffKeyLightWithAgNum(phoneNb, PhoneKey.Logon, agentNb)

      turnOffWAgNum.toAmi should be(
        SetVarRequest(
          SetVarActionRequest(
            s"DEVICE_STATE(Custom:***30$phoneNb*$agentNb)",
            "NOT_INUSE"
          )
        )
      )
    }
  }

  "Turning light on for key with Agent number" should {

    "be transformed to ami request for login key with agent number" in {
      val phoneNb = "77300"
      val agentNb = "8000"
      val turnOnWAgNum =
        TurnOnKeyLightWithAgNum(phoneNb, PhoneKey.Logon, agentNb)

      turnOnWAgNum.toAmi should be(
        SetVarRequest(
          SetVarActionRequest(
            s"DEVICE_STATE(Custom:***30$phoneNb*$agentNb)",
            "INUSE"
          )
        )
      )
    }
  }

  "Blink key" should {

    "be transformed to ami request for pause key" in {
      val phoneNb    = "32500"
      val blinkPause = BlinkKeyLight(phoneNb, PhoneKey.Pause)

      blinkPause.toAmi should be(
        SetVarRequest(
          SetVarActionRequest(s"DEVICE_STATE(Custom:***34$phoneNb)", "ONHOLD")
        )
      )
    }
  }

}
