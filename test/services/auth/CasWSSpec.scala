package services.auth

import models.ws.auth.{AuthenticationError, AuthenticationException}
import models.ws.sso._
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import play.api.libs.ws._
import xivo.network.XiVOWS
import xivo.xuc.XucBaseConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class CasWSSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar
    with ScalaFutures {
  implicit val defaultPatience: PatienceConfig =
    PatienceConfig(timeout = Span(2, Seconds), interval = Span(5, Millis))

  "CasWS" should {

    val testUrl     = "https://cas.server.com:443/cas"
    val serviceName = "http://my.app.com/"
    val ticket      = "TICKET-1234"

    class CasTestService() extends CasWS {
      val ws: WSClient = mock[WSClient]

      val mockRequest: WSRequest = mock[WSRequest]
      val mockResponse: WSResponse = mock[WSResponse]
      val config: XucBaseConfig = mock[XucBaseConfig]
      val xivoWs: XiVOWS = mock[XiVOWS]

      when(xivoWs.WS).thenReturn(ws)
      when(config.casServerUrl).thenReturn(Some(testUrl))

      when(ws.url(any[String])).thenReturn(mockRequest)
      when(mockRequest.withQueryStringParameters(any[(String, String)]))
        .thenReturn(mockRequest)

    }

    "validate a correct ticket for a given service" in new CasTestService() {
      val response: String =
        """<cas:serviceResponse xmlns:cas="http://www.yale.edu/tp/cas">
                       | <cas:authenticationSuccess>
                       |  <cas:user>jbond</cas:user>
                       | </cas:authenticationSuccess>
                       |</cas:serviceResponse>""".stripMargin

      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.body).thenReturn(response)

      whenReady(validate(serviceName, ticket, xivoWs, config)) { result =>
        result shouldBe CasAuthenticationSuccess(CasUser("jbond"), List.empty)
      }
    }

    "return a failure when the server is not configured" in new CasTestService() {
      when(config.casServerUrl).thenReturn(None)

      whenReady(validate(serviceName, ticket, xivoWs, config).failed) {
        result =>
          result shouldBe a[AuthenticationException]
      }
    }

    "return a failure when the server returns a status code other than 200" in new CasTestService() {
      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(400)

      whenReady(validate(serviceName, ticket, xivoWs, config).failed) {
        result =>
          result shouldBe a[AuthenticationException]
          val ex = result.asInstanceOf[AuthenticationException]
          ex.error shouldBe AuthenticationError.CasServerInvalidResponse
      }
    }

    "return a failure when the server returns unparseable content" in new CasTestService() {
      val response = "Garbage !!!!"

      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.body).thenReturn(response)

      whenReady(validate(serviceName, ticket, xivoWs, config).failed) {
        result =>
          result shouldBe a[AuthenticationException]
          val ex = result.asInstanceOf[AuthenticationException]
          ex.error shouldBe AuthenticationError.CasServerInvalidResponse
      }
    }

    "return a failure when the server returns an authentication failure" in new CasTestService() {
      val response: String =
        """<cas:serviceResponse xmlns:cas="http://www.yale.edu/tp/cas">
                       | <cas:authenticationFailure code="INVALID_TICKET">
                       |    Ticket ST-1856339-aA5Yuvrxzpv8Tau1cYQ7 not recognized
                       |  </cas:authenticationFailure>
                       |</cas:serviceResponse>""".stripMargin

      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.body).thenReturn(response)

      whenReady(validate(serviceName, ticket, xivoWs, config).failed) {
        result =>
          result shouldBe a[AuthenticationException]
          val ex = result.asInstanceOf[AuthenticationException]
          ex.error shouldBe AuthenticationError.CasServerInvalidTicket
      }
    }
  }
}
