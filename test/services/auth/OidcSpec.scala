package services.auth

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.testkit.TestProbe
import models.XivoUser
import models.ws.auth.{AuthenticationException, AuthenticationInformation, SoftwareType}
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito.*
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.Json
import play.api.libs.ws.*
import services.config.ConfigRepository
import xivo.xuc.XucBaseConfig
import xuctest.XucUserHelper

import scala.concurrent.Future
import scala.util.{Failure, Success}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.joda.time.DateTime
import models.XucUser

object OidcSampleToken extends XucUserHelper {
  val testUrl  = "http://localhost:8080/myrealm"
  val clientId = "xuc"
  val audience = "phone"

  /** Sample Token used for unit test
    *    {
    *      "sub": "1234567890",
    *      "name": "James bond",
    *      "admin": true,
    *      "iat": 1516239022,
    *      "aud":[
    *          "xuc",
    *          "phone"
    *       ],
    *       "exp": 1967664698,
    *       "azp": "xuc"
    *       "preferred_username": "jbond",
    *      "iss":"http://localhost:8080/myrealm"
    *    }
    */
  val token: String =
    """eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.
        |eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkphbWVzIGJvbmQiLCJhZG1pbiI6dHJ1ZSwiaWF0IjoxNTE2MjM5MDIyLCJhdWQiOlsieHVjIiwicGhvbmUiXSwiZXhwIjoxOTY3NjY0Njk4LCJhenAiOiJ4dWMiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJqYm9uZCIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9teXJlYWxtIn0.
        |LisXJFGLFsxXC8-eZUKPn4dKeU57tnLtgWN_UwmM1hSifkXRCWRI2DbM3vAHMroQG1Y6fkJbtUziIbOC056ZuIlPJGwMYzVVqzOLZG7p0L-lnju7zqalTMsbR-pEuiVs0Kfv_Ws80bzqyD1TValCsSdOKvhL9EOXYS77IS7lFxrHyYqYnoVR6TibSFNQnUo4gPUlM7w_Zs-CW0-peXt0aHlLLasmIMGQFeb49Vijd_ysnrkupIQH3z_vpip20s19TqIMnOpV0s4iSF-0zOzDLN1dv-hjRDaqvKykQzSr3RKq2sv3zfd_Lq6DX-VG_ZvT7lZXQ1uSKevM1Tj2po9Iwg""".stripMargin
      .replaceAll("[ \t\n\r]*", "")

  val tokenWithOneAudience =
    "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkxJVng0QjkzWmI2d2hHc3lKR21PZ0V5cHhZSXZBWjJqRWdTZXc0OUZCek0ifQ.eyJleHAiOjE5MDY3NDg1NjcsImlhdCI6MTYwNjc0ODI2NywiYXV0aF90aW1lIjoxNjA2NzQ2ODIyLCJqdGkiOiJmOTAxMWMwZi04NTcwLTQ2MTEtOTJjYi1jYTU5MjU5YmQ5MmYiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbXlyZWFsbSIsImF1ZCI6InBob25lIiwic3ViIjoiNzgzNjNlZGItMzVmYi00MGNkLThhOTEtZDc3OWM2YjU4ZjEyIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoieHVjIiwibm9uY2UiOiJub25jZSIsInNlc3Npb25fc3RhdGUiOiI2NjAxNGNhZi03YmZkLTQyNDEtOTQ5MC0yMTI3MWZjNTM5ZjMiLCJhY3IiOiIwIiwic2NvcGUiOiJvcGVuaWQgeGl2byBwcm9maWxlIiwibmFtZSI6IkphbWVzIEJvbmQiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJqYm9uZCIsImdpdmVuX25hbWUiOiJKYW1lcyIsImZhbWlseV9uYW1lIjoiQm9uZCJ9.VFWGd9fWuLAxFsldO0sbfEfpCQxa3sr8rymyEVYXLSoX4cAKjFHKupoHvlA6ruod9TrjrwcpCb3ySYaP8GQ6GRjUMN8dVs8VvlmZPs_BvxJAoQu7uEQb16W4JBbphbj7aEu10J7im6h0MjtShuCFMTzjOEcRHX13lp6vN28xVKw"

  val key: String = """MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnzyis1ZjfNB0bBgKFMSv
                |vkTtwlvBsaJq7S5wA+kzeVOVpVWwkWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHc
                |aT92whREFpLv9cj5lTeJSibyr/Mrm/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIy
                |tvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0
                |e+lf4s4OxQawWD79J9/5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWb
                |V6L11BWkpzGXSW4Hv43qa+GSYOD2QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9
                |MwIDAQAB""".stripMargin.replaceAll("[ \t\n\r]*", "")

  val key2 =
    "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCXxMdJnj+fnFa7ZSZdFSKGSuR5Q5p+cqhCbDbx423EFp+WXXWi9DonV+DHsLVvzpL7yEZIlmZMJShXxdesui1sQsJChNrC2m+Ru33Om6FIrYJKRd7SLFS2M/ytL+b3R75oHN6Mlk1q7zBOdQ66Vqww3lswVyPt1BXMuihQlZCOPQIDAQAB"

  val keyResponse: String = s"""{
                         | "realm":"myrealm",
                         | "public_key": "$key",
                         | "token-service":"http://localhost:8080/auth/realms/myrealm/protocol/openid-connect",
                         | "account-service":"http://localhost:8080/auth/realms/myrealm/account",
                         | "tokens-not-before":0
                         |}""".stripMargin

  val keyResponse2: String = s"""{
                         | "realm":"myrealm",
                         | "public_key": "$key2",
                         | "token-service":"http://localhost:8080/auth/realms/myrealm/protocol/openid-connect",
                         | "account-service":"http://localhost:8080/auth/realms/myrealm/account",
                         | "tokens-not-before":0
                         |}""".stripMargin

  val user: XucUser = getXucUser("jbond", "passwd")

  val xivoUser: XivoUser =
    XivoUser(
      1,
      None,
      None,
      "James",
      Some("Bond"),
      Some("jbond"),
      Some("passwd"),
      None,
      None
    )

  val xivoAuthAcls: List[String] = List("alias.ctiuser")
}

class OidcSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar
    with ScalaFutures
    with XucUserHelper {

  "Oidc" should {

    import OidcSampleToken._

    class OidcTestService() extends Oidc {
      implicit val system: ActorSystem = ActorSystem()
      val ws: WSClient                 = mock[WSClient]

      val mockRequest: WSRequest = mock[WSRequest]
      val mockResponse: WSResponse = mock[WSResponse]
      val config: XucBaseConfig = mock[XucBaseConfig]
      val auth: config.Authentication.type = mock[config.Authentication.type]
      val repo: ConfigRepository = mock[ConfigRepository]
      val xivoAuthentication: TestProbe = TestProbe()

      implicit val xivoAuthRef: ActorRef = xivoAuthentication.ref

      when(repo.getCtiUser(user.username)).thenReturn(Some(xivoUser))

      when(auth.expires).thenReturn(5000L)
      when(config.oidcEnable).thenReturn(true)
      when(config.oidcServerUrl).thenReturn(Some(testUrl))
      when(config.oidcClientId).thenReturn(Some(clientId))
      when(config.oidcAudience).thenReturn(List(audience))
      when(config.Authentication).thenReturn(auth)
      when(config.oidcUsernameField).thenReturn(None)
      when(config.oidcAdditionalTrustedServersUrl).thenReturn(List())
      when(config.oidcAdditionalTrustedClientIDs).thenReturn(List())

      when(ws.url(any[String])).thenReturn(mockRequest)
      when(mockRequest.withQueryStringParameters(any[(String, String)]))
        .thenReturn(mockRequest)

    }

    "allow Xuc authentication for a valid token" in new OidcTestService() {

      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse))
      val now: Long = new DateTime().getMillis / 1000

      authenticate(Some(token), config, repo, ws, None)(
        xivoAuthentication.ref
      ) match {
        case Success(result) =>
          val (auth, user) = result
          auth shouldBe a[AuthenticationInformation]
          user shouldBe xivoUser
          auth.login shouldBe "jbond"
          auth.expiresAt should be <= now + config.Authentication.expires
          auth.issuedAt should be <= now
          auth.userType shouldBe "cti"
          auth.acls shouldBe xivoAuthAcls
          auth.softwareType shouldBe None
        case _ => fail()
      }
    }

    "allow Xuc authentication for a valid token in an alternate trusted issuer domain" in new OidcTestService() {
      when(config.oidcServerUrl).thenReturn(
        Some("https://some.unused.url/randomRealm")
      )
      when(config.oidcAdditionalTrustedServersUrl).thenReturn(
        List(
          "https://www.hello.world/someRealm",
          "http://localhost:8080/myrealm",
          "https://www.foo.bar/someOtherRealm"
        )
      )
      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse))

      val now: Long = new DateTime().getMillis / 1000
      authenticate(Some(token), config, repo, ws, None) match {
        case Success(result) =>
          val (auth: AuthenticationInformation, user: XivoUser) = result
          auth shouldBe a[AuthenticationInformation]
          user shouldBe xivoUser
          auth.login shouldBe "jbond"
          auth.expiresAt should be <= now + config.Authentication.expires
          auth.issuedAt should be <= now
          auth.userType shouldBe "cti"
          auth.acls shouldBe xivoAuthAcls
          auth.softwareType shouldBe None
        case _ => fail()
      }
    }

    "allow Xuc authentication for a valid token with one audience" in new OidcTestService() {

      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse2))

      val now: Long = new DateTime().getMillis / 1000
      authenticate(
        Some(tokenWithOneAudience),
        config,
        repo,
        ws,
        Some(SoftwareType.webBrowser)
      ) match {
        case Success(result) =>
          val (auth: AuthenticationInformation, user: XivoUser) = result
          auth shouldBe a[AuthenticationInformation]
          user shouldBe xivoUser
          auth.login shouldBe "jbond"
          auth.expiresAt should be <= now + config.Authentication.expires
          auth.issuedAt should be <= now
          auth.userType shouldBe "cti"
          auth.acls shouldBe xivoAuthAcls
          auth.softwareType shouldBe Some(SoftwareType.webBrowser)

        case Failure(e) => fail()
      }
    }

    "allow Xuc authentication for a valid token with an audience among multiple" in new OidcTestService() {
      when(config.oidcAudience).thenReturn(List("someAudience", "xuc", "phone"))
      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse2))

      val now: Long = new DateTime().getMillis / 1000
      authenticate(
        Some(tokenWithOneAudience),
        config,
        repo,
        ws,
        Some(SoftwareType.mobile)
      ) match {
        case Success(result) =>
          val (auth: AuthenticationInformation, user: XivoUser) = result
          auth shouldBe a[AuthenticationInformation]
          user shouldBe xivoUser
          auth.login shouldBe "jbond"
          auth.expiresAt should be <= now + config.Authentication.expires
          auth.issuedAt should be <= now
          auth.userType shouldBe "cti"
          auth.acls shouldBe xivoAuthAcls
          auth.softwareType shouldBe Some(SoftwareType.mobile)
        case Failure(e) => fail()
      }
    }

    "allow Xuc authentication for a valid token with a valid client id" in new OidcTestService() {
      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse2))

      val now: Long = new DateTime().getMillis / 1000
      authenticate(
        Some(tokenWithOneAudience),
        config,
        repo,
        ws,
        Some(SoftwareType.mobile)
      ) match {
        case Success(result) =>
          val (auth: AuthenticationInformation, user: XivoUser) = result
          auth shouldBe a[AuthenticationInformation]
          user shouldBe xivoUser
          auth.login shouldBe "jbond"
          auth.expiresAt should be <= now + config.Authentication.expires
          auth.issuedAt should be <= now
          auth.userType shouldBe "cti"
          auth.acls shouldBe xivoAuthAcls
          auth.softwareType shouldBe Some(SoftwareType.mobile)
        case Failure(e) => fail()
      }
    }

    "allow Xuc authentication for a valid token with a valid client id among multiples" in new OidcTestService() {
      when(config.oidcClientId).thenReturn(Some("wrongClientID"))
      when(config.oidcAdditionalTrustedClientIDs).thenReturn(
        List("wrongClientID2", "wrongClientID3", clientId)
      )

      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse2))

      val now: Long = new DateTime().getMillis / 1000
      authenticate(
        Some(tokenWithOneAudience),
        config,
        repo,
        ws,
        Some(SoftwareType.mobile)
      ) match {
        case Success(result) =>
          val (auth: AuthenticationInformation, user: XivoUser) = result
          auth shouldBe a[AuthenticationInformation]
          user shouldBe xivoUser
          auth.login shouldBe "jbond"
          auth.expiresAt should be <= now + config.Authentication.expires
          auth.issuedAt should be <= now
          auth.userType shouldBe "cti"
          auth.acls shouldBe xivoAuthAcls
          auth.softwareType shouldBe Some(SoftwareType.mobile)
        case Failure(e) => fail()
      }
    }

    "allow Xuc authentication for a valid token with a valid audience as client ID fallback" in new OidcTestService() {
      when(config.oidcClientId).thenReturn(Some(clientId))

      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse2))

      val now: Long = new DateTime().getMillis / 1000
      authenticate(
        Some(tokenWithOneAudience),
        config,
        repo,
        ws,
        Some(SoftwareType.mobile)
      ) match {
        case Success(result) =>
          val (auth: AuthenticationInformation, user: XivoUser) = result
          auth shouldBe a[AuthenticationInformation]
          user shouldBe xivoUser
          auth.login shouldBe "jbond"
          auth.expiresAt should be <= now + config.Authentication.expires
          auth.issuedAt should be <= now
          auth.userType shouldBe "cti"
          auth.acls shouldBe xivoAuthAcls
          auth.softwareType shouldBe Some(SoftwareType.mobile)
        case Failure(e) => fail()
      }
    }

    "return a failure when oidc is not configured" in new OidcTestService() {
      when(config.oidcEnable).thenReturn(false)

      authenticate(Some(token), config, repo, ws, None) match {
        case Failure(result) => result shouldBe a[AuthenticationException]
        case _               => fail()
      }
    }

    "return a failure when access token is missing" in new OidcTestService() {

      authenticate(None, config, repo, ws, None) match {
        case Failure(result) => result shouldBe a[AuthenticationException]
        case _               => fail()
      }
    }

    "return a failure when the authorization server url is not configured" in new OidcTestService() {
      when(config.oidcServerUrl).thenReturn(None)

      authenticate(Some(token), config, repo, ws, None) match {
        case Failure(result) => result shouldBe a[AuthenticationException]
        case _               => fail()
      }
    }

    "return a failure when token cannot be deciphered" in new OidcTestService() {
      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(
        Json.parse("""{"public_key": "fake"}""")
      )

      authenticate(Some(token), config, repo, ws, None) match {
        case Failure(result) => result shouldBe a[AuthenticationException]
        case _               => fail()
      }
    }

    "return a failure when token is invalid due to missing required audience/clientId" in new OidcTestService() {
      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse))

      when(config.oidcClientId).thenReturn(Some("fakeAudience"))

      authenticate(Some(token), config, repo, ws, None) match {
        case Failure(result) => result shouldBe a[AuthenticationException]
        case _               => fail()
      }
    }

    "return a failure when token is invalid due to wrong issuer" in new OidcTestService() {
      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse))

      when(config.oidcServerUrl).thenReturn(Some("fakeIss"))

      authenticate(Some(token), config, repo, ws, None) match {
        case Failure(result) => result shouldBe a[AuthenticationException]
        case _               => fail()
      }
    }

    "return a failure when token is invalid due to XiVO user not found" in new OidcTestService() {
      when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
      when(mockResponse.status).thenReturn(200)
      when(mockResponse.json).thenReturn(Json.parse(keyResponse))

      when(repo.getCtiUser(user.username)).thenReturn(None)

      authenticate(Some(token), config, repo, ws, None) match {
        case Failure(result) => result shouldBe a[AuthenticationException]
        case _               => fail()
      }
    }
  }
}
