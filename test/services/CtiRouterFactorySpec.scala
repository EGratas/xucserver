package services

import org.apache.pekko.actor.{Actor, PoisonPill, Props}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.XucUser
import org.scalatestplus.mockito.MockitoSugar
import xuctest.XucUserHelper

class CtiRouterFactorySpec
    extends TestKitSpec("XiVO-CTI-system")
    with MockitoSugar
    with XucUserHelper {

  class DummyActor extends Actor {
    def receive: PartialFunction[Any,Unit] = { case _ =>
      throw new IllegalArgumentException
    }
  }

  class CrashActor extends Actor {
    self ! "crash"

    def receive: PartialFunction[Any,Unit] = { case _ =>
      self ! PoisonPill
    }

  }

  class Helper {
    val createCtiRouter: CtiRouter.Factory = new CtiRouter.Factory {
      def apply(u: XucUser) = new DummyActor()
    }
    val createCtiRouterCrashed: CtiRouter.Factory = new CtiRouter.Factory {
      def apply(u: XucUser) = new CrashActor()
    }

    val ctiRouterFactory: TestActorRef[CtiRouterFactory] = TestActorRef[CtiRouterFactory](
      Props(new CtiRouterFactory(createCtiRouter))
    )
    val ctiRouterCrashedFactory: TestActorRef[CtiRouterFactory] = TestActorRef[CtiRouterFactory](
      Props(new CtiRouterFactory(createCtiRouterCrashed))
    )

  }

  "CtiRouterFactory" should {

    "create router if there's no actor for given username" in new Helper {
      val user: XucUser = getXucUser("hawkeye", "45,.dfee", Some("2002"))

      ctiRouterFactory ! GetRouter(user)

      expectMsgType[Router]
    }

    "return existing actor for given username" in new Helper {
      val user: XucUser = getXucUser("hawkeye", "45,.dfee", Some("2002"))
      ctiRouterFactory ! GetRouter(user)

      val router: Router = expectMsgType[Router]

      ctiRouterFactory ! GetRouter(user)

      expectMsg(router)

    }

    "remove router from the map when terminated so expect a new router on next request" in new Helper {

      val user: XucUser = getXucUser("terminated", "45,.dfee", Some("2002"))
      val user2: XucUser = getXucUser("terminated2", "45,.dfee", Some("2002"))

      ctiRouterCrashedFactory ! GetRouter(user)

      val router: Router = expectMsgType[Router]

      val tProbe: TestProbe = TestProbe()
      tProbe.watch(router.ref)

      tProbe.expectTerminated(router.ref)

      ctiRouterCrashedFactory.underlyingActor.routers should be(Symbol("empty"))
    }
  }
}
