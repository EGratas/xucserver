package services.chat

import java.time.ZoneOffset
import models.XivoUser
import org.mockito.Mockito.*
import org.scalatest.compatible.Assertion
import org.scalatest.concurrent.ScalaFutures.whenReady
import org.scalatestplus.mockito.MockitoSugar
import services.config.ConfigRepository
import services.chat.model.{ChatUser, Message}
import xivo.models.*

import scala.concurrent.Future
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AsyncWordSpec

import java.time.OffsetDateTime

class ChatUtilSpec extends AsyncWordSpec with Matchers with MockitoSugar {
  import xivo.models.LineHelper.makeLine

  trait Helper {
    val assertion: Future[Assertion]
    val configRepo: ConfigRepository = mock[ConfigRepository]
    val util                         = new ChatUtil(configRepo)

    val guidFrom = "k7c6h9ur7ty4pxxe6sebbmruyy"
    val jbond: MattermostGuid =
      MattermostGuid(guidFrom, "jbond", Some("James Bond"))

    val guidTo = "ctmrsg87wbbfxb7cjegpi7ad9a"
    val aliceGuid: MattermostGuid =
      MattermostGuid(guidTo, "alice", Some("Alice Sample"))
    val mattermostUserTo: MattermostUser = MattermostUser(
      "1",
      1,
      1,
      1,
      "user_2@xivopbx",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      MattermostTimeZone("", "", "")
    )

    val timezone: (Long, OffsetDateTime) = (
      1582203515356L,
      new java.util.Date(1582203515356L).toInstant.atOffset(ZoneOffset.UTC)
    )

    val jbondXivoUser: XivoUser =
      XivoUser(
        42,
        None,
        None,
        "James",
        Some("Bond"),
        Some("jbond"),
        None,
        None,
        None
      )
    val aliceXivoUser: XivoUser =
      XivoUser(
        42,
        None,
        None,
        "Alice",
        Some("Sample"),
        Some("alice"),
        None,
        None,
        None
      )

    val jdoe: ChatUser = ChatUser(
      jbond.username,
      Some("1000"),
      jbond.displayName,
      Some(jbond.guid)
    )
    val alice: ChatUser = ChatUser(
      aliceGuid.username,
      Some("1001"),
      aliceGuid.displayName,
      Some(aliceGuid.guid)
    )

    when(configRepo.getLineForUser(jdoe.username))
      .thenReturn(
        Some(
          makeLine(
            1,
            "default",
            "SIP",
            "jdoe",
            None,
            None,
            "ip",
            webRTC = false,
            Some("1000")
          )
        )
      )
    when(configRepo.getLineForUser(alice.username))
      .thenReturn(
        Some(
          makeLine(
            1,
            "default",
            "SIP",
            "alice",
            None,
            None,
            "ip",
            webRTC = false,
            Some("1001")
          )
        )
      )
  }

  "FlashTextUtil" should {
    "convert and order mattermost message to message" in new Helper {
      val posts: List[MattermostDirectMessage] = List(
        MattermostDirectMessage(
          "post-id-1",
          "channelId",
          "message 1",
          timezone._1,
          guidFrom
        ),
        MattermostDirectMessage(
          "post-id-2",
          "channelId",
          "message 2",
          timezone._1,
          guidTo
        )
      )

      val expected: List[Message] = List(
        Message(jdoe, alice, "message 1", timezone._2, 0L),
        Message(alice, jdoe, "message 2", timezone._2, 0L)
      )

      val assertion: Future[Assertion] = Future(
        util.mattermostOrderRecipients(jbond, aliceGuid)(posts)
      ).map(res => {
        res shouldBe expected
      })
    }.assertion

    "convert and order no messages to empty list" in new Helper {
      val posts: List[Nothing] = List()

      val expected: List[Nothing] = List()

      val assertion: Future[Assertion] = Future(
        util.mattermostOrderRecipients(jbond, aliceGuid)(posts)
      ).map(res => {
        res shouldBe expected
      })
    }.assertion

    "convert and order mattermost unread messages to message" in new Helper {
      val mattermostUnreadMessage: MattermostDirectMessage = MattermostDirectMessage(
        "post-id-2",
        "channelId",
        "message 2",
        timezone._1,
        guidTo
      )
      val unread: MattermostUnreadNotification = MattermostUnreadNotification(
        DirectionIsToUser,
        jbond.username,
        2L,
        mattermostUnreadMessage,
        timezone._2,
        0
      )

      val expected: Message = Message(
        alice.copy(guid = None),
        jdoe.copy(guid = None),
        "message 2",
        timezone._2,
        0L
      )

      when(configRepo.getCtiUser(unread.userIdTo))
        .thenReturn(Some(aliceXivoUser))
      when(configRepo.getCtiUser("jbond"))
        .thenReturn(Some(jbondXivoUser))

      val assertion: Future[Assertion] = util
        .mattermostOrderRecipients(unread)
        .map(res => {
          res shouldBe expected
        })

    }.assertion

    "return failed if such user is not in config repository" in new Helper {
      val mattermostUnreadMessage: MattermostDirectMessage = MattermostDirectMessage(
        "post-id-2",
        "channelId",
        "message 2",
        timezone._1,
        guidTo
      )
      val unread: MattermostUnreadNotification = MattermostUnreadNotification(
        DirectionIsToUser,
        jbond.username,
        2L,
        mattermostUnreadMessage,
        timezone._2,
        0
      )

      when(configRepo.getCtiUser(unread.userIdTo))
        .thenReturn(None)
      when(configRepo.getCtiUser("jbond"))
        .thenReturn(Some(jbondXivoUser))

      val assertion: Future[Assertion] =
        whenReady(util.mattermostOrderRecipients(unread).failed)(res =>
          res shouldBe a[NoSuchElementException]
        )
    }.assertion
  }
}
