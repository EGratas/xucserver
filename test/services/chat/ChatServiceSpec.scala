package services.chat

import org.apache.pekko.actor.{ActorRef, Props}
import org.apache.pekko.testkit.{TestActorRef, TestKit, TestProbe}
import pekkotest.TestKitSpec
import models.XivoUser
import org.mockito.Mockito.{never, timeout, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import services.chat.ChatService.Username
import services.config.ConfigRepository
import services.chat.model._
import services.VideoChat.{ChatStatus, VideoChatStates}
import xivo.models._
import xivo.network.ChatBackendWS
import xivo.network.ChatLink.{RetrieveDirectChannels, StopChatLink}
import xivo.xuc.ChatConfig

import java.time.OffsetDateTime
import scala.concurrent.{Future, Promise}
import scala.concurrent.duration._

class ChatServiceSpec extends TestKitSpec("ChatServiceSpec") with MockitoSugar {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class Helper {
    val now: OffsetDateTime           = OffsetDateTime.now()
    val config: ChatConfig        = mock[ChatConfig]
    val chatBackendWS: ChatBackendWS = mock[ChatBackendWS]
    val chatLinkMock: (ChatUserState, String) => ActorRef  = mock[(ChatUserState, String) => ActorRef]
    val configRepo: ConfigRepository    = mock[ConfigRepository]
    val flastTextUtil: ChatUtil = mock[ChatUtil]

    val chatLinkFrom: TestProbe  = TestProbe()
    val ctiRouterFrom: TestProbe = TestProbe()
    val ctiRouterTo: TestProbe   = TestProbe()

    val userIdFrom    = 56
    val guidFrom      = "k7c6h9ur7ty4pxxe6sebbmruyy"
    val usernameFrom  = "jdoe"
    val firstNameFrom = "John"
    val lastNameFrom  = "Doe"

    val usernameTo  = "asample"
    val firstNameTo = "Alice"
    val lastNameTo  = "Sample"
    val userIdTo    = 43
    val guidTo      = "x9bg8xjcwbfqfqnw1zoun494zc"

    val connectionToken = "x11sfpzoap86bfu78wtqjsjyky"

    val xivoUserFrom: XivoUser =
      XivoUser(
        userIdFrom,
        None,
        None,
        "John",
        Some("Doe"),
        Some("john"),
        None,
        None,
        None
      )
    val xivoUserTo: XivoUser =
      XivoUser(
        userIdTo,
        None,
        None,
        "Alice",
        Some("Sample"),
        Some("alice"),
        None,
        None,
        None
      )

    def createFlashTextUserState(
        id: Long,
        username: String,
        firstName: String,
        lastName: String,
        guid: Option[String],
        flashTextStatus: ChatStatus,
        ctiRouterRef: Option[ActorRef],
        chatLink: Option[ActorRef]
    ): ChatUserState =
      ChatUserState(
        ChatUser(
          username,
          Some("1000"),
          Some(s"$firstName $lastName"),
          guid
        ),
        flashTextStatus,
        ctiRouterRef,
        chatLink,
        XivoUser(
          id,
          None,
          None,
          firstName,
          Some(lastName),
          Some(username),
          None,
          None,
          None
        )
      )

    def actor(
        mockUsers: Map[
          Username,
          VideoChatStates[ChatUserState, ChatStatus, ChatUser]
        ] = Map.empty
    ): (TestActorRef[ChatService], ChatService) = {

      val a = TestActorRef[ChatService](
        Props(
          new ChatService(config, chatBackendWS, configRepo, flastTextUtil) {
            override def getDate: OffsetDateTime = now

            override def createChatLinkActor(
                flashTextUserState: ChatUserState,
                token: String,
                guid: String
            ): ActorRef = chatLinkMock(flashTextUserState, token)

            users = mockUsers
          }
        )
      )
      (a, a.underlyingActor)
    }
  }

  "Chat Service" should {
    "connect user" in new Helper() {
      val connectUserFrom: ConnectFlashTextUser = ConnectFlashTextUser(xivoUserFrom)

      when(config.chatEnable).thenReturn(true)

      var (ref, a) = actor()

      when(chatBackendWS.getCtiUser(xivoUserFrom.id))
        .thenReturn(Future.successful(List()))

      ctiRouterFrom.send(ref, connectUserFrom)

      verify(chatBackendWS, timeout(500)).getCtiUser(xivoUserFrom.id)
      a.users("john").xivoUser shouldEqual xivoUserFrom
    }

    "send back ACK if message is persisted in mattermost" in new Helper() {
      val connectedUsers: Map[Username, ChatUserState] = Map(
        "jdoe" -> createFlashTextUserState(
          89,
          "jdoe",
          "John",
          "Doe",
          Some("k7c6h9ur7ty4pxxe6sebbmruyybis"),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          None
        )
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor(connectedUsers)

      val msg: MattermostDirectMessageAck = new MattermostDirectMessageAck(
        "1",
        "abcdfg",
        "hello",
        1L,
        "1",
        12L,
        "jdoe",
        "asample"
      ) {
        override def offsetDateTime: OffsetDateTime =
          OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
      }
      ref ! msg

      ctiRouterFrom.expectMsg(
        RequestAck(
          12L,
          true,
          OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
        )
      )
    }

    "forward when message is persisted in mattermost (user online)" in new Helper() {
      val connectedUsers: Map[Username, ChatUserState] = Map(
        "jdoe" -> createFlashTextUserState(
          89,
          "jdoe",
          "John",
          "Doe",
          Some("wet434564450xxe6dsgrehrehh"),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          None
        ),
        "asample" -> createFlashTextUserState(
          90,
          "asample",
          "Alice",
          "Sample",
          Some("k7c6h9ur7ty4pxxe6sebbmruyybis"),
          ChatStatus.Available,
          Some(ctiRouterTo.ref),
          None
        )
      )

      when(config.chatEnable).thenReturn(true)

      var (ref, a) = actor(connectedUsers)

      val msg: MattermostDirectMessageAck = new MattermostDirectMessageAck(
        "1",
        "abcdfg",
        "hello",
        1L,
        "1",
        12L,
        "jdoe",
        "asample"
      ) {
        override def offsetDateTime: OffsetDateTime =
          OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
      }
      ref ! msg

      ctiRouterFrom.expectMsg(
        RequestAck(
          12L,
          date = OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
        )
      )
      ctiRouterTo.expectMsg(
        Message(
          a.users("jdoe").user,
          a.users("asample").user,
          "hello",
          msg.offsetDateTime,
          12L
        )
      )
    }

    "forward when message is persisted in mattermost (user unavailable)" in new Helper() {
      val connectedUsers: Map[Username, ChatUserState] = Map(
        "jdoe" -> createFlashTextUserState(
          89,
          "jdoe",
          "John",
          "Doe",
          Some("wet434564450xxe6dsgrehrehh"),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          None
        ),
        "asample" -> createFlashTextUserState(
          90,
          "asample",
          "Alice",
          "Sample",
          Some("k7c6h9ur7ty4pxxe6sebbmruyybis"),
          ChatStatus.Unavailable,
          Some(ctiRouterTo.ref),
          None
        )
      )

      when(config.chatEnable).thenReturn(true)

      var (ref, a) = actor(connectedUsers)

      val msg: MattermostDirectMessageAck = new MattermostDirectMessageAck(
        "1",
        "abcdfg",
        "hello",
        1L,
        "1",
        12L,
        "jdoe",
        "asample"
      ) {
        override def offsetDateTime: OffsetDateTime =
          OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
      }
      ref ! msg

      ctiRouterFrom.expectMsg(
        RequestAck(
          12L,
          true,
          OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
        )
      )
      ctiRouterTo.expectMsg(
        Message(
          a.users("jdoe").user,
          a.users("asample").user,
          "hello",
          msg.offsetDateTime,
          12L
        )
      )
    }

    "forward when message is persisted in mattermost (user offline)" in new Helper() {
      val connectedUsers: Map[Username, ChatUserState] = Map(
        "jdoe" -> createFlashTextUserState(
          89,
          "jdoe",
          "John",
          "Doe",
          Some("wet434564450xxe6dsgrehrehh"),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          None
        )
      )

      when(config.chatEnable).thenReturn(true)

      var (ref, a) = actor(connectedUsers)

      val msg: MattermostDirectMessageAck = new MattermostDirectMessageAck(
        "1",
        "abcdfg",
        "hello",
        1L,
        "1",
        12L,
        "jdoe",
        "asample"
      ) {
        override def offsetDateTime: OffsetDateTime =
          OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
      }
      ref ! msg

      ctiRouterFrom.expectMsg(
        RequestAck(
          12L,
          true,
          OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
        )
      )
      ctiRouterTo.expectNoMessage(250.milliseconds)
    }

    "Get mattermost user if he's existing and retrieve connection token" in new Helper {
      val userId    = 56
      val guid      = "k7c6h9ur7ty4pxxe6sebbmruyy"
      val username  = "jdoe"
      val firstName = "John"
      val lastName  = "Doe"
      val xivoUser: XivoUser =
        XivoUser(
          userId,
          None,
          None,
          firstName,
          Some(lastName),
          Some(username),
          None,
          None,
          None
        )
      val connectMsg: ConnectFlashTextUser = ConnectFlashTextUser(xivoUser)
      val mattermostUser: MattermostUser = MattermostUser(
        guid,
        0,
        0,
        0,
        "user_56",
        "",
        "user_56@xivopbx",
        "",
        firstName,
        lastName,
        "",
        "",
        "",
        MattermostTimeZone("", "", "")
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor()

      when(chatBackendWS.getCtiUser(userId))
        .thenReturn(Future.successful(List(mattermostUser)))
      when(chatBackendWS.getUserToken(userId))
        .thenReturn(Future.successful(connectionToken))

      ref ! connectMsg

      verify(chatBackendWS, timeout(500)).getCtiUser(userId)
      verify(chatBackendWS, never()).createUser(userId, firstName, lastName)
      verify(chatBackendWS, timeout(500)).getUserToken(userId)
    }

    "Create mattermost user if he's not existing and retrieve connection token" in new Helper {
      val userId    = 56
      val guid      = "k7c6h9ur7ty4pxxe6sebbmruyy"
      val username  = "jdoe"
      val firstName = "John"
      val lastName  = "Doe"
      val xivoUser: XivoUser =
        XivoUser(
          userId,
          None,
          None,
          firstName,
          Some(lastName),
          Some(username),
          None,
          None,
          None
        )
      val connectMsg: ConnectFlashTextUser = ConnectFlashTextUser(xivoUser)
      val mattermostUser: MattermostUser = MattermostUser(
        guid,
        0,
        0,
        0,
        "user_56",
        "",
        "user_56@xivopbx",
        "",
        firstName,
        lastName,
        "",
        "",
        "",
        MattermostTimeZone("", "", "")
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor()

      when(chatBackendWS.getCtiUser(userId))
        .thenReturn(Future.successful(List()))
      when(chatBackendWS.createUser(userId, firstName, lastName))
        .thenReturn(Future.successful(mattermostUser))
      when(chatBackendWS.getUserToken(userId))
        .thenReturn(Future.successful(connectionToken))

      ref ! connectMsg

      verify(chatBackendWS, timeout(500)).getCtiUser(userId)
      verify(chatBackendWS, timeout(500)).createUser(
        userId,
        firstName,
        lastName
      )
      verify(chatBackendWS, timeout(500)).getUserToken(userId)
    }

    "create chatLink when connecting" in new Helper {
      val userId       = 56
      val guid         = "k7c6h9ur7ty4pxxe6sebbmruyy"
      val username     = "jdoe"
      val firstName    = "John"
      val lastName     = "Doe"
      val ctiRouterRef: ActorRef = TestProbe().ref
      val flashTextUserState: ChatUserState = createFlashTextUserState(
        userId,
        username,
        firstName,
        lastName,
        Some(guid),
        ChatStatus.Available,
        Some(ctiRouterRef),
        None
      )

      val connectedUsers: Map[Username, ChatUserState] = Map(
        "asample" ->
          createFlashTextUserState(
            89,
            "asample",
            "Alice",
            "Sample",
            Some("k7c6h9ur7ty4pxxe6sebbmruyybis"),
            ChatStatus.Available,
            Some(TestProbe().ref),
            None
          ),
        "jdoe" -> flashTextUserState
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor(connectedUsers)

      when(chatBackendWS.getUserToken(userId))
        .thenReturn(Future.successful(connectionToken))

      ref ! UpdateUserWithGuid(
        MattermostGuid(guid, username, Some("Alice Sample"))
      )

      verify(chatLinkMock, timeout(500))
        .apply(flashTextUserState, connectionToken)
    }

    "stop chatLink actor when disconnecting" in new Helper {
      val chatLink: TestProbe = TestProbe()
      val connectedUsers: Map[Username, ChatUserState] = Map(
        "asample" -> createFlashTextUserState(
          89,
          "asample",
          "Alice",
          "Sample",
          Some("k7c6h9ur7ty4pxxe6sebbmruyybis"),
          ChatStatus.Available,
          Some(TestProbe().ref),
          Some(chatLink.ref)
        )
      )

      when(config.chatEnable).thenReturn(true)

      var (ref, a) = actor(connectedUsers)

      val disconnectMsg: DisconnectFlashTextUser = DisconnectFlashTextUser(
        XivoUser(
          89,
          None,
          None,
          "Alice",
          Some("Sample"),
          Some("asample"),
          None,
          None,
          None
        )
      )

      ref ! disconnectMsg

      chatLink expectMsg StopChatLink

      a.users("asample").asInstanceOf[ChatUserState].chatLinkRef shouldBe None
      a.users("asample").user.guid shouldBe None
    }

    "stop orphan chatlink that remains when connecting and disconnecting right away before mattermost user is created" in new Helper {
      val userId    = 56
      val guid      = "k7c6h9ur7ty4pxxe6sebbmruyy"
      val username  = "jdoe"
      val firstName = "John"
      val lastName  = "Doe"
      val xivoUser: XivoUser =
        XivoUser(
          userId,
          None,
          None,
          firstName,
          Some(lastName),
          Some(username),
          None,
          None,
          None
        )
      val connectMsg: ConnectFlashTextUser = ConnectFlashTextUser(xivoUser)
      val disconnectMsg: DisconnectFlashTextUser = DisconnectFlashTextUser(xivoUser)
      val mattermostUser: MattermostUser = MattermostUser(
        guid,
        0,
        0,
        0,
        "user_56",
        "",
        "user_56@xivopbx",
        "",
        firstName,
        lastName,
        "",
        "",
        "",
        MattermostTimeZone("", "", "")
      )
      val promiseMattUser: Promise[MattermostUser] = Promise[MattermostUser]()
      val chatLink: TestProbe = TestProbe()

      when(config.chatEnable).thenReturn(true)
      when(chatBackendWS.getCtiUser(userId))
        .thenReturn(Future.successful(List()))
      when(chatBackendWS.createUser(userId, firstName, lastName))
        .thenReturn(promiseMattUser.future)
      when(chatBackendWS.getUserToken(userId))
        .thenReturn(Future.successful(connectionToken))

      val (ref, _) = actor()

      ref ! connectMsg
      ref ! disconnectMsg
      chatLink.send(ref, UpdateChatLinkRef(chatLink.ref, username))
      chatLink.expectMsg(RetrieveDirectChannels)
      ref ! connectMsg
      chatLink.expectMsg(StopChatLink)
    }

    "forward message to chatLink actor" in new Helper {
      val userFrom: MattermostGuid = MattermostGuid(guidFrom, usernameFrom, Some("John Doe"))
      val userTo: MattermostGuid = MattermostGuid(guidTo, usernameTo, Some("Alice Sample"))

      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          None,
          Some(chatLinkFrom.ref)
        ),
        usernameTo -> createFlashTextUserState(
          userIdTo,
          usernameTo,
          "Alice",
          "Sample",
          Some(guidTo),
          ChatStatus.Available,
          Some(ctiRouterTo.ref),
          None
        )
      )

      val msg: SendDirectMessage = SendDirectMessage(usernameFrom, usernameTo, "Hello Alice !", 1)
      val mattermostMsg: SendDirectMattermostMessage =
        SendDirectMattermostMessage(userFrom, userTo, "Hello Alice !", 1)

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor(usersConnected)

      ref ! msg

      chatLinkFrom.expectMsg(mattermostMsg)
    }

    "get direct message history" in new Helper {
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          None,
          Some(chatLinkFrom.ref)
        ),
        usernameTo -> createFlashTextUserState(
          userIdTo,
          usernameTo,
          "Alice",
          "Sample",
          Some(guidTo),
          ChatStatus.Available,
          Some(ctiRouterTo.ref),
          None
        )
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor(usersConnected)

      ref ! GetDirectMessageHistory(("jdoe", "asample"), 0)

      val expected: GetDirectMattermostMessagesHistory = GetDirectMattermostMessagesHistory(
        MattermostGuid(guidFrom, usernameFrom, Some("John Doe")),
        MattermostGuid(guidTo, usernameTo, Some("Alice Sample")),
        0
      )

      chatLinkFrom.expectMsg(expected)
    }

    "get direct message history error" in new Helper {
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          Some(chatLinkFrom.ref)
        )
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor(usersConnected)

      when(configRepo.getCtiUser("asample")).thenReturn(Some(xivoUserTo))
      when(chatBackendWS.getCtiUser(userIdTo))
        .thenReturn(Future.successful(List()))

      ref ! GetDirectMessageHistory(("jdoe", "asample"), 0)

      val expected: MessageHistory = MessageHistory(
        (
          ChatUser("jdoe", None, None, None),
          ChatUser("asample", None, None, None)
        ),
        List(),
        0
      )

      chatLinkFrom.expectNoMessage(250.milliseconds)
      ctiRouterFrom.expectMsg(expected)
    }

    "get direct message history if other party not logged in" in new Helper {
      val mattermostUser: MattermostUser = MattermostUser(
        guidTo,
        0,
        0,
        0,
        "user_43",
        "",
        "user_43@xivopbx",
        "",
        firstNameTo,
        lastNameTo,
        "",
        "",
        "",
        MattermostTimeZone("", "", "")
      )
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          Some(chatLinkFrom.ref)
        )
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor(usersConnected)

      val fromGuid: MattermostGuid = MattermostGuid(guidFrom, "jdoe", Some("John Doe"))
      val toGuid: MattermostGuid = MattermostGuid(guidTo, "alice", Some("Alice Sample"))

      when(flastTextUtil.getDisplayName(xivoUserTo))
        .thenReturn(Some("Alice Sample"))
      when(configRepo.getCtiUser("alice")).thenReturn(Some(xivoUserTo))
      when(chatBackendWS.getCtiUser(userIdTo))
        .thenReturn(Future.successful(List(mattermostUser)))

      ref ! GetDirectMessageHistory(("jdoe", "alice"), 0)

      chatLinkFrom.expectMsg(
        GetDirectMattermostMessagesHistory(fromGuid, toGuid, 0)
      )
    }

    "get direct message history error if other party not in Mattermost" in new Helper {
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          Some(chatLinkFrom.ref)
        )
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor(usersConnected)

      when(configRepo.getCtiUser("alice")).thenReturn(Some(xivoUserTo))
      when(chatBackendWS.getCtiUser(userIdTo))
        .thenReturn(Future.successful(List()))

      ref ! GetDirectMessageHistory(("jdoe", "alice"), 0)

      val expected: MessageHistory = MessageHistory(
        (
          ChatUser("jdoe", None, None, None),
          ChatUser("alice", None, None, None)
        ),
        List(),
        0
      )

      chatLinkFrom.expectNoMessage(250.milliseconds)
      chatLinkFrom.expectNoMessage(250.milliseconds)
      ctiRouterFrom.expectMsg(expected)
    }

    "request for retrieve direct channels " in new Helper {
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          None,
          None
        ),
        usernameTo -> createFlashTextUserState(
          userIdTo,
          usernameTo,
          "Alice",
          "Sample",
          Some(guidTo),
          ChatStatus.Available,
          None,
          None
        )
      )

      when(config.chatEnable).thenReturn(true)

      var (ref, a) = actor(usersConnected)

      ref ! UpdateChatLinkRef(chatLinkFrom.ref, "jdoe")

      chatLinkFrom.expectMsg(RetrieveDirectChannels)

      a.users(usernameFrom)
        .asInstanceOf[ChatUserState]
        .chatLinkRef
        .get shouldBe chatLinkFrom.ref
    }

    "persists to mattermost even if the user is logged out" in new Helper {
      val mattermostUser: MattermostUser = MattermostUser(
        guidTo,
        0,
        0,
        0,
        "user_43",
        "",
        "user_43@xivopbx",
        "",
        firstNameTo,
        lastNameTo,
        "",
        "",
        "",
        MattermostTimeZone("", "", "")
      )
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          Some(chatLinkFrom.ref)
        )
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor(usersConnected)

      val fromGuid: MattermostGuid = MattermostGuid(guidFrom, "jdoe", Some("John Doe"))
      val toGuid: MattermostGuid = MattermostGuid(guidTo, "alice", Some("Alice Sample"))

      when(flastTextUtil.getDisplayName(xivoUserTo))
        .thenReturn(Some("Alice Sample"))
      when(configRepo.getCtiUser("asample")).thenReturn(Some(xivoUserTo))
      when(chatBackendWS.getCtiUser(userIdTo))
        .thenReturn(Future.successful(List(mattermostUser)))

      ref ! SendDirectMessage(usernameFrom, usernameTo, "Hello!", 0)

      chatLinkFrom.expectMsg(
        SendDirectMattermostMessage(fromGuid, toGuid, "Hello!", 0)
      )
    }

    "not persists to mattermost if user not found in repository" in new Helper {
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          Some(chatLinkFrom.ref)
        )
      )

      when(config.chatEnable).thenReturn(true)

      val (ref, _) = actor(usersConnected)

      when(configRepo.getCtiUser("asample")).thenReturn(None)

      ref ! SendDirectMessage(usernameFrom, usernameTo, "Hello!", 0)

      chatLinkFrom.expectNoMessage(250.milliseconds)
      ctiRouterFrom.expectMsg(RequestNack(0))
    }

    "not persist to mattermost if chat backend is not available" in new Helper {
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          Some(chatLinkFrom.ref)
        )
      )

      when(config.chatEnable).thenReturn(false)

      val (ref, _) = actor(usersConnected)

      ref ! SendDirectMessage(usernameFrom, usernameTo, "Hello!", 0)

      chatLinkFrom.expectNoMessage(250.milliseconds)
      ctiRouterFrom.expectNoMessage(250.milliseconds)
    }

    "not retrieve history from mattermost if chat backend is not available" in new Helper {
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          Some(chatLinkFrom.ref)
        ),
        usernameTo -> createFlashTextUserState(
          userIdTo,
          usernameTo,
          "Alice",
          "Sample",
          Some(guidTo),
          ChatStatus.Available,
          Some(ctiRouterTo.ref),
          None
        )
      )

      when(config.chatEnable).thenReturn(false)

      val (ref, _) = actor(usersConnected)

      ref ! GetDirectMessageHistory(("jdoe", "asample"), 0)

      chatLinkFrom.expectNoMessage(250.milliseconds)
      ctiRouterFrom.expectNoMessage(250.milliseconds)
    }

    "mark channel messages as read" in new Helper {
      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          Some(chatLinkFrom.ref)
        )
      )
      val mattermostUser: MattermostUser = MattermostUser(
        guidTo,
        0,
        0,
        0,
        "user_56",
        "",
        "user_56@xivopbx",
        "",
        "Alice",
        "Sample",
        "",
        "",
        "",
        MattermostTimeZone("", "", "")
      )

      val fromGuid: MattermostGuid = MattermostGuid(guidFrom, "jdoe", Some("John Doe"))
      val toGuid: MattermostGuid = MattermostGuid(guidTo, "alice", Some("Alice Sample"))

      when(config.chatEnable)
        .thenReturn(true)
      when(configRepo.getCtiUser("asample"))
        .thenReturn(Some(xivoUserTo))
      when(flastTextUtil.getDisplayName(xivoUserTo))
        .thenReturn(Some("Alice Sample"))
      when(chatBackendWS.getCtiUser(xivoUserTo.id))
        .thenReturn(Future.successful(List(mattermostUser)))

      val (ref, _) = actor(usersConnected)

      ref ! MarkDirectchannelAsRead(("jdoe", "asample"), 0)

      chatLinkFrom.expectMsg(MarkMattermostMessagesAsRead(fromGuid, toGuid, 0))
    }

    "setting an active channel creates a user" in new Helper {
      val firstName = "Alice"
      val lastName  = "Sample"

      val usersConnected: Map[String, ChatUserState] = Map(
        usernameFrom -> createFlashTextUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          Some(guidFrom),
          ChatStatus.Available,
          Some(ctiRouterFrom.ref),
          Some(chatLinkFrom.ref)
        )
      )
      val mattermostUser: MattermostUser = MattermostUser(
        guidTo,
        0,
        0,
        0,
        "user_43",
        "",
        "user_43@xivopbx",
        "",
        "Alice",
        "Sample",
        "",
        "",
        "",
        MattermostTimeZone("", "", "")
      )

      val fromGuid: MattermostGuid = MattermostGuid(guidFrom, "jdoe", Some("John Doe"))
      val toGuid: MattermostGuid = MattermostGuid(guidTo, "alice", Some("Alice Sample"))

      when(config.chatEnable)
        .thenReturn(true)
      when(configRepo.getCtiUser("asample"))
        .thenReturn(Some(xivoUserTo))
      when(flastTextUtil.getDisplayName(xivoUserTo))
        .thenReturn(Some("Alice Sample"))
      when(chatBackendWS.getCtiUser(xivoUserTo.id))
        .thenReturn(Future.successful(List()))
      when(chatBackendWS.createUser(userIdTo, firstName, lastName))
        .thenReturn(Future.successful(mattermostUser))

      val (ref, _) = actor(usersConnected)

      ref ! MarkDirectchannelAsRead(("jdoe", "asample"), 0)

      chatLinkFrom.expectMsg(MarkMattermostMessagesAsRead(fromGuid, toGuid, 0))
    }
  }
}
