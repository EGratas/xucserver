package services.calltracking

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.Subscribe
import org.apache.pekko.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import org.joda.time.DateTime
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import services.{XucAmiBus, XucEventBus}
import services.calltracking.AsteriskGraphTracker.PathsFromChannel
import services.calltracking.ConferenceTracker._
import services.calltracking.SingleDeviceTracker._
import services.calltracking.graph.NodeBridge.BridgeCreator
import services.calltracking.graph.{AsteriskObjectHelper, NodeLocalBridge}
import xivo.events.PhoneEvent
import xivo.models.XivoFeature
import xivo.websocket._
import xivo.xuc.DeviceTrackerConfig
import xivo.xucami.models._
import xivo.models.Line

import scala.reflect.ClassTag

class CustomDeviceTrackerSpec
  extends TestKit(ActorSystem("CustomDeviceTracker"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AsteriskObjectHelper {
  import xivo.models.LineHelper.makeLine

  private val topic: String =
    ConferenceTracker.conferenceParticipantEventTopic("my-conf", "10.181.0.2")

  class DeviceActorWrapper(val feature: XivoFeature, stopRecording: Boolean) {
    val channelTracker = TestProbe()
    val graphTracker = TestProbe()
    val bus = mock[XucEventBus]
    val configDispatcher = TestProbe()
    val amiBus = mock[XucAmiBus]
    val parent = TestProbe()

    val deviceTrackerConfig = new DeviceTrackerConfig {
      def stopRecordingUponExternalXfer: Boolean = stopRecording

      def enableRecordingRules: Boolean = true
    }

    val factory = new DeviceActorFactoryImpl(
      channelTracker.ref,
      graphTracker.ref,
      bus,
      amiBus,
      deviceTrackerConfig,
      configDispatcher.ref,
      mediatorHelper
    )

    val device = TestActorRef[SingleDeviceTracker](
      factory.props(feature),
      parent.ref,
      "MySDTActor"
    )
  }

  def deviceWrapper(feature: XivoFeature, stopRecording: Boolean = true) =
    new DeviceActorWrapper(feature, stopRecording)

  implicit val mediator: TestProbe = TestProbe()
  val mediatorHelper = new MediatorHelperSpec

  def expectOnMediator[T: ClassTag](implicit mediator: TestProbe) = {

    var msgOpt: Option[T] = None
    mediator.fishForMessage() {
      case msg: T =>
        msgOpt = Some(msg)
        true
      case _ => false
    }

    msgOpt
  }

  val defaultLine: Line = makeLine(
    1,
    "default",
    "custom",
    "Local/01230041302@default/n",
    None,
    None,
    "123.123.123.1",
    number = Some("1010")
  )

  "CustomDeviceTracker" should {
    import BaseTracker._

    def makePath(lcname: String, realChannel: String): PathsFromChannel =
      PathsFromChannel(
        NodeLocalChannel(lcname + ";2"),
        Set(
          List(
            NodeLocalBridge(
              NodeLocalChannel(lcname + ";1"),
              NodeLocalChannel(lcname + ";2")
            ),
            NodeLocalChannel(lcname + ";1"),
            NodeBridge("b2", Some(new BridgeCreator("bc2"))),
            NodeChannel("SIP/abcd-0001")
          ),
          List(
            NodeBridge("b1", Some(new BridgeCreator("bc1"))),
            NodeChannel(realChannel)
          )
        )
      )

    "ignore local channel" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "Local/01230041302@default-00001;1"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      wrapper.device ! channel

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))

    }

    "watch real channel based on path received" in {
      val wrapper = deviceWrapper(defaultLine)

      wrapper.channelTracker.expectMsg(
        WatchChannelStartingWith(defaultLine.interface)
      )
      wrapper.graphTracker.expectMsg(
        WatchChannelStartingWith(defaultLine.interface)
      )
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(defaultLine.interface, wrapper.device)
      )

      val lcname      = "Local/01230041302@default-00001"
      val realChannel = "SIP/trunk-something-0001"
      val path        = makePath(lcname, realChannel)

      wrapper.device ! path
      wrapper.channelTracker.expectMsg(WatchChannelStartingWith(realChannel))
      wrapper.graphTracker.expectMsg(WatchChannelStartingWith(realChannel))
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(realChannel, wrapper.device)
      )
    }

    "user real channel info to monitor calls" in {
      val wrapper = deviceWrapper(defaultLine)

      val lcname      = "Local/01230041302@default-00001"
      val realChannel = "SIP/trunk-something-0001"
      val path        = makePath(lcname, realChannel)

      val channel = Channel(
        "123456789.123",
        realChannel,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! channel
      wrapper.device ! GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(DeviceCall(realChannel, Some(channel), Set.empty, Map.empty))
        )
      )
    }

    "unwatch real channel when channel is hangup" in {
      val wrapper = deviceWrapper(defaultLine)

      wrapper.channelTracker.expectMsg(
        WatchChannelStartingWith(defaultLine.interface)
      )
      wrapper.graphTracker.expectMsg(
        WatchChannelStartingWith(defaultLine.interface)
      )
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(defaultLine.interface, wrapper.device)
      )

      val lcname      = "Local/01230041302@default-00001"
      val realChannel = "SIP/trunk-something-0001"
      val path        = makePath(lcname, realChannel)

      wrapper.device ! path
      wrapper.channelTracker.expectMsg(WatchChannelStartingWith(realChannel))
      wrapper.graphTracker.expectMsg(WatchChannelStartingWith(realChannel))
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(realChannel, wrapper.device)
      )

      val channel = Channel(
        "123456789.123",
        realChannel,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HUNGUP
      )

      wrapper.device ! channel

      wrapper.channelTracker.expectMsg(UnWatchChannelStartingWith(realChannel))
      wrapper.graphTracker.expectMsg(UnWatchChannelStartingWith(realChannel))
      wrapper.parent.expectMsg(
        DevicesTracker.UnRegisterActor(realChannel, wrapper.device)
      )

    }

    "remove call from list when channel is hangup (ensure SipDeviceTracker behavior is maintained)" in {
      val wrapper = deviceWrapper(defaultLine)

      val lcname      = "Local/01230041302@default-00001"
      val realChannel = "SIP/trunk-something-0001"
      val path        = makePath(lcname, realChannel)

      val channel = Channel(
        "123456789.123",
        realChannel,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! channel
      wrapper.device ! channel.copy(state = ChannelState.HUNGUP)
      wrapper.device ! GetCalls

      expectMsg(SingleDeviceTracker.Calls(List.empty))
    }

    "rewrite DeviceJoinConference to be attached to the correct channel" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname  = "SIP/abcd-00000001"
      val lcname = "Local/1000@default-0001;1"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )
      val remoteChannel = Channel(
        "123454321.001",
        lcname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        remoteChannel,
        CustomDeviceTrackerType
      )

      wrapper.device ! channel
      wrapper.device ! party
      wrapper.configDispatcher.expectMsgType[PhoneEvent]

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00001",
        CallerId("James Bond", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, lcname, topic)
      expectOnMediator[Subscribe]
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent)

    }

    "rewrite DeviceLeaveConference to be attached to the correct channel" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname  = "SIP/abcd-00000001"
      val lcname = "Local/1000@default-0001;1"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )
      val remoteChannel = Channel(
        "123454321.001",
        lcname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        remoteChannel,
        CustomDeviceTrackerType
      )

      wrapper.device ! channel
      wrapper.device ! party
      wrapper.configDispatcher.expectMsgType[PhoneEvent]

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00001",
        CallerId("James Bond", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, lcname, topic)
      expectOnMediator[Subscribe]
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent)

      wrapper.device ! DeviceLeaveConference(conf, lcname, topic)
      val wsConfLeaveEvent = WsConferenceEvent(
        WsConferenceEventLeave,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )
      wrapper.configDispatcher.expectMsg(wsConfLeaveEvent)
    }
  }
}
