package services.calltracking

import org.apache.pekko.actor._
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.Publish
import org.apache.pekko.testkit._
import org.joda.time.DateTime
import org.scalatest.*
import org.apache.pekko.actor.*
import org.apache.pekko.testkit.*
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.AsteriskGraphTracker.{AsteriskPath, PathsFromChannel}
import services.calltracking.ConferenceTracker.*
import services.calltracking.SingleDeviceTracker.*
import services.calltracking.graph.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.graph.NodeBridge.BridgeCreator
import services.{MediatorWrapper, XucAmiBus, XucEventBus}
import xivo.models.{LocalChannelFeature, XivoFeature}
import xivo.xuc.DeviceTrackerConfig
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import pekkotest.TestKitSpec
import xivo.xucami.models.{CallerId, Channel}

import scala.reflect.ClassTag

class UnknownDeviceTrackerSpec
    extends TestKitSpec("UnknownDeviceTracker")
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AsteriskObjectHelper {

  private val xivoFeature = LocalChannelFeature
  private val topic: String =
    ConferenceTracker.conferenceParticipantEventTopic("my-conf", "10.181.0.2")

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val defaultConfig: DeviceTrackerConfig = new DeviceTrackerConfig {
    def stopRecordingUponExternalXfer: Boolean = true
    def enableRecordingRules: Boolean          = true
  }

  class MediatorHelperSpec()(implicit probe: TestProbe)
      extends MediatorWrapper {
    override def getMediator(acSys: ActorSystem): ActorRef = probe.ref
  }

  def expectOnMediator[T: ClassTag](implicit mediator: TestProbe): Option[T] = {

    var msgOpt: Option[T] = None
    mediator.fishForMessage() {
      case msg: T =>
        msgOpt = Some(msg)
        true
      case _ => false
    }

    msgOpt
  }

  class DeviceActorWrapper(
      val feature: XivoFeature,
      stopRecording: Boolean,
      enableRules: Boolean
  ) {
    val channelTracker: TestProbe    = TestProbe()
    val graphTracker: TestProbe      = TestProbe()
    val bus: XucEventBus             = mock[XucEventBus]
    val amiBus: XucAmiBus            = mock[XucAmiBus]
    val parent: TestProbe            = TestProbe()
    val configDispatcher: TestProbe  = TestProbe()
    implicit val mediator: TestProbe = TestProbe()
    val mediatorWrapper              = new MediatorHelperSpec

    val deviceTrackerConfig: DeviceTrackerConfig = new DeviceTrackerConfig {
      def stopRecordingUponExternalXfer: Boolean = stopRecording
      def enableRecordingRules: Boolean          = enableRules
    }

    val factory = new DeviceActorFactoryImpl(
      channelTracker.ref,
      graphTracker.ref,
      bus,
      amiBus,
      deviceTrackerConfig,
      configDispatcher.ref,
      mediatorWrapper
    )

    val device: TestActorRef[SingleDeviceTracker] =
      TestActorRef[SingleDeviceTracker](
        factory.props(feature),
        parent.ref,
        "MySDTActor"
      )
  }

  def deviceWrapper(
      feature: XivoFeature,
      stopRecording: Boolean = true,
      enableRules: Boolean = true
  ) = new DeviceActorWrapper(feature, stopRecording, enableRules)

  "UnknownDeviceTrackerSpec" should {

    val conference = ConferenceRoom(
      "4000",
      "Superconf",
      ConferenceBusy,
      Some(DateTime.now),
      List.empty,
      "default"
    )

    "be of correct type" in {
      val wrapper = deviceWrapper(xivoFeature)
      wrapper.device.underlyingActor.deviceTrackerType should be(
        UnknownDeviceTrackerType
      )
    }

    "forward join conference event to correct channel" in {
      val cname1      = "Local/4000@default-00001;1"
      val cname2      = "Local/4000@default-00001;2"
      val remoteCname = "SIP/abcd-0000001"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname1),
        Set(
          AsteriskPath(
            NodeLocalBridge(NodeLocalChannel(cname1), NodeLocalChannel(cname2)),
            NodeLocalChannel(cname2),
            NodeBridge("b1", Some(new BridgeCreator("bc1"))),
            NodeChannel(remoteCname)
          )
        )
      )

      val wrapper = deviceWrapper(xivoFeature)
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc

      wrapper.device ! DeviceJoinConference(conference, cname1, topic)
      wrapper.parent.expectMsg(
        DeviceJoinConference(conference, remoteCname, topic)
      )
    }

    "forward leave conference event to correct channel" in {
      val cname1      = "Local/4000@default-00001;1"
      val cname2      = "Local/4000@default-00001;2"
      val remoteCname = "SIP/abcd-0000001"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname1),
        Set(
          AsteriskPath(
            NodeLocalBridge(NodeLocalChannel(cname1), NodeLocalChannel(cname2)),
            NodeLocalChannel(cname2),
            NodeBridge("b1", Some(new BridgeCreator("bc1"))),
            NodeChannel(remoteCname)
          )
        )
      )

      val wrapper = deviceWrapper(xivoFeature)
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc

      wrapper.device ! DeviceLeaveConference(conference, cname1, topic)
      wrapper.parent.expectMsg(
        DeviceLeaveConference(conference, remoteCname, topic)
      )
    }

    "enrich join conference event with sip call id and send it to the mediator" in {
      val cname1    = "Local/4000@default-00001;1"
      val cname2    = "Local/4000@default-00001;2"
      val sipCallId = "123456789"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname1),
        Set(
          AsteriskPath(
            NodeLocalBridge(NodeLocalChannel(cname1), NodeLocalChannel(cname2)),
            NodeLocalChannel(cname2)
          )
        )
      )
      val channel = new Channel(
        "1",
        cname1,
        CallerId("", ""),
        "",
        variables = Map("XIVO_SIPCALLID" -> sipCallId)
      )

      val wrapper           = deviceWrapper(xivoFeature)
      implicit val mediator = wrapper.mediator
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc
      wrapper.device ! channel

      wrapper.device ! DeviceJoinConference(conference, cname1, topic)
      val pubmsg = expectOnMediator[Publish].get
      val msg    = pubmsg.msg.asInstanceOf[DeviceJoinConference]
      msg.conference shouldEqual conference
      msg.channel shouldEqual cname1
      msg.sipCallId shouldEqual Some(sipCallId)
      msg.localChannel shouldEqual Some(cname1)
    }

    "enrich leave conference event with sip call id and send it enriched with sipCallId to the mediator" in {
      val cname1    = "Local/4000@default-00001;1"
      val cname2    = "Local/4000@default-00001;2"
      val sipCallId = "123456789"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname1),
        Set(
          AsteriskPath(
            NodeLocalBridge(NodeLocalChannel(cname1), NodeLocalChannel(cname2)),
            NodeLocalChannel(cname2)
          )
        )
      )
      val channel = new Channel(
        "1",
        cname1,
        CallerId("", ""),
        "",
        variables = Map("XIVO_SIPCALLID" -> sipCallId)
      )

      val wrapper           = deviceWrapper(xivoFeature)
      implicit val mediator = wrapper.mediator
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc
      wrapper.device ! channel

      wrapper.device ! DeviceLeaveConference(conference, cname1, topic)
      val publishMsg = expectOnMediator[Publish].get
      val msg        = publishMsg.msg.asInstanceOf[DeviceLeaveConference]
      msg.conference shouldEqual conference
      msg.channel shouldEqual cname1
      msg.sipCallId shouldEqual Some(sipCallId)
    }

    "forward join conference event with channel matching sip call id to devicetracker" in {
      val cnameDistant = "Local/4001@default-00001;1"
      val cname1       = "Local/4000@default-00001;1"
      val cname2       = "Local/4000@default-00001;2"
      val remoteCname  = "SIP/abcd-0000001"
      val sipCallId    = "123456789"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname1),
        Set(
          AsteriskPath(
            NodeLocalBridge(NodeLocalChannel(cname1), NodeLocalChannel(cname2)),
            NodeLocalChannel(cname2),
            NodeBridge("b1", Some(new BridgeCreator("bc1"))),
            NodeChannel(remoteCname)
          )
        )
      )

      val channel = new Channel(
        "1",
        cname1,
        CallerId("", ""),
        "",
        variables = Map("XIVO_SIPCALLID" -> sipCallId)
      )

      val wrapper = deviceWrapper(xivoFeature)
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc
      wrapper.device ! channel

      wrapper.parent.expectMsgType[PartyInformation]

      wrapper.device ! DeviceJoinConference(
        conference,
        cnameDistant,
        topic,
        Some(sipCallId),
        Some(cnameDistant)
      )
      wrapper.parent.expectMsg(
        DeviceJoinConference(
          conference,
          remoteCname,
          topic,
          Some(sipCallId),
          Some(cnameDistant)
        )
      )
    }

    "forward leave conference event with channel matching sip call id to devicetracker" in {
      val cnameDistant = "Local/4001@default-00001;1"
      val cname1       = "Local/4000@default-00001;1"
      val cname2       = "Local/4000@default-00001;2"
      val remoteCname  = "SIP/abcd-0000001"
      val sipCallId    = "123456789"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname1),
        Set(
          AsteriskPath(
            NodeLocalBridge(NodeLocalChannel(cname1), NodeLocalChannel(cname2)),
            NodeLocalChannel(cname2),
            NodeBridge("b1", Some(new BridgeCreator("bc1"))),
            NodeChannel(remoteCname)
          )
        )
      )

      val channel = new Channel(
        "1",
        cname1,
        CallerId("", ""),
        "",
        variables = Map("XIVO_SIPCALLID" -> sipCallId)
      )

      val wrapper = deviceWrapper(xivoFeature)
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc
      wrapper.device ! channel

      wrapper.parent.expectMsgType[PartyInformation]

      wrapper.device ! DeviceLeaveConference(
        conference,
        cnameDistant,
        topic,
        Some(sipCallId)
      )
      wrapper.parent.expectMsg(
        DeviceLeaveConference(conference, remoteCname, topic, Some(sipCallId))
      )
    }

  }
}
