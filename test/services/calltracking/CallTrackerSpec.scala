package services.calltracking

import org.apache.pekko.actor._
import org.apache.pekko.testkit._
import org.joda.time.DateTime
import org.scalatest._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.AsteriskGraphTracker.AsteriskPath
import services.calltracking.ConferenceTracker._
import services.calltracking.graph.NodeBridge.BridgeCreator
import services.calltracking.graph.{AsteriskObjectHelper, NodeDial, NodeMdsTrunkBridge}
import xivo.events.UserData
import xivo.xucami.models._

import scala.util.Random

class CallTrackerSpec
    extends TestKit(ActorSystem("CallTracker"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AsteriskObjectHelper {

  private val topic: String =
    ConferenceTracker.conferenceParticipantEventTopic("my-conf", "10.181.0.2")

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "CallTracker" should {
    "update call list when channel is created" in {
      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val t = CallTracker().withChannel(c)
      t.calls should contain(
        cname -> DeviceCall(cname, Some(c), Set.empty, Map.empty)
      )
    }

    "update call information with path to remote party" in {
      val cname      = "SIP/abcd-00000001"
      val remoteName = "SIP/efgh-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val paths: Set[AsteriskPath] = Set(AsteriskPath(NodeChannel(remoteName)))
      val t = CallTracker()
        .withChannel(c)
        .withPaths(NodeChannel(cname), paths)
      t.calls should contain(
        cname -> DeviceCall(cname, Some(c), paths, Map.empty)
      )
    }

    "update call information with path to remote party including local channels" in {
      val cname      = "Local/3011@default-00000001"
      val remoteName = "SIP/efgh-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      val paths: Set[AsteriskPath] = Set(AsteriskPath(NodeChannel(remoteName)))
      val t = CallTracker()
        .withChannel(c)
        .withPaths(NodeLocalChannel(cname), paths)
      t.calls should contain(
        cname -> DeviceCall(cname, Some(c), paths, Map.empty)
      )
    }

    "track calls even when receiving information unordered" in {
      val cname      = "SIP/abcd-00000001"
      val remoteName = "SIP/efgh-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val paths: Set[AsteriskPath] = Set(AsteriskPath(NodeChannel(remoteName)))
      val t = CallTracker()
        .withPaths(NodeChannel(cname), paths)
        .withChannel(c)

      t.calls should contain(
        cname -> DeviceCall(cname, Some(c), paths, Map.empty)
      )
    }

    "ignore empty paths when no call associated" in {
      val cname                    = "SIP/abcd-00000001"
      val paths: Set[AsteriskPath] = Set(List.empty)
      val t = CallTracker()
        .withPaths(NodeChannel(cname), paths)

      t.calls shouldBe empty
    }

    "remove call from list when asked for" in {
      val cname = "SIP/abcd-00000001"
      val c1 = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val t = CallTracker()
        .withChannel(c1)
        .removeChannel(cname)

      t.calls shouldBe empty
    }

    "add channel to remoteChannels map when receiving party information" in {
      val cname = "SIP/abcd-00000027"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("sup - 0123456789", "0123456789"),
        "",
        ChannelState.UP
      )
      val remoteName = "SIP/efgh-0000001e;2"
      val rc = Channel(
        "123456789.456",
        remoteName,
        CallerId("James Bond", "1234"),
        "",
        ChannelState.RINGING
      )
      val path: Set[AsteriskPath] = Set(AsteriskPath(NodeChannel(remoteName)))

      val t = CallTracker()
        .withChannel(c)
        .withPaths(NodeChannel(cname), path)

      t.calls should contain(
        cname -> DeviceCall(cname, Some(c), path, Map.empty)
      )

      val res = t.withPartyInformation(cname, rc)

      res.calls should contain(
        cname -> DeviceCall(cname, Some(c), path, Map(remoteName -> rc))
      )
    }

    "remove channel from remoteChannels if channel is not in path anymore" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-00000027",
        CallerId("sup - 0123456789", "0123456789"),
        "",
        ChannelState.UP
      )
      val rc = Channel(
        "123456789.456",
        "Local/id-17@agentcallback-0000001e;2",
        CallerId("sup - 0123456789", "0123456789"),
        "",
        ChannelState.RINGING
      )
      val path: Set[AsteriskPath] = Set(AsteriskPath(NodeChannel(rc.name)))

      val t = CallTracker()
        .withChannel(c)
        .withPaths(NodeChannel(c.name), path)
        .withPartyInformation(c.name, rc)

      t.calls should contain(
        c.name -> DeviceCall(c.name, Some(c), path, Map(rc.name -> rc))
      )

      val newRemoteCName = "SIP/wcovkdex-0000002a"
      val newPath: Set[AsteriskPath] =
        Set(AsteriskPath(NodeChannel(newRemoteCName)))
      val res1 = t.withPaths(NodeChannel(c.name), newPath)

      res1.calls should contain(
        c.name -> DeviceCall(c.name, Some(c), newPath, Map.empty)
      )

      val nrc = Channel(
        "123456789.123",
        newRemoteCName,
        CallerId("sup - 0123456789", "0123456789"),
        "",
        ChannelState.UP
      )
      val res2 = res1.withPartyInformation(c.name, nrc)

      res2.calls should contain(
        c.name -> DeviceCall(c.name, Some(c), newPath, Map(nrc.name -> nrc))
      )
    }

    "remove channel from remoteChannels if channel is not in path anymore (alternative message order: PartyInformation and then Paths)" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-00000027",
        CallerId("sup - 0123456789", "0123456789"),
        "",
        ChannelState.UP
      )
      val rc = Channel(
        "123456789.456",
        "Local/id-17@agentcallback-0000001e;2",
        CallerId("sup - 0123456789", "0123456789"),
        "",
        ChannelState.RINGING
      )
      val path: Set[AsteriskPath] = Set(AsteriskPath(NodeChannel(rc.name)))

      val t = CallTracker()
        .withChannel(c)
        .withPaths(NodeChannel(c.name), path)
        .withPartyInformation(c.name, rc)

      t.calls should contain(
        c.name -> DeviceCall(c.name, Some(c), path, Map(rc.name -> rc))
      )

      val nrc = Channel(
        "123456789.123",
        "SIP/wcovkdex-0000002a",
        CallerId("sup - 0123456789", "0123456789"),
        "",
        ChannelState.UP
      )
      val res1 = t.withPartyInformation(c.name, nrc)

      res1.calls should contain(
        c.name -> DeviceCall(
          c.name,
          Some(c),
          path,
          Map(rc.name -> rc, nrc.name -> nrc)
        )
      )

      val newPath: Set[AsteriskPath] = Set(AsteriskPath(NodeChannel(nrc.name)))
      val res2                       = res1.withPaths(NodeChannel(c.name), newPath)

      res2.calls should contain(
        c.name -> DeviceCall(c.name, Some(c), newPath, Map(nrc.name -> nrc))
      )
    }

    "track conference when joining a conference" in {
      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      val t = CallTracker()
        .withChannel(c)
        .withConferenceEvent(DeviceJoinConference(conf, cname, topic))
      t.conferences should contain(
        conf.number -> ConferenceWithChannels(conf, Set(Set(cname)), topic)
      )
    }

    "update a conference when asked to" in {
      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val otherParticipant = ConferenceParticipant(
        "4000",
        2,
        "SIP/efgh-00000002",
        CallerId("Jason Bourne", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )
      val updatedConf = conf.addParticipant(otherParticipant)

      val t = CallTracker()
        .withChannel(c)
        .withConferenceEvent(DeviceJoinConference(conf, cname, topic))
        .updateConference(updatedConf)
      t.conferences should contain(
        conf.number -> ConferenceWithChannels(
          updatedConf,
          Set(Set(cname)),
          topic
        )
      )
    }

    "track conference including remote channels to allow self-detection across mds" in {
      val cname       = "SIP/abcd-00000001"
      val remotename  = "SIP/from-mds1-00000009"
      val remotename2 = "SIP/anonymous-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )
      val paths = Set(
        AsteriskPath(
          NodeBridge("b1", "mds1", Some(new BridgeCreator("bc1"))),
          NodeChannel(remotename, "mds1")
        )
      )

      val t = CallTracker()
        .withChannel(c)
        .withPaths(NodeChannel(c.name), paths)
        .withConferenceEvent(
          DeviceJoinConference(conf, cname, topic, None, Some(remotename2))
        )
      t.conferences should contain(
        conf.number -> ConferenceWithChannels(
          conf,
          Set(Set(cname, remotename, remotename2)),
          topic
        )
      )
    }

    "track conference including remote channels to allow self-detection across mds multiple times" in {
      val cname1 = "SIP/abcd-00000001"
      val cname2 = "SIP/abcd-00000002"

      val remotename1 = "SIP/from-mds1-00000009"
      val remotename2 = "SIP/from-mds1-00000010"
      val c1 = Channel(
        "123456789.123",
        cname1,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val c2 = Channel(
        "123456789.456",
        cname2,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val p1 = ConferenceParticipant(
        "4000",
        1,
        cname1,
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val p2 = ConferenceParticipant(
        "4000",
        1,
        cname2,
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(p1, p2),
        "default"
      )
      val paths1 = Set(
        AsteriskPath(
          NodeBridge("b1", "mds1", Some(new BridgeCreator("bc1"))),
          NodeChannel(remotename1, "mds1")
        )
      )
      val paths2 = Set(
        AsteriskPath(
          NodeBridge("b2", "mds1", Some(new BridgeCreator("bc2"))),
          NodeChannel(remotename2, "mds1")
        )
      )

      val t = CallTracker()
        .withChannel(c1)
        .withChannel(c2)
        .withPaths(NodeChannel(c1.name), paths1)
        .withPaths(NodeChannel(c2.name), paths2)
        .withConferenceEvent(DeviceJoinConference(conf, cname1, topic))
        .withConferenceEvent(DeviceJoinConference(conf, cname2, topic))

      t.conferences should contain only (conf.number -> ConferenceWithChannels(
        conf,
        Set(Set(cname1, remotename1), Set(cname2, remotename2)),
        topic
      ))
    }

    "remove conference when leaving a conference across mds" in {
      val cname      = "SIP/abcd-00000001"
      val remotename = "SIP/from-mds1-00000009"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )
      val paths = Set(
        AsteriskPath(
          NodeBridge("b1", "mds1", Some(new BridgeCreator("bc1"))),
          NodeChannel(remotename, "mds1")
        )
      )

      val t = CallTracker()
        .withChannel(c)
        .withPaths(NodeChannel(c.name), paths)
        .withConferenceEvent(DeviceJoinConference(conf, cname, topic))
        .withConferenceEvent(DeviceLeaveConference(conf, cname, topic))
      t.conferences shouldBe empty
    }

    "remove conference when leaving a conference across mds connected multiple times" in {
      val cname1 = "SIP/abcd-00000001"
      val cname2 = "SIP/abcd-00000002"

      val remotename1 = "SIP/from-mds1-00000009"
      val remotename2 = "SIP/from-mds1-00000010"
      val c1 = Channel(
        "123456789.123",
        cname1,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val c2 = Channel(
        "123456789.456",
        cname2,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val p1 = ConferenceParticipant(
        "4000",
        1,
        cname1,
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val p2 = ConferenceParticipant(
        "4000",
        1,
        cname2,
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(p1, p2),
        "default"
      )
      val paths1 = Set(
        AsteriskPath(
          NodeBridge("b1", "mds1", Some(new BridgeCreator("bc1"))),
          NodeChannel(remotename1, "mds1")
        )
      )
      val paths2 = Set(
        AsteriskPath(
          NodeBridge("b2", "mds1", Some(new BridgeCreator("bc2"))),
          NodeChannel(remotename2, "mds1")
        )
      )

      val t = CallTracker()
        .withChannel(c1)
        .withChannel(c2)
        .withPaths(NodeChannel(c1.name), paths1)
        .withPaths(NodeChannel(c2.name), paths2)
        .withConferenceEvent(DeviceJoinConference(conf, cname1, topic))
        .withConferenceEvent(DeviceJoinConference(conf, cname2, topic))
        .withConferenceEvent(DeviceLeaveConference(conf, remotename2, topic))

      t.conferences should contain only (conf.number -> ConferenceWithChannels(
        conf,
        Set(Set(cname1, remotename1)),
        topic
      ))
    }

    "remove conference when leaving a conference" in {
      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      val t = CallTracker()
        .withChannel(c)
        .withConferenceEvent(DeviceJoinConference(conf, cname, topic))
        .withConferenceEvent(DeviceLeaveConference(conf, cname, topic))
      t.conferences shouldBe empty
    }

    "update conference when a new participant join" in {
      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      val newParticipant = ConferenceParticipant(
        "4000",
        2,
        "SIP/efgh-00000002",
        CallerId("Jason Bourne", "1001"),
        DateTime.now
      )
      val t = CallTracker()
        .withChannel(c)
        .withConferenceEvent(DeviceJoinConference(conf, cname, topic))
        .withConferenceParticipantEvent(
          ParticipantJoinConference(conf.number, newParticipant)
        )
      t.conferences should contain(
        conf.number -> ConferenceWithChannels(
          conf.addParticipant(newParticipant),
          Set(Set(cname)),
          topic
        )
      )
    }

    "update conference when a participant leave" in {
      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      val newParticipant = ConferenceParticipant(
        "4000",
        2,
        "SIP/efgh-00000002",
        CallerId("Jason Bourne", "1001"),
        DateTime.now
      )
      val t = CallTracker()
        .withChannel(c)
        .withConferenceEvent(DeviceJoinConference(conf, cname, topic))
        .withConferenceParticipantEvent(
          ParticipantJoinConference(conf.number, newParticipant)
        )
        .withConferenceParticipantEvent(
          ParticipantLeaveConference(conf.number, newParticipant)
        )
      t.conferences should contain(
        conf.number -> ConferenceWithChannels(conf, Set(Set(cname)), topic)
      )
    }

    "update conference when a participant begin to speak" in {
      val channelName = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        channelName,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participantNotTalking = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val participantIsTalking = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now,
        true
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List.empty,
        "default"
      )

      val t = CallTracker()
        .withChannel(channel)
        .withConferenceEvent(DeviceJoinConference(conf, channelName, topic))
        .withConferenceParticipantEvent(
          ParticipantJoinConference(conf.number, participantNotTalking)
        )
        .withConferenceParticipantEvent(
          ParticipantUpdated(conf.number, participantIsTalking)
        )

      t.conferences should contain(
        conf.number -> ConferenceWithChannels(
          conf.addParticipant(participantIsTalking),
          Set(Set(channelName)),
          topic
        )
      )
    }

    "update conference when a participant stop to speak" in {
      val channelName = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        channelName,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participantNotTalking = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val participantIsTalking = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now,
        true
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List.empty,
        "default"
      )

      val t = CallTracker()
        .withChannel(channel)
        .withConferenceEvent(DeviceJoinConference(conf, channelName, topic))
        .withConferenceParticipantEvent(
          ParticipantJoinConference(conf.number, participantNotTalking)
        )
        .withConferenceParticipantEvent(
          ParticipantUpdated(conf.number, participantIsTalking)
        )
        .withConferenceParticipantEvent(
          ParticipantUpdated(conf.number, participantNotTalking)
        )

      t.conferences should contain(
        conf.number -> ConferenceWithChannels(
          conf.addParticipant(participantNotTalking),
          Set(Set(channelName)),
          topic
        )
      )
    }

    "update conference when a participant role change" in {
      val channelName = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        channelName,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participantUser = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now,
        role = UserRole
      )
      val participantOrganizer = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now,
        role = OrganizerRole
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List.empty,
        "default"
      )

      val t = CallTracker()
        .withChannel(channel)
        .withConferenceEvent(DeviceJoinConference(conf, channelName, topic))
        .withConferenceParticipantEvent(
          ParticipantJoinConference(conf.number, participantUser)
        )
        .withConferenceParticipantEvent(
          ParticipantUpdated(conf.number, participantOrganizer)
        )

      t.conferences should contain(
        conf.number -> ConferenceWithChannels(
          conf.addParticipant(participantOrganizer),
          Set(Set(channelName)),
          topic
        )
      )
    }

    "update conference when a participant mute state change" in {
      val channelName = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        channelName,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val participantUnMuted = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now,
        isMuted = false
      )
      val participantMuted = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now,
        isMuted = true
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List.empty,
        "default"
      )

      val t = CallTracker()
        .withChannel(channel)
        .withConferenceEvent(DeviceJoinConference(conf, channelName, topic))
        .withConferenceParticipantEvent(
          ParticipantJoinConference(conf.number, participantUnMuted)
        )
        .withConferenceParticipantEvent(
          ParticipantUpdated(conf.number, participantMuted)
        )

      t.conferences should contain(
        conf.number -> ConferenceWithChannels(
          conf.addParticipant(participantMuted),
          Set(Set(channelName)),
          topic
        )
      )
    }

    "find myself in a conference room" in {
      val p1 = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val p2 = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00000001",
        CallerId("Jason Bourne", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(p1, p2),
        "default"
      )
      val confWithChannel =
        ConferenceWithChannels(conf, Set(Set(p1.channelName)), topic)

      confWithChannel.findMe() should contain only p1
    }

    "find all instance of myself in a conference room if connected multiple times" in {
      val p1 = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val p2 = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00000001",
        CallerId("Jason Bourne", "1001"),
        DateTime.now
      )
      val p3 = ConferenceParticipant(
        "4000",
        2,
        "SIP/abcd-00000002",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(p1, p2, p3),
        "default"
      )
      val confWithChannel = ConferenceWithChannels(
        conf,
        Set(Set(p1.channelName), Set(p3.channelName)),
        topic
      )

      confWithChannel.findMe() should contain.only(p1, p3)
    }

  }

  "DeviceCall" should {
    "get queuename from remote channel" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val rc = Channel(
        "123456789.123",
        "SIP/efgh-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP
      )
        .updateVariable(UserData.QueueNameKey, "my-queue")

      val call =
        DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map(rc.name -> rc))

      call.queueName should be(Some("my-queue"))
    }

    "ignore queuename from channel if calling a queue" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
        .updateVariable(UserData.QueueNameKey, "my-queue")
      val rc = Channel(
        "123456789.123",
        "SIP/efgh-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP
      )

      val call =
        DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map(rc.name -> rc))

      call.queueName should be(None)
    }

    "ignore queuename from remote channels if it's an originate" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
        .updateVariable(
          Channel.VarNames.xucCallType,
          Channel.callTypeValOriginate
        )
      val rc = Channel(
        "123456789.123",
        "SIP/efgh-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP
      )
        .updateVariable(UserData.QueueNameKey, "my-queue")

      val call =
        DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map(rc.name -> rc))

      call.queueName shouldBe empty
    }

    "get queuename from linked local channel if it's an outbound originate" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
        .updateVariable(
          Channel.VarNames.xucCallType,
          Channel.callTypeValOutboundOriginate
        )
      val rc = Channel(
        "123456789.123",
        "Local/3012@default-0000002",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP
      )
        .updateVariable(UserData.QueueNameKey, "my-queue")

      val call =
        DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map(rc.name -> rc))

      call.queueName shouldBe Some("my-queue")
    }

    "get remote party name & number from channel.connectedLine if available" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
        .withConnectedLine("1001", "Some One")
      val rc = Channel(
        "123456789.123",
        "SIP/efgh-0000001",
        CallerId("Not Set", "XXXX"),
        "",
        ChannelState.UP
      )

      val call =
        DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map(rc.name -> rc))

      call.remoteLineName should be(Some("Some One"))
      call.remoteLineNumber should be(Some("1001"))
    }

    "get remote party name & number from remote channel.callerId if available" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val rc = Channel(
        "123456789.123",
        "SIP/efgh-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP,
        direction = Option {
          ChannelDirection.OUTGOING
        }
      )

      val call =
        DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map(rc.name -> rc))

      call.remoteLineName should be(Some("Some One"))
      call.remoteLineNumber should be(Some("1001"))
    }

    "get remote party name & number from any remote channel.callerId if available" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val rc1 = Channel(
        "123456789.124",
        "SIP/efgh-0000001",
        CallerId("", ""),
        "",
        ChannelState.UP
      )
      val rc2 = Channel(
        "123456789.125",
        "SIP/ijkl-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP,
        direction = Option {
          ChannelDirection.OUTGOING
        }
      )

      val call = DeviceCall(
        "SIP/abcd-0000001",
        Some(c),
        Set.empty,
        Map(rc1.name -> rc1, rc2.name -> rc2)
      )

      call.remoteLineName should be(Some("Some One"))
      call.remoteLineNumber should be(Some("1001"))
    }

    "get remote party name & number from any non local channel" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val rc1 = Channel(
        "123456789.124",
        "Local/1007@default-00002",
        CallerId("Local Channel", "1007"),
        "",
        ChannelState.UP
      )
      val rc2 = Channel(
        "123456789.125",
        "SIP/ijkl-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP,
        direction = Option {
          ChannelDirection.OUTGOING
        }
      )

      val call = DeviceCall(
        "SIP/abcd-0000001",
        Some(c),
        Set.empty,
        Map(rc1.name -> rc1, rc2.name -> rc2)
      )

      call.remoteLineName should be(Some("Some One"))
      call.remoteLineNumber should be(Some("1001"))
    }

    "get remote party name & number from any non group channel" in {
      val variables =
        Map("__XIVO_FWD_REFERER" -> "group:1", "XIVO_USERID" -> "4")
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val rc1 = Channel(
        "123456789.125",
        "SIP/ijkl-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP,
        variables = variables
      )
      val rc2 = Channel(
        "123456789.126",
        "SIP/efgh-0000001",
        CallerId("Some Two", "1002"),
        "",
        ChannelState.UP,
        direction = Option {
          ChannelDirection.OUTGOING
        }
      )

      val call = DeviceCall(
        "SIP/abcd-0000001",
        Some(c),
        Set.empty,
        Map(rc1.name -> rc1, rc2.name -> rc2)
      )

      call.remoteLineName should be(Some("Some Two"))
      call.remoteLineNumber should be(Some("1002"))
    }

    "get remote party name & number from channel from trunk to group" in {
      val variables =
        Map("__XIVO_FWD_REFERER" -> "group:1", "XIVO_USERID" -> "4")
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val rc1 = Channel(
        "123456789.125",
        "SIP/from-trunk-0000001",
        CallerId(null, "033123456789"),
        "",
        ChannelState.UP,
        direction = Option {
          ChannelDirection.OUTGOING
        }
      )

      val rc2 = Channel(
        "123456789.126",
        "SIP/efgh-0000001",
        CallerId("Some Two", "1002"),
        "",
        ChannelState.UP,
        variables = variables
      )

      val call = DeviceCall(
        "SIP/abcd-0000001",
        Some(c),
        Set.empty,
        Map(rc1.name -> rc1, rc2.name -> rc2)
      )

      call.remoteLineName should be(None)
      call.remoteLineNumber should be(Some("033123456789"))
    }

    "get remote party name & number from channel from trunk to group with empty channel vars" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val rc1 = Channel(
        "123456789.125",
        "SIP/from-trunk-0000001",
        CallerId(null, "033123456789"),
        "",
        ChannelState.UP,
        direction = Option {
          ChannelDirection.OUTGOING
        }
      )
      val rc2 = Channel(
        "123456789.126",
        "SIP/efgh-0000001",
        CallerId("Some Two", "1002"),
        "",
        ChannelState.DOWN
      )

      val call = DeviceCall(
        "SIP/abcd-0000001",
        Some(c),
        Set.empty,
        Map(rc1.name -> rc1, rc2.name -> rc2)
      )

      call.remoteLineName should be(None)
      call.remoteLineNumber should be(Some("033123456789"))
    }

    "get remote party number from channel.exten if no connectedLine nor remote connectedLine" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING,
        exten = Some("1234"),
        direction = Option {
          ChannelDirection.OUTGOING
        }
      )
      val rc = Channel(
        "123456789.123",
        "SIP/efgh-0000001",
        CallerId(null, null),
        "",
        ChannelState.UP
      )

      val call =
        DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map(rc.name -> rc))

      call.remoteLineName should be(None)
      call.remoteLineNumber should be(Some("1234"))
    }

    "get variables from channel & connected party channel" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
        .updateVariable("USR_MY_LOCAL_VAR", "Some value")
      val rc1 = Channel(
        "123456789.123",
        "SIP/efgh-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP
      )
        .updateVariable("USR_MY_REMOTE_VAR", "Another value")
      val rc2 = Channel(
        "123456789.123",
        "SIP/Local-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP
      )
        .updateVariable("USR_MY_REMOTE_LOCAL_VAR", "Another local value")

      val call = DeviceCall(
        "SIP/abcd-0000001",
        Some(c),
        Set.empty,
        Map(rc1.name -> rc1, rc2.name -> rc2)
      )

      call.variables should contain.allOf(
        ("USR_MY_LOCAL_VAR", "Some value"),
        ("USR_MY_REMOTE_VAR", "Another value"),
        ("USR_MY_REMOTE_LOCAL_VAR", "Another local value")
      )

    }

    "get non local remote channels" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val rc1 = Channel(
        "123456789.124",
        "Local/1007@default-00002",
        CallerId("Local Channel", "1007"),
        "",
        ChannelState.UP
      )
      val rc2 = Channel(
        "123456789.125",
        "SIP/ijkl-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP
      )
      val rc3 = Channel(
        "123456789.126",
        "SIP/mnop-0000001",
        CallerId("Some Other One", "1002"),
        "",
        ChannelState.UP
      )

      val call = DeviceCall(
        "SIP/abcd-0000001",
        Some(c),
        Set.empty,
        Map(rc1.name -> rc1, rc2.name -> rc2, rc3.name -> rc3)
      )

      call.nonLocalRemoteChannels should contain.allOf(rc2, rc3)
      call.nonLocalRemoteChannels.size shouldBe 2
    }

    "get monitored channels" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING,
        MonitorState.PAUSED
      )
      val rc1 = Channel(
        "123456789.124",
        "Local/1007@default-00002",
        CallerId("Local Channel", "1007"),
        "",
        ChannelState.UP,
        MonitorState.PAUSED
      )
      val rc2 = Channel(
        "123456789.125",
        "SIP/ijkl-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP,
        MonitorState.PAUSED
      )
      val rc3 = Channel(
        "123456789.126",
        "SIP/mnop-0000001",
        CallerId("Some Other One", "1002"),
        "",
        ChannelState.UP,
        MonitorState.PAUSED
      )
      val rc4 = Channel(
        "123456789.127",
        "SIP/qrst-0000001",
        CallerId("Some Other Two", "1003"),
        "",
        ChannelState.UP,
        MonitorState.DISABLED
      )

      val call = DeviceCall(
        "SIP/abcd-0000001",
        Some(c),
        Set.empty,
        Map(rc1.name -> rc1, rc2.name -> rc2, rc3.name -> rc3, rc4.name -> rc4)
      )
      call.monitoredChannels should contain.allOf(c, rc1, rc2, rc3)
      call.monitoredChannels.size shouldBe 4
    }

    "get monitor state from local channel first" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        MonitorState.PAUSED
      )
      val rc = Channel(
        "123456789.123",
        "SIP/efgh-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP,
        MonitorState.DISABLED
      )
      val call =
        DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map(rc.name -> rc))

      call.monitorState should be(Some(MonitorState.PAUSED))
    }

    "get monitor state from remote channel if disabled on device channel" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        MonitorState.DISABLED
      )
      val rc = Channel(
        "123456789.123",
        "SIP/efgh-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP,
        MonitorState.PAUSED
      )
      val call =
        DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map(rc.name -> rc))

      call.monitorState should be(Some(MonitorState.PAUSED))
    }

    "get first non disabled monitor state from any remote channel if disabled on device channel" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        MonitorState.DISABLED
      )
      val rc1 = Channel(
        "123456789.124",
        "SIP/efgh-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP,
        MonitorState.DISABLED
      )
      val rc2 = Channel(
        "123456789.125",
        "SIP/ijkl-0000001",
        CallerId("Some One", "1001"),
        "",
        ChannelState.UP,
        MonitorState.PAUSED
      )
      val call = DeviceCall(
        "SIP/abcd-0000001",
        Some(c),
        Set.empty,
        Map(rc1.name -> rc1, rc2.name -> rc2)
      )

      call.monitorState should be(Some(MonitorState.PAUSED))
    }

    "not fail to get monitor state if no remote channel" in {
      val c = Channel(
        "123456789.123",
        "SIP/abcd-0000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        MonitorState.DISABLED
      )
      val call = DeviceCall("SIP/abcd-0000001", Some(c), Set.empty, Map.empty)

      call.monitorState should be(None)
    }

    "not fail to get monitor state if no channel" in {
      val call = DeviceCall("SIP/abcd-0000001", None, Set.empty, Map.empty)

      call.monitorState should be(None)
    }
  }

  "getChannelState" should {
    def mkChannel(
        state: ChannelState.ChannelState,
        interface: String = "SIP/abcd",
        number: String = "1000",
        originating: Boolean = false,
        queueHistory: List[QueueHistoryEvent] = List.empty
    ): Channel = {
      val channelName = Random.nextInt(5).formatted(interface + "-%05d")
      val callId      = Random.nextInt(9).toString

      val variables: Map[String, String] =
        if (originating)
          Map(Channel.VarNames.xucCallType -> Channel.callTypeValOriginate)
        else Map.empty

      Channel(
        callId,
        channelName,
        CallerId("Someone", number),
        "",
        state,
        variables = variables,
        queueHistory = queueHistory
      )
    }

    def mkCall(channel: Channel, party: Option[Channel] = None): DeviceCall =
      DeviceCall(
        channel.name,
        Some(channel),
        Set.empty,
        party.map(p => Map(p.name -> p)).getOrElse(Map.empty)
      )

    "return ORIGINATING when originating local channel is RINGING" in {
      val channel = mkChannel(ChannelState.RINGING, originating = true)
      val call    = mkCall(channel)

      call.callState should be(Some(ChannelState.ORIGINATING))
    }

    "return ORIGINATING when outbound originating and channel is RINGING" in {
      val channel = mkChannel(ChannelState.RINGING)
        .updateVariable(
          "__" + Channel.VarNames.xucCallType,
          Channel.callTypeValOutboundOriginate
        )
      val call = mkCall(channel)

      call.callState should be(Some(ChannelState.ORIGINATING))
    }

    "return ORIGINATING when outbound originating and remote channel is RINGING" in {
      val channel = mkChannel(ChannelState.UP)
        .updateVariable(
          "__" + Channel.VarNames.xucCallType,
          Channel.callTypeValOutboundOriginate
        )
      val party          = mkChannel(ChannelState.RINGING)
      val path           = AsteriskPath(NodeChannel(party.name))
      val remoteChannels = Map(party.name -> party)
      val call =
        DeviceCall(channel.name, Some(channel), Set(path), remoteChannels)

      call.callState should be(Some(ChannelState.ORIGINATING))
    }

    "return ORIGINATING when outbound originating and remote channel is RINGING even if intermediates Local channels are UP" in {
      val channel = mkChannel(ChannelState.UP, "SIP/abcd")
        .updateVariable(
          "__" + Channel.VarNames.xucCallType,
          Channel.callTypeValOutboundOriginate
        )
      val lc1       = mkChannel(ChannelState.UP, "Local/1234@default-1")
      val lc2       = mkChannel(ChannelState.UP, "Local/1234@default-2")
      val remoteEnd = mkChannel(ChannelState.RINGING, "SIP/efgh")
      val remoteChannels =
        Map(lc1.name -> lc1, lc2.name -> lc2, remoteEnd.name -> remoteEnd)
      val path = AsteriskPath(
        NodeLocalChannel(lc1.name),
        NodeLocalChannel(lc2.name),
        NodeChannel(remoteEnd.name)
      )
      val call =
        DeviceCall(channel.name, Some(channel), Set(path), remoteChannels)
      call.callState should be(Some(ChannelState.ORIGINATING))
    }

    "return ORIGINATING when outbound originating and if only local channels as remote" in {
      // Here we are dialing but asterisk did not create the end channel (trunk or sip)
      // and only has the intermediate local channels so we assume we are still dialing
      val channel = mkChannel(ChannelState.UP, "SIP/abcd")
        .updateVariable(
          "__" + Channel.VarNames.xucCallType,
          Channel.callTypeValOutboundOriginate
        )
      val lc1 = mkChannel(ChannelState.UP, "Local/1234@default-1")
      val lc2 = mkChannel(ChannelState.UP, "Local/1234@default-2")

      val remoteChannels = Map(lc1.name -> lc1, lc2.name -> lc2)
      val path =
        AsteriskPath(NodeLocalChannel(lc1.name), NodeLocalChannel(lc2.name))
      val call =
        DeviceCall(channel.name, Some(channel), Set(path), remoteChannels)
      call.callState should be(Some(ChannelState.ORIGINATING))
    }

    "return UP when outbound originating if remote end channel is UP and no DIALING state in path" in {
      val channel = mkChannel(ChannelState.UP)
        .updateVariable(
          "__" + Channel.VarNames.xucCallType,
          Channel.callTypeValOutboundOriginate
        )
      val lc1       = mkChannel(ChannelState.UP, "Local/1234@default-1")
      val lc2       = mkChannel(ChannelState.UP, "Local/1234@default-2")
      val remoteEnd = mkChannel(ChannelState.UP, "SIP/efgh")
      val remoteChannels =
        Map(lc1.name -> lc1, lc2.name -> lc2, remoteEnd.name -> remoteEnd)
      val path = AsteriskPath(
        NodeLocalChannel(lc1.name),
        NodeLocalChannel(lc2.name),
        NodeChannel(remoteEnd.name)
      )
      val call =
        DeviceCall(channel.name, Some(channel), Set(path), remoteChannels)
      call.callState should be(Some(ChannelState.UP))
    }

    "return UP when outbound originating even with artifact from MdsTrunkBridge" in {
      val channel = mkChannel(ChannelState.UP)
        .updateVariable(
          "__" + Channel.VarNames.xucCallType,
          Channel.callTypeValOutboundOriginate
        )
      val lc1       = mkChannel(ChannelState.UP, "Local/1234@default-1")
      val lc2       = mkChannel(ChannelState.UP, "Local/1234@default-2")
      val remoteEnd = mkChannel(ChannelState.UP, "SIP/efgh")
      val remoteChannels =
        Map(lc1.name -> lc1, lc2.name -> lc2, remoteEnd.name -> remoteEnd)
      val artifact = AsteriskPath(
        NodeMdsTrunkBridge("42c3a1e157c0934a2eba5dcf35fe88e2@10.49.0.2:5060")
      )
      val path = AsteriskPath(
        NodeLocalChannel(lc1.name),
        NodeLocalChannel(lc2.name),
        NodeChannel(remoteEnd.name)
      )
      val call = DeviceCall(
        channel.name,
        Some(channel),
        Set(artifact, path),
        remoteChannels
      )
      call.callState should be(Some(ChannelState.UP))
    }

    "return RINGING when non-originating local channel is RINGING" in {
      val channel = mkChannel(ChannelState.RINGING)
      val call    = mkCall(channel)

      call.callState should be(Some(ChannelState.RINGING))
    }

    "return UP when originating local channel is UP and remote channel is UP" in {
      val channel = mkChannel(ChannelState.UP, originating = true)
      val party   = Some(mkChannel(ChannelState.UP))
      val call    = mkCall(channel, party)

      call.callState should be(Some(ChannelState.UP))
    }

    "return UP when originating local channel is UP and remote channel is HOLD" in {
      val channel = mkChannel(ChannelState.UP, originating = true)
      val party   = Some(mkChannel(ChannelState.HOLD))
      val call    = mkCall(channel, party)

      call.callState should be(Some(ChannelState.UP))
    }

    "return UP when originating local channel is UP and last queueHistory event is EnterQueue" in {
      val enterQueueEvent = mock[EnterQueue]
      val channel = mkChannel(
        ChannelState.UP,
        originating = true,
        queueHistory = List[QueueHistoryEvent](enterQueueEvent)
      )
      val party = Some(mkChannel(ChannelState.RINGING))
      val call  = mkCall(channel, party)

      call.callState should be(Some(ChannelState.UP))
    }

    "Doesnt return UP when originating local channel is UP and last queueHistory event is LeaveQueue" in {
      val leaveQueueEvent = mock[LeaveQueue]
      val enterQueueEvent = mock[EnterQueue]
      val channel = mkChannel(
        ChannelState.UP,
        originating = true,
        queueHistory = List[QueueHistoryEvent](enterQueueEvent, leaveQueueEvent)
      )
      val party = Some(mkChannel(ChannelState.RINGING))
      val call  = mkCall(channel, party)

      call.callState should be(Some(ChannelState.ORIGINATING))
    }

    "return UP when originating local channel is UP, no remote party but got XIVO_PICKEDUP set" in {
      /*
       This is the case when calling a voicemail (*98).
       In this case, there is only one channel up immediately (because it's an originate) and there is no
       remote party. So we rely on the XIVO_PICKEDUP variable set by the diaplan.
       */
      val channel = mkChannel(ChannelState.UP, originating = true)
        .updateVariable(
          Channel.VarNames.XiVOAnsweredVariable,
          Channel.callAnsweredValue
        )
      val call = mkCall(channel)

      call.callState should be(Some(ChannelState.UP))
    }

    "return UP when originating local channel is UP, no remote party but got DIALSTATUS set to ANSWER" in {
      /*
       This is the case when transferring using local channels to an application (voicemail or conference room).
       In this case, there is only one channel up immediately (because it's an originate) and there is no
       remote party. So we rely on the DIALSTATUS variable set by the Dial application and not only on the
       XIVO_PICKEDUP variable as it is only set on the second local channel.
       */
      val channel = mkChannel(ChannelState.UP, originating = true)
        .updateVariable(
          Channel.VarNames.AsteriskDialStatusVariable,
          Channel.callAnsweredDialStatus
        )
      val call = mkCall(channel)

      call.callState should be(Some(ChannelState.UP))
    }

    "return ORIGINATING when originating local channel is UP" in {
      val channel = mkChannel(ChannelState.UP, originating = true)
      val call    = mkCall(channel)

      call.callState should be(Some(ChannelState.ORIGINATING))
    }

    "return ORIGINATING when originating until DIALING state in path" in {
      val cA = mkChannel(ChannelState.UP, originating = true)
      val cB = mkChannel(ChannelState.UP, "SIP/efgh", "1001")

      val remoteChannels = Map(cB.name -> cB)
      val path = AsteriskPath(
        NodeDial(NodeChannel(cA.name), NodeChannel(cB.name)),
        NodeChannel(cB.name)
      )
      val call = DeviceCall(cA.name, Some(cA), Set(path), remoteChannels)
      call.callState should be(Some(ChannelState.ORIGINATING))
    }

    "return UP when originating if no DIALING state in path" in {
      val cA = mkChannel(ChannelState.UP, originating = true)
      val cB = mkChannel(ChannelState.UP, "SIP/efgh", "1001")

      val remoteChannels = Map(cB.name -> cB)
      val path           = AsteriskPath(NodeChannel("dummyChannel"))
      val call           = DeviceCall(cA.name, Some(cA), Set(path), remoteChannels)
      call.callState should be(Some(ChannelState.UP))
    }

    "return UP when non-originating local channel is UP" in {
      val channel = mkChannel(ChannelState.UP, originating = false)
      val call    = mkCall(channel)

      call.callState should be(Some(ChannelState.UP))
    }

    "return HOLD when local channel is HOLD" in {
      val channel = mkChannel(ChannelState.HOLD)
      val call    = mkCall(channel)

      call.callState should be(Some(ChannelState.HOLD))
    }

    "return HUNGUP when local channel is HUNGUP" in {
      val channel = mkChannel(ChannelState.HUNGUP)
      val call    = mkCall(channel)

      call.callState should be(Some(ChannelState.HUNGUP))
    }

    "return ORIGINATING when local channel is ORIGINATING" in {
      val channel = mkChannel(ChannelState.ORIGINATING)
      val call    = mkCall(channel)

      call.callState should be(Some(ChannelState.ORIGINATING))
    }

    "return UP when originating from switchboard hold queue" in {
      val channel = mkChannel(ChannelState.UP, originating = true)
        .updateVariable(Channel.VarNames.XiVOSwitchBoardRetrieve, "")
      val call = mkCall(channel)

      call.callState should be(Some(ChannelState.UP))
    }
  }
}
