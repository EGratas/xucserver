package services.calltracking

import org.asteriskjava.manager.event.{
  MeetMeJoinEvent,
  MeetMeLeaveEvent,
  MeetMeTalkingEvent
}
import org.joda.time.DateTime
import org.joda.time.Seconds
import xivo.models.MeetMeConference
import xivo.xucami.models.CallerId
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class ConferenceRoomSpec extends AnyWordSpec with Matchers {

  "ConferenceRoom" should {
    "add participant in empty room then store start date and update status" in {
      val emptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default"
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val resultRoom = emptyRoom.addParticipant(participant)

      resultRoom.status shouldBe ConferenceBusy
      resultRoom.startTime shouldBe Some(participant.joinTime)
      resultRoom.participants should contain only participant

    }

    "add participant in non-empty room and do not update start date nor status" in {
      val p1 = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val p2 = ConferenceParticipant(
        "4000",
        2,
        "SIP/efgh-00002",
        CallerId("Some Two", "1002"),
        DateTime.now
      )
      val nonEmptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceBusy,
        Some(DateTime.now.minusMinutes(10)),
        List(p1),
        "default"
      )
      val resultRoom = nonEmptyRoom.addParticipant(p2)

      resultRoom.status shouldBe ConferenceBusy
      resultRoom.startTime shouldBe nonEmptyRoom.startTime
      resultRoom.participants should contain.only(p1, p2)
    }

    "remove participant of one-participant room" in {
      val p1 = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val nonEmptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceBusy,
        Some(DateTime.now.minusMinutes(10)),
        List(p1),
        "default"
      )
      val resultRoom = nonEmptyRoom.removeParticipant(p1)

      resultRoom.status shouldBe ConferenceAvailable
      resultRoom.startTime shouldBe empty
      resultRoom.participants shouldBe empty
    }

    "remove participant of more than one participant room" in {
      val p1 = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val p2 = ConferenceParticipant(
        "4000",
        2,
        "SIP/efgh-00002",
        CallerId("Some Two", "1002"),
        DateTime.now
      )
      val p3 = ConferenceParticipant(
        "4000",
        3,
        "SIP/ijkl-00003",
        CallerId("Some Three", "1003"),
        DateTime.now
      )
      val nonEmptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceBusy,
        Some(DateTime.now.minusMinutes(10)),
        List(p1, p2, p3),
        "default"
      )
      val resultRoom = nonEmptyRoom.removeParticipant(p2)

      resultRoom.status shouldBe ConferenceBusy
      resultRoom.startTime shouldBe nonEmptyRoom.startTime
      resultRoom.participants should contain.only(p1, p3)
    }

    "be created from a MeetMeConference" in {
      val meetme = MeetMeConference(
        1,
        1,
        "MySuperConference",
        "4000",
        "default",
        Some("1234"),
        Some("4321")
      )
      val expected = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        Some("1234"),
        Some("4321")
      )

      ConferenceRoom(meetme) shouldBe expected
    }

    "update isTalking property for a participant in a conference" in {
      val p1 = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val p2 = ConferenceParticipant(
        "4000",
        2,
        "SIP/efgh-00002",
        CallerId("Some Two", "1002"),
        DateTime.now
      )
      val p3 = ConferenceParticipant(
        "4000",
        3,
        "SIP/ijkl-00003",
        CallerId("Some Three", "1003"),
        DateTime.now
      )
      val conferenceRoom = ConferenceRoom(
        "4000",
        "My conference",
        ConferenceBusy,
        Some(DateTime.now.minusMinutes(5)),
        List(p1, p2, p3),
        "default"
      )

      val conferenceUpdated =
        conferenceRoom.updateParticipant(p1.index)(_.copy(isTalking = true))

      conferenceUpdated.status shouldBe ConferenceBusy
      conferenceUpdated.participants.size shouldBe 3
      conferenceUpdated.participants.foreach(participant =>
        participant.index match {
          case p1.index =>
            participant.isTalking shouldBe true
            participant.joinTime shouldBe p1.joinTime
          case _ => ()
        }
      )
    }

    "update role property for a participant in a conference" in {
      val p1 = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val p2 = ConferenceParticipant(
        "4000",
        2,
        "SIP/efgh-00002",
        CallerId("Some Two", "1002"),
        DateTime.now
      )
      val p3 = ConferenceParticipant(
        "4000",
        3,
        "SIP/ijkl-00003",
        CallerId("Some Three", "1003"),
        DateTime.now
      )
      val conferenceRoom = ConferenceRoom(
        "4000",
        "My conference",
        ConferenceBusy,
        Some(DateTime.now.minusMinutes(5)),
        List(p1, p2, p3),
        "default"
      )

      val conferenceUpdated =
        conferenceRoom.updateParticipant(p2.index)(_.copy(role = OrganizerRole))
      val p2admin = p2.copy(role = OrganizerRole)

      conferenceUpdated.status shouldBe ConferenceBusy
      conferenceUpdated.participants.size shouldBe 3
      conferenceUpdated.participants should contain.only(p1, p2admin, p3)
    }
  }

  "ConferenceParticipant" should {
    "convert a MeetMeJoinEvent to a ConferenceParticipant" in {
      val evt = new MeetMeJoinEvent(this)
      evt.setCallerIdName("Some One")
      evt.setCallerIdNum("1001")
      evt.setChannel("SIP/abcd-00001")
      evt.setUser(1)

      val p = ConferenceParticipant(evt)

      p.index shouldBe 1
      p.channelName shouldBe "SIP/abcd-00001"
      p.callerId shouldBe CallerId("Some One", "1001")
      p.isTalking shouldBe false
      Seconds.secondsBetween(p.joinTime, DateTime.now).getSeconds should be < 5
    }

    "convert a MeetMeLeaveEvent to a ConferenceParticipant" in {
      val evt = new MeetMeLeaveEvent(this)
      evt.setCallerIdName("Some One")
      evt.setCallerIdNum("1001")
      evt.setChannel("SIP/abcd-00001")
      evt.setUser(1)

      val p = ConferenceParticipant(evt)

      p.index shouldBe 1
      p.channelName shouldBe "SIP/abcd-00001"
      p.callerId shouldBe CallerId("Some One", "1001")
      p.isTalking shouldBe false
      Seconds.secondsBetween(p.joinTime, DateTime.now).getSeconds should be < 5
    }

    "convert a MeetMeTalkingEvent to a ConferenceParticipant" in {
      val evt = new MeetMeTalkingEvent(this)
      evt.setCallerIdName("Some One")
      evt.setCallerIdNum("1001")
      evt.setChannel("SIP/abcd-00001")
      evt.setUser(1)
      evt.setStatus(true)

      val p = ConferenceParticipant(evt)

      p.index shouldBe 1
      p.channelName shouldBe "SIP/abcd-00001"
      p.callerId shouldBe CallerId("Some One", "1001")
      p.isTalking shouldBe false
      Seconds.secondsBetween(p.joinTime, DateTime.now).getSeconds should be < 5
    }
  }

}
