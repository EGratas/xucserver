package services.calltracking

import org.apache.pekko.actor.*
import org.apache.pekko.testkit.*
import org.asteriskjava.manager.action.{StopMonitorAction, UnpauseMonitorAction}
import org.mockito.Mockito.*
import org.mockito.{ArgumentCaptor, Mockito}
import org.scalatest.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import services.XucAmiBus.*
import services.calltracking.AsteriskGraphTracker.{AsteriskPath, LoopDetected, PathsFromChannel}
import services.calltracking.SingleDeviceTracker.*
import services.calltracking.graph.AsteriskObjectHelper
import services.{MediatorWrapper, XucAmiBus, XucEventBus}
import xivo.models.{LocalChannelFeature, MediaServerTrunk, Trunk, XivoFeature}
import xivo.xuc.DeviceTrackerConfig
import xivo.xucami.models.*

import scala.concurrent.duration.*

class SingleDeviceTrackerSpec
  extends TestKit(ActorSystem("SingleDeviceTracker"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AsteriskObjectHelper {

  import xivo.models.LineHelper.makeLine

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val defaultConfig: DeviceTrackerConfig = new DeviceTrackerConfig {
    def stopRecordingUponExternalXfer: Boolean = true
    def enableRecordingRules: Boolean          = true
  }

  class DeviceActorWrapper(
                            val feature: XivoFeature,
                            stopRecording: Boolean,
                            enableRules: Boolean
                          ) {
    val channelTracker: TestProbe = TestProbe()
    val graphTracker: TestProbe = TestProbe()
    val bus: XucEventBus = mock[XucEventBus]
    val amiBus: XucAmiBus = mock[XucAmiBus]
    val parent: TestProbe = TestProbe()
    val configDispatcher: TestProbe = TestProbe()
    val mediatorWrapper  = new MediatorWrapper

    val deviceTrackerConfig: DeviceTrackerConfig = new DeviceTrackerConfig {
      def stopRecordingUponExternalXfer: Boolean = stopRecording
      def enableRecordingRules: Boolean          = enableRules
    }

    val factory = new DeviceActorFactoryImpl(
      channelTracker.ref,
      graphTracker.ref,
      bus,
      amiBus,
      deviceTrackerConfig,
      configDispatcher.ref,
      mediatorWrapper
    )

    val device: TestActorRef[SingleDeviceTracker] = TestActorRef[SingleDeviceTracker](
      factory.props(feature),
      parent.ref,
      "MySDTActor"
    )
  }

  def deviceWrapper(
                     feature: XivoFeature,
                     stopRecording: Boolean = true,
                     enableRules: Boolean = true
                   ) = new DeviceActorWrapper(feature, stopRecording, enableRules)

  "TrunkDeviceTracker" should {
    "have the tracker type TrunkDeviceTracker" in {
      val trunk   = mock[Trunk]
      val wrapper = deviceWrapper(trunk)

      wrapper.device.underlyingActor.deviceTrackerType should be(
        TrunkDeviceTrackerType
      )
    }
  }

  "UnknownDeviceTracker" should {
    "have the tracker type UnknownDeviceTracker" in {
      val unknown = mock[XivoFeature]
      val wrapper = deviceWrapper(unknown)

      wrapper.device.underlyingActor.deviceTrackerType should be(
        UnknownDeviceTrackerType
      )
    }
  }

  "MediaServerTrunkDeviceTracker" should {
    "have the tracker type MediaServerTrunkDeviceTrackerType" in {
      val mdsTrunk = MediaServerTrunk("from-mds1", SipDriver.SIP)
      val wrapper  = deviceWrapper(mdsTrunk)

      wrapper.device.underlyingActor.deviceTrackerType should be(
        MediaServerTrunkDeviceTrackerType
      )
    }
  }

  "SingleDeviceTracker" should {
    import BaseTracker.*

    "watch graph for its interface for trunks" in {
      val wrapper =
        deviceWrapper(Trunk(1, "default", "sip", "trunk-test", SipDriver.SIP))
      wrapper.graphTracker.expectMsg(WatchChannelStartingWith("SIP/trunk-test"))
    }

    "watch channels for its interface for SIP trunks" in {
      val wrapper =
        deviceWrapper(Trunk(1, "default", "sip", "trunk-test", SipDriver.SIP))
      wrapper.channelTracker.expectMsg(
        WatchChannelStartingWith("SIP/trunk-test")
      )
    }

    "watch channels for its interface for PJSIP trunks" in {
      val wrapper =
        deviceWrapper(Trunk(1, "default", "sip", "trunk-test", SipDriver.PJSIP))
      wrapper.channelTracker.expectMsg(
        WatchChannelStartingWith("PJSIP/trunk-test")
      )
    }

    "get tracked calls" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! c
      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(DeviceCall(cname, Some(c), Set.empty, Map.empty))
        )
      )
    }

    "should not list hungup calls" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HUNGUP
      )

      wrapper.device ! c
      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(SingleDeviceTracker.Calls(List.empty))
    }

    "schedule call removal 2 seconds after hangup" in {
      val mockScheduler       = mock[Scheduler]
      val channelTracker      = TestProbe()
      val graphTracker        = TestProbe()
      val bus                 = mock[XucEventBus]
      val amiBus              = mock[XucAmiBus]
      val deviceTrackerConfig = mock[DeviceTrackerConfig]
      val mediator            = mock[MediatorWrapper]

      val device = TestActorRef(
        new UnknownDeviceTracker(
          "SIP/abcd",
          channelTracker.ref,
          graphTracker.ref,
          amiBus,
          deviceTrackerConfig,
          mediator
        ) {
          override def scheduler: Scheduler = mockScheduler
        }
      )

      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HUNGUP
      )

      device ! c

      verify(mockScheduler, timeout(500)).scheduleOnce(
        2.seconds,
        device,
        device.underlyingActor.RemoveChannel(cname)
      )(scala.concurrent.ExecutionContext.Implicits.global, device)
    }

    "track remote party information" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname             = "SIP/abcd-00000001"
      val remoteCname       = "SIP/efgh-0000001"
      val sourceTrackerType = SipDeviceTrackerType
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val remoteChannel = Channel(
        "123456789.124",
        remoteCname,
        CallerId("Jason Bourne", "1666"),
        "",
        ChannelState.RINGING
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        remoteChannel,
        sourceTrackerType
      )

      wrapper.device ! channel
      wrapper.device ! party
      wrapper.device ! pfc

      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(
            DeviceCall(
              cname,
              Some(channel),
              pfc.paths,
              Map(remoteChannel.name -> remoteChannel)
            )
          )
        )
      )
    }

    "track remote party information (alternative message order)" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname             = "SIP/abcd-00000001"
      val remoteCname       = "SIP/efgh-0000001"
      val sourceTrackerType = SipDeviceTrackerType
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val remoteChannel = Channel(
        "123456789.124",
        remoteCname,
        CallerId("Jason Bourne", "1666"),
        "",
        ChannelState.RINGING
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        remoteChannel,
        sourceTrackerType
      )

      wrapper.device ! channel
      wrapper.device ! pfc
      wrapper.device ! party

      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(
            DeviceCall(
              cname,
              Some(channel),
              pfc.paths,
              Map(remoteChannel.name -> remoteChannel)
            )
          )
        )
      )
    }

    "ensure remote party information are sent when receiving channel and then path" in {
      val line =
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      val wrapper = deviceWrapper(line)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val cname       = "SIP/abcd-00000001"
      val remoteCname = "SIP/efgh-0000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )

      wrapper.device ! channel
      wrapper.device ! pfc

      val expectedTrackerType = SipDeviceTrackerType
      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        channel,
        expectedTrackerType
      )

      wrapper.parent.expectMsg(expected)
    }

    "ensure remote party information are NOT sent to local channels when receiving channel and then path" in {
      val line =
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      val wrapper = deviceWrapper(line)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val cname             = "SIP/abcd-00000001"
      val remoteCname       = "Local/1234@default-00000002"
      val sourceTrackerType = SipDeviceTrackerType
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeLocalChannel(remoteCname)))
      )

      wrapper.device ! channel
      wrapper.device ! pfc

      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        channel,
        sourceTrackerType
      )

      wrapper.parent.expectNoMessage(100.millis)
    }

    "ensure remote party information are sent when receiving path and then channel" in {
      val line =
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      val wrapper = deviceWrapper(line)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val cname       = "SIP/abcd-00000001"
      val remoteCname = "SIP/efgh-0000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! channel

      val expectedSourceTrackerType = SipDeviceTrackerType
      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        channel,
        expectedSourceTrackerType
      )

      wrapper.parent.expectMsg(expected)
    }

    "ensure remote party information contains correct source SIP device tracker type" in {
      val trunkName = "trunk-test"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val trunkCName = "SIP/" + trunkName + "-00000001"
      val trunkChannel = Channel(
        "123456789.123",
        trunkCName,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      val remoteCname = "SIP/efgh-0000001"
      val pfc = PathsFromChannel(
        NodeChannel(trunkCName),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! trunkChannel

      val expectedSourceTrackerType = TrunkDeviceTrackerType
      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        trunkChannel,
        expectedSourceTrackerType
      )

      wrapper.parent.expectMsg(expected)
    }

    "ensure remote party information contains correct source PJSIP device tracker type" in {
      val trunkName = "trunk-test"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.PJSIP)
      val wrapper   = deviceWrapper(trunk)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val trunkCName = "PJSIP/" + trunkName + "-00000001"
      val trunkChannel = Channel(
        "123456789.123",
        trunkCName,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      val remoteCname = "PJSIP/efgh-0000001"
      val pfc = PathsFromChannel(
        NodeChannel(trunkCName),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! trunkChannel

      val expectedSourceTrackerType = TrunkDeviceTrackerType
      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        trunkChannel,
        expectedSourceTrackerType
      )

      wrapper.parent.expectMsg(expected)
    }

    "ensure remote party information are NOT sent to local channel when receiving path and then channel" in {
      val line =
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      val wrapper = deviceWrapper(line)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val cname       = "SIP/abcd-00000001"
      val remoteCname = "Local/1234@default-00000002"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeLocalChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! channel

      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        channel,
        SipDeviceTrackerType
      )

      wrapper.parent.expectNoMessage(100.millis)
    }

    "stop recording if a recorded channel is bridged to another TrunkDeviceTracker" in {
      /* Here we simulate the case where
       - a recorded channel from a trunk (followed by a TrunkDeviceTracker)
       - is bridged with another channel from a trunk (followed by a TrunkDeviceTracker)
       => Recording should be stopped
       */

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val recordedChannelName = "SIP/" + trunkName + "-00000001"
      val recordedChannel = Channel(
        "1522138782.9",
        recordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        MonitorState.ACTIVE
      )

      val remoteChannelName = "SIP/" + trunkName + "-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP
      )

      val pfc = PathsFromChannel(
        NodeChannel(recordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! recordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        recordedChannelName,
        remoteChannel,
        TrunkDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])

      verify(wrapper.amiBus).publish(arg.capture)

      arg.getValue.message match {
        case stop: StopMonitorAction =>
          stop.getChannel shouldBe recordedChannelName
        case any =>
          fail(
            s"Should get StopMonitorAction on channel $recordedChannelName, got: $any"
          )
      }
    }

    "not stop recording of remote recorded channel" in {
      /* Here we simulate the case where
       - a channel from a trunk (followed by a TrunkDeviceTracker)
       - is bridged with another channel from a trunk which is recorded (followed by a TrunkDeviceTracker)
       => Recording should not be stopped here (we should not stop the remote channel)
          the recording will be stopped when the recorded channel receives the partyinformation
       */

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val channelName = "SIP/" + trunkName + "-00000001"
      val channel = Channel(
        "1522138782.9",
        channelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP
      )

      val remoteRecordedChannelName = "SIP/" + trunkName + "-000000a1"
      val remoteRecordedChannel = Channel(
        "1522140032.14",
        remoteRecordedChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP,
        MonitorState.ACTIVE
      )

      val pfc = PathsFromChannel(
        NodeChannel(channelName),
        Set(AsteriskPath(NodeChannel(remoteRecordedChannelName)))
      )

      wrapper.device ! channel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        channelName,
        remoteRecordedChannel,
        TrunkDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "not stop recording if both party are not a TrunkDeviceTracker" in {
      /* Here, we simulate the case where
       - a recorded channel from a trunk (followed by a TrunkDeviceTracker)
       - is bridged with an internal channel (followed by a SipDeviceTracker)
       Therefore recording should not be stopped
       */

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val recordedChannelName = "SIP/" + trunkName + "-00000001"
      val recordedChannel = Channel(
        "1522138782.9",
        recordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        MonitorState.ACTIVE
      )

      val remoteChannelName = "SIP/ml9k87j-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("Agent Secret", "8002"),
        "",
        ChannelState.UP
      )

      val pfcBridgedWithLine = PathsFromChannel(
        NodeChannel(recordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! recordedChannel
      wrapper.device ! pfcBridgedWithLine

      val partyIsLine = SingleDeviceTracker.PartyInformation(
        recordedChannelName,
        remoteChannel,
        SipDeviceTrackerType
      )

      wrapper.device ! partyIsLine

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "not stop recording if channel is not recorded" in {
      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val notRecordedChannelName = "SIP/" + trunkName + "-00000001"
      val notRecordedChannel = Channel(
        "1522138782.9",
        notRecordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP
      )

      val remoteChannelName = "SIP/" + trunkName + "-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP
      )

      val pfc = PathsFromChannel(
        NodeChannel(notRecordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! notRecordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        notRecordedChannelName,
        remoteChannel,
        TrunkDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "not stop recording if option is deactivated" in {
      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk, stopRecording = false)

      val recordedChannelName = "SIP/" + trunkName + "-00000001"
      val recordedChannel = Channel(
        "1522138782.9",
        recordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        MonitorState.ACTIVE
      )

      val remoteChannelName = "SIP/" + trunkName + "-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP
      )

      val pfc = PathsFromChannel(
        NodeChannel(recordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! recordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        recordedChannelName,
        remoteChannel,
        TrunkDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    /* Toggle recording
        Case 1: channel is being recorded
          Given the device call channel's **monitor state** (which is ACTIVE)
          Stop or Not the recording according to the **recording mode** of the end channel

        - XXX: the code written may conflict with the code in onPartyInformation
         in the case of two TrunkDeviceTracker
     */
    "if channel is being recorded, do nothing about recording if end channel is in mode recorded or recordedondemand" in {
      val monitorState   = MonitorState.ACTIVE
      val recordingModes = Vector("recorded", "recordedondemand")
      //val expectedAMIAction = None

      recordingModes.foreach(recordingMode => {
        val trunkName = "trunk-provider"
        val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
        val wrapper   = deviceWrapper(trunk)

        val recordedChannelName = "SIP/" + trunkName + "-00000001"
        val recordedChannel = Channel(
          "1522138782.9",
          recordedChannelName,
          CallerId("0123456789", "0123456789"),
          "",
          ChannelState.UP,
          monitorState
        )

        val remoteChannelName = "SIP/ml9k87j-000000a1"
        val remoteChannel = Channel(
          "1522140032.14",
          remoteChannelName,
          CallerId("", ""),
          "",
          ChannelState.UP
        )
          .updateVariable(Channel.VarNames.RecordingMode, recordingMode)

        val pfc = PathsFromChannel(
          NodeChannel(recordedChannelName),
          Set(AsteriskPath(NodeChannel(remoteChannelName)))
        )

        wrapper.device ! recordedChannel
        wrapper.device ! pfc

        val party = SingleDeviceTracker.PartyInformation(
          recordedChannelName,
          remoteChannel,
          SipDeviceTrackerType
        )

        wrapper.device ! party

        val arg = ArgumentCaptor.forClass(classOf[AmiAction])
        verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
      })
    }

    "if channel is being recorded, stop recording if end channel is in mode notrecorded" in {
      val monitorState  = MonitorState.ACTIVE
      val recordingMode = "notrecorded"
      //val expectedAMIAction = StopMonitorAction

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val recordedChannelName = "SIP/" + trunkName + "-00000001"
      val recordedChannel = Channel(
        "1522138782.9",
        recordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        monitorState
      )

      val remoteChannelName = "SIP/ml9k87j-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP
      )
        .updateVariable(Channel.VarNames.RecordingMode, recordingMode)

      val pfc = PathsFromChannel(
        NodeChannel(recordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! recordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        recordedChannelName,
        remoteChannel,
        SipDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])

      verify(wrapper.amiBus).publish(arg.capture)

      arg.getValue.message match {
        case stop: StopMonitorAction =>
          stop.getChannel shouldBe recordedChannelName
        case any =>
          fail(
            s"Should get StopMonitorAction on channel $recordedChannelName, got: $any"
          )
      }
    }

    "if channel is being recorded, stop recording if end channel is in mode notrecorded, only if rules enabled" in {
      val monitorState  = MonitorState.ACTIVE
      val recordingMode = "notrecorded"
      //val expectedAMIAction = StopMonitorAction

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk, enableRules = false)

      val recordedChannelName = "SIP/" + trunkName + "-00000001"
      val recordedChannel = Channel(
        "1522138782.9",
        recordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        monitorState
      )

      val remoteChannelName = "SIP/ml9k87j-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP
      )
        .updateVariable(Channel.VarNames.RecordingMode, recordingMode)

      val pfc = PathsFromChannel(
        NodeChannel(recordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! recordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        recordedChannelName,
        remoteChannel,
        SipDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    /* Toggle recording
        Case 2: channel is being recorded on pause
          Given the device call channel's **monitor state** (which is PAUSED)
          Unpause or Stop the recording according to the **recording mode** of the end channel
     */
    "if channel is being recorded on pause, unpause recording if end channel is in recorded mode" in {
      val monitorState  = MonitorState.PAUSED
      val recordingMode = "recorded"
      //val expectedAMIAction = UnPauseMonitorAction

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val recordedChannelName = "SIP/" + trunkName + "-00000001"
      val recordedChannel = Channel(
        "1522138782.9",
        recordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        monitorState
      )

      val remoteChannelName = "SIP/ml9k87j-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP
      )
        .updateVariable(Channel.VarNames.RecordingMode, recordingMode)

      val pfc = PathsFromChannel(
        NodeChannel(recordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! recordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        recordedChannelName,
        remoteChannel,
        SipDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus).publish(arg.capture)

      arg.getValue.message match {
        case unpause: UnpauseMonitorAction =>
          unpause.getChannel shouldBe recordedChannelName
        case any =>
          fail(
            s"Should get UnpauseMonitorAction on channel $recordedChannelName, got: $any"
          )
      }
    }

    "if channel is being recorded on pause, do nothing about recording if end channel is in recordedondemand mode" in {
      val monitorState  = MonitorState.PAUSED
      val recordingMode = "recordedondemand"
      //val expectedAMIAction = None

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val recordedChannelName = "SIP/" + trunkName + "-00000001"
      val recordedChannel = Channel(
        "1522138782.9",
        recordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        monitorState
      )

      val remoteChannelName = "SIP/ml9k87j-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP
      )
        .updateVariable(Channel.VarNames.RecordingMode, recordingMode)

      val pfc = PathsFromChannel(
        NodeChannel(recordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! recordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        recordedChannelName,
        remoteChannel,
        SipDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "if channel is being recorded on pause, stop recording if end channel is in notrecorded mode" in {
      val monitorState  = MonitorState.PAUSED
      val recordingMode = "notrecorded"
      //val expectedAMIAction = StopMonitorAction

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val recordedChannelName = "SIP/" + trunkName + "-00000001"
      val recordedChannel = Channel(
        "1522138782.9",
        recordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        monitorState
      )

      val remoteChannelName = "SIP/ml9k87j-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP
      )
        .updateVariable(Channel.VarNames.RecordingMode, recordingMode)

      val pfc = PathsFromChannel(
        NodeChannel(recordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! recordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        recordedChannelName,
        remoteChannel,
        SipDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus).publish(arg.capture)

      arg.getValue.message match {
        case stop: StopMonitorAction =>
          stop.getChannel shouldBe recordedChannelName
        case any =>
          fail(
            s"Should get StopMonitorAction on channel $recordedChannelName, got: $any"
          )
      }
    }

    "if channel is being recorded on pause, stop recording if end channel is in notrecorded mode, only if rules enabled" in {
      val monitorState  = MonitorState.PAUSED
      val recordingMode = "notrecorded"
      //val expectedAMIAction = StopMonitorAction

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk, enableRules = false)

      val recordedChannelName = "SIP/" + trunkName + "-00000001"
      val recordedChannel = Channel(
        "1522138782.9",
        recordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        monitorState
      )

      val remoteChannelName = "SIP/ml9k87j-000000a1"
      val remoteChannel = Channel(
        "1522140032.14",
        remoteChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP
      )
        .updateVariable(Channel.VarNames.RecordingMode, recordingMode)

      val pfc = PathsFromChannel(
        NodeChannel(recordedChannelName),
        Set(AsteriskPath(NodeChannel(remoteChannelName)))
      )

      wrapper.device ! recordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        recordedChannelName,
        remoteChannel,
        SipDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    /* Toggle recording
      Case 3: channel is not being recorded
       Given the device call channel's **monitor state** (which is DISABLED)
       AND given the device call channel's **recording mode** (which is notrecorded)
       Stop the recording on remote channel if it is activted
     */
    "if channel is not being recorded and is in notrecorded mode, stop recording of the given remotechannel if it is being recorded (whatever its recording mode)" in {
      val monitorState = MonitorState.DISABLED
      val recMode      = "notrecorded"

      val remoteRecordingModes = Vector("recorded", "recordedondemand")
      val remoteChanMonitorStates =
        Vector(MonitorState.ACTIVE, MonitorState.PAUSED)
      //val expectedAMIAction = None

      remoteRecordingModes.foreach(remoteRecordingMode => {
        remoteChanMonitorStates.foreach(remoteChanMonitorState => {
          val trunkName = "trunk-provider"
          val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
          val wrapper   = deviceWrapper(trunk)

          val notRecordedChannelName = "SIP/" + trunkName + "-00000001"
          val notRecordedChannel = Channel(
            "1522138782.9",
            notRecordedChannelName,
            CallerId("0123456789", "0123456789"),
            "",
            ChannelState.UP,
            monitorState
          )
            .updateVariable(Channel.VarNames.RecordingMode, recMode)

          val startNotRecordedChannelName = "Local/3000@default-000000a1;1"
          val startNotRecordedChannel = Channel(
            "1522140030.12",
            startNotRecordedChannelName,
            CallerId("", ""),
            "",
            ChannelState.UP
          )

          val remoteRecordedChannelName = "Local/3000@default-000000a1;2"
          val remoteRecordedChannel = Channel(
            "1522140030.14",
            remoteRecordedChannelName,
            CallerId("", ""),
            "",
            ChannelState.UP,
            remoteChanMonitorState
          )
            .updateVariable(Channel.VarNames.RecordingMode, remoteRecordingMode)

          val endNotRecordedChannelName =
            "Local/id-118@agentcallback-000000a1;1"
          val endNotRecordedChannel = Channel(
            "1522140032.12",
            endNotRecordedChannelName,
            CallerId("", ""),
            "",
            ChannelState.UP
          )
            .updateVariable(Channel.VarNames.RecordingMode, remoteRecordingMode)

          val pfc = PathsFromChannel(
            NodeChannel(notRecordedChannelName),
            Set(
              AsteriskPath(
                NodeChannel(startNotRecordedChannelName),
                NodeChannel(remoteRecordedChannelName),
                NodeChannel(endNotRecordedChannelName)
              )
            )
          )

          wrapper.device ! notRecordedChannel
          wrapper.device ! pfc

          val endParty = SingleDeviceTracker.PartyInformation(
            notRecordedChannelName,
            endNotRecordedChannel,
            UnknownDeviceTrackerType
          )
          wrapper.device ! endParty

          val arg1 = ArgumentCaptor.forClass(classOf[AmiAction])
          verify(wrapper.amiBus, Mockito.after(500).never())
            .publish(arg1.capture)

          val recParty = SingleDeviceTracker.PartyInformation(
            notRecordedChannelName,
            remoteRecordedChannel,
            UnknownDeviceTrackerType
          )
          wrapper.device ! recParty

          val arg2 = ArgumentCaptor.forClass(classOf[AmiAction])
          verify(wrapper.amiBus).publish(arg2.capture)

          arg2.getValue.message match {
            case stop: StopMonitorAction =>
              stop.getChannel shouldBe remoteRecordedChannelName
            case any =>
              fail(
                s"Should get StopMonitorAction on channel $remoteRecordedChannelName, got: $any"
              )
          }

          reset(wrapper.amiBus)
          val startParty = SingleDeviceTracker.PartyInformation(
            notRecordedChannelName,
            startNotRecordedChannel,
            UnknownDeviceTrackerType
          )
          wrapper.device ! startParty

          val arg3 = ArgumentCaptor.forClass(classOf[AmiAction])
          verify(wrapper.amiBus, Mockito.after(500).never())
            .publish(arg3.capture)
        })
      })
    }

    "if channel is not being recorded and is in notrecorded mode, do nothing if no channel in call is not being recorded" in {
      val monitorState = MonitorState.DISABLED
      val recMode      = "notrecorded"

      val remoteRecordingMode    = "notrecorded"
      val remoteChanMonitorState = MonitorState.DISABLED
      //val expectedAMIAction = None

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val notRecordedChannelName = "SIP/" + trunkName + "-00000001"
      val notRecordedChannel = Channel(
        "1522138782.9",
        notRecordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        monitorState
      )
        .updateVariable(Channel.VarNames.RecordingMode, recMode)

      val remoteNotRecordedChannelName = "Local/3000@default-000000a1;2"
      val remoteNotRecordedChannel = Channel(
        "1522140032.14",
        remoteNotRecordedChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP,
        remoteChanMonitorState
      )
        .updateVariable(Channel.VarNames.RecordingMode, remoteRecordingMode)

      val pfc = PathsFromChannel(
        NodeChannel(notRecordedChannelName),
        Set(
          AsteriskPath(
            NodeChannel("Local/3000@default-000000a1;1"),
            NodeChannel(remoteNotRecordedChannelName),
            NodeChannel("Local/id-118@agentcallback-000000a1;")
          )
        )
      )

      wrapper.device ! notRecordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        notRecordedChannelName,
        remoteNotRecordedChannel,
        UnknownDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "if channel is not being recorded but has not its mode in notrecorded, do nothing" in {
      val monitorState = MonitorState.DISABLED
      val recMode      = "something"

      val remoteChanMonitorState = MonitorState.ACTIVE
      val remoteRecordingMode    = "recorded"
      //val expectedAMIAction = None

      val trunkName = "trunk-provider"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      val notRecordedChannelName = "SIP/" + trunkName + "-00000001"
      val notRecordedChannel = Channel(
        "1522138782.9",
        notRecordedChannelName,
        CallerId("0123456789", "0123456789"),
        "",
        ChannelState.UP,
        monitorState
      )
        .updateVariable(Channel.VarNames.RecordingMode, recMode)

      val remoteRecordedChannelName = "Local/3000@default-000000a1;2"
      val remoteRecordedChannel = Channel(
        "1522140032.14",
        remoteRecordedChannelName,
        CallerId("", ""),
        "",
        ChannelState.UP,
        remoteChanMonitorState
      )
        .updateVariable(Channel.VarNames.RecordingMode, remoteRecordingMode)

      val pfc = PathsFromChannel(
        NodeChannel(notRecordedChannelName),
        Set(
          AsteriskPath(
            NodeChannel("Local/3000@default-000000a1;1"),
            NodeChannel(remoteRecordedChannelName),
            NodeChannel("Local/id-118@agentcallback-000000a1;")
          )
        )
      )

      wrapper.device ! notRecordedChannel
      wrapper.device ! pfc

      val party = SingleDeviceTracker.PartyInformation(
        notRecordedChannelName,
        remoteRecordedChannel,
        UnknownDeviceTrackerType
      )

      wrapper.device ! party

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "really remove call when asked to" in {
      val unknown = mock[XivoFeature]
      val wrapper = deviceWrapper(unknown)

      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! c
      wrapper.device ! wrapper.device.underlyingActor.RemoveChannel(cname)

      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(SingleDeviceTracker.Calls(List.empty))
    }

    "send initial call state when actor wants to be notified of calls" in {
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        None,
        "123.123.123.1",
        number = Some("1007")
      )
      val wrapper = deviceWrapper(line)

      wrapper.device ! SingleDeviceTracker.MonitorCalls(line.trackingInterface)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))
    }

    "allow other actors to monitor calls for a device" in {
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        None,
        "123.123.123.1",
        number = Some("1007")
      )
      val wrapper = deviceWrapper(line)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )
      val calls = DeviceCalls(
        line.trackingInterface,
        Map(cname -> DeviceCall(cname, Some(channel), Set.empty, Map.empty))
      )

      wrapper.device ! SingleDeviceTracker.MonitorCalls(line.trackingInterface)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))

      wrapper.device ! channel
      expectMsg(calls)
    }

    "allow other actors to monitor calls for a device excluding hangup calls" in {
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        None,
        "123.123.123.1",
        number = Some("1007")
      )
      val wrapper = deviceWrapper(line)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )
      val calls = DeviceCalls(
        line.trackingInterface,
        Map(cname -> DeviceCall(cname, Some(channel), Set.empty, Map.empty))
      )

      wrapper.device ! SingleDeviceTracker.MonitorCalls(line.trackingInterface)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))

      wrapper.device ! channel
      expectMsg(calls)

      wrapper.device ! channel.copy(state = ChannelState.HUNGUP)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))
    }

    "allow other actors to unmonitor calls for a device" in {
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        None,
        "123.123.123.1",
        number = Some("1007")
      )
      val wrapper = deviceWrapper(line)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )
      val calls = DeviceCalls(
        line.trackingInterface,
        Map(cname -> DeviceCall(cname, Some(channel), Set.empty, Map.empty))
      )

      wrapper.device ! SingleDeviceTracker.MonitorCalls(line.trackingInterface)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))
      wrapper.device ! SingleDeviceTracker.UnMonitorCalls(
        line.trackingInterface
      )
      wrapper.device ! channel

      expectNoMessage(100.millis)
    }

    "drop calls concerning a loop after it's been detected" in {
      val wrapper = deviceWrapper(LocalChannelFeature)
      val cname   = "Local/1002@default-00000047;2"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      wrapper.device ! channel

      wrapper.device ! GetCalls
      expectMsg(Calls(List(call)))

      wrapper.device ! LoopDetected(Set(NodeLocalChannel(cname)))

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))
    }

    "ignore Channel events concerning a loop after it's been detected" in {
      val wrapper = deviceWrapper(LocalChannelFeature)
      val cname   = "Local/1002@default-00000047;2"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      wrapper.device ! channel
      wrapper.device ! LoopDetected(Set(NodeLocalChannel(cname)))

      wrapper.device ! channel

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))

    }

    "ignore PathsFromChannel events concerning a loop after it's been detected" in {
      val wrapper = deviceWrapper(LocalChannelFeature)
      val cname   = "Local/1002@default-00000047;2"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      wrapper.device ! channel
      wrapper.device ! LoopDetected(Set(NodeLocalChannel(cname)))

      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel("SIP/abcd-00001")))
      )

      wrapper.device ! pfc

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))

    }

    "ignore PartyInformation events concerning a loop after it's been detected" in {
      val wrapper = deviceWrapper(LocalChannelFeature)
      val cname   = "Local/1002@default-00000047;2"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      wrapper.device ! channel
      wrapper.device ! LoopDetected(Set(NodeLocalChannel(cname)))

      val remoteChannel = Channel(
        "123456789.124",
        "SIP/abcd-000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        remoteChannel,
        UnknownDeviceTrackerType
      )

      wrapper.device ! party

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))

    }

    "schedule looped channel removal 30 seconds after detection" in {
      val mockScheduler  = mock[Scheduler]
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()
      val bus            = mock[XucEventBus]
      val amiBus         = mock[XucAmiBus]
      val mediator       = mock[MediatorWrapper]

      val device = TestActorRef(
        new UnknownDeviceTracker(
          "Local/",
          channelTracker.ref,
          graphTracker.ref,
          amiBus,
          defaultConfig,
          mediator
        ) {
          override def scheduler: Scheduler = mockScheduler
        }
      )

      val cname1 = "Local/1002@default-00000047;1"
      val cname2 = "Local/1002@default-00000047;2"

      device ! LoopDetected(
        Set(NodeLocalChannel(cname1), NodeLocalChannel(cname2))
      )

      verify(mockScheduler, timeout(500)).scheduleOnce(
        30.seconds,
        device,
        device.underlyingActor.RemoveLoopChannel(cname1)
      )(scala.concurrent.ExecutionContext.Implicits.global, device)
      verify(mockScheduler, timeout(500)).scheduleOnce(
        30.seconds,
        device,
        device.underlyingActor.RemoveLoopChannel(cname2)
      )(scala.concurrent.ExecutionContext.Implicits.global, device)
    }

    "ensure loop block list is cleaned after receiving RemoveLoopChannel" in {
      val mockScheduler  = mock[Scheduler]
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()
      val bus            = mock[XucEventBus]
      val amiBus         = mock[XucAmiBus]
      val mediator       = mock[MediatorWrapper]

      val device = TestActorRef(
        new UnknownDeviceTracker(
          "Local/",
          channelTracker.ref,
          graphTracker.ref,
          amiBus,
          defaultConfig,
          mediator
        ) {
          override def scheduler = mockScheduler
        }
      )

      val cname = "Local/1002@default-00000047;1"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      device ! LoopDetected(Set(NodeLocalChannel(cname)))
      device ! device.underlyingActor.RemoveLoopChannel(cname)

      device ! channel

      device ! GetCalls
      expectMsg(Calls(List(call)))
    }
  }

  "DeviceMessageWithInterface trait" should {

    case class DummyDM(interface: DeviceMessage.DeviceInterface)
      extends DeviceMessageWithInterface

    "compare inner interface with interface with trailing dash" in {
      DummyDM("SIP/abcdefgh").isFor("SIP/abcdefgh-") should be(true)
    }

    "compare inner interface with interface without trailing dash" in {
      DummyDM("SIP/abcdefgh").isFor("SIP/abcdefgh") should be(true)
    }

    "compare inner interface with trailing dash with interface with trailing dash" in {
      DummyDM("SIP/abcdefgh-").isFor("SIP/abcdefgh-") should be(true)
    }

    "compare inner interface with trailing dash with interface without trailing dash" in {
      DummyDM("SIP/abcdefgh-").isFor("SIP/abcdefgh") should be(true)
    }

  }
}
