package services.calltracking

import org.scalatest._
import org.joda.time.DateTime
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.graph.NodeLocalChannel
import org.apache.pekko.actor._
import org.apache.pekko.testkit._
import services.calltracking.BaseTracker._
import scala.concurrent.duration._
import xivo.xucami.models.{
  CallerId,
  Channel,
  ChannelState,
  EnterQueue,
  QueueCall
}
import xivo.phonedevices.QueueLogEventCommand
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class MonitorTransferredChannelSpec
    extends TestKit(ActorSystem("MonitorTransferredChannel"))
    with AnyWordSpecLike
    with Matchers
    with ImplicitSender
    with BeforeAndAfterAll
    with MockitoSugar {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  abstract class MonitorTestActor {
    val mockScheduler: Scheduler        = mock[Scheduler]
    val channelTracker: TestProbe       = TestProbe()
    val amiBus: TestProbe               = TestProbe()
    val channel: NodeLocalChannel              = NodeLocalChannel("Local/1234@default-0001", "main")
    val channelId            = "123456789.001"
    val transferredChannelId = "123456789.456"
    val actor: ActorRef = system.actorOf(
      Props(
        new MonitorTransferredChannel(
          channelTracker.ref,
          amiBus.ref,
          channel,
          transferredChannelId
        ) {
          override def scheduler: Scheduler = mockScheduler
        }
      )
    )

    def mkChannel(
        state: ChannelState.ChannelState,
        queue: Option[String] = None
    ): Channel = {
      val c = Channel(
        channelId,
        channel.name,
        CallerId("Name", "1234"),
        "123456789.666",
        state
      )
      queue
        .map(queueName =>
          c.withQueueHistory(
            EnterQueue(
              queueName,
              channelId,
              QueueCall(
                1,
                Some("Name"),
                "1234",
                DateTime.now,
                channelId,
                "main"
              ),
              channel.name
            )
          )
        )
        .getOrElse(c)
    }
  }

  "MonitorTransferredchannel" should {
    "subscribe to channel state of given channel" in new MonitorTestActor {
      channelTracker.expectMsg(WatchChannelStartingWith(channel.name))
    }

    "unsubscribe to channel state of given channel when finishing" in new MonitorTestActor {
      channelTracker.expectMsg(WatchChannelStartingWith(channel.name))
      actor ! PoisonPill
      channelTracker.expectMsg(UnWatchChannelStartingWith(channel.name))
    }

    "schedule a timeout upon startup" in new MonitorTestActor {
      verify(mockScheduler, timeout(500)).scheduleOnce(
        3.minute,
        actor,
        MonitorTransferredChannel.MonitorTimeout
      )(system.dispatcher)
    }

    "send QueueLogEvent when channel enters a queue and stop" in new MonitorTestActor {
      val ch: Channel = mkChannel(ChannelState.UP, Some("queue"))
      watch(actor)
      actor ! ch
      amiBus.expectMsg(
        QueueLogEventCommand(
          "queue",
          QueueLogEventCommand.QueueLogTransferEvent,
          ch.id,
          transferredChannelId
        )
      )
      expectTerminated(actor)
    }

    "stop itself when channel is hunged up" in new MonitorTestActor {
      val ch: Channel = mkChannel(ChannelState.HUNGUP)
      watch(actor)
      actor ! ch
      expectTerminated(actor)
    }

    "stop itself when timeout occurs" in new MonitorTestActor {
      watch(actor)
      actor ! MonitorTransferredChannel.MonitorTimeout
      expectTerminated(actor)

    }

  }

}
