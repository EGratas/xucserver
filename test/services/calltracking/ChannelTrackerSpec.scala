package services.calltracking

import org.asteriskjava.manager.action.*
import org.asteriskjava.manager.event.*
import org.asteriskjava.manager.response.{GetVarResponse, ManagerResponse}
import org.scalatest.*
import org.apache.pekko.actor.*
import org.apache.pekko.testkit.*
import org.mockito.Mockito.*
import org.mockito.ArgumentMatchers.*
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.AsteriskGraphTracker.LoopDetected
import services.calltracking.graph.AsteriskObjectHelper
import services.{AmiEventHelper, XucAmiBus}
import services.XucAmiBus.*
import services.ServiceStatus.*
import ChannelTracker.*
import BaseTracker.*
import xivo.xucami.models.*
import xivo.xuc.ChannelTrackerConfig

import scala.concurrent.duration.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import pekkotest.TestKitSpec

class ChannelTrackerSpec
    extends TestKitSpec("ChannelTracker")
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AmiEventHelper
    with AsteriskObjectHelper
    with ChannelHelper {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val defaultConfig: ChannelTrackerConfig = new ChannelTrackerConfig {
    def enableChannelTrackerThrottling: Boolean = false
    def channelTrackerThrottleTimeoutMs: Int    = 0
    def retryGetCurrentChannelsDelay: Int       = 2
  }

  val throttlingConfig: ChannelTrackerConfig = new ChannelTrackerConfig {
    def enableChannelTrackerThrottling: Boolean = true
    def channelTrackerThrottleTimeoutMs: Int    = 5000
    def retryGetCurrentChannelsDelay: Int       = 2
  }

  abstract class TrackerWrapper(config: ChannelTrackerConfig = defaultConfig) {
    val xucAmiBus: XucAmiBus = mock[XucAmiBus]
    val ref: TestActorRef[ChannelTracker]       = TestActorRef(new ChannelTracker(xucAmiBus, config))

    val newChannel     = new NewChannelEvent(this)
    val getVarAction   = new GetVarAction()
    val getVarResponse = new GetVarResponse()
  }

  "ChannelTracker" should {

    "subscribe to AmiEvent through the XucAmiBus" in new TrackerWrapper {
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiEvent)
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.QueueEvent)
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiResponse)
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiService)
    }

    "unsubscribe from AmiEvent through the XucAmiBus" in new TrackerWrapper {
      ref ! PoisonPill
      verify(xucAmiBus, timeout(500)).unsubscribe(ref)

    }

    "request channels upon startup and schedule a new requests" in {

      val mockScheduler = mock[Scheduler]
      val xucAmiBus     = mock[XucAmiBus]
      val ref = TestActorRef(new ChannelTracker(xucAmiBus, defaultConfig) {
        override def scheduler: Scheduler = mockScheduler
      })

      val complete = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete

      verify(xucAmiBus, timeout(500)).publish(
        AmiAction(any[CoreShowChannelsAction](), None, Some(ref))
      )

      verify(mockScheduler, timeout(500)).scheduleOnce(
        defaultConfig.retryGetCurrentChannelsDelay.seconds,
        ref,
        RetryGetCurrentChannels
      )(scala.concurrent.ExecutionContext.Implicits.global, ref)
    }

    "retry to get current channels on retry received case when actor is started before the ami manager actor " in {
      val mockScheduler = mock[Scheduler]
      val xucAmiBus     = mock[XucAmiBus]
      val ref = TestActorRef(new ChannelTracker(xucAmiBus, defaultConfig) {
        override def scheduler: Scheduler = mockScheduler
      })

      val retry = RetryGetCurrentChannels
      ref ! retry

      verify(xucAmiBus, timeout(500).times(2)).publish(
        AmiAction(any[CoreShowChannelsAction](), None, Some(ref))
      )

      verify(mockScheduler, timeout(500).times(2)).scheduleOnce(
        defaultConfig.retryGetCurrentChannelsDelay.seconds,
        ref,
        RetryGetCurrentChannels
      )(scala.concurrent.ExecutionContext.Implicits.global, ref)

    }

    "be in Starting state when starting up" in new TrackerWrapper {
      ref ! GetServiceStatus
      expectMsg(ServiceStarting(ChannelTracker.serviceName, ref))
    }

    "become ready when all channels are loaded" in new TrackerWrapper {
      val response1: AmiEvent = AmiEvent(new CoreShowChannelEvent(this))
      val response2: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! response1
      ref ! response2
      ref ! GetServiceStatus
      expectMsg(ServiceReady(ChannelTracker.serviceName, ref))
    }

    "send channel information when asked to" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)
      ref ! GetChannel("SIP/abcd-00000002")

      expectMsg(Channel(newChannel))
    }

    "store source mds in channel information" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel, "mds-3")
      ref ! GetChannel("SIP/abcd-00000002")

      expectMsg(Channel(newChannel, "mds-3"))
    }

    "send NoSuchChannel when channel is not found" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      ref ! GetChannel("SIP/abcd-00000002")

      expectMsg(NoSuchChannel("SIP/abcd-00000002"))
    }

    "notify when ready after all channels loaded" in new TrackerWrapper {
      val response1: AmiEvent = AmiEvent(new CoreShowChannelEvent(this))
      val response2: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! NotifyWhenServiceReady
      ref ! response1
      ref ! response2

      expectMsg(ServiceReady(ChannelTracker.serviceName, ref))
    }

    "notify channel watchers when channel is created" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      ref ! WatchChannelStartingWith("SIP/abcd")

      newChannel.setUniqueId("123456789.9876")
      newChannel.setChannel("SIP/abcd-00000001")

      ref ! AmiEvent(newChannel)

      expectMsg(Channel(newChannel))
    }

    "notify channel watchers when channel is loaded upon startup" in new TrackerWrapper {
      val coreShowChannelEvent = new CoreShowChannelEvent(this)
      coreShowChannelEvent.setUniqueid("123456789.9876")
      coreShowChannelEvent.setChannel("SIP/abcd-00000001")

      val complete = new CoreShowChannelsCompleteEvent(this)

      ref ! AmiEvent(coreShowChannelEvent)
      ref ! AmiEvent(complete)

      ref ! WatchChannelStartingWith("SIP/abcd")

      expectMsg(Channel(coreShowChannelEvent))
    }

    "notify with latest channel when new watcher added for channel" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      // Send event before watching
      ref ! AmiEvent(newChannel)
      ref ! WatchChannelStartingWith("SIP/abcd")

      // Expect the last state of the given channel
      expectMsg(Channel(newChannel))
    }

    "notify channel watchers when channel is updated" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val newState = new NewStateEvent(this)
      newState.setChannel("SIP/abcd-00000002")
      newState.setChannelState(ChannelState.UP.id)

      ref ! AmiEvent(newState)

      expectMsg(Channel(newChannel).withState(newState))
    }

    "notify channel watchers when monitor is started" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new MonitorStartEvent(this)
      evt.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(evt)

      expectMsg(Channel(newChannel).copy(monitored = MonitorState.ACTIVE))
    }

    "notify channel watchers when monitor is stopped" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new MonitorStartEvent(this)
      evt.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(evt)

      expectMsg(Channel(newChannel).copy(monitored = MonitorState.ACTIVE))

      val evt2 = new MonitorStopEvent(this)
      evt2.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(evt2)

      expectMsg(Channel(newChannel).copy(monitored = MonitorState.DISABLED))
    }

    "notify channel watchers when monitor is paused" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new MonitorStartEvent(this)
      evt.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(evt)

      expectMsg(Channel(newChannel).copy(monitored = MonitorState.ACTIVE))

      val action = new PauseMonitorAction()
      action.setChannel("SIP/abcd-00000002")
      val resp = new ManagerResponse()
      resp.setResponse("Success")

      ref ! AmiResponse((resp, Some(AmiAction(action))))

      expectMsg(Channel(newChannel).copy(monitored = MonitorState.PAUSED))
    }

    "notify channel watchers when monitor is started in pause" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new MonitorStartEvent(this)
      evt.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(evt)

      expectMsg(Channel(newChannel).copy(monitored = MonitorState.ACTIVE))

      val evt2 = new VarSetEvent(this)
      evt2.setChannel("SIP/abcd-00000002")
      evt2.setVariable("__MONITOR_PAUSED")
      evt2.setValue("TRUE")
      val channelWithVar: Channel =
        Channel(newChannel).updateVariable("__MONITOR_PAUSED", "TRUE")

      ref ! AmiEvent(evt2)
      expectMsg(channelWithVar.copy(monitored = MonitorState.PAUSED))
    }

    "notify channel watchers when monitor is started in pause and unpaused" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new MonitorStartEvent(this)
      evt.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(evt)

      expectMsg(Channel(newChannel).copy(monitored = MonitorState.ACTIVE))

      val evt2 = new VarSetEvent(this)
      evt2.setChannel("SIP/abcd-00000002")
      evt2.setVariable("__MONITOR_PAUSED")
      evt2.setValue("TRUE")
      val channelWithVar: Channel =
        Channel(newChannel).updateVariable("__MONITOR_PAUSED", "TRUE")

      ref ! AmiEvent(evt2)
      expectMsg(channelWithVar.copy(monitored = MonitorState.PAUSED))

      val action = new UnpauseMonitorAction()
      action.setChannel("SIP/abcd-00000002")
      val resp = new ManagerResponse()
      resp.setResponse("Success")

      ref ! AmiResponse((resp, Some(AmiAction(action))))

      expectMsg(channelWithVar.copy(monitored = MonitorState.ACTIVE))
    }

    "notify channel watchers when dialstatus is ANSWER and UP" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")
      newChannel.setChannelState(ChannelState.UP.id)

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      getVarAction.setChannel("SIP/abcd-00000002")

      getVarResponse.setResponse("Success")
      getVarResponse.setVariable("DIALSTATUS")
      getVarResponse.setValue("ANSWER")

      ref ! AmiResponse((getVarResponse, Some(AmiAction(getVarAction))))

      expectMsg(
        Channel(newChannel).copy(direction = Some(ChannelDirection.OUTGOING))
      )
    }

    "notify channel watchers when dialstatus is ANSWER and ORIGINATING" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")
      newChannel.setChannelState(ChannelState.ORIGINATING.id)

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      getVarAction.setChannel("SIP/abcd-00000002")

      getVarResponse.setResponse("Success")
      getVarResponse.setVariable("DIALSTATUS")
      getVarResponse.setValue("ANSWER")

      ref ! AmiResponse((getVarResponse, Some(AmiAction(getVarAction))))

      expectMsg(
        Channel(newChannel).copy(direction = Some(ChannelDirection.OUTGOING))
      )
    }

    "notify channel watchers when dialstatus is ANSWER and DIALING" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")
      newChannel.setChannelState(ChannelState.DIALING.id)

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      getVarAction.setChannel("SIP/abcd-00000002")

      getVarResponse.setResponse("Success")
      getVarResponse.setVariable("DIALSTATUS")
      getVarResponse.setValue("ANSWER")

      ref ! AmiResponse((getVarResponse, Some(AmiAction(getVarAction))))

      expectMsg(
        Channel(newChannel).copy(direction = Some(ChannelDirection.OUTGOING))
      )
    }

    "notify channel watchers when dialstatus is ANSWER and RINGING" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")
      newChannel.setChannelState(ChannelState.RINGING.id)

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      getVarAction.setChannel("SIP/abcd-00000002")

      getVarResponse.setResponse("Success")
      getVarResponse.setVariable("DIALSTATUS")
      getVarResponse.setValue("ANSWER")

      ref ! AmiResponse((getVarResponse, Some(AmiAction(getVarAction))))

      expectMsg(
        Channel(newChannel).copy(direction = Some(ChannelDirection.INCOMING))
      )
    }

    "notify channel watchers when dialstatus is EMPTY but UP" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")
      newChannel.setChannelState(ChannelState.UP.id)

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      getVarAction.setChannel("SIP/abcd-00000002")

      getVarResponse.setResponse("Success")
      getVarResponse.setVariable("DIALSTATUS")
      getVarResponse.setValue("")

      ref ! AmiResponse((getVarResponse, Some(AmiAction(getVarAction))))

      expectMsg(
        Channel(newChannel).copy(direction = Some(ChannelDirection.INCOMING))
      )
    }

    "notify channel watchers when dialstatus is EMPTY but ORIGINATING" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")
      newChannel.setChannelState(ChannelState.ORIGINATING.id)

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      getVarAction.setChannel("SIP/abcd-00000002")

      getVarResponse.setResponse("Success")
      getVarResponse.setVariable("DIALSTATUS")
      getVarResponse.setValue("")

      ref ! AmiResponse((getVarResponse, Some(AmiAction(getVarAction))))

      expectMsg(
        Channel(newChannel).copy(direction = Some(ChannelDirection.OUTGOING))
      )
    }

    "not notify channel watchers when var response does not contain DIALSTATUS" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      getVarAction.setChannel("SIP/abcd-00000002")

      getVarResponse.setResponse("Success")
      getVarResponse.setVariable("SIPCALLID")
      getVarResponse.setValue("value")

      ref ! AmiResponse((getVarResponse, Some(AmiAction(getVarAction))))

      expectNoMessage(250.milliseconds)
    }

    "notify channel watchers when channel connected line change" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new NewConnectedLineEvent(this)
      evt.setChannel("SIP/abcd-00000002")
      evt.setConnectedLineNum("1001")
      evt.setConnectedLineName("James Bond")

      ref ! AmiEvent(evt)

      expectMsg(Channel(newChannel).withConnectedLine("1001", "James Bond"))
    }

    "update channel when receiving PickupEvent" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new PickupEvent((): Unit)
      evt.setChannel("SIP/abcd-00000002")
      evt.setConnectedLineNum("1001")
      evt.setConnectedLineName("James Bond")

      ref ! AmiEvent(evt)

      expectMsg(Channel(newChannel).withInterception)
    }

    "notify channel watchers when channel callerid change" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new NewCallerIdEvent(this)
      evt.setChannel("SIP/abcd-00000002")
      evt.setCallerIdNum("1001")
      evt.setCallerIdName("James Bond")

      ref ! AmiEvent(evt)

      expectMsg(
        Channel(newChannel).withCallerId(CallerId("James Bond", "1001"))
      )
    }

    "don't notify channel watchers when unsubscribing" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      ref ! WatchChannelStartingWith("SIP/abcd")
      ref ! UnWatchChannelStartingWith("SIP/abcd")

      newChannel.setUniqueId("123456789.9876")
      newChannel.setChannel("SIP/abcd-00000001")

      ref ! AmiEvent(newChannel)

      expectNoMessage(100.millis)
    }

    "notify channel watchers when channel is on hold" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val newState = new HoldEvent(this)
      newState.setChannel("SIP/abcd-00000002")
      newState.setChannelState(ChannelState.UP.id)

      ref ! AmiEvent(newState)

      expectMsg(Channel(newChannel).withChannelState(ChannelState.HOLD))
    }

    "notify channel watchers when channel is not on hold anymore" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val newState = new HoldEvent(this)
      newState.setChannel("SIP/abcd-00000002")
      newState.setChannelState(ChannelState.UP.id)

      ref ! AmiEvent(newState)

      expectMsg(Channel(newChannel).withChannelState(ChannelState.HOLD))

      val unhold = new UnholdEvent(this)
      unhold.setChannel("SIP/abcd-00000002")
      unhold.setChannelState(ChannelState.UP.id)

      ref ! AmiEvent(unhold)

      expectMsg(Channel(newChannel).withChannelState(ChannelState.UP))
    }

    "notify local channel watchers with fake hangup when a loop is detected" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete
      ref ! WatchChannelStartingWith("Local/")

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("Local/1002@default-00000047;2")
      newChannel.setChannelState(ChannelState.UP.id)

      ref ! AmiEvent(newChannel)

      expectMsg(Channel(newChannel))

      ref ! LoopDetected(
        Set(
          NodeLocalChannel("Local/1002@default-00000047;2"),
          NodeLocalChannel("Local/1002@default-00000048;1")
        )
      )

      expectMsg(Channel(newChannel).withChannelState(ChannelState.HUNGUP))
    }

    "not notify local channel watchers when channel is updated after a loop is detected" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("Local/1002@default-00000047;2")

      ref ! AmiEvent(newChannel)

      ref ! LoopDetected(
        Set(
          NodeLocalChannel("Local/1002@default-00000047;2"),
          NodeLocalChannel("Local/1002@default-00000048;1")
        )
      )

      ref ! WatchChannelStartingWith("Local/")

      expectNoMessage(100.millis)
    }

    "spool notification upon VarSetEvent when throttling is enabled" in new TrackerWrapper(
      throttlingConfig
    ) {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new VarSetEvent(this)
      evt.setChannel("SIP/abcd-00000002")
      evt.setVariable("DUMMY_VAR")
      evt.setValue("DUMMY_VALUE")

      ref ! AmiEvent(evt)
      expectNoMessage(100.millis)
      ref ! PoisonPill
    }

    "eventually send notification when throttling" in new TrackerWrapper(
      throttlingConfig
    ) {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val evt = new VarSetEvent(this)
      evt.setChannel("SIP/abcd-00000002")
      evt.setVariable("DUMMY_VAR")
      evt.setValue("DUMMY_VALUE")

      ref ! AmiEvent(evt)
      ref ! FlushPendingNotification
      expectMsg(Channel(newChannel).updateVariable("DUMMY_VAR", "DUMMY_VALUE"))
    }

    "directly send notification upon NewChannelEvent when throttling is enabled" in new TrackerWrapper(
      throttlingConfig
    ) {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      ref ! WatchChannelStartingWith("SIP/abcd")

      newChannel.setUniqueId("123456789.9876")
      newChannel.setChannel("SIP/abcd-00000001")

      ref ! AmiEvent(newChannel)

      expectMsg(Channel(newChannel))
      ref ! PoisonPill
    }

    "directly send notification upon NewStateEvent when throttling is enabled" in new TrackerWrapper(
      throttlingConfig
    ) {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val newState = new NewStateEvent(this)
      newState.setChannel("SIP/abcd-00000002")
      newState.setChannelState(ChannelState.UP.id)

      ref ! AmiEvent(newState)

      expectMsg(Channel(newChannel).withState(newState))
      ref ! PoisonPill
    }

    "directly send notification upon HangupEvent when throttling is enabled" in new TrackerWrapper(
      throttlingConfig
    ) {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("SIP/abcd")
      expectMsg(Channel(newChannel))

      val newState = new HangupEvent(this)
      newState.setChannel("SIP/abcd-00000002")

      ref ! AmiEvent(newState)

      expectMsg(Channel(newChannel).withChannelState(ChannelState.HUNGUP))
      ref ! PoisonPill
    }

    "ensure a tick is scheduled when throttling is enabled" in {
      val mockScheduler = mock[Scheduler]
      val xucAmiBus     = mock[XucAmiBus]
      val ref = TestActorRef(new ChannelTracker(xucAmiBus, throttlingConfig) {
        override def scheduler: Scheduler = mockScheduler
      })

      val complete = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete

      verify(mockScheduler, timeout(500)).scheduleAtFixedRate(
        5000.millis,
        5000.millis,
        ref,
        FlushPendingNotification
      )(scala.concurrent.ExecutionContext.Implicits.global, ref)
    }

    "ensure scheduled tick is cancelled when actor is killed" in {
      val mockScheduler = mock[Scheduler]
      val tickSchedule  = mock[Cancellable]
      val xucAmiBus     = mock[XucAmiBus]
      val ref = TestActorRef(new ChannelTracker(xucAmiBus, throttlingConfig) {
        override def scheduler: Scheduler = mockScheduler
      })

      when(
        mockScheduler.scheduleAtFixedRate(
          5000.millis,
          5000.millis,
          ref,
          FlushPendingNotification
        )(scala.concurrent.ExecutionContext.Implicits.global, ref)
      ).thenReturn(tickSchedule)

      val complete = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete
      ref ! PoisonPill

      verify(tickSchedule, timeout(500)).cancel()
    }

    "hangup all channels and notify accordingly when ami connection is lost" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      val watcher4abcd: TestProbe = TestProbe()
      val watcher4efgh: TestProbe = TestProbe()
      ref ! complete

      watcher4abcd.send(ref, WatchChannelStartingWith("SIP/abcd"))
      watcher4efgh.send(ref, WatchChannelStartingWith("SIP/efgh"))

      val newChannel4abcd = new NewChannelEvent(this)
      newChannel4abcd.setUniqueId("123456789.9876")
      newChannel4abcd.setChannel("SIP/abcd-00000001")
      ref ! AmiEvent(newChannel4abcd)
      watcher4abcd.expectMsg(Channel(newChannel4abcd))

      val newChannel4efgh = new NewChannelEvent(this)
      newChannel4efgh.setUniqueId("123456799.7654")
      newChannel4efgh.setChannel("SIP/efgh-00000002")
      ref ! AmiEvent(newChannel4efgh)
      watcher4efgh.expectMsg(Channel(newChannel4efgh))

      ref ! AmiFailure("Something went wrong...", "default")
      watcher4abcd.expectMsg(
        Channel(newChannel4abcd).withChannelState(ChannelState.HUNGUP)
      )
      watcher4efgh.expectMsg(
        Channel(newChannel4efgh).withChannelState(ChannelState.HUNGUP)
      )
    }

    "hangup channels owned by failed MDS only" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      val watcher: TestProbe = TestProbe()
      ref ! complete

      watcher.send(ref, WatchChannelStartingWith("SIP/abcd"))
      watcher.send(ref, WatchChannelStartingWith("SIP/efgh"))

      val newChannelOnMds1 = new NewChannelEvent(this)
      newChannelOnMds1.setUniqueId("123456789.9876")
      newChannelOnMds1.setChannel("SIP/abcd-00000001")
      ref ! AmiEvent(newChannelOnMds1, "mds1")
      watcher.expectMsg(Channel(newChannelOnMds1, "mds1"))

      val newChannelOnMds2 = new NewChannelEvent(this)
      newChannelOnMds2.setUniqueId("123456799.7654")
      newChannelOnMds2.setChannel("SIP/efgh-00000002")
      ref ! AmiEvent(newChannelOnMds2, "mds2")
      watcher.expectMsg(Channel(newChannelOnMds2, "mds2"))

      ref ! AmiFailure("Something went wrong...", "mds2")
      watcher.expectMsg(
        Channel(newChannelOnMds2, "mds2").withChannelState(ChannelState.HUNGUP)
      )
      ref.underlyingActor.channels.values should contain only Channel(
        newChannelOnMds1,
        "mds1"
      )
    }

    "notify channel watchers with call-id when CHANNEL(pjsip,call-id) is received" in new TrackerWrapper {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      newChannel.setUniqueId("223456789.9876")
      newChannel.setChannel("PJSIP/abcd-00000002")
      newChannel.setChannelState(ChannelState.UP.id)

      ref ! AmiEvent(newChannel)

      ref ! WatchChannelStartingWith("PJSIP/abcd")
      expectMsg(Channel(newChannel))

      getVarAction.setChannel("PJSIP/abcd-00000002")

      getVarResponse.setResponse("Success")
      getVarResponse.setVariable("CHANNEL(pjsip,call-id)")
      getVarResponse.setValue("abcd-edfgh")

      ref ! AmiResponse((getVarResponse, Some(AmiAction(getVarAction))))

      expectMsg(
        Channel(newChannel).copy(variables = Map("SIPCALLID" -> getVarResponse.getValue))
      )
    }
  }
}
