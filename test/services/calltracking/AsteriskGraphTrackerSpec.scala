package services.calltracking

import org.asteriskjava.manager.action.CoreShowChannelsAction
import org.asteriskjava.manager.event.{BridgeEnterEvent, CoreShowChannelEvent, CoreShowChannelsCompleteEvent, DialBeginEvent}
import org.scalatest.*
import org.apache.pekko.actor.*
import org.apache.pekko.testkit.*
import org.mockito.Mockito.*
import org.mockito.ArgumentMatchers.*
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.duration.*
import services.{AmiEventHelper, XucAmiBus}
import services.XucAmiBus.*
import services.ServiceStatus.*
import services.calltracking.graph.*
import services.calltracking.AsteriskGraphTracker.*
import services.calltracking.BaseTracker.*
import services.calltracking.graph.NodeBridge.BridgeCreator
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import pekkotest.TestKitSpec

class AsteriskGraphTrackerSpec
    extends TestKitSpec("AsteriskGraphTracker")
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AmiEventHelper
    with AsteriskObjectHelper {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  abstract class TestTracker {
    val xucAmiBus: XucAmiBus      = mock[XucAmiBus]
    val channelTracker: TestProbe = TestProbe()
    val ref: ActorRef = system.actorOf(
      Props(new AsteriskGraphTracker(xucAmiBus, channelTracker.ref))
    )
  }

  "AsteriskGraphTracker Object" should {
    "find all nodechannels endpoints of all paths" in {
      val c1 = NodeChannel("c1")
      val c2 = NodeChannel("c2")
      val paths = Set(
        AsteriskPath(NodeBridge("b1", Some(new BridgeCreator("bc1"))), c1),
        AsteriskPath(NodeBridge("b2", Some(new BridgeCreator("bc2"))), c2)
      )

      AsteriskGraphTracker.pathsEndpoints(paths) shouldBe Set(c1, c2)
    }

    "find all nodechannels endpoints of all paths even if last is object is not a channel" in {
      val c1 = NodeChannel("c1")
      val c2 = NodeChannel("c2")
      val paths = Set(
        AsteriskPath(
          NodeBridge("b1", Some(new BridgeCreator("bc1"))),
          c1,
          NodeMdsTrunkBridge("abcd0123456")
        ),
        AsteriskPath(NodeBridge("b2", Some(new BridgeCreator("bc2"))), c2)
      )

      AsteriskGraphTracker.pathsEndpoints(paths) shouldBe Set(c1, c2)
    }
  }

  "AsteriskGraphTracker" should {

    "subscribe to AmiEvent and AmiFailure through the XucAmiBus" in new TestTracker {
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiEvent)
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiService)
    }

    "unsubscribe from AmiEvent through the XucAmiBus" in new TestTracker {
      ref ! PoisonPill
      verify(xucAmiBus, timeout(500)).unsubscribe(ref)
    }

    "request channels upon startup" in new TestTracker {
      verify(xucAmiBus, timeout(500)).publish(
        AmiAction(any[CoreShowChannelsAction](), None, Some(ref))
      )

    }

    "be in Starting state when starting up" in new TestTracker {
      ref ! GetServiceStatus
      expectMsg(ServiceStarting(AsteriskGraphTracker.serviceName, ref))
    }

    "become ready when all channels are loaded" in new TestTracker {
      val response1: AmiEvent = AmiEvent(new CoreShowChannelEvent(this))
      val response2: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! response1
      ref ! response2
      ref ! GetServiceStatus
      expectMsg(ServiceReady(AsteriskGraphTracker.serviceName, ref))
    }

    "notify when ready after all channels loaded" in new TestTracker {
      val response1: AmiEvent = AmiEvent(new CoreShowChannelEvent(this))
      val response2: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! NotifyWhenServiceReady
      ref ! response1
      ref ! response2

      expectMsg(ServiceReady(AsteriskGraphTracker.serviceName, ref))
    }

    "track connections of a given channel" in new TestTracker {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      val bridgeEvent1 = new BridgeEnterEvent(this)
      bridgeEvent1.setChannel("c1")
      bridgeEvent1.setBridgeUniqueId("b1")
      bridgeEvent1.setBridgeCreator("bc1")

      val bridgeEvent2 = new BridgeEnterEvent(this)
      bridgeEvent2.setChannel("c2")
      bridgeEvent2.setBridgeUniqueId("b1")
      bridgeEvent2.setBridgeCreator("bc1")

      ref ! AmiEvent(bridgeEvent1)
      ref ! AmiEvent(bridgeEvent2)

      ref ! GetPathsFromChannel(NodeChannel("c1"))
      expectMsg(
        PathsFromChannel(
          NodeChannel("c1"),
          Set(
            AsteriskPath(
              NodeBridge("b1", Some(new BridgeCreator("bc1"))),
              NodeChannel("c2")
            )
          )
        )
      )

      ref ! GetPathsFromChannel(NodeChannel("c2"))
      expectMsg(
        PathsFromChannel(
          NodeChannel("c2"),
          Set(
            AsteriskPath(
              NodeBridge("b1", Some(new BridgeCreator("bc1"))),
              NodeChannel("c1")
            )
          )
        )
      )

    }

    "recover graph based on CoreShowChannelEvent" in new TestTracker {
      val chEvent1 = new CoreShowChannelEvent(this)
      chEvent1.setChannel("c1")
      chEvent1.setBridgeid("b")

      val chEvent2 = new CoreShowChannelEvent(this)
      chEvent2.setChannel("c2")
      chEvent2.setBridgeid("b")

      val complete = new CoreShowChannelsCompleteEvent(this)

      ref ! AmiEvent(chEvent1)
      ref ! AmiEvent(chEvent2)
      ref ! AmiEvent(complete)

      ref ! GetPathsFromChannel(NodeChannel("c1"))
      expectMsg(
        PathsFromChannel(
          NodeChannel("c1"),
          Set(
            AsteriskPath(
              NodeBridge("b", Some(new BridgeCreator("CoreShowChannels"))),
              NodeChannel("c2")
            )
          )
        )
      )

      ref ! GetPathsFromChannel(NodeChannel("c2"))
      expectMsg(
        PathsFromChannel(
          NodeChannel("c2"),
          Set(
            AsteriskPath(
              NodeBridge("b", Some(new BridgeCreator("CoreShowChannels"))),
              NodeChannel("c1")
            )
          )
        )
      )
    }

    "recover graph based on CoreShowChannelEvent for local channels" in new TestTracker {
      val chEvent1 = new CoreShowChannelEvent(this)
      chEvent1.setChannel("c1")
      chEvent1.setBridgeid("b1")

      val lchEvent1 = new CoreShowChannelEvent(this)
      lchEvent1.setChannel("Local/d;1")
      lchEvent1.setBridgeid("b1")

      val chEvent2 = new CoreShowChannelEvent(this)
      chEvent2.setChannel("c2")
      chEvent2.setBridgeid("b2")

      val lchEvent2 = new CoreShowChannelEvent(this)
      lchEvent2.setChannel("Local/d;2")
      lchEvent2.setBridgeid("b2")

      val complete = new CoreShowChannelsCompleteEvent(this)

      ref ! AmiEvent(chEvent1)
      ref ! AmiEvent(chEvent2)
      ref ! AmiEvent(lchEvent1)
      ref ! AmiEvent(lchEvent2)
      ref ! AmiEvent(complete)

      val lc1: NodeLocalChannel = NodeLocalChannel("Local/d;1")
      val lc2: NodeLocalChannel = NodeLocalChannel("Local/d;2")

      val path: List[AsteriskObject] = AsteriskPath(
        NodeChannel("c1"),
        NodeBridge("b1", Some(new BridgeCreator("CoreShowChannels"))),
        lc1,
        NodeLocalBridge(lc1, lc2),
        lc2,
        NodeBridge("b2", Some(new BridgeCreator("CoreShowChannels"))),
        NodeChannel("c2")
      )

      ref ! GetPathsFromChannel(NodeChannel("c1"))
      expectMsg(PathsFromChannel(NodeChannel("c1"), Set(path.drop(1))))

      ref ! GetPathsFromChannel(NodeChannel("c2"))
      expectMsg(PathsFromChannel(NodeChannel("c2"), Set(path.reverse.drop(1))))
    }

    "notify channel watchers when channel is linked to another" in new TestTracker {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      ref ! WatchChannelStartingWith("SIP/abcd")

      val bridgeEvent1 = new BridgeEnterEvent(this)
      bridgeEvent1.setChannel("SIP/abcd-0000001")
      bridgeEvent1.setBridgeUniqueId("b1")
      bridgeEvent1.setBridgeCreator("bc1")

      val bridgeEvent2 = new BridgeEnterEvent(this)
      bridgeEvent2.setChannel("SIP/efgh-0000001")
      bridgeEvent2.setBridgeUniqueId("b1")
      bridgeEvent2.setBridgeCreator("bc1")

      ref ! AmiEvent(bridgeEvent1)
      expectMsg(
        PathsFromChannel(
          NodeChannel("SIP/abcd-0000001"),
          Set(AsteriskPath(NodeBridge("b1", Some(new BridgeCreator("bc1")))))
        )
      )

      ref ! AmiEvent(bridgeEvent2)
      expectMsgAllOf(
        PathsFromChannel(
          NodeChannel("SIP/abcd-0000001"),
          Set(
            AsteriskPath(
              NodeBridge("b1", Some(new BridgeCreator("bc1"))),
              NodeChannel("SIP/efgh-0000001")
            )
          )
        )
      )
    }

    "notify channel watchers when channel is linked to another sideways" in new TestTracker {
      val probe1: TestProbe = TestProbe()
      val probe2: TestProbe = TestProbe()

      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))

      ref ! complete

      probe1.send(ref, WatchChannelStartingWith("SIP/abcd"))
      probe2.send(ref, WatchChannelStartingWith("SIP/efgh"))

      val bridgeEvent1 = new BridgeEnterEvent(this)
      bridgeEvent1.setChannel("SIP/abcd-0000001")
      bridgeEvent1.setBridgeUniqueId("b1")
      bridgeEvent1.setBridgeCreator("bc1")

      val bridgeEvent2 = new BridgeEnterEvent(this)
      bridgeEvent2.setChannel("SIP/efgh-0000001")
      bridgeEvent2.setBridgeUniqueId("b1")
      bridgeEvent2.setBridgeCreator("bc1")

      val path: List[AsteriskObject] = AsteriskPath(
        NodeChannel("SIP/abcd-0000001"),
        NodeBridge("b1", Some(new BridgeCreator("bc1"))),
        NodeChannel("SIP/efgh-0000001")
      )

      ref ! AmiEvent(bridgeEvent1)
      probe1.expectMsg(
        PathsFromChannel(
          NodeChannel("SIP/abcd-0000001"),
          Set(AsteriskPath(NodeBridge("b1", Some(new BridgeCreator("bc1")))))
        )
      )

      ref ! AmiEvent(bridgeEvent2)
      probe1.expectMsg(
        PathsFromChannel(NodeChannel("SIP/abcd-0000001"), Set(path.drop(1)))
      )
      probe2.expectMsg(
        PathsFromChannel(
          NodeChannel("SIP/efgh-0000001"),
          Set(path.reverse.drop(1))
        )
      )
    }

    "extract extension from local channel" in {
      localChannelExtension("Local/1002@default-00000047;2") shouldBe Some(
        "1002"
      )
    }

    "fail to extract extension from non local channel" in {
      localChannelExtension("SIP/az9kf7-00000063") shouldBe None
    }

    "notify ChannelTracker when a loop is detected between two local channels" in new TestTracker {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete

      val dial = new DialBeginEvent(this)
      dial.setChannel("Local/1002@default-00000047;2")
      dial.setDestination("Local/1002@default-00000048;1")

      ref ! AmiEvent(dial)

      val loop: LoopDetected = channelTracker.expectMsgType[LoopDetected](500.millis)
      loop.nodes.size shouldBe 2
      loop.nodes should contain.allOf(
        NodeLocalChannel("Local/1002@default-00000047;2"),
        NodeLocalChannel("Local/1002@default-00000048;1")
      )
    }

    "notify local channel device tracker when a loop is detected" in new TestTracker() {
      val deviceTracker: TestProbe = TestProbe()
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete
      deviceTracker.send(ref, WatchChannelStartingWith("Local/"))

      val dial = new DialBeginEvent(this)
      dial.setChannel("Local/1002@default-00000047;2")
      dial.setDestination("Local/1002@default-00000048;1")

      ref ! AmiEvent(dial)

      val loop: LoopDetected = deviceTracker.expectMsgType[LoopDetected](500.millis)
      loop.nodes.size shouldBe 2
      loop.nodes should contain.allOf(
        NodeLocalChannel("Local/1002@default-00000047;2"),
        NodeLocalChannel("Local/1002@default-00000048;1")
      )
    }

    "notify ChannelTracker with only local channels involved in a loop, ignoring non-local channels" in new TestTracker {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete

      val bridge1 = new BridgeEnterEvent(this)
      bridge1.setChannel("SIP/abcd-0000001")
      bridge1.setBridgeUniqueId("b1")
      bridge1.setBridgeCreator("bc1")
      ref ! AmiEvent(bridge1)

      val bridge2 = new BridgeEnterEvent(this)
      bridge2.setChannel("Local/1002@default-00000047;2")
      bridge2.setBridgeUniqueId("b1")
      bridge2.setBridgeCreator("bc2")
      ref ! AmiEvent(bridge2)

      val dial = new DialBeginEvent(this)
      dial.setChannel("Local/1002@default-00000047;2")
      dial.setDestination("Local/1002@default-00000048;1")
      ref ! AmiEvent(dial)

      val loop: LoopDetected = channelTracker.expectMsgType[LoopDetected](500.millis)
      loop.nodes.size shouldBe 2
      loop.nodes should contain.allOf(
        NodeLocalChannel("Local/1002@default-00000047;2"),
        NodeLocalChannel("Local/1002@default-00000048;1")
      )
    }

    "not notify ChannelTracker when a loop is detected between non-local channel" in new TestTracker {
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete

      val dial = new DialBeginEvent(this)
      dial.setChannel("SIP/abcd-0000001")
      dial.setDestination("SIP/abcd-0000002")

      ref ! AmiEvent(dial)

      channelTracker.expectNoMessage(100.millis)
    }

    "notify watchers with removed path when a loop is detected" in new TestTracker {
      val deviceTracker: TestProbe = TestProbe()
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete

      deviceTracker.send(ref, WatchChannelStartingWith("Local/"))

      val dial = new DialBeginEvent(this)
      dial.setChannel("Local/1002@default-00000047;2")
      dial.setDestination("Local/1002@default-00000048;1")

      ref ! AmiEvent(dial)

      deviceTracker.expectMsgType[LoopDetected](500.millis)
      deviceTracker.expectMsgAllOf(
        1.second,
        PathsFromChannel(
          NodeLocalChannel("Local/1002@default-00000047;2"),
          Set(List.empty)
        ),
        PathsFromChannel(
          NodeLocalChannel("Local/1002@default-00000048;1"),
          Set(List.empty)
        )
      )
    }

    "clear graph when AmiFailure received" in new TestTracker {
      val abcdTracker: TestProbe = TestProbe()
      val efghTracker: TestProbe = TestProbe()
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete

      abcdTracker.send(ref, WatchChannelStartingWith("SIP/abcd"))
      efghTracker.send(ref, WatchChannelStartingWith("SIP/efgh"))

      val bridgeEvent1 = new BridgeEnterEvent(this)
      bridgeEvent1.setChannel("SIP/abcd-0000001")
      bridgeEvent1.setBridgeUniqueId("b1")
      bridgeEvent1.setBridgeCreator("bc1")

      val bridgeEvent2 = new BridgeEnterEvent(this)
      bridgeEvent2.setChannel("SIP/efgh-0000002")
      bridgeEvent2.setBridgeUniqueId("b1")
      bridgeEvent2.setBridgeCreator("bc1")

      ref ! AmiEvent(bridgeEvent1)
      abcdTracker.expectMsg(
        PathsFromChannel(
          NodeChannel("SIP/abcd-0000001"),
          Set(AsteriskPath(NodeBridge("b1", Some(new BridgeCreator("bc1")))))
        )
      )

      ref ! AmiEvent(bridgeEvent2)
      abcdTracker.expectMsgAllOf(
        PathsFromChannel(
          NodeChannel("SIP/abcd-0000001"),
          Set(
            AsteriskPath(
              NodeBridge("b1", Some(new BridgeCreator("bc1"))),
              NodeChannel("SIP/efgh-0000002")
            )
          )
        )
      )
      efghTracker.expectMsgAllOf(
        PathsFromChannel(
          NodeChannel("SIP/efgh-0000002"),
          Set(
            AsteriskPath(
              NodeBridge("b1", Some(new BridgeCreator("bc1"))),
              NodeChannel("SIP/abcd-0000001")
            )
          )
        )
      )

      ref ! AmiFailure("Uho, something went wrong...", "default")
      abcdTracker.expectMsgAllOf(
        PathsFromChannel(NodeChannel("SIP/abcd-0000001"), Set.empty)
      )
      efghTracker.expectMsgAllOf(
        PathsFromChannel(NodeChannel("SIP/efgh-0000002"), Set.empty)
      )
    }

    "clear graph of links owned by failed MDS only" in new TestTracker {
      val tracker: TestProbe = TestProbe()
      val complete: AmiEvent = AmiEvent(new CoreShowChannelsCompleteEvent(this))
      ref ! complete

      tracker.send(ref, WatchChannelStartingWith("SIP/abcd"))
      tracker.send(ref, WatchChannelStartingWith("SIP/efgh"))

      val bridgeEvent1 = new BridgeEnterEvent(this)
      bridgeEvent1.setChannel("SIP/abcd-0000001")
      bridgeEvent1.setBridgeUniqueId("b1")
      bridgeEvent1.setBridgeCreator("bc1")

      val bridgeEvent2 = new BridgeEnterEvent(this)
      bridgeEvent2.setChannel("SIP/efgh-0000002")
      bridgeEvent2.setBridgeUniqueId("b2")
      bridgeEvent2.setBridgeCreator("bc2")

      ref ! AmiEvent(bridgeEvent1, "mds1")
      tracker.expectMsg(
        PathsFromChannel(
          NodeChannel("SIP/abcd-0000001", "mds1"),
          Set(
            AsteriskPath(
              NodeBridge("b1", "mds1", Some(new BridgeCreator("bc1")))
            )
          )
        )
      )

      ref ! AmiEvent(bridgeEvent2, "mds2")
      tracker.expectMsg(
        PathsFromChannel(
          NodeChannel("SIP/efgh-0000002", "mds2"),
          Set(
            AsteriskPath(
              NodeBridge("b2", "mds2", Some(new BridgeCreator("bc2")))
            )
          )
        )
      )

      ref ! AmiFailure("Uho, something went wrong...", "mds2")
      tracker.expectMsg(
        PathsFromChannel(NodeChannel("SIP/efgh-0000002", "mds2"), Set.empty)
      )
      ref ! GetPathsFromChannel(NodeChannel("SIP/abcd-0000001", "mds1"))
      expectMsg(
        PathsFromChannel(
          NodeChannel("SIP/abcd-0000001", "mds1"),
          Set(
            AsteriskPath(
              NodeBridge("b1", "mds1", Some(new BridgeCreator("bc1")))
            )
          )
        )
      )
    }
  }
}
