package services.calltracking

import org.joda.time.DateTime
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import services.XucAmiBus._
import services.calltracking.DeviceConferenceAction._
import xivo.websocket.{WsConferenceParticipantOrganizerRole, WsConferenceParticipantUserRole}
import xivo.xucami.models.CallerId

class DeviceConferenceActionSpec extends AnyWordSpec with Matchers {

  def createConferences(
      multiple: Boolean = false,
      organizer: Boolean = false,
      otherParticipants: List[ConferenceParticipant] = List.empty
  ): Map[String, ConferenceWithChannels] = {
    val role = if (organizer) OrganizerRole else UserRole
    val p1 = ConferenceParticipant(
      "4000",
      1,
      "SIP/abcd-00001",
      CallerId("Some One", "1001"),
      DateTime.now,
      role = role
    )
    val p1Bis = ConferenceParticipant(
      "4000",
      3,
      "SIP/abcd-00002",
      CallerId("Some One", "1001"),
      DateTime.now
    )
    val p2 = ConferenceParticipant(
      "4000",
      2,
      "SIP/efgh-00001",
      CallerId("Another One", "1002"),
      DateTime.now
    )
    val p4 = ConferenceParticipant(
      "4000",
      4,
      "SIP/ifkl-00004",
      CallerId("Number 4", "1004"),
      DateTime.now
    )
    val p5 = ConferenceParticipant(
      "4000",
      5,
      "SIP/mnop-00005",
      CallerId("Number 5", "1005"),
      DateTime.now
    )

    val initialRoom = ConferenceRoom(
      "4000",
      "MySuperConference",
      ConferenceAvailable,
      None,
      List.empty,
      "default",
      Some("1234"),
      Some("4321")
    )
      .addParticipant(p1)
      .addParticipant(p2)
      .addParticipant(p4)
      .addParticipant(p5)

    val topic = ConferenceTracker.conferenceParticipantEventTopic(
      initialRoom.number,
      "10.181.0.1"
    )

    val room = otherParticipants.foldLeft(initialRoom) { (c, p) =>
      c.addParticipant(p)
    }
    val channels = Set(p1.channelName)
    val conference = if (multiple) {
      ConferenceWithChannels(
        room.addParticipant(p1Bis),
        Set(channels) + Set(p1Bis.channelName),
        topic
      )
    } else {
      ConferenceWithChannels(room, Set(channels), topic)
    }
    Map("4000" -> conference)
  }

  def createConferenceWithoutMe(): Map[String, ConferenceWithChannels] = {
    val p2 = ConferenceParticipant(
      "4000",
      2,
      "SIP/efgh-00001",
      CallerId("Another One", "1002"),
      DateTime.now
    )
    val p4 = ConferenceParticipant(
      "4000",
      4,
      "SIP/ifkl-00004",
      CallerId("Number 4", "1004"),
      DateTime.now
    )
    val p5 = ConferenceParticipant(
      "4000",
      5,
      "SIP/mnop-00005",
      CallerId("Number 5", "1005"),
      DateTime.now
    )

    val room = ConferenceRoom(
      "4000",
      "MySuperConference",
      ConferenceAvailable,
      None,
      List.empty,
      "default"
    )
      .addParticipant(p2)
      .addParticipant(p4)
      .addParticipant(p5)

    val conference = ConferenceWithChannels(
      room,
      Set.empty,
      ConferenceTracker.conferenceParticipantEventTopic(
        room.number,
        "10.181.0.1"
      )
    )
    Map("4000" -> conference)
  }

  "DeviceConferenceActionSpec" should {

    "return error for Mute action when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, Mute("4000", 2), "default")

      result shouldBe Left(NotOrganizer)
    }

    "return an ami request for Mute action on any participant when organizer" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Mute("4000", 2), "default")

      result.getOrElse(fail()) should contain only MeetMeMuteRequest("4000", 2)
    }

    "return error for Unmute action when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, Unmute("4000", 2), "default")

      result shouldBe Left(NotOrganizer)
    }

    "return an ami request for Unmute action on any participant when organizer" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Unmute("4000", 2), "default")

      result.getOrElse(fail()) should contain only MeetMeUnmuteRequest(
        "4000",
        2
      )
    }

    "return error for MuteAll action when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, MuteAll("4000"), "default")

      result shouldBe Left(NotOrganizer)
    }

    "return a list of ami request for MuteAll action for all participants except self when organizer" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, MuteAll("4000"), "default")

      result.getOrElse(fail()) should contain.only(
        MeetMeMuteRequest("4000", 2),
        MeetMeMuteRequest("4000", 4),
        MeetMeMuteRequest("4000", 5)
      )
    }

    "return error for UnmuteAll action when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, UnmuteAll("4000"), "default")

      result shouldBe Left(NotOrganizer)
    }

    "return a list of ami request for UnmuteAll action for all participants except self when organizer" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, UnmuteAll("4000"), "default")

      result.getOrElse(fail()) should contain.only(
        MeetMeUnmuteRequest("4000", 2),
        MeetMeUnmuteRequest("4000", 4),
        MeetMeUnmuteRequest("4000", 5)
      )
    }

    "return an error from a MuteMe action when not a member" in {
      val conferences = createConferenceWithoutMe()
      val result =
        DeviceConferenceAction.toAmi(conferences, MuteMe("4000"), "default")

      result shouldBe Left(NotAMember)
    }

    "return an ami request from a MuteMe action" in {
      val conferences = createConferences()
      val result =
        DeviceConferenceAction.toAmi(conferences, MuteMe("4000"), "default")

      result
        .getOrElse(fail()) should contain only MeetMeMuteRequest("4000", 1)
    }

    "return a list of ami request from a MuteMe action when multiple times in same conference" in {
      val conferences = createConferences(true)
      val result =
        DeviceConferenceAction.toAmi(conferences, MuteMe("4000"), "default")

      result.getOrElse(fail()) should contain.only(
        MeetMeMuteRequest("4000", 1),
        MeetMeMuteRequest("4000", 3)
      )
    }

    "return an ami request from a UnmuteMe action" in {
      val conferences = createConferences()
      val result =
        DeviceConferenceAction.toAmi(conferences, UnmuteMe("4000"), "default")

      result
        .getOrElse(fail()) should contain only MeetMeUnmuteRequest("4000", 1)
    }

    "return a list of ami request from a UnmuteMe action when multiple times in same conference" in {
      val conferences = createConferences(true)
      val result =
        DeviceConferenceAction.toAmi(conferences, UnmuteMe("4000"), "default")

      result.getOrElse(fail()) should contain.only(
        MeetMeUnmuteRequest("4000", 1),
        MeetMeUnmuteRequest("4000", 3)
      )
    }

    "return an ami request from a Kick action when organizer" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Kick("4000", 2), "default")

      result
        .getOrElse(fail()) should contain only MeetMeKickRequest("4000", 2)
    }

    "return error for Kick request when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, Kick("4000", 1), "default")

      result shouldBe Left(NotOrganizer)
    }

    "return error when kicking out an organizer" in {
      val secondOrganizer = ConferenceParticipant(
        "4000",
        6,
        "SIP/qrst-00006",
        CallerId("Second organizer ", "1006"),
        DateTime.now,
        role = OrganizerRole
      )
      val conferences = createConferences(
        organizer = true,
        otherParticipants = List(secondOrganizer)
      )

      val result =
        DeviceConferenceAction.toAmi(conferences, Kick("4000", 6), "default")

      result shouldBe Left(CannotKickOrganizer)
    }

    "return an error when issuing a kick command on a non existent participant" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Kick("4000", 12), "default")

      result shouldBe Left(ParticipantNotFound)
    }

    "return an error when issuing a Mute command on a non existent participant" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Mute("4000", 12), "default")

      result shouldBe Left(ParticipantNotFound)
    }

    "return an error when issuing a Unmute command on a non existent participant" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Unmute("4000", 12), "default")

      result shouldBe Left(ParticipantNotFound)
    }

    "return error for Invite action when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result = DeviceConferenceAction.toAmi(
        conferences,
        Invite(
          "4000",
          "0102030405",
          WsConferenceParticipantUserRole,
          true,
          Map(("someVariable", "someValue")),
          None
        ),
        "default"
      )

      result shouldBe Left(NotOrganizer)
    }

    "return error for Invite action when not a member" in {
      val conferences = createConferences(organizer = false)
      val result = DeviceConferenceAction.toAmi(
        conferences,
        Invite(
          "4002",
          "0102030405",
          WsConferenceParticipantUserRole,
          true,
          Map(("someVariable", "someValue")),
          None
        ),
        "default"
      )

      result shouldBe Left(NotAMember)
    }

    "return ami command with user pin for Invite action " in {
      val conferences = createConferences(organizer = true)
      val result = DeviceConferenceAction.toAmi(
        conferences,
        Invite(
          "4000",
          "0102030405",
          WsConferenceParticipantUserRole,
          false,
          Map(("someVariable", "someValue")),
          None
        ),
        "default"
      )

      val expected = InviteInConferenceActionRequest(
        "0102030405",
        "4000",
        Some("1234"),
        false,
        false,
        "default",
        Map(("someVariable", "someValue")),
        None
      )

      result shouldBe Right(List(expected))
    }

    "return ami command with organizer pin for Invite action " in {
      val conferences = createConferences(organizer = true)
      val result = DeviceConferenceAction.toAmi(
        conferences,
        Invite(
          "4000",
          "0102030405",
          WsConferenceParticipantOrganizerRole,
          false,
          Map(("someVariable", "someValue")),
          None
        ),
        "default"
      )

      val expected = InviteInConferenceActionRequest(
        "0102030405",
        "4000",
        Some("4321"),
        true,
        false,
        "default",
        Map(("someVariable", "someValue")),
        None
      )

      result shouldBe Right(List(expected))
    }

    "return ami command for Invite action with early join" in {
      val conferences = createConferences(organizer = true)
      val result = DeviceConferenceAction.toAmi(
        conferences,
        Invite(
          "4000",
          "0102030405",
          WsConferenceParticipantUserRole,
          true,
          Map(("someVariable", "someValue")),
          None
        ),
        "default"
      )

      val expected = InviteInConferenceActionRequest(
        "0102030405",
        "4000",
        Some("1234"),
        false,
        true,
        "default",
        Map(("someVariable", "someValue")),
        None
      )

      result shouldBe Right(List(expected))
    }

    "return ami command with user context in AMI Invite action " in {
      val conferences = createConferences(organizer = true)
      val result = DeviceConferenceAction.toAmi(
        conferences,
        Invite(
          "4000",
          "0102030405",
          WsConferenceParticipantUserRole,
          false,
          Map(("someVariable", "someValue")),
          None
        ),
        "somecontext"
      )

      val expected = InviteInConferenceActionRequest(
        "0102030405",
        "4000",
        Some("1234"),
        false,
        false,
        "somecontext",
        Map(("someVariable", "someValue")),
        None
      )

      result shouldBe Right(List(expected))
    }

    "hangup all participant when closing a conference" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Close("4000"), "default")
      val expected = conferences("4000").conference.participants
        .map(participant => HangupActionRequest(participant.channelName))
      result shouldBe Right(expected)
    }

    "return error for close action when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, Close("4000"), "default")

      result shouldBe Left(NotOrganizer)
    }

    "return error for close action when not a member" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, Close("4002"), "default")

      result shouldBe Left(NotAMember)
    }

    "return an ami request from a deafen action when organizer" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Deafen("4000", 2), "default")

      result.getOrElse(fail()) should contain only MeetMeDeafenRequest(
        "SIP/efgh-00001",
        2
      )
    }

    "return error for deafen request when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, Deafen("4000", 1), "default")

      result shouldBe Left(NotOrganizer)
    }

    "return an error when issuing a deafen command on a non existent participant" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Deafen("4000", 12), "default")

      result shouldBe Left(ParticipantNotFound)
    }

    "return an ami request from a undeafen action when organizer" in {
      val conferences = createConferences(organizer = true)
      val result = DeviceConferenceAction.toAmi(
        conferences,
        Undeafen("4000", 2),
        "default"
      )

      result.getOrElse(fail()) should contain only MeetMeUndeafenRequest(
        "SIP/efgh-00001",
        2
      )
    }

    "return error for undeafen request when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, Deafen("4000", 1), "default")

      result shouldBe Left(NotOrganizer)
    }

    "return an error when issuing a undeafen command on a non existent participant" in {
      val conferences = createConferences(organizer = true)
      val result =
        DeviceConferenceAction.toAmi(conferences, Deafen("4000", 12), "default")

      result shouldBe Left(ParticipantNotFound)
    }

    "return error for Reset action when not organizer" in {
      val conferences = createConferences(organizer = false)
      val result =
        DeviceConferenceAction.toAmi(conferences, Reset("4000"), "default")

      result shouldBe Left(NotOrganizer)
    }

    "unmute all muted participants when issuing Reset command" in {
      val confno = "4000"
      val mutedP1 = ConferenceParticipant(
        confno,
        6,
        "SIP/qrst-00006",
        CallerId("Muted Participant 1 ", "1006"),
        DateTime.now,
        isMuted = true
      )

      val mutedP2 = ConferenceParticipant(
        confno,
        7,
        "SIP/uvwx-00007",
        CallerId("Muted Participant 2 ", "1007"),
        DateTime.now,
        isMuted = true
      )

      val conferences =
        createConferences(
          organizer = true,
          otherParticipants = List(mutedP1, mutedP2)
        )
      val result =
        DeviceConferenceAction.toAmi(conferences, Reset(confno), "default")

      result.getOrElse(fail()) should contain.only(
        MeetMeUnmuteRequest(confno, mutedP1.index),
        MeetMeUnmuteRequest(confno, mutedP2.index)
      )
    }

    "undeafen all deaf participants when issuing Reset command" in {
      val confno = "4000"
      val mutedP1 = ConferenceParticipant(
        confno,
        6,
        "SIP/qrst-00006",
        CallerId("Muted Participant 1 ", "1006"),
        DateTime.now,
        isDeaf = true
      )

      val mutedP2 = ConferenceParticipant(
        confno,
        7,
        "SIP/uvwx-00007",
        CallerId("Muted Participant 2 ", "1007"),
        DateTime.now,
        isDeaf = true
      )

      val conferences =
        createConferences(
          organizer = true,
          otherParticipants = List(mutedP1, mutedP2)
        )
      val result =
        DeviceConferenceAction.toAmi(conferences, Reset(confno), "default")

      result.getOrElse(fail()) should contain.only(
        MeetMeUndeafenRequest(mutedP1.channelName, mutedP1.index),
        MeetMeUndeafenRequest(mutedP2.channelName, mutedP2.index)
      )
    }

    "not do anything to participants when issuing Reset command and there is another organizer" in {
      val confno = "4000"
      val mutedP1 = ConferenceParticipant(
        confno,
        6,
        "SIP/qrst-00006",
        CallerId("Muted Participant 1 ", "1006"),
        DateTime.now,
        isMuted = true
      )

      val mutedP2 = ConferenceParticipant(
        confno,
        7,
        "SIP/uvwx-00007",
        CallerId("Muted Participant 2 ", "1007"),
        DateTime.now,
        isMuted = true
      )

      val otherOrganizer = ConferenceParticipant(
        confno,
        8,
        "SIP/poiu-00008",
        CallerId("Other Organizer ", "1008"),
        DateTime.now,
        role = OrganizerRole
      )

      val conferences =
        createConferences(
          organizer = true,
          otherParticipants = List(mutedP1, mutedP2, otherOrganizer)
        )
      val result =
        DeviceConferenceAction.toAmi(conferences, Reset(confno), "default")

      result shouldBe Left(OrganizerStillThere)
    }
  }
}
