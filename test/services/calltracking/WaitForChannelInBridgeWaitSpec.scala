package services.calltracking

import org.mockito.Mockito
import org.scalatest.*
import org.scalatest.concurrent.*
import org.apache.pekko.actor.*
import org.apache.pekko.testkit.*
import org.apache.pekko.util.Timeout
import org.mockito.ArgumentMatchers.*
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.graph.*
import services.calltracking.AsteriskGraphTracker.*
import services.calltracking.BaseTracker.*
import services.calltracking.graph.NodeBridge.BridgeCreator

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{
  ExecutionContext,
  ExecutionContextExecutor,
  Future,
  Promise
}
import scala.concurrent.duration.*
import xivo.xucami.models.{CallerId, Channel, ChannelState}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class WaitForChannelInBridgeWaitSpec(implicit ec: ExecutionContext)
    extends TestKit(ActorSystem("WaitForChannelBridged"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with ScalaFutures
    with AsteriskObjectHelper {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "WaitForChannelInBridgeWait" should {
    "subscribe to channel state of given channel" in {
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()
      val channel        = NodeChannel("SIP/abcd-000001")
      val promise        = Promise[ChannelBridgeResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelInBridgeWait(
            channelTracker.ref,
            graphTracker.ref,
            channel,
            promise
          )
        )
      )

      channelTracker.expectMsg(WatchChannelStartingWith(channel.name))
    }

    "subscribe to graph changes from given channel" in {
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelBridgeResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelInBridgeWait(
            channelTracker.ref,
            graphTracker.ref,
            channel,
            promise
          )
        )
      )

      graphTracker.expectMsg(WatchChannelStartingWith(channel.name))
    }

    "unsubscribe to channel state of given channel when finishing" in {
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelBridgeResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelInBridgeWait(
            channelTracker.ref,
            graphTracker.ref,
            channel,
            promise
          )
        )
      )

      channelTracker.expectMsg(WatchChannelStartingWith(channel.name))

      actor ! PoisonPill

      channelTracker.expectMsg(UnWatchChannelStartingWith(channel.name))
    }

    "unsubscribe to graph changes from given channel when finishing" in {
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelBridgeResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelInBridgeWait(
            channelTracker.ref,
            graphTracker.ref,
            channel,
            promise
          )
        )
      )

      graphTracker.expectMsg(WatchChannelStartingWith(channel.name))

      actor ! PoisonPill

      graphTracker.expectMsg(UnWatchChannelStartingWith(channel.name))
    }

    "DO NOT complete the promise with success of ChannelBridged when given channel is bridged by a bridge creator OTHER THAN 'BridgeWait'" in {
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelBridgeResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelInBridgeWait(
            channelTracker.ref,
            graphTracker.ref,
            channel,
            promise
          )
        )
      )
      channelTracker.expectMsg(WatchChannelStartingWith("SIP/abcd-000001"))

      actor ! PathsFromChannel(
        channel,
        Set(AsteriskPath(NodeBridge("b1", Some(new BridgeCreator("bc1")))))
      )

      channelTracker.expectNoMessage(50.millis)
    }

    "DO NOT complete the promise with success of ChannelBridged when given channel is bridged by an unknown (null) bridge creator" in {
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelBridgeResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelInBridgeWait(
            channelTracker.ref,
            graphTracker.ref,
            channel,
            promise
          )
        )
      )
      channelTracker.expectMsg(WatchChannelStartingWith("SIP/abcd-000001"))

      actor ! PathsFromChannel(
        channel,
        Set(AsteriskPath(NodeBridge("b1", None)))
      )

      channelTracker.expectNoMessage(50.millis)
    }

    "complete the promise with success of ChannelBridged when given channel is bridged by a bridge creator 'BridgeWait'" in {
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelBridgeResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelInBridgeWait(
            channelTracker.ref,
            graphTracker.ref,
            channel,
            promise
          )
        )
      )

      actor ! PathsFromChannel(
        channel,
        Set(
          AsteriskPath(NodeBridge("b1", Some(new BridgeCreator("BridgeWait"))))
        )
      )

      whenReady(promise.future) { r =>
        r should be(ChannelBridged)
      }

    }

    "complete the promise with a failure of ChannelDead when given channel is hung up" in {

      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelBridgeResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelInBridgeWait(
            channelTracker.ref,
            graphTracker.ref,
            channel,
            promise
          )
        )
      )

      actor ! Channel(
        "123456789.123",
        channel.name,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HUNGUP
      )

      whenReady(promise.future.failed) { e =>
        e should be(ChannelDead)
      }
    }

    "be wrapped in a future" in {
      val channelTracker                 = TestProbe()
      val graphTracker                   = TestProbe()
      val channel                        = NodeChannel("SIP/abcd-000001")
      val dummyActor                     = TestProbe()
      val promise                        = Promise[ChannelBridgeResult]()
      implicit val context: ActorContext = mock[ActorContext]

      Mockito.when(context.actorOf(any[Props])).thenReturn(dummyActor.ref)
      Mockito.when(context.system).thenReturn(system)
      Mockito.when(context.dispatcher).thenReturn(system.dispatcher)

      val f = WaitForChannelInBridgeWait(
        channelTracker.ref,
        graphTracker.ref,
        channel
      )

      f shouldBe a[Future[_]]
    }

    "handle timeout in the wrapping future" in {
      val channelTracker = TestProbe()
      val graphTracker   = TestProbe()
      val testProbe      = TestProbe()
      val channel        = NodeChannel("SIP/abcd-000001")
      val promise        = Promise[ChannelBridgeResult]()

      class MyExecutor(ref: ActorRef) extends Actor {
        import org.apache.pekko.pattern.pipe

        implicit val ec: ExecutionContextExecutor = context.dispatcher

        pipe(
          WaitForChannelInBridgeWait(
            channelTracker.ref,
            graphTracker.ref,
            channel,
            Timeout(10.millis)
          )
        ) to self

        override def receive: Receive = {
          case org.apache.pekko.actor.Status.Failure(t) =>
            ref ! t
        }
      }

      val a = system.actorOf(Props(new MyExecutor(testProbe.ref)))

      testProbe.expectMsg(ChannelWaitTimeout(channel.name))

    }

  }

}
