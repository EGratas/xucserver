package services.calltracking

import org.apache.pekko.actor.*
import org.apache.pekko.testkit.*
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.{Publish, Subscribe}
import org.asteriskjava.manager.action.MuteAudioAction
import org.asteriskjava.manager.event.*
import org.asteriskjava.manager.response.ManagerResponse
import org.joda.time.{DateTime, Seconds}
import org.mockito.Mockito.*
import org.scalatest.*
import org.scalatestplus.mockito.MockitoSugar
import services.XucAmiBus.{AmiAction, AmiResponse, AmiType}
import services.calltracking.BaseTracker.*
import services.calltracking.ConferenceTracker.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.{AmiEventHelper, MediatorWrapper, XucAmiBus}
import xivo.models.{ConferenceFactory, MeetMeConference}
import xivo.xuc.XucConfig
import xivo.xucami.models.{CallerId, Channel, ChannelState}

import scala.concurrent.Future
import scala.concurrent.duration.*
import scala.language.postfixOps
import pekkotest.TestKitSpec
import scala.reflect.ClassTag

class MediatorHelperSpec()(implicit probe: TestProbe) extends MediatorWrapper {
  override def getMediator(acSys: ActorSystem): ActorRef = probe.ref
}

class ConferenceTrackerSpec
    extends TestKitSpec("CallTracker")
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AmiEventHelper {

  type LeaveOrJoin = (ActorRef, String, Int, String, String, String) => Unit

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }
  val xivoHost = "10.10.10.10"
  def requestConf(
      testRef: ActorRef,
      confNo: String
  ): ConferenceRoom = {
    testRef ! GetConference(confNo)
    fishForSpecificMessage(
      100.millis,
      "assertion failed: Timeout: No ConferenceRoom received"
    )({ case c: ConferenceRoom =>
      c
    })
  }

  def leaveOrJoin(amiEvent: AbstractMeetMeEvent, mds: String): LeaveOrJoin = {
    (ref, channel, index, callerName, callerNum, confNo) =>
      amiEvent.setChannel(channel)
      amiEvent.setUser(index)
      amiEvent.setCallerIdName(callerName)
      amiEvent.setCallerIdNum(callerNum)
      amiEvent.setMeetMe(confNo)

      ref ! AmiEvent(amiEvent, mds)
  }

  def join: LeaveOrJoin  = leaveOrJoin(new MeetMeJoinEvent(this), "default")
  def leave: LeaveOrJoin = leaveOrJoin(new MeetMeLeaveEvent(this), "default")
  def joinMds(mds: String): LeaveOrJoin =
    leaveOrJoin(new MeetMeJoinEvent(this), mds)

  abstract class CreateTracker(defaultConf: Option[MeetMeConference] = None) {
    implicit val mediator: TestProbe   = TestProbe()
    val mediatorHelper                 = new MediatorHelperSpec
    val xucAmiBus: XucAmiBus           = mock[XucAmiBus]
    val confFactory: ConferenceFactory = mock[ConferenceFactory]
    val confList: List[MeetMeConference] =
      defaultConf.map(List(_)).getOrElse(List.empty)
    val xucConfig: XucConfig = mock[XucConfig]
    when(xucConfig.enableAmiTransfer).thenReturn(true)
    when(xucConfig.amiTransferTimeoutSeconds).thenReturn(180)
    when(xucConfig.conferenceRefreshTimerSeconds).thenReturn(150)
    when(xucConfig.xivoHost).thenReturn(xivoHost)
    val schedulerMock: Scheduler  = mock[Scheduler]
    val deviceTracker: TestProbe  = TestProbe()
    val channelTracker: TestProbe = TestProbe()
    when(confFactory.all()).thenReturn(Future.successful(confList))
    val ref: TestActorRef[ConferenceTracker] = TestActorRef[ConferenceTracker](
      new ConferenceTracker(
        xucAmiBus,
        confFactory,
        channelTracker.ref,
        xucConfig,
        mediatorHelper
      ) {
        override def scheduler: Scheduler = schedulerMock
      }
    )
  }

  "ConferenceRoomTracker" should {

    val defaultConf = Some(MeetMeConference(1, 4, "My Conf", "4000", "default"))
    val defaultParticipant = ConferenceParticipant(
      "4000",
      4,
      "sip/abcd-001",
      CallerId("Erwan Sevellec", "1001"),
      DateTime.now
    )

    def expectPublishOf[T: ClassTag](implicit mediator: TestProbe) = {

      var msgOpt: Option[T] = None
      mediator.fishForMessage() {
        case Publish(_, msg: T, _) =>
          msgOpt = Some(msg)
          true
        case _ => false
      }

      msgOpt
    }

    def expectSubscribe(implicit mediator: TestProbe): Option[Subscribe] = {
      var msgOpt: Option[Subscribe] = None
      mediator.fishForMessage() {
        case s: Subscribe =>
          msgOpt = Some(s)
          true
        case _ => false
      }
      msgOpt
    }

    "subscribe to AmiEvent through the XucAmiBus" in new CreateTracker {
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiEvent)
    }

    "subscribe to AmiResponse through the XucAmiBus" in new CreateTracker {
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiResponse)
    }

    "load conference room from database" in new CreateTracker {
      verify(confFactory, timeout(500)).all()
    }

    "get conference by number" in new CreateTracker(defaultConf) {
      val expected: ConferenceRoom = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceAvailable,
        None,
        List.empty,
        "default"
      )

      verify(confFactory, timeout(500)).all()
      requestConf(ref, "4000") shouldBe expected
    }

    "send an error message if conference is not found when requested" in new CreateTracker {
      verify(confFactory, timeout(500)).all()

      ref ! GetConference("4000")
      expectMsg(NoConferenceRoom("4000"))
    }

    "track participant in a dynamic conference" in new CreateTracker(None) {
      joinMds("mds1")(
        ref,
        "sip/abcd-001",
        4,
        "Erwan Sevellec",
        "1001",
        "dynamic-conf-no1"
      )

      val conf: ConferenceRoom = requestConf(ref, "dynamic-conf-no1")
      conf.mds shouldBe "mds1"
    }

    "track participant in a dynamic conference on a mds" in new CreateTracker(
      None
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "dynamic-conf-no1")

      val conf: ConferenceRoom = requestConf(ref, "dynamic-conf-no1")
      conf.participants.size shouldBe 1
      val p: ConferenceParticipant = conf.participants.head
      p.channelName shouldBe "sip/abcd-001"
      p.index shouldBe 4
      p.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      Seconds.secondsBetween(p.joinTime, DateTime.now).getSeconds should be < 5
    }

    "track new participant in a conference and request associated channel" in new CreateTracker(
      defaultConf
    ) {
      private val chanName  = "sip/abcd-001"
      private val confTopic = "10.181.11.2_abcd-001"
      join(ref, chanName, 4, "Erwan Sevellec", "1001", "4000")
      ref ! GetParticipantConfRole(chanName)

      val conf: ConferenceRoom = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      val p: ConferenceParticipant = conf.participants.head
      p.channelName shouldBe chanName
      p.index shouldBe 4
      p.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      Seconds.secondsBetween(p.joinTime, DateTime.now).getSeconds should be < 5
    }

    "track a removed participant in a conference and unsubscribe from ChannelTracker" in new CreateTracker(
      defaultConf
    ) {
      private val chanName1  = "sip/abcd-001"
      private val chanName2  = "sip/abcd-002"
      private val confNumber = "4000"
      join(ref, chanName1, 4, "Erwan Sevellec", "1001", confNumber)
      expectPublishOf[ParticipantJoinConference]

      join(ref, chanName2, 5, "Jp Thomasset", "1002", confNumber)
      expectPublishOf[ParticipantJoinConference]

      leave(ref, chanName2, 4, "Erwan Sevellec", "1001", confNumber)
      expectPublishOf[ParticipantLeaveConference]

      val conf: ConferenceRoom = requestConf(ref, confNumber)

      conf.participants.size shouldBe 1
      val p: ConferenceParticipant = conf.participants.head

      p.channelName shouldBe chanName2
      p.index shouldBe 5
      p.callerId shouldBe CallerId("Jp Thomasset", "1002")
    }

    "notify subscribers of new participant in a conference" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")
      val evt: ParticipantJoinConference =
        expectPublishOf[ParticipantJoinConference].get

      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe "sip/abcd-001"
      evt.participant.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      Seconds
        .secondsBetween(evt.participant.joinTime, DateTime.now)
        .getSeconds should be < 5
    }

    "notify subscribers of removed participant in a conference" in new CreateTracker(
      defaultConf
    ) {
      private val chanName = "sip/abcd-001"
      join(ref, chanName, 4, "Erwan Sevellec", "1001", "4000")
      expectPublishOf[ParticipantJoinConference]
      leave(ref, chanName, 4, "Erwan Sevellec", "1001", "4000")
      val evt: ParticipantLeaveConference =
        expectPublishOf[ParticipantLeaveConference].get

      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe chanName
      evt.participant.callerId shouldBe CallerId("Erwan Sevellec", "1001")
    }

    "notify subscribers when conference room become busy " in new CreateTracker(
      defaultConf
    ) {
      ref ! SubscribeToConferenceStatus("4000")

      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      expectMsg(ConferenceStatusChange("4000", ConferenceBusy))
    }

    "notify subscribers when conference room become available " in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")
      expectPublishOf[DeviceJoinConference]

      ref ! SubscribeToConferenceStatus("4000")

      leave(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")
      expectPublishOf[DeviceLeaveConference]

      expectMsg(ConferenceStatusChange("4000", ConferenceAvailable))
    }

    "unsubscribe to conference status" in new CreateTracker(defaultConf) {

      ref ! SubscribeToConferenceStatus("4000")
      ref ! UnsubscribeConferenceStatus("4000")

      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      expectNoMessage(500.millis)
    }

    "notify mediator when a device join a conference" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")
      val msg: DeviceJoinConference = expectPublishOf[DeviceJoinConference].get
      msg.channel shouldBe "sip/abcd-001"
      msg.conference.number shouldBe "4000"
      msg.topic shouldBe ConferenceTracker.conferenceParticipantEventTopic(
        msg.conference.number,
        xivoHost
      )
    }

    "notify mediator when a device leave a conference" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val joinMsg: DeviceJoinConference =
        expectPublishOf[DeviceJoinConference].get
      joinMsg.channel shouldBe "sip/abcd-001"
      joinMsg.conference.number shouldBe "4000"

      leave(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")
      val leaveMsg: DeviceLeaveConference =
        expectPublishOf[DeviceLeaveConference].get
      leaveMsg.channel shouldBe "sip/abcd-001"
      leaveMsg.conference.number shouldBe "4000"
      leaveMsg.topic shouldBe ConferenceTracker.conferenceParticipantEventTopic(
        leaveMsg.conference.number,
        xivoHost
      )
    }

    "use the scheduler" in new CreateTracker(defaultConf) {
      verify(schedulerMock, timeout(500)).scheduleWithFixedDelay(
        xucConfig.conferenceRefreshTimerSeconds second,
        xucConfig.conferenceRefreshTimerSeconds second,
        ref,
        RefreshConference
      )(ref.underlyingActor.context.dispatcher, ref)
    }

    "reload conference room from database" in new CreateTracker(defaultConf) {
      verify(confFactory, timeout(5000)).all()
    }

    "update confList if a conf is removed" in new CreateTracker(defaultConf) {
      when(confFactory.all()).thenReturn(Future.successful(List.empty))
      ref ! RefreshConference
      ref ! GetConference("4000")
      expectMsg(NoConferenceRoom("4000"))
    }

    "update confList if a conf is add" in new CreateTracker(defaultConf) {
      val firstConf: MeetMeConference =
        MeetMeConference(1, 4, "My Conf", "4000", "default")
      val newConf: MeetMeConference =
        MeetMeConference(2, 5, "My new Conf", "4001", "default")
      val expected: List[MeetMeConference] = List(firstConf, newConf)
      when(confFactory.all()).thenReturn(Future.successful(expected))
      ref ! RefreshConference
      requestConf(ref, "4001") shouldBe ConferenceRoom(newConf)
    }

    "update confList if a participant begin to speak" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val talkingEvent = new MeetMeTalkingEvent(this)
      talkingEvent.setChannel("sip/abcd-001")
      talkingEvent.setUser(4)
      talkingEvent.setCallerIdName("Erwan Sevellec")
      talkingEvent.setCallerIdNum("1001")
      talkingEvent.setMeetMe("4000")
      talkingEvent.setStatus(true)

      ref ! AmiEvent(talkingEvent)

      val conf: ConferenceRoom = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isTalking shouldBe true
    }

    "update confList if a participant stop to speak" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val talkingEventTrue = new MeetMeTalkingEvent(this)
      talkingEventTrue.setChannel("sip/abcd-001")
      talkingEventTrue.setUser(4)
      talkingEventTrue.setCallerIdName("Erwan Sevellec")
      talkingEventTrue.setCallerIdNum("1001")
      talkingEventTrue.setMeetMe("4000")
      talkingEventTrue.setStatus(true)

      ref ! AmiEvent(talkingEventTrue)

      val talkingEventFalse = new MeetMeTalkingEvent(this)
      talkingEventFalse.setChannel("sip/abcd-001")
      talkingEventFalse.setUser(4)
      talkingEventFalse.setCallerIdName("Erwan Sevellec")
      talkingEventFalse.setCallerIdNum("1001")
      talkingEventFalse.setMeetMe("4000")
      talkingEventFalse.setStatus(false)

      ref ! AmiEvent(talkingEventFalse)

      val conf: ConferenceRoom = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isTalking shouldBe false
    }

    "notify if a participant begin to speak" in new CreateTracker(defaultConf) {
      private val chanName = "sip/abcd-001"
      join(ref, chanName, 4, "Erwan Sevellec", "1001", "4000")
      expectPublishOf[ParticipantJoinConference]

      val talkingEvent = new MeetMeTalkingEvent(this)
      talkingEvent.setChannel(chanName)
      talkingEvent.setUser(4)
      talkingEvent.setCallerIdName("Erwan Sevellec")
      talkingEvent.setCallerIdNum("1001")
      talkingEvent.setMeetMe("4000")
      talkingEvent.setStatus(true)

      ref ! AmiEvent(talkingEvent)

      val evt: ParticipantUpdated = expectPublishOf[ParticipantUpdated].get
      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe chanName
      evt.participant.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      evt.participant.isTalking shouldBe true
    }

    "notify if a participant stop speaking" in new CreateTracker(defaultConf) {
      private val chanName = "sip/abcd-001"
      join(ref, chanName, 4, "Erwan Sevellec", "1001", "4000")
      expectPublishOf[ParticipantJoinConference]
      val talkingEventTrue = new MeetMeTalkingEvent(this)
      talkingEventTrue.setChannel(chanName)
      talkingEventTrue.setUser(4)
      talkingEventTrue.setCallerIdName("Erwan Sevellec")
      talkingEventTrue.setCallerIdNum("1001")
      talkingEventTrue.setMeetMe("4000")
      talkingEventTrue.setStatus(true)

      ref ! AmiEvent(talkingEventTrue)
      expectPublishOf[ParticipantUpdated]

      val talkingEventFalse = new MeetMeTalkingEvent(this)
      talkingEventFalse.setChannel(chanName)
      talkingEventFalse.setUser(4)
      talkingEventFalse.setCallerIdName("Erwan Sevellec")
      talkingEventFalse.setCallerIdNum("1001")
      talkingEventFalse.setMeetMe("4000")
      talkingEventFalse.setStatus(false)

      ref ! AmiEvent(talkingEventFalse)

      val evt: ParticipantUpdated = expectPublishOf[ParticipantUpdated].get
      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe chanName
      evt.participant.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      evt.participant.isTalking shouldBe false
    }

    "do not notify of participant role change when user as it's the default" in new CreateTracker(
      defaultConf
    ) {
      private val confName = "sip/abcd-001"
      join(ref, confName, 4, "Erwan Sevellec", "1001", "4000")
      expectPublishOf[DeviceJoinConference]

      val channel: Channel = Channel(
        "123456789.123",
        confName,
        CallerId("Erwan Sevellec", "1001"),
        "",
        ChannelState.UP
      )
        .updateVariable("XIVO_MEETMEROLE", "USER")

      ref ! channel
      mediator.expectNoMessage(250.millis)
    }

    "notify of participant role change when user is detected as organizer" in new CreateTracker(
      defaultConf
    ) {
      private val chanName = "sip/efgh-002"
      join(ref, "sip/efgh-002", 4, "James Bond", "1007", "4000")
      expectPublishOf[ParticipantJoinConference]

      ref ! GetParticipantConfRole(chanName)

      channelTracker expectMsg WatchChannelStartingWith(chanName)

      val channel: Channel = Channel(
        "123456702.456",
        "sip/efgh-002",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
        .updateVariable("XIVO_MEETMEROLE", "ADMIN")

      ref ! channel

      val evt: ParticipantUpdated = expectPublishOf[ParticipantUpdated].get
      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe "sip/efgh-002"
      evt.participant.role shouldBe OrganizerRole
    }

    "update confList if a participant is muted" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val evt = new MeetMeMuteEvent(this)
      evt.setChannel("sip/abcd-001")
      evt.setUser(4)
      evt.setCallerIdName("Erwan Sevellec")
      evt.setCallerIdNum("1001")
      evt.setMeetMe("4000")
      evt.setStatus(true)

      ref ! AmiEvent(evt)

      val conf: ConferenceRoom = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isMuted shouldBe true
    }

    "update confList if a participant is UN-muted" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val evt = new MeetMeMuteEvent(this)
      evt.setChannel("sip/abcd-001")
      evt.setUser(4)
      evt.setCallerIdName("Erwan Sevellec")
      evt.setCallerIdNum("1001")
      evt.setMeetMe("4000")
      evt.setStatus(true)
      ref ! AmiEvent(evt)

      evt.setStatus(false)
      ref ! AmiEvent(evt)

      val conf: ConferenceRoom = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isMuted shouldBe false
    }

    "update confList if a participant is deafened" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "James Bond", "1001", "4000")

      val action = new MuteAudioAction()
      action.setChannel("sip/abcd-001")
      action.setDirection(MuteAudioAction.Direction.OUT)
      action.setState(MuteAudioAction.State.MUTE)

      val evt = new ManagerResponse()
      evt.setResponse("Success")

      ref ! AmiResponse((evt, Some(AmiAction(action))))

      val conf: ConferenceRoom = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isDeaf shouldBe true
    }

    "update confList if a participant is UN-deafened" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "James Bond", "1001", "4000")

      val action = new MuteAudioAction()
      action.setChannel("sip/abcd-001")
      action.setDirection(MuteAudioAction.Direction.OUT)
      action.setState(MuteAudioAction.State.UNMUTE)

      val evt = new ManagerResponse()
      evt.setResponse("Success")

      ref ! AmiResponse((evt, Some(AmiAction(action))))

      val conf: ConferenceRoom = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isDeaf shouldBe false
    }

    "Subscribe to mediator conference events" in new CreateTracker(
      defaultConf
    ) {
      expectSubscribe match {
        case Some(subscribe) =>
          subscribe.topic shouldBe ConferenceTracker.conferenceEventTopic
        case None => fail("No subscribe message received")
      }
    }
  }
}
