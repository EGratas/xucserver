package services.calltracking

import org.apache.pekko.actor.*
import org.apache.pekko.testkit.*
import models.XivoUser
import org.joda.time.DateTime
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.scalatest.*
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.AsteriskGraphTracker.{AsteriskPath, PathsFromChannel}
import services.calltracking.BaseTracker.{UnWatchChannelStartingWith, WatchChannelStartingWith}
import services.calltracking.graph.NodeBridge.BridgeCreator
import services.calltracking.graph.*
import xivo.phonedevices.*
import xivo.xucami.models.*

import scala.concurrent.{ExecutionContextExecutor, Promise}
import scala.concurrent.duration.*
import xivo.models.Line

class TransferUtilSpec
    extends TestKit(ActorSystem("TransferUtilSpec"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with BeforeAndAfterAll
    with ScalaFutures
    with AsteriskObjectHelper {
  import xivo.models.LineHelper.makeLine

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class HelperComplete {

    /** Example calls to transfer :
      * Phone1 - Channel(c1) - Bridge - Channel(rc1) - Phone2
      *       \- Channel(c2) - LocalChannel(lc;1) - LocalBridge - LocalChannel(lc;2) - Channel(rc2) - Phone 3
      *
      *  The steps to transfer are
      *  - First, we put everyone in a BridgeWait bridge
      *  - Then we hangup c1 & c2
      *  - At the same time we wait for lc;1 to be up
      *  - When lc;1 is up, we bridge it with rc1 and enable optimization on lc;1
      *
      *  All these steps are done asynchronously and we wait for channels to be bridged
      *  before moving to the next step
      */
    val channelTracker: TestProbe = TestProbe()
    val graphTracker: TestProbe   = TestProbe()
    val amiBus: TestProbe         = TestProbe()
    val testProbe: TestProbe      = TestProbe()
    val channel: NodeChannel        = NodeChannel("SIP/abcd-000001")
    val promise: Promise[ChannelBridgeResult]        = Promise[ChannelBridgeResult]()
    val transferUtilImpl =
      new TransferUtilImpl(channelTracker.ref, graphTracker.ref, amiBus.ref)
    val agentNum: Some[String] = Some("2001")

    val calls: CallsForTransfer = CallsForTransfer(
      CallChannelsForTransfer(NodeChannel("c1"), NodeChannel("rc1")),
      CallChannelsForTransfer(NodeChannel("c2"), NodeLocalChannel("lc;1")),
      Some("987654321.321")
    )

    class MyExecutor(
        ref: ActorRef,
        timeout: FiniteDuration,
        agentNum: Option[String] = None
    ) extends Actor {
      implicit val ec: ExecutionContextExecutor = context.dispatcher

      transferUtilImpl.completeTransfer(calls, timeout, agentNum)

      override def receive: PartialFunction[Any,Unit] = {
        case org.apache.pekko.actor.Status.Failure(t) => ref ! t
        case o                            => ref ! o
      }
    }

    def initiateTransfer(
        timeout: FiniteDuration = 1.minute,
        agentNum: Option[String] = None
    ): Map[String, ActorRef] = {
      val a =
        system.actorOf(Props(new MyExecutor(testProbe.ref, timeout, agentNum)))

      // Set hangup source for channel connected to local channel
      amiBus.expectMsg(SetVarCommand("c2", "CHANNEL(hangupsource)", "c2"))

      // Expect all channels to be put in a waiting bridge
      amiBus.expectMsg(
        SetVarCommand(
          calls.call1.local.name,
          "XIVO_CHANNEL2_LINKEDID",
          "987654321.321"
        )
      )

      amiBus.expectMsg(
        MultipleRedirectCommand(
          "rc1",
          "xuc_attended_xfer_wait",
          "s",
          1,
          "c1",
          "xuc_attended_xfer_wait",
          "s",
          1
        )
      )

      amiBus.expectMsg(
        MultipleRedirectCommand(
          "lc;1",
          "xuc_attended_xfer_wait",
          "s",
          1,
          "c2",
          "xuc_attended_xfer_wait",
          "s",
          1
        )
      )

      // 4 Channels to watch, inform directly they are bridged
      val watchers = channelTracker
        .receiveWhile(messages = 4)({ case WatchChannelStartingWith(name) =>
          (name, channelTracker.sender())
        })
        .toMap

      watchers.size should be(4)
      watchers
        .get("c1")
        .map(
          _ ! PathsFromChannel(
            NodeChannel("c1"),
            Set(
              AsteriskPath(
                NodeBridge("bridgewait", Some(new BridgeCreator("BridgeWait")))
              )
            )
          )
        )
      watchers
        .get("c2")
        .map(
          _ ! PathsFromChannel(
            NodeChannel("c2"),
            Set(
              AsteriskPath(
                NodeBridge("bridgewait", Some(new BridgeCreator("BridgeWait")))
              )
            )
          )
        )

      // we should hangup c1 & c2 when bridged as they are useless
      amiBus
        .receiveWhile(messages = 2)({
          case HangupChannelCommand("c1") =>
          case HangupChannelCommand("c2") =>
        })
        .size should be(2)

      // Actor watching c1 & c2 should stop
      channelTracker
        .receiveWhile(messages = 2)({
          case UnWatchChannelStartingWith("c1") =>
          case UnWatchChannelStartingWith("c2") =>
        })
        .size should be(2)

      // Inform rc1 & lc;1 are in bridgewait to continue
      watchers
        .get("rc1")
        .map(
          _ ! PathsFromChannel(
            NodeChannel("rc1"),
            Set(
              AsteriskPath(
                NodeBridge("bridgewait", Some(new BridgeCreator("BridgeWait")))
              )
            )
          )
        )
      watchers
        .get("lc;1")
        .map(
          _ ! PathsFromChannel(
            NodeChannel("lc;1"),
            Set(
              AsteriskPath(
                NodeBridge("bridgewait", Some(new BridgeCreator("BridgeWait")))
              )
            )
          )
        )

      // Actor watching rc1 & lc;1 should stop and we should
      // have two new actors watching them. doing it in the same
      // batch as we don't now the order of the received messsages
      val msgReceived = channelTracker.receiveWhile(messages = 4)({
        case UnWatchChannelStartingWith("rc1")  => None
        case UnWatchChannelStartingWith("lc;1") => None
        case WatchChannelStartingWith(name) =>
          Some((name, channelTracker.sender()))
      })

      msgReceived.size should be(4)
      val watchers2 = msgReceived.flatten.toMap
      watchers2.size should be(2)
      watchers2
    }
  }

  class HelperAttendedTransfer {
    val channelTracker: TestProbe = TestProbe()
    val graphTracker: TestProbe   = TestProbe()
    val amiBus: TestProbe         = TestProbe()
    val phoneNumber    = "1000"
    val line: Line = makeLine(
      1,
      "userContext",
      "sip",
      "abcd",
      None,
      None,
      "123.123.123.123",
      false,
      Some(phoneNumber)
    )
    val xivoUser: XivoUser =
      XivoUser(1, None, None, "James", Some("Bond"), Some("jbond"), None, None, None)
    val testProbe: TestProbe     = TestProbe()
    val deviceAdapter: DeviceAdapter = mock[DeviceAdapter]
    val transferUtil =
      new TransferUtilImpl(channelTracker.ref, graphTracker.ref, amiBus.ref)

    class MyExecutor(
        ref: ActorRef,
        calls: Map[String, DeviceCall],
        destination: String
    ) extends Actor {
      import org.apache.pekko.pattern.pipe

      implicit val ec: ExecutionContextExecutor = context.dispatcher

      transferUtil
        .attendedTransfer(
          calls,
          destination,
          None,
          line,
          xivoUser,
          deviceAdapter,
          self
        )
        .pipeTo(self)

      override def receive: PartialFunction[Any,Unit] = {
        case org.apache.pekko.actor.Status.Failure(t) => ref ! t
        case o                            => ref ! o
      }
    }

    def attendedTransfer(
        calls: Map[String, DeviceCall],
        destination: String
    ): ActorRef = {
      system.actorOf(Props(new MyExecutor(testProbe.ref, calls, destination)))
    }

  }

  "TransferUtil" should {

    "extract call channels to prepare transfer" in new HelperAttendedTransfer {

      val path1: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b1", Some(new BridgeCreator("bc1"))),
        NodeChannel("rc1")
      )
      val call1: DeviceCall = DeviceCall("c1", None, Set(path1), Map.empty)
      val lc1: NodeLocalChannel = NodeLocalChannel("lc1")
      val lc2: NodeLocalChannel = NodeLocalChannel("lc2")
      val path2: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b3", Some(new BridgeCreator("bc2"))),
        lc1,
        NodeLocalBridge(lc1, lc2),
        lc2,
        NodeBridge("b4", Some(new BridgeCreator("bc14"))),
        NodeChannel("rc2")
      )
      val call2: DeviceCall = DeviceCall("c2", None, Set(path2), Map.empty)

      transferUtil.getChannelsForTransfer(
        Map("c1" -> call1, "c2" -> call2)
      ) should be(
        Some(
          CallsForTransfer(
            CallChannelsForTransfer(NodeChannel("c1"), NodeChannel("rc1")),
            CallChannelsForTransfer(NodeChannel("c2"), NodeLocalChannel("lc1"))
          )
        )
      )

    }

    "extract correct local channel on first call to allow multiple transfer" in new HelperAttendedTransfer {
      /* This is the case when a user receives a call through a local channel and try to transfer it.
       * This happen when the call he receives a call transferred by someone else. Then we should take the first leg
       * of the local channel and transfer it
       */
      val rc1: NodeChannel = NodeChannel("rc1")
      val lc1_1: NodeLocalChannel = NodeLocalChannel("lc1;1")
      val lc1_2: NodeLocalChannel = NodeLocalChannel("lc1;2")
      val path1: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b1", Some(new BridgeCreator("bc1"))),
        lc1_2,
        NodeLocalBridge(lc1_2, lc1_1),
        lc1_1,
        NodeBridge("b2", Some(new BridgeCreator("bc2"))),
        rc1
      )
      val call1: DeviceCall = DeviceCall("c1", None, Set(path1), Map.empty)

      val rc2: NodeChannel = NodeChannel("rc2")
      val lc2_1: NodeLocalChannel = NodeLocalChannel("lc2;1")
      val lc2_2: NodeLocalChannel = NodeLocalChannel("lc2;2")
      val path2: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b3", Some(new BridgeCreator("bc3"))),
        lc2_1,
        NodeLocalBridge(lc2_1, lc2_2),
        lc2_2,
        NodeBridge("b4", Some(new BridgeCreator("bc4"))),
        rc2
      )
      val call2: DeviceCall = DeviceCall("c2", None, Set(path2), Map.empty)

      transferUtil.getChannelsForTransfer(
        Map("c1" -> call1, "c2" -> call2)
      ) should be(
        Some(
          CallsForTransfer(
            CallChannelsForTransfer(NodeChannel("c1"), lc1_2),
            CallChannelsForTransfer(NodeChannel("c2"), lc2_1)
          )
        )
      )

    }

    "extract call channels to prepare transfer even with artifacts NodeMdsTrunkBridge" in new HelperAttendedTransfer {

      val path1_1: List[AsteriskObject] = AsteriskPath(
        NodeMdsTrunkBridge("1df85422689e32e609604af05c45efe4@10.49.0.2:5060")
      )
      val path1_2: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b1", Some(new BridgeCreator("bc1"))),
        NodeChannel("rc1"),
        NodeMdsTrunkBridge("356129493ce779ee2a68a76b2ef1e8d9@10.49.0.2:5060")
      )
      val call1: DeviceCall = DeviceCall("c1", None, Set(path1_1, path1_2), Map.empty)
      val lc1: NodeLocalChannel = NodeLocalChannel("lc1")
      val lc2: NodeLocalChannel = NodeLocalChannel("lc2")
      val path2_1: List[AsteriskObject] = AsteriskPath(NodeMdsTrunkBridge("0_3588786575@10.49.0.107"))
      val path2_2: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b3", Some(new BridgeCreator("bc3"))),
        lc1,
        NodeLocalBridge(lc1, lc2),
        lc2,
        NodeBridge("b4", Some(new BridgeCreator("bc4"))),
        NodeChannel("rc2"),
        NodeMdsTrunkBridge("2f6abcb1252662ee6ff0768d27ebc96f@10.49.0.2:5060")
      )
      val call2: DeviceCall = DeviceCall("c2", None, Set(path2_1, path2_2), Map.empty)

      transferUtil.getChannelsForTransfer(
        Map("c1" -> call1, "c2" -> call2)
      ) should be(
        Some(
          CallsForTransfer(
            CallChannelsForTransfer(NodeChannel("c1"), NodeChannel("rc1")),
            CallChannelsForTransfer(NodeChannel("c2"), NodeLocalChannel("lc1"))
          )
        )
      )

    }

    "set remoteId of second channel to prepare the transfer" in new HelperAttendedTransfer {

      val path1: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b1", Some(new BridgeCreator("bc1"))),
        NodeChannel("rc1")
      )
      val call1: DeviceCall = DeviceCall("c1", None, Set(path1), Map.empty)
      val lc1: NodeLocalChannel = NodeLocalChannel("lc1")
      val lc2: NodeLocalChannel = NodeLocalChannel("lc2")
      val path2: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b3", Some(new BridgeCreator("bc3"))),
        lc1,
        NodeLocalBridge(lc1, lc2),
        lc2,
        NodeBridge("b4", Some(new BridgeCreator("bc4"))),
        NodeChannel("rc2")
      )
      val cname = "SIP/abcd-0001"
      val channel2: Channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "987654321.321",
        ChannelState.HOLD
      )

      val call2: DeviceCall = DeviceCall("c2", Some(channel2), Set(path2), Map.empty)

      transferUtil.getChannelsForTransfer(
        Map("c1" -> call1, "c2" -> call2)
      ) should be(
        Some(
          CallsForTransfer(
            CallChannelsForTransfer(NodeChannel("c1"), NodeChannel("rc1")),
            CallChannelsForTransfer(NodeChannel("c2"), NodeLocalChannel("lc1")),
            Some("987654321.321")
          )
        )
      )

    }

    "complete transfer using ami command" in new HelperComplete {

      val watchers: Map[BridgeCreator, ActorRef] = initiateTransfer()
      val linkedId = "987654321.321"
      // Transfer Destination answers
      watchers
        .get("lc;1")
        .map(
          _ ! Channel(
            "123456789.123",
            "lc;1",
            CallerId("James Bond", "1007"),
            "",
            ChannelState.UP
          )
        )

      // Redirect caller to dialplan which will bridge everything
      amiBus.expectMsg(SetVarCommand("rc1", "XIVO_CHAN_TO_BRIDGE", "lc;1"))
      amiBus.expectMsg(SetVarCommand("rc1", "XIVO_CHANNEL2_LINKEDID", linkedId))
      amiBus.expectMsg(RedirectCommand("rc1", "xuc_attended_xfer_end", "s", 1))
      // Expect channel optimization on local channel
      amiBus.expectMsg(LocalOptimizeAwayCommand("lc;1"))

      // everyone should unwatch
      // - lc;1 watcher because its work is done
      // - rc1 watcher because it has been cancelled
      channelTracker
        .receiveWhile(messages = 2)({ case UnWatchChannelStartingWith(_) =>
        })
        .size should be(2)
    }

    "complete transfer with agent number using ami command" in new HelperComplete {

      val watchers: Map[BridgeCreator, ActorRef] = initiateTransfer(agentNum = Some("2001"))
      val linkedId = "987654321.321"
      // Transfer Destination answers
      watchers
        .get("lc;1")
        .map(
          _ ! Channel(
            "123456789.123",
            "lc;1",
            CallerId("James Bond", "1007"),
            "",
            ChannelState.UP
          )
        )

      // Redirect caller to dialplan which will bridge everything
      amiBus.expectMsg(SetVarCommand("rc1", "XIVO_CHAN_TO_BRIDGE", "lc;1"))
      amiBus.expectMsg(SetVarCommand("rc1", "XIVO_CHANNEL2_LINKEDID", linkedId))
      amiBus.expectMsg(RedirectCommand("rc1", "xuc_attended_xfer_end", "s", 1))
      // Expect channel optimization on local channel
      amiBus.expectMsg(LocalOptimizeAwayCommand("lc;1"))

      amiBus.expectMsg(2500.millis, QueueStatusCommand("Agent/2001"))

      // everyone should unwatch
      // - lc;1 watcher because its work is done
      // - rc1 watcher because it has been cancelled
      channelTracker
        .receiveWhile(messages = 2)({ case UnWatchChannelStartingWith(_) =>
        })
        .size should be(2)
    }

    "extract local channel 2 uniqueId when transferring a call to a queue" in new HelperAttendedTransfer {
      val path1: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b1", Some(new BridgeCreator("bc1"))),
        NodeChannel("rc1")
      )
      val rc1: Channel = Channel(
        "123456789.000",
        "rc1",
        CallerId("Caller", "1001"),
        "123456789.000",
        ChannelState.UP
      )
      val call1: DeviceCall = DeviceCall("c1", None, Set(path1), Map(rc1.name -> rc1))

      val lc1: NodeLocalChannel = NodeLocalChannel("lc;1")
      val lc1Channel: Channel = Channel(
        "123456789.001",
        "lc;1",
        CallerId("James Bond", "1007"),
        "123456789.001",
        ChannelState.UP
      )
      val lc2: NodeLocalChannel = NodeLocalChannel("lc;2")
      val enterQueue: EnterQueue = EnterQueue(
        "cars",
        "123456789.002",
        QueueCall(
          1,
          Some("James Bond"),
          "1007",
          new DateTime(),
          "123456789.002",
          "main"
        ),
        "lc;2"
      )
      val lc2Channel: Channel = Channel(
        "123456789.002",
        "lc;2",
        CallerId("James Bond", "1007"),
        "123456789.002",
        ChannelState.UP
      ).withQueueHistory(enterQueue)
      val path2: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b3", Some(new BridgeCreator("bc3"))),
        lc1,
        NodeLocalBridge(lc1, lc2),
        lc2,
        NodeBridge("b4", Some(new BridgeCreator("bc4"))),
        NodeChannel("rc2")
      )
      val cname = "SIP/abcd-0001"
      val channel2: Channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "987654321.321",
        ChannelState.HOLD
      )

      val call2: DeviceCall = DeviceCall(
        "c2",
        Some(channel2),
        Set(path2),
        Map(lc1Channel.name -> lc1Channel, lc2Channel.name -> lc2Channel)
      )

      transferUtil.getChannelsForTransfer(
        Map("c1" -> call1, "c2" -> call2)
      ) should be(
        Some(
          CallsForTransfer(
            CallChannelsForTransfer(NodeChannel("c1"), NodeChannel("rc1")),
            CallChannelsForTransfer(
              NodeChannel("c2"),
              NodeLocalChannel("lc;1")
            ),
            Some("987654321.321"),
            Some(QueueTransferEvent("cars", "123456789.002", "123456789.000"))
          )
        )
      )
    }

    "extract local channel 2 uniqueId when transferring a call to a queue from a queue" in new HelperAttendedTransfer {
      val path1: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b1", Some(new BridgeCreator("bc1"))),
        NodeChannel("rc1")
      )
      val rc1: Channel = Channel(
        "123456789.000",
        "rc1",
        CallerId("Caller", "1001"),
        "123456789.000",
        ChannelState.UP
      )
      val call1: DeviceCall = DeviceCall("c1", None, Set(path1), Map(rc1.name -> rc1))

      val lc1: NodeLocalChannel = NodeLocalChannel("lc;1")
      val lc1Channel: Channel = Channel(
        "123456789.001",
        "lc;1",
        CallerId("James Bond", "1007"),
        "123456789.001",
        ChannelState.UP
      )
      val lc2: NodeLocalChannel = NodeLocalChannel("lc;2")
      val enterQueue: EnterQueue = EnterQueue(
        "cars",
        "123456789.002",
        QueueCall(
          1,
          Some("James Bond"),
          "1007",
          new DateTime(),
          "123456789.002",
          "main"
        ),
        "lc;2"
      )
      val lc2Channel: Channel = Channel(
        "123456789.002",
        "lc;2",
        CallerId("James Bond", "1007"),
        "123456789.002",
        ChannelState.UP
      ).withQueueHistory(enterQueue)
      val path2a: List[AsteriskObject] = AsteriskPath(
        NodeMdsTrunkBridge("260ae876748a6cca5e0c41e52cc26a61@0.0.0.0")
      )
      val path2b: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b3", Some(new BridgeCreator("bc3"))),
        lc1,
        NodeLocalBridge(lc1, lc2),
        lc2,
        NodeBridge("b4", Some(new BridgeCreator("bc4"))),
        NodeChannel("rc2")
      )
      val cname = "SIP/abcd-0001"
      val channel2: Channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "987654321.321",
        ChannelState.HOLD
      )

      val call2: DeviceCall = DeviceCall(
        "c2",
        Some(channel2),
        Set(path2a, path2b),
        Map(lc1Channel.name -> lc1Channel, lc2Channel.name -> lc2Channel)
      )

      transferUtil.getChannelsForTransfer(
        Map("c1" -> call1, "c2" -> call2)
      ) should be(
        Some(
          CallsForTransfer(
            CallChannelsForTransfer(NodeChannel("c1"), NodeChannel("rc1")),
            CallChannelsForTransfer(
              NodeChannel("c2"),
              NodeLocalChannel("lc;1")
            ),
            Some("987654321.321"),
            Some(QueueTransferEvent("cars", "123456789.002", "123456789.000"))
          )
        )
      )
    }

    "extract local channel 2 uniqueId when transferring a call to not a queue" in new HelperAttendedTransfer {
      val path1: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b1", Some(new BridgeCreator("bc1"))),
        NodeChannel("rc1")
      )
      val rc1: Channel = Channel(
        "123456789.000",
        "rc1",
        CallerId("Caller", "1001"),
        "123456789.000",
        ChannelState.UP
      )
      val call1: DeviceCall = DeviceCall("c1", None, Set(path1), Map(rc1.name -> rc1))

      val lc1: NodeLocalChannel = NodeLocalChannel("lc;1")
      val lc1Channel: Channel = Channel(
        "123456789.001",
        "lc;1",
        CallerId("James Bond", "1007"),
        "123456789.001",
        ChannelState.UP
      )
      val lc2: NodeLocalChannel = NodeLocalChannel("lc;2")
      val enterQueue: EnterQueue = EnterQueue(
        "cars",
        "123456789.002",
        QueueCall(
          1,
          Some("James Bond"),
          "1007",
          new DateTime(),
          "123456789.00",
          "main"
        ),
        "lc;2"
      )
      val lc2Channel: Channel = Channel(
        "123456789.002",
        "lc;2",
        CallerId("James Bond", "1007"),
        "123456789.002",
        ChannelState.UP
      )
      val path2: List[AsteriskObject] = AsteriskPath(
        NodeBridge("b3", Some(new BridgeCreator("bc3"))),
        lc1,
        NodeLocalBridge(lc1, lc2),
        lc2,
        NodeBridge("b4", Some(new BridgeCreator("bc4"))),
        NodeChannel("rc2")
      )
      val cname = "SIP/abcd-0001"
      val channel2: Channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "987654321.321",
        ChannelState.HOLD
      )

      val call2: DeviceCall = DeviceCall(
        "c2",
        Some(channel2),
        Set(path2),
        Map(lc1Channel.name -> lc1Channel, lc2Channel.name -> lc2Channel)
      )

      transferUtil.getChannelsForTransfer(
        Map("c1" -> call1, "c2" -> call2)
      ) should be(
        Some(
          CallsForTransfer(
            CallChannelsForTransfer(NodeChannel("c1"), NodeChannel("rc1")),
            CallChannelsForTransfer(
              NodeChannel("c2"),
              NodeLocalChannel("lc;1")
            ),
            Some("987654321.321"),
            Some(NoQueueTransferEvent(lc2, "123456789.002", "123456789.000"))
          )
        )
      )
    }

    "send additional queue log event when completing a transfer to a queue to allow statistic tracking" in new HelperComplete {

      override val calls: CallsForTransfer = CallsForTransfer(
        CallChannelsForTransfer(NodeChannel("c1"), NodeChannel("rc1")),
        CallChannelsForTransfer(NodeChannel("c2"), NodeLocalChannel("lc;1")),
        Some("987654321.321"),
        Some(QueueTransferEvent("cars", "123456789.002", "123456789.000"))
      )

      val watchers: Map[BridgeCreator, ActorRef] = initiateTransfer()
      val linkedId = "987654321.321"
      // Transfer Destination answers
      watchers
        .get("lc;1")
        .map(
          _ ! Channel(
            "123456789.123",
            "lc;1",
            CallerId("James Bond", "1007"),
            "",
            ChannelState.UP
          )
        )

      // Redirect caller to dialplan which will bridge everything
      amiBus.expectMsg(SetVarCommand("rc1", "XIVO_CHAN_TO_BRIDGE", "lc;1"))
      amiBus.expectMsg(SetVarCommand("rc1", "XIVO_CHANNEL2_LINKEDID", linkedId))
      amiBus.expectMsg(RedirectCommand("rc1", "xuc_attended_xfer_end", "s", 1))
      // Expect channel optimization on local channel
      amiBus.expectMsg(LocalOptimizeAwayCommand("lc;1"))

      amiBus.expectMsg(
        QueueLogEventCommand(
          "cars",
          QueueLogEventCommand.QueueLogTransferEvent,
          "123456789.002",
          "123456789.000"
        )
      )

      // everyone should unwatch
      // - lc;1 watcher because its work is done
      // - rc1 watcher because it has been cancelled
      channelTracker
        .receiveWhile(messages = 2)({ case UnWatchChannelStartingWith(_) =>
        })
        .size should be(2)

    }

    "abort transfer when caller hangsup during transfer" in new HelperComplete {

      val watchers: Map[BridgeCreator, ActorRef] = initiateTransfer()

      // Caller hangup
      watchers
        .get("rc1")
        .map(
          _ ! Channel(
            "123456789.123",
            "rc1",
            CallerId("James Bond", "1007"),
            "",
            ChannelState.HUNGUP
          )
        )

      // Should trigger hangup "lc;1"
      amiBus.expectMsg(HangupChannelCommand("lc;1"))

      // Ensure the "lc;1" watcher receive the Hangup event
      watchers
        .get("lc;1")
        .map(
          _ ! Channel(
            "123456789.123",
            "lc;1",
            CallerId("Some One", "1001"),
            "",
            ChannelState.HUNGUP
          )
        )

      channelTracker
        .receiveWhile(messages = 2)({ case UnWatchChannelStartingWith(_) =>
        })
        .size should be(2)

    }

    "abort transfer with message if nobody answers after timeout" in new HelperComplete {

      val watchers: Map[BridgeCreator, ActorRef] = initiateTransfer(100.millis)

      // Should trigger hangup "lc;1" & redirect caller to some message in dialplan

      amiBus
        .receiveWhile(500.millis, messages = 2)({
          case HangupChannelCommand("lc;1")                                =>
          case RedirectCommand("rc1", "xuc_attended_xfer_timeout", "s", 1) =>
        })
        .size should be(2)

      channelTracker
        .receiveWhile(messages = 2)({ case UnWatchChannelStartingWith(_) =>
        })
        .size should be(2)

    }

    "initiate a second call for attended transfer if current call is on hold" in new HelperAttendedTransfer {
      val cname = "SIP/abcd-0001"
      val channel: Channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HOLD
      )
      val calls: Map[BridgeCreator, DeviceCall] =
        Map(cname -> DeviceCall(cname, Some(channel), Set.empty, Map.empty))

      attendedTransfer(calls, "1001")

      amiBus.expectMsg(
        DialWithLocalChannelCommand(
          line.interface,
          xivoUser.fullName,
          phoneNumber,
          "1001",
          None,
          "123.123.123.123",
          xivoUser.id,
          line.context,
          Map.empty,
          line.driver
        )
      )

      testProbe.expectMsg(true)
    }

    "initiate a second call for attended transfer if current call is up" in new HelperAttendedTransfer {
      val cname = "SIP/abcd-0001"
      val channel: Channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val calls: Map[BridgeCreator, DeviceCall] =
        Map(cname -> DeviceCall(cname, Some(channel), Set.empty, Map.empty))

      val actor: ActorRef = attendedTransfer(calls, "1001")

      verify(deviceAdapter, Mockito.timeout(100)).hold(None, actor)

      // wait for channel to be on hold
      channelTracker.expectMsg(WatchChannelStartingWith(cname))
      channelTracker.reply(channel.copy(state = ChannelState.HOLD))
      channelTracker.expectMsg(UnWatchChannelStartingWith(cname))

      amiBus.expectMsg(
        DialWithLocalChannelCommand(
          line.interface,
          xivoUser.fullName,
          phoneNumber,
          "1001",
          None,
          "123.123.123.123",
          xivoUser.id,
          line.context,
          Map.empty,
          line.driver
        )
      )

      testProbe.expectMsg(true)
    }

    "ensure variables are copied to second call" in new HelperAttendedTransfer {
      val cname = "SIP/abcd-0001"
      val channel: Channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HOLD
      ).updateVariable("USR_1", "Value 1")
      val calls: Map[BridgeCreator, DeviceCall] =
        Map(cname -> DeviceCall(cname, Some(channel), Set.empty, Map.empty))

      attendedTransfer(calls, "1001")

      amiBus.expectMsg(
        DialWithLocalChannelCommand(
          line.interface,
          xivoUser.fullName,
          phoneNumber,
          "1001",
          None,
          "123.123.123.123",
          xivoUser.id,
          line.context,
          channel.variables,
          line.driver
        )
      )

      testProbe.expectMsg(true)
    }
  }

}
