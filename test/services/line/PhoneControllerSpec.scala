package services.line

import java.util.concurrent.atomic.AtomicLong
import org.apache.pekko.actor.{PoisonPill, *}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.XivoUser
import org.asteriskjava.manager.action.SetVarAction
import org.joda.time.DateTime
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*
import org.scalatestplus.mockito.MockitoSugar
import services.XucAmiBus.AmiAction
import services.{XucAmiBus, XucEventBus}
import services.calltracking.AsteriskGraphTracker.AsteriskPath
import services.calltracking.DeviceConferenceAction.CannotKickOrganizer
import services.calltracking.RetrieveUtil.RetrieveQueueCallContext
import services.calltracking.graph.AsteriskObjectHelper
import services.calltracking.*
import services.config.ConfigDispatcher.*
import services.config.ObjectType
import services.config.{AgentOnPhone, ConfigRepository, OutboundQueue}
import services.request.PhoneRequest.*
import services.request.{MonitorPause, MonitorUnpause, PhoneRequest, UserBaseRequest}
import xivo.events.AgentState.{AgentDialing, AgentLoggedOut, AgentReady}
import xivo.models.{Agent, AgentQueueMember, QueueConfigUpdate}
import xivo.phonedevices.{DeviceAdapter, DeviceAdapterFactory, SetDataCommand}
import xivo.websocket.WsConferenceParticipantUserRole
import xivo.xuc.TransferConfig
import xivo.xucami.models.*
import xuctest.{ChannelGen, XucUserHelper}
import org.mockito.ArgumentMatchers.{eq => mockitoEq}

import scala.concurrent.duration.*
import models.XucUser
import org.mockito.ArgumentMatchers.any
import xivo.models.Line

import java.util

class PhoneControllerSpec
    extends TestKitSpec("PhoneControllerSpec")
    with MockitoSugar
    with XucUserHelper
    with ChannelGen
    with AsteriskObjectHelper {

  import xivo.models.LineHelper.makeLine

  private val number = new AtomicLong
  private def randomName: String = {
    val l = number.getAndIncrement()
    "$" + org.apache.pekko.util.Helpers.base64(l)
  }

  class Helper() {
    val deviceAdapter: DeviceAdapter             = mock[DeviceAdapter]
    val eventBus: XucEventBus                  = mock[XucEventBus]
    val amiBus: XucAmiBus                    = mock[XucAmiBus]
    val transferUtil: TransferUtil              = mock[TransferUtil]
    val retrieveUtil: RetrieveUtil              = mock[RetrieveUtil]
    val configRepository: ConfigRepository          = mock[ConfigRepository]
    val recordingActionController: RecordingActionController = mock[RecordingActionController]
    val deviceAdapterFactory: DeviceAdapterFactory      = mock[DeviceAdapterFactory]
    val lineNumber                = "1000"
    val agentNumber               = "1000"
    val configDispatcher: TestProbe          = TestProbe()
    val devicesTracker: TestProbe            = TestProbe()
    val channelTracker: TestProbe            = TestProbe()
    val graphTracker: TestProbe              = TestProbe()
    val amiBusConnector: TestProbe           = TestProbe()
    val line: Line = makeLine(
      1,
      "default",
      "sip",
      "abcd",
      None,
      None,
      "123.123.123.1",
      number = Some(lineNumber)
    )
    val xucUser: XucUser = getXucUser("toto", "pwd", Some("2104"))
    val queueConfig: QueueConfigUpdate = QueueConfigUpdate(
      3,
      "queue_one",
      "Queue One",
      "3000",
      Some("default"),
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      "url",
      "",
      Some(1),
      Some("preprocess_subroutine"),
      1,
      Some(1),
      Some(1),
      1,
      "recorded",
      1
    )
    val queueCall: QueueCall = QueueCall(
      1,
      Some("Graham Bell"),
      "12345",
      DateTime.now,
      "SIP/defg",
      "main"
    )

    when(deviceAdapterFactory.getAdapter(line, lineNumber))
      .thenReturn(deviceAdapter)

    val xucConfig: TransferConfig = new TransferConfig {
      def enableAmiTransfer             = true
      def amiTransferTimeoutSeconds     = 180
      def conferenceRefreshTimerSeconds = 300
    }

    def actor(config: TransferConfig = xucConfig, parent: ActorRef = self): (TestActorRef[PhoneController], PhoneController) = {
      val res = TestActorRef[PhoneController](
        Props(
          new PhoneController(
            configRepository,
            eventBus,
            amiBus,
            transferUtil,
            recordingActionController,
            config,
            deviceAdapterFactory,
            retrieveUtil,
            devicesTracker.ref,
            channelTracker.ref,
            graphTracker.ref,
            amiBusConnector.ref,
            configDispatcher.ref,
            lineNumber,
            line,
            xucUser.xivoUser
          )
        ),
        parent,
        randomName
      )
      (res, res.underlyingActor)
    }

  }

  "PhoneController actor" should {
    "subscribe to device calls upon startup" in new Helper {
      var (ref, a) = actor()

      devicesTracker.expectMsg(SingleDeviceTracker.MonitorCalls("SIP/abcd"))
    }

    "unsubscribe from device calls when actor is stopped" in new Helper {
      var (ref, a) = actor()

      devicesTracker.expectMsg(SingleDeviceTracker.MonitorCalls("SIP/abcd"))
      ref ! PoisonPill

      devicesTracker.expectMsg(SingleDeviceTracker.UnMonitorCalls("SIP/abcd"))
    }

    "subscribe to device calls & send mobile number to device tracker upon startup" in new Helper {
      val mobileNum = "0612345678"
      override val xucUser: XucUser =
        getXucUser("toto", "pwd", Some("2104"), Some(mobileNum))
      var (ref, a) = actor()

      devicesTracker.expectMsg(SingleDeviceTracker.MonitorCalls("SIP/abcd"))
      devicesTracker.expectMsg(
        SipDeviceTracker.WatchOutboundCallTo("SIP/abcd", mobileNum)
      )
    }

    "unsubscribe from device calls when actor is stopped and send mobile number to device tracker" in new Helper {
      val mobileNum = "0612345678"
      override val xucUser: XucUser =
        getXucUser("toto", "pwd", Some("2104"), Some(mobileNum))
      var (ref, a) = actor()

      devicesTracker.expectMsg(SingleDeviceTracker.MonitorCalls("SIP/abcd"))
      devicesTracker.expectMsg(
        SipDeviceTracker.WatchOutboundCallTo("SIP/abcd", mobileNum)
      )
      ref ! PoisonPill

      devicesTracker.expectMsg(SingleDeviceTracker.UnMonitorCalls("SIP/abcd"))
    }

    "subscribe to queue membership when logging in" in new Helper {
      var (ref, a) = actor()
      val queueIds: List[Int] = List(7, 9, 6)

      ref ! AgentReady(
        12,
        new DateTime,
        lineNumber,
        queueIds,
        Some(""),
        agentNumber
      )
      verify(eventBus).subscribe(
        ref,
        XucEventBus.configTopic(ObjectType.TypeQueueMember)
      )
    }

    "unsubscribe from queue membership when logging out" in new Helper {
      var (ref, a) = actor()
      ref ! AgentLoggedOut(12, new DateTime, lineNumber, List(), "")
      verify(eventBus).unsubscribe(
        ref,
        XucEventBus.configTopic(ObjectType.TypeQueueMember)
      )
    }

    "request outbound queue if none and joining a new queue" in new Helper {
      var (ref, a) = actor()
      a.agentId = Some(1)

      ref ! AgentQueueMember(1, 4, 2)
      configDispatcher.expectMsg(
        RequestConfig(ref, OutboundQueueQuery(List(4L)))
      )
    }

    "remove outbound queue if set and leaving it" in new Helper {
      var (ref, a) = actor()
      a.agentId = Some(1)
      a.queue = Some(queueConfig)

      ref ! AgentQueueMember(1, queueConfig.id, -1)
      a.queue shouldBe empty
    }

    "subscribe to agent state events and ask for current agent state when receiving Start" in new Helper {
      var (ref, a) = actor()

      ref ! PhoneController.Start

      verify(eventBus).subscribe(ref, XucEventBus.allAgentsEventsTopic)
      configDispatcher.expectMsg(
        RequestConfig(ref, GetAgentOnPhone(lineNumber))
      )
    }

    "unsubscribe from eventbus when actor is stopped" in new Helper {
      var (ref, a) = actor()

      ref ! PhoneController.Start

      verify(eventBus).subscribe(ref, XucEventBus.allAgentsEventsTopic)
      configDispatcher.expectMsg(
        RequestConfig(ref, GetAgentOnPhone(lineNumber))
      )

      ref ! PoisonPill

      verify(eventBus).unsubscribe(ref)

    }

    "save the agent id and ask for outbound queue when receiving an AgentLogin on the current line" in new Helper {
      var (ref, a) = actor()
      val queueIds: List[Int] = List(7, 9, 6)

      ref ! AgentReady(
        12,
        new DateTime,
        lineNumber,
        queueIds,
        Some(""),
        agentNumber
      )

      configDispatcher.expectMsg(
        RequestConfig(ref, OutboundQueueQuery(queueIds.map(_.toLong)))
      )
      a.agentId shouldEqual Some(12)
    }

    "save the queue number when receiving an OutboundQueue" in new Helper {
      var (ref, a) = actor()
      ref ! OutboundQueue(queueConfig)
      a.queue shouldEqual Some(queueConfig)
    }

    "unset the agentId and the queue when receiving AgentLogout" in new Helper {
      var (ref, a) = actor()
      a.agentId = Some(12)
      a.queue = Some(queueConfig)

      ref ! AgentLoggedOut(12, new DateTime, lineNumber, List(), "")

      a.agentId shouldEqual None
      a.queue shouldEqual None
    }

    "do noting if the AgentLoggedOut is not for the current line" in new Helper {
      var (ref, a) = actor()
      a.agentId = Some(12)
      a.queue = Some(queueConfig)

      ref ! AgentLoggedOut(12, new DateTime, lineNumber + "1", List(), "")

      a.agentId shouldEqual Some(12)
      a.queue shouldEqual Some(queueConfig)
    }

    "set the agentId and ask for the agent queues and the agent state when receiving AgentOnPhone" in new Helper {
      var (ref, a) = actor()
      val agent: Agent = Agent(12, "Agent", "One", agentNumber, "default")

      when(configRepository.getAgent(12)).thenReturn(Some(agent))

      ref ! AgentOnPhone(12, lineNumber)

      a.agentId shouldEqual Some(12)
      configDispatcher.expectMsg(RequestConfig(ref, GetQueuesForAgent(12)))
      configDispatcher.expectMsg(RequestStatus(ref, 12, ObjectType.TypeAgent))
    }

    "ask for outbound queue when receiving QueuesForAgent" in new Helper {
      var (ref, a) = actor()
      val agentId  = 12
      val queues: List[AgentQueueMember] =
        List(AgentQueueMember(agentId, 3, 0), AgentQueueMember(agentId, 4, 0))

      ref ! QueuesForAgent(queues, agentId)

      configDispatcher.expectMsg(
        RequestConfig(ref, OutboundQueueQuery(List(3, 4)))
      )
    }

    "enable outbound dial and ask for the outbound queue when receiving AgentReady" in new Helper {
      var (ref, a) = actor()

      ref ! AgentReady(
        12,
        new DateTime,
        lineNumber,
        List(3, 4),
        agentNb = "2000"
      )

      a.enableOutboundDial should be(true)
      configDispatcher.expectMsg(
        RequestConfig(ref, OutboundQueueQuery(List(3, 4)))
      )
    }

    "not enable outbound dial if the AgentReady is not for us" in new Helper {
      var (ref, a) = actor()
      ref ! AgentReady(
        12,
        new DateTime,
        lineNumber + "1",
        List(),
        agentNb = "2000"
      )
      a.enableOutboundDial should be(false)
    }

    "disable outbound dial when receiving another agent state" in new Helper {
      var (ref, a) = actor()
      a.enableOutboundDial = true

      ref ! AgentDialing(12, new DateTime, lineNumber, List(), agentNb = "2000")

      a.enableOutboundDial should be(false)
    }

    "not disable outbound dial if the agent state is not for us" in new Helper {
      var (ref, a) = actor()
      a.enableOutboundDial = true

      ref ! AgentDialing(
        12,
        new DateTime,
        lineNumber + "1",
        List(),
        agentNb = "2000"
      )

      a.enableOutboundDial should be(true)
    }

    "do an outbound dial if the agentId and the queue number are set, and outbound dial is enabled" in new Helper {
      var (ref, a) = actor()
      a.enableOutboundDial = true
      a.agentId = Some(12)
      a.queue = Some(queueConfig)
      val probe: TestProbe = TestProbe()

      when(configRepository.getLineUser(1)).thenReturn(Some(xucUser.xivoUser))

      ref ! UserBaseRequest(
        probe.ref,
        Dial("335847", Map("VARIABLE" -> "Value")),
        xucUser
      )

      verify(deviceAdapter).odial(
        "335847",
        12,
        "3000",
        Map("VARIABLE" -> "Value"),
        1L,
        self,
        line.driver
      )
    }

    "do a listen callback message when receiving the request" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(
        ref,
        ListenCallbackMessage("1499074373.5", Map("VARIABLE" -> "Value")),
        xucUser
      )
      verify(deviceAdapter).listenCallbackMessage(
        "1499074373.5",
        Map("VARIABLE" -> "Value"),
        self
      )
    }

    "do a standard dial otherwise" in new Helper {
      var (ref, a) = actor()
      val probe: TestProbe = TestProbe()
      ref ! UserBaseRequest(
        probe.ref,
        Dial("335847", Map("VARIABLE" -> "Value")),
        xucUser
      )
      verify(deviceAdapter).dial("335847", Map("VARIABLE" -> "Value"), self)
    }

    "sanitize phone number before dial" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(self, Dial("+33(0) 6 87 65 43 21"), xucUser)
      verify(deviceAdapter).dial("0033687654321", Map.empty, self)
    }

    "legacy hangup call without specifying uniqueId" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(ref, Hangup(None), xucUser)
      verify(deviceAdapter).hangup(self)
    }

    "legacy hangup if call defined by its uniqueId is not found" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(ref, Hangup(Some("1616681250.0")), xucUser)
      verify(deviceAdapter).hangup(self)
    }

    "hangup on specific mds if call defined by its uniqueId" in new Helper {
      val callChannel: Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1000"),
        "",
        ChannelState.HOLD,
        mdsName = "mds1"
      )
      val call: DeviceCall =
        DeviceCall(callChannel.name, Some(callChannel), Set.empty, Map.empty)
      val calls: DeviceCalls = DeviceCalls("SIP/abcd", Map(call.channelName -> call))

      var (ref, a) = actor()
      ref ! calls
      ref ! UserBaseRequest(ref, Hangup(Some("123456789.123")), xucUser)

      val args: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus, times(2)).publish(args.capture)
      val list: util.List[AmiAction] = args.getAllValues

      list.get(0).message match {
        case setvar: SetVarAction =>
          setvar.getChannel shouldBe callChannel.name
          setvar.getVariable shouldBe "CHANNEL(hangupsource)"
          setvar.getValue shouldBe callChannel.name
        case any =>
          fail(s"Should get SetVarAction, got: $any")
      }
    }

    "toggle user microphone without uniqueId" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(
        ref,
        ToggleMicrophone(None),
        xucUser
      )
      verify(deviceAdapter).toggleMicrophone(None, self)
    }

    "toggle user microphone with uniqueId" in new Helper {
      val callChannel: Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1000"),
        "",
        variables = Map("SIPCALLID" -> "123-456@192.168")
      )
      val call: DeviceCall =
        DeviceCall(callChannel.name, Some(callChannel), Set.empty, Map.empty)
      val calls: DeviceCalls =
        DeviceCalls("SIP/abcd", Map(call.channelName -> call))

      val (ref, _) = actor()
      ref ! calls
      ref ! UserBaseRequest(
        ref,
        ToggleMicrophone(Some("123456789.123")),
        xucUser
      )
      verify(deviceAdapter).toggleMicrophone(Some(call), self)
    }

    "do an attended transfer the old way" in new Helper {
      val config: TransferConfig = new TransferConfig {
        def enableAmiTransfer             = false
        def amiTransferTimeoutSeconds     = 180
        def conferenceRefreshTimerSeconds = 300
      }
      var (ref, a) = actor(config)
      ref ! UserBaseRequest(ref, AttendedTransfer("1000", None), xucUser)
      verify(deviceAdapter).attendedTransfer("1000", self)
    }

    "do an attended transfer using AMI" in new Helper {
      var (ref, a) = actor()

      val callChannel: Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HOLD
      )
      val call: DeviceCall =
        DeviceCall(callChannel.name, Some(callChannel), Set.empty, Map.empty)
      val calls: DeviceCalls = DeviceCalls("SIP/abcd", Map(call.channelName -> call))
      // We should use the user of the line, not the current user as it may be different
      // for agents logged on another line see #1438
      when(configRepository.getLineUser(1)).thenReturn(Some(xucUser.xivoUser))

      ref ! calls
      ref ! UserBaseRequest(ref, AttendedTransfer("1000", None), xucUser)

      verify(transferUtil, timeout(1000))
        .attendedTransfer(
          mockitoEq(calls.calls),
          mockitoEq("1000"),
          mockitoEq(None),
          mockitoEq(line),
          mockitoEq(xucUser.xivoUser),
          mockitoEq(deviceAdapter),
          mockitoEq(self)
        )(any[ActorContext])
    }

    "complete the transfer the old way if configured to do so" in new Helper {
      val config: TransferConfig = new TransferConfig {
        def enableAmiTransfer             = false
        def amiTransferTimeoutSeconds     = 180
        def conferenceRefreshTimerSeconds = 300
      }
      var (ref, a) = actor(config)

      ref ! UserBaseRequest(ref, CompleteTransfer, xucUser)
      verify(deviceAdapter).completeTransfer(self)
    }

    "complete transfer using AMI if two calls with remote channel including one local channel" in new Helper {
      var (ref, a) = actor()

      val call1Channel: Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HOLD
      )
      val call2Channel: Channel = Channel(
        "123456789.321",
        "SIP/abcd-00002",
        CallerId("Jason Bourne", "1001"),
        "",
        ChannelState.UP
      )

      val call1RemoteChannel       = "SIP/efgh-00001"
      val call2RemoteLocalChannel2 = "Local/1000@default;2"
      val call2RemoteLocalChannel1 = "Local/1000@default;1"
      val call1: DeviceCall = DeviceCall(
        call1Channel.name,
        Some(call1Channel),
        Set(AsteriskPath(NodeChannel(call1RemoteChannel))),
        Map.empty
      )
      val call2: DeviceCall = DeviceCall(
        call2Channel.name,
        Some(call2Channel),
        Set(AsteriskPath(NodeLocalChannel(call2RemoteLocalChannel2))),
        Map.empty
      )
      val calls: DeviceCalls = DeviceCalls(
        "SIP/abcd",
        Map(
          call1.channelName -> call1,
          call2.channelName -> call2
        )
      )

      val callsForTransfer: CallsForTransfer = CallsForTransfer(
        CallChannelsForTransfer(
          NodeChannel("SIP/abcd-00001"),
          NodeChannel("SIP/efgh-00001")
        ),
        CallChannelsForTransfer(
          NodeChannel("SIP/abcd-00002"),
          NodeLocalChannel("Local/1000@default;1")
        )
      )

      ref ! calls

      when(transferUtil.getChannelsForTransfer(calls.calls))
        .thenReturn(Some(callsForTransfer))

      ref ! UserBaseRequest(ref, CompleteTransfer, xucUser)
      // Ensure we use the correct side of the local channel. i.e. ";1"
      verify(transferUtil, timeout(100)).getChannelsForTransfer(calls.calls)
      verify(transferUtil, timeout(100)).completeTransfer(
        mockitoEq(callsForTransfer),
        any[FiniteDuration],
        any[Option[String]]
      )(any[ActorContext])
    }

    "cancel the transfer" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(ref, CancelTransfer, xucUser)
      verify(deviceAdapter).cancelTransfer(self)
    }

    "direct transfer" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(ref, DirectTransfer("0678543251"), xucUser)
      verify(deviceAdapter).directTransfer("0678543251", self)
    }

    "do a conference" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(ref, Conference, xucUser)
      verify(deviceAdapter).conference(self)
    }

    "hold the call without uniqueId" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(ref, Hold(None), xucUser)
      verify(deviceAdapter).hold(None, self)
    }

    "hold the call with uniqueId" in new Helper {
      val callChannel: Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1000"),
        "",
        variables = Map("SIPCALLID" -> "123-456@192.168")
      )
      val call: DeviceCall =
        DeviceCall(callChannel.name, Some(callChannel), Set.empty, Map.empty)
      val calls: DeviceCalls =
        DeviceCalls("SIP/abcd", Map(call.channelName -> call))

      val (ref, _) = actor()
      ref ! calls
      ref ! UserBaseRequest(
        ref,
        Hold(Some("123456789.123")),
        xucUser
      )
      verify(deviceAdapter).hold(Some(call), self)
    }

    "answer the call without uniqueId" in new Helper {
      var (ref, a) = actor()
      ref ! UserBaseRequest(ref, Answer(None), xucUser)
      verify(deviceAdapter).answer(None, self)
    }

    "answer the call with uniqueId" in new Helper {
      val callChannel: Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1000"),
        "",
        variables = Map("SIPCALLID" -> "123-456@192.168")
      )
      val call: DeviceCall =
        DeviceCall(callChannel.name, Some(callChannel), Set.empty, Map.empty)
      val calls: DeviceCalls =
        DeviceCalls("SIP/abcd", Map(call.channelName -> call))

      val (ref, _) = actor()
      ref ! calls
      ref ! UserBaseRequest(
        ref,
        Answer(Some("123456789.123")),
        xucUser
      )
      verify(deviceAdapter).answer(Some(call), self)
    }

    "on setData build a setData Command and send back to sender" in new Helper {
      var (ref, a)                       = actor()
      val variables: Map[String, String] = Map("VARIABLE" -> "Value")

      ref ! UserBaseRequest(ref, SetData(variables), xucUser)

      expectMsg(SetDataCommand(lineNumber, variables))
    }

    "on getCurrentCallsPhoneEvents ask devicesTracker to sent current calls phone events" in new Helper {
      devicesTracker.ignoreMsg { case m: SingleDeviceTracker.MonitorCalls =>
        true
      }
      var (ref, a) = actor()

      ref ! UserBaseRequest(ref, GetCurrentCallsPhoneEvents, xucUser)

      devicesTracker.expectMsg(
        SingleDeviceTracker.SendCurrentCallsPhoneEvents(line.interface)
      )
      devicesTracker.ignoreNoMsg()
    }

    "forward Conference phone request to Device tracker" in new Helper {
      val xivoUser: XivoUser = XivoUser(1L, None, None, "User", Some("One"), None, None, None, None)
      when(configRepository.getLineForPhoneNb("0102030405")).thenReturn(Some(1L))
      when(configRepository.getLineUser(1L)).thenReturn(Some(xivoUser))

      val msgs: Map[PhoneRequest with Serializable, DeviceConferenceAction.DeviceConferenceCommand with Serializable] = Map(
        ConferenceMuteMe("4000")    -> DeviceConferenceAction.MuteMe("4000"),
        ConferenceUnmuteMe("4000")  -> DeviceConferenceAction.UnmuteMe("4000"),
        ConferenceMuteAll("4000")   -> DeviceConferenceAction.MuteAll("4000"),
        ConferenceUnmuteAll("4000") -> DeviceConferenceAction.UnmuteAll("4000"),
        ConferenceMute("4000", 1)   -> DeviceConferenceAction.Mute("4000", 1),
        ConferenceUnmute("4000", 1) -> DeviceConferenceAction.Unmute("4000", 1),
        ConferenceKick("4000", 1)   -> DeviceConferenceAction.Kick("4000", 1),
        ConferenceInvite(
          "4000",
          "0102030405",
          WsConferenceParticipantUserRole,
          true,
          Map(("someVariable", "someValue"))
        ) -> DeviceConferenceAction
          .Invite("4000", "0102030405", WsConferenceParticipantUserRole, true, Map(("someVariable", "someValue")), Some(xivoUser)),
        ConferenceClose("4000")     -> DeviceConferenceAction.Close("4000"),
        ConferenceDeafen("4000", 1) -> DeviceConferenceAction.Deafen("4000", 1),
        ConferenceUndeafen("4000", 1) -> DeviceConferenceAction.Undeafen(
          "4000",
          1
        ),
        ConferenceReset("4000") -> DeviceConferenceAction.Reset("4000")
      )
      devicesTracker.ignoreMsg { case m: SingleDeviceTracker.MonitorCalls =>
        true
      }
      var (ref, a) = actor()

      msgs.foreach { case (request, action) =>
        ref ! UserBaseRequest(ref, request, xucUser)
        devicesTracker.expectMsg(
          SingleDeviceTracker.DeviceConferenceMessage(line.interface, action)
        )
      }

      devicesTracker.ignoreNoMsg()
    }

    "report Conference action error to parent (CtiRouter)" in new Helper {
      val parent: TestProbe = TestProbe()
      var (ref, a) = actor(parent = parent.ref)
      ref ! CannotKickOrganizer
      parent.expectMsg(CannotKickOrganizer)

    }

    "on SendDtmf request sends dtmf by the device adapter" in new Helper {
      val (ref, _) = actor()

      ref ! UserBaseRequest(ref, SendDtmfRequest('4'), xucUser)

      verify(deviceAdapter).sendDtmf('4', self)
    }

    "on RetrieveQueueCall request sends retrieve to get the queue call" in new Helper {
      var (ref, a) = actor()
      a.agentId = Some(1)
      val agent: Agent = Agent(1, "Agent", "One", agentNumber, "default")

      val queueName: Some[String] = Some("__switchboard")
      val channel: Channel = bchan(queueCall.channel)

      val enterEvent: EnterQueue = EnterQueue(
        "__switchboard_hold_2001",
        channel.id,
        QueueCall(
          1,
          Some("User Three"),
          "1000",
          new DateTime(),
          channel.name,
          "main"
        ),
        channel.name
      )
      val channelWithQueueHistory: Channel = channel.withQueueHistory(enterEvent)

      when(configRepository.getAgent(1)).thenReturn(Some(agent))

      ref ! AgentOnPhone(1, lineNumber)
      ref ! UserBaseRequest(ref, RetrieveQueueCall(queueCall), xucUser)

      val retrieveContext: RetrieveQueueCallContext = RetrieveQueueCallContext(
        line,
        queueCall,
        Map.empty,
        Some(agentNumber),
        "John Doe"
      )

      verify(retrieveUtil).processRetrieve(
        mockitoEq(List()),
        mockitoEq(retrieveContext),
        mockitoEq(deviceAdapter),
        mockitoEq(self)
      )(any[ActorContext])
    }

    "dial by username" in new Helper {
      var (ref, a) = actor()
      val probe: TestProbe = TestProbe()

      when(configRepository.phoneNumberForUser("userone"))
        .thenReturn(Some("2001"))

      ref ! UserBaseRequest(
        probe.ref,
        DialByUsername("userone", Map("VARIABLE" -> "Value"), "default"),
        xucUser
      )
      verify(deviceAdapter).dial("2001", Map("VARIABLE" -> "Value"), self)
    }

    "not dial by username for unknown username" in new Helper {
      var (ref, a) = actor()

      when(configRepository.phoneNumberForUser("userone")).thenReturn(None)

      ref ! UserBaseRequest(ref, DialByUsername("userone", domain = "default"), xucUser)
      verifyNoInteractions(deviceAdapter)
    }
  }

  "Phone Controller on recording (monitor) action request" should {
    trait recordingHelper {
      val interface       = "SIP/abcd"
      val recordedChannel: Channel = bchan(interface)

      val recordedCall: DeviceCall = DeviceCall(
        recordedChannel.name,
        Some(recordedChannel),
        Set(),
        Map.empty
      )

      val calls: DeviceCalls =
        DeviceCalls(interface, Map(recordedCall.channelName -> recordedCall))

    }

    "Pause recording on pause monitor request" in new Helper
      with recordingHelper {

      var (ref, a) = actor()

      ref ! calls

      when(recordingActionController.getMonitoredChannel(calls.calls))
        .thenReturn(Some(recordedChannel))

      ref ! MonitorPause(23)

      verify(recordingActionController, timeout(100))
        .pauseRecording(recordedChannel)

    }

    "Un pause recording on un pause monitor request" in new Helper
      with recordingHelper {
      var (ref, a) = actor()

      ref ! calls

      when(recordingActionController.getMonitoredChannelOnPause(calls.calls))
        .thenReturn(Some(recordedChannel))

      ref ! MonitorUnpause(23)

      verify(recordingActionController, timeout(100))
        .unPauseRecording(recordedChannel)

    }
  }
}
