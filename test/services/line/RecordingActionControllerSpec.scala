package services.line

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.TestKit
import org.asteriskjava.manager.action.{PauseMonitorAction, UnpauseMonitorAction}
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.verify
import pekkotest.TestKitSpec
import services.XucAmiBus
import services.XucAmiBus.AmiAction
import services.calltracking.DeviceCall
import xivo.xucami.models.{Channel, MonitorState}
import xuctest.{ChannelGen, ScalaTestTools}

class RecordingActionControllerSpec
    extends TestKitSpec("RecordingActionControllerSpec")
    with ScalaTestTools
    with ChannelGen {

  class Helper {
    val amiBus: XucAmiBus = mock[XucAmiBus]

  }

  "A recording action controller" should {
    "return first recorded channel" in new Helper {

      val interface = "SIP/abcd"
      val c: Channel = bchan(interface).copy(monitored = MonitorState.ACTIVE)
      val call: DeviceCall = DeviceCall(interface, Some(c), Set.empty, Map())

      new RecordingActionControllerImpl(amiBus).getMonitoredChannel(
        Map(interface -> call)
      ) should be(Some(c))

    }
    "return first channel recorded found" in new Helper {

      val interface      = "SIP/abcd"
      val otherinterface = "SIP/sklmdjf"
      val c: Channel = bchan(interface).copy(monitored = MonitorState.DISABLED)
      val rc: Channel = bchan(interface).copy(monitored = MonitorState.ACTIVE)
      val call: DeviceCall =
        DeviceCall(otherinterface, Some(c), Set.empty, Map(rc.name -> rc))

      new RecordingActionControllerImpl(amiBus).getMonitoredChannel(
        Map(interface -> call)
      ) should be(Some(rc))

    }

    "return first recorded channel in pause" in new Helper {

      val interface = "SIP/abcd-0000001"
      val c: Channel = bchan(interface).copy(monitored = MonitorState.PAUSED)
      val call: DeviceCall = DeviceCall(interface, Some(c), Set.empty, Map())

      new RecordingActionControllerImpl(amiBus).getMonitoredChannelOnPause(
        Map(interface -> call)
      ) should be(Some(c))

    }

    "return first channel recorded in pause when remote channels exists" in new Helper {

      val interface      = "SIP/abcd"
      val otherinterface = "SIP/sklmdjf"
      val c: Channel = bchan(interface, m = MonitorState.DISABLED)
      val rc: Channel = bchan(interface, m = MonitorState.PAUSED)
      val call: DeviceCall =
        DeviceCall(otherinterface, Some(c), Set.empty, Map(rc.name -> rc))

      new RecordingActionControllerImpl(amiBus).getMonitoredChannelOnPause(
        Map(interface -> call)
      ) should be(Some(rc))

    }

    "pause a recorded channel" in new Helper {

      val interface  = "SIP/ilsudf"
      val recChannel: Channel = bchan(interface, m = MonitorState.ACTIVE)

      new RecordingActionControllerImpl(amiBus).pauseRecording(recChannel)

      val arg: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case pauseMonitor: PauseMonitorAction =>
          pauseMonitor.getChannel should be(recChannel.name)
        case any =>
          fail(s"Should get PauseMonitorAction, got: $any")
      }

      arg.getValue.reference should be(Some(recChannel.id))

    }

    "unpause a paused recorded channel" in new Helper {

      val interface  = "SIP/ilsudf"
      val recChannel: Channel = bchan(interface, m = MonitorState.PAUSED)

      new RecordingActionControllerImpl(amiBus).unPauseRecording(recChannel)

      val arg: ArgumentCaptor[AmiAction] = ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case unPauseMonitor: UnpauseMonitorAction =>
          unPauseMonitor.getChannel should be(recChannel.name)
        case any =>
          fail(s"Should get unpauseMonitorAction, got: $any")
      }

      arg.getValue.reference should be(Some(recChannel.id))

    }

  }
}
