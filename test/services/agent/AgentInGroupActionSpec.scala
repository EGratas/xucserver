package services.agent

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito.verify
import services.agent.AgentInGroupAction.{AgentsDestination, RemoveAgents}
import services.config.ConfigDispatcher._
import xivo.models.Agent

import scala.concurrent.duration.DurationInt

class AgentInGroupActionSpec
    extends TestKitSpec("agentInGroupAction")
    with MockitoSugar {

  class Helper {

    val fakeAction: (Agent, Option[Long], Option[Int]) => Unit       = mock[(Agent, Option[Long], Option[Int]) => Unit]
    val configDispatcher: TestProbe = TestProbe()

    class AgentInGroupDoIt(groupId: Long, fromQueueId: Long, fromPenalty: Int)
        extends AgentInGroupAction(
          groupId,
          fromQueueId,
          fromPenalty,
          configDispatcher.ref
        ) {

      override def actionOnAgent(
          agent: Agent,
          toQueueId: Option[Long],
          toPenalty: Option[Int]
      ): Unit = fakeAction(agent, toQueueId, toPenalty)

    }

    def actor(groupId: Long, queueId: Long, penalty: Int): (TestActorRef[AgentInGroupDoIt], AgentInGroupDoIt) = {
      val a = TestActorRef[AgentInGroupDoIt](
        Props(new AgentInGroupDoIt(groupId, queueId, penalty))
      )
      (a, a.underlyingActor)
    }
  }

  "an agent group action" should {
    "request agents from config dispatcher" in new Helper {
      val (groupId, fromQueueId, fromPenalty) = (1, 52, 2)
      val (toQueueId, toPenalty)              = (85, 9)
      val (ref, _)                            = actor(groupId, fromQueueId, fromPenalty)

      ref ! AgentsDestination(toQueueId, toPenalty)

      configDispatcher.expectMsg(
        RequestConfig(ref, GetAgents(groupId, fromQueueId, fromPenalty))
      )
    }

    "should execute action on agents on destination received" in new Helper {

      val (groupId, fromQueueId, fromPenalty) = (7, 44, 8)
      val (toQueueId, toPenalty)              = (22, 2)
      val (ref, _)                            = actor(groupId, fromQueueId, fromPenalty)

      val john: Agent = Agent(1, "John", "Malt", "33784", "default", groupId)
      val agents: List[Agent] = List(john)

      ref ! AgentsDestination(toQueueId, toPenalty)

      ref ! AgentList(agents)

      verify(fakeAction)(john, Some(toQueueId), Some(toPenalty))

    }
    "should execute action on agents on remove agents" in new Helper {
      val (groupId, queueId, penalty) = (7, 44, 8)
      val (ref, _)                    = actor(groupId, queueId, penalty)

      val john: Agent = Agent(1, "John", "Malt", "33784", "default", groupId)
      val agents: List[Agent] = List(john)

      ref ! RemoveAgents

      ref ! AgentList(agents)

      verify(fakeAction)(john, None, None)

    }

    "should terminate on completion" in new Helper {
      val watcher: TestProbe = TestProbe()

      val (groupId, fromQueueId, fromPenalty) = (7, 44, 8)
      val (toQueueId, toPenalty)              = (fromQueueId, 2)
      val (ref, _)                            = actor(groupId, fromQueueId, fromPenalty)

      val agents: List[Agent] = List(Agent(1, "John", "Malt", "33784", "default", groupId))

      watcher.watch(ref)

      ref ! AgentsDestination(toQueueId, toPenalty)

      ref ! AgentList(agents)

      watcher.expectTerminated(ref, 500.milliseconds)

    }

  }

}
