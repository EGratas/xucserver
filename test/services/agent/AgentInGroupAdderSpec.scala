package services.agent

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import services.agent.AgentInGroupAction.AgentsDestination
import services.config.ConfigDispatcher._
import services.request.SetAgentQueue
import xivo.models.Agent

class AgentInGroupAdderSpec extends TestKitSpec("agentingroupadder") {

  class Helper {
    val configDispatcher: TestProbe = TestProbe()

    def actor(groupId: Long, queueId: Long, penalty: Int): (TestActorRef[AgentInGroupAdder], AgentInGroupAdder) = {
      val a = TestActorRef[AgentInGroupAdder](
        Props(
          new AgentInGroupAdder(groupId, queueId, penalty, configDispatcher.ref)
        )
      )
      (a, a.underlyingActor)
    }
  }

  "an agent group adder" should {

    "should set agent in new queue upon reception of agents" in new Helper {

      val (groupId, fromQueueId, fromPenalty) = (7, 44, 8)
      val (toQueueId, toPenalty)              = (22, 2)
      val (ref, _)                            = actor(groupId, fromQueueId, fromPenalty)

      val agents: List[Agent] = List(Agent(1, "John", "Malt", "33784", "default", groupId))

      ref ! AgentsDestination(toQueueId, toPenalty)

      ref ! AgentList(agents)

      configDispatcher.expectMsgAllOf(
        RequestConfig(ref, GetAgents(groupId, fromQueueId, fromPenalty)),
        ConfigChangeRequest(ref, SetAgentQueue(1, toQueueId, toPenalty))
      )

    }
  }
}
