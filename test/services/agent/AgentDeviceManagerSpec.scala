package services.agent

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import services.XucEventBus
import services.request._
import xivo.ami.AmiBusConnector.{AgentSpyStart, AgentSpyStop}
import xivo.events.AgentState.{
  AgentLoggedOut,
  AgentOnPause,
  AgentOnWrapup,
  AgentReady
}

class AgentDeviceManagerSpec
    extends TestKitSpec("AgentDeviceManagerSpec")
    with MockitoSugar {

  class Helper {
    val eventBus: XucEventBus        = mock[XucEventBus]
    val amiBusConnector: TestProbe = TestProbe()

    def actor: (TestActorRef[AgentDeviceManager], AgentDeviceManager) = {
      val a = TestActorRef[AgentDeviceManager](
        Props(new AgentDeviceManager(eventBus, amiBusConnector.ref))
      )
      (a, a.underlyingActor)
    }
  }

  "An agent device manager " should {
    "subscribe to event bus at startup" in new Helper {

      val (ref, _) = actor

      verify(eventBus).subscribe(ref, XucEventBus.allAgentsEventsTopic)
    }

    "turn on pause light on event agent on pause " in new Helper {
      val phoneNb = "33000"
      val agentOnPause: AgentOnPause =
        AgentOnPause(34, null, phoneNb, List(), agentNb = "2000")

      val (ref, _) = actor

      ref ! agentOnPause

      amiBusConnector.expectMsg(TurnOnKeyLight(phoneNb, PhoneKey.Pause))

    }
    "Turn off pause light turn on logon light, on event agent ready" in new Helper {
      val phoneNb    = "45000"
      val agentReady: AgentReady = AgentReady(89, null, phoneNb, List(), agentNb = "2000")

      val (ref, _) = actor

      ref ! agentReady

      amiBusConnector.expectMsgAllOf(
        TurnOffKeyLight(phoneNb, PhoneKey.Pause),
        TurnOnKeyLight(phoneNb, PhoneKey.Logon),
        TurnOnKeyLightWithAgNum(phoneNb, PhoneKey.Logon, "2000"),
        TurnOffKeyLight(phoneNb, PhoneKey.Spied)
      )

    }

    "Turn off pause light, logon light, logon with agent number light on event agent logged out" in new Helper {
      val phoneNb = "44201"
      val agentNb = "10244"

      val agentLoggedout: AgentLoggedOut = AgentLoggedOut(97, null, phoneNb, List(), agentNb)

      val (ref, _) = actor

      ref ! agentLoggedout

      amiBusConnector.expectMsgAllOf(
        TurnOffKeyLight(phoneNb, PhoneKey.Pause),
        TurnOffKeyLight(phoneNb, PhoneKey.Logon),
        TurnOffKeyLightWithAgNum(phoneNb, PhoneKey.Logon, agentNb),
        TurnOffKeyLight(phoneNb, PhoneKey.Spied)
      )
    }

    "Not turn off pause, logon, logon with agent number light on event agent logged out received with phoneNb empty" in new Helper {
      val phoneNb = ""
      val agentNb = "10245"

      val agentLoggedout: AgentLoggedOut = AgentLoggedOut(97, null, phoneNb, List(), agentNb)

      val (ref, _) = actor

      ref ! agentLoggedout

      amiBusConnector.expectNoMessage()
    }

    "Blink pause key, on event agent on wrapup" in new Helper {
      val phoneNb = "32500"

      val agentOnWrapup: AgentOnWrapup =
        AgentOnWrapup(5, null, phoneNb, List(), agentNb = "8325")

      val (ref, _) = actor

      ref ! agentOnWrapup

      amiBusConnector.expectMsg(BlinkKeyLight(phoneNb, PhoneKey.Pause))
    }

    "turn on spied light on event agent spy start " in new Helper {
      val phoneNb    = "33000"
      val agentSpied: AgentSpyStart = AgentSpyStart(34, phoneNb)

      val (ref, _) = actor

      ref ! agentSpied

      amiBusConnector.expectMsg(TurnOnKeyLight(phoneNb, PhoneKey.Spied))
    }

    "turn off spied light on event agent spy stop " in new Helper {
      val phoneNb    = "33000"
      val agentSpied: AgentSpyStop = AgentSpyStop(34, phoneNb)

      val (ref, _) = actor

      ref ! agentSpied

      amiBusConnector.expectMsg(TurnOffKeyLight(phoneNb, PhoneKey.Spied))
    }

  }

}
