package services.agent

import org.apache.pekko.actor.{Props, ReceiveTimeout}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import controllers.helpers.{RequestSuccess, RequestTimeout}
import org.joda.time.DateTime
import org.scalatestplus.mockito.MockitoSugar
import services.XucEventBus
import services.request.{AgentLogout, BaseRequest}
import xivo.events.AgentState.AgentLoggedOut

class LogoutProcessorSpec
    extends TestKitSpec("LogoutProcessorSpec")
    with MockitoSugar {

  val eventBus: XucEventBus      = mock[XucEventBus]
  val configManager: TestProbe = TestProbe()

  class Helper {
    def actor: (TestActorRef[LogoutProcessor], LogoutProcessor) = {
      val a = TestActorRef[LogoutProcessor](
        Props(new LogoutProcessor(eventBus, configManager.ref))
      )
      (a, a.underlyingActor)
    }
  }

  "A LogoutProcessor" should {
    "process Logout requests" in new Helper {
      val (ref, _) = actor
      ref ! LogoutAgent("1010")
      configManager.expectMsg(BaseRequest(ref, AgentLogout("1010")))
    }

    "process AgentLoggedOut event" in new Helper {
      val (ref, processor) = actor
      val requester: TestProbe = TestProbe()
      processor.expectingResult(requester.ref, "1010")(
        AgentLoggedOut(11L, new DateTime(), "1010", List(1), "")
      )
      requester.expectMsgClass(classOf[RequestSuccess])
    }

    "process ReceiveTimeout event in receive state" in new Helper {
      val (ref, processor) = actor
      val requester: TestProbe = TestProbe()
      requester watch ref
      ref ! ReceiveTimeout
      requester expectTerminated ref
    }

    "process ReceiveTimeout event in expectingResult state" in new Helper {
      val (ref, processor) = actor
      val requester: TestProbe = TestProbe()
      processor.expectingResult(requester.ref, "1010")(ReceiveTimeout)
      requester.expectMsgClass(classOf[RequestTimeout])
    }
  }
}
