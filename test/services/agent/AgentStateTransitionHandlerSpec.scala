package services.agent

import org.apache.pekko.actor._
import org.apache.pekko.testkit.{TestFSMRef, TestProbe}
import pekkotest.TestKitSpec
import models.LineConfig
import org.mockito.ArgumentCaptor
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import services.AgentStateFSM.{AgentStateContext, _}
import services._
import services.calltracking.SingleDeviceTracker.{MonitorCalls, UnMonitorCalls}
import services.calltracking.{DeviceCall, DeviceCalls}
import services.config.ConfigDispatcher.LineConfigQueryByNb
import services.config.ConfigRepository
import xivo.events.AgentState._
import xivo.events._
import xivo.xucami.models._

class AgentStateTransitionHandlerSpec
    extends TestKitSpec("AgentStateTransitionHandlerSpec")
    with MockitoSugar {
  import xivo.models.LineHelper.makeLine

  val agentId     = 1L
  val agentNumber = "2200"

  def createFSM(
      eventBus: XucEventBus,
      configRepository: ConfigRepository,
      deviceTracker: ActorRef
  ): TestFSMRef[MAgentStates,MContext,AgentStateFSM with AgentStateProdTHandler] = {

    val agentStatusFSM = TestFSMRef(
      new AgentStateFSM(
        agentId,
        agentNumber,
        eventBus,
        configRepository,
        deviceTracker
      ) with AgentStateProdTHandler
    )
    agentStatusFSM
  }

  def createFSMForPublishing(state: MAgentStates, context: MContext): (XucEventBus, TestFSMRef[MAgentStates,MContext,AgentStateFSM with AgentStateProdTHandler]) = {
    val eventBus                           = mock[XucEventBus]
    val configRepository: ConfigRepository = mock[ConfigRepository]
    val deviceTracker                      = TestProbe()
    val line = makeLine(
      1,
      "default",
      "sip",
      "abcd",
      None,
      xivoIp = "123.123.123.123",
      number = Some("1000")
    )
    val lineConfig = LineConfig("1", "1000", Some(line))

    when(configRepository.getLineConfig(LineConfigQueryByNb("1000")))
      .thenReturn(Some(lineConfig))

    val agentStatusFSM = TestFSMRef(
      new AgentStateFSM(
        agentId,
        agentNumber,
        eventBus,
        configRepository,
        deviceTracker.ref
      ) with AgentStateProdTHandler
    )
    agentStatusFSM.setState(state, context)
    reset(eventBus)
    (eventBus, agentStatusFSM)
  }

  "AgentStateTransitionHandler" should {

    "should subscribe to specific device tracker when logging in" in {
      val eventBus                           = mock[XucEventBus]
      val configRepository: ConfigRepository = mock[ConfigRepository]
      val deviceTracker                      = TestProbe()
      val fsm                                = createFSM(eventBus, configRepository, deviceTracker.ref)
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        xivoIp = "123.123.123.123",
        number = Some("1000")
      )
      val lineConfig = LineConfig("1", "1000", Some(line))

      when(configRepository.getLineConfig(LineConfigQueryByNb("1000")))
        .thenReturn(Some(lineConfig))
      fsm ! EventAgentLogin(1, "1000", "2000")

      deviceTracker.expectMsg(MonitorCalls("SIP/abcd"))
    }

    "should unsubscribe from device tracker when logging out" in {
      val eventBus                           = mock[XucEventBus]
      val configRepository: ConfigRepository = mock[ConfigRepository]
      val deviceTracker                      = TestProbe()
      val fsm                                = createFSM(eventBus, configRepository, deviceTracker.ref)
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        xivoIp = "123.123.123.123",
        number = Some("1000")
      )
      val lineConfig = LineConfig("1", "1000", Some(line))

      when(configRepository.getLineConfig(LineConfigQueryByNb("1000")))
        .thenReturn(Some(lineConfig))

      fsm ! EventAgentLogin(1, "1000", "2000")

      deviceTracker.expectMsg(MonitorCalls("SIP/abcd"))

      fsm ! EventAgentLogout(1, "2000")
      deviceTracker.expectMsg(UnMonitorCalls("SIP/abcd"))
    }

    "should publish message when logging in" in {
      val eventBus                           = mock[XucEventBus]
      val configRepository: ConfigRepository = mock[ConfigRepository]
      val deviceTracker                      = TestProbe()
      val fsm                                = createFSM(eventBus, configRepository, deviceTracker.ref)
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        xivoIp = "123.123.123.123",
        number = Some("1000")
      )
      val lineConfig = LineConfig("1", "1000", Some(line))

      when(configRepository.getLineConfig(LineConfigQueryByNb("1000")))
        .thenReturn(Some(lineConfig))
      fsm ! EventAgentLogin(1, "1000", "2000")

      verify(eventBus).publish(AgentLoggingIn(1, "1000"))
    }

    "should publish message when logging out" in {
      val eventBus                           = mock[XucEventBus]
      val configRepository: ConfigRepository = mock[ConfigRepository]
      val deviceTracker                      = TestProbe()
      val fsm                                = createFSM(eventBus, configRepository, deviceTracker.ref)
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        xivoIp = "123.123.123.123",
        number = Some("1000")
      )
      val lineConfig = LineConfig("1", "1000", Some(line))

      when(configRepository.getLineConfig(LineConfigQueryByNb("1000")))
        .thenReturn(Some(lineConfig))
      reset(eventBus)

      fsm ! EventAgentLogin(1, "1000", "2000")
      verify(eventBus).publish(AgentLoggingIn(1, "1000"))

      fsm ! EventAgentLogout(1, "2000")
      verify(eventBus).publish(AgentLoggingOut(1))
    }

    "should publish AgentLoggedOut event when in MAgentLoggedOut state" in {
      val context         = AgentStateContext(None, Some("1000"), None, List.empty)
      val (eventBus, fsm) = createFSMForPublishing(MAgentReady, context)

      fsm ! EventAgentLogout(1, agentNumber)

      val captor = ArgumentCaptor.forClass(classOf[AgentLoggedOut])

      verify(eventBus).publish(captor.capture())

      captor.getValue should have(
        Symbol("id")(agentId),
        Symbol("phoneNb")("1000"),
        Symbol("queues")(List.empty[Int]),
        Symbol("agentNb")(agentNumber),
        Symbol("cause")(None)
      )

    }

    "should publish AgentReady event when in MAgentReady state" in {
      val context         = AgentStateEmptyContext()
      val (eventBus, fsm) = createFSMForPublishing(MAgentLoggedOut, context)

      fsm ! EventAgentLogin(agentId, "1000", agentNumber)

      val captor = ArgumentCaptor.forClass(classOf[AgentReady])

      verify(eventBus).publish(captor.capture())

      captor.getValue should have(
        Symbol("id")(agentId),
        Symbol("phoneNb")("1000"),
        Symbol("queues")(List.empty[Int]),
        Symbol("cause")(None)
      )

    }

  }

  "should publish AgentPause event when in MAgentOnPause state" in {
    val context         = AgentStateContext(None, Some("1000"), None, List.empty)
    val (eventBus, fsm) = createFSMForPublishing(MAgentReady, context)

    fsm ! EventAgentPause(agentId)

    val captor = ArgumentCaptor.forClass(classOf[AgentOnPause])

    verify(eventBus).publish(captor.capture())

    captor.getValue should have(
      Symbol("id")(agentId),
      Symbol("phoneNb")("1000"),
      Symbol("queues")(List.empty[Int]),
      Symbol("cause")(None)
    )

  }

  "should clear pause reason when AgentPause event without reason when in MAgentOnPause state with agentstatut" in {
    val context =
      AgentStateContext(Some("lunchTime"), Some("1000"), None, List.empty)
    val (eventBus, fsm) = createFSMForPublishing(MAgentReady, context)

    fsm ! EventAgentPause(agentId)

    val captor = ArgumentCaptor.forClass(classOf[AgentOnPause])

    verify(eventBus).publish(captor.capture())

    captor.getValue should have(
      Symbol("id")(agentId),
      Symbol("phoneNb")("1000"),
      Symbol("queues")(List.empty[Int]),
      Symbol("cause")(None)
    )

  }

  "should publish AgentWrapup event when in MAgentOnWrapup state" in {
    val context         = AgentStateContext(None, Some("1000"), None, List.empty)
    val (eventBus, fsm) = createFSMForPublishing(MAgentReady, context)

    fsm ! EventAgentWrapup(agentId)

    val captor = ArgumentCaptor.forClass(classOf[AgentOnWrapup])

    verify(eventBus).publish(captor.capture())

    captor.getValue should have(
      Symbol("id")(agentId),
      Symbol("phoneNb")("1000"),
      Symbol("queues")(List.empty[Int]),
      Symbol("cause")(None)
    )

  }

  "should publish AgentWrapup event when in MAgentOnWrapup state from MAgentPaused state" in {
    val context         = AgentStateContext(None, Some("1000"), None, List.empty)
    val (eventBus, fsm) = createFSMForPublishing(MAgentPaused, context)

    fsm ! EventAgentWrapup(agentId)

    val captor = ArgumentCaptor.forClass(classOf[AgentOnWrapup])

    verify(eventBus).publish(captor.capture())

    captor.getValue should have(
      Symbol("id")(agentId),
      Symbol("phoneNb")("1000"),
      Symbol("queues")(List.empty[Int]),
      Symbol("cause")(None)
    )
  }

  "should publish AgentOnCall event when logged and a call up" in {
    val channel = Channel(
      "12345679.012",
      "SIP/abcd-00001",
      CallerId("1000", "Someone"),
      "12345679.123",
      ChannelState.UP,
      direction = Some(ChannelDirection.OUTGOING),
      monitored = MonitorState.ACTIVE
    )
    val deviceCall =
      DeviceCall("SIP/abcd-00001", Some(channel), Set.empty, Map.empty)

    val calls =
      DeviceCalls("SIP/abcd", Map(deviceCall.channelName -> deviceCall))

    val context         = AgentStateContext(None, Some("1000"), None, List.empty)
    val (eventBus, fsm) = createFSMForPublishing(MAgentReady, context)

    fsm ! calls

    val captor = ArgumentCaptor.forClass(classOf[AgentOnCall])

    verify(eventBus).publish(captor.capture())

    captor.getValue should have(
      Symbol("id")(agentId),
      Symbol("phoneNb")("1000"),
      Symbol("acd")(false),
      Symbol("direction")(CallDirection.Outgoing),
      Symbol("onPause")(false),
      Symbol("queues")(List.empty[Int]),
      Symbol("cause")(None),
      Symbol("monitorState")(MonitorState.ACTIVE)
    )
  }

  "should publish AgentOnCall event when logged and a call up in pause" in {
    val channel = Channel(
      "12345679.012",
      "SIP/abcd-00001",
      CallerId("1000", "Someone"),
      "12345679.123",
      ChannelState.UP,
      direction = Some(ChannelDirection.OUTGOING)
    )
    val deviceCall =
      DeviceCall("SIP/abcd-00001", Some(channel), Set.empty, Map.empty)

    val calls =
      DeviceCalls("SIP/abcd", Map(deviceCall.channelName -> deviceCall))

    val context         = AgentStateContext(None, Some("1000"), None, List.empty)
    val (eventBus, fsm) = createFSMForPublishing(MAgentPaused, context)

    fsm ! calls

    val captor = ArgumentCaptor.forClass(classOf[AgentOnCall])

    verify(eventBus).publish(captor.capture())

    captor.getValue should have(
      Symbol("id")(agentId),
      Symbol("phoneNb")("1000"),
      Symbol("acd")(false),
      Symbol("direction")(CallDirection.Outgoing),
      Symbol("onPause")(true),
      Symbol("queues")(List.empty[Int]),
      Symbol("cause")(None)
    )
  }

  "should publish AgentOnCall event when logged and a call up in wrapup" in {
    val channel = Channel(
      "12345679.012",
      "SIP/abcd-00001",
      CallerId("1000", "Someone"),
      "12345679.123",
      ChannelState.UP,
      direction = Some(ChannelDirection.OUTGOING)
    )
    val deviceCall =
      DeviceCall("SIP/abcd-00001", Some(channel), Set.empty, Map.empty)

    val calls =
      DeviceCalls("SIP/abcd", Map(deviceCall.channelName -> deviceCall))

    val context         = AgentStateContext(None, Some("1000"), None, List.empty)
    val (eventBus, fsm) = createFSMForPublishing(MAgentOnWrapup, context)

    fsm ! calls

    val captor = ArgumentCaptor.forClass(classOf[AgentOnCall])

    verify(eventBus).publish(captor.capture())

    captor.getValue should have(
      Symbol("id")(agentId),
      Symbol("phoneNb")("1000"),
      Symbol("acd")(false),
      Symbol("direction")(CallDirection.Outgoing),
      Symbol("onPause")(true),
      Symbol("queues")(List.empty[Int]),
      Symbol("cause")(None)
    )
  }

  "should publish AgentRinging event when logged and a call ringing" in {
    val channel = Channel(
      "12345679.012",
      "SIP/abcd-00001",
      CallerId("1000", "Someone"),
      "12345679.123",
      ChannelState.RINGING,
      direction = Some(ChannelDirection.INCOMING)
    )
    val deviceCall =
      DeviceCall("SIP/abcd-00001", Some(channel), Set.empty, Map.empty)

    val calls =
      DeviceCalls("SIP/abcd", Map(deviceCall.channelName -> deviceCall))

    val context         = AgentStateContext(None, Some("1000"), None, List.empty)
    val (eventBus, fsm) = createFSMForPublishing(MAgentReady, context)

    fsm ! calls

    val captor = ArgumentCaptor.forClass(classOf[AgentRinging])

    verify(eventBus).publish(captor.capture())

    captor.getValue should have(
      Symbol("id")(agentId),
      Symbol("phoneNb")("1000"),
      Symbol("acd")(false),
      Symbol("queues")(List.empty[Int]),
      Symbol("cause")(None)
    )
  }

  "should publish AgentDialing event when logged and dialing" in {
    val channel = Channel(
      "12345679.012",
      "SIP/abcd-00001",
      CallerId("1000", "Someone"),
      "12345679.123",
      ChannelState.DIALING,
      direction = Some(ChannelDirection.OUTGOING)
    )
    val deviceCall =
      DeviceCall("SIP/abcd-00001", Some(channel), Set.empty, Map.empty)

    val calls =
      DeviceCalls("SIP/abcd", Map(deviceCall.channelName -> deviceCall))

    val context         = AgentStateContext(None, Some("1000"), None, List.empty)
    val (eventBus, fsm) = createFSMForPublishing(MAgentReady, context)

    fsm ! calls

    val captor = ArgumentCaptor.forClass(classOf[AgentDialing])

    verify(eventBus).publish(captor.capture())
    captor.getValue should have(
      Symbol("id")(agentId),
      Symbol("phoneNb")("1000"),
      Symbol("queues")(List.empty[Int]),
      Symbol("cause")(None)
    )
  }

}
