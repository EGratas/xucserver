package services.agent

import org.apache.pekko.actor.{ActorRef, Props}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.{
  QueueMembership,
  UserQueueDefaultMembership,
  UsersQueueMembership
}
import org.scalatestplus.mockito.MockitoSugar
import services.config.ConfigDispatcher.{
  ConfigChangeRequest,
  GetAgentByUserId,
  GetQueuesForAgent,
  QueuesForAgent,
  RequestConfig
}
import services.config.{ConfigServerRequester, GetEntry}
import services.request._
import xivo.models.{Agent, AgentQueueMember}
import scala.concurrent.duration._

class AgentConfigSpec extends TestKitSpec("AgentConfig") with MockitoSugar {

  class Helper {

    val requester: ConfigServerRequester = mock[ConfigServerRequester]
    val defaultMembershipRepo: TestProbe            = TestProbe()
    val moveAgentInGroups: TestProbe                = TestProbe()
    val addAgentInGroups: TestProbe                 = TestProbe()
    val removeAgentGroupFromQueueGroup: TestProbe   = TestProbe()
    val addAgentsNotInQueueFromGroupTo: TestProbe   = TestProbe()
    val agentGroupSetter: TestProbe                 = TestProbe()
    val configDispatcher: TestProbe                 = TestProbe()

    val mockMove: (Long, Long, Int) => ActorRef  = mock[(Long, Long, Int) => ActorRef]
    val mockEmpty: () => ActorRef = mock[() => ActorRef]

    def actor: (TestActorRef[AgentConfig], AgentConfig) = {
      val a = TestActorRef[AgentConfig](
        Props(
          new AgentConfig(
            requester,
            defaultMembershipRepo.ref,
            configDispatcher.ref,
            agentGroupSetter.ref
          )
        )
      )
      (a, a.underlyingActor)
    }
  }

  "Agent config" should {

    "get agent default membership" in new Helper {
      val (ref, agentConfig) = actor
      val userId             = 123
      val membership: List[QueueMembership] = List(QueueMembership(1, 5), QueueMembership(8, 1))
      val reqActor: TestProbe = TestProbe()

      ref ! BaseRequest(reqActor.ref, GetUserDefaultMembership(userId))
      defaultMembershipRepo.expectMsg(GetEntry(userId))
      defaultMembershipRepo.reply(
        UserQueueDefaultMembership(userId, membership)
      )

      reqActor.expectMsg(UserQueueDefaultMembership(userId, membership))

    }

    "set agent default membership" in new Helper {
      val (ref, agentConfig) = actor
      val userId             = 123
      val membership: List[QueueMembership] = List(QueueMembership(1, 5), QueueMembership(8, 1))

      ref ! BaseRequest(
        TestProbe().ref,
        SetUserDefaultMembership(userId, membership)
      )
      defaultMembershipRepo.expectMsg(
        SetUserDefaultMembership(userId, membership)
      )

    }

    "apply default membership to a set of users" in new Helper {
      val (ref, agentConfig) = actor

      val userId1  = 1L
      val agentId1: Long = userId1 + 10
      val userId2  = 2L
      val userId3  = 5L
      val agentId3: Long = userId3 + 10

      val userIds: List[Long] = List(userId1, userId2, userId3)

      // Active membership
      val user1Membership: List[AgentQueueMember] =
        List(AgentQueueMember(agentId1, 1, 2), AgentQueueMember(agentId1, 3, 5))
      val user3Membership: List[AgentQueueMember] = List(AgentQueueMember(agentId3, 3, 5))

      // Default membership
      val user1DefaultMembership: UserQueueDefaultMembership = UserQueueDefaultMembership(
        userId1,
        List(QueueMembership(1, 2), QueueMembership(2, 3))
      )
      val user2DefaultMembership: UserQueueDefaultMembership =
        UserQueueDefaultMembership(userId2, List.empty)
      val user3DefaultMembership: UserQueueDefaultMembership = UserQueueDefaultMembership(
        userId3,
        List(QueueMembership(1, 4), QueueMembership(2, 5))
      )

      ref ! BaseRequest(ref, ApplyUsersDefaultMembership(userIds))

      defaultMembershipRepo.expectMsg(GetEntry(userId1))
      defaultMembershipRepo.reply(user1DefaultMembership)

      configDispatcher.expectMsg(RequestConfig(ref, GetAgentByUserId(userId1)))
      configDispatcher.reply(
        Agent(agentId1, "James", "Bond", "3000", "", 0, userId1)
      )

      defaultMembershipRepo.expectMsg(GetEntry(userId2))
      defaultMembershipRepo.reply(user2DefaultMembership)

      defaultMembershipRepo.expectMsg(GetEntry(userId3))
      defaultMembershipRepo.reply(user3DefaultMembership)

      configDispatcher.receiveWhile(1.seconds, 1.seconds, 5) {
        case RequestConfig(ref, GetQueuesForAgent(id)) =>
          if (id == agentId1)
            configDispatcher.reply(QueuesForAgent(user1Membership, agentId1))
          else configDispatcher.reply(QueuesForAgent(user3Membership, agentId3))
        case ConfigChangeRequest(ref, RemoveAgentFromQueue(id, 3)) =>
        case ConfigChangeRequest(ref, SetAgentQueue(id, 2, 3))     =>
        case RequestConfig(ref, GetAgentByUserId(id)) =>
          configDispatcher.reply(
            Agent(agentId3, "Jason", "Bourne", "3000", "", 0, id)
          )
      }

      configDispatcher.expectMsg(
        ConfigChangeRequest(ref, RemoveAgentFromQueue(agentId3, 3))
      )
      configDispatcher.expectMsg(
        ConfigChangeRequest(ref, SetAgentQueue(agentId3, 1, 4))
      )
      configDispatcher.expectMsg(
        ConfigChangeRequest(ref, SetAgentQueue(agentId3, 2, 5))
      )

    }

    "set default membership for a list of users" in new Helper {
      val (ref, agentConfig) = actor
      val userIds: List[Long] = List(1L, 2L, 3L)
      val uqm: UsersQueueMembership = UsersQueueMembership(
        userIds,
        List(QueueMembership(1, 5), QueueMembership(8, 1))
      )

      ref ! BaseRequest(
        TestProbe().ref,
        SetUsersDefaultMembership(userIds, uqm.membership)
      )
      defaultMembershipRepo.expectMsg(
        SetUsersDefaultMembership(userIds, uqm.membership)
      )
    }
  }

}
