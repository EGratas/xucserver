package services.agent

import services.calltracking.DeviceCall
import xivo.xucami.models.{
  CallerId,
  Channel,
  ChannelDirection,
  ChannelState,
  MonitorState
}
import xivo.events.UserData
import xivo.events.CallDirection
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class AgentCallSpec extends AnyWordSpec with Matchers with MockitoSugar {

  "AgentCallSpec" should {

    val possibleChannelStates = Map(
      ChannelState.UNITIALIZED     -> None,
      ChannelState.HUNGUP          -> None,
      ChannelState.DOWN            -> None,
      ChannelState.RSRVD           -> None,
      ChannelState.OFFHOOK         -> None,
      ChannelState.DIALING         -> Some(AgentCallState.Dialing),
      ChannelState.ORIGINATING     -> Some(AgentCallState.Dialing),
      ChannelState.RINGING         -> Some(AgentCallState.Ringing),
      ChannelState.UP              -> Some(AgentCallState.OnCall),
      ChannelState.BUSY            -> None,
      ChannelState.DIALING_OFFHOOK -> None,
      ChannelState.PRERING         -> None,
      ChannelState.HOLD            -> Some(AgentCallState.OnCall)
    )

    val possibleAcdState = Map(
      None              -> false,
      Some("queuename") -> true
    )

    val possibleDirection = Map(
      None                            -> None,
      Some(ChannelDirection.INCOMING) -> Some(CallDirection.Incoming),
      Some(ChannelDirection.OUTGOING) -> Some(CallDirection.Outgoing)
    )

    val possibleMonitorState = List(
      MonitorState.DISABLED,
      MonitorState.ACTIVE,
      MonitorState.PAUSED
    )

    "transform all basic call to an agent call" in {
      for {
        (channelState, callState)         <- possibleChannelStates
        (qname, isAcd)                    <- possibleAcdState
        (channelDirection, callDirection) <- possibleDirection
        monitor                           <- possibleMonitorState
      } {
        val channel = Channel(
          "12345679.012",
          "SIP/abcd-00001",
          CallerId("1000", "Someone"),
          "12345679.123",
          channelState,
          direction = channelDirection,
          monitored = monitor
        )
        val remoteChannelWithoutQueue = Channel(
          "12345679.123",
          "SIP/efgh-00001",
          CallerId("1001", "Someone else"),
          "12345679.012"
        )
        val remoteChannel = qname
          .map(q =>
            remoteChannelWithoutQueue.updateVariable(UserData.QueueNameKey, q)
          )
          .getOrElse(remoteChannelWithoutQueue)

        val deviceCall = DeviceCall(
          "SIP/abcd-00001",
          Some(channel),
          Set.empty,
          Map(remoteChannel.name -> remoteChannel)
        )

        if (callState.isDefined && callDirection.isDefined) {
          val agentCall =
            AgentCall(callState.get, isAcd, callDirection.get, monitor)
          AgentCall.deviceCallToAgentCall(deviceCall) should be(Some(agentCall))
        } else {
          AgentCall.deviceCallToAgentCall(deviceCall) should be(None)
        }
      }
    }

    "transform a empty list of deviceCall in a empty list of agentCall" in {
      val calls = List.empty[DeviceCall]

      AgentCall.callListToAgentCallList(calls) should be(List.empty)
    }

    "transform a list of n deviceCall in a list of n valid agentcall" in {
      val channel1 = Channel(
        "12345679.012",
        "SIP/abcd-00001",
        CallerId("1000", "Someone"),
        "12345679.123",
        ChannelState.ORIGINATING,
        remoteState = Some(ChannelState.RINGING),
        direction = Some(ChannelDirection.OUTGOING)
      )
      val deviceCall1 =
        DeviceCall("SIP/abcd-00001", Some(channel1), Set.empty, Map.empty)
      val channel2 = Channel(
        "12345679.012",
        "SIP/abcd-00002",
        CallerId("1000", "Someone"),
        "12345679.123",
        ChannelState.UP,
        remoteState = Some(ChannelState.RINGING),
        direction = Some(ChannelDirection.INCOMING)
      )
      val deviceCall2 =
        DeviceCall("SIP/abcd-00002", Some(channel2), Set.empty, Map.empty)
      val calls = List(deviceCall1, deviceCall2)

      AgentCall.callListToAgentCallList(calls) should contain.only(
        AgentCall.deviceCallToAgentCall(deviceCall1).get,
        AgentCall.deviceCallToAgentCall(deviceCall2).get
      )
    }

    "transform a list of n deviceCall in a list of m valid agentCall" in {
      val channel1 = Channel(
        "12345679.012",
        "SIP/abcd-00001",
        CallerId("1000", "Someone"),
        "12345679.123",
        ChannelState.ORIGINATING,
        remoteState = Some(ChannelState.RINGING),
        direction = Some(ChannelDirection.OUTGOING)
      )
      val deviceCall1 =
        DeviceCall("SIP/abcd-00001", Some(channel1), Set.empty, Map.empty)
      val channel2 = Channel(
        "12345679.012",
        "SIP/abcd-00002",
        CallerId("1000", "Someone"),
        "12345679.123",
        ChannelState.DOWN,
        remoteState = Some(ChannelState.RINGING),
        direction = Some(ChannelDirection.INCOMING)
      )
      val deviceCall2 =
        DeviceCall("SIP/abcd-00002", Some(channel2), Set.empty, Map.empty)
      val calls = List(deviceCall1, deviceCall2)

      AgentCall.callListToAgentCallList(calls) should contain only AgentCall
        .deviceCallToAgentCall(deviceCall1)
        .get

    }

    "transform an empty list of agentCall in None" in {
      val agentCalls = List.empty[AgentCall]
      AgentCall.currentAgentCall(agentCalls) should be(None)
    }

    "transform a list of one agentCall to this agentCall" in {
      val agentCall =
        AgentCall(AgentCallState.OnCall, true, CallDirection.Incoming)
      AgentCall.currentAgentCall(List(agentCall)) should be(Some(agentCall))
    }

    "keep in priority acd calls over non acd calls" in {
      val agentCallAcd =
        AgentCall(AgentCallState.OnCall, true, CallDirection.Incoming)
      val agentCallNonAcd =
        AgentCall(AgentCallState.OnCall, false, CallDirection.Incoming)
      AgentCall.currentAgentCall(List(agentCallAcd, agentCallNonAcd)) should be(
        Some(agentCallAcd)
      )
      AgentCall.currentAgentCall(List(agentCallNonAcd, agentCallAcd)) should be(
        Some(agentCallAcd)
      )
    }

    "keep in priority oncall calls over dialing" in {
      val agentCallOnCall =
        AgentCall(AgentCallState.OnCall, false, CallDirection.Incoming)
      val agentCallDialing =
        AgentCall(AgentCallState.Dialing, false, CallDirection.Outgoing)
      AgentCall.currentAgentCall(
        List(agentCallOnCall, agentCallDialing)
      ) should be(Some(agentCallOnCall))
      AgentCall.currentAgentCall(
        List(agentCallDialing, agentCallOnCall)
      ) should be(Some(agentCallOnCall))
    }

    "keep in priority dialing calls over ringing" in {
      val agentCallRinging =
        AgentCall(AgentCallState.Ringing, false, CallDirection.Incoming)
      val agentCallDialing =
        AgentCall(AgentCallState.Dialing, false, CallDirection.Outgoing)
      AgentCall.currentAgentCall(
        List(agentCallRinging, agentCallDialing)
      ) should be(Some(agentCallDialing))
      AgentCall.currentAgentCall(
        List(agentCallDialing, agentCallRinging)
      ) should be(Some(agentCallDialing))
    }

    "keep in priority oncall calls over ringing" in {
      val agentCallOnCall =
        AgentCall(AgentCallState.OnCall, false, CallDirection.Incoming)
      val agentCallRinging =
        AgentCall(AgentCallState.Ringing, false, CallDirection.Outgoing)
      AgentCall.currentAgentCall(
        List(agentCallOnCall, agentCallRinging)
      ) should be(Some(agentCallOnCall))
      AgentCall.currentAgentCall(
        List(agentCallRinging, agentCallOnCall)
      ) should be(Some(agentCallOnCall))
    }

  }

}
