package services.agent

import java.util.Date
import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import controllers.helpers.{RequestError, RequestSuccess}
import org.joda.time.DateTime
import org.json.JSONObject
import org.mockito.Mockito.{verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.IpbxCommandResponse
import services.XucAmiBus.*
import services.XucEventBus
import services.config.ConfigRepository
import services.request.*
import xivo.events.AgentState.*
import xivo.events.CallDirection
import xivo.models.Agent
import xivo.websocket.WebSocketEvent

class AgentActionSpec extends TestKitSpec("AgentAction") with MockitoSugar {

  import xivo.models.LineHelper.makeLine

  val ctiLink: TestProbe                 = TestProbe()
  val configDispatcher: TestProbe        = TestProbe()
  val mockMessageFactory: MessageFactory = mock[MessageFactory]
  val messageFactory                     = new MessageFactory()
  val configRepo: ConfigRepository       = mock[ConfigRepository]
  val amiBusConnector: TestProbe         = TestProbe()
  val eventBus: XucEventBus              = mock[XucEventBus]

  class Helper {
    def actor: (TestActorRef[AgentAction], AgentAction) = {
      val a = TestActorRef[AgentAction](
        Props(
          new AgentAction(
            ctiLink.ref,
            mockMessageFactory,
            configRepo,
            eventBus,
            amiBusConnector.ref
          )
        )
      )
      (a, a.underlyingActor)
    }
  }

  "agent action service " should {
    "send agent listen to cti link" in new Helper {

      val agentId = 56
      val userId  = 72

      when(configRepo.getLineForUser(userId)).thenReturn(
        Some(makeLine(34, "default", "sip", "oljjo", None, None, "ip"))
      )
      when(configRepo.getLineForAgent(agentId)).thenReturn(
        Some(makeLine(56, "default", "sip", "kvdsd", None, None, "ip"))
      )

      val (ref, _) = actor

      ref ! AgentListen(agentId, Some(userId))

      amiBusConnector.expectMsg(
        ListenRequest(
          ListenActionRequest(listener = "SIP/oljjo", listened = "SIP/kvdsd")
        )
      )

    }
    "send request agent login agentid phone number to cti link" in new Helper {
      val loginMsg: JSONObject = messageFactory.createAgentLogin("89", "756")
      when(mockMessageFactory.createAgentLogin("89", "7560")).thenReturn(loginMsg)
      when(configRepo.getAgentLoggedOnPhoneNumber("7560")).thenReturn(None)
      val (ref, _) = actor

      ref ! AgentLoginRequest(Some(89), Some("7560"))

      ctiLink.expectMsg(loginMsg)

    }
    "send agent logout request if a different agent is logged on the phone number before sending login request" in
      new Helper {
        val phoneNumber     = "2000"
        val alreadyLoggedId = 56L
        val agentId         = 345
        val loginMsg: JSONObject =
          messageFactory.createAgentLogin(agentId.toString, phoneNumber)
        val logoutMsg: JSONObject =
          messageFactory.createAgentLogout(alreadyLoggedId.toString)
        when(configRepo.getAgentLoggedOnPhoneNumber(phoneNumber))
          .thenReturn(Some(alreadyLoggedId))
        when(mockMessageFactory.createAgentLogout(alreadyLoggedId.toString))
          .thenReturn(logoutMsg)
        when(mockMessageFactory.createAgentLogin(agentId.toString, phoneNumber))
          .thenReturn(loginMsg)

        val (ref, _) = actor

        ref ! AgentLoginRequest(Some(agentId), Some(phoneNumber))

        ctiLink.expectMsg(logoutMsg)
        ctiLink.expectMsg(loginMsg)
      }
    "send request agent login with only agentid to cti link" in new Helper {
      val agentId  = 78
      val loginMsg: JSONObject = messageFactory.createAgentLogin(agentId.toString, "")
      when(mockMessageFactory.createAgentLogin(agentId.toString, ""))
        .thenReturn(loginMsg)
      val (ref, _) = actor

      ref ! AgentLoginRequest(Some(agentId), None)

      ctiLink.expectMsg(loginMsg)
    }
    "send request agent login with only phone number to cti link" in new Helper {
      val phoneNumber = "44200"
      val loginMsg: JSONObject = messageFactory.createAgentLogin(phoneNumber)
      when(mockMessageFactory.createAgentLogin(phoneNumber)).thenReturn(loginMsg)
      val (ref, _) = actor

      ref ! AgentLoginRequest(None, Some(phoneNumber))

      ctiLink.expectMsg(loginMsg)
    }
    "send back encoded agent request error" in new Helper {
      val (ref, _) = actor

      val agentRequestError =
        new IpbxCommandResponse("agent_login_invalid_exten", new Date)

      ref ! agentRequestError

      expectMsg(WebSocketEvent.createEvent(agentRequestError))
    }

    "send agent logout request on agent logout received" in new Helper {
      val (ref, _)  = actor
      val phoneNb   = "1011"
      val agentId   = 11L
      val logoutMsg: JSONObject = messageFactory.createAgentLogout(agentId.toString)
      when(configRepo.getAgentLoggedOnPhoneNumber(phoneNb))
        .thenReturn(Some(agentId))
      when(mockMessageFactory.createAgentLogout(agentId.toString))
        .thenReturn(logoutMsg)

      ref ! BaseRequest(self, AgentLogout(phoneNb))

      ctiLink.expectMsg(logoutMsg)
    }

    "reject a base request with agent login request if the agent doesn't exist" in new Helper {
      val (ref, _) = actor
      val agentId  = 11L

      when(configRepo.getAgent(agentId)).thenReturn(None)

      ref ! BaseRequest(self, AgentLoginRequest(Some(agentId), None))

      expectMsgClass(classOf[RequestError])
    }

    "forward a base request with agent login request with phoneNb only to itself" in new Helper {
      val (ref, _) = actor
      val phoneNb  = "1011"
      val loginMsg: JSONObject = messageFactory.createAgentLogin(phoneNb)
      when(mockMessageFactory.createAgentLogin(phoneNb)).thenReturn(loginMsg)
      ref ! BaseRequest(self, AgentLoginRequest(None, Some(phoneNb)))
      verify(eventBus).subscribe(self, XucEventBus.allAgentsEventsTopic)
      ctiLink.expectMsg(loginMsg)
    }

    "reply to a base request with agent login request when the agent is already logged on" in new Helper {
      val (ref, _) = actor
      val agentId  = 14L
      val agent: Agent = Agent(agentId, "first", "last", "number", "context")
      when(configRepo.getAgent(agentId)).thenReturn(Some(agent))
      when(configRepo.getAgentState(agentId)).thenReturn(
        Some(
          AgentReady(
            agentId,
            new DateTime,
            "phoneNb",
            List[Int](),
            None,
            "2000"
          )
        )
      )
      ref ! BaseRequest(self, AgentLoginRequest(Some(agentId), None))

      expectMsgClass(classOf[RequestSuccess])
    }

    "forward a base request with agent login request to itself when agentState is logged out" in new Helper {
      val (ref, _) = actor
      val agentId  = 14L
      val agent: Agent = Agent(agentId, "first", "last", "number", "context")
      val loginMsg: JSONObject = messageFactory.createAgentLogin(agentId.toString, "")
      when(configRepo.getAgent(agentId)).thenReturn(Some(agent))
      when(configRepo.getAgentState(agentId)).thenReturn(
        Some(
          AgentLoggedOut(
            agentId,
            new DateTime,
            "phoneNb",
            List[Int](),
            "",
            None
          )
        )
      )
      when(mockMessageFactory.createAgentLogin(agentId.toString, ""))
        .thenReturn(loginMsg)
      ref ! BaseRequest(self, AgentLoginRequest(Some(agentId), None))

      ctiLink.expectMsg(loginMsg)
    }

    "forward a base request with agent login request to itself when agentState is unknown" in new Helper {
      val (ref, _) = actor
      val agentId  = 14L
      val agent: Agent = Agent(agentId, "first", "last", "number", "context")
      val loginMsg: JSONObject = messageFactory.createAgentLogin(agentId.toString, "")
      when(configRepo.getAgent(agentId)).thenReturn(Some(agent))
      when(configRepo.getAgentState(agentId)).thenReturn(None)
      when(mockMessageFactory.createAgentLogin(agentId.toString, ""))
        .thenReturn(loginMsg)
      ref ! BaseRequest(self, AgentLoginRequest(Some(agentId), None))

      ctiLink.expectMsg(loginMsg)
    }

    "reject a base request with agent login request when agentNumber is unknown" in new Helper {
      val (ref, _) = actor
      val agentNb  = "2001"
      val phoneNb  = "1001"
      when(configRepo.getAgent(agentNb)).thenReturn(None)
      ref ! BaseRequest(
        self,
        AgentLoginRequest(None, Some(phoneNb), Some(agentNb))
      )

      expectMsgClass(classOf[RequestError])
    }

    "forward a base request with agent login request to itself when agentNumber is known" in new Helper {
      val (ref, _) = actor
      val agentNb  = "2001"
      val phoneNb  = "1001"
      val agentId  = 14L
      val agent: Agent = Agent(agentId, "first", "last", agentNb, "context")
      val loginMsg: JSONObject = messageFactory.createAgentLogin(agentId.toString, phoneNb)
      when(configRepo.getAgent(agentNb)).thenReturn(Some(agent))
      when(configRepo.getAgentState(agentId)).thenReturn(
        Some(
          AgentLoggedOut(agentId, new DateTime, phoneNb, List[Int](), "", None)
        )
      )
      when(configRepo.getAgentLoggedOnPhoneNumber(phoneNb)).thenReturn(None)
      when(mockMessageFactory.createAgentLogin(agentId.toString, phoneNb))
        .thenReturn(loginMsg)

      ref ! BaseRequest(
        self,
        AgentLoginRequest(None, Some(phoneNb), Some(agentNb))
      )

      ctiLink.expectMsg(loginMsg)
    }

    "send agent logout request to cti link" in new Helper {
      val (ref, _) = actor
      val agentId  = 67L

      val logoutMsg: JSONObject = messageFactory.createAgentLogout(agentId.toString)
      when(mockMessageFactory.createAgentLogout(agentId.toString))
        .thenReturn(logoutMsg)

      ref ! AgentLogoutRequest(Some(agentId))

      ctiLink.expectMsg(logoutMsg)

    }

    "send agent pause request to ami bus connector" in new Helper {
      val (ref, _) = actor
      val agentId  = 67L
      val reason   = "testReason"

      ref ! AgentPauseRequest(Some(agentId), Some(reason))

      val request: QueuePauseRequest = amiBusConnector.expectMsgType[QueuePauseRequest]
      request.message.iface shouldBe s"Local/id-$agentId@agentcallback"
      request.message.reason shouldBe Some("testReason")
    }
    "send agent unpause request to ami bus connector" in new Helper {
      val (ref, _) = actor
      val agentId  = 987L

      ref ! AgentUnPauseRequest(Some(agentId))

      val request: QueueUnpauseRequest = amiBusConnector.expectMsgType[QueueUnpauseRequest]
      request.message.iface shouldBe s"Local/id-$agentId@agentcallback"

    }
  }
  "agent action service on toggle pause received " should {
    class togglePauseHelper extends Helper {
      val (ref, _)   = actor
      val phoneNb    = "3400"
      val agentId    = 35L
      val pauseMsg: JSONObject = messageFactory.createPauseAgent(agentId.toString)
      val unpauseMsg: JSONObject = messageFactory.createUnpauseAgent(agentId.toString)
      when(configRepo.getAgentLoggedOnPhoneNumber(phoneNb))
        .thenReturn(Some(agentId))
      when(mockMessageFactory.createPauseAgent(agentId.toString))
        .thenReturn(pauseMsg)
      when(mockMessageFactory.createUnpauseAgent(agentId.toString))
        .thenReturn(unpauseMsg)

    }
    "pause agent if agent ready" in new togglePauseHelper {
      when(configRepo.getAgentState(agentId)).thenReturn(
        Some(AgentReady(agentId, null, phoneNb, List(), Some(""), "2000"))
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(pauseMsg)
    }
    "pause agent if agent on call ready" in new togglePauseHelper {
      when(configRepo.getAgentState(agentId)).thenReturn(
        Some(
          AgentOnCall(
            agentId,
            null,
            true,
            CallDirection.Incoming,
            phoneNb,
            List(),
            onPause = false,
            agentNb = "2000"
          )
        )
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(pauseMsg)
    }
    "un pause agent if agent on call on pause" in new togglePauseHelper {
      when(configRepo.getAgentState(agentId)).thenReturn(
        Some(
          AgentOnCall(
            agentId,
            null,
            true,
            CallDirection.Incoming,
            phoneNb,
            List(),
            onPause = true,
            agentNb = "2000"
          )
        )
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(unpauseMsg)
    }
    "set agent ready if agent on pause" in new togglePauseHelper {
      when(configRepo.getAgentState(agentId)).thenReturn(
        Some(AgentOnPause(agentId, null, phoneNb, List(), agentNb = "2000"))
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(unpauseMsg)
    }
    "set agent ready if agent on wrapup" in new togglePauseHelper {
      when(configRepo.getAgentState(agentId)).thenReturn(
        Some(AgentOnWrapup(agentId, null, phoneNb, List(), agentNb = "2000"))
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(unpauseMsg)
    }

  }
}
