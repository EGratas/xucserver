package services.agent

import org.joda.time.DateTime
import org.mockito.ArgumentMatchers.*
import org.mockito.ArgumentMatchers.{eq => mockitoEq}
import org.scalatestplus.mockito.MockitoSugar
import services.AgentStateFSM.*
import xivo.events.AgentState.*
import xuctest.BaseTest
import org.mockito.Mockito.{atLeastOnce, never, times, verify, verifyNoInteractions}
import xivo.events.CallDirection

class AgentStatCalculatorSpec extends BaseTest with MockitoSugar {

  class Helper {
    trait TestTimeProvider extends TimeProvider {
      var dateTime = new DateTime
      def getTime: DateTime = dateTime
    }

    case class AgentEventTotalTime(
        override val name: String,
        collector: StatCollector
    ) extends AgentStatCalculator
        with AgentTotalTime
        with TestTimeProvider {}

  }

  class AgentDateTimeHelper {
    val statCollector: StatCollector = mock[StatCollector]

  }

  def agentOnCall(acd: Boolean = true): AgentStateContext =
    AgentStateContext(
      None,
      None,
      Some(AgentCall(AgentCallState.OnCall, acd, CallDirection.Incoming))
    )

  def agentRinging(acd: Boolean = true): AgentStateContext =
    AgentStateContext(
      None,
      None,
      Some(AgentCall(AgentCallState.Ringing, acd, CallDirection.Incoming))
    )

  def agentDialing: AgentStateContext =
    AgentStateContext(
      None,
      None,
      Some(AgentCall(AgentCallState.Dialing, false, CallDirection.Outgoing))
    )

  def agentOnCallOut: AgentStateContext =
    AgentStateContext(
      None,
      None,
      Some(AgentCall(AgentCallState.OnCall, false, CallDirection.Outgoing))
    )

  def agentNoCall: AgentStateContext =
    AgentStateContext(
      None,
      None,
      None
    )

  "agent login time" should {
    "be calculated from login event date time" in new AgentDateTimeHelper {
      val loginDateTime: LoginDateTime = LoginDateTime("loginDateTime", statCollector)
      val loginDate     = new DateTime(2005, 3, 26, 11, 0, 0, 0)

      val toContext: AgentStateContext = AgentStateContext(None, None, None, List.empty, loginDate)

      loginDateTime.processTransition(
        AgentTransition(
          MAgentLoggedOut,
          AgentStateEmptyContext(),
          MAgentReady,
          toContext
        )
      )

      verify(statCollector).onStatCalculated(
        "loginDateTime",
        StatDateTime(loginDate)
      )
    }
    "be calculated from logout event date time" in new AgentDateTimeHelper {
      val logoutDateTime: LogoutDateTime = LogoutDateTime("logoutDateTime", statCollector)
      val logoutDate     = new DateTime(2006, 1, 16, 22, 0, 0, 0)

      val eventLogout: AgentLoggedOut = AgentLoggedOut(21, logoutDate, "1010", List(), "")

      logoutDateTime.processEvent(eventLogout)

      verify(statCollector).onStatCalculated(
        "logoutDateTime",
        StatDateTime(logoutDate)
      )
    }
  }
  "agentTotalTime" should {
    "only intialize once start time" in new Helper {

      val total: AgentEventTotalTime = AgentEventTotalTime("test", mock[StatCollector])

      val startTime = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.start(startTime.plusSeconds(20))
      total.dateTime = startTime.plusSeconds(600)
      total.accumulate().toInt should be(600)
    }

    "should not accumulate any more after stop and publish" in new Helper {

      val total: AgentEventTotalTime = AgentEventTotalTime("test", mock[StatCollector])

      val startTime = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime = startTime.plusSeconds(60)
      total.stopAndPublish()
      total.dateTime = startTime.plusSeconds(120)
      total.accumulate().toInt should be(60)

    }

    "reset period to 0 and restart from reset date if started" in new Helper {
      val statCollector: StatCollector = mock[StatCollector]
      val total: AgentEventTotalTime = AgentEventTotalTime("test", statCollector)
      val startTime     = new DateTime(2015, 4, 2, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime = new DateTime(2015, 4, 3, 0, 0, 0, 0)
      total.reset()
      total.accumulate().toInt should be(0)
      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }
    "should reset period to 0 and do not reset date if not started" in new Helper {
      val statCollector: StatCollector = mock[StatCollector]

      val total: AgentEventTotalTime = AgentEventTotalTime("test", statCollector)

      val startTime = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime = startTime.plusSeconds(10)
      total.reset()
      total.dateTime = total.dateTime.plusSeconds(30)
      total.stopAndPublish()

      verify(statCollector).onStatCalculated("test", StatPeriod(30))
      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }

    "should publish 0 on reset" in new Helper {
      val statCollector: StatCollector = mock[StatCollector]
      val total: AgentEventTotalTime = AgentEventTotalTime("test", statCollector)

      val startTime = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime = startTime.plusSeconds(200)
      total.reset()

      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }
    "on reset publish value for stopped statistics" in new Helper {
      val statCollector: StatCollector = mock[StatCollector]
      val total: AgentEventTotalTime = AgentEventTotalTime("test", statCollector)

      total.reset()

      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }
    "do not publish stat if not started" in new Helper {
      val statCollector: StatCollector = mock[StatCollector]
      val total: AgentEventTotalTime = AgentEventTotalTime("test", statCollector)

      total.stopAndPublish()

      verify(statCollector, never).onStatCalculated("test", StatPeriod(0))

    }
    "publish 0 if end time is before start time machine time desynchronized" in new Helper {
      val statCollector: StatCollector = mock[StatCollector]
      val total: AgentEventTotalTime = AgentEventTotalTime("test", statCollector)
      val startTime     = new DateTime(2005, 3, 26, 11, 0, 0, 0)
      total.start(startTime)
      total.dateTime = startTime.plusSeconds(-2)

      total.stopAndPublish()

      verify(statCollector).onStatCalculated("test", StatPeriod(0))

    }
  }

  class TotalCallHelper {
    val statCollector: StatCollector = mock[StatCollector]

    case class AgentAnyTotalCalls(
        override val name: String,
        collector: StatCollector
    ) extends AgentStatCalculator
        with AgentTotalCalls {}

    val totalCalls: AgentAnyTotalCalls = AgentAnyTotalCalls("testTotalCalls", statCollector)
  }
  "Agent total call" should {
    "be set to 0 on creation" in new TotalCallHelper {

      totalCalls.total should be(0)
    }

    "reset total to 0 " in new TotalCallHelper {
      totalCalls.reset()
      totalCalls.total should be(0)
    }
    "publish 0 on reset" in new TotalCallHelper {
      totalCalls.total = 58
      totalCalls.reset()
      totalCalls.total should be(0)

      verify(statCollector).onStatCalculated("testTotalCalls", StatTotal(0))
    }
  }

  "Agent total inbound calls" should {
    "increment and publish totalInbound on agent ringing" in {
      val statCollector = mock[StatCollector]
      val totalInbound  = AgentInboundTotalCalls("totalInbound", statCollector)

      totalInbound.processTransition(
        AgentTransition(
          MAgentReady,
          AgentStateEmptyContext(),
          MAgentReady,
          agentOnCall()
        )
      )
      verify(statCollector).onStatCalculated("totalInbound", StatTotal(1))

      totalInbound.processTransition(
        AgentTransition(
          MAgentReady,
          AgentStateEmptyContext(),
          MAgentReady,
          agentRinging()
        )
      )
      verify(statCollector).onStatCalculated("totalInbound", StatTotal(2))

      totalInbound.processTransition(
        AgentTransition(
          MAgentPaused,
          AgentStateEmptyContext(),
          MAgentPaused,
          agentRinging()
        )
      )
      verify(statCollector).onStatCalculated("totalInbound", StatTotal(3))

      totalInbound.processTransition(
        AgentTransition(
          MAgentPaused,
          AgentStateEmptyContext(),
          MAgentPaused,
          agentOnCall()
        )
      )
      verify(statCollector).onStatCalculated("totalInbound", StatTotal(4))

      totalInbound.processTransition(
        AgentTransition(
          MAgentOnWrapup,
          AgentStateEmptyContext(),
          MAgentOnWrapup,
          agentOnCall()
        )
      )
      verify(statCollector).onStatCalculated("totalInbound", StatTotal(5))
    }

    "increment and publish totalInbound with multiple calls" in {
      val statCollector = mock[StatCollector]
      val totalInbound  = AgentInboundTotalCalls("totalInbound", statCollector)

      // Assume two calls, one is OnCall, second is Ringing
      // First call is hungup so we transition from OnCall -> Ringing
      totalInbound.processTransition(
        AgentTransition(MAgentReady, agentOnCall(), MAgentReady, agentRinging())
      )
      verify(statCollector).onStatCalculated("totalInbound", StatTotal(1))

      // Assume two calls, one is Dialing, second is Ringing
      // First call is hungup so we transition from Dialing -> Ringing
      totalInbound.processTransition(
        AgentTransition(MAgentReady, agentDialing, MAgentReady, agentRinging())
      )
      verify(statCollector).onStatCalculated("totalInbound", StatTotal(2))

    }
  }

  "Agent total inbound ACD calls" should {
    "increment and publish totalInbound on agent ringing ACD call" in {
      val statCollector = mock[StatCollector]
      val totalInboundAcd =
        AgentInboundTotalAcdCalls("totalInboundAcd", statCollector)

      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentReady,
          AgentStateEmptyContext(),
          MAgentReady,
          agentOnCall()
        )
      )
      verify(statCollector).onStatCalculated("totalInboundAcd", StatTotal(1))

      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentReady,
          AgentStateEmptyContext(),
          MAgentReady,
          agentRinging()
        )
      )
      verify(statCollector).onStatCalculated("totalInboundAcd", StatTotal(2))

      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentPaused,
          AgentStateEmptyContext(),
          MAgentPaused,
          agentRinging()
        )
      )
      verify(statCollector).onStatCalculated("totalInboundAcd", StatTotal(3))

      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentPaused,
          AgentStateEmptyContext(),
          MAgentPaused,
          agentOnCall()
        )
      )
      verify(statCollector).onStatCalculated("totalInboundAcd", StatTotal(4))

      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentOnWrapup,
          AgentStateEmptyContext(),
          MAgentOnWrapup,
          agentOnCall()
        )
      )
      verify(statCollector).onStatCalculated("totalInboundAcd", StatTotal(5))
    }
    "ignore agent ringing non-ACD call" in {
      val statCollector = mock[StatCollector]
      val totalInboundAcd =
        AgentInboundTotalAcdCalls("totalInboundAcd", statCollector)

      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentReady,
          AgentStateEmptyContext(),
          MAgentReady,
          agentOnCall(false)
        )
      )
      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentReady,
          AgentStateEmptyContext(),
          MAgentReady,
          agentRinging(false)
        )
      )
      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentPaused,
          AgentStateEmptyContext(),
          MAgentPaused,
          agentRinging(false)
        )
      )
      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentPaused,
          AgentStateEmptyContext(),
          MAgentPaused,
          agentOnCall(false)
        )
      )
      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentOnWrapup,
          AgentStateEmptyContext(),
          MAgentPaused,
          agentOnCall(false)
        )
      )
      verifyNoInteractions(statCollector)
    }

    "increment and publish totalInboundAcd if acd information is received later" in {
      val statCollector = mock[StatCollector]
      val totalInboundAcd =
        AgentInboundTotalAcdCalls("totalInboundAcd", statCollector)

      // First receive an event that we have a non-acd call
      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentReady,
          AgentStateEmptyContext(),
          MAgentReady,
          agentRinging(false)
        )
      )
      verifyNoInteractions(statCollector)

      // Then we receive acd information for the same call
      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentReady,
          agentRinging(false),
          MAgentReady,
          agentRinging(true)
        )
      )
      verify(statCollector).onStatCalculated("totalInboundAcd", StatTotal(1))

    }

    "ignore if acd information is received later when already onCall" in {
      val statCollector = mock[StatCollector]
      val totalInboundAcd =
        AgentInboundTotalAcdCalls("totalInboundAcd", statCollector)

      // First receive an event that we have a non-acd call
      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentReady,
          AgentStateEmptyContext(),
          MAgentReady,
          agentOnCall(false)
        )
      )
      verifyNoInteractions(statCollector)

      // Then we receive acd information for the same call
      totalInboundAcd.processTransition(
        AgentTransition(
          MAgentReady,
          agentOnCall(false),
          MAgentReady,
          agentOnCall(true)
        )
      )
      verifyNoInteractions(statCollector)

    }
  }

  "Agent inbound answered calls" should {
    "increment and publish answeredInbound on transition to agentOnCall" in {
      val statCollector = mock[StatCollector]
      val answeredInbound =
        AgentInboundAnsweredCalls("answeredInbound", statCollector)

      answeredInbound.processTransition(
        AgentTransition(MAgentReady, agentRinging(), MAgentReady, agentOnCall())
      )
      verify(statCollector).onStatCalculated("answeredInbound", StatTotal(1))

      answeredInbound.processTransition(
        AgentTransition(
          MAgentPaused,
          agentRinging(),
          MAgentPaused,
          agentOnCall()
        )
      )
      verify(statCollector).onStatCalculated("answeredInbound", StatTotal(2))
    }
  }

  "Agent inbound answered ACD calls" should {
    "increment and publish answeredInbound on transition to agentOnCall on ACD call" in {
      val statCollector = mock[StatCollector]
      val answeredAcdInbound =
        AgentInboundAnsweredAcdCalls("answeredAcdInbound", statCollector)

      answeredAcdInbound.processTransition(
        AgentTransition(MAgentReady, agentRinging(), MAgentReady, agentOnCall())
      )
      verify(statCollector).onStatCalculated("answeredAcdInbound", StatTotal(1))

      answeredAcdInbound.processTransition(
        AgentTransition(
          MAgentPaused,
          agentRinging(),
          MAgentPaused,
          agentOnCall()
        )
      )
      verify(statCollector).onStatCalculated("answeredAcdInbound", StatTotal(2))
    }
    "ignore answeredInbound on transition to agentOnCall on non-ACD call" in {
      val statCollector = mock[StatCollector]
      val answeredAcdInbound =
        AgentInboundAnsweredAcdCalls("answeredAcdInbound", statCollector)

      answeredAcdInbound.processTransition(
        AgentTransition(
          MAgentReady,
          agentRinging(false),
          MAgentReady,
          agentOnCall(false)
        )
      )
      answeredAcdInbound.processTransition(
        AgentTransition(
          MAgentPaused,
          agentRinging(false),
          MAgentPaused,
          agentOnCall(false)
        )
      )
      verifyNoInteractions(statCollector)
    }
  }

  "Agent Inbound Total Call Time" should {
    "accumulate conversation time" in {
      val statCollector = mock[StatCollector]
      val timeInbound   = AgentInboundTotalCallTime("timeInbound", statCollector)

      val answer: AgentTransition = AgentTransition(
        MAgentReady,
        agentRinging(false),
        MAgentReady,
        agentOnCall(false)
      )
      val hangup: AgentTransition = AgentTransition(
        MAgentReady,
        agentOnCall(false),
        MAgentReady,
        agentNoCall
      )
      timeInbound.processTransition(answer)
      timeInbound.processTransition(hangup)
      verify(statCollector).onStatCalculated(
        mockitoEq("timeInbound"),
        any[StatPeriod]
      )
    }
  }

  "Agent Inbound Total ACD Call Time" should {
    "accumulate conversation time on ACD call" in {
      val statCollector = mock[StatCollector]
      val timeInbound =
        AgentInboundTotalAcdCallTime("timeAcdInbound", statCollector)
      val answer: AgentTransition =
        AgentTransition(MAgentReady, agentRinging(), MAgentReady, agentOnCall())
      val hangup: AgentTransition =
        AgentTransition(MAgentReady, agentOnCall(), MAgentReady, agentNoCall)
      timeInbound.processTransition(answer)
      timeInbound.processTransition(hangup)
      verify(statCollector).onStatCalculated(
        mockitoEq("timeAcdInbound"),
        any[StatPeriod]
      )
    }
    "do not accumulate conversation time on non ACD call" in {
      val statCollector = mock[StatCollector]
      val timeInbound =
        AgentInboundTotalAcdCallTime("timeAcdInbound", statCollector)
      val answer: AgentTransition = AgentTransition(
        MAgentReady,
        agentRinging(false),
        MAgentReady,
        agentOnCall(false)
      )
      val hangup: AgentTransition = AgentTransition(
        MAgentReady,
        agentOnCall(false),
        MAgentReady,
        agentNoCall
      )
      timeInbound.processTransition(answer)
      timeInbound.processTransition(hangup)
      verifyNoInteractions(statCollector)
    }
  }

  "Agent outbound total calls" should {
    "increment and publish on agent dialing" in {
      val statCollector = mock[StatCollector]
      val totalOutbound =
        AgentOutboundTotalCalls("totalOutbound", statCollector)

      val outcall1 =
        AgentTransition(MAgentReady, agentNoCall, MAgentReady, agentDialing)
      totalOutbound.processTransition(outcall1)
      verify(statCollector).onStatCalculated("totalOutbound", StatTotal(1))

      val outcall2 = AgentTransition(
        MAgentReady,
        agentOnCall(false),
        MAgentReady,
        agentDialing
      )
      totalOutbound.processTransition(outcall2)
      verify(statCollector).onStatCalculated("totalOutbound", StatTotal(2))
    }
  }

  "Agent Outbound Total Call Time" should {
    "accumulate conversation time from ready" in {
      val statCollector = mock[StatCollector]
      val timeInbound =
        AgentOutboundTotalCallTime("timeOutBound", statCollector)
      val answer: AgentTransition =
        AgentTransition(MAgentReady, agentDialing, MAgentReady, agentOnCallOut)
      val hangup: AgentTransition =
        AgentTransition(MAgentReady, agentOnCallOut, MAgentReady, agentNoCall)
      timeInbound.processTransition(answer)
      timeInbound.processTransition(hangup)
      verify(statCollector).onStatCalculated(
        mockitoEq("timeOutBound"),
        any[StatPeriod]
      )
    }
    "accumulate conversation time from pause" in {
      val statCollector = mock[StatCollector]
      val timeInbound =
        AgentOutboundTotalCallTime("timeOutBound", statCollector)
      val answer: AgentTransition = AgentTransition(
        MAgentPaused,
        agentDialing,
        MAgentPaused,
        agentOnCallOut
      )
      val hangup: AgentTransition =
        AgentTransition(MAgentPaused, agentOnCallOut, MAgentPaused, agentNoCall)
      timeInbound.processTransition(answer)
      timeInbound.processTransition(hangup)
      verify(statCollector).onStatCalculated(
        mockitoEq("timeOutBound"),
        any[StatPeriod]
      )
    }
  }

  "AgentTransitionQualifier" should {
    "provide a function detecting Answer of an incoming call" in {
      object TestAgentTransitionQualifier extends AgentTransitionQualifier
      val answer: AgentTransition = AgentTransition(
        MAgentReady,
        agentRinging(false),
        MAgentReady,
        agentOnCall(false)
      )
      TestAgentTransitionQualifier.isNewAnsweredIncomingCall(
        answer
      ) shouldBe true

      val stay: AgentTransition = AgentTransition(
        MAgentReady,
        agentOnCall(false),
        MAgentReady,
        agentOnCall(false)
      )
      TestAgentTransitionQualifier.isNewAnsweredIncomingCall(
        stay
      ) shouldBe false

      val outgoing: AgentTransition = AgentTransition(
        MAgentReady,
        agentOnCallOut,
        MAgentReady,
        agentOnCallOut
      )
      TestAgentTransitionQualifier.isNewAnsweredIncomingCall(
        outgoing
      ) shouldBe false

    }
    "provide a function detecting Answer of an outging call" in {
      object TestAgentTransitionQualifier extends AgentTransitionQualifier

      val answer: AgentTransition =
        AgentTransition(MAgentReady, agentDialing, MAgentReady, agentOnCallOut)
      TestAgentTransitionQualifier.isNewAnsweredOutgoingCall(
        answer
      ) shouldBe true

      val stay: AgentTransition = AgentTransition(
        MAgentReady,
        agentOnCallOut,
        MAgentReady,
        agentOnCallOut
      )
      TestAgentTransitionQualifier.isNewAnsweredOutgoingCall(
        stay
      ) shouldBe false

      val incoming: AgentTransition = AgentTransition(
        MAgentReady,
        agentRinging(),
        MAgentReady,
        agentOnCallOut
      )
      TestAgentTransitionQualifier.isNewAnsweredOutgoingCall(
        incoming
      ) shouldBe false
    }
    "provide a function detecting call hangup" in {
      object TestAgentTransitionQualifier extends AgentTransitionQualifier
      val answer: AgentTransition =
        AgentTransition(MAgentReady, agentOnCallOut, MAgentReady, agentNoCall)
      TestAgentTransitionQualifier.isHangup(answer) shouldBe true

      val ready: AgentTransition =
        AgentTransition(MAgentReady, agentNoCall, MAgentReady, agentNoCall)
      TestAgentTransitionQualifier.isHangup(ready) shouldBe false

      val stay: AgentTransition = AgentTransition(
        MAgentReady,
        agentOnCallOut,
        MAgentReady,
        agentOnCallOut
      )
      TestAgentTransitionQualifier.isHangup(stay) shouldBe false

    }

  }

  "Agent Inbound Average Call Time" should {
    class AgtInbAverHelper {
      val statCollector: StatCollector = mock[StatCollector]
      val inboundAverageCallTime: AgentInboundAverageCallTime =
        AgentInboundAverageCallTime(collector = statCollector)
      val statName: String = AgentInboundAverageCallTime.name

      def inbTotalCallTime(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundTotalCallTime.name, StatPeriod(nb)))
        )
      def answeredCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundAnsweredCalls.name, StatTotal(nb)))
        )
    }
    "increment and publish rounded value on new agent inbound total call time" in new AgtInbAverHelper {

      inboundAverageCallTime.processStat(answeredCalls(3L))
      inboundAverageCallTime.processStat(inbTotalCallTime(10L))

      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(3)))
    }
    "on 0 answered calls, should publish 0 as average" in new AgtInbAverHelper {

      inboundAverageCallTime.processStat(answeredCalls(0L))
      inboundAverageCallTime.processStat(inbTotalCallTime(10L))

      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(0)))

    }
  }

  "Agent Inbound Average ACD Call Time" should {
    class AgtInbAverHelper {
      val statCollector: StatCollector = mock[StatCollector]
      val inboundAverageAcdCallTime: AgentInboundAverageAcdCallTime =
        AgentInboundAverageAcdCallTime(collector = statCollector)
      val statName: String = AgentInboundAverageAcdCallTime.name

      def inbTotalCallTime(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundTotalAcdCallTime.name, StatPeriod(nb)))
        )
      def answeredCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundAnsweredAcdCalls.name, StatTotal(nb)))
        )
    }
    "increment and publish rounded value on new agent inbound total call time" in new AgtInbAverHelper {

      inboundAverageAcdCallTime.processStat(answeredCalls(3L))
      inboundAverageAcdCallTime.processStat(inbTotalCallTime(10L))

      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(3)))
    }
    "on 0 answered calls, should publish 0 as average" in new AgtInbAverHelper {

      inboundAverageAcdCallTime.processStat(answeredCalls(0L))
      inboundAverageAcdCallTime.processStat(inbTotalCallTime(10L))

      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(0)))

    }
  }

  "Agent Total unanswered calls" should {
    class AgtUnansCallsHelper {
      val statCollector: StatCollector = mock[StatCollector]
      val inboundUnansweredCalls: AgentInboundUnansweredCalls =
        AgentInboundUnansweredCalls(collector = statCollector)
      val statName: String = AgentInboundUnansweredCalls.name

      def totalCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundTotalCalls.name, StatTotal(nb)))
        )
      def answeredCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundAnsweredCalls.name, StatTotal(nb)))
        )
    }

    "on first total calls publish total calls" in new AgtUnansCallsHelper {
      inboundUnansweredCalls.processStat(totalCalls(12L))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(12L)))

    }
    "publish total calls minus answered calls on last answered calls received" in new AgtUnansCallsHelper {
      inboundUnansweredCalls.processStat(totalCalls(12L))
      inboundUnansweredCalls.processStat(answeredCalls(10L))
      verify(statCollector, atLeastOnce())
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(2L)))

    }
    "do not publish if more answered calls than received calls could be after reset" in new AgtUnansCallsHelper {
      inboundUnansweredCalls.processStat(answeredCalls(22L))
      verifyNoInteractions(statCollector)
    }
    "publish on last total calls total minus last answerd calls" in new AgtUnansCallsHelper {
      inboundUnansweredCalls.processStat(answeredCalls(5L))
      inboundUnansweredCalls.processStat(totalCalls(12L))
      verify(statCollector, atLeastOnce())
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(7L)))

    }
    "Reset all values to 0 on reset and publish" in new AgtUnansCallsHelper {
      inboundUnansweredCalls.processStat(totalCalls(12L))
      inboundUnansweredCalls.processStat(answeredCalls(10L))
      inboundUnansweredCalls.reset()
      verify(statCollector, atLeastOnce())
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(0L)))

    }

  }

  "Agent Total unanswered ACD calls" should {
    class AgtUnansCallsHelper {
      val statCollector: StatCollector = mock[StatCollector]
      val inboundUnansweredAcdCalls: AgentInboundUnansweredAcdCalls =
        AgentInboundUnansweredAcdCalls(collector = statCollector)
      val statName: String = AgentInboundUnansweredAcdCalls.name

      def totalAcdCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundTotalAcdCalls.name, StatTotal(nb)))
        )
      def answeredAcdCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundAnsweredAcdCalls.name, StatTotal(nb)))
        )
    }

    "on first total calls publish total calls" in new AgtUnansCallsHelper {
      inboundUnansweredAcdCalls.processStat(totalAcdCalls(12L))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(12L)))

    }
    "publish total calls minus answered calls on last answered calls received" in new AgtUnansCallsHelper {
      inboundUnansweredAcdCalls.processStat(totalAcdCalls(12L))
      inboundUnansweredAcdCalls.processStat(answeredAcdCalls(10L))
      verify(statCollector, atLeastOnce())
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(2L)))

    }
    "do not publish if more answered calls than received calls could be after reset" in new AgtUnansCallsHelper {
      inboundUnansweredAcdCalls.processStat(answeredAcdCalls(22L))
      verifyNoInteractions(statCollector)
    }
    "publish on last total calls total minus last answerd calls" in new AgtUnansCallsHelper {
      inboundUnansweredAcdCalls.processStat(answeredAcdCalls(5L))
      inboundUnansweredAcdCalls.processStat(totalAcdCalls(12L))
      verify(statCollector, atLeastOnce())
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(7L)))

    }
    "Reset all values to 0 on reset and publish" in new AgtUnansCallsHelper {
      inboundUnansweredAcdCalls.processStat(totalAcdCalls(12L))
      inboundUnansweredAcdCalls.processStat(answeredAcdCalls(10L))
      inboundUnansweredAcdCalls.reset()
      verify(statCollector, atLeastOnce())
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatTotal(0L)))

    }

  }

  "Agent unanswered calls percentage" should {
    class AgtUnanswHelper {
      val statCollector: StatCollector = mock[StatCollector]
      val inboundUnansweredCalls: AgentInboundPercentUnansweredCalls =
        AgentInboundPercentUnansweredCalls(collector = statCollector)
      val statName: String = AgentInboundPercentUnansweredCalls.name

      def totalCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundTotalCalls.name, StatTotal(nb)))
        )
      def unAnsweredCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundUnansweredCalls.name, StatTotal(nb)))
        )
    }
    "on first total calls publish 0 %" in new AgtUnanswHelper {
      inboundUnansweredCalls.processStat(totalCalls(12L))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(0.0)))

    }
    "on first unanswered calls publish 100%" in new AgtUnanswHelper {
      inboundUnansweredCalls.processStat(unAnsweredCalls(40L))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(100.0)))

    }

    "publish percentage of unanswered calls" in new AgtUnanswHelper {
      inboundUnansweredCalls.processStat(totalCalls(12L))
      inboundUnansweredCalls.processStat(unAnsweredCalls(6L))
      inboundUnansweredCalls.processStat(totalCalls(24L))

      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(0.0)))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(50.0)))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(25.0)))
    }
    "publish max 100% if answered calls > total calls (period transition)" in new AgtUnanswHelper {
      inboundUnansweredCalls.processStat(totalCalls(5L))
      inboundUnansweredCalls.processStat(unAnsweredCalls(6L))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(0.0)))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(100.0)))

    }
    "publish O% on reset and restart calculation" in new AgtUnanswHelper {
      inboundUnansweredCalls.processStat(totalCalls(12L))
      inboundUnansweredCalls.processStat(unAnsweredCalls(6L))
      inboundUnansweredCalls.reset()
      inboundUnansweredCalls.processStat(totalCalls(10L))
      inboundUnansweredCalls.processStat(unAnsweredCalls(2L))

      verify(statCollector, times(3))
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(0.0)))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(50.0)))
      verify(statCollector, atLeastOnce())
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(20.0)))
    }
  }

  "Agent unanswered ACD calls percentage" should {
    class AgtUnanswHelper {
      val statCollector: StatCollector = mock[StatCollector]
      val inboundUnansweredAcdCalls: AgentInboundPercentUnansweredAcdCalls =
        AgentInboundPercentUnansweredAcdCalls(collector = statCollector)
      val statName: String = AgentInboundPercentUnansweredAcdCalls.name

      def totalCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundTotalAcdCalls.name, StatTotal(nb)))
        )
      def unAnsweredCalls(nb: Long): AgentStatistic =
        AgentStatistic(
          44,
          List(Statistic(AgentInboundUnansweredAcdCalls.name, StatTotal(nb)))
        )
    }
    "on first total calls publish 0 %" in new AgtUnanswHelper {
      inboundUnansweredAcdCalls.processStat(totalCalls(12L))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(0.0)))

    }
    "on first unanswered calls publish 100%" in new AgtUnanswHelper {
      inboundUnansweredAcdCalls.processStat(unAnsweredCalls(40L))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(100.0)))

    }

    "publish percentage of unanswered calls" in new AgtUnanswHelper {
      inboundUnansweredAcdCalls.processStat(totalCalls(12L))
      inboundUnansweredAcdCalls.processStat(unAnsweredCalls(6L))
      inboundUnansweredAcdCalls.processStat(totalCalls(24L))

      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(0.0)))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(50.0)))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(25.0)))
    }
    "publish max 100% if answered calls > total calls (period transition)" in new AgtUnanswHelper {
      inboundUnansweredAcdCalls.processStat(totalCalls(5L))
      inboundUnansweredAcdCalls.processStat(unAnsweredCalls(6L))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(0.0)))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(100.0)))

    }
    "publish O% on reset and restart calculation" in new AgtUnanswHelper {
      inboundUnansweredAcdCalls.processStat(totalCalls(12L))
      inboundUnansweredAcdCalls.processStat(unAnsweredCalls(6L))
      inboundUnansweredAcdCalls.reset()
      inboundUnansweredAcdCalls.processStat(totalCalls(10L))
      inboundUnansweredAcdCalls.processStat(unAnsweredCalls(2L))

      verify(statCollector, times(3))
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(0.0)))
      verify(statCollector).onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(50.0)))
      verify(statCollector, atLeastOnce())
        .onStatCalculated(mockitoEq(statName), mockitoEq(StatAverage(20.0)))
    }
  }

}
