package services.agent

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import services.agent.AgentInGroupAction.RemoveAgents
import services.config.ConfigDispatcher._
import services.request.RemoveAgentFromQueue
import xivo.models.Agent
import org.apache.pekko.actor.ActorRef

class AgentGroupRemoverSpec extends TestKitSpec("agentingroupremover") {

  class Helper {
    val configDispatcher: TestProbe = TestProbe()

    def actor(groupId: Long, queueId: Long, penalty: Int): ActorRef = {
      system.actorOf(
        Props(
          new AgentGroupRemover(groupId, queueId, penalty, configDispatcher.ref)
        )
      )
    }
  }

  "an agent group remover" should {

    "remove agent group from queue penalty on RemoveAgents command" in new Helper {

      val (groupId, queueId, penalty) = (9, 52, 7)
      val ref: ActorRef = actor(groupId, queueId, penalty)

      val agents: List[Agent] = List(Agent(1L, "Kim", "Notch", "4589", "browser", groupId))

      ref ! RemoveAgents

      ref ! AgentList(agents)

      configDispatcher.expectMsgAllOf(
        RequestConfig(ref, GetAgents(groupId, queueId, penalty)),
        ConfigChangeRequest(ref, RemoveAgentFromQueue(1L, queueId))
      )

    }
  }
}
