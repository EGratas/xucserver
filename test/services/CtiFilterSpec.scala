package services

import org.apache.pekko.actor.PoisonPill
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.{LineConfig, XucUser}
import org.joda.time.DateTime
import org.mockito.Mockito.{timeout, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{UserConfigUpdate as _, *}
import org.xivo.cti.model.PhoneHintStatus
import play.api.libs.json.{JsValue, Json, Writes}
import services.XucStatsEventBus.{AggregatedStatEvent, Stat}
import services.callhistory.CallHistoryEnricher
import services.calltracking.SipDriver
import services.config.ObjectType.*
import services.config.ConfigDispatcher.*
import services.config.{ConfigRepository, ConfigServerRequester}
import services.request.*
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped}
import xivo.events.AgentState.{AgentDialing, AgentOnWrapup, AgentReady}
import xivo.events.*
import cti.models.{CtiStatus, CtiStatusActionLegacy, CtiStatusLegacy}
import org.scalatest.Assertion
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.*
import xivo.network.LoggedOn
import xivo.websocket.WsBus.WsContent
import xivo.websocket.{WSMsgType, WebSocketEvent}
import xivo.xucami.models.CallerId
import xuctest.XucUserHelper

import scala.concurrent.duration.DurationInt

class CtiFilterSpec
    extends TestKitSpec("CtiFilterSpec")
    with MockitoSugar
    with XucUserHelper {

  class Helper {
    val messageFactory   = new MessageFactory()
    val eventBus: XucEventBus         = mock[XucEventBus]
    val configRepository: ConfigRepository = mock[ConfigRepository]
    type ToBrowserMessage = JsValue
    val ctiRouter: TestProbe                  = TestProbe()
    val agentActionService: TestProbe         = TestProbe()
    val callHistoryEnricherFactory: CallHistoryEnricher.Factory = mock[CallHistoryEnricher.Factory]
    val personalContactRepository: TestProbe  = TestProbe()
    val configServerRequester: ConfigServerRequester      = mock[ConfigServerRequester]
    val userPreferenceService: TestProbe      = TestProbe()

    def actor(
        user: Option[XucUser] = None,
        filterConfig: Option[FilterConfig] = None
    ): (TestActorRef[CtiFilter], CtiFilter) = {

      val a = TestActorRef(
        new CtiFilter(
          user.get,
          ctiRouter.ref,
          personalContactRepository.ref,
          configRepository,
          eventBus,
          callHistoryEnricherFactory,
          userPreferenceService.ref
        )
      )
      filterConfig.foreach(a.underlyingActor.filterConfig = _)
      ctiRouter.expectMsg(RequestConfig(a, GetUserServices(1)))
      (a, a.underlyingActor)
    }

    def createUserConfigUpdate(
        userId: Int,
        agentId: Int,
        phoneId: Int
    ): UserConfigUpdate = {
      UserConfigUpdate(
        userId = userId,
        firstName = "First",
        lastName = "Last",
        fullName = "First Last",
        agentId = agentId,
        dndEnabled = false,
        naFwdEnabled = false,
        naFwdDestination = "",
        uncFwdEnabled = false,
        uncFwdDestination = "",
        busyFwdEnabled = false,
        busyFwdDestination = "",
        mobileNumber = "",
        lineIds = List(phoneId),
        voiceMailId = 0,
        voiceMailEnabled = false
      )
    }

    def createUserServices(
        userId: Int,
        dnd: Boolean,
        forward: Boolean,
        destination: String
    ): UserServicesUpdated = {
      val userServicesUpdated = UserServicesUpdated(
        userId,
        UserServices(
          dnd,
          UserForward(forward, destination),
          UserForward(forward, destination),
          UserForward(forward, destination)
        )
      )
      userServicesUpdated
    }

    def createPhoneStatusMessageForMe(phoneId: String, status: String): PhoneStatusUpdate = {
      val phoneStatusUpdate = new PhoneStatusUpdate()
      phoneStatusUpdate.setLineId(Integer.parseInt(phoneId))
      phoneStatusUpdate.setHintStatus(
        PhoneHintStatus.valueOf(status).getHintStatus.toString
      )
      phoneStatusUpdate
    }

    def createPhoneConfigUpdateMessageForMe(
        phoneId: String,
        phoneNumber: String
    ): PhoneConfigUpdate = {
      val phoneConfigUpdate = new PhoneConfigUpdate()
      phoneConfigUpdate.setNumber(phoneNumber)
      phoneConfigUpdate.setId(Integer.parseInt(phoneId))
      phoneConfigUpdate
    }

    def expectToBrowserMessage[T](msgType: String, payLoad: T)(implicit
        tjs: Writes[T]
    ): ToBrowserMessage = {
      val toBrowserMessage = createToBrowserMessage(msgType, payLoad)
      expectMsg(toBrowserMessage)
    }

    def createToBrowserMessage[T](msgType: String, payLoad: T)(implicit
        tjs: Writes[T]
    ): ToBrowserMessage =
      Json.obj(
        "msgType"    -> msgType,
        "ctiMessage" -> payLoad
      )
  }

  "A CtiFilter actor" should {

    "subscribe to user event bus on start" in new Helper {
      val user: XucUser = getXucUser("bob", "pwd")
      val (ref, _) = actor(Some(user))

      val userTopic: XucEventBus.Topic = XucEventBus.userEventTopic(1)
      verify(eventBus).subscribe(ref, userTopic)
    }

    """upon reception of LoggedOn:
    - save userId
    - save user statuses and send it to web socket
    - send evtLoggedOn,
    - send UserConfigUpdated to config dispatcher
    - send GetUserConfig request
    - send GetUserServices request
    - Request user status""" in new Helper {

      import services.config.ObjectType.TypeUser

      val user: XucUser = getXucUser("bob", "pwd")
      val userId = 5
      val (ref, ctiFilter) =
        actor(Some(user), Some(FilterConfig().withUserId(userId)))
      val myMsgFactory: MessageFactory = mock[MessageFactory]
      ctiFilter.messageFactory = myMsgFactory

      val ctiStatuses: List[CtiStatus] = List(
        CtiStatus(Option("test"), Option("test"), Option(1)),
        CtiStatus(Option("toto"), Option("toto"), Option(0))
      )
      val ctiStatuses2: List[CtiStatusLegacy] = List(
        CtiStatusLegacy(
          Option("test"),
          Option("test"),
          Option("red"),
          List(CtiStatusActionLegacy("queueunpause_all", Option("")))
        ),
        CtiStatusLegacy(
          Option("toto"),
          Option("toto"),
          Option("red"),
          List(CtiStatusActionLegacy("queuepause_all", Option("")))
        )
      )
      val wsUserStatuses: ToBrowserMessage = WebSocketEvent.createEvent(ctiStatuses)
      val wsUserStatuses2: ToBrowserMessage = WebSocketEvent.createEventLegacy(ctiStatuses2)
      val wsLoggedOn: ToBrowserMessage = Json.parse("{\"msgType\": \"LoggedOn\"}")
      val requestUserStatus: RequestStatus = RequestStatus(ref, 5, TypeUser)

      when(configRepository.getCtiStatusesLegacy(1))
        .thenReturn(Some(ctiStatuses2))
      when(configRepository.getCtiStatuses(1)).thenReturn(Some(ctiStatuses))

      ref ! LoggedOn(user, userId.toString)

      expectMsgAllOf(
        wsLoggedOn,
        wsUserStatuses,
        wsUserStatuses2,
        requestUserStatus
      )

      ctiRouter.expectMsg(UserConfigUpdated(user.xivoUser.id, None))
      ctiRouter.expectMsg(
        RequestConfig(ref, GetUserConfig(user.xivoUser.id.toInt))
      )
      ctiRouter.expectMsg(
        RequestConfig(ref, GetUserServices(user.xivoUser.id.toInt))
      )

      ctiFilter.filterConfig.userId should be(Some(userId))
    }

    """upon reception of ClientConnected :
      | send evtLoggedOn
      | send user statuses to web socket
      | Request user status
      | Request phone status
      | Send GetUserConfig request
      | Send GetUserServices request
    """ in new Helper {
      val userId          = 896
      val agentId         = 741
      val phoneId         = 623
      val phoneHintStatus: PhoneHintStatus = PhoneHintStatus.getHintStatus(4)
      val lineConfig: LineConfig = LineConfig(phoneId.toString, "2350")
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2350"))
      val (ref, ctiFilter) = actor(
        Some(user),
        Some(
          FilterConfig()
            .withAgentId(agentId)
            .withUserId(userId)
            .withLineConfig(lineConfig)
        )
      )
      val connectedActor: TestProbe = TestProbe()
      val myMsgFactory: MessageFactory = mock[MessageFactory]
      ctiFilter.messageFactory = myMsgFactory

      val wsLoggedOn: ToBrowserMessage = Json.parse("{\"msgType\": \"LoggedOn\"}")

      val userStatuses: List[CtiStatus] = List(
        CtiStatus(Option("test"), Option("test"), Option(1)),
        CtiStatus(Option("toto"), Option("toto"), Option(0))
      )
      val userStatuses2: List[CtiStatusLegacy] = List(
        CtiStatusLegacy(
          Option("test"),
          Option("test"),
          Option("red"),
          List(CtiStatusActionLegacy("queueunpause_all", Option("")))
        ),
        CtiStatusLegacy(
          Option("toto"),
          Option("toto"),
          Option("red"),
          List(CtiStatusActionLegacy("queuepause_all", Option("")))
        )
      )
      val wsUserStatuses: ToBrowserMessage = WebSocketEvent.createEvent(userStatuses)
      val wsUserStatuses2: ToBrowserMessage = WebSocketEvent.createEventLegacy(userStatuses2)
      val wsPhoneStatusUpdate: ToBrowserMessage = WebSocketEvent.createEvent(phoneHintStatus)
      val requestUserStatus: RequestStatus = RequestStatus(ref, userId, TypeUser)

      when(configRepository.getCtiStatusesLegacy(1))
        .thenReturn(Some(userStatuses2))
      when(configRepository.getCtiStatuses(1)).thenReturn(Some(userStatuses))
      when(
        configRepository.getPhoneHintStatusByNumber("2350")
      )
        .thenReturn(Some(phoneHintStatus))

      ref ! ClientConnected(
        connectedActor.ref,
        LoggedOn(user, userId.toString)
      )

      connectedActor.expectMsgAllOf(
        WsContent(wsLoggedOn),
        WsContent(wsUserStatuses),
        WsContent(wsUserStatuses2),
        WsContent(wsPhoneStatusUpdate)
      )
      ctiRouter.expectMsg(RequestStatus(ref, userId, TypeUser))
      ctiRouter.expectMsg(
        RequestConfig(ref, GetUserConfig(user.xivoUser.id.toInt))
      )
      ctiRouter.expectMsg(
        RequestConfig(ref, GetUserServices(user.xivoUser.id.toInt))
      )

    }
  }
  "A ctifiler on user config update" should {
    trait UserConfigUpdateData {

      val userId  = 55
      val agentId = 41
      val phoneId = 3

      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2350"))

      val myMsgFactory: MessageFactory = mock[MessageFactory]

      val agentTopic: XucEventBus.Topic = XucEventBus.agentEventTopic(agentId)
      val agentLogTopic: XucEventBus.Topic = XucEventBus.agentLogTransitionTopic(agentId)

      def checkFilter(filterConfig: FilterConfig): Assertion = {
        filterConfig.agentId should be(Some(agentId))
        filterConfig.userId should be(Some(userId))
      }
    }
    """upon reception of UserUpdateConfig when agent not logged in (empty agentId):
      - save agentId,
      - request line configuration by id,
      - request last agent status
      - unsubscribe to agent events
      - subcribe to agent events for this id
      - publish user config update
      """ in new Helper with UserConfigUpdateData {

      val (ref, ctiFilter) =
        actor(Some(user), Some(FilterConfig().withUserId(userId)))
      ctiFilter.messageFactory = myMsgFactory

      val userConfigUpdate: UserConfigUpdate = createUserConfigUpdate(userId, agentId, phoneId)
      val jsonUserConfigUpdate: ToBrowserMessage = WebSocketEvent.createEvent(userConfigUpdate)

      ref ! userConfigUpdate

      checkFilter(ctiFilter.filterConfig)

      ctiRouter.expectMsgAllOf(
        BaseRequest(ref, UpdateLineForUser(userConfigUpdate.userId)),
        RequestStatus(ref, agentId, TypeAgent),
        jsonUserConfigUpdate
      )

      verify(eventBus).unsubscribe(ref, agentTopic)
      verify(eventBus).unsubscribe(ref, agentLogTopic)

      verify(eventBus).subscribe(ref, agentTopic)
      verify(eventBus).subscribe(ref, agentLogTopic)
    }

    "do not update agentId if not present (0) in UserUpdateConfig" in new Helper {
      val agentId = 127
      val userId  = 55
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2350"))
      val (ref, ctiFilter) = actor(
        Some(user),
        Some(FilterConfig().withAgentId(agentId).withUserId(userId))
      )

      val userConfigUpdate: UserConfigUpdate = createUserConfigUpdate(userId, 0, 55)

      ref ! userConfigUpdate

      expectNoMessage(100.millis)
      ctiFilter.filterConfig.agentId should be(Some(agentId))
    }
    "Do not request line config if agentid and line config is set in config" in new Helper {
      val agentId    = 127
      val userId     = 55
      val lineConfig: LineConfig = LineConfig("56", "3400")
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2350"))
      val (ref, ctiFilter) = actor(
        Some(user),
        Some(
          FilterConfig()
            .withAgentId(agentId)
            .withUserId(userId)
            .withLineConfig(lineConfig)
        )
      )

      val userConfigUpdate: UserConfigUpdate = createUserConfigUpdate(userId, 0, 55)
      val jsonUserConfigUpdate: ToBrowserMessage = WebSocketEvent.createEvent(userConfigUpdate)

      ref ! userConfigUpdate

      ctiRouter.expectMsg(jsonUserConfigUpdate)
    }

    "Update UserConfigUpdate object with updated field only" in new Helper {
      val agentId    = 127
      val userId     = 55
      val lineConfig: LineConfig = LineConfig("56", "3400")
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2350"))
      val (ref, ctiFilter) = actor(
        Some(user),
        Some(
          FilterConfig()
            .withAgentId(agentId)
            .withUserId(userId)
            .withLineConfig(lineConfig)
        )
      )

      val userConfigUpdate: UserConfigUpdate = createUserConfigUpdate(userId, 0, 55)
      val jsonUserConfigUpdate: ToBrowserMessage = WebSocketEvent.createEvent(userConfigUpdate)

      ref ! userConfigUpdate

      ctiRouter.expectMsg(jsonUserConfigUpdate)

      val newUserConfigUpdate: UserConfigUpdate =
        createUserConfigUpdate(userId, 0, 55).copy(fullName = "Bond")
      ref ! newUserConfigUpdate

      val expected: UserConfigUpdate = userConfigUpdate.copy(fullName = "Bond")
      val newJson: ToBrowserMessage = WebSocketEvent.createEvent(expected)
      ctiRouter.expectMsg(newJson)
    }

    "Merge UserConfigUpdate object with UserStatuses" in new Helper {
      val user: XucUser = getXucUser("bob", "pwd")
      val userId = 1
      val (ref, _) = actor(
        Some(user),
        Some(
          FilterConfig()
            .withUserId(userId)
        )
      )

      val userConfigUpdate: UserConfigUpdate = createUserConfigUpdate(userId, 0, 0)
      val jsonUserConfigUpdate: ToBrowserMessage = WebSocketEvent.createEvent(userConfigUpdate)

      ref ! userConfigUpdate

      ctiRouter.expectMsg(BaseRequest(ref, UpdateLineForUser(userId)))
      ctiRouter.expectMsg(jsonUserConfigUpdate)

      val userStatuses: UserServicesUpdated = createUserServices(userId, true, true, "1000")
      ref ! userStatuses

      val expected: UserConfigUpdate = userConfigUpdate.copy(
        dndEnabled = true,
        busyFwdEnabled = true,
        busyFwdDestination = "1000",
        naFwdEnabled = true,
        naFwdDestination = "1000",
        uncFwdEnabled = true,
        uncFwdDestination = "1000"
      )

      val newJson: ToBrowserMessage = WebSocketEvent.createEvent(expected)
      ctiRouter.expectMsg(newJson)
    }
  }
  "A Ctifilter " should {

    "Stop Personnal contact actor when terminated and unsubscribe to user xuc event" in new Helper {
      val user: XucUser = getXucUser("testTerminated", "pwd", Some("2004"))
      val lineConfig: LineConfig = LineConfig("3", "2350")
      val (ref, ctiFilter) =
        actor(Some(user), Some(FilterConfig().withLineConfig(lineConfig)))

      val probe: TestProbe = TestProbe()
      probe.watch(personalContactRepository.ref)

      ref ! PoisonPill
      probe.expectTerminated(personalContactRepository.ref)

    }

    "send PhoneStatusChanged when phone status update received" in new Helper {
      val user: XucUser = getXucUser("testPhoneStatusChanged", "pwd", Some("2004"))
      val lineConfig: LineConfig = LineConfig("3", "2350")
      val (ref, ctiFilter) =
        actor(Some(user), Some(FilterConfig().withLineConfig(lineConfig)))

      val phoneStatusUpdate: PhoneStatusUpdate = createPhoneStatusMessageForMe("3", "RINGING")
      val expPhoneStatus: ToBrowserMessage =
        WebSocketEvent.createEvent(PhoneHintStatus.valueOf("RINGING"))

      ref ! phoneStatusUpdate

      ctiRouter.expectMsg(expPhoneStatus)
    }

    "send  sheet message on received" in new Helper {
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2004"))
      val (ref, ctiFilter) = actor(Some(user))

      val sheet    = new Sheet()
      val expSheet: ToBrowserMessage = WebSocketEvent.createEvent(sheet)

      ref ! sheet

      ctiRouter.expectMsgAllOf(expSheet)
    }

    """upon reception of lineconfig:
       - unsubscribe from phoneEvents with old line config phone number
       - update user phoneid
       - subscribe to LineEvents
       - subscribe to phoneEvents
       - subscribe to phoneEventHints
       - send lineconfig to the router""" in new Helper {
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("6005"))
      val (ref, ctiFilter) = actor(Some(user))
      val lineConfig: LineConfig = LineConfig("23", user.phoneNumber.get)
      ctiFilter.filterConfig =
        ctiFilter.filterConfig.withLineConfig(LineConfig("35", "3002"))

      ref ! lineConfig

      verify(eventBus).unsubscribe(ref, XucEventBus.phoneEventTopic("3002"))
      verify(eventBus).unsubscribe(ref, XucEventBus.lineEventTopic("3002"))
      verify(eventBus).unsubscribe(ref, XucEventBus.phoneHintEventTopic("3002"))
      verify(eventBus).subscribe(ref, XucEventBus.lineEventTopic("6005"))
      verify(eventBus).subscribe(ref, XucEventBus.phoneEventTopic("6005"))
      verify(eventBus).subscribe(ref, XucEventBus.phoneHintEventTopic("6005"))
      ctiFilter.filterConfig.lineConfig should be(Some(lineConfig))
      ctiRouter.expectMsg(lineConfig)
    }

    "send queue statistic message on receive statistic message with one stat from stat event bus" in new Helper {

      import xivo.models.XivoObject.ObjectType._

      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2004"))
      val (ref, ctiFilter) = actor(Some(user))

      val queueId = 5
      val objDef: ObjectDefinition = ObjectDefinition(Queue, Some(queueId))
      val aggregatedStatEvent: AggregatedStatEvent =
        AggregatedStatEvent(objDef, List[Stat](Stat("totalnumber", 32)))

      ref ! aggregatedStatEvent

      ctiRouter.expectMsg(
        Json.parse(
          "{\"msgType\": \"QueueStatistics\",\"ctiMessage\":{\"queueId\":" + queueId + ",\"counters\":[{\"statName\":\"totalnumber\",\"value\":32}]}}"
        )
      )
    }

    "send queue statistic message on receive statistic message with more stats from stat event bus" in new Helper {

      import xivo.models.XivoObject.ObjectType._

      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2004"))
      val (ref, ctiFilter) = actor(Some(user))

      val queueId = 4
      val objDef: ObjectDefinition = ObjectDefinition(Queue, Some(queueId))
      val aggregatedStatEvent: AggregatedStatEvent = AggregatedStatEvent(
        objDef,
        List[Stat](Stat("totalnumber", 32), Stat("totaltime", 30))
      )

      ref ! aggregatedStatEvent

      ctiRouter.expectMsg(
        Json.parse(
          "{\"msgType\": \"QueueStatistics\",\"ctiMessage\":{\"queueId\":" + queueId + ",\"counters\":" +
            "[{\"statName\":\"totalnumber\",\"value\":32}, {\"statName\":\"totaltime\",\"value\":30}]}}"
        )
      )
    }

    "On agent login request without id, send back a login request with user agent id" in new Helper {
      val agtId    = 98
      val phoneNb  = "3018"
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2004"))
      val (ref, _) = actor(Some(user), Some(FilterConfig().withAgentId(agtId)))

      val lineConfig: LineConfig = LineConfig("1", user.phoneNumber.get, None)

      when(configRepository.getLineConfig(LineConfigQueryByNb(phoneNb)))
        .thenReturn(Some(lineConfig))

      ref ! AgentLoginRequest(None, Some(phoneNb))

      ctiRouter.expectMsg(
        BaseRequest(ref, AgentLoginRequest(Some(agtId), Some(phoneNb)))
      )
    }
    "on agent login request without id send an error if user is not an agent" in new Helper {
      val phoneNb  = "2030"
      val user: XucUser = getXucUser("usernotagent", "pwd", Some("2004"))
      val (ref, _) = actor(Some(user))

      ref ! AgentLoginRequest(None, Some(phoneNb))

      ctiRouter.expectMsg(
        WebSocketEvent.createError(WSMsgType.AgentError, "NotAnAgent")
      )

    }

    "on agent login request check that line used is not owned by a UA position" in new Helper {
      val phoneNb = "2030"
      val agtId   = 345
      val user: XucUser =
        getXucUser("testAgentUaPosition", "pwd", Some(phoneNb))
      val lineConfig: LineConfig = LineConfig(
        "23",
        user.phoneNumber.get,
        Some(
          Line(
            1,
            "default",
            "sip",
            "device",
            None,
            None,
            "1.1.1.1",
            webRTC = true,
            Some(phoneNb),
            None,
            ua = true,
            CallerId("user", phoneNb),
            SipDriver.SIP
          )
        )
      )

      when(configRepository.getLineConfig(LineConfigQueryByNb(phoneNb)))
        .thenReturn(Some(lineConfig))

      val (ref, _) = actor(Some(user), Some(FilterConfig().withAgentId(agtId)))

      ref ! AgentLoginRequest(None, Some(phoneNb))

      ctiRouter.expectMsg(
        WebSocketEvent.createError(WSMsgType.AgentError, "NotUaPosition")
      )
    }

    "On agent logout request without id, send back a logout request with user agent id" in new Helper {
      val agtId    = 678
      val user: XucUser = getXucUser("testLogoutNoAgentId", "pwd", Some("2004"))
      val (ref, _) = actor(Some(user), Some(FilterConfig().withAgentId(agtId)))

      ref ! AgentLogoutRequest(None)

      ctiRouter.expectMsg(BaseRequest(ref, AgentLogoutRequest(Some(agtId))))

    }
    "on agent logout request without id send an error if user is not an agent" in new Helper {
      val phoneNb  = "2030"
      val user: XucUser = getXucUser("usernotagent", "pwd", Some("2004"))
      val (ref, _) = actor(Some(user))

      ref ! AgentLogoutRequest(None)

      ctiRouter.expectMsg(
        WebSocketEvent.createError(WSMsgType.AgentError, "NotAnAgent")
      )

    }
    "On agent pause request without id, send back a pause request with user agent id while propagating the reason" in new Helper {
      val agtId    = 678
      val user: XucUser = getXucUser("testPauseNoAgentId", "pwd", Some("2004"))
      val (ref, _) = actor(Some(user), Some(FilterConfig().withAgentId(agtId)))

      ref ! AgentPauseRequest(None, Some("toBePropagated"))

      ctiRouter.expectMsg(
        BaseRequest(ref, AgentPauseRequest(Some(agtId), Some("toBePropagated")))
      )

    }
    "on agent pause request without id send an error if user is not an agent" in new Helper {
      val phoneNb  = "2030"
      val user: XucUser = getXucUser("usernotagent", "pwd", Some("2004"))
      val (ref, _) = actor(Some(user))

      ref ! AgentPauseRequest(None)

      ctiRouter.expectMsg(
        WebSocketEvent.createError(WSMsgType.AgentError, "NotAnAgent")
      )

    }

    "On agent unpause request without id, send back a unpause request with user agent id" in new Helper {
      val agtId    = 345
      val user: XucUser = getXucUser("testPauseNoAgentId", "pwd", Some("2004"))
      val (ref, _) = actor(Some(user), Some(FilterConfig().withAgentId(agtId)))

      ref ! AgentUnPauseRequest(None)

      ctiRouter.expectMsg(BaseRequest(ref, AgentUnPauseRequest(Some(agtId))))

    }

    "on agent unpause request without id send an error if user is not an agent" in new Helper {
      val phoneNb  = "2030"
      val user: XucUser = getXucUser("usernotagent", "pwd", Some("2004"))
      val (ref, _) = actor(Some(user))

      ref ! AgentUnPauseRequest(None)

      ctiRouter.expectMsg(
        WebSocketEvent.createError(WSMsgType.AgentError, "NotAnAgent")
      )

    }

    "on user status update request without user id, send back to router with proper user id" in new Helper {
      val userId   = 789
      val user: XucUser = getXucUser("testUserStatusUpdateNoUserId", "pwd", Some("2004"))
      val (ref, _) = actor(Some(user), Some(FilterConfig().withUserId(userId)))

      ref ! UserStatusUpdateReq(None, "outtolunch")

      ctiRouter.expectMsg(
        BaseRequest(ref, UserStatusUpdateReq(Some(userId), "outtolunch"))
      )

    }
  }
  "A ctifilter upon reception of agent state" should {
    trait AgentStateFactory {
      val loggedOnPhoneNb = "2350"
      val agentId         = 18
      val agentNumber     = "2000"
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some(loggedOnPhoneNb))

      case class AnyAgentState(
          dummy: String,
          override val phoneNb: String = loggedOnPhoneNb
      ) extends AgentState(
            "anyAgentState",
            agentId,
            new DateTime,
            phoneNb,
            List(),
            None,
            agentNumber
          ) {
        def withCause(newCause: String): AnyAgentState = this

        def withPhoneNb(newPhoneNb: String): AgentState = this
      }

      def agState(name: String, phoneNb: String = loggedOnPhoneNb): (AnyAgentState, JsValue) = {
        val agS = AnyAgentState("test", phoneNb)
        (agS, WebSocketEvent.createEvent(agS))
      }

      val (agentState, jsonAgentState) = agState("test")

    }

    "publish event to router request line config" in new Helper
      with AgentStateFactory {
      val (ref, ctiFilter) = actor(Some(user))

      ref ! AgentLoggingIn(agentId, loggedOnPhoneNb)

      ctiRouter.expectMsg(
        RequestConfig(ref, LineConfigQueryByNb(loggedOnPhoneNb))
      )
    }

    "publish event to router and request line config if agent logged on a different number" in new Helper
      with AgentStateFactory {

      val lineConfig: LineConfig = LineConfig("321", loggedOnPhoneNb)
      val (ref, ctiFilter) =
        actor(Some(user), Some(FilterConfig().withLineConfig(lineConfig)))

      val (ags, jsonAgs) = agState("test", "5600")

      ref ! ags

      ctiRouter.expectMsgAllOf(
        RequestConfig(ref, LineConfigQueryByNb("5600")),
        jsonAgs
      )

    }

    "publish event to router and request line config when no line config " in new Helper
      with AgentStateFactory {
      val lineConfig: LineConfig = LineConfig("321", loggedOnPhoneNb)
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))

      ref ! agentState

      ctiRouter.expectMsgAllOf(
        RequestConfig(ref, LineConfigQueryByNb(loggedOnPhoneNb)),
        jsonAgentState
      )

    }

    " publish event to router and NOT request line config if agent logged on same phone number " in new Helper
      with AgentStateFactory {
      val lineConfig: LineConfig = LineConfig("321", loggedOnPhoneNb)
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))

      ref ! agentState

      ctiRouter.expectMsg(jsonAgentState)

    }
    """ if phone number requested is not the one the agent is logged on
        send error
        and publish event to router
    """ in new Helper {
      def loggedInOnAnotherPhoneMsg(phoneNb: String): ToBrowserMessage =
        Json.parse(
          "{\"msgType\": \"AgentError\",\"ctiMessage\":{\"Error\":\"LoggedInOnAnotherPhone\",\"phoneNb\":\"" + phoneNb + "\",\"RequestedNb\":\"2350\"}}"
        )

      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2350"))
      val lineConfig: LineConfig = LineConfig("321", "7777")
      val (ref, ctiFilter) = actor(
        Some(user),
        Some(FilterConfig().withLineConfig(lineConfig).withAgentId(123))
      )

      val agentState: AgentReady =
        AgentReady(18, new DateTime(), "7777", List(32, 45, 54), None, "2000")

      val jsonAgentState: ToBrowserMessage = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsg(jsonAgentState)

      ref ! AgentLoginRequest(None, Some("2350"), None)
      ctiRouter.expectMsg(loggedInOnAnotherPhoneMsg("7777"))
      ctiRouter.expectMsg(jsonAgentState)
    }

  }
  """ A CtiFilter upon reception off agent logout should
      release all pending callbacks
  """ in new Helper {
    val agentId    = 52
    val lineConfig: LineConfig = LineConfig("321", "7777")
    val (ref, ctiFilter) = actor(
      Some(getXucUser("testFilterUser", "pwd", Some("2350"))),
      Some(FilterConfig().withLineConfig(lineConfig))
    )

    val agentState: AgentLoggingOut = AgentLoggingOut(agentId)

    ref ! agentState

    ctiRouter.fishForMessage() {
      case BaseRequest(ref, ReleaseAllCallbacks(`agentId`)) => true
      case _                                                => false
    }

  }
  "A CtiFilter upon reception on agent state" should {
    import xivo.events.AgentState.AgentLoggedOut
    """ publish event to router
        request line config
    """ in new Helper {
      val agentId          = 18
      val loggedOnPhoneNb  = "2350"
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some(loggedOnPhoneNb))
      val (ref, ctiFilter) = actor(Some(user))
      val agentState: AgentOnWrapup = AgentOnWrapup(
        agentId,
        new DateTime(),
        loggedOnPhoneNb,
        List(),
        Some(""),
        "2000"
      )

      val exp: ToBrowserMessage = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsgAllOf(
        RequestConfig(ref, LineConfigQueryByNb(loggedOnPhoneNb)),
        exp
      )

    }
    """ publish event to router
        and NOT request line config if already done
    """ in new Helper {
      val agentId          = 18
      val loggedOnPhoneNb  = "2440"
      val user: XucUser = getXucUser("testAgentState", "pwd", Some(loggedOnPhoneNb))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))

      val agentState: AgentOnWrapup = AgentOnWrapup(
        agentId,
        new DateTime(),
        loggedOnPhoneNb,
        List(),
        Some(""),
        "2000"
      )

      val exp: ToBrowserMessage = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsgAllOf(exp)
    }
    "publish event to router and do not request line config if event is agent loggedout" in new Helper {
      val agentId          = 18
      val loggedOnPhoneNb  = "2440"
      val user: XucUser = getXucUser("testAgentState", "pwd", Some(loggedOnPhoneNb))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))

      val agentState: AgentLoggedOut =
        AgentLoggedOut(agentId, new DateTime(), loggedOnPhoneNb, List(), "")

      val exp: ToBrowserMessage = WebSocketEvent.createEvent(agentState)

      ref ! agentState
      ref ! AgentLoggingOut(agentId)

      ctiRouter.expectMsgAllOf(
        exp,
        BaseRequest(ref, ReleaseAllCallbacks(agentId))
      )
      ctiRouter.expectNoMessage(expectMsgTimeout)
    }

    """ if phone number requested is not the one the agent is logged on
        do not send error if agentStatereceived is loggedOut
        and publish event to router
    """ in new Helper {
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2350"))
      val (ref, ctiFilter) = actor(Some(user))

      val agentState: AgentLoggedOut =
        AgentLoggedOut(18, new DateTime(), "4460", List(), "4460")

      val jsonAgentState: ToBrowserMessage = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsg(jsonAgentState)
    }
    """ on agent ready event
        - request line config
        - publish AgentReady to the router""" in new Helper {
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("4350"))
      val (ref, ctiFilter) = actor(Some(user))
      val agentReady: AgentReady = AgentReady(
        32,
        new DateTime(),
        "4350",
        List(32, 45, 54),
        Some(""),
        "2000"
      )
      val jsonAgentReady: ToBrowserMessage = WebSocketEvent.createEvent(agentReady)

      ref ! agentReady

      ctiRouter.expectMsgAllOf(
        RequestConfig(ref, LineConfigQueryByNb("4350")),
        jsonAgentReady
      )
    }

  }
  "A CtiFilter on agent listen" should {
    "on started publish event to router" in new Helper() {
      val (ref, ctiFilter) =
        actor(Some(getXucUser("testlistenstarted", "pwd", Some("4410"))))

      val agls: AgentListenStarted = AgentListenStarted("4410", Some(54))
      val jsonEvent: ToBrowserMessage = WebSocketEvent.createEvent(agls)

      ref ! agls

      ctiRouter.expectMsg(jsonEvent)
    }

    "on stopped publish event to router" in new Helper() {
      val (ref, ctiFilter) =
        actor(Some(getXucUser("testlistenstopped", "pwd", Some("1540"))))

      val listenStopped: AgentListenStopped = AgentListenStopped("1540", Some(25))
      val jsonEvent: ToBrowserMessage = WebSocketEvent.createEvent(listenStopped)

      ref ! listenStopped

      ctiRouter.expectMsg(jsonEvent)

    }

    """on request, update with my agentid and forward to agent action""" in new Helper {
      val (ref, ctiFilter) =
        actor(Some(getXucUser("testagentlisten", "pwd", Some("3350"))))
      ctiFilter.filterConfig = FilterConfig() withUserId 78

      ref ! AgentListen(56)

      ctiRouter.expectMsg(AgentListen(56, Some(78)))

    }

    """do nothing if not an agent""" in new Helper {
      val (ref, ctiFilter) =
        actor(Some(getXucUser("testagentlisten", "pwd", Some("3350"))))

      ref ! AgentListen(56)

      ctiRouter.expectMsg(
        InvalidRequest(
          "Cannot listen valid userid not found",
          s"${AgentListen(56)}"
        )
      )

    }

    """ if the phone number in the agent state is empty
      do not send error
      and publish event to router
    """ in new Helper {
      val user: XucUser = getXucUser("testFilterUser", "pwd", Some("2350"))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))

      val agentState: AgentDialing =
        AgentDialing(18, new DateTime(), "", List(32, 45, 54), Some(""), "2000")

      val jsonAgentState: ToBrowserMessage = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsg(jsonAgentState)
    }
  }
  "A cti filter on phone events" should {
    "publish the phone event" in new Helper {
      val user: XucUser = getXucUser("testPhoneEventUser", "pwd", Some("2350"))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))

      val phoneEvent: PhoneEvent = PhoneEvent(
        PhoneEventType.EventRinging,
        "1500",
        "0664486754",
        "John Doe",
        "1230045.6",
        "1230045.8"
      )

      val jsonPhoneEvent: ToBrowserMessage = WebSocketEvent.createEvent(phoneEvent)

      ref ! phoneEvent

      ctiRouter.expectMsg(jsonPhoneEvent)
    }
    "publish the current calls phone events message" in new Helper {
      val user: XucUser = getXucUser("testPhoneEventUser", "pwd", Some("2350"))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))

      val ccpe: CurrentCallsPhoneEvents = CurrentCallsPhoneEvents(
        "1500",
        List(
          PhoneEvent(
            PhoneEventType.EventRinging,
            "1500",
            "0664486754",
            "John Doe",
            "1230045.6",
            "1230045.8"
          )
        )
      )

      val jsonCcpe: ToBrowserMessage = WebSocketEvent.createEvent(ccpe)

      ref ! ccpe

      ctiRouter.expectMsg(jsonCcpe)
    }

  }

  "CtiFilter creates CallHistoryEnricher actor when userCallHistory is received with size" in new Helper {
    val user: XucUser =
      getXucUser("testCallHistoryEnricher", "pwd", Some("2350"))
    val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))
    val requestSender: TestProbe = TestProbe()

    ref ! BaseRequest(requestSender.ref, GetUserCallHistory(HistorySize(5)))
    verify(callHistoryEnricherFactory, timeout(500)).apply(
      requestSender.ref,
      personalContactRepository.ref,
      HistorySize(5),
      "testCallHistoryEnricher"
    )
  }

  "CtiFilter creates CallHistoryEnricher actor when userCallHistory is received with days" in new Helper {
    val user: XucUser =
      getXucUser("testCallHistoryEnricher", "pwd", Some("2350"))
    val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))
    val requestSender: TestProbe = TestProbe()

    ref ! BaseRequest(
      requestSender.ref,
      GetUserCallHistoryByDays(HistoryDays(5))
    )
    verify(callHistoryEnricherFactory, timeout(500)).apply(
      requestSender.ref,
      personalContactRepository.ref,
      HistoryDays(5),
      "testCallHistoryEnricher"
    )
  }

  "CtiFilter send a UserPreferenceInit on User login" in new Helper {
    val user: XucUser =
      getXucUser("testCallHistoryEnricher", "pwd", Some("2350"))
    val (ref, ctiFilter)         = actor(Some(user), Some(FilterConfig()))
    val requestSender: TestProbe = TestProbe()

    val userStatuses: List[CtiStatus] = List(
      CtiStatus(Option("test"), Option("test"), Option(1)),
      CtiStatus(Option("toto"), Option("toto"), Option(0))
    )
    val userStatuses2: List[CtiStatusLegacy] = List(
      CtiStatusLegacy(
        Option("test"),
        Option("test"),
        Option("red"),
        List(CtiStatusActionLegacy("queueunpause_all", Option("")))
      ),
      CtiStatusLegacy(
        Option("toto"),
        Option("toto"),
        Option("red"),
        List(CtiStatusActionLegacy("queuepause_all", Option("")))
      )
    )

    when(configRepository.getCtiStatusesLegacy(1))
      .thenReturn(Some(userStatuses2))
    when(configRepository.getCtiStatuses(1)).thenReturn(Some(userStatuses))

    ref.tell(
      LoggedOn(
        user,
        "1"
      ),
      requestSender.ref
    )

    userPreferenceService.expectMsg(
      UserPreferencesInit(Some(1), requestSender.ref)
    )
  }

  "CtiFilter forward ClientConnected event to userPreferenceService" in new Helper {
    val user: XucUser =
      getXucUser("testCallHistoryEnricher", "pwd", Some("2350"))
    val phoneId                   = 623
    val userId                    = 5
    val (ref, _)                  = actor(Some(user), Some(FilterConfig()))
    val connectedActor: TestProbe = TestProbe()
    val userStatuses: List[CtiStatus] = List(
      CtiStatus(Option("test"), Option("test"), Option(1)),
      CtiStatus(Option("toto"), Option("toto"), Option(0))
    )
    val userStatuses2: List[CtiStatusLegacy] = List(
      CtiStatusLegacy(
        Option("test"),
        Option("test"),
        Option("red"),
        List(CtiStatusActionLegacy("queueunpause_all", Option("")))
      ),
      CtiStatusLegacy(
        Option("toto"),
        Option("toto"),
        Option("red"),
        List(CtiStatusActionLegacy("queuepause_all", Option("")))
      )
    )

    when(configRepository.getCtiStatusesLegacy(1))
      .thenReturn(Some(userStatuses2))
    when(configRepository.getCtiStatuses(1)).thenReturn(Some(userStatuses))

    ref ! ClientConnected(
      connectedActor.ref,
      LoggedOn(user, userId.toString)
    )

    ref ! LoggedOn(
      user,
      "1"
    )

    userPreferenceService.expectMsg(
      ClientConnected(
        connectedActor.ref,
        LoggedOn(user, userId.toString)
      )
    )
  }

  "CtiFilter forward PhoneHintStatusEvent to CtiRouter if event is for him" in new Helper {
    val user: XucUser = getXucUser("jbond", "pwd", Some("2004"))
    val lineConfig: LineConfig = LineConfig("3", "2350")
    val (ref, ctiFilter) =
      actor(Some(user), Some(FilterConfig()))

    val phoneHintStatus: PhoneHintStatus =
      PhoneHintStatus.valueOf("RINGING")
    val phoneHintStatusEvent: PhoneHintStatusEvent = PhoneHintStatusEvent("2350", phoneHintStatus)
    val expPhoneHintStatus: ToBrowserMessage =
      WebSocketEvent.createEvent(phoneHintStatus)

    ref ! lineConfig
    ref ! phoneHintStatusEvent

    ctiRouter.expectMsgAllOf(lineConfig, expPhoneHintStatus)
  }

  "CtiFilter not forward PhoneHintStatusEvent to CtiRouter if event is not him" in new Helper {
    val user: XucUser          = getXucUser("jbond", "pwd", Some("2004"))
    val lineConfig: LineConfig = LineConfig("3", "2350")
    val (ref, _) =
      actor(Some(user), Some(FilterConfig()))

    val phoneHintStatus: PhoneHintStatus =
      PhoneHintStatus.valueOf("RINGING")
    val phoneHintStatusEvent: PhoneHintStatusEvent =
      PhoneHintStatusEvent("9999", phoneHintStatus)

    ref ! lineConfig
    ref ! phoneHintStatusEvent

    ctiRouter.expectMsg(lineConfig)
    ctiRouter.expectNoMessage()
  }
}
