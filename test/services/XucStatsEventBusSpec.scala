package services

import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import org.scalatest.BeforeAndAfter
import org.scalatestplus.mockito.MockitoSugar
import services.XucStatsEventBus.{AggregatedStatEvent, Stat}
import stats.Statistic.RequestStat
import xivo.models.XivoObject.ObjectDefinition

class XucStatsEventBusSpec
    extends TestKitSpec("XucStatsEventBusSpec")
    with BeforeAndAfter
    with MockitoSugar {

  import xivo.models.XivoObject.ObjectType._

  var statsBus: XucStatsEventBus = _
  var actor: TestProbe           = null

  before {
    statsBus = new XucStatsEventBus
    actor = TestProbe()
  }

  "XucStatsEventBus" should {

    "send stat to the subscriber on object type" in {

      val mockEvent =
        AggregatedStatEvent(ObjectDefinition(Queue, Some(1)), List[Stat]())
      statsBus.subscribe(actor.ref, ObjectDefinition(Queue))

      statsBus.publish(mockEvent)

      actor.expectMsg(mockEvent)
    }

    "send stat to the subscriber on object ref" in {
      val mockEvent =
        AggregatedStatEvent(ObjectDefinition(Queue, Some(2)), List[Stat]())
      statsBus.subscribe(actor.ref, ObjectDefinition(Queue, Some(2)))

      statsBus.publish(mockEvent)

      actor.expectMsg(mockEvent)

    }
    "not send stat to the subscriber on other object ref" in {
      val mockEvent =
        AggregatedStatEvent(ObjectDefinition(Queue, Some(3)), List[Stat]())
      statsBus.subscribe(actor.ref, ObjectDefinition(Queue, Some(99)))

      statsBus.publish(mockEvent)

      actor.expectNoMessage(expectMsgTimeout)

    }

    "send stat to object type only subscriber" in {
      val mockEvent =
        AggregatedStatEvent(ObjectDefinition(Queue, Some(4)), List[Stat]())

      statsBus.subscribe(actor.ref, ObjectDefinition(Queue))

      statsBus.publish(mockEvent)
      actor.expectMsg(mockEvent)
    }

    "send subscription to bus manager" in {
      val busManager = TestProbe()
      val subscriber = TestProbe()
      val objdef     = ObjectDefinition(Queue, Some(5))

      statsBus.setStatCache(busManager.ref)

      statsBus.subscribe(subscriber.ref, objdef)

      busManager.expectMsg(RequestStat(subscriber.ref, objdef))
    }
  }
}
