package services

import org.apache.pekko.testkit.{TestFSMRef, TestProbe}
import pekkotest.TestKitSpec
import org.joda.time.{DateTime, Seconds}
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.*
import org.scalatestplus.mockito.MockitoSugar
import services.AgentStateFSM.*
import services.agent.{AgentCall, AgentCallState, AgentStateTHandler}
import services.calltracking.{DeviceCall, DeviceCalls}
import services.config.ConfigRepository
import xivo.events.{CallDirection, *}
import xivo.xucami.models.{CallerId, Channel, ChannelDirection, ChannelState}
import org.mockito.ArgumentMatchers.{eq => mockitoEq}

class AgentStateFSMSpec
    extends TestKitSpec("AgentStateFSMSpec")
    with MockitoSugar {

  val eventBus: XucEventBus         = mock[XucEventBus]
  val configRepository: ConfigRepository = mock[ConfigRepository]
  val onLogin: MContext => Unit          = mock[MContext => Unit]
  val onLogout: MContext => Unit         = mock[MContext => Unit]
  val publish: (MAgentStates, MContext) => Unit          = mock[(MAgentStates, MContext) => Unit]
  val publishTransitionMock: (MAgentStates, MContext, MAgentStates, MContext) => Unit =
    mock[(MAgentStates, MContext, MAgentStates, MContext) => Unit]

  trait TestTransitionHandler extends AgentStateTHandler {
    this: AgentStateFSM =>

    def whenLoggingIn(context: MContext): Unit  = onLogin(context)
    def whenLoggingOut(context: MContext): Unit = onLogout(context)
    def publishState(state: MAgentStates, context: MContext): Unit =
      publish(state, context)
    def publishTransition(
        from: MAgentStates,
        fromContext: MContext,
        to: MAgentStates,
        toContext: MContext
    ): Unit =
      publishTransitionMock(from, fromContext, to, toContext)
  }

  def createFSM(
      initialState: Option[MAgentStates] = None,
      stateData: Option[MContext] = None
  ): TestFSMRef[MAgentStates,MContext,AgentStateFSM with TestTransitionHandler] = {

    val agentNumber   = "2200"
    val deviceTracker = TestProbe()
    val agentStatusFSM = TestFSMRef(
      new AgentStateFSM(
        1L,
        agentNumber,
        eventBus,
        configRepository,
        deviceTracker.ref
      ) with TestTransitionHandler
    )
    initialState.foreach(agentStatusFSM.setState(_))
    stateData.foreach(d => agentStatusFSM.setState(stateData = d))
    reset(onLogin)
    reset(onLogout)
    reset(publish)
    reset(publishTransitionMock)
    agentStatusFSM
  }

  "AgentStateFSM" should {

    "initialize with MAgentLoggedOut state" in {
      val fsm = createFSM()
      fsm.stateName should be(MAgentLoggedOut)
    }

    "become MAgentReady with an AgentStateContext context when receiving EventAgentLogin in MAgentLoggedOut state" in {
      val fsm = createFSM()
      fsm ! EventAgentLogin(1L, "1000", "2000")
      fsm.stateName should be(MAgentReady)
      fsm.stateData shouldBe an[AgentStateContext]
    }

    "become MAgentPaused with an AgentStateContext context when receiving EventAgentPause in MAgentReady state" in {
      val fsm =
        createFSM(Some(MAgentReady), Some(AgentStateContext(None, None)))
      fsm ! EventAgentPause(1L)
      fsm.stateName should be(MAgentPaused)
      fsm.stateData shouldBe an[AgentStateContext]
    }

    "become MAgentPaused when receiving EventAgentPause in MAgentReady state with Reason" in {
      val fsm =
        createFSM(Some(MAgentReady), Some(AgentStateContext(None, None)))
      fsm ! EventAgentPause(1L, Some("test reason"))
      fsm.stateName should be(MAgentPaused)
      fsm.stateData match {
        case a: AgentStateContext =>
          a.pauseReason shouldBe Some("test reason")
        case _ => fail("Wrong FSM context")
      }
    }

    "stay MAgentPaused when receiving EventAgentPause with reason when in MAgentPaused state with no Reason" in {
      val fsm =
        createFSM(Some(MAgentPaused), Some(AgentStateContext(None, None)))
      fsm ! EventAgentPause(1L, Some("test reason"))
      fsm.stateName should be(MAgentPaused)
      fsm.stateData match {
        case a: AgentStateContext =>
          a.pauseReason shouldBe Some("test reason")
        case _ => fail("Wrong FSM context")
      }
    }

    "stay MAgentPaused when receiving EventAgentPause with new reason when in MAgentPaused state with Reason" in {
      val fsm = createFSM(
        Some(MAgentPaused),
        Some(AgentStateContext(Some("Old Reason"), None))
      )
      fsm ! EventAgentPause(1L, Some("New reason"))
      fsm.stateName should be(MAgentPaused)
      fsm.stateData match {
        case a: AgentStateContext =>
          a.pauseReason shouldBe Some("New reason")
        case _ => fail("Wrong FSM context")
      }
    }

    "become MAgentPaused with an AgentStateContext context when receiving EventAgentPause in MAgentOnWrapup state" in {
      val fsm =
        createFSM(Some(MAgentOnWrapup), Some(AgentStateContext(None, None)))
      fsm ! EventAgentPause(1L)
      fsm.stateName should be(MAgentPaused)
      fsm.stateData shouldBe an[AgentStateContext]
    }

    "become MAgentReady with an AgentStateContext context when receiving EventAgentUnPause in MAgentPaused state" in {
      val fsm =
        createFSM(Some(MAgentPaused), Some(AgentStateContext(None, None)))
      fsm ! EventAgentUnPause(1L)
      fsm.stateName should be(MAgentReady)
      fsm.stateData shouldBe an[AgentStateContext]
    }

    "become MAgentReady with an AgentStateContext context when receiving EventAgentUnPause in MAgentOnWrapup state" in {
      val fsm =
        createFSM(Some(MAgentOnWrapup), Some(AgentStateContext(None, None)))
      fsm ! EventAgentUnPause(1L)
      fsm.stateName should be(MAgentReady)
      fsm.stateData shouldBe an[AgentStateContext]
    }

    "become MAgentOnWrapup with an AgentStateContext context when receiving EventAgentWrapup in MAgentReady state" in {
      val fsm =
        createFSM(Some(MAgentReady), Some(AgentStateContext(None, None)))
      fsm ! EventAgentWrapup(1L)
      fsm.stateName should be(MAgentOnWrapup)
      fsm.stateData shouldBe an[AgentStateContext]
    }

    "become MAgentOnWrapup with an AgentStateContext context when receiving EventAgentWrapup in MAgentPaused state" in {
      val fsm =
        createFSM(Some(MAgentPaused), Some(AgentStateContext(None, None)))
      fsm ! EventAgentWrapup(1L)
      fsm.stateName should be(MAgentOnWrapup)
      fsm.stateData shouldBe an[AgentStateContext]
    }

    "become MAgentLoggedOut with an AgentStateContext context when receiving EventAgentLogout in any state" in {
      val fromStates = List(MAgentReady, MAgentPaused, MAgentOnWrapup)
      fromStates.foreach(state => {
        val fsm = createFSM(Some(state), Some(AgentStateContext(None, None)))
        fsm ! EventAgentLogout(1L, "1000")
        fsm.stateName should be(MAgentLoggedOut)
        fsm.stateData shouldBe an[AgentStateContext]
      })
    }

    "become MAgentLoggedOut with an AgentStateContext context when receiving EventAgentLogout in any state " +
      "even if initial context is Empty context" in {
      val fromStates = List(MAgentReady, MAgentPaused, MAgentOnWrapup)
      fromStates.foreach(state => {
        val fsm =
          createFSM(Some(state), Some(AgentStateEmptyContext(DateTime.now())))
        fsm ! EventAgentLogout(1L, "1000")
        fsm.stateName should be(MAgentLoggedOut)
        fsm.stateData shouldBe an[AgentStateContext]
      })
    }

  }

  "AgentStateFSM with transitionHandler" should {
    "publish state on transition" in {
      val fsm =
        createFSM(Some(MAgentReady), Some(AgentStateContext(None, None)))
      fsm ! EventAgentPause(1L)
      verify(publish).apply(mockitoEq(MAgentPaused), any[MContext])
    }

    "publish state with correct phoneNb and correct timestamp when moving to MAgentLoggedOut state" in {
      val phoneNb = "1002"
      val previousContext = AgentStateContext(
        None,
        Some(phoneNb),
        lastChanged = DateTime.now().minusHours(1)
      )
      val fsm = createFSM(Some(MAgentReady), Some(previousContext))
      fsm ! EventAgentLogout(1L, "2000")

      val captor = ArgumentCaptor.forClass(classOf[AgentStateContext])

      verify(publish).apply(mockitoEq(MAgentLoggedOut), captor.capture())

      captor.getValue.phoneNumber should be(Some(phoneNb))
      Seconds
        .secondsBetween(captor.getValue.lastChanged, new DateTime())
        .getSeconds should be < 1
    }

    "publish transition when changing state" in {
      val previousContext = AgentStateContext(None, None)
      val fsm             = createFSM(Some(MAgentReady), Some(previousContext))
      fsm ! EventAgentPause(1L)

      verify(publishTransitionMock).apply(
        mockitoEq(MAgentReady),
        mockitoEq(previousContext),
        mockitoEq(MAgentPaused),
        any[AgentStateContext]
      )
    }

    "publish transition when changing state to MAgentLoggedout" in {
      val previousContext = AgentStateContext(None, None)
      val fsm             = createFSM(Some(MAgentReady), Some(previousContext))
      fsm ! EventAgentLogout(1L, "2000")

      verify(publishTransitionMock).apply(
        mockitoEq(MAgentReady),
        mockitoEq(previousContext),
        mockitoEq(MAgentLoggedOut),
        any[AgentStateEmptyContext]
      )
    }

    "trigger whenLoggingIn handler when logging in" in {
      val fsm =
        createFSM(Some(MAgentLoggedOut), Some(AgentStateContext(None, None)))
      fsm ! EventAgentLogin(1L, "1000", "2000")

      val captor = ArgumentCaptor.forClass(classOf[AgentStateContext])

      verify(onLogin).apply(captor.capture())

      captor.getValue.phoneNumber should be(Some("1000"))
    }

    "trigger whenLoggingOut handler when logging out" in {
      val fsm = createFSM(
        Some(MAgentReady),
        Some(AgentStateContext(Some("out_to_lunch"), Some("1000")))
      )
      fsm ! EventAgentLogout(1L, "2000")

      val captor = ArgumentCaptor.forClass(classOf[AgentStateContext])

      verify(onLogout).apply(captor.capture())

      captor.getValue.pauseReason should be(Some("out_to_lunch"))
      captor.getValue.phoneNumber should be(Some("1000"))
    }
  }

  "AgentStateFSM context message handler" should {

    "keep phoneNb in context when moving to AgentLoggedOut state but with updated timestamp" in {
      val phoneNb = "1002"
      val initialContext = AgentStateContext(
        Some("out to lunch"),
        Some(phoneNb),
        None,
        queues = List(1, 3),
        lastChanged = DateTime.now().minusHours(1)
      )

      val fromStates = List(MAgentReady, MAgentPaused, MAgentOnWrapup)
      fromStates.foreach(state => {
        val fsm = createFSM(Some(state), Some(initialContext))
        fsm ! EventAgentLogout(1L, "2000")

        fsm.stateName should be(MAgentLoggedOut)
        fsm.stateData shouldBe an[AgentStateContext]
        fsm.stateData should have(
          Symbol("pauseReason")(None),
          Symbol("phoneNumber")(Some(phoneNb)),
          Symbol("currentCall")(None),
          Symbol("queues")(List.empty)
        )
        Seconds
          .secondsBetween(fsm.stateData.lastChanged, new DateTime())
          .getSeconds should be < 1
      })
    }

    "store calls in context without changing state" in {
      val initialContext = AgentStateContext(None, None)
      val fromStates     = List(MAgentReady, MAgentPaused, MAgentOnWrapup)
      val channel = Channel(
        "12345679.012",
        "SIP/abcd-00001",
        CallerId("1000", "Someone"),
        "12345679.123",
        ChannelState.UP,
        direction = Some(ChannelDirection.INCOMING)
      )
      val call =
        DeviceCall("SIP/abcd-00001", Some(channel), Set.empty, Map.empty)
      val calls: Map[String, DeviceCall] = Map(call.channelName -> call)
      val agentCall =
        Some(AgentCall(AgentCallState.OnCall, false, CallDirection.Incoming))

      fromStates.foreach(state => {
        val fsm = createFSM(Some(state), Some(initialContext))
        fsm ! DeviceCalls("SIP/abcd", calls)

        fsm.stateName should be(state)
        fsm.stateData should have(
          Symbol("currentCall")(agentCall)
        )
      })
    }

    "reset lastChanged property when receiving a call" in {
      val initialContext = AgentStateContext(
        None,
        None,
        lastChanged = DateTime.now().minusHours(1)
      )
      val channel = Channel(
        "12345679.012",
        "SIP/abcd-00001",
        CallerId("1000", "Someone"),
        "12345679.123",
        ChannelState.UP,
        direction = Some(ChannelDirection.INCOMING)
      )
      val call =
        DeviceCall("SIP/abcd-00001", Some(channel), Set.empty, Map.empty)
      val calls: Map[String, DeviceCall] = Map(call.channelName -> call)
      val agentCall =
        Some(AgentCall(AgentCallState.OnCall, false, CallDirection.Incoming))

      val fsm = createFSM(Some(MAgentReady), Some(initialContext))
      fsm ! DeviceCalls("SIP/abcd", calls)

      fsm.stateName should be(MAgentReady)
      fsm.stateData should have(
        Symbol("currentCall")(agentCall)
      )
      Seconds
        .secondsBetween(fsm.stateData.lastChanged, new DateTime())
        .getSeconds should be < 1

      val contextTransitionTo =
        ArgumentCaptor.forClass(classOf[AgentStateContext])
      verify(publishTransitionMock).apply(
        mockitoEq(MAgentReady),
        mockitoEq(initialContext),
        mockitoEq(MAgentReady),
        contextTransitionTo.capture()
      )
      Seconds
        .secondsBetween(
          contextTransitionTo.getValue.lastChanged,
          new DateTime()
        )
        .getSeconds should be < 1
      contextTransitionTo.getValue.currentCall should be(agentCall)

      val publishContext = ArgumentCaptor.forClass(classOf[AgentStateContext])
      verify(publish).apply(mockitoEq(MAgentReady), publishContext.capture())
      Seconds
        .secondsBetween(publishContext.getValue.lastChanged, new DateTime())
        .getSeconds should be < 1
      publishContext.getValue.currentCall should be(agentCall)
    }

    "when DeviceCall is updated and lose ACD information, we should ignore it" in {

      val remoteChannel = Channel(
        "12345679.112",
        "SIP/efgh-00001",
        CallerId("1001", "Someone Else"),
        "12345679.123",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )
        .updateVariable(UserData.QueueNameKey, "myqueue")
      val channel = Channel(
        "12345679.012",
        "SIP/abcd-00001",
        CallerId("1000", "Someone"),
        "12345679.123",
        ChannelState.UP,
        direction = Some(ChannelDirection.INCOMING)
      )
      val call = DeviceCall(
        "SIP/abcd-00001",
        Some(channel),
        Set.empty,
        Map(remoteChannel.name -> remoteChannel)
      )
      val agentCall = AgentCall.deviceCallToAgentCall(call)
      val initialContext =
        AgentStateContext(None, None, currentCall = agentCall)

      val fsm = createFSM(Some(MAgentReady), Some(initialContext))

      val callNoQueue                    = call.copy(remoteChannels = Map.empty)
      val calls: Map[String, DeviceCall] = Map(call.channelName -> callNoQueue)

      fsm ! DeviceCalls("SIP/abcd", calls)

      fsm.stateData
        .asInstanceOf[AgentStateContext]
        .currentCall
        .map(_.acd) shouldBe Some(true)
    }

    "store agent queues in context without changing state" in {
      val initialContext = AgentStateContext(None, None)
      val fromStates     = List(MAgentReady, MAgentPaused, MAgentOnWrapup)
      val xivoAgentState = AgentQueues(1L, List(1, 5, 6))
      fromStates.foreach(state => {
        val fsm = createFSM(Some(state), Some(initialContext))
        fsm ! xivoAgentState

        fsm.stateName should be(state)
        fsm.stateData should have(
          Symbol("queues")(List(1, 5, 6))
        )
      })
    }

  }

}
