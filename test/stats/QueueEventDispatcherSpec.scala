package stats

import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import org.apache.pekko.actor.ActorContext
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.QueueLog._
import xivo.models.XivoObject.ObjectDefinition

class QueueEventDispatcherSpec
    extends TestKitSpec("QueueEventDispatcherSpec")
    with MockitoSugar {

  class Helper {
    val testStatistic: TestProbe      = TestProbe()
    val queueStat: TestProbe          = TestProbe()
    val testStatRepository: QueueStatRepository = mock[QueueStatRepository]

    def actor(): (TestActorRef[QueueEventDispatcher], QueueEventDispatcher) = {
      val a = TestActorRef(new QueueEventDispatcher(testStatRepository))
      (a, a.underlyingActor)
    }
  }

  "Queue event dispatcher" should {
    "create queueStatistic actor on new name received" in new Helper {
      val (ref, _) = actor()
      val queueId  = 5
      val event: EnterQueue = EnterQueue(queueId)

      when(testStatRepository.getQueueStat(queueId)).thenReturn(None)
      when(testStatRepository.createQueueStat(any[Int], any[ActorContext]))
        .thenReturn(queueStat.ref)

      ref ! event

      queueStat.expectMsg(event)
    }
    "send enter queue event to existing queueStatistic actor" in new Helper {
      val (ref, _) = actor()
      val queueId  = 7
      val event: EnterQueue = EnterQueue(queueId)

      when(testStatRepository.getQueueStat(queueId))
        .thenReturn(Some(queueStat.ref))

      ref ! event

      queueStat.expectMsg(event)
    }
    "send abandonned queue event to  existing queueStatistic actor" in new Helper {
      val (ref, _) = actor()
      val queueId  = 11
      val event: Abandonned = Abandonned(queueId, 0)

      when(testStatRepository.getQueueStat(queueId))
        .thenReturn(Some(queueStat.ref))

      ref ! event

      queueStat.expectMsg(event)

    }
    "send complete queue event to  existing queueStatistic actor" in new Helper {
      val (ref, _) = actor()
      val queueId  = 20
      val event: Complete = Complete(queueId, "2850", 5, 12)

      when(testStatRepository.getQueueStat(queueId))
        .thenReturn(Some(queueStat.ref))

      ref ! event

      queueStat.expectMsg(event)
    }

    "send connect queue event to  existing queueStatistic actor" in new Helper {
      val (ref, _) = actor()
      val queueId  = 21
      val event: Connect = Connect(queueId, "2850", 5, "65546546546.22")

      when(testStatRepository.getQueueStat(queueId))
        .thenReturn(Some(queueStat.ref))

      ref ! event

      queueStat.expectMsg(event)
    }

    "send closed queue event to existing queueStatistic actor" in new Helper {
      val (ref, _) = actor()
      val queueId  = 15
      val event: Closed = Closed(queueId)

      when(testStatRepository.getQueueStat(queueId))
        .thenReturn(Some(queueStat.ref))

      ref ! event

      queueStat.expectMsg(event)
    }
    "send timeout queue event to existing queueStatistic actor" in new Helper {
      val (ref, _) = actor()
      val queueId  = 15
      val event: Timeout = Timeout(queueId, 25)

      when(testStatRepository.getQueueStat(queueId))
        .thenReturn(Some(queueStat.ref))

      ref ! event

      queueStat.expectMsg(event)
    }
    "send exitwithkey queue event to existing queueStatistic actor" in new Helper {
      val (ref, _) = actor()
      val queueId = 15
      val event: ExitWithKey = ExitWithKey(queueId)

      when(testStatRepository.getQueueStat(queueId))
        .thenReturn(Some(queueStat.ref))

      ref ! event

      queueStat.expectMsg(event)
    }
    "send leaveemtpy queue event to existing queueStatistic actor" in new Helper {
      val (ref, _) = actor()
      val queueId = 15
      val event: LeaveEmpty = LeaveEmpty(queueId)

      when(testStatRepository.getQueueStat(queueId))
        .thenReturn(Some(queueStat.ref))

      ref ! event

      queueStat.expectMsg(event)
    }

    "send request stat message to all queue managers" in new Helper {
      import stats.Statistic._
      import xivo.models.XivoObject.ObjectType._
      val statManager: TestProbe = TestProbe()

      val (ref, _) = actor()

      when(testStatRepository.getAllStats).thenReturn(List(statManager.ref))

      val request: RequestStat = RequestStat(TestProbe().ref, ObjectDefinition(Queue))

      ref ! request

      statManager.expectMsg(request)
    }

    "send reset stats to all queue managers" in new Helper {
      import stats.Statistic._
      val statManager: TestProbe = TestProbe()

      val (ref, _) = actor()

      when(testStatRepository.getAllStats).thenReturn(List(statManager.ref))

      ref ! ResetStat

      statManager.expectMsg(ResetStat)

    }
  }
}
