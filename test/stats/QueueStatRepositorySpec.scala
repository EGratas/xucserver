package stats

import org.apache.pekko.actor.Actor
import pekkotest.TestKitSpec
import org.apache.pekko.testkit.TestProbe

class QueueStatRepositorySpec extends TestKitSpec("QueueStatRepositorySpec") {

  class Helper {
    val queueStatManagerFactory: QueueStatManager.Factory = new QueueStatManager.Factory {
      def apply(queueId: Int): Actor =
        new Actor { def receive: Receive = Actor.emptyBehavior }
    }
    val queueStatRepository = new QueueStatRepository(queueStatManagerFactory)
  }

  "Queue stat repository" should {
    "should retrieve created actor" in new Helper {
      val queueId   = 4
      val statActor: TestProbe = TestProbe()

      queueStatRepository.repo += (4 -> statActor.ref)

      queueStatRepository.getQueueStat(queueId) should be(Some(statActor.ref))
    }
    "shoud pass a list of created stats managers" in new Helper {
      val statActor: TestProbe = TestProbe()

      queueStatRepository.repo += (4 -> statActor.ref)

      queueStatRepository.getAllStats(0) should be(statActor.ref)
    }
  }
}
