package stats

import org.apache.pekko.testkit.TestActorRef
import pekkotest.TestKitSpec
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import services.XucStatsEventBus
import services.XucStatsEventBus.{AggregatedStatEvent, Stat, StatUpdate}
import stats.StatsAggregator.Flush
import xivo.models.XivoObject.{ObjectDefinition, ObjectType}
import xivo.xucstats.XucBaseStatsConfig

class StatsAggregatorSpec
    extends TestKitSpec("StatsAggregatorSpec")
    with MockitoSugar {

  class Helper() {
    val statsBus: XucStatsEventBus = mock[XucStatsEventBus]
    val config: XucBaseStatsConfig   = mock[XucBaseStatsConfig]

    when(config.aggregationPeriod).thenReturn(1L)

    def actor(): (TestActorRef[StatsAggregator], StatsAggregator) = {
      val sa = TestActorRef(new StatsAggregator(statsBus, config))
      (sa, sa.underlyingActor)
    }
  }

  "StatsAggregator" should {

    "publish statistic when received following its creation" in new Helper() {
      val (ref, _) = actor()
      val objDef: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(11))

      val statUpdate: StatUpdate =
        StatUpdate(objDef, List(Stat("TotalTestCounter", 2.0), Stat("EWT", 2)))
      ref ! statUpdate

      verify(statsBus).publish(
        AggregatedStatEvent(
          objDef,
          List[Stat](Stat("TotalTestCounter", 2.0), Stat("EWT", 2))
        )
      )
    }

    "aggregate statistic when received following other statistics" in new Helper() {
      val (ref, _) = actor()
      val objDef1: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(11))
      val objDef2: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(22))

      ref ! StatUpdate(objDef1, List(Stat("TotalTestCounter", 2.0)))

      verify(statsBus).publish(
        AggregatedStatEvent(objDef1, List[Stat](Stat("TotalTestCounter", 2.0)))
      )
      verifyNoMoreInteractions(statsBus)

      ref ! StatUpdate(
        objDef1,
        List(Stat("TotalTestCounter", 3.0), Stat("ShellTestCounter", 3.0))
      )
      ref ! StatUpdate(objDef2, List(Stat("TotalTestSecondCounter", 5.0)))
      ref ! StatUpdate(objDef1, List(Stat("TotalTestSecondCounter", 2.0)))
      ref ! Flush
      verify(statsBus).publish(
        AggregatedStatEvent(
          objDef1,
          List(
            Stat("TotalTestCounter", 3.0),
            Stat("ShellTestCounter", 3.0),
            Stat("TotalTestSecondCounter", 2.0)
          )
        )
      )
      verify(statsBus).publish(
        AggregatedStatEvent(
          objDef2,
          List(
            Stat("TotalTestSecondCounter", 5.0)
          )
        )
      )
    }

    "replace old values in aggregated statistics" in new Helper() {
      val (ref, _) = actor()
      val objDef1: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(11))
      val objDef2: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(22))

      ref ! StatUpdate(objDef1, List(Stat("TotalTestCounter", 2.0)))
      verify(statsBus).publish(
        AggregatedStatEvent(objDef1, List[Stat](Stat("TotalTestCounter", 2.0)))
      )
      verifyNoMoreInteractions(statsBus)

      ref ! StatUpdate(
        objDef1,
        List(
          Stat("TotalTestCounter", 3.0),
          Stat("ShellTestCounter", 3.0),
          Stat("TotalTestSecondCounter", 2.0)
        )
      )
      ref ! StatUpdate(objDef2, List(Stat("TotalTestSecondCounter", 5.0)))
      ref ! StatUpdate(objDef2, List(Stat("TotalTestSecondCounter", 7.0)))
      ref ! StatUpdate(objDef1, List(Stat("TotalTestSecondCounter", 8.0)))

      ref ! Flush
      verify(statsBus).publish(
        AggregatedStatEvent(
          objDef1,
          List(
            Stat("TotalTestCounter", 3.0),
            Stat("ShellTestCounter", 3.0),
            Stat("TotalTestSecondCounter", 8.0)
          )
        )
      )
      verify(statsBus).publish(
        AggregatedStatEvent(
          objDef2,
          List(
            Stat("TotalTestSecondCounter", 7.0)
          )
        )
      )
    }

    "publish statistic when received after the aggregation period" in new Helper() {
      val (ref, _) = actor()
      val objDef: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(11))

      ref ! StatUpdate(objDef, List(Stat("TotalTestCounter", 2.0)))
      verify(statsBus).publish(
        AggregatedStatEvent(objDef, List[Stat](Stat("TotalTestCounter", 2.0)))
      )
      verifyNoMoreInteractions(statsBus)

      ref ! StatUpdate(objDef, List(Stat("TotalTestCounter", 3.0)))
      ref ! StatUpdate(objDef, List(Stat("TotalTestSecondCounter", 5.0)))
      ref ! Flush
      verify(statsBus).publish(
        AggregatedStatEvent(
          objDef,
          List(
            Stat("TotalTestCounter", 3.0),
            Stat("TotalTestSecondCounter", 5.0)
          )
        )
      )

      ref ! Flush
      ref ! StatUpdate(objDef, List(Stat("TotalTestThirdCounter", 30.0)))
      verify(statsBus).publish(
        AggregatedStatEvent(
          objDef,
          List[Stat](Stat("TotalTestThirdCounter", 30.0))
        )
      )
    }

  }
}
