package stats

import services.XucStatsEventBus.{AggregatedStatEvent, Stat, StatUpdate}
import xivo.models.XivoObject.{ObjectDefinition, ObjectType}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class StatisticBufferSpec extends AnyWordSpec with Matchers {

  trait Buffer {
    val statBuffer = new StatisticBuffer
  }

  "a statistic buffer" should {
    "store a stat update" in new Buffer {

      val forAQueue: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(11))

      val newBuffer: StatisticBuffer = statBuffer.addStat(
        StatUpdate(
          forAQueue,
          List(Stat("TotalTestCounter", 3.0), Stat("ShellTestCounter", 7.0))
        )
      )

      newBuffer.statBuffer(forAQueue) should contain(
        "TotalTestCounter" -> 3.0
      )
      newBuffer.statBuffer(forAQueue) should contain(
        "ShellTestCounter" -> 7.0
      )
    }

    "update a stat value on stat update" in new Buffer {

      val forAQueue: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(32))

      statBuffer.addStat(
        StatUpdate(
          forAQueue,
          List(Stat("TotalTestCounter", 3.0), Stat("ShellTestCounter", 7.0))
        )
      )

      val newBuffer: StatisticBuffer = statBuffer.addStat(
        StatUpdate(forAQueue, List(Stat("ShellTestCounter", 12.0)))
      )

      newBuffer.statBuffer(forAQueue) should contain(
        "TotalTestCounter" -> 3.0
      )
      newBuffer.statBuffer(forAQueue) should contain(
        "ShellTestCounter" -> 12.0
      )

    }

    "is empty at init" in new Buffer {
      statBuffer.isEmpty should be(true)
    }
    "is not empty when stats are accumated" in new Buffer {

      val forAQueue: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(32))
      statBuffer.addStat(
        StatUpdate(
          forAQueue,
          List(Stat("TotalTestCounter", 3.0), Stat("ShellTestCounter", 7.0))
        )
      )

      statBuffer.isEmpty should be(false)

    }

    "aggregate statistics" in new Buffer {
      val forAQueue: ObjectDefinition = ObjectDefinition(ObjectType.Queue, Some(11))

      val newBuffer: StatisticBuffer = statBuffer.addStat(
        StatUpdate(
          forAQueue,
          List(Stat("TotalTestCounter", 3.0), Stat("ShellTestCounter", 7.0))
        )
      )

      newBuffer.aggregate should contain(
        AggregatedStatEvent(
          forAQueue,
          List(Stat("TotalTestCounter", 3.0), Stat("ShellTestCounter", 7.0))
        )
      )

    }
  }

}
