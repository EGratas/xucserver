package stats

import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceInjectorBuilder
import xivo.xuc.XucConfig
import xivo.xucstats.XucBaseStatsConfig
import play.api.inject.Injector
import us.theatr.pekko.quartz.AddCronSchedule

class QueueStatManagerSpec
    extends TestKitSpec("QueueStatManagerSpec")
    with MockitoSugar {
  import stats.Statistic._

  val injector: Injector = new GuiceInjectorBuilder()
    .injector()

  class Helper {
    val queueId                        = 13
    val cronScheduler: TestProbe                  = TestProbe()
    val config: XucConfig              = mock[XucConfig]
    val statConfig: XucBaseStatsConfig = mock[XucBaseStatsConfig]

    when(statConfig.resetSchedule).thenReturn("0 32 0 * * ?")
    when(statConfig.queuesStatTresholdsInSec).thenReturn(Set[Int]())
    when(config.metricsRegistryName).thenReturn("TestMetrics")

    def actor(): (TestActorRef[QueueStatManager], QueueStatManager) = {
      val a = TestActorRef(
        new QueueStatManager(
          queueId,
          injector,
          cronScheduler.ref,
          config,
          statConfig
        )
      )
      (a, a.underlyingActor)
    }
  }

  "Queue Stat Manager" should {
    "self register to quartz scheduler" in new Helper {

      val (ref, _) = actor()

      val cronSchedule: AddCronSchedule = AddCronSchedule(ref, "0 32 0 * * ?", ResetStat)

      cronScheduler.expectMsg(cronSchedule)

    }
  }
}
