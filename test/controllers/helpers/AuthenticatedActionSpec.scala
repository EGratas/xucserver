package controllers.helpers

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.Materializer
import org.apache.pekko.testkit.TestProbe
import models.XivoUser
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.TryValues._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.libs.ws.WSRequest
import play.api.libs.ws.WSResponse
import play.api.mvc._
import play.api.test.Helpers._
import play.api.test._
import services.auth.OidcSampleToken
import services.config.ConfigRepository
import xivo.xuc.XucBaseConfig
import xuctest.BaseTest
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success
import models.ws.auth.AuthenticationException
import models.ws.auth.AuthenticationInformation

class AuthenticatedActionSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {

  implicit val system: ActorSystem    = ActorSystem()
  implicit lazy val mat: Materializer = app.materializer

  val oidcConfig: Map[String, String] = Map(
    "oidc.enable"           -> "true",
    "oidc.trustedServerUrl" -> OidcSampleToken.testUrl,
    "oidc.clientId"         -> OidcSampleToken.clientId,
    "oidc.audience"         -> OidcSampleToken.audience
  )

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig ++ oidcConfig)
    ).build()

  "AuthenticatedRequestParser" should {
    "extract bearer information from string" in {
      AuthenticatedRequestParser.extractBearer(
        "Bearer my-token"
      ) shouldBe Success("my-token")
    }

    "fails to extract bearer if none is present" in {
      val res = AuthenticatedRequestParser.extractBearer("my-token")
      res should be a Symbol("failure")
      res.failure.exception shouldBe a[AuthenticationException]
    }

    "extract token from query string" in {
      val r = FakeRequest("GET", "/toto?token=abcd-efgh")
      AuthenticatedRequestParser.extractToken(r) shouldBe Success("abcd-efgh")
    }

    "extract token from Authorization header" in {
      val r = FakeRequest(
        "GET",
        "/toto",
        FakeHeaders(Seq(("Authorization", "Bearer efgh-abcd"))),
        AnyContentAsEmpty
      )
      AuthenticatedRequestParser.extractToken(r) shouldBe Success("efgh-abcd")
    }

    "fail to extract token if none is present" in {
      val r = FakeRequest("GET", "/toto?token")
      AuthenticatedRequestParser.extractToken(r) should be a Symbol("failure")
    }
  }

  "AuthenticatedAction" should {
    val defaultParser: BodyParser[AnyContent] = new BodyParsers.Default()

    "fails when no token is present" in {
      val ctrl = call(
        AuthenticatedAction(authenticationSecret)(
          mock[ConfigRepository],
          global,
          defaultParser
        ) { _ =>
          Ok("It works!")
        },
        FakeRequest()
      )

      status(ctrl) shouldBe BAD_REQUEST
    }

    "fails when token is invalid" in {
      val rq = FakeRequest(
        "GET",
        "/toto",
        FakeHeaders(Seq(("Authorization", "Bearer efgh-abcd"))),
        AnyContentAsEmpty
      )
      val ctrl = call(
        AuthenticatedAction(authenticationSecret)(
          mock[ConfigRepository],
          global,
          defaultParser
        ) { _ =>
          Ok("It works!")
        },
        rq
      )

      status(ctrl) shouldBe UNAUTHORIZED
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe "InvalidToken"
    }

    "succeed if token is present in header" in {
      val repo    = mock[ConfigRepository]
      val expires = 54000
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val now = new DateTime().getMillis / 1000
      val token =
        AuthenticationInformation(
          "jbond",
          now + expires,
          now,
          "cti",
          List("alias.ctiuser"),
          None
        )
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/contact/personal",
        FakeHeaders(
          Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
        ),
        AnyContentAsEmpty
      )
      val ctrl = call(
        AuthenticatedAction(authenticationSecret)(repo, global, defaultParser) {
          _ =>
            Ok("It works!")
        },
        rq
      )

      status(ctrl) shouldBe OK
    }

    "succeed if token is present in querystring" in {
      val expires = 54000
      val repo    = mock[ConfigRepository]
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )
      val now = new DateTime().getMillis / 1000
      val token =
        AuthenticationInformation(
          "jbond",
          now + expires,
          now,
          "cti",
          List("alias.ctiuser"),
          None
        )
      val rq =
        FakeRequest(
          "GET",
          "/xuc/api/2.0/contact/personal?token=" + token.encode(
            authenticationSecret
          )
        )
      val ctrl = call(
        AuthenticatedAction(authenticationSecret)(repo, global, defaultParser) {
          _ =>
            Ok("It works!")
        },
        rq
      )

      status(ctrl) shouldBe OK
    }

    "fails if token is expired" in {
      val expires = 50000

      val repo = mock[ConfigRepository]
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val now = new DateTime().getMillis / 1000
      val token =
        AuthenticationInformation(
          "jbond",
          now - expires,
          now,
          "cti",
          List.empty,
          None
        )
      val rq =
        FakeRequest(
          "GET",
          "/xuc/api/2.0/contact/personal?token=" + token.encode(
            authenticationSecret
          )
        )
      val ctrl = call(
        AuthenticatedAction(authenticationSecret)(repo, global, defaultParser) {
          _ =>
            Ok("It fails!")
        },
        rq
      )

      status(ctrl) shouldBe FORBIDDEN
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe "TokenExpired"
    }

    "converts a request to the requested acl for the request" in {
      val request1 =
        FakeRequest("POST", "/xuc/api/2.0/mobile/push/register")
      AuthenticatedAction.aclFromRequest(
        request1.path,
        request1.method
      ) shouldBe Some("xuc.rest.mobile.push.register.create")

      val request2 = FakeRequest("GET", "/xuc/api/2.0/contact/personal/toto")
      AuthenticatedAction.aclFromRequest(
        request2.path,
        request2.method
      ) shouldBe Some("xuc.rest.contact.personal.*.read")

      val request3 =
        FakeRequest("POST", "/xuc/api/2.0/call_qualification/queue/19830")
      AuthenticatedAction.aclFromRequest(
        request3.path,
        request3.method
      ) shouldBe Some("xuc.rest.call_qualification.queue.*.create")

      val request4 =
        FakeRequest(
          "GET",
          "/xuc/api/2.0/config/meetingrooms/temporary/token/💃kilia@°.;:n%2.read./0e\\rw an🏴‍☠️'"
        )
      AuthenticatedAction.aclFromRequest(
        request4.path,
        request4.method
      ) shouldBe Some("xuc.rest.config.meetingrooms.temporary.token.*.read")

      val request5 =
        FakeRequest(
          "POST",
          "/xuc/api/2.0/dial/number/bouffi"
        )
      AuthenticatedAction.aclFromRequest(
        request5.path,
        request5.method
      ) shouldBe Some("xuc.rest.dial.number.*.create")

      val request6 =
        FakeRequest(
          "POST",
          "/xuc/api/2.0/dial/user/bouffi"
        )
      AuthenticatedAction.aclFromRequest(
        request6.path,
        request6.method
      ) shouldBe Some("xuc.rest.dial.user.*.create")

      val request7 =
        FakeRequest(
          "POST",
          "/xuc/api/2.0/dial/fromqueue/bouffi"
        )
      AuthenticatedAction.aclFromRequest(
        request7.path,
        request7.method
      ) shouldBe Some("xuc.rest.dial.fromqueue.*.create")

      val request8 =
        FakeRequest(
          "POST",
          "/xuc/api/2.0/dnd/bouffi"
        )
      AuthenticatedAction.aclFromRequest(
        request8.path,
        request8.method
      ) shouldBe Some("xuc.rest.dnd.*.create")

      val request9 =
        FakeRequest(
          "POST",
          "/xuc/api/2.0/forward/unconditional/bouffi"
        )
      AuthenticatedAction.aclFromRequest(
        request9.path,
        request9.method
      ) shouldBe Some("xuc.rest.forward.unconditional.*.create")

      val request10 =
        FakeRequest(
          "POST",
          "/xuc/api/2.0/forward/noanswer/bouffi"
        )
      AuthenticatedAction.aclFromRequest(
        request10.path,
        request10.method
      ) shouldBe Some("xuc.rest.forward.noanswer.*.create")

      val request11 =
        FakeRequest(
          "POST",
          "/xuc/api/2.0/forward/busy/bouffi"
        )
      AuthenticatedAction.aclFromRequest(
        request11.path,
        request11.method
      ) shouldBe Some("xuc.rest.forward.busy.*.create")

      val request12 =
        FakeRequest(
          "GET",
          "/xuc/api/2.0/history/bouffi"
        )
      AuthenticatedAction.aclFromRequest(
        request12.path,
        request12.method
      ) shouldBe Some("xuc.rest.history.*.read")

      val request13 =
        FakeRequest(
          "GET",
          "/xuc/api/2.0/history/days/bouffi"
        )
      AuthenticatedAction.aclFromRequest(
        request13.path,
        request13.method
      ) shouldBe Some("xuc.rest.history.days.*.read")

      val request14 =
        FakeRequest(
          "POST",
          "/xuc/api/2.0/callback_lists/callback_requests/csv"
        )
      AuthenticatedAction.aclFromRequest(
        request14.path,
        request14.method
      ) shouldBe Some("xuc.rest.callback_lists.callback_requests.csv.create")

      val request15 =
        FakeRequest(
          "POST",
          "/xuc/api/2.0/callback_lists/callback_tickets/csv"
        )
      AuthenticatedAction.aclFromRequest(
        request15.path,
        request15.method
      ) shouldBe Some("xuc.rest.callback_lists.callback_tickets.csv.create")
    }

    "return true when validating ACL for /auth as they are technical API" in {
      val acls = List(
        "alias.ctiuser"
      )

      AuthenticatedAction.validateACL(
        "/xuc/api/2.0/auth/login",
        None,
        acls
      ) shouldBe true
    }

    "return true if required ACL is in list of ACLs" in {
      val requiredAcl = Some("xuc.rest.mobile.push.register.create")
      val acls = List(
        "alias.ctiuser"
      )
      AuthenticatedAction.validateACL(
        "/xuc/api/2.0/mobile/push/register",
        requiredAcl,
        acls
      ) shouldBe true
    }

    "return false if required ACL is not in list of ACLs" in {
      val requiredAcl = Some("xuc.rest.mobile.push.falsy.create")
      val acls = List(
        "alias.ctiuser"
      )

      AuthenticatedAction.validateACL(
        "/xuc/api/2.0/mobile/push/falsy",
        requiredAcl,
        acls
      ) shouldBe false
    }

    "succeed if endpoint required ACL is present in token ACLs" in {
      val repo    = mock[ConfigRepository]
      val expires = 54000
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val now = new DateTime().getMillis / 1000
      val token =
        AuthenticationInformation(
          "jbond",
          now + expires,
          now,
          "cti",
          List("alias.ctiuser"),
          None
        )
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/contact/personal",
        FakeHeaders(
          Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
        ),
        AnyContentAsEmpty
      )
      val ctrl = call(
        AuthenticatedAction(authenticationSecret)(
          repo,
          global,
          defaultParser
        ) { _ =>
          Ok("It works!")
        },
        rq
      )

      status(ctrl) shouldBe OK
    }

    "fail if endpoint required ACL is missing from token ACLs" in {
      val repo    = mock[ConfigRepository]
      val expires = 54000
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val now = new DateTime().getMillis / 1000
      val token =
        AuthenticationInformation(
          "jbond",
          now + expires,
          now,
          "cti",
          List("alias.ctiuser"),
          None
        )
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/fake",
        FakeHeaders(
          Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
        ),
        AnyContentAsEmpty
      )
      val ctrl = call(
        AuthenticatedAction(authenticationSecret)(
          repo,
          global,
          defaultParser
        ) { _ =>
          Ok("It works!")
        },
        rq
      )

      status(ctrl) shouldBe FORBIDDEN
    }
  }

  "OidcAuthenticatedAction" should {
    val defaultParser: BodyParser[AnyContent] = new BodyParsers.Default()
    val wsClient                              = mock[WSClient]
    val mockRequest                           = mock[WSRequest]
    val mockResponse                          = mock[WSResponse]
    val repo                                  = mock[ConfigRepository]
    val xivoAuthentication                    = TestProbe()
    lazy val config: XucBaseConfig            = app.injector.instanceOf[XucBaseConfig]

    when(wsClient.url(any[String])).thenReturn(mockRequest)
    when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
    when(mockResponse.json).thenReturn(Json.parse(OidcSampleToken.keyResponse))
    when(repo.getCtiUser(OidcSampleToken.user.username))
      .thenReturn(Some(OidcSampleToken.xivoUser))

    "fails when no token is present" in {
      val ctrl = call(
        OidcAuthenticatedAction(wsClient, config)(
          repo,
          global,
          defaultParser,
          xivoAuthentication.ref
        ) { _ =>
          Ok("It works!")
        },
        FakeRequest()
      )

      status(ctrl) shouldBe BAD_REQUEST
    }

    "fails when token is invalid" in {
      val rq = FakeRequest(
        "GET",
        "/toto",
        FakeHeaders(Seq(("Authorization", "Bearer efgh-abcd"))),
        AnyContentAsEmpty
      )
      val ctrl = call(
        OidcAuthenticatedAction(wsClient, config)(
          repo,
          global,
          defaultParser,
          xivoAuthentication.ref
        ) { _ =>
          Ok("It works!")
        },
        rq
      )

      status(ctrl) shouldBe UNAUTHORIZED
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe "InvalidToken"
    }

    "succeed if token is present in querystring" in {

      val rq =
        FakeRequest("GET", "/toto?token=" + OidcSampleToken.token)
      val ctrl = call(
        OidcAuthenticatedAction(wsClient, config)(
          repo,
          global,
          defaultParser,
          xivoAuthentication.ref
        ) { _ =>
          Ok("It works!")
        },
        rq
      )

      status(ctrl) shouldBe OK
    }

    "succeed if token is present in header" in {

      val rq =
        FakeRequest("GET", "/toto")
          .withHeaders(("Authorization", "Bearer " + OidcSampleToken.token))

      val ctrl = call(
        OidcAuthenticatedAction(wsClient, config)(
          repo,
          global,
          defaultParser,
          xivoAuthentication.ref
        ) { _ =>
          Ok("It works!")
        },
        rq
      )

      status(ctrl) shouldBe OK
    }
  }
}
