package controllers.xuc

import org.scalatestplus.play.guice.GuiceOneServerPerSuite
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.ws.WSClient
import play.api.test.Helpers._
import play.api.test.WsTestClient
import xuctest.BaseTest

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import play.api.Application

class WebSocketAppSpec extends BaseTest with GuiceOneServerPerSuite {

  private def await[T](future: Future[T]) = Await.result(future, 30.seconds)

  private def wsUrl(url: String)(implicit client: WSClient) = {
    client
      .url(s"http://localhost:$port" + url)
      .withHttpHeaders(
        "Upgrade"               -> "websocket",
        "Connection"            -> "Upgrade",
        "Sec-WebSocket-Version" -> "13",
        "Sec-WebSocket-Key"     -> "uzmck/1+1A5FQvwCEuwdSA==",
        "Origin"                -> "http://xuc.org"
      )
  }

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .build()

  "WebSocketApp" should {

    "reject request for empty username" in {
      WsTestClient.withClient { implicit client =>
        await(
          wsUrl("/xuc/ctichannel?username=&agentNumber=0&password=asd").get()
        ).status shouldBe UNAUTHORIZED
      }
    }

    "reject request for empty token" in {
      WsTestClient.withClient { implicit client =>
        await(
          wsUrl("/xuc/ctichannel/tokenAuthentication?token=").get()
        ).status shouldBe FORBIDDEN
      }
    }

    "reject authentication request for unknown token" in {
      {
        WsTestClient.withClient { implicit client =>
          await(
            wsUrl(
              "/xuc/ctichannel/tokenAuthentication?token=unexisting_authtoken123456"
            ).get()
          ).status shouldBe FORBIDDEN
        }
      }
    }
  }
}
