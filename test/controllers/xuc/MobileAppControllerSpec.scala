package controllers.xuc

import models.ws.auth.AuthenticationInformation
import models.{MobileAppPushToken, XivoUser}
import org.mockito.Mockito.{reset, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.WSResponse
import play.api.mvc._
import play.api.test.Helpers.{call, status, _}
import play.api.test.{FakeHeaders, FakeRequest}
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.network.WebServiceException
import xuctest.BaseTest
import org.joda.time.DateTime
import org.apache.pekko.stream.Materializer

import scala.concurrent.Future
import play.api.Application

class MobileAppControllerSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {
  val repo: ConfigRepository                                 = mock[ConfigRepository]
  val configMgtMock: ConfigServerRequester = mock[ConfigServerRequester]
  implicit lazy val mat: Materializer = app.materializer

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[ConfigServerRequester].to(configMgtMock))
      .build()

  class Helper {
    reset(repo)
    reset(configMgtMock)

    def getCtrl: MobileAppController = app.injector.instanceOf[MobileAppController]

    val wsResponse: WSResponse = mock[WSResponse]
    val expires                = 56700

    val user: XivoUser =
      XivoUser(1, None, None, "James", Some("Bond"), Some("jbond"), None, None, None)
    when(repo.getCtiUser("jbond")).thenReturn(Some(user))
    val now: Long = new DateTime().getMillis / 1000
    val xucToken: AuthenticationInformation =
      AuthenticationInformation(
        user.username.get,
        now + expires,
        now,
        "cti",
        List("alias.ctiuser"),
        None
      )
    val fakeAuth: FakeHeaders = FakeHeaders(
      Seq(("Authorization", "Bearer " + xucToken.encode(authenticationSecret)))
    )

    val pushToken: MobileAppPushToken            = MobileAppPushToken(Some("myToken"))
    val pushTokenRaw: String = """{"token": "myToken"}"""

  }

  "Mobile App Controller" should {

    "set a mobile push notification token for a user" in new Helper {
      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        s"/xuc/api/2.0/mobile/push/register",
        fakeAuth,
        AnyContentAsJson(Json.parse(pushTokenRaw))
      )

      when(
        configMgtMock.setMobileAppPushToken(
          user.username,
          pushToken
        )
      )
        .thenReturn(Future.unit)

      val ctrl: Future[Result] = call(getCtrl.add(), rq)

      status(ctrl) shouldBe CREATED
      verify(configMgtMock).setMobileAppPushToken(
        user.username,
        pushToken
      )
    }

    "remove a mobile push notification token for a user" in new Helper {
      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        s"/xuc/api/2.0/mobile/push/register",
        fakeAuth,
        AnyContentAsJson(Json.parse("""{"token": null}"""))
      )

      when(
        configMgtMock.setMobileAppPushToken(
          user.username,
          MobileAppPushToken(None)
        )
      )
        .thenReturn(Future.unit)

      val ctrl: Future[Result] = call(getCtrl.add(), rq)

      status(ctrl) shouldBe CREATED
      verify(configMgtMock).setMobileAppPushToken(
        user.username,
        MobileAppPushToken(None)
      )
    }

    "respond 400 if json body is not found in create" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "POST",
        s"/xuc/api/2.0/mobile/push/register",
        fakeAuth,
        AnyContentAsEmpty
      )
      val ctrl: Future[Result] = call(getCtrl.add(), rq)

      status(ctrl) shouldBe BAD_REQUEST
    }

    "respond 500 if exception when web service called" in new Helper {
      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        s"/xuc/api/2.0/mobile/push/register",
        fakeAuth,
        AnyContentAsJson(Json.parse(pushTokenRaw))
      )

      when(
        configMgtMock.setMobileAppPushToken(
          user.username,
          pushToken
        )
      )
        .thenReturn(
          Future.failed(
            new WebServiceException("Call to web service failed.")
          )
        )

      val ctrl: Future[Result] = call(getCtrl.add(), rq)

      status(ctrl) shouldBe INTERNAL_SERVER_ERROR
      verify(configMgtMock).setMobileAppPushToken(
        user.username,
        pushToken
      )
    }
  }
}
