package controllers.api

import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.mvc._
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.xuc.IPFilterConfig
import xuctest.BaseTest
import scala.concurrent.{ExecutionContext, Future}
import org.apache.pekko.stream.Materializer

class IPFilterSpec
    extends BaseTest
    with MockitoSugar
    with Results
    with GuiceOneAppPerSuite {

  implicit lazy val mat: Materializer = app.materializer
  implicit lazy val executionContext: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  class Helper {

    val defaultParser: BodyParser[AnyContent] = new BodyParsers.Default()

    def buildRequest(): FakeRequest[AnyContentAsEmpty.type] = {
      FakeRequest("POST", s"").withHeaders(("Content-Type", "application/json"))
    }

    def block[A](request: Request[A]): Future[Result] = {
      Future(Ok("block"))
    }

    def getIPFilter(address: String): IPFilter = {
      new IPFilter(
        new BodyParsers.Default(),
        new IPFilterConfig { def ipAccepted: List[String] = List[String](address) }
      )
    }
  }

  "IPFilter" should {
    "Return OK" in new Helper {
      val req: FakeRequest[AnyContentAsEmpty.type] = buildRequest()
      val result: Future[Result] = getIPFilter("127.0.0.1").invokeBlock(req, block)
      status(result) shouldBe OK
    }
    "Return Service Unavailable" in new Helper {
      val req: FakeRequest[AnyContentAsEmpty.type] = buildRequest()
      val result: Future[Result] = getIPFilter("13.13.13.13").invokeBlock(req, block)

      status(result) shouldBe SERVICE_UNAVAILABLE
    }
  }

}
