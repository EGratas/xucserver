package xuctest

import models.{XivoUser, XucUser}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.Tag
import play.api.db.{Database, Databases}
import xivo.xucami.models.{CallerId, Channel, ChannelState, MonitorState}
import xivo.xucami.models.ChannelState.*
import xivo.xucami.models.MonitorState.MonitorState
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.{AnyWordSpec, AnyWordSpecLike}

import scala.util.Random

object IntegrationTest extends Tag("xuc.tags.IntegrationTest")

trait ChannelGen {
  val cid: CallerId = CallerId("Birgit", "2025")
  val r: Random.type = scala.util.Random

  private def callId = s"${r.nextInt(100000000)}.${r.nextInt(99)}"

  private def bChanName(interface: String) =
    s"$interface-00000${r.nextInt(99)};1"

  private def bLocalName(extension: String) =
    s"Local/$extension@default-00000${r.nextInt(99)};1"

  def genChan(
      e: String,
      chanName: String => String,
      st: ChannelState,
      callerId: CallerId,
      linkedId: String
  ): Channel =
    Channel(
      callId,
      chanName(e),
      callerId,
      if (linkedId == "") callId else linkedId,
      state = st
    )

  def bchan(
      interface: String,
      st: ChannelState = ChannelState.UP,
      callerId: CallerId = cid,
      linkedId: String = "",
      connectedLine: String = "",
      m: MonitorState = MonitorState.DISABLED
  ): Channel = {
    Channel(
      callId,
      bChanName(interface),
      callerId,
      if (linkedId == "") callId else linkedId,
      state = st,
      connectedLineNb = Some(connectedLine),
      monitored = m
    )
  }

  def bChanLocal(
      extension: String,
      st: ChannelState = ChannelState.UP,
      callerId: CallerId = cid,
      linkedId: String = "",
      connectedLine: String = ""
  ): Channel = {
    Channel(
      callId,
      bLocalName(extension),
      callerId,
      if (linkedId == "") callId else linkedId,
      state = st,
      connectedLineNb = Some(connectedLine)
    )
  }

  def bChanNumbers(
      callerNo: String,
      calledNo: String,
      chanName: String = "test"
  ): Channel =
    Channel(
      callId,
      bChanName(chanName),
      CallerId("caller", callerNo),
      callId,
      ChannelState.UP,
      connectedLineNb = Some(calledNo)
    )

  def monitored(chan: Channel): Channel =
    chan.copy(monitored = MonitorState.ACTIVE)
}

trait IntegrationConfig {
  def authenticationSecret = "test"
  def xivoIntegrationConfig: Map[String, Any] = {
    Map(
      "XivoWs.wsUser"           -> "xivows",
      "XivoWs.wsPwd"            -> "xivows",
      "XivoWs.host"             -> "xivo-integration",
      "XivoWs.port"             -> "9486",
      "ws.acceptAnyCertificate" -> "true",
      "api.eventUrl"            -> "",
      "authentication.secret"   -> authenticationSecret,
      "api.ipaccepted"          -> "127.0.0.1"
    )
  }

  def dbtestConfig: Map[String, String] = {
    Map(
      "db.default.driver"   -> "org.postgresql.Driver",
      "db.default.url"      -> "jdbc:postgresql://127.0.0.1/testdata",
      "db.default.user"     -> "test",
      "db.default.password" -> "test"
    )
  }
}

trait XucUserHelper {
  def getXucUser(
      username: String,
      pwd: String,
      phone: Option[String] = None,
      mobilePhone: Option[String] = None
  ): XucUser = {
    XucUser(
      username,
      XivoUser(
        1,
        Some(1),
        None,
        "John",
        Some("Doe"),
        Some(username),
        Some(pwd),
        mobilePhone,
        Some(1)
      ),
      phone
    )
  }

}

trait TestDatabase {
  def withTestDataDatabase[T](block: Database => T): T = {
    Databases.withDatabase(
      driver = "org.postgresql.Driver",
      url = "jdbc:postgresql://127.0.0.1/testdata",
      config = Map(
        "user"     -> "test",
        "password" -> "test"
      )
    )(block)
  }
}

trait ScalaTestTools extends AnyWordSpecLike with Matchers with MockitoSugar {}

class BaseTest
    extends AnyWordSpec
    with Matchers
    with IntegrationConfig
    with TestDatabase {}
