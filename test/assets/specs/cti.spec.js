describe("CTI receive function", function() {
    it("should call subscriber on msgType", function () {

        var event = {};
        var message = {};
        message.msgType = Cti.MessageType.AGENTERROR;
        message.ctiMessage = "{ctiMessage}";
        event.data = JSON.stringify(message);

        var Callback = {
                handler: function() {
                  return true;
                }
            };

        spyOn(Callback, 'handler');
        Cti.setHandler(Cti.MessageType.AGENTERROR,Callback.handler);
        Cti.receive(event);
        expect(Callback.handler).toHaveBeenCalledWith(message.ctiMessage);
    });
});

describe("CTI functions", function() {
    var testSocket ={};

    beforeEach(function() {
        testSocket = {
                sendCallback: function() {
                  return true;
                },
                close: function() {
                  return true;
                }
        };
        spyOn(testSocket, 'sendCallback');
        spyOn(testSocket, 'close');
        Cti.init("user", "agent", testSocket);
    });

    it("should send agent login message to websocket", function() {
        var agentPhoneNumber = 2002;
        var message = Cti.WebsocketMessageFactory.createAgentLogin(agentPhoneNumber);

        Cti.loginAgent(agentPhoneNumber);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send agent login message to websocket with agentid", function() {
        spyOn(Cti.WebsocketMessageFactory, 'createAgentLogin');
        var agentPhoneNumber = 2002;
        var agentId = 32;
        Cti.loginAgent(agentPhoneNumber,agentId);
        expect(Cti.WebsocketMessageFactory.createAgentLogin).toHaveBeenCalledWith(agentPhoneNumber,agentId);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it("should send agent logout message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createAgentLogout();

        Cti.logoutAgent();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send agent logout message to websocket with agentId", function() {
        spyOn(Cti.WebsocketMessageFactory, 'createAgentLogout');
        var agentId = 77;
        var message = "{}";
        Cti.logoutAgent(agentId);
        expect(Cti.WebsocketMessageFactory.createAgentLogout).toHaveBeenCalledWith(agentId);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it("should send pause agent message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createPauseAgent();

        Cti.pauseAgent();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send pause agent with agentid message to websocket", function() {
        spyOn(Cti.WebsocketMessageFactory, 'createPauseAgent');
        var agentId = 41;

        Cti.pauseAgent(agentId);
        expect(Cti.WebsocketMessageFactory.createPauseAgent).toHaveBeenCalledWith(agentId, undefined);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it("should send pause agent message with agentid and reason to websocket", function() {
        spyOn(Cti.WebsocketMessageFactory, 'createPauseAgent');
        var agentId = 41;

        Cti.pauseAgent(agentId, 'reason dej');
        expect(Cti.WebsocketMessageFactory.createPauseAgent).toHaveBeenCalledWith(agentId, "reason dej");
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });


    it("should send unpause agent message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createUnpauseAgent();

        Cti.unpauseAgent();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send unpause agent with agent id message to websocket", function() {
        spyOn(Cti.WebsocketMessageFactory, 'createUnpauseAgent');
        var agentId = 88;

        var message = Cti.WebsocketMessageFactory.createUnpauseAgent();

        Cti.unpauseAgent(agentId);

        expect(Cti.WebsocketMessageFactory.createUnpauseAgent).toHaveBeenCalledWith(agentId);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it('should send listen to an agent with agent id to websocket', function(){
          var msg = {"listenToAgent":1};
           var agentId = 456;

           var args = {};
           args.agentid = agentId;

           spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

           Cti.listenAgent(agentId);

           expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('listenAgent',args);
           expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });
    it("should send user status message to websocket", function() {
        var status = "unavailable";
        var message = Cti.WebsocketMessageFactory.createUserStatusUpdate(status);

        Cti.changeUserStatus(status);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send dnd status change request to websocket", function() {
        var state = true;
        var message = Cti.WebsocketMessageFactory.createDnd(state);

        Cti.dnd(state);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send dial message to websocket", function() {
        var number = "06324567";
        var message = Cti.WebsocketMessageFactory.createDial(number);

        Cti.dial(number);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send dial from mobile message to websocket", function() {
        var number = "775533";
        var message = Cti.WebsocketMessageFactory.createDialMobile(number);

        Cti.dialFromMobile(number);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send dial from queue message to websocket", function() {
        var destination = "123456";
        var queueId = 111;
        var callerIdName = "Jabberwocky";
        var callerIdNumber = "9988777";
        var variables = {"foo": "bar"};
        var message = Cti.WebsocketMessageFactory.createDialFromQueue(destination, queueId, callerIdName, callerIdNumber, variables);
        Cti.dialFromQueue(destination, queueId, callerIdName, callerIdNumber, variables);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("Hangup should send hangup message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createHangup();

        Cti.hangup();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("Hangup a specific call should send hangup message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createHangup("1616681250.0");

        Cti.hangup("1616681250.0");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("ToggleMicrophone should send toggleMicrophone message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createToggleMicrophone();

        Cti.toggleMicrophone();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("Answer should send answer message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createAnswer();

        Cti.answer();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("Hold should send hold message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createHold();

        Cti.hold();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send direct transfer message to websocket", function() {
        var destination = "23490";
        var message = Cti.WebsocketMessageFactory.createDirectTransfer(destination);

        Cti.directTransfer(destination);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send attended transfer message to websocket", function() {
        var destination = "78450";
        var message = Cti.WebsocketMessageFactory.createAttendedTransfer(destination);

        Cti.attendedTransfer(destination);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send optional source device info for attended transfer message to websocket", function() {
        var destination = "78450";
        var device = "MobileApp"
        var message = Cti.WebsocketMessageFactory.createAttendedTransfer(destination, device);

        Cti.attendedTransfer(destination, device);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });


    it("should send complete transfer message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createCompleteTransfer();

        Cti.completeTransfer();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send cancel transfer message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createCancelTransfer();

        Cti.cancelTransfer();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conference should send conference message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createConference();

        Cti.conference();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceInvite should send conference message to websocket", function() {
      const numConf = "4000";
      const exten = "0102030405";
      const role = "Organizer";
      const earlyJoin = true;
      const variables = {"someVariableA": "someValueA", "someVariableB": "someValueB"};
      var message = Cti.WebsocketMessageFactory.createConferenceInvite(numConf, exten, role, earlyJoin, variables);

      Cti.conferenceInvite(numConf, exten, role, earlyJoin, variables);
      expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceMuteMe should send conference message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createConferenceMuteMe("4000");

        Cti.conferenceMuteMe("4000");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceUnmuteMe should send conference message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createConferenceUnmuteMe("4000");

        Cti.conferenceUnmuteMe("4000");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceMute should send conference message to websocket", function() {
      var message = Cti.WebsocketMessageFactory.createConferenceMute("4000", 1);

      Cti.conferenceMute("4000", 1);
      expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceUnmute should send conference message to websocket", function() {
      var message = Cti.WebsocketMessageFactory.createConferenceUnmute("4000", 1);

      Cti.conferenceUnmute("4000", 1);
      expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceMuteAll should send conference message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createConferenceMuteAll("4000");

        Cti.conferenceMuteAll("4000");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceUnmuteAll should send conference message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createConferenceUnmuteAll("4000");

        Cti.conferenceUnmuteAll("4000");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceKick should send conference message to websocket", function() {
      var message = Cti.WebsocketMessageFactory.createConferenceKick("4000", 1);

      Cti.conferenceKick("4000", 1);
      expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceClose should send conference message to websocket", function() {
       var message = Cti.WebsocketMessageFactory.createConferenceClose("4000");

       Cti.conferenceClose("4000");
       expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
  
    it("conferenceDeafen should send conference message to websocket", function() {
      var message = Cti.WebsocketMessageFactory.createConferenceDeafen("4000", 1);

      Cti.conferenceDeafen("4000", 1);
      expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceUndeafen should send conference message to websocket", function() {
      var message = Cti.WebsocketMessageFactory.createConferenceUndeafen("4000", 1);

      Cti.conferenceUndeafen("4000", 1);
      expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conferenceReset should send conference message to websocket", function() {
      var message = Cti.WebsocketMessageFactory.createConferenceReset("4000");

      Cti.conferenceReset("4000", 1);
      expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });


    it("should send directory look up message to websocket", function() {
        var pattern = "zuc";
        var message = Cti.WebsocketMessageFactory.createDirectoryLookUp(pattern);

        Cti.directoryLookUp(pattern);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send get favorites message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetFavorites();

        Cti.getFavorites();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send add favorite message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createAddFavorite("123", "xivou");

        Cti.addFavorite("123", "xivou");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send remove favorites message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createRemoveFavorite("234", "other");

        Cti.removeFavorite("234", "other");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send invite to meetingroom message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createInviteToMeetingRoom(1, "some.random.token", "James Bond", "jbond");

        Cti.inviteToMeetingRoom(1, "some.random.token", "James Bond", "jbond");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send meetingroom invite ack message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createMeetingRoomInviteAck(1, "jbond", "ACK");

        Cti.meetingRoomInviteAck(1, "jbond", "ACK");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send meetingroom invite accept response to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createMeetingRoomInviteAccept(1, "jbond");

        Cti.meetingRoomInviteAccept(1, "jbond");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send meetingroom invite reject response to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createMeetingRoomInviteReject(1, "jbond");

        Cti.meetingRoomInviteReject(1, "jbond");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send SubscribeToQueueStats message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToQueueStats();

        Cti.subscribeToQueueStats();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send SubscribeToAgentStats message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToAgentStats();

        Cti.subscribeToAgentStats();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send getAgentStates message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetAgentStates();

        Cti.getAgentStates();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });


    it("should send getQueueStatistics message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetQueueStatistics("1",1800,30);

        Cti.getQueueStatistics("1",1800,30);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send subscribeToAgentEvents message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToAgentEvents();

        Cti.subscribeToAgentEvents();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send get config message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetConfig("queue");

        Cti.getConfig("queue");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send get agent directory message to websocket",function() {
       var message = Cti.WebsocketMessageFactory.createGetAgentDirectory();
       Cti.getAgentDirectory();
       expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send get queue list to web socket", function() {
       var msg = {"GetList" :"queue"};
       spyOn(Cti.WebsocketMessageFactory, 'createGetList').and.callFake(function(objectType) {return msg;});

       Cti.getList("queue");

       expect(Cti.WebsocketMessageFactory.createGetList).toHaveBeenCalledWith('queue');
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it("should send set agent queue message to web socket", function() {
       var msg = {"SetAgentQueue":1};
       var agentId = "37";
       var queueId = "56";
       var penalty = 3;
       spyOn(Cti.WebsocketMessageFactory, 'createSetAgentQueue').and.callFake(function(agentId, queueId, penalty) {return msg;});

       Cti.setAgentQueue(agentId, queueId, penalty);

       expect(Cti.WebsocketMessageFactory.createSetAgentQueue).toHaveBeenCalledWith(agentId, queueId, penalty);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it("should send remove agent from queue message to web socket", function() {
       var msg = {"RemoveAgentFromQueue":1};
       var agentId = "12";
       var queueId = "43";
       spyOn(Cti.WebsocketMessageFactory, 'createRemoveAgentFromQueue').and.callFake(function(agentId,queueId) {return msg;});

       Cti.removeAgentFromQueue(agentId,queueId);

       expect(Cti.WebsocketMessageFactory.createRemoveAgentFromQueue).toHaveBeenCalledWith(agentId,queueId);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it('should send move group of agents to web socket', function() {
       var msg = {"moveAgentsInGroup":1};
       var groupId = 72;
       var fromQueueId = 830, fromPenalty = 7;
       var toQueueId = 120, toPenalty = 2;

       var args = {};
       args.groupId = groupId;
       args.fromQueueId = fromQueueId;
       args.fromPenalty = fromPenalty;
       args.toQueueId = toQueueId;
       args.toPenalty = toPenalty;

       spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

       Cti.moveAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);

       expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('moveAgentsInGroup',args);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it('should send add group of agents to web socket', function() {
       var msg = {"addAgentsInGroup":1};
       var groupId = 72;
       var fromQueueId = 830, fromPenalty = 7;
       var toQueueId = 120, toPenalty = 2;

       var args = {};
       args.groupId = groupId;
       args.fromQueueId = fromQueueId;
       args.fromPenalty = fromPenalty;
       args.toQueueId = toQueueId;
       args.toPenalty = toPenalty;

       spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

       Cti.addAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);

       expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('addAgentsInGroup',args);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it('should send remove agents from queue group to web socket', function() {
       var msg = {"removeAgentGroupFromQueueGroup":1},
       groupId = 72,
       queueId = 830, penalty = 7;

       var args = {'groupId':groupId, 'queueId':queueId, 'penalty':penalty};

       spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

       Cti.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);

       expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('removeAgentGroupFromQueueGroup',args);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it('should send add agent not in queue from group to queue penalty to web socket', function() {
       var msg = {"addAgentsNotInQueueFromGroupTo":1},
       groupId = 64,
       queueId = 238, penalty = 4;

       var args = {'groupId':groupId, 'queueId':queueId, 'penalty':penalty};

       spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

       Cti.addAgentsNotInQueueFromGroupTo(groupId, queueId, penalty);

       expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('addAgentsNotInQueueFromGroupTo',args);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it("should send pause monitor with agent id message to websocket", function() {
        var msg = {"monitorPause":1}
        var agentId = 332;

        var args = {};
        args.agentid = agentId;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.monitorPause(agentId);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('monitorPause',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });
    it("should send unpause monitor with agent id message to websocket", function() {
        var msg = {"monitorUnpause":1}
        var agentId = 332;

        var args = {};
        args.agentid = agentId;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.monitorUnpause(agentId);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('monitorUnpause',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it("should send na forward to websocket", function(){
        var msg = {"naFwd":1}
        var destination = "3456";
        var state = true;

        var args = {};
        args.destination = destination;
        args.state = state;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.naFwd(destination,state);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('naFwd',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });

    it("should send unc forward to websocket", function(){
        var msg = {"uncFwd":1}
        var destination = "7789";
        var state = true;

        var args = {};
        args.destination = destination;
        args.state = state;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.uncFwd(destination,state);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('uncFwd',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it("should send busy forward to websocket", function(){
        var msg = {"busyFwd":1}
        var destination = "8899";
        var state = true;

        var args = {};
        args.destination = destination;
        args.state = state;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.busyFwd(destination,state);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('busyFwd',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });

    it("should close the websocket", function() {
        Cti.close();

        expect(testSocket.close).toHaveBeenCalled();
    });

    it("should send getConferenceRooms message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetList("meetme");

        Cti.getConferenceRooms();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send inviteConferenceRoom message to websocket", function() {
        var userId = 5;
        var message = Cti.WebsocketMessageFactory.createInviteConferenceRoom(userId);

        Cti.inviteConferenceRoom(userId);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send getAgentCallHistory message to websocket", function() {
        var msg = {"claz":"web","command":"getAgentCallHistory","size":7}
        var args = {};
        args.size = 7;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.getAgentCallHistory(7);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('getAgentCallHistory',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it("should send findCustomerCallHistory message to websocket with filter and request_id", function() {
        var filters = [{"field": "src_num", "operator": "=", "value": "1234"}];
        var size = 12;
        var args = {};
        args.id = 1;
        args.request = {"filters": filters, "size": size};
        var msg = {"id": args.id , "request": args.request}
        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.findCustomerCallHistory(1, filters, size);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('findCustomerCallHistory',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it("should send getUserCallHistory message to websocket", function() {
        var args = {};
        args.size = 12;
        var msg = {"size":args.size};
        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.getUserCallHistory(args.size);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('getUserCallHistory',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it("should send getUserCallHistoryByDays message to websocket", function() {
        var args = {};
        args.days = 12;
        var msg = {"days":args.days};
        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.getUserCallHistoryByDays(args.days);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('getUserCallHistoryByDays',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it("should send getQueueCallHistory message to websocket", function() {
        var args = {};
        args.size = 12;
        args.queue = "blu";
        var msg = {"claz":"web", "command":"getQueueCallHistory", "queueIds": args.queue, "size":args.size}
        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.getQueueCallHistory(args.queue, args.size);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('getQueueCallHistory',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send setData message to websocket', function() {
        var data = {"Data1":"Content", "Data2":"Other Content"};
        var msg = {"claz":"web", "command":"setData", "variables" : data};
        var args = {"variables": data}
        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.setData(data);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('setData',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });
    it('should send getCurrentCallsPhoneEvents message to websocket', function() {
        var msg = {"claz":"web", "command":"getCurrentCallsPhoneEvents"};

        Cti.getCurrentCallsPhoneEvents();

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });
    it('should send flashTextMessage  message to websocket', function() {
        var user = {"username": "user"};
        var msg = {"claz":"web", "command":"flashTextBrowserRequest", "request" : "FlashTextDirectMessage",
                    "sequence" : 24,
                    "to": user,
                    "message": "This is my message"};

        Cti.sendFlashTextMessage("user", 24, "This is my message");

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send sendDtmf message to websocket', function() {
        var msg = {"claz":"web", "command":"sendDtmf", "key": "2"};

        Cti.sendDtmf('2');

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send subscribeToPhoneHints message to websocket', function() {
        var msg = {"claz":"web", "command":"subscribeToPhoneHints",
                    "phoneNumbers": ["1005", "1099"]};

        Cti.subscribeToPhoneHints(["1005", "1099"]);

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send unsubscribeFromAllPhoneHints message to websocket', function() {
        var msg = {"claz":"web", "command":"unsubscribeFromAllPhoneHints"};

        Cti.unsubscribeFromAllPhoneHints();

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send subscribeToVideoStatus message to websocket', function() {
        var msg = {"claz":"web", "command":"subscribeToVideoStatus",
                    "usernames": ["ahonnet", "alder"]};

        Cti.subscribeToVideoStatus(["ahonnet", "alder"]);

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send unsubscribeFromAllVideoStatus message to websocket', function() {
        var msg = {"claz":"web", "command":"unsubscribeFromAllVideoStatus"};

        Cti.unsubscribeFromAllVideoStatus();

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });
    
    it('should send videoEvent message to websocket', function() {
        var msg = {"claz":"web", "command":"videoEvent", "status": "videoStart"};

        Cti.videoEvent("videoStart");

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send toggleUniqueAccountDevice message to websocket', function() {
        var msg = {"claz":"web", "command":"toggleUniqueAccountDevice", "deviceType": "phone"};

        Cti.toggleUniqueAccountDevice("phone");

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send displayNameLookup message to websocket', function() {
        var msg = {"claz":"web", "command":"displayNameLookup", "username": "jbond"};

        Cti.displayNameLookup("jbond");

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send addToServerLog message to websocket', function() {
        var msg = {"claz":"web", "command":"pushLogToServer", "level": "error", "log": "The squirrels are starving !"};

        Cti.pushLogToServer("error", "The squirrels are starving !");

        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send userPreference message to websocket', function() {
        var msg = {"claz":"web", "command":"setUserPreference", "key": "PREFERRED_DEVICE", "value": "webrtc", "value_type": "String"};
        Cti.setUserPreference("PREFERRED_DEVICE", "webrtc", "String");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it('should send unregisterMobileApp message to websocket', function() {
        var msg = {"claz":"web", "command":"unregisterMobileApp"};
        Cti.unregisterMobileApp();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });
});



var expectWebMessage = function(message, cmd) {
    expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
    expect(message.command).toBe(cmd);
};

describe("WebsocketMessageFactory agent queue", function() {
    it("should create set agent queue message", function() {
       var agentId = 12;
       var queueId = 23;
       var penalty = 9;

       var message = Cti.WebsocketMessageFactory.createSetAgentQueue(agentId, queueId, penalty);
       expectWebMessage(message,Cti.WebsocketMessageFactory.setAgentQueueCmd);
       expect(message.agentId).toBe(agentId);
       expect(message.queueId).toBe(queueId);
       expect(message.penalty).toBe(penalty);

    });
    it("should create remove agent from queue message", function() {
       var agentId = 10;
       var queueId = 20;

       var message = Cti.WebsocketMessageFactory.createRemoveAgentFromQueue(agentId,queueId);
       expectWebMessage(message,Cti.WebsocketMessageFactory.removeAgentFromQueueCmd);
       expect(message.agentId).toBe(agentId);
       expect(message.queueId).toBe(queueId);

    });
});

describe("WebsocketMessageFactory createAgentLogin function", function() {
    it("should create agent login message", function() {
        var agentPhoneNumber = 2002;
        var message = Cti.WebsocketMessageFactory.createAgentLogin(agentPhoneNumber);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.agentLoginCmd);
        expect(message.agentphonenumber).toBe(agentPhoneNumber);

    });
    it("should create agent login message with agentid", function() {
        var agentPhoneNumber = 2002;
        var agentId = 32;
        var message = Cti.WebsocketMessageFactory.createAgentLogin(agentPhoneNumber,agentId);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.agentLoginCmd);
        expect(message.agentphonenumber).toBe(agentPhoneNumber);
        expect(message.agentid).toBe(agentId);

    });
});

describe("WebsocketMessageFactory createAgentLogout function", function() {

    it("should create agent logout message", function() {
        var message = Cti.WebsocketMessageFactory.createAgentLogout();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.agentLogoutCmd);
    });
    it("should create agent logout message with agentId", function() {
        var agentId = 55;
        var message = Cti.WebsocketMessageFactory.createAgentLogout(agentId);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.agentLogoutCmd);
        expect(message.agentid).toBe(agentId);
    });
});
describe("WebsocketMessageFactory createPauseAgent function", function() {
    it("should create pause agent message", function() {
        var message = Cti.WebsocketMessageFactory.createPauseAgent();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.pauseAgentCmd);
    });
    it("should create pause agent message with agentid", function() {
        var agentId = 75;
        var message = Cti.WebsocketMessageFactory.createPauseAgent(agentId);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.pauseAgentCmd);
        expect(message.agentid).toBe(agentId);
    });
    it("should create pause agent message with agentid", function() {
        var agentId = 75;
        var message = Cti.WebsocketMessageFactory.createPauseAgent(agentId, 'test reason');
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.pauseAgentCmd);
        expect(message.agentid).toBe(agentId);
        expect(message.reason).toBe('test reason');
    });
});

describe("WebsocketMessageFactory createUnpauseAgent function", function() {
    it("should create unpause agent message", function() {
        var message = Cti.WebsocketMessageFactory.createUnpauseAgent();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.unpauseAgentCmd);
    });
    it("should create unpause agent message with agent id", function() {
        var agentId = 12;
        var message = Cti.WebsocketMessageFactory.createUnpauseAgent(agentId);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.unpauseAgentCmd);
        expect(message.agentid).toBe(agentId);
    });
});
describe("WebsocketMessageFactory createUserStatusUpdate function", function() {
    it("should create UserStatusUpdate message", function() {
        var message = Cti.WebsocketMessageFactory.createUserStatusUpdate("available");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.userStatusUpdateCmd);
        expect(message.status).toBe("available");
    });
});


describe("WebsocketMessageFactory createDnd function", function() {
    it("should create dnd message", function() {
        var message = Cti.WebsocketMessageFactory.createDnd(true);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.dndCmd);
        expect(message.state).toBe(true);
    });
});

describe("WebsocketMessageFactory createDial function", function() {
    it("should create dial message", function() {
        var number = "1234"
        var message = Cti.WebsocketMessageFactory.createDial(number);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.dialCmd);
        expect(message.destination).toBe(number);
    });
});

describe("WebsocketMessageFactory createDialMobile function", function() {
    it("should create dial from mobile message", function() {
        var number = "445566"
        var message = Cti.WebsocketMessageFactory.createDialMobile(number);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.dialFromMobileCmd);
        expect(message.destination).toBe(number);
    });
});

describe("WebsocketMessageFactory createDialByUsername function", function() {
    it("should create dial by username message", function() {
        var username = "jbond"
        var message = Cti.WebsocketMessageFactory.createDialByUsername(username);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.dialByUsernameCmd);
        expect(message.username).toBe(username);
    });
});

describe("WebsocketMessageFactory createDialFromQueue function", function() {
    it("should create dial from queue message", function() {
        var destination = "123456";
        var queueId = 111;
        var callerIdName = "Jabberwocky";
        var callerIdNumber = "9988777";
        var variables = {"foo": "bar"};
        var message = Cti.WebsocketMessageFactory.createDialFromQueue(destination, queueId, callerIdName, callerIdNumber, variables);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.dialFromQueueCmd);
        expect(message.destination).toBe(destination);
        expect(message.queueId).toBe(queueId);
        expect(message.callerIdName).toBe(callerIdName);
        expect(message.callerIdNumber).toBe(callerIdNumber);
        expect(message.variables).toBe(variables);
    });
});

describe("WebsocketMessageFactory createHangup function", function() {
    it("should create hangup message", function() {

        var message = Cti.WebsocketMessageFactory.createHangup();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.hangupCmd);
    });

    it("should create hangup message for a specific uniqueId", function() {

        var message = Cti.WebsocketMessageFactory.createHangup("1616681250.0");

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.hangupCmd);
        expect(message.uniqueId).toBe("1616681250.0");
    });
});

describe("WebsocketMessageFactory createToggleMicrophone function", function() {
    it("should create toggleMicrophone message", function() {

        var message = Cti.WebsocketMessageFactory.createToggleMicrophone();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.toggleMicrophoneCmd);
    });

    it("should create createToggleMicrophone message for a specific uniqueId", function() {

        var message = Cti.WebsocketMessageFactory.createToggleMicrophone("1616681250.0");

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.toggleMicrophoneCmd);
        expect(message.uniqueId).toBe("1616681250.0");
    });
});

describe("WebsocketMessageFactory createHold function", function() {
    it("should create hold message", function() {

        var message = Cti.WebsocketMessageFactory.createHold();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.holdCmd);
    });

    it("should create hold message for a specific uniqueId", function() {

        var message = Cti.WebsocketMessageFactory.createHold("1616681250.0");

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.holdCmd);
        expect(message.uniqueId).toBe("1616681250.0");
    });
});

describe("WebsocketMessageFactory createAnswer function", function() {
    it("should create answer message", function() {

        var message = Cti.WebsocketMessageFactory.createAnswer();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.answerCmd);
    });

    it("should create answer message for a specific uniqueId", function() {

        var message = Cti.WebsocketMessageFactory.createAnswer("1616681250.0");

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.answerCmd);
        expect(message.uniqueId).toBe("1616681250.0");
    });
});

describe("WebsocketMessageFactory createDirectTransfer function", function() {
    it("should create direct transfer message", function() {
        var message = Cti.WebsocketMessageFactory.createDirectTransfer("1234");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.directTransferCmd);
        expect(message.destination).toBe("1234");
    });
});

describe("WebsocketMessageFactory createAttendedTransfer function", function() {
    it("should create attended transfer message", function() {
        var message = Cti.WebsocketMessageFactory.createAttendedTransfer("7654");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.attendedTransferCmd);
        expect(message.destination).toBe("7654");
    });
});

describe("WebsocketMessageFactory createCompleteTransfer function", function() {
    it("should create complete transfer message", function() {

        var message = Cti.WebsocketMessageFactory.createCompleteTransfer();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.completeTransferCmd);
    });
});

describe("WebsocketMessageFactory createCancelTransfer function", function() {
    it("should create cancel transfer message", function() {

        var message = Cti.WebsocketMessageFactory.createCancelTransfer();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.cancelTransferCmd);
    });
});

describe("WebsocketMessageFactory createConference function", function() {
    it("should create conference message", function() {

        var message = Cti.WebsocketMessageFactory.createConference();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.conferenceCmd);
    });
});

describe("WebsocketMessageFactory createDirectoryLookUp function", function() {
    it("should create directory look up message", function() {
        var message = Cti.WebsocketMessageFactory.createDirectoryLookUp("abc");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.directoryLookUpCmd);
        expect(message.term).toBe("abc");
    });
});

describe("WebsocketMessageFactory getFavorites function", function() {
    it("should create get favorites message", function() {
        var message = Cti.WebsocketMessageFactory.createGetFavorites();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getFavoritesCmd);
    });
});

describe("WebsocketMessageFactory inviteToMeetingRoom function", function() {
    it("should create invite to meetingroom message", function() {
        var message = Cti.WebsocketMessageFactory.createInviteToMeetingRoom(1, "some.random.token", "jbond");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.inviteToMeetingRoomCmd);
        expect(message.requestId).toBe(1);
        expect(message.token).toBe("some.random.token");
        expect(message.username).toBe("jbond");
    });
});

describe("WebsocketMessageFactory meetingRoomInviteAck function", function() {
    it("should create meetingroom invite ack message", function() {
        var message = Cti.WebsocketMessageFactory.createMeetingRoomInviteAck(1, "jbond");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.meetingRoomInviteAckCmd);
        expect(message.requestId).toBe(1);
        expect(message.username).toBe("jbond");
    });
});

describe("WebsocketMessageFactory meetingRoomInviteAccept function", function() {
    it("should create meetingroom invite accept message", function() {
        var message = Cti.WebsocketMessageFactory.createMeetingRoomInviteAccept(1, "jbond");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.meetingRoomInviteAcceptCmd);
        expect(message.requestId).toBe(1);
        expect(message.username).toBe("jbond");
    });
});

describe("WebsocketMessageFactory meetingRoomInviteReject function", function() {
    it("should create meetingroom invite reject message", function() {
        var message = Cti.WebsocketMessageFactory.createMeetingRoomInviteReject(1, "jbond");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.meetingRoomInviteRejectCmd);
        expect(message.requestId).toBe(1);
        expect(message.username).toBe("jbond");
    });
});

describe("WebsocketMessageFactory addFavorite function", function() {
    it("should create set favorite message", function() {
        var message = Cti.WebsocketMessageFactory.createAddFavorite("123", "xivou");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.addFavoriteCmd);
    });
});

describe("WebsocketMessageFactory removeFavorites function", function() {
    it("should create remove favorites message", function() {
        var message = Cti.WebsocketMessageFactory.createRemoveFavorite("234", "other");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.removeFavoriteCmd);
    });
});

describe("WebsocketMessageFactory SubscribeToQueueStats function", function() {
    it("should create SubscribeToQueueStats message", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToQueueStats();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.subscribeToQueueStatsCmd);
    });
});

describe("WebsocketMessageFactory SubscribeToAgentStats function", function() {
    it("should create SubscribeToAgentStats message", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToAgentStats();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.subscribeToAgentStatsCmd);
    });
});

describe("WebsocketMessageFactory GetAgentStates function", function() {
    it("should create getAgentStates message", function() {
        var message = Cti.WebsocketMessageFactory.createGetAgentStates();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getAgentStatesCmd);
    });
});

describe("WebsocketMessageFactory createGetQueueStatistics function", function() {
    it("should create createGetQueueStatistics message", function() {
        var message = Cti.WebsocketMessageFactory.createGetQueueStatistics("34",1800,30);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getQueueStatisticsCmd);
        expect(message.queueId).toBe("34");
        expect(message.window).toBe(1800);
        expect(message.xqos).toBe(30);
    });
});

describe("WebsocketMessageFactory SubscribeToAgentEvents function", function() {
    it("should create SubscribeToAgentEvents message", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToAgentEvents();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.subscribeToAgentEventsCmd);
    });
});

describe("WebsocketMessageFactory createGetConfig function", function() {
    it("should create getConfig message", function() {
        var message = Cti.WebsocketMessageFactory.createGetConfig("queue");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getConfigCmd);
        expect(message.objectType).toBe("queue");
    });
});
describe("WebsocketMessageFactory createGetList function", function() {
    it("should create getList message", function() {
        var message = Cti.WebsocketMessageFactory.createGetList("queue");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getListCmd);
        expect(message.objectType).toBe("queue");
    });
});

describe("WebsocketMessageFactory createGetAgentDirectory function", function() {
        it("should create GetAgentDirectory message", function() {
            var message = Cti.WebsocketMessageFactory.createGetAgentDirectory();
            expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
            expect(message.command).toBe(Cti.WebsocketMessageFactory.getAgentDirectoryCmd);
        });
});
describe("WebsocketMessageFactory createPing function", function() {
    it("should create ping message", function() {
        var message = Cti.WebsocketMessageFactory.createPing();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.pingClaz);
    });
});
describe("WebsocketMessageFactory createFrom args", function() {
    it("should create web message with arguments", function() {
        var args ={'one':32, 'two':'hello'};

        var message = Cti.WebsocketMessageFactory.createMessageFromArgs('messageCommand', args);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.one).toBe(32);
        expect(message.two).toBe('hello');
    });
});
