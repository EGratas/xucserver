describe("xc_webrtc ", function() {

    var autoAnswerHeaders = [ {s_name: 'Alert-Info', s_value: 'xivo-autoanswer'} ];
    var defaultLineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};

    var lineHandler = null;
    var iceHandler = null;
    var ctiCmdHandler = null;
    var generalEventListener;
    var sessionEventListener;
    var sipReadyCallback;
    var sessionType;
    var sessionConfig;
    var StreamUpdateHandler;
    var audioContext = {
      createMediaStreamSource: () => { return true}
    }
    var session = {
        register: function() {},
        unregister: function() {},
        getId: function() { return id; },
        call: function(destination) {return 0; },
        setConfiguration: function(config) { sessionConfig = config; },
        addEventListener: function(type, listener) {},
        removeEventListener: function(stype) {}
    };
    var sipCallId = "456546-df456@192.168";

    var stackConfig = {};

    var callSessions = [];

    var dictTracks = {};
  
    var stack = {
        newSession: function(type, config) {
            sessionType = type;
            sessionConfig = config;
            displayName = 'username';
            id = 'outSessionId';
            if (type === 'call-audiovideo') {
                session.o_session = {get_stream_local: function() {return 0; }, 
                media: {e_type: {s_name: xc_webrtc.mediaType.AUDIOVIDEO }}}
            }
            else {
              session.o_session = {get_stream_local: function() {return 0; }, 
                media: {e_type: {s_name: xc_webrtc.mediaType.AUDIO }}};
            }
            return session;
        },
        start: function() {},
        stop: function() { return 0; },
        removeEventListener: function(stype) {},
        setConfiguration: function(newConfig) {
          console.error("set new config");
          stackConfig = Object.assign({}, newConfig);
        },
    };

    function StackMock(config) {
      generalEventListener = config.events_listener.listener;
      stackConfig = Object.assign({}, config);
      return stack;
    };

    var cti = {
        setHandler: function(msgType, handler) {
            switch(msgType) {
              case Cti.MessageType.LINECONFIG: lineHandler = handler;
              case Cti.MessageType.ICECONFIG: iceHandler = handler;
              case Cti.MessageType.WEBRTCCMD: ctiCmdHandler = handler;
            }
        },
        unsetHandler: function(msgType) {
            switch(msgType) {
              case Cti.MessageType.LINECONFIG: lineHandler = null;
              case Cti.MessageType.ICECONFIG: iceHandler = null;
              case Cti.MessageType.WEBRTCCMD: ctiCmdHandler = null;
            }
        },
        getConfig: function() {},
        getIceConfig: function() {},
        attendedTransfer: jasmine.createSpy(),
        completeTransfer: jasmine.createSpy()
    };

    var sipml = {
        init: function(readyCB, failedCB) {
            sipReadyCallback = readyCB;
        },
        isInitialized: function() { return true; },
        Stack: StackMock,
        setDebugLevel: function() {},
    };

    function legacyInitStack() {
        xc_webrtc.init('username', true, 8090, 'token');
    };

    function initStackByLineConfig() {
        return xc_webrtc.initByLineConfig(defaultLineCfg, 'name', true, 8090, 'token');
    };

    function allowStartStack(iceCfg = {}) {
        sipReadyCallback();
        iceHandler(iceCfg);
    };

    function initIncomingSession(headers, media) {
        var callback = {
            newCall: jasmine.createSpy()
        };
        xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, callback.newCall);
        return getEvent(headers, 13, media);
    };

    function initNextIncomingSession(headers, id, scId) {
        return getEvent(headers, id, undefined, scId);
    };

    function getEvent(headers, id, media, scId = sipCallId) {
        var mediaValue = ((typeof media === 'undefined') ? 'audio' : media);
        var event = {
            type: 'i_new_call',
            o_event: {
                o_session: {o_uri_from: {s_display_name: 'username'}},
                o_message: {ao_headers: headers, o_hdr_Call_ID: {s_value: scId}}
            },
            newSession: {
                accept: function(callConfig) {
                    sessionEventListener = callConfig.events_listener.listener;
                    return 0;
                },
                hold: function() {
                  this.o_session.get_stream_local = function () { return undefined };
                  return 0;
                },
                holdBySipCallId: function() { return 0; },
                resume: function() {
                  this.o_session = createO_Session(id, mediaValue);
                  return 0;
                },
                id: id,
                getId: function() { return this.id; },
                dtmf: function() {return 0; },
                addEventListener: jasmine.createSpy(),
                setConfiguration: jasmine.createSpy(),
                o_session: createO_Session(id, mediaValue),
                getTracks: () => {
                  return {local: {id: 1, type: 'audio'}, 
                  remote: {id: 2, type: 'audio'}}
                },
                injectStream: () => {return true}
            }
        };
        if(headers == autoAnswerHeaders) {
          insertCallSessionsMock(event, scId);
        }
        spyOn(event.newSession, 'hold').and.callThrough();
        spyOn(event.newSession, 'holdBySipCallId').and.callThrough();
        spyOn(event.newSession, 'resume').and.callThrough();
        spyOn(event.newSession, 'dtmf').and.callThrough();
        return event;
    };

    getProgressEvent = function(id, media) {
        var event = getEvent([], 'outSessionId', 'audio/video');
        event.type = 'i_ao_request';
        event.session = event.newSession;
        event.getSipResponseCode = function() { return 183; };
        event.o_event.o_session.o_uri_from = { s_diplay_name: 'test', s_user_name: 'test'};
        return event;
    };

    function getConnectedEvent(id, media) {
        return {
            type: 'connected',
            session: getEvent([], id, media).newSession,
            o_event: {
                o_session: {
                    o_uri_to: {s_user_name: 'to_name'},
                    o_uri_from: {s_user_name: 'from_name'}
                },
                o_message: {o_hdr_Call_ID: {s_value: sipCallId}}
            }
        };
    };

    function getStreamAudioRemoteEvent(id, media) {
      return {
          type: 'm_stream_audio_remote_added',
          session: getEvent([], id, media).newSession,
          o_event: {
              o_session: {
                  o_uri_to: {s_user_name: 'to_name'},
                  o_uri_from: {s_user_name: 'from_name'}
              },
              o_message: {o_hdr_Call_ID: {s_value: sipCallId}}
          }
      };
    };

    function getResumedEvent(id) {
        var sid; id ? sid = id: sid = 'outSessionId';
        var event = getEvent([], sid);
        event.type = 'm_local_resume_ok';
        event.session = event.newSession;
        return event;
    };

    function getHoldedEvent(id) {
        var sid; id ? sid = id: sid = 'outSessionId';
        var event = getEvent([], sid);
        event.type = 'm_local_hold_ok';
        event.session = event.newSession;
        return event;
    };

    function getTerminatedEvent(id) {
        var terminatedEvent = getEvent([], id);
        terminatedEvent.type = 'terminated';
        terminatedEvent.session = terminatedEvent.newSession;
        callSessions.pop();
        dictTracks[id] = undefined;
        return terminatedEvent;
    };

    function initFirstCall(media, id) {
        var firstCallEvent = initIncomingSession(autoAnswerHeaders, media);
        spyOn(firstCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(firstCallEvent);
        expect(firstCallEvent.newSession.setConfiguration).toHaveBeenCalled();
        expect(firstCallEvent.newSession.accept).toHaveBeenCalled();
        return firstCallEvent.newSession;
    };

    function sendConnectedEvent(id, media) {
        sessionEventListener(getConnectedEvent(id, media));
    };

    function insertCallSessionsMock(event, sipCallId) {
      callSessions.push({
        id: event.newSession.getId(),
        session: event.newSession,
        sipCallId: sipCallId,
        audioLocalSource: null,
        audioRemoteSource: null
      });
    }

    function createO_Session(_id, _mediaValue) {
      return {
        media: {e_type: {s_name: _mediaValue }},
        get_stream_remote: () => {return {}},
        get_stream_local: () => {
          return {
            getTracks: () => {
              for (let key in dictTracks) {
                if (key == _id) {
                  return dictTracks[_id];
                }
              }
              dictTracks[_id] = [{enabled: true}];
              return dictTracks[_id];
            }
          }
        }
      };
    }

    beforeEach(function() {
        jasmine.clock().install();
        xc_webrtc.injectTestDependencies(cti, sipml);
        spyOn(cti, 'setHandler').and.callThrough();
        spyOn(cti, 'getConfig');
        spyOn(cti, 'getIceConfig');
        spyOn(sipml, 'init').and.callThrough();
        spyOn(stack, 'start');
        spyOn(stack, 'stop').and.callThrough();
        spyOn(stack, 'newSession').and.callThrough();
        spyOn(stack, 'removeEventListener');
        spyOn(stack, 'setConfiguration').and.callThrough();
        spyOn(session, 'register');
        spyOn(session, 'unregister');
        spyOn(session, 'removeEventListener');
        spyOn(xc_webrtc, 'getAudioContext').and.returnValue(audioContext);
        spyOn(xc_webrtc, 'createAndGetMixedTrack').and.callFake(() => {});
    });

    afterEach(function() {
        jasmine.clock().uninstall();
        callSessions = [];
        dictTracks = [];
    });

    it('should request LineConfig (legacy init)', function () {
        legacyInitStack();
        expect(cti.setHandler.calls.count()).toEqual(1);
        expect(cti.setHandler.calls.argsFor(0)[0]).toEqual(Cti.MessageType.LINECONFIG);
        expect(cti.getConfig).toHaveBeenCalledWith('line');
    });

    it('should init stack on LineConfig event when received and request IceConfig (legacy init)', function () {
        legacyInitStack();
        lineHandler(defaultLineCfg);
        expect(cti.getIceConfig).toHaveBeenCalled();
        expect(sipml.init).toHaveBeenCalled();
    });

    it('should init by LineConfig and request IceConfig', function() {
        expect(xc_webrtc.getSipStackStatus()).toBe('NotInitialized');
        initStackByLineConfig();
        expect(xc_webrtc.getSipStackStatus()).toBe('Initializing');
        expect(cti.setHandler.calls.count()).toEqual(2);
        expect(cti.setHandler.calls.argsFor(0)[0]).toEqual(Cti.MessageType.LINECONFIG);
        expect(cti.setHandler.calls.argsFor(1)[0]).toEqual(Cti.MessageType.ICECONFIG);
        expect(cti.getIceConfig).toHaveBeenCalled();
        expect(sipml.init).toHaveBeenCalled();
    });

  it('should set user agent upon stack creation', function(done) {
    let appVersion = '2020.07.00-deadbeef';
    window.appVersion = appVersion;

    let promise = initStackByLineConfig();
    sipReadyCallback();
    promise.then(() => {
      expect(stackConfig.sip_headers).toContain({ name: 'User-Agent', value: 'XiVO XC WebRTC ' + appVersion });
      done();
    });      
  });

  it('should not set user agent when updating ice config', function(done) {
    let appVersion = '2020.07.00-deadbeef';
    let iceCfg = {turnConfig:{urls: ["turn:host:3478","turns:host:3478"], username:"2002:84600",credential:"pwd",ttl:10}};
    window.appVersion = appVersion;

    let promise = initStackByLineConfig();
    sipReadyCallback();
    promise.then(() => {
      iceHandler(iceCfg);
      expect(stackConfig.sip_headers).toBeUndefined();
      done();
    });      
  });

    it('should init sipml only once', function () {
      initStackByLineConfig();
      expect(sipml.init).toHaveBeenCalled();
      sipml.init.calls.reset();
      initStackByLineConfig();
      expect(sipml.init).not.toHaveBeenCalled();
    });

    it('should start stack when IceConfig received after sip ready callback', function (done) {

      expect(xc_webrtc.getSipStackStatus()).toBe('NotInitialized');
      let promise = initStackByLineConfig();
      expect(xc_webrtc.getSipStackStatus()).toBe('Initializing');
      expect(sipml.init).toHaveBeenCalled();
      expect(cti.getIceConfig).toHaveBeenCalled();

      sipReadyCallback();
      expect(xc_webrtc.getSipStackStatus()).toBe('WaitingForIce');
      expect(stack.start).not.toHaveBeenCalled();

      iceHandler({});

      promise.then(() => {
        expect(xc_webrtc.getSipStackStatus()).toBe('Starting');
        expect(cti.setHandler.calls.argsFor(0)[0]).toEqual(Cti.MessageType.LINECONFIG);
        expect(cti.setHandler.calls.argsFor(1)[0]).toEqual(Cti.MessageType.ICECONFIG);
        expect(cti.setHandler.calls.argsFor(2)[0]).toEqual(Cti.MessageType.WEBRTCCMD);
        expect(stack.start).toHaveBeenCalled();
        done();
      });
    });

    it('should start stack when IceConfig received before sip ready callback', (done) => {
      expect(xc_webrtc.getSipStackStatus()).toBe('NotInitialized');
      let promise = initStackByLineConfig();

      iceHandler({});
      expect(xc_webrtc.getSipStackStatus()).toBe('WaitingForSip');
      expect(stack.start).not.toHaveBeenCalled();

      sipReadyCallback();

      promise.then(() => {
        expect(xc_webrtc.getSipStackStatus()).toBe('Starting');
        expect(cti.setHandler.calls.argsFor(0)[0]).toEqual(Cti.MessageType.LINECONFIG);
        expect(cti.setHandler.calls.argsFor(1)[0]).toEqual(Cti.MessageType.ICECONFIG);
        expect(cti.setHandler.calls.argsFor(2)[0]).toEqual(Cti.MessageType.WEBRTCCMD);
        expect(stack.start).toHaveBeenCalled();
        done();
      });
    });

    it('should not restart stack if initStackByLineConfig is invoked two times in a row', function (done) {
      let promise = initStackByLineConfig();
      allowStartStack();

      promise.then(() => {
        sipml.init.calls.reset();
        cti.getIceConfig.calls.reset();
        stack.start.calls.reset();
        initStackByLineConfig();
        expect(sipml.init).not.toHaveBeenCalled();
        expect(cti.getIceConfig).not.toHaveBeenCalled();
        expect(stack.start).not.toHaveBeenCalled();
        done();
      });
    });

    it('should not restart stack when LineConfig is received and stack is not fully started', function (done) {
      let promise = initStackByLineConfig();
      allowStartStack();

      promise.then(() => {
        expect(stack.start).toHaveBeenCalled();
        stack.start.calls.reset();
        expect(xc_webrtc.getSipStackStatus()).toBe('Starting');
        legacyInitStack();
        lineHandler(defaultLineCfg);
        expect(stack.start).not.toHaveBeenCalled();
        expect(stack.setConfiguration).not.toHaveBeenCalled();
        done();
      });
    });

    it('should not restart stack when LineConfig is received and stack is started', function (done) {
      let promise = initStackByLineConfig();
      allowStartStack();

      promise.then(() => {
        expect(stack.start).toHaveBeenCalled();
        stack.start.calls.reset();
        startedEvent = {type: 'started'};
        generalEventListener(startedEvent);
        expect(xc_webrtc.getSipStackStatus()).toBe('Started');
        legacyInitStack();
        lineHandler(defaultLineCfg);
        expect(stack.start).not.toHaveBeenCalled();
        expect(stack.setConfiguration).not.toHaveBeenCalled();
        done();
      });
    });

    it('should restart stack when LineConfig is received and stack is stopped', function (done) {
      let promise = initStackByLineConfig();
      allowStartStack();

      promise.then(() => {
        expect(stack.start).toHaveBeenCalled();
        stack.start.calls.reset();
        startedEvent = {type: 'started'};
        generalEventListener(startedEvent);
        expect(xc_webrtc.getSipStackStatus()).toBe('Started');
        xc_webrtc.stop();
        expect(xc_webrtc.getSipStackStatus()).toBe('Stopped');
        expect(stack.stop).toHaveBeenCalledWith();
        legacyInitStack();
        lineHandler(defaultLineCfg);
        expect(stack.start).toHaveBeenCalled();
        expect(xc_webrtc.getSipStackStatus()).toBe('Starting');
        done();
      });
    });

    it('should update stack configuration when IceConfig is received after turn ttl', function (done) {
      var iceCfg = {turnConfig:{urls: ["turn:host:3478","turns:host:3478"], username:"2002:84600",credential:"pwd",ttl:10}};
      let promise = initStackByLineConfig();

      sipReadyCallback();
      iceHandler(iceCfg);
      expect(stack.setConfiguration).not.toHaveBeenCalled();

      promise.then(() => {
        jasmine.clock().tick(5000);
        iceHandler(iceCfg);
        expect(stack.setConfiguration).toHaveBeenCalled();
        done();
      });
    });

    it('should request register when correctly initiated', function (done) {
      let promise = initStackByLineConfig();
      expect(cti.getIceConfig).toHaveBeenCalled();
      expect(sipml.init).toHaveBeenCalled();
      allowStartStack();

      promise.then(() => {
        expect(xc_webrtc.getSipStackStatus()).toBe('Starting');
        expect(stack.start).toHaveBeenCalled();
        startedEvent = {type: 'started'};
        generalEventListener(startedEvent);
        expect(xc_webrtc.getSipStackStatus()).toBe('Started');
        expect(stack.newSession).toHaveBeenCalled();
        expect(sessionType).toBe('register');
        expect(session.register).toHaveBeenCalled();
        done();
      });
    });

    it('should request retry register when unregistered immediately and wait before next try', function () {
        startedEvent = {type: 'started'};
        generalEventListener(startedEvent);
        expect(stack.newSession).toHaveBeenCalled();
        sessionConfig.events_listener.listener({type: 'terminated'});
        jasmine.clock().tick(1);
        expect(stack.newSession.calls.count()).toEqual(2);
        sessionConfig.events_listener.listener({type: 'terminated'});
        jasmine.clock().tick(xc_webrtc.getRegisterTimeoutStep() * 1000 + 1);
        expect(stack.newSession.calls.count()).toEqual(3);
        expect(sessionType).toBe('register');
    });

    it('initByLineConfig throws exception if lineCfg.password is not set', function () {
        var lineCfg = {name: "lineName", xivoIp: "xivoIP"};
        expect( function() {
            xc_webrtc.initByLineConfig(lineCfg, 'username', true, 8090,'token');
        } ).toThrow(new Error("Unable to configure WebRTC - LineConfig does not contains password"));
    });

    it('initByLineConfig should set correct wsserver if sipProxyName is not defined ', function (done) {
      let promise = xc_webrtc.initByLineConfig(defaultLineCfg, 'username', true, 443, 'token', "audio_remote", "nginxsrv");
      allowStartStack();
      promise.then(() => {
        expect(stackConfig.websocket_proxy_url).toBe("wss://nginxsrv:443/wssip?token=token");
        done();
      });
    });

    it('initByLineConfig should set correct wsserver when line is on main', function (done) {
      var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP", sipProxyName: "default"};
      let promise = xc_webrtc.initByLineConfig(lineCfg, 'username', true, 443, 'token', "audio_remote", "nginxsrv");
      allowStartStack();
      promise.then(() => {
        expect(stackConfig.websocket_proxy_url).toBe("wss://nginxsrv:443/wssip?token=token");
        done();
      });
    });

    it('initByLineConfig should set correct wsserver when line is on mds', function (done) {
      var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP", sipProxyName: "mds1"};
      let promise =  xc_webrtc.initByLineConfig(lineCfg, 'username', true, 443, 'token', "audio_remote", "nginxsrv");
      allowStartStack();
      promise.then(() => {
        expect(stackConfig.websocket_proxy_url).toBe("wss://nginxsrv:443/wssip-mds1?token=token");
        expect(stackConfig.realm).toBe("mds1");
        done();
      });
    });

    it('stop should remove event callbacks and stop sip stack, session will be closed by framework', function (done) {
        let promise = initStackByLineConfig();
        allowStartStack();

        promise.then(() => {
          xc_webrtc.stop();
          expect(session.removeEventListener).toHaveBeenCalledWith('*');
          expect(stack.removeEventListener).toHaveBeenCalledWith('*');
          expect(stack.stop).toHaveBeenCalledWith();
          done();
        });
    });

    it('should not call hold/resume when the call is terminated', function() {
        var new_call_event = initIncomingSession([]);
        spyOn(new_call_event.newSession, 'accept').and.callThrough();
        generalEventListener(new_call_event );
        expect(xc_webrtc.answer()).toBe(true);
        expect(new_call_event.newSession.accept).toHaveBeenCalled();
        expect(xc_webrtc.hold()).toBe(true);
        expect(new_call_event.newSession.hold).toHaveBeenCalled();
        new_call_event.newSession.hold.calls.reset();
        var terminatedEvent = getTerminatedEvent(new_call_event.newSession.id);
        sessionEventListener(terminatedEvent);
        expect(xc_webrtc.hold()).toBe(false);
        expect(terminatedEvent.session.hold).not.toHaveBeenCalled();
        expect(terminatedEvent.session.resume).not.toHaveBeenCalled();
    });

    it('without initialized callSession, answer() returns false', function() {
        expect(xc_webrtc.answer()).toBe(false);
    });

    it('doesn\'t auto-answer the call if the autoAnswer header is NOT present', function() {
        var session = initIncomingSession([]);
        spyOn(session.newSession, 'accept').and.callThrough();
        generalEventListener(session);
        expect(session.newSession.accept).not.toHaveBeenCalled();
    });

    it('auto-answers the call if the autoAnswer header is present', function() {
        var newCallEvent = initIncomingSession(autoAnswerHeaders);
        spyOn(newCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(newCallEvent);
        expect(newCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('put second call on hold when auto answering a new call', function() {
        var firstCallSession = initFirstCall();
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(firstCallSession.hold).toHaveBeenCalled();
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('put second call on hold when answering without id', function() {
        var firstCallSession = initFirstCall();
        sendConnectedEvent(firstCallSession.id);

        var secondCallEvent = initNextIncomingSession([], 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(xc_webrtc.answer()).toBe(true);
        expect(firstCallSession.hold).toHaveBeenCalled();
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('answer call by id', function() {
        initFirstCall();
        var secondCallEvent = initNextIncomingSession([], 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(xc_webrtc.answer(14)).toBe(true);
        expect(secondCallEvent.newSession.setConfiguration).toHaveBeenCalled();
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('answering an unexisting id returns false', function() {
        initFirstCall();
        var secondCallEvent = initNextIncomingSession([], 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(xc_webrtc.answer(15)).toBe(false);
        expect(xc_webrtc.answer(14)).toBe(true);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('hold call by session id', function() {
        initFirstCall()
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        expect(xc_webrtc.hold(14)).toBe(true);
        expect(secondCallEvent.newSession.hold).toHaveBeenCalled();
    });

    it('hold call by sip call id', function() {
        const scId = "123123-df456@192.168";

        initFirstCall()
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14, scId);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        expect(xc_webrtc.holdBySipCallId(scId)).toBe(true);
        expect(secondCallEvent.newSession.hold).toHaveBeenCalled();
    });

    it('discard hold id sip call id is not known', function() {
        const scId = "123123-df456@192.168";

        initFirstCall()
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14, scId);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        expect(xc_webrtc.holdBySipCallId("fakeSipCallId")).toBe(false);
        expect(secondCallEvent.newSession.hold).not.toHaveBeenCalled();
    });

    it('put current call on hold when initiating an attended transfer', function() {
        var firstCallSession = initFirstCall();
        sendConnectedEvent(firstCallSession.id);

        xc_webrtc.attendedTransfer('2002');
        expect(firstCallSession.hold).toHaveBeenCalled();
        expect(cti.attendedTransfer).toHaveBeenCalled();
    });

    it('do not put on hold if already have two callSession', function() {
        var firstCallSession = initFirstCall();
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        sendConnectedEvent(firstCallSession.id);
        sendConnectedEvent(secondCallEvent.newSession.id);

        xc_webrtc.attendedTransfer('2002');
        expect(secondCallEvent.newSession.hold).not.toHaveBeenCalled();
    });

    it('proxy completeTransfer command to cti', function() {
        xc_webrtc.completeTransfer();
        expect(cti.completeTransfer).toHaveBeenCalled();
    });

    it('on hangup remove correct call from its internal state', function() {
        var firstCallEvent = initFirstCall();
        sendConnectedEvent(firstCallEvent.id);

        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        var terminatedEvent = getTerminatedEvent(14);
        sessionEventListener(terminatedEvent);

        expect(xc_webrtc.hold(14)).toBe(false);
        expect(xc_webrtc.hold()).toBe(true);
    });

    it('refuses to send dtmf when there\'s no established call', function() {
        var firstCallEvent = initFirstCall();

        expect(xc_webrtc.dtmf('1')).toBe(false);
    });

    it('refuses to send dtmf when more than one character is passed', function() {
        var firstCallSession = initFirstCall();
        sendConnectedEvent(firstCallSession.id);

        expect(xc_webrtc.dtmf('11')).toBe(false);
        expect(xc_webrtc.dtmf('1')).toBe(true);
        expect(firstCallSession.dtmf).toHaveBeenCalledWith('1')
    });

    it('sends dtmf on all active calls', function() {
        var firstCallSession = initFirstCall();
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        var thirdCallEvent = initNextIncomingSession([], 15);
        spyOn(thirdCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(thirdCallEvent);

        sendConnectedEvent(firstCallSession.id);
        sendConnectedEvent(secondCallEvent.newSession.id);

        expect(xc_webrtc.dtmf('5')).toBe(true);
        expect(firstCallSession.dtmf).toHaveBeenCalled();
        expect(secondCallEvent.newSession.dtmf).toHaveBeenCalledWith('5');
    });

    it('injects a new audio element on new incoming call', function() {
        var firstCallSession = initFirstCall();

        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(false);
    });

    it('removes the audio element on hangup', function() {
        var firstCallSession = initFirstCall();

        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(false);
        expect(xc_webrtc.internalAudioStream !== null).toBe(true);
        sessionEventListener(getTerminatedEvent(firstCallSession.id));
        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(true);
    });

    it('injects a new audio element on new outgoing call', function(done) {
        let promise = initStackByLineConfig();
        allowStartStack();

        promise.then(() => {
          xc_webrtc.dial('1100');
          expect(document.getElementById('audio_remote' + session.getId()) === null).toBe(false);
          done();
        });
    });

    it('injects a new audio element on second incoming call', function() {
        var firstCallSession = initFirstCall();
        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(false);
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        generalEventListener(secondCallEvent);

        expect(document.getElementById('audio_remote' + secondCallEvent.newSession.id) === null).toBe(false);
    })

    it('removes the audio element on second incoming call hangup', function() {
        var firstCallSession = initFirstCall();
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        generalEventListener(secondCallEvent);

        sessionEventListener(getTerminatedEvent(secondCallEvent.newSession.id));
        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(false);
        expect(document.getElementById('audio_remote' + secondCallEvent.newSession.id) === null).toBe(true);
    })

    it('registers session handler on new session notification', function () {
        var firstCallEvent = initIncomingSession([]);
        generalEventListener(firstCallEvent);

        expect(firstCallEvent.newSession.addEventListener).toHaveBeenCalledWith('*', sessionEventListener);
    })

    it('starts an audio call when the 2nd dial parameter is omitted', function() {
        initStackByLineConfig();
        xc_webrtc.dial('1100');
        expect(stack.newSession).toHaveBeenCalledWith('call-audio', {});
    })

    it('starts an audio call when the 2nd dial parameter is false', function() {
        initStackByLineConfig();
        xc_webrtc.dial('1100', false);
        expect(stack.newSession).toHaveBeenCalledWith('call-audio', {});
    })

    it('returns mediaType by sipCallId for incoming call', function() {
        initStackByLineConfig();
        var firstCallSession = initFirstCall();
        expect(xc_webrtc.getMediaTypeBySipCallId(sipCallId)).toBe(xc_webrtc.mediaType.AUDIO);
    })

    it('returns mediaType by sipCallId for outgoing call init by progress', function() {
        initStackByLineConfig();
        xc_webrtc.dial('1100');
        var progressEvent = getProgressEvent('outSessionId', 'audio/video');
        sessionEventListener(progressEvent);
        expect(xc_webrtc.getMediaTypeBySipCallId(sipCallId)).toBe(xc_webrtc.mediaType.AUDIO);
    })

    it('updates elements on connected event', function() {
        initStackByLineConfig();
        xc_webrtc.dial('1100', true);
        var connectedEvent = getConnectedEvent('outSessionId', 'audio/video');
        sessionEventListener(connectedEvent);
        expect(connectedEvent.session.setConfiguration).toHaveBeenCalled();
    })

    it('provides a unique audio context',function() {
        var firstContext = xc_webrtc.getAudioContext();
        var secondContext = xc_webrtc.getAudioContext();
        expect(firstContext === secondContext).toBe(true)
    })

    it('returns the unregistered handler when the unregister method of localStreamHandlers is called', function() {
        var handler = jasmine.createSpy();
        var handlerBis = jasmine.createSpy();
        var unregisterMethod = xc_webrtc.setLocalStreamHandler(handler, "123-56");
        var unregisterMethodBis = xc_webrtc.setLocalStreamHandler(handlerBis, "123-56");
        var result = unregisterMethod();
        expect(result).toBe(handler);
        expect(unregisterMethodBis()).toBe(handlerBis);
    })

     it('returns the unregistered handler when the unregister method of remoteStreamHandlers is called', function() {
        var handler = jasmine.createSpy();
        var handlerBis = jasmine.createSpy();
        var unregisterMethod = xc_webrtc.setRemoteStreamHandler(handler, "123-56");
        var unregisterMethodBis = xc_webrtc.setRemoteStreamHandler(handlerBis, "123-56");
        var result = unregisterMethod();
        expect(result).toBe(handler);
        expect(unregisterMethodBis()).toBe(handlerBis);
    })

    it('propagates an event with the sipCallId of the session', function() {
        var handler = jasmine.createSpy();
        xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, handler);
        xc_webrtc.setHandler(xc_webrtc.MessageType.OUTGOING, handler);
        xc_webrtc.dial();
        expect(handler).not.toHaveBeenCalledWith();

        sessionEventListener(getProgressEvent());
        sessionEventListener(getConnectedEvent('outSessionId'));
        sessionEventListener(getResumedEvent());
        sessionEventListener(getHoldedEvent());
        sessionEventListener(getTerminatedEvent('outSessionId'));

        expect(handler.calls.all()[0].args[0].type).toBe('Establishing');
        expect(handler.calls.all()[1].args[0].type).toBe('Ringing');
        expect(handler.calls.all()[2].args[0].type).toBe('Connected');
        expect(handler.calls.all()[3].args[0].type).toBe('Resume');
        expect(handler.calls.all()[4].args[0].type).toBe('Hold');
        expect(handler.calls.all()[5].args[0].type).toBe('Terminated');

        expect(handler.calls.all()[2].args[0].sipCallId).toBe(sipCallId);
        expect(handler.calls.all()[3].args[0].sipCallId).toBe(sipCallId);
        expect(handler.calls.all()[4].args[0].sipCallId).toBe(sipCallId);
        expect(handler.calls.all()[5].args[0].sipCallId).toBe(sipCallId);
    })

    it('answers on cti answer command', function(done) {
      let promise = initStackByLineConfig();
      allowStartStack();

      promise.then(() => {
        var newCallEvent = initIncomingSession([]);
        generalEventListener(newCallEvent);
        spyOn(newCallEvent.newSession, 'accept').and.callThrough();
        ctiCmdHandler({'command':'Answer'});
        expect(newCallEvent.newSession.accept).toHaveBeenCalled();
        done();
      });
    })

    it('put the call on hold on cti hold command', function() {
        var session = initFirstCall();
        sendConnectedEvent(session.id);
        ctiCmdHandler({'command':'Hold'});
        expect(session.hold).toHaveBeenCalled();
    })

    it('send dtmf on cti sendDtmf command', function() {
        var session = initFirstCall();
        sendConnectedEvent(session.id);
        ctiCmdHandler({'command':'SendDtmf', 'key': '7'});
        expect(session.dtmf).toHaveBeenCalledWith('7');
    })

    function startConference(idA = 1, idB = 2) {
        var firstCallSession = initFirstCall(undefined, idA);
        sendConnectedEvent(firstCallSession.id);
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, idB, '123123-df456@192.168');
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        sendConnectedEvent(secondCallEvent.newSession.id);

        expect(firstCallSession.hold).toHaveBeenCalled();
        sessionEventListener(getHoldedEvent(firstCallSession.id));

        xc_webrtc.conference();
        return {'firstCallSession': firstCallSession, 'secondCallSession': secondCallEvent.newSession};
    }

    it('deactivates hold on first session when a conference is started', function() {
        var confSessions = startConference();
        expect(confSessions.firstCallSession.resume).toHaveBeenCalled();
    })

    it('puts on hold both channels when a conference is put on hold', function() {
        var confSessions = startConference();
        sessionEventListener(getResumedEvent(confSessions.firstCallSession.id));
        xc_webrtc.hold();
        expect(confSessions.firstCallSession.hold).toHaveBeenCalled();
        expect(confSessions.secondCallSession.hold).toHaveBeenCalled();
    })

    it('resumes both channels when a conference is resumed by calling conference once more', function() {
        var confSessions = startConference();
        sessionEventListener(getResumedEvent(confSessions.firstCallSession.id));
        xc_webrtc.hold();
        sessionEventListener(getHoldedEvent(confSessions.firstCallSession.id));
        sessionEventListener(getHoldedEvent(confSessions.secondCallSession.id));
        xc_webrtc.conference();
        expect(confSessions.firstCallSession.resume.calls.count()).toBe(2);
        expect(confSessions.secondCallSession.resume).toHaveBeenCalled();
    })

    it('resumes both channels when a conference is resumed by calling hold once more', function() {
        var confSessions = startConference();
        sessionEventListener(getResumedEvent(confSessions.firstCallSession.id));
        xc_webrtc.holdBySipCallId();
        sessionEventListener(getHoldedEvent(confSessions.firstCallSession.id));
        sessionEventListener(getHoldedEvent(confSessions.secondCallSession.id));
        xc_webrtc.holdBySipCallId();
        expect(confSessions.firstCallSession.resume.calls.count()).toBe(2);
        expect(confSessions.secondCallSession.resume).toHaveBeenCalled();
    })

    it('starts a conference when retrieving in conference context', function() {
      var confSessions = startConference();
      sessionEventListener(getStreamAudioRemoteEvent(confSessions.firstCallSession.id));
      xc_webrtc.conference();
      expect(xc_webrtc.createAndGetMixedTrack).toHaveBeenCalledTimes(2);
      expect(xc_webrtc.createAndGetMixedTrack.calls.allArgs()[0][0].session).toEqual(confSessions.firstCallSession);
      expect(xc_webrtc.createAndGetMixedTrack.calls.allArgs()[0][1].session).toEqual(confSessions.secondCallSession);
    });
    
    it('disable ice_servers when Ice configuration is empty', function (done) {
      let promise = initStackByLineConfig();
      allowStartStack();

      promise.then(() => {
        expect(xc_webrtc.getIceUrls().length).toBe(0);
        done();
      });
    });

    it('set ice_servers when whole Ice configuration is defined', function (done) {
      var iceCfg = {stunConfig:{urls:["stun:host:3478"]},turnConfig:{urls:["turn:host:3478","turns:host:3478"], username:"2002:84600",credential:"pwd",ttl:84600}};
      let promise = initStackByLineConfig();
      sipReadyCallback();
      iceHandler(iceCfg);

      promise.then(() => {
        expect(xc_webrtc.getIceUrls()).toEqual([
          {
            "urls":[
              "stun:host:3478"
             ]
          },
          {
            "urls":[
              "turn:host:3478",
              "turns:host:3478"
            ],
            "username":"2002:84600",
            "credential":"pwd"
          }
        ]);
        done();
      });
    });

    it('set only stun ice_servers when stun Ice configuration is defined', function (done) {
      var iceCfg = {stunConfig:{urls:["stun:host:3478"]}};
      let promise = initStackByLineConfig();
      sipReadyCallback();
      iceHandler(iceCfg);

      promise.then(() => {
        expect(xc_webrtc.getIceUrls()).toEqual([
          {
            "urls":[
              "stun:host:3478"
             ]
          }
        ]);
        done();
      });
    });

    it('set only turn ice_servers when turn Ice configuration is defined', function (done) {
      var iceCfg = {turnConfig:{urls:["turn:host:3478","turns:host:3478"], username:"2002:84600",credential:"pwd",ttl:84600}};
      let promise = initStackByLineConfig();
      sipReadyCallback();
      iceHandler(iceCfg);

      promise.then(() => {
        expect(xc_webrtc.getIceUrls()).toEqual([
          {
            "urls":[
              "turn:host:3478",
              "turns:host:3478"
            ],
            "username":"2002:84600",
            "credential":"pwd"
          }
        ]);
        done();
      });
    });

    it('should not break the stack when calling Cti.toggleMicrophone with no active calls', function(done) {

      spyOn(console, 'warn').and.callThrough();
      spyOn(xc_webrtc, 'toggleMicrophone').and.callThrough();

      xc_webrtc.toggleMicrophone();
      expect(xc_webrtc.toggleMicrophone).toHaveBeenCalled();
      expect(console.warn).toHaveBeenCalledWith("Cannot mute user : no active calls");
      expect(callSessions.length).toEqual(0);

      done();
    });

    it('should disable tracks when calling Cti.toggleMicrophone with one active call', function(done) {

      spyOn(console, 'warn').and.callThrough();
      spyOn(xc_webrtc, 'toggleMicrophone').and.callThrough();

      expect(callSessions.length).toEqual(0);

      initFirstCall();
      expect(callSessions[0].session.o_session.get_stream_local().getTracks()[0].enabled).toBeTruthy();

      xc_webrtc.toggleMicrophone();
      expect(xc_webrtc.toggleMicrophone).toHaveBeenCalled();
      expect(console.warn).not.toHaveBeenCalledWith("Cannot mute user : no active calls");
      expect(callSessions[0].session.o_session.get_stream_local().getTracks()[0].enabled).toBeFalsy();
      
      xc_webrtc.toggleMicrophone();
      expect(xc_webrtc.toggleMicrophone).toHaveBeenCalledTimes(2);
      expect(console.warn).not.toHaveBeenCalledWith("Cannot mute user : no active calls");
      expect(callSessions[0].session.o_session.get_stream_local().getTracks()[0].enabled).toBeTruthy();

      done();
    });

    it('should not break the stack when calling Cti.toggleMicrophone with target call on hold', function(done) {

      spyOn(console, 'warn').and.callThrough();
      spyOn(xc_webrtc, 'toggleMicrophone').and.callThrough();

      expect(callSessions.length).toEqual(0);

      initFirstCall();
      expect(callSessions[0].session.o_session.get_stream_local().getTracks()[0].enabled).toBeTruthy();

      xc_webrtc.hold();

      xc_webrtc.toggleMicrophone();
      expect(xc_webrtc.toggleMicrophone).toHaveBeenCalled();
      expect(console.warn).toHaveBeenCalledWith("Cannot mute user : no stream found");
      expect(callSessions[0].session.o_session.get_stream_local()).not.toBeDefined();

      callSessions[0].session.resume();

      expect(callSessions[0].session.o_session.get_stream_local()).toBeDefined();
      expect(callSessions[0].session.o_session.get_stream_local().getTracks()[0].enabled).toBeTruthy();

      done();
    });

    it('should disable target track when calling Cti.toggleMicrophone with an argument', function(done) {

      spyOn(console, 'warn').and.callThrough();
      spyOn(xc_webrtc, 'toggleMicrophone').and.callThrough();

      var firstCallEvent = initFirstCall();
      sendConnectedEvent(firstCallEvent.id);
      expect(callSessions[0].sipCallId).toEqual(sipCallId);

      xc_webrtc.toggleMicrophone("123123-df123@192.168");
      expect(xc_webrtc.toggleMicrophone).toHaveBeenCalled();
      expect(console.warn).not.toHaveBeenCalledWith("Cannot mute user : no active calls");
      expect(console.warn).toHaveBeenCalledWith("Cannot mute user : call with given id not found");
      expect(callSessions[0].session.o_session.get_stream_local().getTracks()[0].enabled).toBeTruthy();

      
      xc_webrtc.toggleMicrophone(sipCallId);
      expect(xc_webrtc.toggleMicrophone).toHaveBeenCalledTimes(2);
      expect(console.warn).not.toHaveBeenCalledWith("Cannot mute user : no active calls");
      expect(callSessions[0].session.o_session.get_stream_local().getTracks()[0].enabled).toBeFalsy();

      done();
    });

    it('should disable target track when calling Cti.toggleMicrophone with an argument and two active calls', function(done) {

      spyOn(console, 'warn').and.callThrough();
      spyOn(xc_webrtc, 'toggleMicrophone').and.callThrough();

      var sipCallIdSecondCall = "789789-df789@192.168";

      var firstCallEvent = initFirstCall();
      sendConnectedEvent(firstCallEvent.id);
      expect(callSessions[0].sipCallId).toEqual(sipCallId);

      var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14, sipCallIdSecondCall);
      spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
      generalEventListener(secondCallEvent);
      expect(secondCallEvent.newSession.accept).toHaveBeenCalled();
      expect(callSessions[1].sipCallId).toEqual(sipCallIdSecondCall);

      expect(callSessions.length).toEqual(2);

      callSessions.forEach( (callSession) => {
        if (callSession.sipCallId == sipCallIdSecondCall) {
          expect(callSession.session.o_session.get_stream_local().getTracks()[0].enabled).toBeTruthy();
        } else {
          expect(callSession.session.o_session.get_stream_local()).not.toBeDefined();
        }
      });

      xc_webrtc.toggleMicrophone("123123-df123@192.168");
      expect(xc_webrtc.toggleMicrophone).toHaveBeenCalled();
      expect(console.warn).not.toHaveBeenCalledWith("Cannot mute user : no active calls");
      expect(console.warn).toHaveBeenCalledWith("Cannot mute user : call with given id not found");
      expect(callSessions[0].session.o_session.get_stream_local()).not.toBeDefined();
      
      xc_webrtc.toggleMicrophone(sipCallId);
      expect(xc_webrtc.toggleMicrophone).toHaveBeenCalledTimes(2);
      expect(console.warn).toHaveBeenCalledWith("Cannot mute user : no stream found");
      expect(callSessions[0].session.o_session.get_stream_local()).not.toBeDefined();

      xc_webrtc.toggleMicrophone(sipCallIdSecondCall);
      expect(xc_webrtc.toggleMicrophone).toHaveBeenCalledTimes(3);
      callSessions.forEach( (callSession) => {
        if (callSession.sipCallId == sipCallIdSecondCall) {
          expect(callSession.session.o_session.get_stream_local().getTracks()[0].enabled).toBeFalsy();
        } else {
          expect(callSession.session.o_session.get_stream_local()).not.toBeDefined();
        }
      });

      done();
    });

    it('should replace underscores for hyphens', function() {
      let result = xc_webrtc.replaceUnderscores('someRealm');
      expect(result).toEqual('someRealm');

      result = xc_webrtc.replaceUnderscores('some-Rea-lm');
      expect(result).toEqual('some-Rea-lm');

      result = xc_webrtc.replaceUnderscores('_some-Rea_lm_');
      expect(result).toEqual('-some-Rea-lm-');

      result = xc_webrtc.replaceUnderscores('_some_Rea_lm_');
      expect(result).toEqual('-some-Rea-lm-');
    });
});
