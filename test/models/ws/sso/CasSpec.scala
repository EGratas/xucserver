package models.ws.sso

import scala.util.{Failure, Success}
import scala.xml.XML
import CasAuthenticationError._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class CasSpec extends AnyWordSpec with Matchers {
  "CasAuthenticationError" should {
    "decode all possible error" in {
      val stringToCode: Map[String, CasAuthenticationError.Value] = Map(
        "INVALID_REQUEST"            -> InvalidRequest,
        "INVALID_TICKET_SPEC"        -> InvalidTicketSpec,
        "UNAUTHORIZED_SERVICE_PROXY" -> UnauthorizedServiceProxy,
        "INVALID_PROXY_CALLBACK"     -> InvalidProxyCallback,
        "INVALID_TICKET"             -> InvalidTicket,
        "INVALID_SERVICE"            -> InvalidService,
        "INTERNAL_ERROR"             -> InternalError
      )

      stringToCode.foreach { case (str, code) =>
        CasAuthenticationError.decode(str) shouldBe Success(code)
      }
    }

    "not decode unknown error" in {
      CasAuthenticationError.decode("Whaaaaa !!") shouldBe a[Failure[_]]
    }
  }

  "CasAuthenticationFailure" should {
    "decode a failure" in {
      val text = """<cas:authenticationFailure code="INVALID_TICKET">
                   | Ticket ST-1856339-aA5Yuvrxzpv8Tau1cYQ7 not recognized
                   |</cas:authenticationFailure>""".stripMargin
      val xml  = XML.loadString(text)
      val expected = CasAuthenticationFailure(
        InvalidTicket,
        Some("Ticket ST-1856339-aA5Yuvrxzpv8Tau1cYQ7 not recognized")
      )

      CasAuthenticationFailure.decode(xml) shouldBe Success(expected)
    }

    "decode a failure without message" in {
      val text     = """<cas:authenticationFailure code="INVALID_TICKET">
                   |</cas:authenticationFailure>""".stripMargin
      val xml      = XML.loadString(text)
      val expected = CasAuthenticationFailure(InvalidTicket, None)

      CasAuthenticationFailure.decode(xml) shouldBe Success(expected)
    }
  }

  "CasUser" should {
    "decode a user" in {
      val text     = "<cas:user>username</cas:user>"
      val xml      = XML.loadString(text)
      val expected = CasUser("username")
      CasUser.decode(xml) shouldBe Success(expected)
    }

    "lowercase the username" in {
      val text     = "<cas:user>USERname</cas:user>"
      val xml      = XML.loadString(text)
      val expected = CasUser("username")
      CasUser.decode(xml) shouldBe Success(expected)
    }
  }

  "CasAttribute" should {
    "decode an attribute" in {
      val text     = "<cas:firstname>John</cas:firstname>"
      val xml      = XML.loadString(text)
      val expected = CasAttribute("firstname", "John")
      CasAttribute.decode(xml) shouldBe Success(expected)
    }

    "decode a list of attributes" in {
      val text = """<cas:attributes>
                   | <cas:firstname>John</cas:firstname>
                   | <cas:lastname>Doe</cas:lastname>
                   | <cas:title>Mr.</cas:title>
                   |</cas:attributes>"""
      val xml  = XML.loadString(text)
      val expected = List(
        CasAttribute("firstname", "John"),
        CasAttribute("lastname", "Doe"),
        CasAttribute("title", "Mr.")
      )

      CasAttribute.decodeAttributes(xml) shouldBe Success(expected)
    }
  }

  "CasAuthenticationSuccess" should {
    "decode a success" in {
      val text = """<cas:authenticationSuccess>
                   | <cas:user>username</cas:user>
                   | <cas:attributes>
                   |  <cas:firstname>John</cas:firstname>
                   |  <cas:lastname>Doe</cas:lastname>
                   |  <cas:title>Mr.</cas:title>
                   | </cas:attributes>
                   |</cas:authenticationSuccess>""".stripMargin
      val xml  = XML.loadString(text)
      val attributes = List(
        CasAttribute("firstname", "John"),
        CasAttribute("lastname", "Doe"),
        CasAttribute("title", "Mr.")
      )
      val expected = CasAuthenticationSuccess(CasUser("username"), attributes)

      CasAuthenticationSuccess.decode(xml) shouldBe Success(expected)
    }
  }

  "CasServiceResponse" should {
    "decode a failed authentication" in {
      val response =
        """<cas:serviceResponse xmlns:cas="http://www.yale.edu/tp/cas">
                       | <cas:authenticationFailure code="INVALID_TICKET">
                       |    Ticket ST-1856339-aA5Yuvrxzpv8Tau1cYQ7 not recognized
                       |  </cas:authenticationFailure>
                       |</cas:serviceResponse>""".stripMargin

      val expected = CasServiceResponse(
        CasAuthenticationFailure(
          CasAuthenticationError.InvalidTicket,
          Some("Ticket ST-1856339-aA5Yuvrxzpv8Tau1cYQ7 not recognized")
        )
      )
      CasServiceResponse.decode(response) shouldBe Success(expected)
    }

    "decode a successful authentication" in {
      val response = """<cas:serviceResponse xmlns:cas="http://www.yale.edu/tp/cas">
                       | <cas:authenticationSuccess>
                       |  <cas:user>username</cas:user>
                       |  <cas:attributes>
                       |   <cas:firstname>John</cas:firstname>
                       |   <cas:lastname>Doe</cas:lastname>
                       |   <cas:title>Mr.</cas:title>
                       |   <cas:email>jdoe@example.org</cas:email>
                       |   <cas:affiliation>staff</cas:affiliation>
                       |   <cas:affiliation>faculty</cas:affiliation>
                       |  </cas:attributes>
                       |  <cas:proxyGrantingTicket>PGTIOU-84678-8a9d...</cas:proxyGrantingTicket>
                       | </cas:authenticationSuccess>
                       |</cas:serviceResponse>""".stripMargin

      val attributes = List(
        CasAttribute("firstname", "John"),
        CasAttribute("lastname", "Doe"),
        CasAttribute("title", "Mr."),
        CasAttribute("email", "jdoe@example.org"),
        CasAttribute("affiliation", "staff"),
        CasAttribute("affiliation", "faculty")
      )

      val expected = CasServiceResponse(
        CasAuthenticationSuccess(CasUser("username"), attributes)
      )
      CasServiceResponse.decode(response) shouldBe Success(expected)
    }
  }
}
