package models

import play.api.libs.json.Json
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class XivoDirdSpec extends AnyWordSpec with Matchers {

  val searchResponse: String =
    """
    |{
    |    "column_headers": [
    |        "Nom",
    |        "Numéro",
    |        "Mobile",
    |        "Autre numéro",
    |        "Favoris",
    |        "Personnel",
    |        "Email"
    |    ],
    |    "column_types": [
    |        "name",
    |        "number",
    |        "mobile",
    |        "number",
    |        "favorite",
    |        "personal",
    |        "other"
    |    ],
    |    "results": [
    |        {
    |            "column_values": [
    |                "Peter Pan",
    |                "44201",
    |                "+33061254658",
    |                "+33231568456",
    |                false,
    |                false,
    |                "pp@test.nodomain"
    |            ],
    |            "relations": {
    |                "agent_id": null,
    |                "endpoint_id": null,
    |                "source_entry_id": "565-qsdf-qsdf-5-555",
    |                "user_id": null,
    |                "xivo_id": null
    |            },
    |            "source": "avc"
    |        },
    |        {
    |            "column_values": [
    |                "Lord Aramis",
    |                "44205",
    |                "012345687",
    |                null,
    |                true,
    |                false,
    |                null
    |            ],
    |            "relations": {
    |                "agent_id": 4,
    |                "endpoint_id": 2,
    |                "source_entry_id": "1",
    |                "user_id": 111,
    |                "xivo_id": "3c3f8fe6-f776-4917-bc5d-1733975256d2"
    |            },
    |            "source": "internal"
    |        }
    |    ],
    |    "term": "44"
    |}
    |    """.stripMargin

  val searchResponseBis: String =
    """
    |{
    |    "column_headers": [
    |        "Prenom",
    |        "Nom",
    |        "Numéro",
    |        "Mobile",
    |        "Autre numéro",
    |        "Favoris",
    |        "Personnel"
    |    ],
    |    "column_types": [
    |        "name",
    |        "name",
    |        "number",
    |        "callable",
    |        "number",
    |        "favorite",
    |        "personal"
    |    ],
    |    "results": [
    |        {
    |            "column_values": [
    |                "Robert",
    |                "Dupont",
    |                "42305",
    |                "458795462",
    |                null,
    |                false,
    |                false
    |            ],
    |            "relations": {
    |                "agent_id": null,
    |                "endpoint_id": 58,
    |                "source_entry_id": "13",
    |                "user_id": 13,
    |                "xivo_id": "1cc7fbf2-5f13-4898-986"
    |            },
    |            "source": "quebec"
    |        },
    |        {
    |            "column_values": [
    |                "Robert",
    |                "Dupont",
    |                "42305",
    |                "+3465959588",
    |                "+14567897895",
    |                false,
    |                false
    |            ],
    |            "relations": {
    |                "agent_id": null,
    |                "endpoint_id": null,
    |                "source_entry_id": "62fdbe2a-f044-1031-8d2",
    |                "user_id": null,
    |                "xivo_id": null
    |            },
    |            "source": "ldap"
    |        },
    |        {
    |            "column_values": [
    |                "Malo",
    |                "Saint",
    |                null,
    |                null,
    |                null,
    |                false,
    |                false
    |            ],
    |            "relations": {
    |                "agent_id": null,
    |                "endpoint_id": null,
    |                "source_entry_id": "93ebb926-6067-1033",
    |                "user_id": null,
    |                "xivo_id": null
    |            },
    |            "source": "ldap"
    |        }
    |    ],
    |    "term": "Du"
    |}
  """.stripMargin

  val unsupportedResponse: String =
    """
      |{
      |    "column_headers": [
      |        "Firstname",
      |        "Lastname",
      |        "Number",
      |        "Mobile",
      |        "Email",
      |        "Favorite"
      |    ],
      |    "column_types": [
      |        "name",
      |        "name",
      |        "number",
      |        "mobile",
      |        "mobile",
      |        "mail",
      |        "favorite"
      |    ],
      |    "results": [
      |        {
      |            "column_values": [
      |                "web",
      |                "browser",
      |                "1500",
      |                "06",
      |                null,
      |                "rien@no.ff",
      |                false
      |            ],
      |            "relations": {
      |                "agent_id": null,
      |                "endpoint_id": 18,
      |                "source_entry_id": "11",
      |                "user_id": 11,
      |                "xivo_id": "02e0dc13-407c-47f9-9e60-a7cb671f26b1"
      |            },
      |            "source": "xivo_users"
      |        }
      |    ],
      |    "term": "b"
      |}
      |    """.stripMargin

  val unsupportedTypeResponse: String =
    """
      |{
      |    "column_headers": [
      |        "Firstname",
      |        "Lastname",
      |        "Number",
      |        "Mobile",
      |        "Email",
      |        "Something"
      |    ],
      |    "column_types": [
      |        "name",
      |        "name",
      |        "number",
      |        "mobile",
      |        "mobile",
      |        "mail",
      |        ""
      |    ],
      |    "results": [
      |        {
      |            "column_values": [
      |                "web",
      |                "browser",
      |                "1500",
      |                "06",
      |                null,
      |                "rien@no.ff",
      |                true
      |            ],
      |            "relations": {
      |                "agent_id": null,
      |                "endpoint_id": 18,
      |                "source_entry_id": "11",
      |                "user_id": 11,
      |                "xivo_id": "02e0dc13-407c-47f9-9e60-a7cb671f26b1"
      |            },
      |            "source": "xivo_users"
      |        }
      |    ],
      |    "term": "b"
      |}
      |    """.stripMargin

  "XivoDird" should {
    "be able to decode search response" in {
      val response: DirSearchResult =
        DirSearchResult.parse(Json.parse(searchResponse))
      response.results(0).item.name shouldEqual "Peter Pan"
      response.results(0).item.mobile shouldEqual Some("+33061254658")
      response.results(0).item.externalNumber shouldEqual Some("+33231568456")
      response.results(0).item.email shouldEqual Some("pp@test.nodomain")
      response.results(1).relations.userId shouldEqual Some(111)
    }

    "be able to decode search response wiht different format" in {
      val response: DirSearchResult =
        DirSearchResult.parse(Json.parse(searchResponseBis))
      response.results(0).item.name shouldEqual "Robert Dupont"
      response.results(0).item.mobile shouldEqual Some("458795462")
      response.results(1).relations.userId shouldEqual None
    }

    "use the first occurence of a given key if multiples are present" in {
      val response: DirSearchResult =
        DirSearchResult.parse(Json.parse(unsupportedResponse))
      response.results(0).item.mobile shouldEqual Some("06")
    }

    "not fail if favorites are not defined" in {
      val response: DirSearchResult =
        DirSearchResult.parse(Json.parse(unsupportedResponse))
      response.results(0).item.favorite shouldEqual false
    }
  }
}
