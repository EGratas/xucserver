package models

import java.util.UUID

import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import play.api.libs.json.Json
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.joda.time.format.DateTimeFormatter

class CallbackTicketSpec extends AnyWordSpec with Matchers {
  val format: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  "A CallbackTicket" should {
    "be written in JSON" in {
      val ticket = CallbackTicket(
        Some(UUID.randomUUID()),
        UUID.randomUUID(),
        UUID.randomUUID(),
        "thequeue",
        "1000",
        new LocalDate(2015, 1, 8),
        format.parseDateTime("2015-01-08 08:00:00"),
        Some(format.parseDateTime("2015-01-08 08:30:00")),
        Some("123456.789"),
        Some(CallbackStatus.Voicemail),
        Some("my comment"),
        Some("1000"),
        Some("0633333333"),
        Some("John"),
        Some("Doe"),
        Some("Avencall"),
        Some("my description")
      )

      Json.toJson(ticket) shouldEqual Json.obj(
        "uuid"              -> ticket.uuid.get.toString,
        "listUuid"          -> ticket.listUuid.toString,
        "requestUuid"       -> ticket.requestUuid.toString,
        "queueRef"          -> ticket.queueRef,
        "agentNum"          -> ticket.agentNum,
        "dueDate"           -> "2015-01-08",
        "started"           -> "2015-01-08 08:00:00",
        "lastUpdate"        -> "2015-01-08 08:30:00",
        "callid"            -> "123456.789",
        "status"            -> "voicemail",
        "comment"           -> "my comment",
        "phoneNumber"       -> "1000",
        "mobilePhoneNumber" -> "0633333333",
        "firstName"         -> "John",
        "lastName"          -> "Doe",
        "company"           -> "Avencall",
        "description"       -> "my description"
      )
    }

    "be read from json" in {
      val (uuid1, uuid2, uuid3) =
        (UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID())
      Json
        .obj(
          "uuid"              -> uuid1,
          "listUuid"          -> uuid2,
          "requestUuid"       -> uuid3,
          "queueRef"          -> "thequeue",
          "agentNum"          -> "1000",
          "dueDate"           -> "2015-01-08",
          "started"           -> "2015-01-08 08:00:00",
          "lastUpdate"        -> "2015-01-08 08:30:00",
          "callid"            -> "123456.789",
          "status"            -> "voicemail",
          "comment"           -> "my comment",
          "phoneNumber"       -> "1000",
          "mobilePhoneNumber" -> "0633333333",
          "firstName"         -> "John",
          "lastName"          -> "Doe",
          "company"           -> "Avencall",
          "description"       -> "my description"
        )
        .validate[CallbackTicket]
        .get shouldEqual CallbackTicket(
        Some(uuid1),
        uuid2,
        uuid3,
        "thequeue",
        "1000",
        new LocalDate(2015, 1, 8),
        format.parseDateTime("2015-01-08 08:00:00"),
        Some(format.parseDateTime("2015-01-08 08:30:00")),
        Some("123456.789"),
        Some(CallbackStatus.Voicemail),
        Some("my comment"),
        Some("1000"),
        Some("0633333333"),
        Some("John"),
        Some("Doe"),
        Some("Avencall"),
        Some("my description")
      )
    }
  }
}
