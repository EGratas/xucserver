package models

import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

/**
  */
class QueueMembershipSpec extends AnyWordSpec with Matchers {
  import QueueMembership._

  "A QueueMembership" should {
    "convert to json" in {
      val qm = QueueMembership(10, 5)
      Json.toJson(qm) shouldEqual Json.obj(
        "queueId" -> qm.queueId,
        "penalty" -> qm.penalty
      )
    }

    "be created from json" in {
      val json: JsValue = Json.parse("""{"queueId": 8, "penalty": 2}""")
      json.validate[QueueMembership] match {
        case JsSuccess(qm, _) => qm shouldEqual QueueMembership(8, 2)
        case JsError(errors)  => fail()
      }
    }
  }

  "A UserQueueMembership" should {
    "convert to json" in {
      val membership = List(QueueMembership(10, 5), QueueMembership(17, 0))
      val qm         = UserQueueDefaultMembership(1, membership)
      Json.toJson(qm) shouldEqual Json.obj(
        "userId"     -> qm.userId,
        "membership" -> Json.toJson(qm.membership)
      )
    }

    "be created from json" in {
      val json: JsValue = Json.parse(
        """{"userId": 8, "membership": [{"queueId": 10, "penalty": 5},{"queueId": 17, "penalty": 0}]}"""
      )
      json.validate[UserQueueDefaultMembership] match {
        case JsSuccess(qm, _) =>
          qm shouldEqual UserQueueDefaultMembership(
            8,
            List(QueueMembership(10, 5), QueueMembership(17, 0))
          )
        case JsError(errors) => fail()
      }
    }
  }

}
