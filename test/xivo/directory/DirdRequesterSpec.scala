package xivo.directory

import org.apache.pekko.actor.{Actor, Props}
import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import models.{DirSearchResult, RichDirectoryResult, Token, XivoUser}
import org.joda.time.DateTime
import org.mockito.Mockito.when
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.time.{Seconds, Span}
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.http.Status
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsObject, JsValue, Json}
import play.api.libs.ws.{WSRequest, WSResponse}
import services.directory.DirectoryTransformer.EnrichDirectoryResult
import xivo.models._
import xivo.network.XiVOWS
import xivo.services.XivoAuthentication.GetCtiToken
import xivo.xuc.XucConfig
import models.ws.auth.ApiUser

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.apache.pekko.actor.ActorRef

class DirdRequesterSpec
    extends TestKitSpec("DirdRequesterSpec")
    with MockitoSugar
    with GuiceOneAppPerSuite {
  val appliConfig: Map[String, Any] = {
    Map(
      "xivohost"                 -> "xivoIP",
      "xivoAuthPort"             -> 9497,
      "xivo.auth.defaultExpires" -> 1
    )
  }

  override def fakeApplication(): Application =
    new GuiceApplicationBuilder().configure(appliConfig).build()

  "DirdRequester actor " should {

    "list dird results of personal contacts formatted as RichDirectoryResult" in new Helper
      with ScalaFutures {
      when(
        xivoWS.get(
          "xivoIP",
          "0.1/directories/personal/default",
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      when(wsResponse.json).thenReturn(searchResponse)
      when(wsRequest.execute()).thenReturn(Future.successful(wsResponse))

      val f: Future[RichDirectoryResult] = dirdRequester.richList(apiUser)

      val enrichedResult = new RichDirectoryResult(List("a"))

      transformer.expectMsg(
        EnrichDirectoryResult(DirSearchResult.parse(searchResponse), user.id)
      )
      transformer.reply(enrichedResult)

      whenReady(f, Timeout(Span(3, Seconds))) { result =>
        result should be(enrichedResult)
      }
    }

    "list personal contacts" in new Helper with ScalaFutures {

      when(
        xivoWS.get(
          "xivoIP",
          s"0.1/personal",
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      when(wsResponse.json).thenReturn(contactList)
      when(wsResponse.status).thenReturn(Status.OK)
      when(wsRequest.execute()).thenReturn(Future.successful(wsResponse))

      val f: Future[List[PersonalContactResult]] = dirdRequester.list(apiUser)

      val contacts: List[PersonalContactResult] = List(
        PersonalContactResult(
          contactId,
          Some("Mike"),
          Some("Swarm"),
          Some("1234"),
          Some("4321"),
          Some("4444"),
          Some("m.swarm@mail.com"),
          Some("myCompany")
        ),
        PersonalContactResult(
          contactIdBis,
          Some("John"),
          Some("Doe"),
          Some("1111"),
          Some("2222"),
          Some("3333"),
          Some("j.doe@mail.com"),
          Some("myCompany")
        )
      )
      whenReady(f, Timeout(Span(3, Seconds))) { result =>
        result should be(contacts)
      }
    }

    "return personal contact if successfully added in dird" in new Helper
      with ScalaFutures {
      when(
        xivoWS.post(
          "xivoIP",
          "0.1/personal",
          payload = Some(contactCreated),
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      val contactCreatedResult: JsValue =
        Json.obj("id" -> contactId) ++ contactCreated.as[JsObject]

      when(wsResponse.json).thenReturn(contactCreatedResult)
      when(wsResponse.status).thenReturn(Status.CREATED)
      when(wsRequest.execute()).thenReturn(Future.successful(wsResponse))

      val f: Future[PersonalContactResult] =
        dirdRequester.add(apiUser, contactCreated.as[PersonalContactRequest])

      val contact: PersonalContactResult = PersonalContactResult(
        contactId,
        Some("Mike"),
        Some("Swarm"),
        Some("1234"),
        Some("4321"),
        Some("4444"),
        Some("m.swarm@mail.com"),
        Some("myCompany")
      )
      whenReady(f, Timeout(Span(3, Seconds))) { result =>
        result should be(contact)
      }
    }

    "return DirdException if error returned while adding personal contact" in new Helper
      with ScalaFutures {
      when(
        xivoWS.post(
          "xivoIP",
          "0.1/personal",
          payload = Some(contactCreated),
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      val response: WSResponse = mock[WSResponse]
      val errorResult: JsObject = Json.obj("error" -> "crappy content")

      when(response.json).thenReturn(errorResult)
      when(wsRequest.execute()).thenReturn(Future.successful(response))

      val f: Future[PersonalContactResult] =
        dirdRequester.add(apiUser, contactCreated.as[PersonalContactRequest])

      whenReady(f.failed) { result =>
        result shouldBe a[DirdRequesterException]
      }
    }

    "return personal contact if successfully edited in dird" in new Helper
      with ScalaFutures {

      when(
        xivoWS.put(
          "xivoIP",
          s"0.1/personal/$contactId",
          payload = Some(contactUpdated),
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      val contactUpdatedResult: JsObject =
        Json.obj("id" -> contactId) ++ contactUpdated.as[JsObject]

      when(wsResponse.json).thenReturn(contactUpdatedResult)
      when(wsResponse.status).thenReturn(Status.OK)
      when(wsRequest.execute()).thenReturn(Future.successful(wsResponse))

      val f: Future[PersonalContactResult] =
        dirdRequester.edit(apiUser, contactUpdated.as[PersonalContactRequest])(
          contactId
        )

      val contact: PersonalContactResult = PersonalContactResult(
        contactId,
        Some("Mike"),
        Some("Swarm"),
        Some("3333"),
        Some("4444"),
        Some("5555"),
        Some("m.swarm@mail.com"),
        Some("myCompany")
      )
      whenReady(f, Timeout(Span(3, Seconds))) { result =>
        result should be(contact)
      }
    }

    "return DirdException if error returned while editing personal contact" in new Helper
      with ScalaFutures {

      when(
        xivoWS.post(
          "xivoIP",
          s"0.1/personal/$contactId",
          payload = Some(contactUpdated),
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      val response: WSResponse = mock[WSResponse]
      val errorResult: JsObject = Json.obj("error" -> "crappy content")

      when(response.json).thenReturn(errorResult)
      when(wsRequest.execute()).thenReturn(Future.successful(response))

      val f: Future[PersonalContactResult] =
        dirdRequester.edit(apiUser, contactUpdated.as[PersonalContactRequest])(
          contactId
        )

      whenReady(f.failed) { result =>
        result shouldBe a[DirdRequesterException]
      }
    }

    "get personal contact" in new Helper with ScalaFutures {
      when(
        xivoWS.get(
          "xivoIP",
          s"0.1/personal/$contactId",
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      val contactResult: JsObject =
        Json.obj("id" -> contactId) ++ contactCreated.as[JsObject]

      when(wsResponse.json).thenReturn(contactResult)
      when(wsResponse.status).thenReturn(Status.OK)
      when(wsRequest.execute()).thenReturn(Future.successful(wsResponse))

      val f: Future[PersonalContactResult] = dirdRequester.get(apiUser)(contactId)

      val contact: PersonalContactResult = PersonalContactResult(
        contactId,
        Some("Mike"),
        Some("Swarm"),
        Some("1234"),
        Some("4321"),
        Some("4444"),
        Some("m.swarm@mail.com"),
        Some("myCompany")
      )
      whenReady(f, Timeout(Span(3, Seconds))) { result =>
        result should be(contact)
      }
    }

    "delete one personal contact" in new Helper with ScalaFutures {

      when(
        xivoWS.del(
          "xivoIP",
          s"0.1/personal/$contactId",
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      when(wsResponse.status).thenReturn(Status.NO_CONTENT)
      when(wsRequest.execute()).thenReturn(Future.successful(wsResponse))

      val f: Future[WSResponse] = dirdRequester.delete(apiUser)(contactId)

      whenReady(f, Timeout(Span(3, Seconds))) { result =>
        result shouldBe a[WSResponse]
      }
    }

    "delete all personal contacts" in new Helper with ScalaFutures {

      when(
        xivoWS.del(
          "xivoIP",
          s"0.1/personal",
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      when(wsResponse.status).thenReturn(Status.NO_CONTENT)
      when(wsRequest.execute()).thenReturn(Future.successful(wsResponse))

      val f: Future[WSResponse] = dirdRequester.deleteAll(apiUser)

      whenReady(f, Timeout(Span(3, Seconds))) { result =>
        result shouldBe a[WSResponse]
      }
    }

    "export all personal contacts witout contactId" in new Helper
      with ScalaFutures {
      when(
        xivoWS.get(
          "xivoIP",
          s"0.1/personal?format=text%2Fcsv",
          headers = Map("X-Auth-Token" -> token.token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      when(wsResponse.status).thenReturn(Status.OK)
      when(wsResponse.body).thenReturn(csvDirdResult)
      when(wsRequest.execute()).thenReturn(Future.successful(wsResponse))

      val f: Future[String] = dirdRequester.exportCsv(apiUser)

      whenReady(f, Timeout(Span(3, Seconds))) { result =>
        result should be(csvResult)
      }
    }

    "import personal contacts" in new Helper with ScalaFutures {
      when(
        xivoWS.genericPost(
          "xivoIP",
          s"0.1/personal/import",
          headers = Map(
            "X-Auth-Token" -> token.token,
            "Content-Type" -> "text/csv;charset=UTF-8"
          ),
          payload = Some(csvResult),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)

      when(wsResponse.status).thenReturn(Status.CREATED)
      when(wsResponse.json).thenReturn(importResult)
      when(wsRequest.execute()).thenReturn(Future.successful(wsResponse))

      val f: Future[PersonalContactImportResult] =
        dirdRequester.importCsv(apiUser)(Some(csvResult))

      val contactImportSuccess: PersonalContactResult = PersonalContactResult(
        contactId,
        Some("john"),
        Some("doe"),
        Some("1111"),
        Some("2222"),
        Some("3333"),
        Some("j.doe@my.corp"),
        Some("corp")
      )
      val contactImportFailure: PersonalContactImportFailure =
        PersonalContactImportFailure(Some(1), Some(List("Some error")))
      val contactImportResult: PersonalContactImportResult = PersonalContactImportResult(
        Some(List(contactImportSuccess)),
        Some(List(contactImportFailure))
      )
      whenReady(f, Timeout(Span(3, Seconds))) { result =>
        result should be(contactImportResult)
      }
    }

  }

  class FakeAuth(token: Token) extends Actor {
    def receive: PartialFunction[Any,Unit] = { case GetCtiToken(_) =>
      sender() ! token
    }
  }

  class Helper {
    val date         = new DateTime(2017, 11, 10, 1, 22, 3)
    val token: Token        = Token("token", date, date, "cti", None, List.empty)
    val contactId    = "d0e4286b-b3e3-4d25"
    val contactIdBis = "efa4282b-c1e3-8a27"
    val user: XivoUser =
      XivoUser(12, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
    val apiUser: ApiUser =
      ApiUser(user.username, user.id)

    def auth(userId: Long): Future[Token] = Future(token)

    val xivoAuth: ActorRef = system.actorOf(Props(new FakeAuth(token)))

    val transformer: TestProbe    = TestProbe()
    val xivoWS: XiVOWS = mock[XiVOWS]

    val dirdRequester = new DirdRequester(
      xivoAuth,
      transformer.ref,
      xivoWS,
      new XucConfig(app.configuration)
    )
    val wsRequest: WSRequest   = mock[WSRequest]
    val wsResponse: WSResponse = mock[WSResponse]

    val searchDirdResponse: String =
      """
        |{
        |    "column_headers": [
        |        "Favoris",
        |        "Nom",
        |        "Numéro",
        |        "Mobile",
        |        "Autre numéro",
        |        "Personnel",
        |        "Email"
        |    ],
        |    "column_types": [
        |        "favorite",
        |        "name",
        |        "number",
        |        "mobile",
        |        "number",
        |        "personal",
        |        "name"
        |    ],
        |    "results": [
        |        {
        |            "column_values": [
        |                false,
        |                "Peter Pan",
        |                "44201",
        |                "+33061254658",
        |                "+33231568456",
        |                false,
        |                "pp@test.nodomain"
        |            ],
        |            "relations": {
        |                "agent_id": null,
        |                "endpoint_id": null,
        |                "source_entry_id": "565-qsdf-qsdf-5-555",
        |                "user_id": null,
        |                "xivo_id": null
        |            },
        |            "source": "avc"
        |        },
        |        {
        |            "column_values": [
        |                true,
        |                "Lord Aramis",
        |                "44205",
        |                "012345687",
        |                null,
        |                false,
        |                null
        |            ],
        |            "relations": {
        |                "agent_id": null,
        |                "endpoint_id": null,
        |                "source_entry_id": "585-qsdf-qsdf-5-575",
        |                "user_id": null,
        |                "xivo_id": "null"
        |            },
        |            "source": "internal"
        |        }
        |    ],
        |    "term": ""
        |}
        |    """.stripMargin
    val searchResponse: JsValue = Json.parse(searchDirdResponse)

    val contactCreatedRaw: String =
      """|{
         |  "firstname": "Mike",
         |  "lastname": "Swarm",
         |  "number": "1234",
         |  "mobile": "4321",
         |  "fax": "4444",
         |  "email": "m.swarm@mail.com",
         |  "company" : "myCompany"
         |}""".stripMargin
    val contactCreated: JsValue = Json.parse(contactCreatedRaw)

    val contactUpdatedRaw: String =
      """|{
         |  "firstname": "Mike",
         |  "lastname": "Swarm",
         |  "number": "3333",
         |  "mobile": "4444",
         |  "fax": "5555",
         |  "email": "m.swarm@mail.com",
         |  "company" : "myCompany"
         |}""".stripMargin
    val contactUpdated: JsValue = Json.parse(contactUpdatedRaw)

    val csvDirdResult: String =
      "company,email,fax,firstname,id,lastname,mobile,number\r\ncorp,j.doe@my.corp,3333,john,28079dc0-2c6b-4332-9075-61da9229389e,doe,2222,1111\r\ncorp,m.twain@my.corp,8888,mark,28079dc0-2c6b-4332-9075-61da9229389e,twain,6666,"

    val csvResult: String =
      "\uFEFFcompany,email,fax,firstname,lastname,mobile,number\r\ncorp,j.doe@my.corp,3333,john,doe,2222,1111\r\ncorp,m.twain@my.corp,8888,mark,twain,6666,"

    val importDirdResult: String =
      """
        |{
        |  "failed": [
        |   {
        |     "line": 1,
        |     "errors": ["Some error"]
        |   }
        |  ],
        |  "created": [
        |    {
        |      "fax": "3333",
        |      "firstname": "john",
        |      "mobile": "2222",
        |      "lastname": "doe",
        |      "company": "corp",
        |      "number": "1111",
        |      "id": "d0e4286b-b3e3-4d25",
        |      "email": "j.doe@my.corp"
        |    }
        |  ]
        |}
      """.stripMargin
    val importResult: JsValue = Json.parse(importDirdResult)

    val contactListRaw: String =
      """
        | {
        |   "items": [
        |   {
        |     "id": "d0e4286b-b3e3-4d25",
        |     "firstname": "Mike",
        |     "lastname": "Swarm",
        |     "number": "1234",
        |     "mobile": "4321",
        |     "fax": "4444",
        |     "email": "m.swarm@mail.com",
        |     "company" : "myCompany"
        |   },
        |   {
        |     "id": "efa4282b-c1e3-8a27",
        |     "firstname": "John",
        |     "lastname": "Doe",
        |     "number": "1111",
        |     "mobile": "2222",
        |     "fax": "3333",
        |     "email": "j.doe@mail.com",
        |     "company" : "myCompany"
        |  }]
        |}
      """.stripMargin
    val contactList: JsValue = Json.parse(contactListRaw)
  }
}
