package xivo.directory

import org.apache.pekko.testkit.TestActorRef
import pekkotest.TestKitSpec
import models.XivoUser
import models.ws.auth.ApiUser
import org.mockito.Mockito.{verify, when}
import org.scalatestplus.mockito.MockitoSugar
import services.config.{DeleteAllEntries, GetAllEntries}
import xivo.directory.PersonalContactRepository.{
  DeletePersonalContact,
  PersonalContacts,
  SetPersonalContact
}
import xivo.models.PersonalContactResult

import scala.concurrent.Future

class PersonalContactRepositorySpec
    extends TestKitSpec("PersonalContactRepository")
    with MockitoSugar {

  "PersonalContactRepository" should {

    "get Personal Contact reverse lookup cache when user ask for history" in new Helper {
      val (ref, repo) = actor()
      ref ! GetAllEntries
      expectMsgType[PersonalContacts]
    }

    "check that dirdRequester is called at startup" in new Helper {
      val (ref, repo) = actor()

      verify(dirdRequester).list(apiUser)
      ref ! GetAllEntries

      expectMsg(PersonalContacts(Map.empty))
    }

    "fills repository cache at startup" in new Helper {
      val (ref, repo) = actor(withPC = true)

      ref ! GetAllEntries

      val entries: PersonalContacts = expectMsgType[PersonalContacts]
      entries.map should contain.only(
        ("1000", pc1),
        ("1001", pc1),
        ("3000", pc3)
      )
    }

    "not add PersonalContact to repository cache" in new Helper {
      val (ref, repo) = actor()

      val pc: PersonalContactResult = PersonalContactResult(
        "1",
        Some("Jane"),
        Some("Block"),
        None,
        None,
        None,
        Some("j.block@mail.com"),
        Some("myCompany")
      )
      ref ! SetPersonalContact(pc)

      ref ! GetAllEntries
      expectMsg(PersonalContacts(Map.empty))
    }

    "delete all PersonalContact from repository cache" in new Helper {
      val (ref, repo) = actor(withPC = true)

      ref ! DeleteAllEntries
      ref ! GetAllEntries
      expectMsg(PersonalContacts(Map.empty))
    }

    "add PersonalContact to repository cache" in new Helper {
      val (ref, repo) = actor()

      val pc: PersonalContactResult = PersonalContactResult(
        "1",
        Some("Jane"),
        Some("Block"),
        Some("4000"),
        None,
        None,
        Some("j.block@mail.com"),
        Some("myCompany")
      )
      ref ! SetPersonalContact(pc)

      ref ! GetAllEntries
      expectMsg(PersonalContacts(Map("4000" -> pc)))
    }

    "update name of PersonalContact in repository cache" in new Helper {
      val (ref, repo) = actor(withPC = true)

      val pc: PersonalContactResult = PersonalContactResult(
        "1",
        Some("Mike Jr"),
        Some("Swarm"),
        Some("1000"),
        None,
        Some("1001"),
        Some("m.swarm@mail.com"),
        Some("myCompany")
      )
      ref ! SetPersonalContact(pc)

      ref ! GetAllEntries

      val entries: PersonalContacts = expectMsgType[PersonalContacts]
      entries.map should contain.only(("1000", pc), ("1001", pc), ("3000", pc3))

    }

    "update numbers of a PersonalContact in repository cache" in new Helper {
      val (ref, repo) = actor(withPC = true)

      val pc: PersonalContactResult =
        pc1.copy(number = Some("1010"), mobile = Some("1011"))

      ref ! SetPersonalContact(pc)
      ref ! GetAllEntries
      val entries: PersonalContacts = expectMsgType[PersonalContacts]
      entries.map should contain.only(
        ("1010", pc),
        ("1011", pc),
        ("1001", pc),
        ("3000", pc3)
      )
    }

    "delete PersonalContact from repository cache" in new Helper {
      val (ref, repo) = actor(withPC = true)

      val pcId = "1"
      ref ! DeletePersonalContact(pcId)

      ref ! GetAllEntries
      expectMsg(PersonalContacts(Map("3000" -> pc3)))
    }

  }

  class Helper {
    val user: XivoUser =
      XivoUser(1, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
    val apiUser: ApiUser =
      ApiUser(user.username, user.id)
    val dirdRequester: DirdRequester = mock[DirdRequester]

    val pc1: PersonalContactResult = PersonalContactResult(
      "1",
      Some("Mike"),
      Some("Swarm"),
      Some("1000"),
      None,
      Some("1001"),
      Some("m.swarm@mail.com"),
      Some("myCompany")
    )
    val pc2: PersonalContactResult = PersonalContactResult(
      "2",
      Some("John"),
      Some("Doe"),
      None,
      None,
      None,
      Some("j.doe@mail.com"),
      Some("myCompany")
    )
    val pc3: PersonalContactResult = PersonalContactResult(
      "3",
      Some("David"),
      Some("Cross"),
      None,
      Some("3000"),
      None,
      Some("d.cross@mail.com"),
      Some("myCompany")
    )

    def actor(withPC: Boolean = false): (TestActorRef[PersonalContactRepository], PersonalContactRepository) = {
      val emptyList = List.empty
      val pcList: List[PersonalContactResult] = List(
        pc1,
        pc2,
        pc3
      )
      val returnList = if (withPC) pcList else emptyList
      when(dirdRequester.list(apiUser))
        .thenReturn(Future.successful(returnList))

      val a = TestActorRef[PersonalContactRepository](
        new PersonalContactRepository(user, dirdRequester)
      )
      (a, a.underlyingActor)
    }
  }

}
