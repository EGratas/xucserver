package xivo.events

import pekkotest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar
import xivo.xucami.models.Channel

class UserDataSpec extends TestKitSpec("UserDataSpec") with MockitoSugar {

  val systemData: Map[String,String] = Map(
    "XIVO_USERID"                -> "12",
    "XIVO_SRCNUM"                -> "+3254",
    "XIVO_CONTEXT"               -> "default",
    "XIVO_EXTENPATTERN"          -> "X.",
    "XIVO_DSTNUM"                -> "456",
    "XIVO_REVERSE_LOOKUP"        -> "",
    Channel.VarNames.xucCallType -> "xucCallType",
    "XIVO_DST_FIRSTNAME"         -> "firstname",
    "XIVO_DST_LASTNAME"          -> "lastname",
    "SIPCALLID"                  -> "456-54@129.1:20"
  )
  val userData: Map[String,String] =
    Map("USR_some_data" -> "user data", "USR_other_data" -> "other user data")

  "User data" should {
    "filter system variables" in {
      val result =
        UserData.filterData(Map("TO_BE_FILTERED" -> "easy") ++ systemData)
      result.shouldEqual(systemData)
    }

    "filter user variables" in {
      val result =
        UserData.filterData(Map("TO_BE_FILTERED" -> "easy") ++ userData)
      result.shouldEqual(userData)
    }
  }
}
