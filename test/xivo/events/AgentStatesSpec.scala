package xivo.events

import org.joda.time.DateTime
import xivo.xucami.models.MonitorState
import xuctest.{BaseTest, JsonMatchers}

class AgentStatesSpec extends BaseTest with JsonMatchers {
  import AgentState._
  import xivo.events.CallDirection._

  val agentId = 5
  val now     = new DateTime

  "AgentState" should {

    "return a complete json transform for agent pause" in {
      val s: String = AgentState
        .toJson(
          AgentOnPause(
            agentId,
            new DateTime().minusSeconds(57),
            "2000",
            List(2, 4, 9),
            Some("dejeuner"),
            "2000"
          )
        )
        .toString

      s should matchJson(
        """{"name":"AgentOnPause","agentId":5,"phoneNb":"2000","since":57,"queues":[2,4,9],"cause":"dejeuner"}"""
          .replace(" ", "")
      )
    }

    "return a complete json transform for agent on call" in {
      val s: String = AgentState
        .toJson(
          AgentOnCall(
            agentId,
            new DateTime().minusSeconds(52),
            false,
            DirectionUnknown,
            "2000",
            List(2, 4, 9),
            onPause = true,
            monitorState = MonitorState.PAUSED,
            agentNb = "2001"
          )
        )
        .toString
        .replace(" ", "")

      s should matchJson(
        """{"agentId":5,"phoneNb":"2000","acd":false,"queues":[2,4,9],"name":"AgentOnCall","cause":"","monitorState":"PAUSED","since":52,"direction":"DirectionUnknown"}"""
          .replace(" ", "")
      )
    }
  }
}
