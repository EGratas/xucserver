package xivo.xucami.models

import org.asteriskjava.manager.event.{QueueCallerJoinEvent, QueueCallerLeaveEvent}
import org.joda.time.DateTime
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class QueueCallSpec extends AnyWordSpec with Matchers with MockitoSugar {

  "A QueueCall" should {
    "be created from a QueueCallerJoinEvent" in {
      val event      = mock[QueueCallerJoinEvent]
      val position   = 3
      val dateJoined = new DateTime()
      val name       = "foo"
      val number     = "3315987"
      val channel    = "SIP/abcd-000001"
      when(event.getCallerIdName).thenReturn(name)
      when(event.getCallerIdNum).thenReturn(number)
      when(event.getPosition).thenReturn(position)
      when(event.getDateReceived).thenReturn(dateJoined.toDate)
      when(event.getChannel).thenReturn(channel)

      QueueCall.from(event, "main") shouldEqual QueueCall(
        position,
        Some(name),
        number,
        dateJoined,
        channel,
        "main"
      )
    }
  }

  "An EnterQueue" should {
    "be created from a QueueCallerJoinEvent" in {
      val event      = mock[QueueCallerJoinEvent]
      val position   = 3
      val dateJoined = new DateTime()
      val name       = "foo"
      val number     = "3315987"
      val queue      = "bar"
      val uniqueid   = "123456.789"
      val channel    = "SIP/abcd-000001"
      when(event.getCallerIdName).thenReturn(name)
      when(event.getCallerIdNum).thenReturn(number)
      when(event.getPosition).thenReturn(position)
      when(event.getDateReceived).thenReturn(dateJoined.toDate)
      when(event.getQueue).thenReturn(queue)
      when(event.getUniqueId).thenReturn(uniqueid)
      when(event.getChannel).thenReturn(channel)

      EnterQueue(event, "main") shouldEqual EnterQueue(
        queue,
        uniqueid,
        QueueCall(position, Some(name), number, dateJoined, channel, "main"),
        channel
      )
    }
  }

  "A LeaveQueue" should {
    "be created from a QueueCallerLeaveEvent" in {
      val event    = mock[QueueCallerLeaveEvent]
      val queue    = "bar"
      val uniqueId = "12456.789"
      val dateLeft = new DateTime()
      val channel  = "SIP/abcd-000001"
      when(event.getDateReceived).thenReturn(dateLeft.toDate)
      when(event.getQueue).thenReturn(queue)
      when(event.getUniqueId).thenReturn(uniqueId)
      when(event.getChannel).thenReturn(channel)

      LeaveQueue(event) shouldEqual LeaveQueue(
        queue,
        uniqueId,
        dateLeft,
        channel
      )
    }

  }

}
