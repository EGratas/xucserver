package xivo.xucami.ami

import org.apache.pekko.actor.{Cancellable, PoisonPill}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.asteriskjava.manager.action.*
import org.asteriskjava.manager.event.CoreShowChannelsCompleteEvent
import org.asteriskjava.manager.event.*
import org.asteriskjava.manager.response.CoreStatusResponse
import org.asteriskjava.manager.{ManagerConnection, ManagerConnectionFactory}
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.{clearInvocations, timeout, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.message.QueueStatistics
import org.xivo.cti.model.{Counter, StatName}
import services.{AmiEventHelper, XucAmiBus}
import services.XucAmiBus.*
import services.config.ConfigRepository
import xivo.models.QueueConfigUpdate
import xivo.xucami.models.{EnterQueue, LeaveQueue}
import xivo.xucami.userevents.{QueueMemberWrapupStartEvent, UserEventAgent}

import scala.collection.immutable.HashMap

class ManagerConnectorSpec
    extends TestKitSpec("ManagerConnectorSpec")
    with MockitoSugar
    with AmiEventHelper {

  class Helper() {
    val amiBus: XucAmiBus           = mock[XucAmiBus]
    val factory: ManagerConnectionFactory          = mock[ManagerConnectionFactory]
    val connection: ManagerConnection       = mock[ManagerConnection]
    val amiBusConnector: TestProbe  = TestProbe()
    val configDispatcher: TestProbe = TestProbe()
    val repo: ConfigRepository             = mock[ConfigRepository]

    when(repo.getQueue("0")).thenReturn(
      Some(
        new QueueConfigUpdate(
          0L,
          "",
          "",
          "",
          Some(""),
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          "",
          "",
          Some(0),
          Some(""),
          0,
          Some(0),
          Some(0),
          0,
          "",
          0
        )
      )
    )

    when(repo.getQueue("1")).thenReturn(None)
    def actor(): (TestActorRef[ManagerConnector], ManagerConnector) = {
      when(factory.createManagerConnection()).thenReturn(connection)
      val sa = TestActorRef(
        new ManagerConnector(
          amiBus,
          factory,
          "default",
          amiBusConnector.ref,
          configDispatcher.ref,
          repo
        )
      )
      (sa, sa.underlyingActor)
    }
  }

  "ManagerConnector" should {

    "initiate ManagerConnection" in new Helper() {
      var (ref, a) = actor()
      verify(factory).createManagerConnection()
      a.managerConnection shouldBe connection
      verify(a.managerConnection, timeout(250)).login()
    }

    "stop LoginTimeout if ConnectEvent received" in new Helper() {
      var (ref, a) = actor()
      a.loginTimeout = mock[Cancellable]
      ref ! new ConnectEvent("test")
      verify(a.loginTimeout).cancel()
    }

    "logoff and defer manager connection if the actor is terminated" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("testLogout")
      ref ! PoisonPill

      a.managerConnection shouldBe null
    }

    "publish Extension status event to the bus" in new Helper() {
      var (ref, a) = actor()

      ref ! new ConnectEvent("test")
      val event = new ExtensionStatusEvent("test")

      ref ! event

      verify(amiBus).publish(XucAmiBus.AmiExtensionStatusEvent(event))
    }

    "publish AgentConnect event to the bus with channel id" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val event = new AgentConnectEvent("agentConnectTest")
      ref ! event

      verify(amiBus).publish(AmiEvent(event))
    }

    "publish unprocessed events to the bus without channel" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val event = new ChannelReloadEvent("unprocessed")
      ref ! event

      verify(amiBus).publish(AmiEvent(event))
    }

    "subscribe to the bus for AmiActions" in new Helper() {
      var (ref, a) = actor()
      verify(amiBus).subscribe(ref, AmiType.AmiAction)
    }

    "send Manager Actions with refence to the ami and save them in pendingRequests with actionId" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val action: PauseMonitorAction = mock[PauseMonitorAction]
      ref ! AmiAction(action, Some("uniqueId3"))

      verify(action).setActionId("1")
      verify(connection).sendAction(action, a.managerListener)
      a.pendingRequests.get(1) shouldBe Some(
        AmiAction(action, Some("uniqueId3"))
      )
    }

    "send Manager Actions only if targetMds match current mds if defined" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val action: PauseMonitorAction = mock[PauseMonitorAction]
      val action2: UnpauseMonitorAction = mock[UnpauseMonitorAction]

      ref ! AmiAction(action, Some("uniqueId3"), targetMds = Some("hole"))
      ref ! AmiAction(action2, Some("uniqueId4"), targetMds = Some("default"))

      verify(action2).setActionId("1")
      verify(connection).sendAction(action2, a.managerListener)
      a.pendingRequests.size shouldBe 1
      a.pendingRequests.get(1) shouldBe Some(
        AmiAction(action2, Some("uniqueId4"), targetMds = Some("default"))
      )
    }

    "publish information when logged" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")
      verify(amiBus).publish(AmiConnected("default"))
    }

    "buffer Manager Actions when waiting for login and send them once logged" in new Helper() {
      var (ref, a) = actor()
      val action1  = new PauseMonitorAction()
      val action2  = new UnpauseMonitorAction()
      ref ! AmiAction(action1)
      ref ! AmiAction(action2)
      ref ! new ConnectEvent("test")

      verify(connection).sendAction(action1, a.managerListener)
      verify(connection).sendAction(action2, a.managerListener)
    }

    "publish Ami responses to the bus with corresponding action" in new Helper() {
      var (ref, a) = actor()
      val action   = new CoreStatusAction()
      a.pendingRequests = HashMap(2L -> AmiAction(action))
      val response = new CoreStatusResponse()
      response.setActionId("2")
      a.logged(response)

      a.pendingRequests.get(2) shouldBe None
      verify(amiBus).publish(AmiResponse((response, Some(AmiAction(action)))))
    }

    "Ami Manager responses with non integer value shouldn't throw an exception " in new Helper() {
      var (ref, a) = actor()
      val response = new CoreStatusResponse()
      response.setActionId("originate-fa38ee68d953e9b1e43")
      a.logged(response)
    }

    "Ami response events with non integer value shouldn't throw an exception " in new Helper() {
      var (ref, a) = actor()
      val response = new OriginateResponseEvent(this)
      response.setActionId("originate-fa38ee68d953e9b1e43")
      a.logged(response)
    }

    "publish Ami responses directly to requesting actor and not to the bus" in new Helper() {
      var (ref, a)  = actor()
      val action    = new CoreStatusAction()
      val amiAction: AmiAction = AmiAction(action, None, Some(testActor))
      a.pendingRequests = HashMap(2L -> amiAction)
      val response = new CoreStatusResponse()
      response.setActionId("2")
      val amiResponse: AmiResponse = AmiResponse((response, Some(amiAction)))
      a.logged(response)

      a.pendingRequests.get(2) shouldBe None
      expectMsg(amiResponse)
      verify(amiBus, timeout(500).times(0)).publish(amiResponse)
    }

    "publish All Ami responses events when it's a multi response action request" in new Helper() {
      var (ref, a)  = actor()
      val action    = new CoreShowChannelsAction()
      val amiAction: AmiAction = AmiAction(action, None, Some(testActor))
      val response1: CoreShowChannelEvent = mock[CoreShowChannelEvent]
      response1.setActionId("2")
      val amiResponse1: AmiEvent = AmiEvent(response1)

      val response2: CoreShowChannelsCompleteEvent = mock[CoreShowChannelsCompleteEvent]
      response2.setActionId("2")
      val amiResponse2: AmiEvent = AmiEvent(response2)

      a.logged(response1)
      a.logged(response2)
      a.pendingRequests shouldBe empty
    }

    "publish Ami user event agent to bus as AmiAgentEvent" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val userEventAgent: UserEventAgent = mock[UserEventAgent]

      ref ! userEventAgent

      verify(amiBus).publish(AmiAgentEvent(userEventAgent))
    }

    "publish Ami QueueMemberPauseEvent to bus as AmiAgentEvent" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val evt: QueueMemberPauseEvent = mock[QueueMemberPauseEvent]

      ref ! evt

      verify(amiBus).publish(AmiAgentEvent(evt))
    }

    "publish Ami QueueMemberWrapupStartEvent to bus as AmiAgentEvent" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val evt: QueueMemberWrapupStartEvent = mock[QueueMemberWrapupStartEvent]

      ref ! evt

      verify(amiBus).publish(AmiAgentEvent(evt))
    }

    "publish AMI QueueMemberEvent to bus as AmiAgentEvent" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val evt: QueueMemberEvent = mock[QueueMemberEvent]

      ref ! evt

      verify(amiBus).publish(AmiAgentEvent(evt))
    }

    "publish AMI QueueEntryEvent to bus as AmiAgentEvent" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val evt: QueueEntryEvent = mock[QueueEntryEvent]

      ref ! evt

      verify(amiBus).publish(AmiAgentEvent(evt))
    }

    "publish an EnterQueue event on the bus when receiving a QueueCallerJoinEvent" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val joinEvent: QueueCallerJoinEvent = mock[QueueCallerJoinEvent]

      clearInvocations(amiBus)
      ref ! joinEvent

      verify(amiBus).publish(any[EnterQueue])
    }

    "publish a LeaveQueue event on the bus when receiving a QueueCallerLeaveEvent" in new Helper() {
      var (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val leaveEvent: QueueCallerLeaveEvent = mock[QueueCallerLeaveEvent]

      clearInvocations(amiBus)
      ref ! leaveEvent

      verify(amiBus).publish(any[LeaveQueue])
    }

    "increase the key in pending requests" in new Helper() {
      var (ref, a) = actor()
      val action   = new ExtensionStateAction("1000", "")

      1 to 10 foreach { _ => a.logged(AmiAction(action)) }

      a.pendingRequests.keys.size shouldEqual 10
      a.pendingRequests.keys.toSet shouldEqual Set(1, 2, 3, 4, 5, 6, 7, 8, 9,
        10)

    }

    "ignore AMI Event Queue Summary Event if the queue is not found" in new Helper() {
      var (ref, a) = actor()

      ref ! new ConnectEvent("test")
      val failEvent = new QueueSummaryEvent("test")
      failEvent.setQueue("1")
      failEvent.setAvailable(1)
      failEvent.setCallers(2)
      failEvent.setHoldTime(3)
      failEvent.setLongestHoldTime(4)

      val failQueueStatistics = new QueueStatistics
      failQueueStatistics.setQueueId(
        0
      )
      failQueueStatistics.addCounter(
        new Counter(StatName.AvailableAgents, 1)
      )
      failQueueStatistics.addCounter(
        new Counter(StatName.TalkingAgents, 2)
      )
      failQueueStatistics.addCounter(
        new Counter(StatName.EWT, 3)
      )
      failQueueStatistics.addCounter(
        new Counter(StatName.LongestWaitTime, 4)
      )
      ref.receive(AmiEvent(failEvent))
      configDispatcher.expectNoMessage()
    }
  }
}
