package xivo.phonedevices

import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.SipDriver.{PJSIP, SIP, SipDriver}
import xivo.phonedevices.YealinkDevice.Keys
import xivo.xuc.XucConfig
import xivo.models.Line

class YealinkDeviceSpec extends TestKitSpec("Yealink") with MockitoSugar {

  import xivo.models.LineHelper.makeLine

  class Helper(driver: SipDriver) {
    val config: XucConfig = mock[XucConfig]
    when(config.sipDriver).thenReturn(driver)

    val lineSip: Line = makeLine(1, "default", "SIP", "ihvbur", None, None, "ip", driver = driver)
    val yealinkSip = new YealinkDevice("192.168.56.3", lineSip, "1100", config)

    val sender: TestProbe = TestProbe()
  }
  "A yealink device" should {

    "on answer reply with a sip notify request with ok command" in new Helper(SIP) {
      yealinkSip.answer(None, sender.ref)

      sender.expectMsg(SipNotifyCommand("SIP/ihvbur", yealinkSip.xml(Keys.ok), SIP))
    }
    "on hold reply with a sip notify request with hold command" in new Helper(SIP) {
      yealinkSip.hold(None, sender.ref)

      sender.expectMsg(SipNotifyCommand("SIP/ihvbur", yealinkSip.xml(Keys.hold), SIP))
    }
    "on answer reply with a pjsip notify request with ok command" in new Helper(PJSIP) {
      yealinkSip.answer(None, sender.ref)

      sender.expectMsg(SipNotifyCommand("PJSIP/ihvbur", yealinkSip.xml(Keys.ok), PJSIP))
    }
    "on hold reply with a pjsip notify request with hold command" in new Helper(PJSIP) {
      yealinkSip.hold(None, sender.ref)

      sender.expectMsg(SipNotifyCommand("PJSIP/ihvbur", yealinkSip.xml(Keys.hold), PJSIP))
    }
  }

}
