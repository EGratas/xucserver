package xivo.phonedevices

import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.SipDriver.{PJSIP, SIP, SipDriver}
import xivo.models.XivoDevice
import xivo.phonedevices.AastraDevice.Keys
import xivo.xuc.XucConfig
import xivo.models.Line

class AastraDeviceSpec extends TestKitSpec("Yealink") with MockitoSugar {
  import xivo.models.LineHelper.makeLine

  class Helper(driver: SipDriver) {
    val config: XucConfig = mock[XucConfig]
    when(config.sipDriver).thenReturn(driver)
    val sipLine: Line = makeLine(53, "default", "sip", "4ylkfc18",
      Some(XivoDevice("002e99695", Some("10.50.2.109"), "Aastra")), None, "ip", driver = driver,
    )

    val astraSip = new AastraDevice("192.168.56.3", sipLine, "1100", config)
    val sender: TestProbe = TestProbe()
  }
  "An aastra device" should {

    "on answer reply with a sip notify request with line1 command" in new Helper(SIP) {
      astraSip.answer(None, sender.ref)

      sender.expectMsg(SipNotifyCommand("SIP/4ylkfc18", astraSip.xml(Keys.Line1), SIP))
    }

    "on hold reply with a sip notify request with hold command" in new Helper(SIP) {
      astraSip.hold(None, sender.ref)

      sender.expectMsg(SipNotifyCommand("SIP/4ylkfc18", astraSip.xml(Keys.hold), SIP))
    }

    "on answer reply with a pjsip notify request with line1 command" in new Helper(PJSIP) {
      astraSip.answer(None, sender.ref)

      sender.expectMsg(SipNotifyCommand("PJSIP/4ylkfc18", astraSip.xml(Keys.Line1), PJSIP))
    }
    "on hold reply with a pjsip notify request with hold command" in new Helper(PJSIP) {
      astraSip.hold(None, sender.ref)

      sender.expectMsg(SipNotifyCommand("PJSIP/4ylkfc18", astraSip.xml(Keys.hold), PJSIP))
    }
  }

}
