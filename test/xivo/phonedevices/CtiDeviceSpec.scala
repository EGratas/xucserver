package xivo.phonedevices

import org.apache.pekko.actor.ActorRef
import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import org.joda.time.DateTime
import org.mockito.Mockito.verify
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Helpers.*
import services.XucAmiBus.{DialActionRequest, DialFromMobileActionRequest}
import services.calltracking.{DeviceCall, SipDriver}
import xivo.models.Line
import xivo.xuc.XucConfig
import xivo.xucami.models.{CallerId, QueueCall}
import play.api.Application

class CtiDeviceSpec
    extends TestKitSpec("CtiDeviceSpec")
    with MockitoSugar
    with GuiceOneAppPerSuite {

  import xivo.models.LineHelper.makeLine

  var ctiDevice: CtiDevice = _

  val xivoHost       = "10.20.30.50"
  val outboundLength = 6

  val testConfig: Map[String, Any] = {
    Map("xuc.outboundLength" -> outboundLength, "xivohost" -> xivoHost)
  }
  val line: Line        = makeLine(1, "default", "SIP", "ihvbur", None, None, "ip")
  val phoneNumber = "1000"

  override def fakeApplication(): Application =
    new GuiceApplicationBuilder(configuration = Configuration.from(testConfig))
      .build()

  override def beforeAll(): Unit = {
    ctiDevice =
      new CtiDevice(line, phoneNumber, new XucConfig(app.configuration))
  }

  "send dial cti command upon receipt of dial websocket command message" in {
    running(app) {
      val destination = "3567"

      val sender = TestProbe()

      ctiDevice.dial(destination, Map("VAR" -> "Value"), sender.ref)

      sender.expectMsg(
        StandardDial(line, destination, Map("VAR" -> "Value"), xivoHost, line.driver)
      )
    }
  }

  "send listen callback message cti command upon receipt of listenCallbackMessage websocket command message" in {
    running(app) {
      val voiceMessageRef = "1499074373.5"

      val sender = TestProbe()

      ctiDevice.listenCallbackMessage(
        voiceMessageRef,
        Map("VAR" -> "Value"),
        sender.ref
      )

      sender.expectMsg(
        ListenCallbackMessage(
          line,
          voiceMessageRef,
          Map("VAR" -> "Value"),
          xivoHost,
          line.driver
        )
      )
    }
  }

  "send hangup command upon receipt of hangup websocket command message" in {

    val sender = TestProbe()

    ctiDevice.hangup(sender.ref)

    sender.expectMsg(HangupCommand(line, phoneNumber))
  }

  "send attended transfer command  upon receipt of attended transfer websocket command message" in {
    val destination = "3568"

    val sender = TestProbe()

    ctiDevice.attendedTransfer(destination, sender.ref)

    sender.expectMsg(AttendedTransferCommand(line, destination))
  }

  "send complete transfer command upon receipt of complete transfer websocket command message" in {

    val sender = TestProbe()

    ctiDevice.completeTransfer(sender.ref)

    sender.expectMsg(CompleteTransferCommand(line))
  }

  "send cancel transfer command upon receipt of cancel transfer websocket command message" in {

    val sender = TestProbe()

    ctiDevice.cancelTransfer(sender.ref)

    sender.expectMsg(CancelTransferCommand(line, phoneNumber))
  }

  "Send direct transfer command on direct transfer phone request" in {
    val destination = "0765432145"

    val sender = TestProbe()

    ctiDevice.directTransfer(destination, sender.ref)

    sender.expectMsg(DirectTransferCommand(line, destination))
  }

  "a odial" should {

    val line1 = makeLine(1, "default", "SIP", "callerLine", None, None, "ip")

    class OdialHelper(val line: Line = line1, val phoneNb: String = "1000") {

      val mockDial: (String, Map[String, String], ActorRef) => Unit = mock[(String, Map[String, String], ActorRef) => Unit]
      val mockDialFromMobile: (String, String, String, String, Map[String, String], ActorRef, Long) => Unit = mock[
        (
            String,
            String,
            String,
            String,
            Map[String, String],
            ActorRef,
            Long
        ) => Unit
      ]

      class Device
          extends CtiDevice(line, phoneNb, new XucConfig(app.configuration)) {
        override def dial(
            destination: String,
            variables: Map[String, String],
            sender: ActorRef
        ): Unit =
          mockDial(destination, variables, sender)
        override def dialFromMobile(
            mobileNumber: String,
            destination: String,
            callerNumber: String,
            callerName: String,
            variables: Map[String, String],
            sender: ActorRef,
            xivoUserId: Long
        ): Unit =
          mockDialFromMobile(
            mobileNumber,
            destination,
            callerNumber,
            callerName,
            variables,
            sender,
            xivoUserId
          )
        override def conference(sender: ActorRef): Unit = {}
        override def attendedTransfer(
            destination: String,
            sender: ActorRef
        ): Unit = {}
        override def completeTransfer(sender: ActorRef): Unit = {}
        override def hold(
            call: Option[DeviceCall] = None,
            sender: ActorRef
        ): Unit = {}
        override def hangup(sender: ActorRef): Unit = {}
        override def cancelTransfer(sender: ActorRef): Unit = {}
        override def answer(
            call: Option[DeviceCall] = None,
            sender: ActorRef
        ): Unit = {}
      }

      val sender: TestProbe = TestProbe()
      val outDevice = new Device()
    }

    "return an outbound call if number length > configured length and outboutqueue configured" in new OdialHelper() {
      val destination = "012345678910"
      val agentId     = 58
      val queueNumber = "3000"
      val variables: Map[String, String] = Map("VAR" -> "value")
      val driver: SipDriver.Value = SipDriver.SIP
      running(app) {
        outDevice.odial(
          destination,
          agentId,
          queueNumber,
          variables,
          123L,
          sender.ref,
          driver
        )
        sender.expectMsg(
          OutboundDial(
            destination,
            agentId,
            queueNumber,
            variables,
            xivoHost,
            123L,
            driver
          )
        )
      }
    }

    "call adapter dial if destination length < configured length and outboutqueue configured" in new OdialHelper() {
      val destination = "1250"
      val queueNumber = "3000"
      val variables: Map[String, String] = Map("VAR" -> "value")
      val driver: SipDriver.Value = SipDriver.SIP

      running(app) {
        outDevice.odial(
          destination,
          58,
          queueNumber,
          variables,
          123L,
          sender.ref,
          driver
        )
        verify(mockDial)(destination, variables, sender.ref)
      }

    }
    "call adapter dial if destination length > configured length and outboutqueue not configured" in new OdialHelper() {
      val destination = "12504567987"
      val queueNumber = ""
      val variables: Map[String, String] = Map("VAR" -> "value")
      val driver: SipDriver.Value = SipDriver.SIP

      running(app) {
        outDevice.odial(
          destination,
          58,
          queueNumber,
          variables,
          123L,
          sender.ref,
          driver
        )

        verify(mockDial)(destination, variables, sender.ref)
      }
    }
  }

  "StandardDial" should {
    "return DialActionRequest when the requested line exists" in {
      val callerId = CallerId("Some One", "1000")
      val line = makeLine(
        1,
        "default",
        "SIP",
        "callerLine",
        None,
        None,
        "ip",
        callerId = callerId
      )

      val sd =
        StandardDial(line, "0230210000", Map("VAR" -> "Value"), "xivoHost", line.driver)

      sd.toAmi() shouldBe Some(
        DialActionRequest(
          "SIP/callerLine",
          "default",
          callerId,
          "0230210000",
          Map("VAR" -> "Value"),
          "xivoHost",
          SipDriver.SIP
        )
      )
    }
    "return DialActionRequest with the custom interface when the requested line exists" in {
      val callerId = CallerId("Some One", "1000")
      val line = makeLine(
        1,
        "default",
        "CUSTOM",
        "local/01321564@default/n",
        None,
        None,
        "ip",
        callerId = callerId
      )

      val sd =
        StandardDial(line, "0230210000", Map("VAR" -> "Value"), "xivoHost", line.driver)

      sd.toAmi() shouldBe Some(
        DialActionRequest(
          "Local/01321564@default",
          "default",
          callerId,
          "0230210000",
          Map("VAR" -> "Value"),
          "xivoHost",
          SipDriver.SIP
        )
      )
    }
  }

  "DialFromMobile" should {
    "return DialFromMobileActionRequest when the requested line exists" in {
      val line = makeLine(1, "default", "SIP", "callerLine", None, None, "ip")

      val sd = DialFromMobile(
        "123456789",
        "0230210000",
        "123",
        "Isaac Newton",
        Map("VAR" -> "Value"),
        "xivoHost",
        1
      )

      sd.toAmi() shouldBe Some(
        DialFromMobileActionRequest(
          "123456789",
          "0230210000",
          "123",
          "Isaac Newton",
          Map("VAR" -> "Value"),
          "xivoHost",
          1
        )
      )
    }
    "return DialActionRequestion with the custom interface when the requested line exists" in {
      val line = makeLine(
        1,
        "default",
        "CUSTOM",
        "local/01321564@default/n",
        None,
        None,
        "ip"
      )

      val sd = DialFromMobile(
        "123456789",
        "0230210000",
        "123",
        "Isaac Newton",
        Map("VAR" -> "Value"),
        "xivoHost",
        1
      )

      sd.toAmi() shouldBe Some(
        DialFromMobileActionRequest(
          "123456789",
          "0230210000",
          "123",
          "Isaac Newton",
          Map("VAR" -> "Value"),
          "xivoHost",
          1
        )
      )
    }
  }

  "Send retrieve queue call command on incoming websocket message" in {
    val sourceNumber  = "1200"
    val sourceName    = "Isaac Asimov"
    val sourceChannel = "SIP/abcd"
    val queueCall = QueueCall(
      1,
      Some("Graham Bell"),
      "12345",
      DateTime.now,
      "SIP/defg",
      "main"
    )
    val callId    = "123456.789"
    val queueName = "__switchboard"
    val agentNum  = "2100"
    val driver = SipDriver.SIP

    val sender = TestProbe()

    ctiDevice.retrieveQueueCall(
      sourceChannel,
      Some(sourceNumber),
      sourceName,
      queueCall,
      Map.empty,
      sender.ref,
      callId,
      Some(queueName),
      Some(agentNum),
      autoAnswer = true,
      driver
    )

    sender.expectMsg(
      SetVarCommand(queueCall.channel, "XIVO_RETRIEVE_QUEUE_NAME", queueName)
    )
    sender.expectMsg(SetVarCommand(queueCall.channel, "XIVO_UNIQUE_ID", callId))
    sender.expectMsg(
      SetVarCommand(queueCall.channel, "XIVO_AGENT_NUM", agentNum)
    )
    sender.expectMsg(
      RetrieveQueueCall(
        sourceChannel,
        Some(sourceNumber),
        sourceName,
        queueCall.channel,
        queueCall.number,
        queueCall.name,
        Map.empty,
        xivoHost,
        callId,
        Some(queueName),
        Some(agentNum),
        autoAnswer = true,
        SipDriver.SIP
      )
    )
  }
}
