package xivo.phonedevices

import org.apache.pekko.stream.Materializer
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.ws.WSClient
import xivo.models.XivoDevice
import xivo.xuc.XucConfig
import xuctest.BaseTest

class DeviceAdapterFactorySpec extends BaseTest with MockitoSugar {

  import xivo.models.LineHelper.makeLine

  var deviceAdapterFactory: DeviceAdapterFactory =
    new DeviceAdapterFactory(mock[WSClient], mock[XucConfig])(
      mock[Materializer]
    )

  "factory" should {

    "get a default cti adapter when phone type is not known" in {
      val line = makeLine(
        56,
        "default",
        "sip",
        "ojhf",
        Some(
          XivoDevice(
            "ecbe7520c44c481cbf7bbc196294f27c",
            Some("10.50.2.104"),
            "Unknown"
          )
        ),
        None,
        "ip"
      )
      val lineNumber = "1000"

      val adapter = deviceAdapterFactory.getAdapter(line, lineNumber)

      adapter shouldBe a[CtiDevice]

    }

    "get a default cti adapter when no device is present" in {
      val line       = makeLine(56, "default", "sip", "ojhf", None, None, "ip")
      val lineNumber = "1000"

      val adapter = deviceAdapterFactory.getAdapter(line, lineNumber)

      adapter shouldBe a[CtiDevice]

    }
    "get a default cti adapter when no ip address is present" in {
      val line = makeLine(
        56,
        "default",
        "sip",
        "ojhf",
        Some(XivoDevice("ecbe7520c44c481cbf7bbc196294f27c", None, "Snom")),
        None,
        "ip"
      )
      val lineNumber = "1000"

      val adapter = deviceAdapterFactory.getAdapter(line, lineNumber)

      adapter shouldBe a[CtiDevice]

    }

    "get a default webrtc adapter when line is webrtc" in {
      val line =
        makeLine(56, "default", "sip", "ojhf", None, None, "ip", webRTC = true)
      val lineNumber = "1000"

      val adapter = deviceAdapterFactory.getAdapter(line, lineNumber)

      adapter shouldBe a[WebRTCDevice]
    }

    "get a Snom adapter when phone type is a Snom" in {
      val line = makeLine(
        56,
        "default",
        "sip",
        "ojhf",
        Some(
          XivoDevice(
            "ecbe7520c44c481cbf7bbc196294f27c",
            Some("10.50.2.104"),
            "Snom"
          )
        ),
        None,
        "ip"
      )
      val lineNumber = "1000"

      val adapter = deviceAdapterFactory.getAdapter(line, lineNumber)

      adapter shouldBe a[SnomDevice]
    }

    "get a Polycom adapter when phone type is a Polycom" in {
      val line = makeLine(
        56,
        "default",
        "sip",
        "ojhf",
        Some(
          XivoDevice(
            "ecbe7520c44c481cbf7bbc196294f27c",
            Some("10.50.2.104"),
            "Polycom"
          )
        ),
        None,
        "ip"
      )
      val lineNumber = "1000"

      val adapter = deviceAdapterFactory.getAdapter(line, lineNumber)

      adapter shouldBe a[PolycomDevice]
    }

    "get a Yealink adapter when phone type is a Yealink" in {
      val line = makeLine(
        56,
        "default",
        "sip",
        "ojhf",
        Some(
          XivoDevice(
            "ecbe7520c44c481cbf7bbc196294f27c",
            Some("10.50.2.124"),
            "Yealink"
          )
        ),
        None,
        "ip"
      )
      val lineNumber = "1100"

      val adapter = deviceAdapterFactory.getAdapter(line, lineNumber)

      adapter shouldBe a[YealinkDevice]

    }

    "get a Aastra adapter when phone type is a Aastra" in {
      val line = makeLine(
        53,
        "default",
        "sip",
        "4ylkfc18",
        Some(XivoDevice("002e99695", Some("10.50.2.109"), "Aastra")),
        None,
        "ip"
      )
      val lineNumber = "1100"

      val adapter = deviceAdapterFactory.getAdapter(line, lineNumber)

      adapter shouldBe a[AastraDevice]

    }
  }

}
