package xivo.services

import org.apache.pekko.testkit.TestActorRef
import pekkotest.TestKitSpec
import models.ws.auth.AuthenticationException
import models.{Token, TokenRequest, XivoUser}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.mockito.Mockito.when
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.{WSRequest, WSResponse}
import play.api.test.Helpers._
import services.config.ConfigRepository
import xivo.network.XiVOWS
import xivo.services.XivoAuthentication.{AuthTimeSynchronizationError, Init}
import xivo.xuc.XucConfig

import scala.concurrent.Future
import models.WebServiceUser
import models.ws.auth.AuthenticationError
import play.api.Application
class XivoAuthenticationSpec
    extends TestKitSpec("XivoAuthenticationSpec")
    with MockitoSugar
    with GuiceOneAppPerSuite {

  val appliConfig: Map[String, Any] = {
    Map(
      "xivohost"                 -> "xivoIP",
      "xivoAuthPort"             -> 9497,
      "xivo.auth.defaultExpires" -> 3600
    )
  }

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration = Configuration.from(appliConfig))
      .build()

  class Helper {

    val repo: ConfigRepository = mock[ConfigRepository]
    val xivoWS: XiVOWS         = mock[XiVOWS]

    def actor(): (TestActorRef[XivoAuthentication], XivoAuthentication) = {
      val a = TestActorRef(
        new XivoAuthentication(repo, xivoWS, new XucConfig(app.configuration))
      )
      (a, a.underlyingActor)
    }
  }

  "XivoAuthentication actor " should {
    "get cti token from xivo when requested " in new Helper {
      running(app) {
        val (ref, _)  = actor()
        val userId    = 46
        val wsRequest = mock[WSRequest]
        when(repo.getCtiUser(46)).thenReturn(
          Some(
            XivoUser(
              46,
              None,
              None,
              "first",
              Some("last"),
              Some("login"),
              Some("pass"),
              None,
              None
            )
          )
        )
        when(
          xivoWS.post(
            "xivoIP",
            "0.1/token",
            Some(Json.toJson(TokenRequest("xivo_user", 3600))),
            Some("login"),
            Some("pass"),
            port = Some(9497)
          )
        ).thenReturn(wsRequest)
        val response = mock[WSResponse]
        val date     = new DateTime()
        val fmt      = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
        when(response.json).thenReturn(Json.parse(s"""{"data":
              | {
              |   "issued_at": "${fmt.print(date)}",
              |   "token": "5ca1d522-3f6b-1db9-339a-731e61c38871",
              |   "auth_id": "authId",
              |   "expires_at": "${fmt.print(date.plusHours(1))}",
              |   "xivo_user_uuid": "userId",
              |   "acls": []
              | }
              |}""".stripMargin))
        when(wsRequest.execute()).thenReturn(Future.successful(response))
        ref ! XivoAuthentication.GetCtiToken(userId)

        expectMsg(
          Token(
            "5ca1d522-3f6b-1db9-339a-731e61c38871",
            date.plusHours(1),
            date,
            "cti",
            Some("userId"),
            List.empty
          )
        )
      }
    }

    "get web service token from xivo when requested " in new Helper {
      running(app) {
        val (ref, _)  = actor()
        val wsRequest = mock[WSRequest]
        when(
          xivoWS.post(
            "xivoIP",
            "0.1/token",
            Some(Json.toJson(TokenRequest("xivo_service", 86400))),
            Some("login"),
            Some("password"),
            port = Some(9497)
          )
        ).thenReturn(wsRequest)
        when(repo.getWebServiceUser("login")).thenReturn(
          Some(
            WebServiceUser(
              "login",
              "password",
              "name of web service user"
            )
          )
        )
        val response = mock[WSResponse]
        val date     = new DateTime()
        val fmt      = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
        when(response.json).thenReturn(Json.parse(s"""{"data":
              | {
              |   "issued_at": "${fmt.print(date)}",
              |   "token": "5ca1d522-3f6b-1db9-339a-731e61c38871",
              |   "auth_id": "authId",
              |   "expires_at": "${fmt.print(date.plusDays(1))}",
              |   "xivo_user_uuid": "null",
              |   "acls": []
              | }
              |}""".stripMargin))
        when(wsRequest.execute()).thenReturn(Future.successful(response))
        ref ! XivoAuthentication.GetWebServiceToken("login", "password", 86400)

        expectMsg(
          Token(
            "5ca1d522-3f6b-1db9-339a-731e61c38871",
            date.plusDays(1),
            date,
            "webservice",
            None,
            List.empty
          )
        )
      }
    }

    "throw an authentication exception if the credentials are invalid " in new Helper {
      running(app) {
        val (ref, _) = actor()
        when(repo.getWebServiceUser("login")).thenReturn(
          Some(
            WebServiceUser(
              "login",
              "rightpassword",
              "name of web service user"
            )
          )
        )

        ref ! XivoAuthentication.GetWebServiceToken(
          "login",
          "wrongpassword",
          86400
        )

        expectMsgPF() {
          case e: AuthenticationException
              if e.error == AuthenticationError.InvalidCredentials =>
            ()
        }
      }
    }

    "throw an authentication exception web service user does not exist " in new Helper {
      running(app) {
        val (ref, _) = actor()
        when(repo.getWebServiceUser("wronglogin")).thenReturn(None)

        ref ! XivoAuthentication.GetWebServiceToken(
          "wronglogin",
          "password",
          86400
        )

        expectMsgPF() {
          case e: AuthenticationException
              if e.error == AuthenticationError.UserNotFound =>
            ()
        }
      }
    }

    """use saved token""" in new Helper {

      var (ref, a) = actor()
      val date     = new DateTime(2015, 11, 10, 1, 22, 3)
      val tcti: Token = Token("token", date, date, "cti", Some("authId"), List.empty)
      ref ! Init(Map(46L -> tcti))

      ref ! XivoAuthentication.GetCtiToken(46)
      expectMsg(tcti)

    }

    "renew token on expiration" in new Helper {
      running(app) {
        val (ref, _)  = actor()
        val userId    = 46
        val wsRequest = mock[WSRequest]
        when(repo.getCtiUser(46)).thenReturn(
          Some(
            XivoUser(
              46,
              None,
              None,
              "first",
              Some("last"),
              Some("login"),
              Some("pass"),
              None,
              None
            )
          )
        )
        when(
          xivoWS.post(
            "xivoIP",
            "0.1/token",
            Some(Json.toJson(TokenRequest("xivo_user", 3600))),
            Some("login"),
            Some("pass"),
            port = Some(9497)
          )
        ).thenReturn(wsRequest)
        val response = mock[WSResponse]
        val date     = new DateTime()
        val fmt      = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
        when(response.json)
          .thenReturn(Json.parse(s"""{"data":
                | {
                |   "issued_at": "${fmt.print(date)}",
                |   "token": "first",
                |   "auth_id": "authId",
                |   "expires_at": "${fmt.print(date.plusSeconds(1))}",
                |   "xivo_user_uuid": "userId",
                |   "acls": []
                | }
                |}""".stripMargin))
          .thenReturn(Json.parse(s"""{"data":
                | {
                |   "issued_at": "${fmt.print(date)}",
                |   "token": "second",
                |   "auth_id": "authId",
                |   "expires_at": "${fmt.print(date.plusDays(1))}",
                |   "xivo_user_uuid": "userId",
                |   "acls": []
                | }
                |}""".stripMargin))

        when(wsRequest.execute()).thenReturn(Future.successful(response))
        ref ! XivoAuthentication.GetCtiToken(userId)
        expectMsgClass(classOf[Token])
        Thread.sleep(2000)
        ref ! XivoAuthentication.GetCtiToken(userId)
        expectMsg(
          Token(
            "second",
            date.plusDays(1),
            date,
            "cti",
            Some("userId"),
            List.empty
          )
        )

      }
    }

    """send back AuthTimeSynchronizationError when xivo replies with an already expired token""" in
      new Helper() {
        running(app) {
          val (ref, _)  = actor()
          val userId    = 46
          val wsRequest = mock[WSRequest]
          when(repo.getCtiUser(46)).thenReturn(
            Some(
              XivoUser(
                46,
                None,
                None,
                "first",
                Some("last"),
                Some("login"),
                Some("pass"),
                None,
                None
              )
            )
          )
          when(
            xivoWS.post(
              "xivoIP",
              "0.1/token",
              Some(Json.toJson(TokenRequest("xivo_user", 3600))),
              Some("login"),
              Some("pass"),
              port = Some(9497)
            )
          ).thenReturn(wsRequest)
          val response = mock[WSResponse]
          val date     = new DateTime().minusMonths(1)
          val fmt      = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
          when(response.json)
            .thenReturn(Json.parse(s"""{"data":
                   | {
                   |   "issued_at": "${fmt.print(date)}",
                   |   "token": "first",
                   |   "auth_id": "authId",
                   |   "expires_at": "${fmt.print(date.plusDays(1))}",
                   |   "xivo_user_uuid": "userId",
                   |   "acls": []
                   | }
                   |}""".stripMargin))

          when(wsRequest.execute()).thenReturn(Future.successful(response))
          ref ! XivoAuthentication.GetCtiToken(userId)
          expectMsg(AuthTimeSynchronizationError)
        }
      }

    "return user associated to the token" in new Helper {
      var (ref, a) = actor()
      val date     = new DateTime(2015, 11, 10, 1, 22, 3)
      val t: Token = Token("token", date, date, "cti", None, List.empty)
      val user: XivoUser =
        XivoUser(46, None, None, "John", Some("Doe"), Some("j.doe"), None, None, None)
      ref ! Init(Map(user.id -> t))
      when(repo.getCtiUser(user.id)).thenReturn(Some(user))

      ref ! XivoAuthentication.UserByToken(t.token)

      expectMsg(Some(user))
    }

    "return None if there is no user for that token" in new Helper {
      var (ref, a) = actor()
      ref ! Init(Map.empty)
      ref ! XivoAuthentication.UserByToken("token")
      expectMsg(None)
    }

    "return Future[AuthenticationException] when token is asked but not existing" in new Helper()
      with ScalaFutures {
      val (ref, _) = actor()
      ref ! Init(Map.empty)
      when(repo.getCtiUser(1)).thenReturn(None)
      val f: Future[Token] = XivoAuthentication.getCtiTokenHelper(ref, 1)

      whenReady(f.failed) { result =>
        result shouldBe a[AuthenticationException]
      }
    }

    "return Future[Token] when token is asked and existing" in new Helper()
      with ScalaFutures {
      val (ref, _) = actor()
      val date     = new DateTime(2017, 11, 10, 1, 22, 3)
      val t: Token = Token("token", date, date, "cti", None, List.empty)
      ref ! Init(Map(88L -> t))

      val f: Future[Token] = XivoAuthentication.getCtiTokenHelper(ref, 88)
      whenReady(f) { result =>
        result should be(t)
      }
    }
  }
}
