package xivo.services

import java.net.URLEncoder
import org.apache.pekko.actor.{Actor, Props}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.{DirSearchResult, Token, XivoUser}
import org.joda.time.DateTime
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsString, JsValue, Json}
import play.api.libs.ws.{WSRequest, WSResponse}
import services.config.ConfigRepository
import services.directory.DirectoryTransformer.RawDirectoryResult
import services.request.*
import xivo.network.XiVOWS
import xivo.services.XivoAuthentication.GetCtiToken
import xivo.services.XivoDirectory.*
import xivo.xuc.XucConfig
import xuctest.XucUserHelper

import scala.concurrent.Future
import models.XucUser
import org.apache.pekko.actor.ActorRef
import play.api.Application

class XivoDirectorySpec
    extends TestKitSpec("XivoDirectorySpec")
    with MockitoSugar
    with GuiceOneAppPerSuite
    with XucUserHelper {

  val appliConfig: Map[String, Any] = {
    Map(
      "xivohost"     -> "xivoIP",
      "xivoAuthPort" -> 9497
    )
  }

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration = Configuration.from(appliConfig))
      .build()

  "XivoDirectory actor " should {
    "get token from xivoAuth and return search result for a given term" in new Helper {
      val (ref, _)  = actor()
      val result: DirSearchResult = mock[DirSearchResult]
      val wsRequest: WSRequest = mock[WSRequest]
      val url: String = "0.1/directories/lookup/default?term=" + URLEncoder.encode(
        "ben jones",
        "UTF-8"
      )
      when(
        xivoWS.get(
          "xivoIP",
          url,
          headers = Map("X-Auth-Token" -> token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)
      val dirdResult: JsValue = Json.parse(searchResponse)
      val response: WSResponse = mock[WSResponse]
      when(response.json).thenReturn(dirdResult)
      when(wsRequest.execute()).thenReturn(Future.successful(response))

      ref ! UserBaseRequest(requester.ref, DirectoryLookUp("ben jones"), user)

      directoryTransformer.expectMsg(
        RawDirectoryResult(
          requester.ref,
          DirLookupResult(DirSearchResult.parse(dirdResult)),
          userId
        )
      )
    }

    "get token from xivoAuth and return search result for favorites" in new Helper {
      val (ref, _)  = actor()
      val result: DirSearchResult = mock[DirSearchResult]
      val wsRequest: WSRequest = mock[WSRequest]
      when(
        xivoWS.get(
          "xivoIP",
          "0.1/directories/favorites/default",
          headers = Map("X-Auth-Token" -> token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)
      val dirdResult: JsValue = Json.parse(searchResponse)
      val response: WSResponse = mock[WSResponse]
      when(response.json).thenReturn(dirdResult)
      when(wsRequest.execute()).thenReturn(Future.successful(response))
      ref ! UserBaseRequest(requester.ref, GetFavorites, user)
      directoryTransformer.expectMsg(
        RawDirectoryResult(
          requester.ref,
          Favorites(DirSearchResult.parse(dirdResult)),
          userId
        )
      )
    }

    "get token from xivoAuth and add contact to favorites" in new Helper {
      val (ref, _)  = actor()
      val wsRequest: WSRequest = mock[WSRequest]
      val directory = "xivo-users"
      val contactId = "123"
      when(
        xivoWS.put(
          "xivoIP",
          s"0.1/directories/favorites/$directory/$contactId",
          headers = Map("X-Auth-Token" -> token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)
      val response: WSResponse = mock[WSResponse]
      when(response.status).thenReturn(204)
      when(wsRequest.execute()).thenReturn(Future.successful(response))
      ref ! UserBaseRequest(
        requester.ref,
        AddFavorite(contactId, directory),
        user
      )
      directoryTransformer.expectMsg(
        RawDirectoryResult(
          requester.ref,
          FavoriteUpdated(Action.Added, contactId, directory),
          userId
        )
      )
    }

    "get token from xivoAuth and remove contact from favorites" in new Helper {
      val (ref, _)  = actor()
      val wsRequest: WSRequest = mock[WSRequest]
      val directory = "xivo-users"
      val contactId = "123"
      when(
        xivoWS.del(
          "xivoIP",
          s"0.1/directories/favorites/$directory/$contactId",
          headers = Map("X-Auth-Token" -> token),
          port = Some(9489)
        )
      ).thenReturn(wsRequest)
      val response: WSResponse = mock[WSResponse]
      when(response.status).thenReturn(204)
      when(wsRequest.execute()).thenReturn(Future.successful(response))
      ref ! UserBaseRequest(
        requester.ref,
        RemoveFavorite(contactId, directory),
        user
      )
      directoryTransformer.expectMsg(
        RawDirectoryResult(
          requester.ref,
          FavoriteUpdated(Action.Removed, contactId, directory),
          userId
        )
      )
    }
  }

  "FavoriteUpdated case class" should {
    "provide implicit json writer" in {
      val result = Json.toJson(
        FavoriteUpdated(Action.Added, "contactIdValue", "sourceValue")
      )
      (result \ "action").get shouldEqual JsString("Added")
      (result \ "contact_id").get shouldEqual JsString("contactIdValue")
      (result \ "source").get shouldEqual JsString("sourceValue")
    }
  }

  class Helper {
    val appliConfig: Map[String, Any] = {
      Map(
        "xivohost"     -> "xivoIP",
        "xivoAuthPort" -> 9497
      )
    }

    val requester: TestProbe = TestProbe()
    val userId    = 45
    val token: String     = s"auth-toke-user-id$userId"
    val xivoAuth: ActorRef = system.actorOf(Props(new Actor() {
      def receive: Receive = { case GetCtiToken(id) =>
        if (id == userId) {
          sender() ! Token(
            token,
            new DateTime(),
            new DateTime(),
            "cti",
            Some(s"xivoId$userId"),
            List.empty
          )
        }
      }
    }))
    val directoryTransformer: TestProbe = TestProbe()
    val repo: ConfigRepository                 = mock[ConfigRepository]
    val user: XucUser                 = getXucUser("testUsername", "pass")
    when(repo.getCtiUser(user.username)).thenReturn(
      Some(
        XivoUser(
          userId,
          None,
          None,
          "tFirst",
          Some("tLast"),
          Some("testUsername"),
          Some("password"),
          None,
          None
        )
      )
    )
    val xivoWS: XiVOWS = mock[XiVOWS]
    val config         = new XucConfig(app.configuration)
    def actor(): (TestActorRef[XivoDirectory], XivoDirectory) = {
      val a = TestActorRef(
        new XivoDirectory(
          xivoAuth,
          directoryTransformer.ref,
          repo,
          xivoWS,
          config
        )
      )
      (a, a.underlyingActor)
    }

    val searchResponse: String =
      """
        |{
        |    "column_headers": [
        |        "Favoris",
        |        "Nom",
        |        "Numéro",
        |        "Mobile",
        |        "Autre numéro",
        |        "Personnel",
        |        "Email"
        |    ],
        |    "column_types": [
        |        "favorite",
        |        "name",
        |        "number",
        |        "mobile",
        |        "number",
        |        "personal",
        |        "name"
        |    ],
        |    "results": [
        |        {
        |            "column_values": [
        |                false,
        |                "Peter Pan",
        |                "44201",
        |                "+33061254658",
        |                "+33231568456",
        |                false,
        |                "pp@test.nodomain"
        |            ],
        |            "relations": {
        |                "agent_id": null,
        |                "endpoint_id": null,
        |                "source_entry_id": "565-qsdf-qsdf-5-555",
        |                "user_id": null,
        |                "xivo_id": null
        |            },
        |            "source": "avc"
        |        },
        |        {
        |            "column_values": [
        |                true,
        |                "Lord Aramis",
        |                "44205",
        |                "012345687",
        |                null,
        |                false,
        |                null
        |            ],
        |            "relations": {
        |                "agent_id": 4,
        |                "endpoint_id": 2,
        |                "source_entry_id": "1",
        |                "user_id": 111,
        |                "xivo_id": "3c3f8fe6-f776-4917-bc5d-1733975256d2"
        |            },
        |            "source": "internal"
        |        }
        |    ],
        |    "term": "44"
        |}
        |    """.stripMargin
  }

}
