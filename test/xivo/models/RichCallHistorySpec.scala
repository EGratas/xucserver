package xivo.models

import org.joda.time.{DateTime, Period}
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.model.PhoneHintStatus
import play.api.libs.json.{JsArray, Json}
import services.config.ConfigRepository
import services.video.model.VideoEvents
import xuctest.BaseTest

class RichCallHistorySpec extends BaseTest with MockitoSugar {

  val num1 = "1000"
  val num2 = "2000"
  val num3 = "3000"

  val date   = new DateTime
  val period: Some[Period] = Some(new Period)

  "A RichCallDetail" should {
    "be created from a CallDetail" in {
      val repo = mock[ConfigRepository]
      when(repo.userNameFromPhoneNb("4000")).thenReturn(Some("j.doe"))
      when(repo.userNameFromPhoneNb("4001")).thenReturn(Some("l.luke"))
      when(repo.getUserVideoStatus("4000"))
        .thenReturn(Some(VideoEvents.Available))
      when(repo.getUserVideoStatus("4001"))
        .thenReturn(Some(VideoEvents.Available))

      when(repo.richPhones).thenReturn(
        Map(
          "4000" -> PhoneHintStatus.BUSY,
          "4001" -> PhoneHintStatus.BUSY
        )
      )

      val detail = CallDetail(
        new DateTime,
        Some(new Period(0, 15, 12, 5)),
        "4000",
        Some("4001"),
        CallStatus.Answered,
        Some("Mike"),
        Some("Swarm"),
        Some("first" + num2),
        Some("last" + num2)
      )

      RichCallDetail(
        detail,
        repo.userNameFromPhoneNb(_).getOrElse(""),
        repo.richPhones.getOrElse(_, PhoneHintStatus.UNEXISTING),
        repo
          .getUserVideoStatus(_)
          .getOrElse(VideoEvents.Available)
      ) shouldEqual RichCallDetail(
        detail.start,
        detail.duration,
        "j.doe",
        "l.luke",
        detail.status,
        PhoneHintStatus.BUSY,
        VideoEvents.Available,
        PhoneHintStatus.BUSY,
        VideoEvents.Available,
        "4000",
        Some("4001"),
        Some("Mike"),
        Some("Swarm"),
        Some("first" + num2),
        Some("last" + num2)
      )
    }
  }

  "A RichCallHistory" should {
    "be serialized to json" in {
      val date   = RichCallDetail.format.parseDateTime("2014-01-01 08:00:00")
      val period = RichCallDetail.periodFormat.parsePeriod("00:12:14")
      val history = RichCallHistory(
        List(
          RichCallDetail(
            date,
            Some(period),
            "j.doe",
            "l.luke",
            CallStatus.Answered,
            PhoneHintStatus.UNEXISTING,
            VideoEvents.Busy,
            PhoneHintStatus.UNEXISTING,
            VideoEvents.Busy,
            num1,
            Some(num2),
            Some("Mike"),
            Some("Swarm"),
            Some("first" + num2),
            Some("last" + num2)
          )
        )
      )

      Json.toJson(history) shouldEqual JsArray(
        List(
          Json.obj(
            "start"          -> "2014-01-01 08:00:00",
            "duration"       -> "00:12:14",
            "srcUsername"    -> "j.doe",
            "dstUsername"    -> "l.luke",
            "status"         -> "answered",
            "srcPhoneStatus" -> -2,
            "srcVideoStatus" -> "Busy",
            "dstPhoneStatus" -> -2,
            "dstVideoStatus" -> "Busy",
            "srcNum"         -> "1000",
            "dstNum"         -> "2000",
            "srcFirstName"   -> "Mike",
            "srcLastName"    -> "Swarm",
            "dstFirstName"   -> "first2000",
            "dstLastName"    -> "last2000"
          )
        )
      )
    }
  }

}
