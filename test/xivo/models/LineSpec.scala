package xivo.models

import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar.mock
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.xivo.cti.MessageFactory
import play.api.Configuration
import play.api.db.Database
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.ws.{WSRequest, WSResponse}
import services.calltracking.SipDriver
import services.calltracking.SipDriver.SipDriver
import services.config.ConfigDispatcher.TypePhoneDevice
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.network.XiVOWS
import xivo.xuc.{IceServerConfig, SipConfig, XucBaseConfig}
import xivo.xucami.models.CallerId
import xuctest.{BaseTest, IntegrationTest}

import scala.concurrent.Future
import play.api.Application
import play.api.libs.json.OWrites

object LineHelper {
  def makeLine(
      id: Int,
      context: String,
      protocol: String,
      name: String,
      dev: Option[XivoDevice],
      password: Option[String] = None,
      xivoIp: String,
      webRTC: Boolean = false,
      number: Option[String] = None,
      registrar: Option[String] = None,
      ua: Boolean = false,
      callerId: CallerId = CallerId.empty,
      driver: SipDriver = SipDriver.SIP,
      mobileApp: Boolean = false
  ): Line =
    Line(
      id,
      context,
      protocol,
      name,
      dev,
      password,
      xivoIp,
      webRTC,
      number,
      registrar,
      ua,
      callerId,
      driver,
      mobileApp
    )

}

class LineSpec extends BaseTest with GuiceOneAppPerSuite {
  import LineHelper.makeLine

  val db: Database   = mock[Database]
  val ws: XiVOWS   = mock[XiVOWS]
  val conf: XucBaseConfig = mock[XucBaseConfig]

  val factory: MessageFactory                 = mock[MessageFactory]
  val lineFactory: LineFactory             = mock[LineFactory]
  val agentFactory: AgentFactory            = mock[AgentFactory]
  val agentQueueMemberFactory: AgentQueueMemberFactory = mock[AgentQueueMemberFactory]
  val configServer: ConfigServerRequester            = mock[ConfigServerRequester]
  val turnServerConfig: IceServerConfig        = mock[IceServerConfig]
  val sipConfig: SipConfig               = mock[SipConfig]
  val configRepository: ConfigRepository = new ConfigRepository(
    factory,
    lineFactory,
    agentFactory,
    agentQueueMemberFactory,
    configServer,
    turnServerConfig,
    sipConfig
  )

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .build()

  "xuc" should {
    "be able to get a line from xivo" taggedAs IntegrationTest in {
      val lineFactory = app.injector.instanceOf[LineFactory]
      val line        = lineFactory.get(2)
      println(s"-> Line : $line")
      line.get shouldBe a[Line]
    }

    "be able to get a line with enrich action from xivo" taggedAs IntegrationTest in {
      val lineFactory = app.injector.instanceOf[LineFactory]
      val line        = lineFactory.get(2, deviceType = TypePhoneDevice)
      println(s"-> Line : $line")
      line.get shouldBe a[Line]
    }
  }

  "line" should {
    "have a Local interface on custom" in {
      makeLine(
        56,
        "default",
        "custom",
        "local/06789876",
        None,
        xivoIp = ""
      ).interface shouldBe "Local/06789876"
    }
    "have a Local interface without ending /n on custom" in {
      makeLine(
        56,
        "default",
        "custom",
        "local/06789876/n",
        None,
        xivoIp = ""
      ).interface shouldBe "Local/06789876"
    }
    "have a sip interface" in {
      makeLine(
        78,
        "default",
        "sip",
        "isyfd",
        None,
        xivoIp = ""
      ).interface shouldBe "SIP/isyfd"
    }
    "extract webRTC option from sip options" in {
      Line.isWebRTC(
        Some(
          Array(
            List("test", "no"),
            List("webrtc", "yes"),
            List("encryption_taglen", "16")
          )
        )
      ) shouldEqual true
      Line.isWebRTC(
        Some(Array(List("test", "no"), List("webrtc", "no")))
      ) shouldEqual false
    }
    "extract webRTC option from sip options if ua" in {
      Line.isWebRTC(
        Some(Array(List("test", "no"), List("webrtc", "ua")))
      ) shouldEqual true
    }
    "return webRTC option false when option not present or wrongly formatted" in {
      Line.isWebRTC(Some(Array(List("test", "no")))) shouldEqual false
      Line.isWebRTC(
        Some(Array(List("test", "no"), List("webrtc")))
      ) shouldEqual false
    }
    "extract and return ua option from sip options" in {
      Line.isUa(
        Some(Array(List("test", "no"), List("webrtc", "ua")))
      ) shouldEqual true
    }
    "enrich line with no device and _w appended to name if ua is set" in {
      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val line = makeLine(
        78,
        "default",
        "sip",
        "isyfd",
        Some(device),
        xivoIp = "",
        webRTC = true,
        ua = true
      )
      val lineClassImpl = new LineFactoryImpl(null, null, null)

      lineClassImpl.enrichUaAsDefaultDevice(line) shouldBe makeLine(
        78,
        "default",
        "sip",
        "isyfd_w",
        None,
        xivoIp = "",
        webRTC = true,
        ua = true
      )
    }
    "return line unchanged if ua is not set" in {
      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val line =
        makeLine(78, "default", "sip", "isyfd", Some(device), xivoIp = "")
      val lineClassImpl = new LineFactoryImpl(db, ws, conf)

      lineClassImpl.enrichUaAsDefaultDevice(line) shouldBe makeLine(
        78,
        "default",
        "sip",
        "isyfd",
        Some(device),
        xivoIp = ""
      )
    }

    "enrich ua line when switching to webrtc device" in {
      import play.api.http.Status.OK
      import play.api.libs.json.Json
      implicit val deviWrites: OWrites[XivoDevice] = Json.writes[XivoDevice]

      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val line = makeLine(
        78,
        "default",
        "sip",
        "isyfd",
        Some(device),
        xivoIp = "",
        ua = true,
        webRTC = true
      )
      val res = Some((line, Some("1")))

      val wsResp: WSResponse = mock[WSResponse]
      val wsReq: WSRequest   = mock[WSRequest]

      when(wsReq.get()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.toJson(device))
      when(ws.withWS(s"devices/1")).thenReturn(wsReq)
      when(
        db.withConnection(
          any[java.sql.Connection => Option[(Line, Option[String])]]()
        )
      ).thenReturn(res)

      val lineClassImpl = new LineFactoryImpl(db, ws, conf)

      lineClassImpl.get(78).get shouldBe makeLine(
        78,
        "default",
        "sip",
        "isyfd_w",
        None,
        xivoIp = "",
        ua = true,
        webRTC = true
      )
    }

    "change to phone device if ua is set and line has device" in {
      import play.api.http.Status.OK
      import play.api.libs.json.Json
      implicit val deviWrites: OWrites[XivoDevice] = Json.writes[XivoDevice]

      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val line = makeLine(
        78,
        "default",
        "sip",
        "isyfd",
        Some(device),
        xivoIp = "",
        ua = true,
        webRTC = true
      )
      val res = Some((line, Some("1")))

      val wsResp: WSResponse = mock[WSResponse]
      val wsReq: WSRequest   = mock[WSRequest]

      when(wsReq.get()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.toJson(device))
      when(ws.withWS(s"devices/1")).thenReturn(wsReq)
      when(
        db.withConnection(
          any[java.sql.Connection => Option[(Line, Option[String])]]()
        )
      ).thenReturn(res)

      val lineClassImpl = new LineFactoryImpl(db, ws, conf)
      val expected = makeLine(
        78,
        "default",
        "sip",
        "isyfd",
        Some(device),
        xivoIp = "",
        ua = true,
        webRTC = false
      )

      lineClassImpl.get(78, deviceType = TypePhoneDevice).get shouldBe expected
    }

    "change to phone device if ua is set and line has no device" in {
      import play.api.http.Status.OK
      import play.api.libs.json.Json
      implicit val deviWrites: OWrites[XivoDevice] = Json.writes[XivoDevice]

      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val line = makeLine(
        78,
        "default",
        "sip",
        "isyfd",
        None,
        xivoIp = "",
        ua = true,
        webRTC = true
      )
      val res = Some((line, Some("1")))

      val wsResp: WSResponse = mock[WSResponse]
      val wsReq: WSRequest   = mock[WSRequest]

      when(wsReq.get()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.toJson(device))
      when(ws.withWS(s"devices/1")).thenReturn(wsReq)
      when(
        db.withConnection(
          any[java.sql.Connection => Option[(Line, Option[String])]]()
        )
      ).thenReturn(res)

      val lineClassImpl = new LineFactoryImpl(db, ws, conf)
      val expected = makeLine(
        78,
        "default",
        "sip",
        "isyfd",
        Some(device),
        xivoIp = "",
        ua = true,
        webRTC = false
      )

      lineClassImpl.get(78, deviceType = TypePhoneDevice).get shouldBe expected
    }

    "change to webrtc device without ua" in {
      import play.api.http.Status.OK
      import play.api.libs.json.Json
      implicit val deviWrites: OWrites[XivoDevice] = Json.writes[XivoDevice]

      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val line =
        makeLine(78, "default", "sip", "isyfd", Some(device), xivoIp = "")
      val res = Some((line, Some("1")))

      val wsResp: WSResponse = mock[WSResponse]
      val wsReq: WSRequest   = mock[WSRequest]

      when(wsReq.get()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.toJson(device))
      when(ws.withWS(s"devices/1")).thenReturn(wsReq)
      when(
        db.withConnection(
          any[java.sql.Connection => Option[(Line, Option[String])]]()
        )
      ).thenReturn(res)

      val lineClassImpl = new LineFactoryImpl(db, ws, conf)
      val expected =
        makeLine(78, "default", "sip", "isyfd", Some(device), xivoIp = "")

      lineClassImpl.get(78).get shouldBe expected
    }

    "change to phone device without ua" in {
      import play.api.http.Status.OK
      import play.api.libs.json.Json
      implicit val deviWrites: OWrites[XivoDevice] = Json.writes[XivoDevice]

      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val line =
        makeLine(78, "default", "sip", "isyfd", Some(device), xivoIp = "")
      val res = Some((line, Some("1")))

      val wsResp: WSResponse = mock[WSResponse]
      val wsReq: WSRequest   = mock[WSRequest]

      when(wsReq.get()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.toJson(device))
      when(ws.withWS(s"devices/1")).thenReturn(wsReq)
      when(
        db.withConnection(
          any[java.sql.Connection => Option[(Line, Option[String])]]()
        )
      ).thenReturn(res)

      val lineClassImpl = new LineFactoryImpl(db, ws, conf)
      val expected =
        makeLine(78, "default", "sip", "isyfd", Some(device), xivoIp = "")

      lineClassImpl.get(78, deviceType = TypePhoneDevice).get shouldBe expected
    }

    "get line by endpoint id" in {
      import play.api.http.Status.OK
      import play.api.libs.json.Json
      implicit val deviWrites: OWrites[XivoDevice] = Json.writes[XivoDevice]

      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val line =
        makeLine(78, "default", "sip", "isyfd", Some(device), xivoIp = "")
      val res = Some((line, Some("1")))

      val wsResp: WSResponse = mock[WSResponse]
      val wsReq: WSRequest   = mock[WSRequest]

      when(wsReq.get()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.toJson(device))
      when(ws.withWS(s"devices/1")).thenReturn(wsReq)
      when(
        db.withConnection(
          any[java.sql.Connection => Option[(Line, Option[String])]]()
        )
      ).thenReturn(res)

      val lineClassImpl = new LineFactoryImpl(db, ws, conf)
      val expected =
        makeLine(78, "default", "sip", "isyfd", Some(device), xivoIp = "")

      lineClassImpl.get(SipEndpoint(25)).get shouldBe expected
    }

    "add mobileApp to lineconfig" in {
      import play.api.http.Status.OK
      import play.api.libs.json.Json
      implicit val deviWrites: OWrites[XivoDevice] = Json.writes[XivoDevice]

      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val line = makeLine(
        79,
        "default",
        "sip",
        "isyfd",
        Some(device),
        xivoIp = "",
        webRTC = true,
        mobileApp = true
      )
      val res = Some((line, Some("1")))

      val wsResp: WSResponse = mock[WSResponse]
      val wsReq: WSRequest   = mock[WSRequest]

      when(wsReq.get()).thenReturn(Future.successful(wsResp))
      when(wsResp.status).thenReturn(OK)
      when(wsResp.json).thenReturn(Json.toJson(device))
      when(ws.withWS(s"devices/1")).thenReturn(wsReq)
      when(
        db.withConnection(
          any[java.sql.Connection => Option[(Line, Option[String])]]()
        )
      ).thenReturn(res)

      val lineClassImpl = new LineFactoryImpl(db, ws, conf)

      lineClassImpl.get(79).get shouldBe makeLine(
        79,
        "default",
        "sip",
        "isyfd",
        Some(device),
        xivoIp = "",
        webRTC = true,
        mobileApp = true
      )
    }
  }
}
