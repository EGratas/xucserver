package xivo.models

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class ExtensionPatternSpec extends AnyWordSpec with Matchers {
  val phoneprogfunckey: ExtensionPattern =
    ExtensionPattern(ExtensionName.ProgrammableKey, "_*735.")
  val enablednd: ExtensionPattern = ExtensionPattern(ExtensionName.DND, "*25")
  val fwdunc: ExtensionPattern    = ExtensionPattern(ExtensionName.UnconditionalForward, "_*21.")
  val fwdrna: ExtensionPattern    = ExtensionPattern(ExtensionName.NoAnswerForward, "_*22.")
  val fwdbusy: ExtensionPattern   = ExtensionPattern(ExtensionName.BusyForward, "_*23.")

  "ExtensionPattern" should {
    "Convert pattern to extension" in {
      phoneprogfunckey.exten() shouldBe "*735"
      enablednd.exten() shouldBe "*25"
    }

    "create user DND function key" in {
      val fkey = ExtensionPattern.userService(
        "123",
        phoneprogfunckey,
        enablednd,
        None,
        KeyInUse
      )
      fkey shouldBe FunctionKey("*735123***225", KeyInUse)
    }

    "create user forward function key" in {
      val fkey = ExtensionPattern.userService(
        "123",
        phoneprogfunckey,
        fwdrna,
        Some("8888"),
        KeyInUse
      )
      fkey shouldBe FunctionKey("*735123***222*8888", KeyInUse)
    }

    "create user forward function key with no destination" in {
      val fkey = ExtensionPattern.userService(
        "123",
        phoneprogfunckey,
        fwdrna,
        None,
        KeyInUse
      )
      fkey shouldBe FunctionKey("*735123***222", KeyInUse)
    }

    "create function key with star pattern" in {
      val fkey = ExtensionPattern.userService(
        "123",
        phoneprogfunckey,
        fwdrna,
        Some("*8888"),
        KeyInUse
      )
      fkey shouldBe FunctionKey("*735123***222***38888", KeyInUse)
    }

    "create all function keys for user services" in {
      val services = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )
      val fkeys = ExtensionPattern.userServices(
        "123",
        None,
        services,
        phoneprogfunckey,
        enablednd,
        fwdbusy,
        fwdrna,
        fwdunc
      )

      fkeys should contain.allOf(
        FunctionKey("*735123***225", KeyInUse),
        FunctionKey("*735123***223*1234", KeyNotInUse),
        FunctionKey("*735123***223", KeyNotInUse),
        FunctionKey("*735123***222*4567", KeyNotInUse),
        FunctionKey("*735123***222", KeyNotInUse),
        FunctionKey("*735123***221*7890", KeyNotInUse),
        FunctionKey("*735123***221", KeyNotInUse)
      )
    }

    "update all function keys for new user services and previous one" in {

      val previousServices = Some(
        UserServices(
          true,
          UserForward(false, "1234"),
          UserForward(false, "4567"),
          UserForward(true, "7890")
        )
      )

      val newServices = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "7777"),
        UserForward(true, "9999")
      )
      val fkeys = ExtensionPattern.userServices(
        "123",
        previousServices,
        newServices,
        phoneprogfunckey,
        enablednd,
        fwdbusy,
        fwdrna,
        fwdunc
      )

      fkeys should contain.allOf(
        FunctionKey("*735123***225", KeyInUse),
        FunctionKey("*735123***223*1234", KeyNotInUse),
        FunctionKey("*735123***223", KeyNotInUse),
        FunctionKey("*735123***222*4567", KeyNotInUse),
        FunctionKey("*735123***222*7777", KeyNotInUse),
        FunctionKey("*735123***222", KeyNotInUse),
        FunctionKey("*735123***221*7890", KeyNotInUse),
        FunctionKey("*735123***221*9999", KeyInUse),
        FunctionKey("*735123***221", KeyInUse)
      )
    }
  }
}
