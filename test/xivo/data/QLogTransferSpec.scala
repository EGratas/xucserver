package xivo.data

import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.QueueLog
import models.QueueLog.QueueLogData
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Helpers.running
import play.api.{Application, Configuration}
import stats.Statistic.ResetStat
import xuctest.IntegrationConfig

class QLogTransferSpec
    extends TestKitSpec("QLogTransferSpec")
    with IntegrationConfig
    with MockitoSugar
    with GuiceOneAppPerSuite {

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    ).build()

  class Helper {
    val getQueueLog: String => List[QueueLogData] = mock[String => List[QueueLogData]]
    val queueLog: QueueLog = new QueueLog {
      def getAll(clause: String): List[QueueLogData] = getQueueLog(clause)
      def defaultClause: String                      = "cast(current_date as varchar)"
    }

    val qLogDispatcher: TestProbe = TestProbe()

    def actor(): (TestActorRef[QLogTransfer], QLogTransfer) = {
      val a = TestActorRef(new QLogTransfer(queueLog, qLogDispatcher.ref))
      (a, a.underlyingActor)
    }
  }

  "Qlog transfer" should {
    import xivo.data.QLogTransfer.LoadQueueLogs
    "get all queue log with default query for the first time" in new Helper {
      running(app) {

        val (ref, qLogTransfer) = actor()
        when(getQueueLog(anyString)).thenReturn(List())

        ref ! LoadQueueLogs(queueLog.defaultClause)

        verify(getQueueLog, atLeastOnce())(queueLog.defaultClause)
      }
    }

    "setup next clause with the last time stamp received" in new Helper {
      val (ref, qLogTransfer) = actor()

      val lastQueueLogReceived: QueueLogData =
        QueueLogData("2014-04-18 12:22:20.225112", "Agent/1702", "WRAPUPSTART")
      when(getQueueLog(anyString)).thenReturn(List(lastQueueLogReceived))

      ref ! LoadQueueLogs(queueLog.defaultClause)

      qLogTransfer.nextClause should be(s"'${lastQueueLogReceived.queuetime}'")
    }
    "send reset stat on startup" in new Helper {
      val (ref, _) = actor()

      qLogDispatcher.expectMsg(ResetStat)
    }
    "send each queue log event received to queuelog dispatcher" in new Helper {
      val (ref, _) = actor()

      val queueLogReceived: QueueLogData =
        QueueLogData("2014-04-18 12:22:20.225112", "Agent/1702", "WRAPUPSTART")
      when(getQueueLog(anyString)).thenReturn(List(queueLogReceived))

      ref ! LoadQueueLogs(queueLog.defaultClause)

      qLogDispatcher.expectMsgAllOf(ResetStat, queueLogReceived)
    }
  }
}
