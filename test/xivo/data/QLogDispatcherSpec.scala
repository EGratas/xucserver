package xivo.data

import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import org.scalatestplus.mockito.MockitoSugar
import models.QLogTransformer.QLogDataTransformer
import models.QueueLog._
import stats.Statistic.ResetStat
import pekkotest.TestKitSpec

class QLogDispatcherSpec
    extends TestKitSpec("QLogDispatcherSpec")
    with MockitoSugar {

  class Helper {
    val qLogTransformer: QLogDataTransformer         = mock[QLogDataTransformer]
    val eventObjQueueDispatcher: TestProbe = TestProbe()

    def actor(): (TestActorRef[QLogDispatcher], QLogDispatcher) = {
      val a = TestActorRef(new QLogDispatcher(eventObjQueueDispatcher.ref))
      (a, a.underlyingActor)
    }
  }

  "Qlog dispatcher" should {
    "forward reset event " in new Helper {
      val (ref, _) = actor()

      ref ! ResetStat

      eventObjQueueDispatcher.expectMsg(ResetStat)

    }
    "transform qlog data to qlog event" in new Helper {
      val (ref, _) = actor()
      val qLogData: QueueLogData = QueueLogData(
        "2014-04-18 12:22:20.225112",
        "NONE",
        "ENTERQUEUE",
        None,
        None,
        None,
        Some(7)
      )

      ref ! qLogData

      eventObjQueueDispatcher.expectMsg(EnterQueue(7))
    }

    "send event to queue event dispatcher" in new Helper {
      val (ref, _) = actor()
      val qLogData: QueueLogData = QueueLogData(
        "2014-04-18 12:22:20.225112",
        "NONE",
        "ENTERQUEUE",
        None,
        None,
        None,
        Some(5)
      )

      ref ! qLogData

      eventObjQueueDispatcher.expectMsg(EnterQueue(5))
    }

  }
}
