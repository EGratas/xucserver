package xivo.data.dbunit

import anorm._
import org.dbunit.database.{DatabaseConfig, DatabaseConnection}
import org.dbunit.dataset.datatype.DataType
import org.dbunit.dataset.xml.{FlatXmlDataSet, FlatXmlProducer}
import org.dbunit.ext.postgresql.{GenericEnumType, PostgresqlDataTypeFactory}
import org.dbunit.operation.DatabaseOperation
import org.xml.sax.InputSource
import play.api.db.Database

class CustomDataTypeFactory extends PostgresqlDataTypeFactory {
  val enumTypes: List[String] = List(
    "userfeatures_voicemailtype",
    "generic_bsfilter",
    "queuemember_usertype",
    "queue_category",
    "extenumbers_type"
  )
  override def createDataType(sqlType: Int, sqlTypeName: String): DataType = {
    if (enumTypes.contains(sqlTypeName)) new GenericEnumType(sqlTypeName)
    else super.createDataType(sqlType, sqlTypeName)
  }
  override def isEnumType(sqlTypeName: String): Boolean = true
}

object DBUtil {
  private var dbunitConnection: DatabaseConnection = null

  def setupData(fileName: String)(implicit db: Database): Unit = {
    db.withConnection { implicit conn =>
      var dataset = new FlatXmlDataSet(
        new FlatXmlProducer(new InputSource(inputFromFile(fileName)))
      )

      try {
        this.dbunitConnection = new DatabaseConnection(conn)
        this.dbunitConnection.getConfig.setProperty(
          DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
          new CustomDataTypeFactory()
        )
        this.dbunitConnection.getConfig
          .setProperty(DatabaseConfig.FEATURE_ALLOW_EMPTY_FIELDS, true)

        for (table <- dataset.getTableNames) {
          val createTable = scala.io.Source
            .fromInputStream(inputFromFile(s"$table.sql"))
            .mkString
          SQL(createTable).execute()
        }
      } catch {
        case e: Exception =>
          println(s"****SetupDB Create table Error : $e")
      }

      try {

        DatabaseOperation.CLEAN_INSERT.execute(this.dbunitConnection, dataset)
      } catch {
        case e: Exception =>
          println(s"****SetupDB insert data Error : $e")
          e.printStackTrace();
      }

    }
  }
  private def inputFromFile(fileName: String) =
    getClass.getClassLoader.getResourceAsStream(fileName)
}
