package xivo.websocket

import org.json.JSONObject
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.xivo.cti.MessageFactory
import play.libs.Json

import scala.jdk.CollectionConverters._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class MessageParserSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterAll {
  "The Websocket MessageParser" should {

    val messageFactory = new MessageFactory

    "prepare AgentLogin object upon receipt of agentLogin command message" in {
      val messageParser = new MessageParser(messageFactory)
      val agentNumber   = "2004"
      val message       = new JSONObject
      message.accumulate("command", "agentLogin")
      message.accumulate("agentno", agentNumber)
      val expectedResponse = messageFactory.createAgentLogin(agentNumber)

      val response = messageParser.decodeMessage(Json.parse(message.toString()))

      val keys: List[String] = response
        .keys()
        .asScala
        .collect { case s: String =>
          s
        }
        .toList

      keys.foreach { key =>
        response.get(key).equals(expectedResponse.get(key))
      }

    }

    "prepare AgentLogout object upon receipt of agentLogout command message" in {
      val messageParser = new MessageParser(messageFactory)
      val agentId       = "2"
      val message       = new JSONObject
      message.accumulate("command", "agentLogout")
      message.accumulate("agentid", agentId)
      val expectedResponse = messageFactory.createAgentLogout(agentId)

      val response = messageParser.decodeMessage(Json.parse(message.toString()))

      val keys: List[String] = response
        .keys()
        .asScala
        .collect { case s: String =>
          s
        }
        .toList

      keys.foreach { key =>
        response.get(key).equals(expectedResponse.get(key))
      }

    }

  }

}
