package xivo.websocket

import org.apache.pekko.Done
import org.apache.pekko.actor.{Actor, ActorRef, Props}
import org.apache.pekko.stream.OverflowStrategy
import org.apache.pekko.stream.scaladsl.{Source, _}
import org.apache.pekko.stream.testkit.scaladsl.TestSink
import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.{JsValue, Json}
import xivo.websocket.ActorFlowOut.ActorFlowOutAck
import xuctest.XucUserHelper

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._
import org.apache.pekko.stream.testkit.TestSubscriber

class ActorFlowOutSpec
    extends TestKitSpec("ActorFlowOutSpec")
    with MockitoSugar
    with XucUserHelper {

  class Helper() {
    val upstreamRef: TestProbe = TestProbe()

    def fakeWsActorProps(downstreamRef: ActorRef): Props =
      Props(new Actor {
        override def receive: Receive = {
          case ActorFlowOutAck =>
            upstreamRef.ref ! ActorFlowOutAck
          case msg =>
            downstreamRef ! msg
        }
      })

    val testSink: Sink[JsValue,TestSubscriber.Probe[JsValue]] = TestSink.probe[JsValue]
  }

  "ActorFlowOutSpec" should {
    "emit ack message after received a message" in new Helper {
      val message: JsValue = Json.toJson("message")
      val sinkHelper: Sink[JsValue, Future[Done]] =
        Flow[JsValue].toMat(Sink.ignore)(Keep.right)
      val actorFlow: Flow[JsValue, JsValue, _] =
        ActorFlowOut.actorRef(fakeWsActorProps)

      val (ref, future) = Source
        .actorRef(3, OverflowStrategy.fail)
        .via(actorFlow)
        .toMat(sinkHelper)(Keep.both)
        .run()

      ref ! message
      ref ! message
      ref ! message

      upstreamRef.expectMsgAllOf(
        5.seconds,
        ActorFlowOutAck,
        ActorFlowOutAck,
        ActorFlowOutAck
      )
    }
  }

  "allow some burstiness on input" in new Helper {
    val message: JsValue = Json.toJson("message")

    val listOfMessages: List[JsValue] = List.fill(25)(message)

    val actorFlow: Flow[JsValue, JsValue, _] =
      ActorFlowOut.actorRef(fakeWsActorProps, 25)
    Source(listOfMessages)
      .via(actorFlow)
      .runWith(testSink)
      .request(25)
      .expectNextN(listOfMessages)
      .request(1)
      .expectNoMessage()
  }

  "throttle too many messages on input" in new Helper {
    val message: JsValue = Json.toJson("message")
    val expected: JsValue = Json.parse(
      """{"msgType":"Error","ctiMessage":{"Error":"Maximum throttle throughput exceeded."}}"""
    )

    val listOfMessages: List[JsValue] = List.fill(26)(message)

    val actorFlow: Flow[JsValue, JsValue, _] =
      ActorFlowOut.actorRef(fakeWsActorProps, 26)

    Source(listOfMessages)
      .via(actorFlow)
      .runWith(testSink)
      .request(25)
      .expectNextN(listOfMessages.tail)
      .request(1)
      .expectNext(expected)
  }
}
