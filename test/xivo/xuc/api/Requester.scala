package xivo.xuc.api

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.util.Timeout
import controllers.helpers.RequestResult
import org.xivo.cti.model.PhoneHintStatus
import services.config.ConfigRepository
import services.request.{HistorySize, HistoryDays}
import services.video.model.VideoEvents
import xivo.models.{CallStatus, RichCallDetail, RichCallHistory}
import controllers.helpers.RequestSuccess
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import org.joda.time.{ DateTime, Period }

object Requester {
  val date: DateTime = RichCallDetail.format.parseDateTime("2014-01-01 08:00:00")
  val period: Period = RichCallDetail.periodFormat.parsePeriod("00:12:14")

  def richCallHistory(
   username: String,
   param: HistorySize,
   repo: ConfigRepository,
   callHistoryManager: ActorRef)(implicit
                                 system: ActorSystem,
                                 ec: ExecutionContext
                     ): Future[RichCallHistory] = {
    Future.successful(
      RichCallHistory(
        List(
          RichCallDetail(
            date,
            Some(period),
            "j.doe",
            "l.luke",
            CallStatus.Answered,
            PhoneHintStatus.UNEXISTING,
            VideoEvents.Busy,
            PhoneHintStatus.UNEXISTING,
            VideoEvents.Busy,
            "1000",
            Some("2000"),
            Some("Mike"),
            Some("Swarm"),
            Some("first" + "2000"),
            Some("last" + "2000")
          )
        )
      )
    )
  }

  def richCallHistoryByDays(
                             username: String,
                             param: HistoryDays,
                             repo: ConfigRepository,
                             callHistoryManager: ActorRef
                           )(implicit
                             system: ActorSystem,
                             ec: ExecutionContext
                           ): Future[RichCallHistory] = {
    Future.successful(
      RichCallHistory(
        List(
          RichCallDetail(
            date,
            Some(period),
            "j.doe",
            "l.luke",
            CallStatus.Answered,
            PhoneHintStatus.UNEXISTING,
            VideoEvents.Busy,
            PhoneHintStatus.UNEXISTING,
            VideoEvents.Busy,
            "1000",
            Some("2000"),
            Some("Mike"),
            Some("Swarm"),
            Some("first" + "2000"),
            Some("last" + "2000")
          )
        )
      )
    )
  }

  def exportTicketsCsv(callbackManager: ActorRef, listUuid: String)(implicit
                                                                    system: ActorSystem
  ): Future[RequestResult] = {
    implicit val timeout: Timeout = Timeout(10.seconds)
    val result = Future.successful(RequestSuccess("foobarbat"))
    result.mapTo[RequestResult]
  }

  def importCsvCallback(
                         callbackManager: ActorRef,
                         listUuid: String,
                         text: String
                       )(implicit system: ActorSystem): Future[RequestResult] = {
    implicit val timeout: Timeout = Timeout(10.seconds)
    val result = Future.successful(RequestSuccess("CSV successfully imported"))
    result.mapTo[RequestResult]
  }
}