package xivo.rabbitmq

import org.apache.pekko.testkit.TestProbe
import org.apache.pekko.util.Timeout
import pekkotest.TestKitSpec
import com.rabbitmq.client._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import play.api.mvc.Results
import xivo.models
import xivo.models._

import scala.concurrent.duration.DurationInt

class XivoRabbitEventsConsumerSpec
    extends TestKitSpec("XivoRabbitEventsConsumerSpec")
    with Results
    with MockitoSugar {

  class Helper {
    implicit val timeout: Timeout = Timeout(100.milliseconds)

    val factory: XivoRabbitEventsFactory = mock[XivoRabbitEventsFactory]
    val channel: Channel                 = mock[Channel]
    val envelope: Envelope               = mock[Envelope]

    val eventManager: TestProbe = TestProbe()

    val consumerTag = "the-consumer-tag"

    when(factory.channel).thenReturn(channel)
    when(factory.queueName).thenReturn("test-queue")
    when(factory.consumerTag).thenReturn(consumerTag)
    when(factory.exchangeName).thenReturn("xivo")
    when(factory.routingKeys).thenReturn(List("key1"))

    val eventConsumer = new XivoRabbitEventsConsumer(eventManager.ref, factory)
  }

  "A XivoRabbitEventsConsumer" should {
    "parse the message received: queue_created" in new Helper {
      val message =
        "{\"data\": {\"id\": 1}, \"name\": \"queue_created\", \"origin_uuid\": null}"
      val expected: Some[RabbitEventQueueCreated] = Some(
        RabbitEventQueueCreated(RabbitEventData(1L, None), "queue_created")
      )

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

      result shouldBe expected
    }

    "parse the message received: queue_edited" in new Helper {
      val message =
        "{\"data\": {\"id\": 1}, \"name\": \"queue_edited\", \"origin_uuid\": null}"
      val expected: Some[RabbitEventQueueEdited] =
        Some(RabbitEventQueueEdited(RabbitEventData(1L, None), "queue_edited"))

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

      result shouldBe expected
    }

    "parse the message received: agent_edited" in new Helper {
      val message =
        "{\"data\": {\"id\": 1}, \"name\": \"agent_edited\", \"origin_uuid\": null}"
      val expected: Some[RabbitEventAgentEdited] = Some(
        RabbitEventAgentEdited(RabbitEventAgentEditedData(1), "agent_edited")
      )

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

      result shouldBe expected
    }

    "parse the message received: agent_created" in new Helper {
      val message =
        "{\"data\": {\"id\": 1}, \"name\": \"agent_created\", \"origin_uuid\": null}"
      val expected: Some[RabbitEventAgentCreated] = Some(
        models.RabbitEventAgentCreated(
          models.RabbitEventAgentCreatedData(1),
          "agent_created"
        )
      )

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

      result shouldBe expected
    }

    "parse the message received: agent_deleted" in new Helper {
      val message =
        "{\"data\": {\"id\": 1}, \"name\": \"agent_deleted\", \"origin_uuid\": null}"
      val expected: Some[RabbitEventAgentDeleted] = Some(
        RabbitEventAgentDeleted(RabbitEventAgentDeletedData(1), "agent_deleted")
      )

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

      result shouldBe expected
    }

    "parse the message for media servers" in new Helper {
      val messages: Map[String, RabbitEvent with Serializable] = Map(
        "mediaserver_created" -> RabbitEventMediaServerCreated(
          RabbitEventData(1, None),
          "mediaserver_created"
        ),
        "mediaserver_edited" -> RabbitEventMediaServerEdited(
          RabbitEventData(1, None),
          "mediaserver_edited"
        ),
        "mediaserver_deleted" -> RabbitEventMediaServerDeleted(
          RabbitEventData(1, None),
          "mediaserver_deleted"
        )
      )

      messages.foreach(e => {
        val message =
          s"""{"data": {"id": 1}, "name": "${e._1}", "origin_uuid": null}"""
        val expected                    = Some(e._2)
        val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

        result shouldBe expected
      })
    }

    "parse the message for sip endpoints" in new Helper {
      val messages: Map[String, RabbitEventSipEndpointEdited] = Map(
        "sip_endpoint_edited" -> RabbitEventSipEndpointEdited(
          RabbitEventData(1, None),
          "sip_endpoint_edited"
        )
      )

      messages.foreach(e => {
        val message =
          s"""{"data": {"id": 1}, "name": "${e._1}", "origin_uuid": null}"""
        val expected                    = Some(e._2)
        val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

        result shouldBe expected
      })
    }

    "parse the message received: unhandled event type" in new Helper {
      val message =
        "{\"data\": {\"id\": 1}, \"name\": \"unhandled_message\", \"origin_uuid\": null}"

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

      result shouldBe None
    }

    "parse the message received: expected key not found" in new Helper {
      val message = "{\"data\": {\"id\": 1}, \"origin_uuid\": null}"

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

      result shouldBe None
    }

    "parse the message received: unexpected value in event" in new Helper {
      val message =
        "{\"data\": {\"id\": 1}, \"name\": 123, \"origin_uuid\": null}"

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

      result shouldBe None
    }

    "handle delivery with message" in new Helper {
      val message =
        "{\"data\": {\"id\": 1}, \"name\": \"queue_edited\", \"origin_uuid\": null}"
      eventConsumer.consumer.handleDelivery(
        consumerTag,
        envelope,
        new AMQP.BasicProperties,
        message.getBytes
      )
      val expected: RabbitEventQueueEdited =
        RabbitEventQueueEdited(RabbitEventData(1L, None), "queue_edited")

      eventManager.expectMsg(expected)
    }

    "handle delivery with wrong json" in new Helper {
      val message = "{\"name\": \"queue_edited\"}"

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)
      result shouldBe None
    }

    "handle delivery with non-json message" in new Helper {
      val message = "Test"

      val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

      result shouldBe None
    }

    "not receive any message on unhandled message" in new Helper {
      val message =
        "{\"data\": {\"id\": 1}, \"name\": \"unhandled_message\", \"origin_uuid\": null}"

      eventConsumer.consumer.handleDelivery(
        consumerTag,
        envelope,
        new AMQP.BasicProperties,
        message.getBytes
      )

      eventManager.expectNoMessage(expectMsgTimeout)
    }

    "parse the message for user services" in new Helper {
      val message: RabbitEventUserServicesEdited = RabbitEventUserServicesEdited(
        RabbitEventData(1, None),
        "user_services_edited"
      )

      val json =
        s"""{"data":{"id":1},"name":"user_services_edited","origin_uuid":"62785906-bdb2-4ca7-b254-8a6a5d94fa59"}"""
      val result: Option[RabbitEvent] = eventConsumer.parseMessage(json)

      result shouldBe Some(message)
    }

    "parse the message for sip config" in new Helper {
      val messages: Map[String, RabbitEventSipConfigEdited] = Map(
        "sip_config_edited" -> RabbitEventSipConfigEdited("sip_config_edited")
      )

      messages.foreach(e => {
        val message =
          s"""{"data": {"id": 1}, "name": "${e._1}", "origin_uuid": null}"""
        val expected                    = Some(e._2)
        val result: Option[RabbitEvent] = eventConsumer.parseMessage(message)

        result shouldBe expected
      })
    }

    "parse the message for user edited" in new Helper {
      val message: RabbitEventUserConfigEdited = RabbitEventUserConfigEdited(
        RabbitEventData(1, None)
      )

      val json =
        s"""{"data": {"id": 1, "uuid": "d66f2f15-a620-4e9d-922a-929f1627b5af"}, "name": "user_edited", "origin_uuid": "d66f2f15-a620-4e9d-922a-929f1627b5af"}"""
      val result: Option[RabbitEvent] = eventConsumer.parseMessage(json)

      result shouldBe Some(message)
    }

    "parse the message for user preference edited" in new Helper {
      val message: RabbitEventUserPreferenceEdited = RabbitEventUserPreferenceEdited(
        RabbitEventData(42, None),
        "user_preference_edited"
      )

      val json =
        s"""{"data": {"id": 42}, "name": "user_preference_edited", "origin_uuid": "d66f2f15-a620-4e9d-922a-929f1627b5af"}"""
      val result: Option[RabbitEvent] = eventConsumer.parseMessage(json)

      result shouldBe Some(message)
    }

    "parse the message for user preference created" in new Helper {
      val message: RabbitEventUserPreferenceCreated = RabbitEventUserPreferenceCreated(
        RabbitEventData(42, None),
        "user_preference_created"
      )

      val json =
        s"""{"data": {"id": 42}, "name": "user_preference_created", "origin_uuid": "d66f2f15-a620-4e9d-922a-929f1627b5af"}"""
      val result: Option[RabbitEvent] = eventConsumer.parseMessage(json)

      result shouldBe Some(message)
    }

    "parse the message for user preference deleted" in new Helper {
      val message: RabbitEventUserPreferenceDeleted = RabbitEventUserPreferenceDeleted(
        RabbitEventData(42, None),
        "user_preference_deleted"
      )

      val json =
        s"""{"data": {"id": 42}, "name": "user_preference_deleted", "origin_uuid": "d66f2f15-a620-4e9d-922a-929f1627b5af"}"""
      val result: Option[RabbitEvent] = eventConsumer.parseMessage(json)

      result shouldBe Some(message)
    }

    "parse the message for webservice user creation" in new Helper {
      val message: RabbitEventWebserviceUserCreated = RabbitEventWebserviceUserCreated(
        RabbitEventWebserviceUserData("jdoe"),
        "webservice_user_created"
      )

      val json =
        s"""{"data": {"login": "jdoe"}, "name": "webservice_user_created", "origin_uuid": "d66f2f15-a620-4e9d-922a-929f1627b5af"}"""
      val result: Option[RabbitEvent] = eventConsumer.parseMessage(json)

      result shouldBe Some(message)
    }

    "parse the message for webservice user edition" in new Helper {
      val message: RabbitEventWebserviceUserEdited = RabbitEventWebserviceUserEdited(
        RabbitEventWebserviceUserData("jdoe"),
        "webservice_user_edited"
      )

      val json =
        s"""{"data": {"login": "jdoe"}, "name": "webservice_user_edited", "origin_uuid": "d66f2f15-a620-4e9d-922a-929f1627b5af"}"""
      val result: Option[RabbitEvent] = eventConsumer.parseMessage(json)

      result shouldBe Some(message)
    }

    "parse the message for webservice user global reload" in new Helper {
      val message: RabbitEventWebserviceUsersReload = RabbitEventWebserviceUsersReload(
        "webservice_users_reload"
      )

      val json =
        s"""{"name": "webservice_users_reload", "origin_uuid": "d66f2f15-a620-4e9d-922a-929f1627b5af"}"""
      val result: Option[RabbitEvent] = eventConsumer.parseMessage(json)

      result shouldBe Some(message)
    }
  }
}
