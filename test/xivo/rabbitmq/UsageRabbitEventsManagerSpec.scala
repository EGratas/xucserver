package xivo.rabbitmq

import org.apache.pekko.actor.{ActorSystem, PoisonPill, Props}
import org.apache.pekko.testkit.TestActorRef
import com.rabbitmq.client.Channel
import models.usm.LoginEvent
import org.mockito.Mockito.{times, verify, when}
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar

class UsageRabbitEventsManagerSpec
  extends MockitoSugar
    with AnyWordSpecLike
    with Matchers
    with Eventually {

  class Helper {
    implicit val system: ActorSystem = ActorSystem()

    val rabbitProducer: UsageRabbitEventsProducer  = mock[UsageRabbitEventsProducer]
    val usageEvents: UsageRabbitEventsManager                  = mock[UsageRabbitEventsManager]
    val channel: Channel                          = mock[Channel]

    val loginEvent: LoginEvent = LoginEvent("electron", "uc", "webRTC", "xxx-xxx-xxx", "2018-05-09 10:04:25.375")

    def actor(): (TestActorRef[UsageRabbitEventsManager], UsageRabbitEventsManager) = {
      def factory: UsageRabbitEventsManager = new UsageRabbitEventsManager(rabbitProducer)

      val a = TestActorRef.apply[UsageRabbitEventsManager](
        Props(factory),
      )
      (a, a.underlyingActor)
    }

    when(rabbitProducer.channel).thenReturn(channel)
    when(rabbitProducer.queueName).thenReturn("usage.event_login")
  }

  "A UsageLoginEvents" should {
    "connect to rabbitmq on init" in new Helper {
      actor()
      verify(rabbitProducer, times(1)).createConnection()
    }

    "publish a login event to rabbitmq" in new Helper {
      val (ref, _) = actor()
      ref ! loginEvent
      verify(rabbitProducer, times(1)).publishMessage(loginEvent)
    }

    "close connection to rabbitmq" in new Helper {
      val (ref, _) = actor()
      ref ! PoisonPill
      verify(rabbitProducer, times(1)).shutdownConnection()
    }
  }
}
