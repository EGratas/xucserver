package xivo.rabbitmq

import com.rabbitmq.client.AMQP.Queue.DeclareOk
import com.rabbitmq.client.*
import models.usm.LoginEvent
import org.mockito.Mockito.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.Json
import play.api.libs.json.Json.toBytes
import xivo.xuc.{RabbitConfig, RabbitConfigGenerator}

import scala.util.Try


class UsageRabbitEventsProducerSpec
  extends MockitoSugar
    with AnyWordSpecLike
    with Matchers
    with GuiceOneAppPerSuite {

  class Helper {
    val factory: UsageRabbitEventsProducer             = mock[UsageRabbitEventsProducer]
    val channel: Channel                              = mock[Channel]
    val configuration: ConnectionFactory              = mock[ConnectionFactory]
    val rabbitConnection: Connection                  = mock[Connection]
    val declaredQueue: DeclareOk                      = mock[DeclareOk]
    val rabbitConfigGenerator: RabbitConfigGenerator  = app.injector.instanceOf[RabbitConfigGenerator]
    val rabbitConfig: RabbitConfig                    = rabbitConfigGenerator.rabbitUsageConfig()
    val queue: String                                 = rabbitConfig.queueName.get
    val queueDurability: Boolean                      = rabbitConfig.queueDurability.get
    val queueRestricted: Boolean                      = rabbitConfig.queueRestricted.get
    val queueDelete: Boolean                          = rabbitConfig.queueDelete.get

    val message: LoginEvent =
      LoginEvent(
        "electron",
        "uc",
        "webRTC",
        "xxx-xxx-xxx",
        "2018-05-09 10:04:25.375",
      )

    val messageByte: Array[Byte] = toBytes(Json.obj("LoginEvent" -> Json.toJson(message)))

    when(channel.queueDeclare(
      queue,
      queueDurability,
      queueRestricted,
      queueDelete,
      null)
    ).thenReturn(declaredQueue)
    when(channel.queueDeclare(
      queue,
      queueDurability,
      queueRestricted,
      queueDelete,
      null).getQueue
    ).thenReturn("usage.event_login")
    doNothing().when(channel)
      .basicPublish(
        "",
        "usage.event_login",
        null,
        messageByte
      )
  }

  "A UsageRabbitEventsFactory" should {
    "create a connection and declare a queue" in new Helper {
      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      val factoryUnderTest: UsageRabbitEventsProducer =
        new UsageRabbitEventsProducer(configuration, rabbitConfigGenerator)
      val connection: Try[Unit] = factoryUnderTest.createConnection()

      verify(configuration).newConnection
      verify(rabbitConnection).createChannel
      verify(channel.queueDeclare(queue, queueDurability, queueRestricted, queueDelete, null)).getQueue

      channel.queueDeclare(queue, queueDurability, queueRestricted, queueDelete, null).getQueue shouldEqual "usage.event_login"
      connection.isSuccess shouldBe true
    }

    "send a message to rabbitmq" in new Helper {
      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      val factoryUnderTest: UsageRabbitEventsProducer =
        new UsageRabbitEventsProducer(configuration, rabbitConfigGenerator)

      factoryUnderTest.createConnection()

      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      factoryUnderTest.publishMessage(message)
      verify(channel, times(1)).basicPublish("", "usage.event_login", null, messageByte)
    }
  }
}
