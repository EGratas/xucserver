package xivo.rabbitmq

import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import play.api.libs.json._
import play.api.mvc.Results
import xivo.models._

class XivoRabbitEventsSpec extends PlaySpec with Results with MockitoSugar {

  "A RabbitEventQueueEdited" should {
    "be created from json" in {
      val eventData = RabbitEventData(id = 1L, None)
      val event     = RabbitEventQueueEdited(eventData, "queue_edited")

      val json: JsValue = Json.parse(
        """
          | {"data": {"id": 1}, "name": "queue_edited"}
          | """.stripMargin
      )

      json.validate[RabbitEventQueueEdited] match {
        case JsSuccess(e, _) => e mustEqual event
        case JsError(errors) => fail()
      }
    }
  }

  "A RabbitEventAgentEdited" should {
    "be created from json" in {
      val eventData = RabbitEventAgentEditedData(2L)
      val event     = RabbitEventAgentEdited(eventData, "agent_edited")

      val json: JsValue = Json.parse(
        """
          | {"data":{"id":2},"name":"agent_edited","origin_uuid":null}
          | """.stripMargin
      )

      json.validate[RabbitEventAgentEdited] match {
        case JsSuccess(e, _) => e mustEqual event
        case JsError(errors) => fail()
      }
    }
  }

  "A RabbitEventAgentCreated" should {
    "be created from json" in {
      val eventData = RabbitEventAgentCreatedData(2L)
      val event     = RabbitEventAgentCreated(eventData, "agent_created")

      val json: JsValue = Json.parse(
        """
          | {"data":{"id":2},"name":"agent_created","origin_uuid":null}
          | """.stripMargin
      )

      json.validate[RabbitEventAgentCreated] match {
        case JsSuccess(e, _) => e mustEqual event
        case JsError(errors) => fail()
      }
    }
  }

  "A RabbitEventAgentDeleted" should {
    "be created from json" in {
      val eventData = RabbitEventAgentDeletedData(2L)
      val event     = RabbitEventAgentDeleted(eventData, "agent_deleted")

      val json: JsValue = Json.parse(
        """
          | {"data":{"id":2},"name":"agent_deleted","origin_uuid":null}
          | """.stripMargin
      )

      json.validate[RabbitEventAgentDeleted] match {
        case JsSuccess(e, _) => e mustEqual event
        case JsError(errors) => fail()
      }
    }
  }
}
