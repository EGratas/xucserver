package xivo.rabbitmq

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.TestProbe
import com.rabbitmq.client.AMQP.Queue.DeclareOk
import com.rabbitmq.client.*
import org.mockito.Mockito.*
import org.scalatest.TryValues.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import xivo.models.RabbitEventConnectionClosed
import xivo.xuc.{RabbitConfig, RabbitConfigGenerator}

import java.util.concurrent.TimeoutException
import scala.util.Try

class XivoRabbitEventsFactorySpec
    extends MockitoSugar
    with AnyWordSpecLike
    with Matchers
    with GuiceOneAppPerSuite {

  class Helper {
    val factory: XivoRabbitEventsFactory              = mock[XivoRabbitEventsFactory]
    val channel: Channel                              = mock[Channel]
    val configuration: ConnectionFactory              = mock[ConnectionFactory]
    val rabbitConnection: Connection                  = mock[Connection]
    val declaredQueue: DeclareOk                      = mock[DeclareOk]
    val rabbitConfigGenerator: RabbitConfigGenerator  = app.injector.instanceOf[RabbitConfigGenerator]
    val rabbitConfig: RabbitConfig                    = rabbitConfigGenerator.rabbitXivoConfig()

    when(channel.queueDeclare()).thenReturn(declaredQueue)
    when(channel.queueDeclare().getQueue).thenReturn("test-queue")
    implicit val system: ActorSystem = ActorSystem()
  }

  "A XivoRabbitEventsFactory" should {
    "create a connection and declare a queue" in new Helper {
      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      val factoryUnderTest: XivoRabbitEventsFactory =
        new XivoRabbitEventsFactory(configuration, rabbitConfigGenerator)
      val connection: Try[Unit] = factoryUnderTest.createConnection()

      verify(configuration).newConnection
      verify(rabbitConnection).createChannel
      verify(channel.queueDeclare()).getQueue

      channel.queueDeclare().getQueue shouldEqual "test-queue"
      connection.isSuccess shouldBe true
    }

    "can fail on creating a connection" in new Helper {
      when(configuration.newConnection).thenThrow(new TimeoutException)

      val factoryUnderTest: XivoRabbitEventsFactory =
        new XivoRabbitEventsFactory(configuration, rabbitConfigGenerator)
      val connection: Try[Unit] = factoryUnderTest.createConnection()

      verify(configuration).newConnection

      connection.isFailure shouldBe true
      connection.failure.exception shouldBe a[TimeoutException]
    }

    "declare and bind the queue to single key" in new Helper {
      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      val factoryUnderTest: XivoRabbitEventsFactory =
        new XivoRabbitEventsFactory(configuration, rabbitConfigGenerator)
      factoryUnderTest.createConnection()

      verify(channel.queueDeclare()).getQueue
      verify(channel).queueBind(
        "test-queue",
        "xivo-exchange",
        "config.queue.edited",
        null
      )
    }

    "declare and bind the queue to multiple keys" in new Helper {
      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      val factoryUnderTest: XivoRabbitEventsFactory =
        new XivoRabbitEventsFactory(configuration, rabbitConfigGenerator)
      factoryUnderTest.createConnection()

      verify(channel).queueBind(
        "test-queue",
        "xivo-exchange",
        "config.queue.edited",
        null
      )
      verify(channel).queueBind(
        "test-queue",
        "xivo-exchange",
        "config.queue.add",
        null
      )
    }

    "close the connection" in new Helper {
      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      val factoryUnderTest: XivoRabbitEventsFactory =
        new XivoRabbitEventsFactory(configuration, rabbitConfigGenerator)
      factoryUnderTest.createConnection()

      factoryUnderTest.shutdownConnection()

      verify(channel).queueUnbind(
        "test-queue",
        "xivo-exchange",
        "config.queue.edited"
      )
      verify(channel).queueUnbind(
        "test-queue",
        "xivo-exchange",
        "config.queue.add"
      )
      verify(channel).queueDelete("test-queue")
      verify(channel).close()
      rabbitConnection.close()
    }

    "notify listener on abrupt shutdown" in new Helper {
      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      val probe: TestProbe = TestProbe()

      val factoryUnderTest: XivoRabbitEventsFactory =
        new XivoRabbitEventsFactory(configuration, rabbitConfigGenerator)
      factoryUnderTest.createConnection()
      factoryUnderTest.addShutdownListener(probe.ref)

      rabbitConnection.close()
      factoryUnderTest.shutdownListener.get.shutdownCallback(new ShutdownSignalException(true, false, mock[Method], mock[Object]), probe.ref)
      probe.expectMsg(RabbitEventConnectionClosed)
    }

    "add shutdown listener on current connection" in new Helper {
      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      val probe: TestProbe = TestProbe()

      val factoryUnderTest: XivoRabbitEventsFactory =
        new XivoRabbitEventsFactory(configuration, rabbitConfigGenerator)
      factoryUnderTest.createConnection()
      factoryUnderTest.addShutdownListener(probe.ref)
      assert(factoryUnderTest.shutdownListener.get.isInstanceOf[ShutdownListenerWithRef])
    }

    "remove shutdown listener on current connection" in new Helper {
      when(configuration.newConnection).thenReturn(rabbitConnection)
      when(rabbitConnection.createChannel).thenReturn(channel)

      val probe: TestProbe = TestProbe()

      val factoryUnderTest: XivoRabbitEventsFactory =
        new XivoRabbitEventsFactory(configuration, rabbitConfigGenerator)
      factoryUnderTest.createConnection()
      factoryUnderTest.addShutdownListener(probe.ref)
      factoryUnderTest.removeShutdownListener()
      assert(factoryUnderTest.shutdownListener.isEmpty)
    }

  }
}
