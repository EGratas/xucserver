package xivo.ami

import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.asteriskjava.manager.event.{ConnectEvent, OriginateResponseEvent, QueueEntryEvent, QueueMemberEvent, QueueMemberPauseEvent, QueueSummaryEvent}
import org.joda.time.DateTime
import org.mockito.Mockito.{verify, verifyNoMoreInteractions, when}
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.message.QueueStatistics
import org.xivo.cti.model.{Counter, StatName}
import services.XucAmiBus.*
import services.calltracking.SipDriver
import services.channel.ChannelRequestProc.*
import services.config.ConfigRepository
import services.request.KeyLightRequest
import services.{XucAmiBus, XucEventBus}
import services.{AmiEventHelper, XucAmiBus}
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped, AgentSpyStart, AgentSpyStop}
import xivo.events.PhoneEventType.EventFailure
import xivo.events.*
import xivo.models.{Agent, QueueConfigUpdate}
import xivo.phonedevices.*
import xivo.xuc.ChanSpyNotificationConfig
import xivo.xucami.models.*
import xivo.xucami.userevents.*



class AmiBusConnectorSpec
    extends TestKitSpec("AmiBusConnectorSpec")
      with MockitoSugar 
      with AmiEventHelper {
  import xivo.models.LineHelper.makeLine

  class Helper {
    val configRepository: ConfigRepository   = mock[ConfigRepository]
    val agentManager: TestProbe       = TestProbe()
    val agentDeviceManager: TestProbe = TestProbe()
    val amiBus: XucAmiBus             = mock[XucAmiBus]
    val eventBus: XucEventBus           = mock[XucEventBus]
    val configDispatcher: TestProbe   = TestProbe()

    val spyConfig: ChanSpyNotificationConfig = mock[ChanSpyNotificationConfig]

    when(configRepository.getQueue("0")).thenReturn(
      Some(
        new QueueConfigUpdate(
          0,
          "",
          "",
          "",
          Some(""),
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          "",
          "",
          Some(0),
          Some(""),
          0,
          Some(0),
          Some(0),
          0,
          "",
          0
        )
      )
    )

    def actor(): (TestActorRef[AmiBusConnector], AmiBusConnector) = {
      val a = TestActorRef(
        new AmiBusConnector(
          configRepository,
          agentManager.ref,
          configDispatcher.ref,
          agentDeviceManager.ref,
          amiBus,
          eventBus,
          spyConfig
        )
      )
      (a, a.underlyingActor)

    }
  }

  "AmiBusConnector" should {
    "subscribe to the amiBus for ChannelEvents and AmiAgentEvent" in new Helper {
      val (ref, _) = actor()
      verify(amiBus).subscribe(ref, AmiType.ChannelEvent)
      verify(amiBus).subscribe(ref, AmiType.AmiAgentEvent)
    }

    "completeChannel with agentId and forward it to the agentManager" in new Helper {
      val (ref, _) = actor()
      val agent325: Agent = Agent(123, "thomas", "legrand", "325", "default")
      when(configRepository.getAgent("325")).thenReturn(Some(agent325))
      var channel = new Channel(
        "123",
        "SIP/eert",
        CallerId("unknownCalled", "0123456"),
        "123",
        monitored = MonitorState.ACTIVE,
        agentNumber = Some("325")
      )

      ref ! ChannelEvent(channel)

      verify(configRepository).getAgent("325")
      agentManager.expectMsg(AgentCallUpdate(123, MonitorState.ACTIVE))
    }

    "transform DialAnswered to AgentCallUpdate" in new Helper {
      val (ref, _) = actor()
      val agent325: Agent = Agent(123, "thomas", "legrand", "325", "default")
      when(configRepository.getAgent("325")).thenReturn(Some(agent325))
      var channel = new Channel(
        "123",
        "SIP/eert",
        CallerId("unknownCalled", "0123456"),
        "123",
        monitored = MonitorState.ACTIVE,
        agentNumber = Some("325")
      )

      ref ! ChannelEvent(channel)

      verify(configRepository).getAgent("325")
      agentManager.expectMsg(AgentCallUpdate(123, MonitorState.ACTIVE))
    }

    "Create set var action on key light request" in new Helper {
      val (ref, _) = actor()

      val keyLightRequest: KeyLightRequest = mock[KeyLightRequest]
      val amiRequest: AmiMessage = mock[AmiMessage]
      when(keyLightRequest.toAmi).thenReturn(amiRequest)

      ref ! keyLightRequest

      verify(amiBus).publish(amiRequest)

    }
    "forward Spy started to line manager" in new Helper {
      val (ref, _)   = actor()
      val listenerId = 789
      val listenedId = 451
      val spyStarted: SpyStarted = SpyStarted(
        SpyChannels(
          Channel("1", "b", CallerId("a", "2000"), "1"),
          Channel("2", "b", CallerId("a", "3000"), "1")
        )
      )

      when(configRepository.getAgentLoggedOnPhoneNumber("3000"))
        .thenReturn(Some(listenedId))
      when(spyConfig.enableChanSpyBeep).thenReturn(false)

      ref ! spyStarted

      verify(eventBus).publish(AgentListenStarted("3000", Some(listenedId)))
    }

    "forward Spy Stopped to line manager" in new Helper {
      val (ref, _)   = actor()
      val listenedId = 213L

      val spyStopped: SpyStopped = SpyStopped(Channel("1", "n", CallerId("u", "5000"), "1"))
      when(configRepository.getAgentLoggedOnPhoneNumber("5000"))
        .thenReturn(Some(listenedId))
      when(spyConfig.enableChanSpyBeep).thenReturn(false)

      ref ! spyStopped

      verify(eventBus).publish(AgentListenStopped("5000", Some(listenedId)))

    }

    "On spy started publish Beep Action Request and notify AgentDeviceManager" in new Helper {
      val (ref, _)   = actor()
      val listenedId = 451L
      val spyStarted: SpyStarted = SpyStarted(
        SpyChannels(
          Channel("1", "SIP/abcd-123", CallerId("a", "2000"), "1"),
          Channel("2", "SIP/abcd-456", CallerId("a", "3000"), "1")
        )
      )

      when(configRepository.getAgentLoggedOnPhoneNumber("3000"))
        .thenReturn(Some(listenedId))
      when(spyConfig.enableChanSpyBeep).thenReturn(true)

      ref ! spyStarted

      verify(amiBus).publish(BeepRequest(BeepActionRequest("SIP/abcd-456")))
      agentDeviceManager.expectMsg(AgentSpyStart(listenedId, "3000"))
    }

    "On spy stopped notify AgentDeviceManager" in new Helper {
      val (ref, _)   = actor()
      val listenedId = 451L
      val spyStopped: SpyStopped = SpyStopped(Channel("1", "n", CallerId("u", "5000"), "1"))

      when(configRepository.getAgentLoggedOnPhoneNumber("5000"))
        .thenReturn(Some(listenedId))
      when(spyConfig.enableChanSpyBeep).thenReturn(true)

      ref ! spyStopped
      agentDeviceManager.expectMsg(AgentSpyStop(listenedId, "5000"))
    }

    "On spy started not publish Beep Action Request if beep already executed" in new Helper {
      val (ref, _)   = actor()
      val listenedId = 451L
      val spyStarted: SpyStarted = SpyStarted(
        SpyChannels(
          Channel(
            "1",
            "SIP/abcd-123",
            CallerId("a", "2000"),
            "1",
            variables = Map("XIVO_CHANNEL_TO_BEEP" -> "SIP/abcd-456")
          ),
          Channel("2", "SIP/abcd-456", CallerId("a", "3000"), "1")
        )
      )

      when(configRepository.getAgentLoggedOnPhoneNumber("3000"))
        .thenReturn(Some(listenedId))
      when(spyConfig.enableChanSpyBeep).thenReturn(true)

      ref ! spyStarted

      verify(amiBus).subscribe(ref, AmiType.ChannelEvent)
      verify(amiBus).subscribe(ref, AmiType.AmiAgentEvent)
      verify(amiBus).subscribe(ref, AmiType.AmiEvent)
      verifyNoMoreInteractions(amiBus)
      agentDeviceManager.expectNoMessage()
    }

    "On spy started not publish Beep Action Request if beep is disabled" in new Helper {
      val (ref, _)   = actor()
      val listenedId = 451
      val spyStarted: SpyStarted = SpyStarted(
        SpyChannels(
          Channel("1", "SIP/abcd-123", CallerId("a", "2000"), "1"),
          Channel("2", "SIP/abcd-456", CallerId("a", "3000"), "1")
        )
      )

      when(configRepository.getAgentLoggedOnPhoneNumber("3000"))
        .thenReturn(Some(listenedId))
      when(spyConfig.enableChanSpyBeep).thenReturn(false)

      ref ! spyStarted

      verify(amiBus).subscribe(ref, AmiType.ChannelEvent)
      verify(amiBus).subscribe(ref, AmiType.AmiAgentEvent)
      verify(amiBus).subscribe(ref, AmiType.AmiEvent)
      verifyNoMoreInteractions(amiBus)
      agentDeviceManager.expectNoMessage()
    }

    "on ami AmiAgentEvent with agent login, send EventAgentLogin to agent manager" in new Helper {
      val (ref, _) = actor()

      val userEventAgentLogin = new UserEventAgentLogin("test")
      userEventAgentLogin.setAgentId("18")

      ref ! AmiAgentEvent(userEventAgentLogin)

      agentManager.expectMsg(EventAgentLogin(userEventAgentLogin))
    }
    "on ami AmiAgentEvent with agent logout, send EventAgentLogout to agent manager" in new Helper {
      val (ref, _) = actor()

      val userEventAgentLogoff = new UserEventAgentLogoff("test")
      userEventAgentLogoff.setAgentId("18")

      ref ! AmiAgentEvent(userEventAgentLogoff)

      agentManager.expectMsg(EventAgentLogout(userEventAgentLogoff))
    }

    "on ami AmiAgentEvent with QueueMemberPauseEvent(true), send EventAgentPause to agent manager" in new Helper {
      val (ref, _) = actor()

      val event = new QueueMemberPauseEvent("test")
      event.setMemberName("Agent/2000")
      event.setPaused(true)

      val agent18: Agent = Agent(18, "Some", "One", "2000", "default")
      when(configRepository.getAgent("2000")).thenReturn(Some(agent18))

      ref ! AmiAgentEvent(event)

      agentManager.expectMsg(EventAgentPause(18))
    }

    "extract pause reason from QueueMemberPauseEvent" in new Helper {
      val (ref, _) = actor()

      val reason      = "give_me_a_break"
      val agentId     = 18
      val agentNumber = "2000"
      val agent: Agent = Agent(agentId, "Some", "One", agentNumber, "default")

      val event = new QueueMemberPauseEvent("test")

      event.setMemberName(s"Agent/$agentNumber")
      event.setPaused(true)
      event.setPausedreason(reason)

      when(configRepository.getAgent("2000")).thenReturn(Some(agent))

      ref ! AmiAgentEvent(event)

      agentManager.expectMsg(EventAgentPause(18, Some(reason)))
    }

    "on ami AmiAgentEvent with QueueMemberPauseEvent(false), send EventAgentUnPause to agent manager" in new Helper {
      val (ref, _) = actor()

      val event = new QueueMemberPauseEvent("test")
      event.setMemberName("Agent/2000")
      event.setPaused(false)

      val agent18: Agent = Agent(18, "Some", "One", "2000", "default")
      when(configRepository.getAgent("2000")).thenReturn(Some(agent18))

      ref ! AmiAgentEvent(event)

      agentManager.expectMsg(EventAgentUnPause(18))
    }

    "on ami AmiAgentEvent with QueueMemberWrapupStart, send EventAgentWrapup to agent manager" in new Helper {
      val (ref, _) = actor()

      val event = new QueueMemberWrapupStartEvent("test")
      event.setMemberName("Agent/2000")

      val agent18: Agent = Agent(18, "Some", "One", "2000", "default")
      when(configRepository.getAgent("2000")).thenReturn(Some(agent18))

      ref ! AmiAgentEvent(event)

      agentManager.expectMsg(EventAgentWrapup(18))
    }

    "on ami AmiAgentEvent with QueueEntryEvent, publish updated QueueCallList" in new Helper {
      val (ref, _) = actor()

      val event = new QueueEntryEvent("test")
      event.setCallerIdName("User Two")
      event.setCallerIdNum("2002")
      event.setQueue("switchboard_hold")
      event.setUniqueId("123456789.123")
      event.setChannel("Local/1016@default-00000036;1")

      val queueCall: QueueCall = QueueCall(
        1,
        Some("User One"),
        "2001",
        new DateTime(),
        "Local/1016@default-00000036;1",
        "main"
      )
      val expectedQueueCall: QueueCall =
        queueCall.copy(name = Some("User Two"), number = "2002")

      ref ! AmiAgentEvent(event)

      configDispatcher.expectMsg(event)
    }

    "forward AmiAgentEvent with QueueMemberEvent for an agent in a paused state to agent manager" in new Helper {
      val (ref, _) = actor()
      val event    = new QueueMemberEvent("test")
      event.setMemberName("Agent/2000")
      event.setPaused(true)
      event.setPausedreason("testReason")

      val agent20: Agent = Agent(20, "Some", "One", "2000", "default")
      when(configRepository.getAgent("2000")).thenReturn(Some(agent20))

      ref ! AmiAgentEvent(event)

      agentManager.expectMsg(EventAgentPause(20, Some("testReason")))

    }

    "publish ami action request" in new Helper {
      val (ref, _) = actor()
      val requestToAmi: OutboundDial = OutboundDial(
        "0298143388",
        85,
        "3000",
        Map("VAR" -> "Value"),
        "10.20.10.3",
        123L,
        SipDriver.SIP
      )

      ref ! requestToAmi

      val expected: AmiRequest = AmiRequest(
        OutBoundDialActionRequest(
          "0298143388",
          "select_agent(agent=agent_85)",
          "3000",
          Map("VAR" -> "Value"),
          "10.20.10.3",
          123L,
          SipDriver.SIP
        )
      )
      verify(amiBus).publish(expected)
    }

    "publish RequestToMds to correct mds" in new Helper {
      val (ref, _) = actor()
      val requestToAmi: OutboundDial = OutboundDial(
        "0298143388",
        85,
        "3000",
        Map("VAR" -> "Value"),
        "10.20.10.3",
        123L,
        SipDriver.SIP
      )

      ref ! RequestToMds(requestToAmi, Some("mds1"))

      val expected: AmiRequest = AmiRequest(
        OutBoundDialActionRequest(
          "0298143388",
          "select_agent(agent=agent_85)",
          "3000",
          Map("VAR" -> "Value"),
          "10.20.10.3",
          123L,
          SipDriver.SIP
        ),
        targetMds = Some("mds1")
      )
      verify(amiBus).publish(expected)
    }

    "republish RequestToMds as RequestToAmi if toAmi fails" in new Helper {
      val (ref, _) = actor()
      val hc: HangupCommand = HangupCommand(
        makeLine(1, "default", "SIP", "0aog0v", None, None, "ip"),
        "1200"
      )

      ref ! RequestToMds(hc, Some("mds1"))

      verify(amiBus).publish(
        ChannelRequest(HangupActionReq("SIP/0aog0v", "1200"))
      )
    }

    "On Hangup Command publish ChannelLineActionRequest" in new Helper {
      val (ref, _) = actor()

      val hc: HangupCommand = HangupCommand(
        makeLine(1, "default", "SIP", "0aog0v", None, None, "ip"),
        "1200"
      )

      ref ! hc

      verify(amiBus).publish(
        ChannelRequest(HangupActionReq("SIP/0aog0v", "1200"))
      )
    }

    "On Hangup Channel Command publish update hangup source before hanging up " in new Helper {
      val (ref, _) = actor()
      val name     = "SIP/deadbeef-000001"

      val hc: HangupChannelCommand = HangupChannelCommand(name)

      ref ! hc

      verify(amiBus).publish(
        AmiRequest(SetChannelVarRequest(name, "CHANNEL(hangupsource)", name))
      )
      verify(amiBus).publish(AmiRequest(HangupActionRequest(name)))
    }

    "On attented transfer publish Channel Action Request" in new Helper {
      val (ref, _)    = actor()
      val destination = "3400"

      val atx: AttendedTransferCommand = AttendedTransferCommand(
        makeLine(1, "mycontext", "SIP", "0aog0v", None, None, "ip"),
        destination
      )

      ref ! atx

      verify(amiBus).publish(
        ChannelRequest(AtxFerActionReq("SIP/0aog0v", destination, "mycontext"))
      )

    }

    "On complete transfer publish channel action request" in new Helper {

      val (ref, _) = actor()

      val hc: CompleteTransferCommand = CompleteTransferCommand(
        makeLine(1, "default", "SIP", "0aog0v", None, None, "ip")
      )

      ref ! hc

      verify(amiBus).publish(
        ChannelRequest(CompleteXferActionReq("SIP/0aog0v"))
      )

    }

    "On cancel transfer publish channel action request" in new Helper {
      val (ref, _) = actor()

      val hc: CancelTransferCommand = CancelTransferCommand(
        makeLine(1, "default", "SIP", "0aog0v", None, None, "ip"),
        "1200"
      )

      ref ! hc

      verify(amiBus).publish(
        ChannelRequest(CancelXferActionReq("SIP/0aog0v", "1200"))
      )

    }

    "On direct transfer publish Channel Action Request" in new Helper {
      val (ref, _)    = actor()
      val destination = "5200"

      val atx: DirectTransferCommand = DirectTransferCommand(
        makeLine(1, "mycontext", "SIP", "0aog0v", None, None, "ip"),
        destination
      )

      ref ! atx

      verify(amiBus).publish(
        ChannelRequest(
          DirectXferActionReq("SIP/0aog0v", destination, "mycontext")
        )
      )

    }

    "On set data command publish channel action request" in new Helper {
      val (ref, _)                       = actor()
      val variables: Map[String, String] = Map("USR_Var" -> "Value")
      val phoneNb                        = "1200"

      val sd: SetDataCommand = SetDataCommand(phoneNb, variables)

      ref ! sd

      verify(amiBus).publish(
        ChannelRequest(SetDataActionReq(phoneNb, variables))
      )
    }

    "On set data append usr prefix" in new Helper {
      val (ref, _)                       = actor()
      val variables: Map[String, String] = Map("Var" -> "Value")
      var targetVar: Map[String, String] = Map("USR_Var" -> "Value")
      val phoneNb                        = "1200"

      val sd: SetDataCommand = SetDataCommand(phoneNb, variables)

      ref ! sd

      verify(amiBus).publish(
        ChannelRequest(SetDataActionReq(phoneNb, targetVar))
      )

    }

    "Publish QueuePauseRequest on the bus" in new Helper {
      val (ref, _) = actor()

      val queuePauseRequest: QueuePauseRequest = mock[QueuePauseRequest]

      ref ! queuePauseRequest

      verify(amiBus).publish(AmiRequest(queuePauseRequest.message))

    }

    "Publish QueueUnpauseRequest on the bus" in new Helper {
      val (ref, _) = actor()

      val queueUnpauseRequest: QueueUnpauseRequest = mock[QueueUnpauseRequest]

      ref ! queueUnpauseRequest

      verify(amiBus).publish(AmiRequest(queueUnpauseRequest.message))

    }

    "receive AMI Event Queue Summary Event" in new Helper() {
      val (ref, a) = actor()

      ref ! new ConnectEvent("test")
      val event = new QueueSummaryEvent("test")
      event.setQueue("0")
      event.setAvailable(1)
      event.setCallers(2)
      event.setHoldTime(3)
      event.setLongestHoldTime(4)

      val queueStatistics = new QueueStatistics;
      queueStatistics.setQueueId(
        0
      )
      queueStatistics.addCounter(
        new Counter(StatName.AvailableAgents, 1)
      )
      queueStatistics.addCounter(
        new Counter(StatName.TalkingAgents, 2)
      )
      queueStatistics.addCounter(
        new Counter(StatName.EWT, 3)
      )
      queueStatistics.addCounter(
        new Counter(StatName.LongestWaitTime, 4)
      )

      ref ! AmiEvent(event)

      verify(amiBus).subscribe(ref, AmiType.AmiEvent)
      val response: QueueStatistics = configDispatcher.expectMsgType[QueueStatistics];
      response.getCounters shouldBe queueStatistics.getCounters
      response.getQueueId shouldBe queueStatistics.getQueueId
    }

    "on AmiEvent with OriginateResponseEvent in Failure, send PhoneEvent configDispatcher" in new Helper {
      val (ref, _) = actor()

      val event = new OriginateResponseEvent((): Unit)
      event.setResponse("Failure")
      event.setCallerIdNum("1000")
      event.setCallerIdName("jdoe")
      event.setChannel("SIP/mr8sep")
      event.setUniqueId("123456.78")

      when(configRepository.getPhoneNbfromInterface("SIP/mr8sep"))
        .thenReturn(Some("2000"))

      ref ! AmiEvent(event, "default")

      configDispatcher.expectMsg(
        PhoneEvent(
          EventFailure,
          "2000",
          "1000",
          "jdoe",
          "123456.78",
          "123456.78"
        )
      )
    }

  }

}
