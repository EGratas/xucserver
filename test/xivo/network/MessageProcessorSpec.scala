package xivo.network

import java.security.InvalidParameterException

import org.json.JSONException
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.BeforeAndAfter
import org.xivo.cti.MessageParser
import org.xivo.cti.message.{PhoneConfigUpdate, PhoneStatusUpdate}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class MessageProcessorSpec
    extends AnyWordSpec
    with Matchers
    with BeforeAndAfter
    with MockitoSugar {
  var messageProcessor: MessageProcessor = _
  var messageParser: MessageParser       = _

  before {
    messageProcessor = new MessageProcessor("xxxx")
    messageParser = mock[MessageParser]
    messageProcessor.messageParser = messageParser
  }

  "messageprocessor should return one ctimessageResponse on a string message receveived" in {
    val message        = "{\"json\":1}"
    val decodedMessage = new PhoneStatusUpdate()

    doReturn(decodedMessage).when(messageParser).parseBuffer(message)

    val messages = messageProcessor.processBuffer(message + "\n")

    verify(messageParser).parseBuffer(message)
  }

  "should return 2 messages when a string message received with two carraige returns" in {
    val json1   = "{\"json\":1}"
    val json2   = "{\"json\":2}"
    val message = json1 + "\n" + json2 + "\n"
    val dMsg1   = new PhoneStatusUpdate
    val dMsg2   = new PhoneConfigUpdate

    doReturn(dMsg1).when(messageParser).parseBuffer(json1)
    doReturn(dMsg2).when(messageParser).parseBuffer(json2)

    val messages = messageProcessor.processBuffer(message)

    messages.size should be(2)

  }

  "should not process last partial message not carriage return terminated" in {
    val json1   = "{\"json\":1}"
    val message = json1 + "\n{\"json\":"
    val dMsg1   = new PhoneStatusUpdate
    doReturn(dMsg1).when(messageParser).parseBuffer(json1)

    val messages = messageProcessor.processBuffer(message)

    messages.size should be(1)

  }

  "should complete last partial message received" in {
    val messagePart1 = "{\"json\":"
    val messagePart2 = "54}\n"
    val dMsg1        = new PhoneStatusUpdate

    doReturn(dMsg1).when(messageParser).parseBuffer("{\"json\":54}")

    val firstMessages = messageProcessor.processBuffer(messagePart1)

    firstMessages.size should be(0)

    val sndMessages = messageProcessor.processBuffer(messagePart2)

    sndMessages.size should be(1)

    verify(messageParser).parseBuffer("{\"json\":54}")

  }

  "should not keep last partial message received" in {
    val messagePart1 = "{\"json\":"
    val messagePart2 = "54}\n"
    val dMsg1        = new PhoneStatusUpdate

    doReturn(dMsg1).when(messageParser).parseBuffer("{\"json\":54}")

    val firstMessages = messageProcessor.processBuffer(messagePart1)
    val sndMessages   = messageProcessor.processBuffer(messagePart2)

    verify(messageParser).parseBuffer("{\"json\":54}")

    val thirdMessages = messageProcessor.processBuffer("{\"json\":42}\n")

    verify(messageParser).parseBuffer("{\"json\":42}")

  }

  "should not return message on parser jsonexception" in {
    val message  = "{\"json\":}"
    val message2 = "{\"json\": 33}"
    val buffer   = message + "\n" + message2 + "\n"
    val dMsg1    = new PhoneStatusUpdate

    when(messageParser.parseBuffer(message))
      .thenThrow(new JSONException("malformed json"))
    doReturn(dMsg1).when(messageParser).parseBuffer(message2)

    val messages = messageProcessor.processBuffer(buffer)

    messages.size should be(1)
  }

  "should not return message on parser InvalidParameterException" in {
    val message = "546546"
    val buffer  = message + "\n"

    when(messageParser.parseBuffer(message))
      .thenThrow(new InvalidParameterException())

    val messages = messageProcessor.processBuffer(buffer)

    messages.size should be(0)
  }
  "should not return message on parser IllegalArgumentException" in {
    val message = "546546"
    val buffer  = message + "\n"

    when(messageParser.parseBuffer(message))
      .thenThrow(new IllegalArgumentException())

    val messages = messageProcessor.processBuffer(buffer)

    messages.size should be(0)
  }
  "should return None when parser returns null" in {
    val message = "546546"
    val buffer  = message + "\n"

    when(messageParser.parseBuffer(message)).thenReturn(null)
    val messages = messageProcessor.processBuffer(buffer)

    messages.size should be(0)
  }
}
