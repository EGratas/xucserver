package xivo.network

import java.net.ConnectException
import java.time.{OffsetDateTime, ZoneOffset}
import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestKit, TestProbe}
import pekkotest.TestKitSpec
import models.XivoUser
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.JsObject
import services.chat.model.*
import services.chat.{ChatUserState, ChatUtil, UpdateChatLinkRef}
import services.VideoChat.ChatStatus
import xivo.models.*
import xivo.network.ChatLink.{CheckUnreadMessages, RetrieveDirectChannels, StopChatLink, UpdateDirectChannels}

import scala.concurrent.Future
import scala.concurrent.duration.*

class ChatLinkSpec extends TestKitSpec("ChatLinkSpec") with MockitoSugar {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class Helper {
    val flashTextService: TestProbe = TestProbe()
    val ctiRouter: TestProbe        = TestProbe()
    val flashtTextUtil: ChatUtil   = mock[ChatUtil]
    val flashTextUserState: ChatUserState = ChatUserState(
      ChatUser(
        "jbond",
        Some("1000"),
        Some("James Bond"),
        Some("k7c6h9ur7ty4pxxe6sebbmruyy")
      ),
      ChatStatus.Available,
      Some(ctiRouter.ref),
      None,
      XivoUser(42, None, None, "James", Some("Bond"), Some("jbond"), None, None, None)
    )
    val chatBackendWS: ChatBackendWS = mock[ChatBackendWS]
    val token         = "x11sfpzoap86bfu78wtqjsjyky"

    val usernameFrom = "jbond"
    val guidFrom     = "k7c6h9ur7ty4pxxe6sebbmruyy"
    val userFrom: MattermostGuid     = MattermostGuid(guidFrom, usernameFrom, Some("James Bond"))

    val guidTo     = "ctmrsg87wbbfxb7cjegpi7ad9a"
    val usernameTo = "recipient"
    val userTo: MattermostGuid     = MattermostGuid(guidTo, usernameTo, Some("recipient"))
    val mattermostUserTo: MattermostUser = MattermostUser(
      "1",
      1,
      1,
      1,
      "user_2",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      MattermostTimeZone("", "", "")
    )

    val channelId = "12345"
    val channel: MattermostDirectChannel =
      MattermostDirectChannel(channelId, "team", s"${guidFrom}__$guidTo", "D")

    def actor(
        flashTextUserState: ChatUserState,
        token: String
    ): (TestActorRef[ChatLink], ChatLink) = {

      val a = TestActorRef[ChatLink](
        Props(
          new ChatLink(
            flashTextUserState,
            token,
            flashTextService.ref,
            chatBackendWS,
            guidFrom,
            flashtTextUtil
          )
        )
      )
      (a, a.underlyingActor)
    }
  }

  "ChatLink" should {
    "Send ref to flashTextService when start" in new Helper {
      val (ref, _) = actor(flashTextUserState, token)

      flashTextService.expectMsg(UpdateChatLinkRef(ref, "jbond"))
    }

    "send message (new channel)" in new Helper {
      val (ref, _) = actor(flashTextUserState, token)
      val content  = "this is some message"

      val message: SendDirectMattermostMessage = SendDirectMattermostMessage(userFrom, userTo, content, 0)
      val post: MattermostDirectMessageAck = mock[MattermostDirectMessageAck]

      when(chatBackendWS.createDirectMessageChannel(guidFrom, guidTo, token))
        .thenReturn(Future.successful(channel))
      when(
        chatBackendWS.createPostInChannel(
          NewMattermostChannelPost(channelId, content, 0, "jbond", "recipient"),
          token
        )
      )
        .thenReturn(Future.successful(post))

      ref ! message

      verify(chatBackendWS, timeout(1500)).createPostInChannel(
        NewMattermostChannelPost(channelId, content, 0, "jbond", "recipient"),
        token
      )
    }

    "send message (create channel)" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)
      val content  = "this is some message"

      a.directChannels = Map(guidTo -> channel)

      val message: SendDirectMattermostMessage = SendDirectMattermostMessage(userFrom, userTo, content, 0)
      val post: MattermostDirectMessageAck = mock[MattermostDirectMessageAck]

      when(
        chatBackendWS.createPostInChannel(
          NewMattermostChannelPost(channelId, content, 0, "jbond", "recipient"),
          token
        )
      )
        .thenReturn(Future.successful(post))

      ref ! message

      verify(chatBackendWS, times(0)).createDirectMessageChannel(
        guidFrom,
        guidTo,
        token
      )
      verify(chatBackendWS, timeout(1500)).createPostInChannel(
        NewMattermostChannelPost(channelId, content, 0, "jbond", "recipient"),
        token
      )
    }

    "update direct channels" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)
      val member: MattermostChannelMember = MattermostChannelMember(channelId, userTo.guid)

      a.directChannels = Map()

      ref ! UpdateDirectChannels(channel, List(member))

      a.directChannels shouldEqual Map(userTo.guid -> channel)
    }

    "retrieve direct channels" ignore new Helper {
      var (ref, a) = actor(flashTextUserState, token)
      val member: MattermostChannelMember = MattermostChannelMember(channelId, userTo.guid)

      a.directChannels = Map()

      when(chatBackendWS.getChannelsForUser(userFrom.guid))
        .thenReturn(Future.successful(List(channel)))
      when(
        chatBackendWS.getChannelMembers(
          channelId,
          token,
          Some(userFrom.guid),
          true
        )
      )
        .thenReturn(Future.successful(List(member)))

      ref ! RetrieveDirectChannels

      verify(chatBackendWS, timeout(1500)).getChannelsForUser(userFrom.guid)
      verify(chatBackendWS, timeout(1500)).getChannelMembers(
        channel.id,
        token,
        flashTextUserState.user.guid,
        filtered = true
      )

      a.directChannels shouldEqual Map(userTo.guid -> channel)
    }

    "retrieve message history (existing conversation)" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)
      val created1: (Long, OffsetDateTime) = (
        1582203515356L,
        new java.util.Date(1582203515356L).toInstant.atOffset(ZoneOffset.UTC)
      )
      val created2: (Long, OffsetDateTime) = (
        1582039531647L,
        new java.util.Date(1582039531647L).toInstant.atOffset(ZoneOffset.UTC)
      )
      val posts: List[MattermostDirectMessage] = List(
        MattermostDirectMessage(
          "post-id-1",
          channelId,
          "MESSAGE-ORDER-1",
          created1._1,
          guidFrom
        ),
        MattermostDirectMessage(
          "post-id-2",
          channelId,
          "MESSAGE-ORDER-2",
          created2._1,
          guidTo
        )
      )

      val msg1: Message = Message(
        ChatUser(
          userFrom.username,
          Some("1000"),
          userFrom.displayName,
          Some(userFrom.guid)
        ),
        ChatUser(
          userTo.username,
          Some("1001"),
          userTo.displayName,
          Some(userTo.guid)
        ),
        "MESSAGE-ORDER-1",
        created1._2,
        0
      )

      val msg2: Message = Message(
        ChatUser(
          userTo.username,
          Some("1001"),
          userTo.displayName,
          Some(userTo.guid)
        ),
        ChatUser(
          userFrom.username,
          Some("1000"),
          userFrom.displayName,
          Some(userFrom.guid)
        ),
        "MESSAGE-ORDER-2",
        created2._2,
        0
      )

      a.directChannels = Map(guidTo -> channel)

      when(chatBackendWS.getPostsForChannel(channelId, token))
        .thenReturn(Future.successful(posts))

      when(flashtTextUtil.mattermostOrderRecipients(userFrom, userTo))
        .thenReturn(posts => List(msg1, msg2))

      when(flashtTextUtil.getPhoneNumber(userFrom.username))
        .thenReturn(Some("1000"))

      when(flashtTextUtil.getPhoneNumber(userTo.username))
        .thenReturn(Some("1001"))

      ref ! GetDirectMattermostMessagesHistory(userFrom, userTo, 0)

      flashTextService.expectMsg(UpdateChatLinkRef(ref, "jbond"))

      ctiRouter.expectMsg(
        MessageHistory(
          (
            ChatUser(
              userFrom.username,
              Some("1000"),
              userFrom.displayName,
              Some(userFrom.guid)
            ),
            ChatUser(
              userTo.username,
              Some("1001"),
              userTo.displayName,
              Some(userTo.guid)
            )
          ),
          List(
            Message(
              ChatUser(
                userFrom.username,
                Some("1000"),
                userFrom.displayName,
                Some(userFrom.guid)
              ),
              ChatUser(
                userTo.username,
                Some("1001"),
                userTo.displayName,
                Some(userTo.guid)
              ),
              "MESSAGE-ORDER-1",
              created1._2,
              0
            ),
            Message(
              ChatUser(
                userTo.username,
                Some("1001"),
                userTo.displayName,
                Some(userTo.guid)
              ),
              ChatUser(
                userFrom.username,
                Some("1000"),
                userFrom.displayName,
                Some(userFrom.guid)
              ),
              "MESSAGE-ORDER-2",
              created2._2,
              0
            )
          ),
          0
        )
      )
    }

    "retrieve message history (new conversation)" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)
      val posts: List[MattermostDirectMessage] =
        List(mock[MattermostDirectMessage], mock[MattermostDirectMessage])

      val msg1: Message = mock[Message]
      val msg2: Message = mock[Message]

      val historyMock: MessageHistory = mock[MessageHistory]
      when(historyMock.sequence).thenReturn(0L)
      when(historyMock.messages).thenReturn(List())
      when(historyMock.messages).thenReturn(List())

      a.directChannels = Map()

      when(chatBackendWS.createDirectMessageChannel(guidFrom, guidTo, token))
        .thenReturn(Future.successful(channel))
      when(chatBackendWS.getPostsForChannel(channelId, token))
        .thenReturn(Future.successful(posts))
      when(flashtTextUtil.mattermostOrderRecipients(userFrom, userTo))
        .thenReturn(posts => List(msg1, msg2))
      when(flashtTextUtil.getPhoneNumber(userFrom.username))
        .thenReturn(Some("1000"))
      when(flashtTextUtil.getPhoneNumber(userTo.username))
        .thenReturn(Some("1001"))

      ref ! GetDirectMattermostMessagesHistory(userFrom, userTo, 0)

      flashTextService.expectMsg(UpdateChatLinkRef(ref, "jbond"))

      ctiRouter.expectMsg(
        MessageHistory(
          (
            ChatUser(
              userFrom.username,
              Some("1000"),
              userFrom.displayName,
              Some(userFrom.guid)
            ),
            ChatUser(
              userTo.username,
              Some("1001"),
              userTo.displayName,
              Some(userTo.guid)
            )
          ),
          List(msg1, msg2),
          0
        )
      )
    }

    "retrieve empty history" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)

      a.directChannels = Map()
      when(chatBackendWS.createDirectMessageChannel(guidFrom, guidTo, token))
        .thenReturn(Future.successful(channel))
      when(chatBackendWS.getPostsForChannel(channelId, token))
        .thenReturn(Future.successful(List()))
      when(flashtTextUtil.mattermostOrderRecipients(userFrom, userTo))
        .thenReturn(posts => List())
      when(flashtTextUtil.getPhoneNumber(userFrom.username))
        .thenReturn(None)
      when(flashtTextUtil.getPhoneNumber(userTo.username))
        .thenReturn(None)

      ref ! GetDirectMattermostMessagesHistory(userFrom, userTo, 0)

      ctiRouter.expectMsg(
        MessageHistory(
          (
            ChatUser(
              userFrom.username,
              None,
              userFrom.displayName,
              Some(userFrom.guid)
            ),
            ChatUser(
              userTo.username,
              None,
              userTo.displayName,
              Some(userTo.guid)
            )
          ),
          List(),
          0
        )
      )
    }

    "notify user with unread messages in direct channels" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)

      a.directChannels = Map(guidTo -> channel)

      val created1: (Long, OffsetDateTime) = (
        1582203515356L,
        new java.util.Date(1582203515356L).toInstant.atOffset(ZoneOffset.UTC)
      )
      val created2: (Long, OffsetDateTime) = (
        1582039531647L,
        new java.util.Date(1582039531647L).toInstant.atOffset(ZoneOffset.UTC)
      )

      val unread1: Message = Message(
        ChatUser(
          userFrom.username,
          Some("1000"),
          userFrom.displayName,
          Some(userFrom.guid)
        ),
        ChatUser(
          userTo.username,
          Some("1001"),
          userTo.displayName,
          Some(userTo.guid)
        ),
        "unread message",
        created1._2,
        0
      )

      val mattermostUnreadMessage: MattermostDirectMessage = MattermostDirectMessage(
        "post-id-2",
        channelId,
        "unread message",
        created2._1,
        guidTo
      )
      val mattermostUnreadNotif: MattermostUnreadNotification = MattermostUnreadNotification(
        DirectionIsToUser,
        userFrom.username,
        2L,
        mattermostUnreadMessage,
        created2._2,
        0
      )

      when(
        chatBackendWS.getUnreadMessagessCounterByChannel(
          channel.id,
          userFrom.guid,
          token
        )
      )
        .thenReturn(
          Future.successful(MattermostUnreadCounter("team-1", channelId, 1))
        )
      when(chatBackendWS.getCtiUser(userTo.guid))
        .thenReturn(Future.successful(mattermostUserTo))
      when(
        chatBackendWS.getPostsAroundLastUnread(channelId, userFrom.guid, token)
      )
        .thenReturn(Future.successful(List(mattermostUnreadMessage)))
      when(flashtTextUtil.mattermostOrderRecipients(mattermostUnreadNotif))
        .thenReturn(Future.successful(unread1))

      ref ! CheckUnreadMessages(channel)

      val expected: MessageUnreadNotification = MessageUnreadNotification(List(unread1), 0)

      verify(chatBackendWS, Mockito.timeout(500))
        .getUnreadMessagessCounterByChannel(
          any[String],
          any[String],
          any[String]
        )
      verify(chatBackendWS, Mockito.timeout(500))
        .getCtiUser("ctmrsg87wbbfxb7cjegpi7ad9a")
      verify(chatBackendWS, Mockito.timeout(500))
        .getPostsAroundLastUnread(any[String], any[String], any[String])
      ctiRouter.expectMsg(expected)
    }

    "not notify user if there are no unread messages" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)

      a.directChannels = Map(guidTo -> channel)

      val created1: (Long, OffsetDateTime) = (
        1582203515356L,
        new java.util.Date(1582203515356L).toInstant.atOffset(ZoneOffset.UTC)
      )

      when(
        chatBackendWS.getUnreadMessagessCounterByChannel(
          channel.id,
          userFrom.guid,
          token
        )
      )
        .thenReturn(
          Future.successful(MattermostUnreadCounter("team-1", channelId, 0))
        )

      ref ! CheckUnreadMessages(channel)

      ctiRouter.expectNoMessage(250.milliseconds)
    }

    "mark channel messages as read (existing conversation)" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)
      a.directChannels = Map(guidTo -> channel)

      when(chatBackendWS.setActiveChannel(channelId, guidFrom, token))
        .thenReturn(
          Future.successful(MattermostViewChannelResponse("OK", mock[JsObject]))
        )

      ref ! MarkMattermostMessagesAsRead(userFrom, userTo, 0)

      verify(chatBackendWS, timeout(500)).setActiveChannel(
        channel.id,
        guidFrom,
        token
      )
      ctiRouter.expectMsg(MessageMarkAsRead(usernameTo, "OK", 0))
    }

    "mark channel messages as read (new conversation)" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)
      a.directChannels = Map()

      when(chatBackendWS.createDirectMessageChannel(guidFrom, guidTo, token))
        .thenReturn(Future.successful(channel))
      when(chatBackendWS.setActiveChannel(channelId, guidFrom, token))
        .thenReturn(
          Future.successful(MattermostViewChannelResponse("OK", mock[JsObject]))
        )

      ref ! MarkMattermostMessagesAsRead(userFrom, userTo, 0)

      verify(chatBackendWS, timeout(500)).setActiveChannel(
        channel.id,
        guidFrom,
        token
      )
      ctiRouter.expectMsg(MessageMarkAsRead(usernameTo, "OK", 0))
    }

    "handle error if user id is in unknown format" in new Helper {
      var (ref, a)        = actor(flashTextUserState, token)
      val invalidUsername = "unknown" // valid format is user_

      a.directChannels = Map(guidTo -> channel)

      val created1: (Long, OffsetDateTime) = (
        1582203515356L,
        new java.util.Date(1582203515356L).toInstant.atOffset(ZoneOffset.UTC)
      )
      val created2: (Long, OffsetDateTime) = (
        1582039531647L,
        new java.util.Date(1582039531647L).toInstant.atOffset(ZoneOffset.UTC)
      )

      val unread1: Message = Message(
        ChatUser(
          userFrom.username,
          Some("1000"),
          userFrom.displayName,
          Some(userFrom.guid)
        ),
        ChatUser(
          userTo.username,
          Some("1001"),
          userTo.displayName,
          Some(userTo.guid)
        ),
        "unread message",
        created1._2,
        0
      )

      val mattermostUnreadMessage: MattermostDirectMessage = MattermostDirectMessage(
        "post-id-2",
        channelId,
        "unread message",
        created2._1,
        guidTo
      )
      val mattermostUnreadNotif: MattermostUnreadNotification = MattermostUnreadNotification(
        DirectionIsToUser,
        userFrom.username,
        2L,
        mattermostUnreadMessage,
        created2._2,
        0
      )

      when(
        chatBackendWS.getUnreadMessagessCounterByChannel(
          channel.id,
          userFrom.guid,
          token
        )
      )
        .thenReturn(
          Future.successful(MattermostUnreadCounter("team-1", channelId, 1))
        )
      when(chatBackendWS.getCtiUser(userTo.guid))
        .thenReturn(
          Future.successful(mattermostUserTo.copy(username = invalidUsername))
        )
      when(
        chatBackendWS.getPostsAroundLastUnread(channelId, userFrom.guid, token)
      )
        .thenReturn(Future.successful(List(mattermostUnreadMessage)))
      when(flashtTextUtil.mattermostOrderRecipients(mattermostUnreadNotif))
        .thenReturn(Future.successful(unread1))

      ref ! CheckUnreadMessages(channel)

      verify(chatBackendWS, Mockito.timeout(500))
        .getUnreadMessagessCounterByChannel(
          any[String],
          any[String],
          any[String]
        )
      verify(chatBackendWS, Mockito.timeout(500)).getCtiUser(any[String])
      verifyNoMoreInteractions(chatBackendWS)
      ctiRouter.expectNoMessage(250.milliseconds)
    }

    "handle error if receiving user is not within a list of existing direct channels" in new Helper {
      var (ref, a) = actor(flashTextUserState, token)

      a.directChannels = Map()

      when(
        chatBackendWS.getUnreadMessagessCounterByChannel(
          channel.id,
          userFrom.guid,
          token
        )
      )
        .thenReturn(
          Future.successful(MattermostUnreadCounter("team-1", channelId, 1))
        )

      ref ! CheckUnreadMessages(channel)

      verify(chatBackendWS, Mockito.timeout(500))
        .getUnreadMessagessCounterByChannel(
          any[String],
          any[String],
          any[String]
        )
      verifyNoMoreInteractions(chatBackendWS)
      ctiRouter.expectNoMessage(250.milliseconds)
    }

    "handle message history error" in new Helper {
      reset(chatBackendWS)
      var (ref, a) = actor(flashTextUserState, token)

      a.directChannels = Map(guidTo -> channel)

      when(chatBackendWS.getPostsForChannel(channel.id, token))
        .thenReturn(Future.failed(new WebServiceException("error")))

      ref ! GetDirectMattermostMessagesHistory(userFrom, userTo, 0)

      ctiRouter.expectMsg(250.millis, RequestNack(0))
    }

    "handle WebServiceException" in new Helper {
      val (ref, _) = actor(flashTextUserState, token)

      when(chatBackendWS.createDirectMessageChannel(guidFrom, guidTo, token))
        .thenReturn(Future.failed(new WebServiceException("error")))

      ref ! SendDirectMattermostMessage(userFrom, userTo, "msg", 0)

      verify(chatBackendWS, timeout(1500)).createDirectMessageChannel(
        guidFrom,
        guidTo,
        token
      )
      ctiRouter.expectMsg(RequestNack(0))
    }

    "handle ConnectException" in new Helper {
      val (ref, _) = actor(flashTextUserState, token)

      when(chatBackendWS.createDirectMessageChannel(guidFrom, guidTo, token))
        .thenReturn(Future.failed(new ConnectException("error")))

      ref ! SendDirectMattermostMessage(userFrom, userTo, "msg", 0)

      verify(chatBackendWS, timeout(1500)).createDirectMessageChannel(
        guidFrom,
        guidTo,
        token
      )
      ctiRouter.expectMsg(RequestNack(0))
    }

    "stop when receiving stop message" in new Helper {
      val (ref, _) = actor(flashTextUserState, token)

      val probe: TestProbe = TestProbe()
      probe.watch(ref)

      ref ! StopChatLink

      probe.expectTerminated(ref)
    }
  }
}
