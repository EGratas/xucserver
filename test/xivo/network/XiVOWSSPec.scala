package xivo.network

import org.apache.pekko.stream.Materializer
import models.Token
import org.joda.time.DateTime
import play.api.http.Status._
import play.api.libs.ws.{WSClient, WSResponse}
import xivo.xuc.XucBaseConfig
import xuctest.ScalaTestTools
import play.api.test._
import play.api.mvc._

import play.core.server.Server
import play.api.routing.sird._

import scala.concurrent.Await
import scala.concurrent.duration._

class XiVOWSSPec extends ScalaTestTools {

  class Helper {
    implicit val materializer: Materializer = mock[Materializer]
    val xucBaseConfig: XucBaseConfig        = mock[XucBaseConfig]
    val wsClient: WSClient                  = mock[WSClient]

    val cfgHost     = "localhost"
    val cfgUri      = "import"
    val cfgProtocol = "http"

    val date  = new DateTime(2017, 11, 10, 1, 22, 3)
    val token: Token = Token("token", date, date, "cti", Some("cti"), List.empty)

    val headers: Map[String,String] = Map(
      "X-Auth-Token" -> token.token,
      "Content-Type" -> "text/csv;charset=UTF-8"
    )
    val csvResult: String =
      "\uFEFFcompany,email,fax,firstname,lastname,mobile,number\r\ncorp,j.doe@my.corp,3333,john,doe,2222,1111\r\ncorp,m.twain@my.corp,8888,mark,twain,6666,"

    val payload: Some[String] = Some(csvResult)
  }

  "XiVO WS" should {
    "create post request" in new Helper {

      Server.withRouterFromComponents() { components =>
        import Results._
        import components.{defaultActionBuilder => Action}
        { case POST(p"/personal") =>
          Action(Created)
        }
      } { implicit port =>
        WsTestClient.withClient { client =>
          val result = new XiVOWSImpl(xucBaseConfig, client)
            .post(
              host = cfgHost,
              uri = "personal",
              protocol = cfgProtocol,
              port = Some(port.value),
              headers = headers
            )

          result.headers should contain theSameElementsAs Map(
            "X-Auth-Token" -> List(token.token),
            "Content-Type" -> List("text/csv;charset=UTF-8")
          )
          result.method shouldBe "POST"
          result.url shouldBe s"http://localhost:$port/personal"

          val executedResult: WSResponse =
            Await.result(result.execute(), 3.seconds)
          executedResult.status shouldBe CREATED
        }
      }
    }

    "create generic post request" in new Helper {

      Server.withRouterFromComponents() { components =>
        import Results._
        import components.{defaultActionBuilder => Action}
        { case POST(p"/personal/import") =>
          Action(Created)
        }
      } { implicit port =>
        WsTestClient.withClient { client =>
          val result = new XiVOWSImpl(xucBaseConfig, client)
            .genericPost(
              host = cfgHost,
              uri = "personal/import",
              protocol = cfgProtocol,
              port = Some(port.value),
              headers = headers,
              payload = payload
            )
          result.headers should contain theSameElementsAs Map(
            "X-Auth-Token" -> List(token.token),
            "Content-Type" -> List("text/csv;charset=UTF-8")
          )
          result.method shouldBe "POST"
          result.url shouldBe s"http://localhost:$port/personal/import"

          val executedResult: WSResponse =
            Await.result(result.execute(), 3.seconds)
          executedResult.status shouldBe CREATED
        }
      }
    }

    "create get request" in new Helper {
      Server.withRouterFromComponents() { components =>
        import Results._
        import components.{defaultActionBuilder => Action}
        { case GET(p"/personal") =>
          Action(Ok)
        }
      } { implicit port =>
        WsTestClient.withClient { client =>
          val result = new XiVOWSImpl(xucBaseConfig, client)
            .get(
              host = cfgHost,
              uri = "personal",
              protocol = cfgProtocol,
              port = Some(port.value),
              headers = headers
            )

          result.headers should contain theSameElementsAs Map(
            "X-Auth-Token" -> List(token.token),
            "Content-Type" -> List("text/csv;charset=UTF-8")
          )
          result.method shouldBe "GET"
          result.url shouldBe s"http://localhost:$port/personal"

          val executedResult: WSResponse =
            Await.result(result.execute(), 3.seconds)
          executedResult.status shouldBe OK
        }
      }
    }

    "create put request" in new Helper {
      Server.withRouterFromComponents() { components =>
        import Results._
        import components.{defaultActionBuilder => Action}
        { case PUT(p"/personal") =>
          Action(Ok)
        }
      } { implicit port =>
        WsTestClient.withClient { client =>
          val result = new XiVOWSImpl(xucBaseConfig, client)
            .put(
              host = cfgHost,
              uri = "personal",
              protocol = cfgProtocol,
              port = Some(port.value),
              headers = headers
            )

          result.headers should contain theSameElementsAs Map(
            "X-Auth-Token" -> List(token.token),
            "Content-Type" -> List("text/csv;charset=UTF-8")
          )
          result.method shouldBe "PUT"
          result.url shouldBe s"http://localhost:$port/personal"

          val executedResult: WSResponse =
            Await.result(result.execute(), 3.seconds)
          executedResult.status shouldBe OK
        }
      }
    }

    "create del" in new Helper {
      Server.withRouterFromComponents() { components =>
        import Results._
        import components.{defaultActionBuilder => Action}
        { case DELETE(p"/personal") =>
          Action(NoContent)
        }
      } { implicit port =>
        WsTestClient.withClient { client =>
          val result = new XiVOWSImpl(xucBaseConfig, client)
            .del(
              host = cfgHost,
              uri = "personal",
              protocol = cfgProtocol,
              port = Some(port.value),
              headers = headers
            )

          result.headers should contain theSameElementsAs Map(
            "X-Auth-Token" -> List(token.token),
            "Content-Type" -> List("text/csv;charset=UTF-8")
          )
          result.method shouldBe "DELETE"
          result.url shouldBe s"http://localhost:$port/personal"

          val executedResult: WSResponse =
            Await.result(result.execute(), 3.seconds)
          executedResult.status shouldBe NO_CONTENT
        }
      }
    }
  }
}
