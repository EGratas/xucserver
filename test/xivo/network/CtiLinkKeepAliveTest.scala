package xivo.network

import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import org.json.JSONObject
import pekkotest.TestKitSpec
import org.xivo.cti.MessageFactory
import xivo.network.CtiLinkKeepAlive.{StartKeepAlive, StopKeepALive}

class CtiLinkKeepAliveTest extends TestKitSpec("CtiLinkKeepAliveTest") {

  class Helper {
    def actor(): (TestActorRef[CtiLinkKeepAlive], CtiLinkKeepAlive) = {
      val a = TestActorRef[CtiLinkKeepAlive](CtiLinkKeepAlive.props("testUser"))
      (a, a.underlyingActor)
    }
  }

  "keep alive" should {
    "send a user status to ctiLink" in new Helper {
      val (ref, ctiLinkKeepAlive) = actor()
      val ctiLink: TestProbe = TestProbe()

      ref ! StartKeepAlive("67", ctiLink.ref)

      val msg: JSONObject = new MessageFactory().createGetUserStatus("67")

      ref ! msg

      ctiLink.expectMsg(msg)

    }

    "Restart on StopKeepALive" in new Helper {
      val (ref, ctiLinkKeepAlive) = actor()
      val ctiLink: TestProbe = TestProbe()

//       Im in the context CtiLinkKeepAlive.recieved and i want to start
      ref ! StartKeepAlive("67", ctiLink.ref)
// I 've jumped into the context CtiLinkKeepAlive.keepAlive
//      I stop the keepAlive context and get back to CtiLinkKeepAlive.recieved to be able to restart
      ref ! StopKeepALive

      val msg2: JSONObject = new MessageFactory().createGetUserStatus("68")
// I send a message that should be received by the context CtiLinkKeepAlive.keepAlive
      ref ! msg2

      // I'm still in the context CtiLinkKeepAlive.recieved so i didn't received this message
      ctiLink.expectNoMessage()

//      I resart the keep alive and so i go into the context CtiLinkKeepAlive.keepAlive
      ref ! StartKeepAlive("68", ctiLink.ref)
      ref ! msg2

//      Now im in the right context to receive a message
      ctiLink.expectMsg(msg2)

    }
  }

}
