package xivo.network

import models.XucUser

import java.net.InetSocketAddress
import org.apache.pekko.actor.{ActorRef, Props, Scheduler}
import org.apache.pekko.io.Tcp
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import org.apache.pekko.util.ByteString
import org.json.JSONObject
import org.mockito.ArgumentMatchers.{any, same}
import pekkotest.TestKitSpec
import org.mockito.Mockito.verify
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{LoginCapasAck, PhoneStatusUpdate}
import org.xivo.cti.model.Endpoint
import org.xivo.cti.model.Endpoint.EndpointType
import services.Start
import xivo.websocket.LinkState.*
import xivo.websocket.LinkStatusUpdate
import xivo.xuc.XucBaseConfig
import xuctest.XucUserHelper
import org.mockito.Mockito.when

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{DurationInt, FiniteDuration}

class CtiLinkSpec
    extends TestKitSpec("CtiLinkSpec")
    with MockitoSugar
    with GuiceOneAppPerSuite
    with XucUserHelper {

  import xivo.network.CtiLink._

  class Helper {

    val messageProcessor: MessageProcessor = mock[MessageProcessor]
    val messageFactory                     = new MessageFactory()
    val xucConfig: XucBaseConfig           = mock[XucBaseConfig]
    val parent: TestProbe                  = TestProbe()
    val ctiLoginStep: TestProbe            = TestProbe()
    val endpoint                           = new InetSocketAddress("0.0.0.0", 0)

    when(xucConfig.XIVOCTI_HOST).thenReturn("0.0.0.0")
    when(xucConfig.XIVOCTI_PORT).thenReturn("0")
    when(xucConfig.XivoCtiVersion).thenReturn("2.1")
    when(xucConfig.metricsRegistryName).thenReturn("testMetrics")

    def actor(username: String): (TestActorRef[CtiLink], CtiLink) = {
      var a = TestActorRef[CtiLink](
        Props(
          new CtiLink(username, messageProcessor, xucConfig)
            with OnTcpFailedKill {
            override def onConnectFailed(): Unit = {}
          }
        ),
        parent.ref,
        "testCtiLink"
      )
      a.underlyingActor.ctiLoginStep = ctiLoginStep.ref
      (a, a.underlyingActor)
    }

    def fullActor(username: String): (TestActorRef[CtiLink], CtiLink) = {
      var a = TestActorRef[CtiLink](
        Props(
          new CtiLink(username, messageProcessor, xucConfig)
            with OnTcpFailedKill
        ),
        parent.ref,
        "testCtiLink"
      )
      a.underlyingActor.ctiLoginStep = ctiLoginStep.ref
      (a, a.underlyingActor)
    }

    def errorActorKilled(username: String): TestActorRef[CtiLink] =
      TestActorRef[CtiLink](
        Props(
          new CtiLink(username, messageProcessor, xucConfig)
            with OnTcpFailedKill {
            override def receive: Receive = onFailed
          }
        ),
        parent.ref,
        "testCtiLinkOnFailedKill"
      )

    def errorActorRestart(
        username: String
    ): (TestActorRef[CtiLink], CtiLink) = {
      var a = TestActorRef[CtiLink](
        Props(
          new CtiLink(username, messageProcessor, xucConfig)
            with OnTcpFailedRestart {
            override val scheduler: Scheduler = mock[Scheduler]

            override def receive: Receive = onFailed
          }
        ),
        parent.ref,
        "testCtiLinkOnFailedKill"
      )
      (a, a.underlyingActor)
    }

  }

  "A CtiLink actor" should {

    "forward login messages to the ctiLoginStepActor" in new Helper {
      var (ref, ctiLink) = actor("loginmessage")

      val message = new LoginCapasAck()

      ctiLink.processDecodedMessage(message)
      ctiLoginStep.expectMsg(message)
    }

    "parse received messages and send them to its parent except first login messages" in new Helper {
      val (ref, _)    = actor("firstlogin")
      val message     = "{.... json phone status udpate ....}"
      val phoneStatus = new PhoneStatusUpdate()
      when(messageProcessor.processBuffer(message)).thenReturn(Array(phoneStatus))

      ref ! Tcp.Received(ByteString(message))

      parent.expectMsgClass(classOf[PhoneStatusUpdate])
    }

    "not send received cti objects before the network connection is initialized" in new Helper {
      var (ref, ctiLink) = actor("receive")
      val ctiMessage: JSONObject =
        messageFactory.createDial(new Endpoint(EndpointType.PHONE, "1234"))
      val connection: TestProbe = TestProbe()
      ctiLink.connection = connection.ref
      ref ! ctiMessage

      connection.expectNoMessage(100.millis)
    }

    "send received cti objects to the initialized network connection" in new Helper {
      var (ref, ctiLink) = actor("sendreceive")
      val ctiMessage: JSONObject =
        messageFactory.createDial(new Endpoint(EndpointType.PHONE, "1234"))
      val connection: TestProbe = TestProbe()
      ctiLink.connection = connection.ref
      ctiLink.receiveNoBufferize(ctiMessage)
      connection.expectMsg(
        Tcp.Write(ByteString("%s\n".format(ctiMessage.toString)), ctiLink.Ack)
      )
    }

    "send a linkstatus up to parent on connect" in new Helper {
      val (ref, ctiLink) = actor("linkstatusup")

      ref ! Tcp.Connected(null, null)

      parent.expectMsg(LinkStatusUpdate(up))

    }
    "updates user field with user info received in start message and send start login to login step" in new Helper {
      val (ref, ctiLink) = actor("start")

      val user: XucUser = getXucUser("username", "1234")
      ref ! Start(user)

      ctiLoginStep.expectMsg(StartLogin(user))

    }
  }

  "A CtiLink stop on failing actor" should {
    "send a linkstatus down message on peer disconnect and stop itself" in new Helper {
      val ref: TestActorRef[CtiLink] = errorActorKilled("linkstatusdown")

      val probe: TestProbe = TestProbe()
      probe.watch(ref)

      ref ! Tcp.PeerClosed

      parent.expectMsg(LinkStatusUpdate(down))

      probe.expectTerminated(ref)
    }
    "Stop itself on connection closed" in new Helper {
      val ref: TestActorRef[CtiLink] = errorActorKilled("connectionClosed")

      val probe: TestProbe = TestProbe()
      probe.watch(ref)

      ref ! Tcp.Closed

      probe.expectTerminated(ref)
    }
    "Stop itself on connect failed" in new Helper {
      val ref: TestActorRef[CtiLink] = errorActorKilled("connectfailed")

      val probe: TestProbe = TestProbe()
      probe.watch(ref)

      ref ! Tcp.CommandFailed(Tcp.Connect(new InetSocketAddress(45000)))

      probe.expectTerminated(ref)
    }
    "Stop itself on write failed" in new Helper {
      val ref: TestActorRef[CtiLink] = errorActorKilled("writefailed")

      val probe: TestProbe = TestProbe()
      probe.watch(ref)

      ref ! Tcp.CommandFailed(Tcp.Write(ByteString.empty, Tcp.NoAck(null)))

      probe.expectTerminated(ref)
    }

    "A CtiLink restart on failing actor" should {

      "send a linkstatus down message on peer disconnect and schedule restart" in new Helper {
        var (ref, a) = errorActorRestart("linkstatusdown")
        a.connection = TestProbe().ref

        ref ! Tcp.PeerClosed

        parent.expectMsg(LinkStatusUpdate(down))

        a.connection shouldBe null

        verify(a.scheduler).scheduleOnce(
          any[FiniteDuration],
          same(ref),
          any[Restart]
        )(any[ExecutionContext], any[ActorRef])
      }
      """Restart itself on connection closed and clear the connection
        |Avoids sending a Tcp.Close to a connection which is not always closed, in this case this reschedule a restart
      """.stripMargin in new Helper {
        var (ref, a) = errorActorRestart("TcpClosed")
        a.connection = TestProbe().ref

        ref ! Tcp.Closed

        a.connection shouldBe null

        verify(a.scheduler).scheduleOnce(
          any[FiniteDuration],
          same(ref),
          any[Restart]
        )(any[ExecutionContext], any[ActorRef])
      }
      "Restart itself on connect failed" in new Helper {
        var (ref, a) = errorActorRestart("linkstatusdown")

        ref ! Tcp.CommandFailed(Tcp.Connect(new InetSocketAddress(45000)))

        verify(a.scheduler).scheduleOnce(
          any[FiniteDuration],
          same(ref),
          any[Restart]
        )(any[ExecutionContext], any[ActorRef])
      }
      "Restart itself  on write failed" in new Helper {
        var (ref, a) = errorActorRestart("linkstatusdown")

        ref ! Tcp.CommandFailed(Tcp.Write(ByteString.empty, Tcp.NoAck(null)))

        verify(a.scheduler).scheduleOnce(
          any[FiniteDuration],
          same(ref),
          any[Restart]
        )(any[ExecutionContext], any[ActorRef])
      }
    }
  }
}
