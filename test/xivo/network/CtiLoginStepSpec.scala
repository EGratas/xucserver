package xivo.network

import org.apache.pekko.actor.{ActorRef, Props}
import org.apache.pekko.testkit.TestActorRef
import pekkotest.TestKitSpec
import org.json.JSONObject
import org.mockito.Mockito.{verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{LoginCapasAck, LoginIdAck, LoginPassAck}
import org.xivo.cti.model.{Capacities, UserStatus, Xlet}
import play.api.test.Helpers.running
import xuctest.XucUserHelper
import models.XucUser

class CtiLoginStepSpec
    extends TestKitSpec("CtiLinkSpec")
    with MockitoSugar
    with XucUserHelper
    with GuiceOneAppPerSuite {
  val user: XucUser                   = getXucUser("thierry", "wer554.", Some("2002"))
  var ctiLoginStep: ActorRef = null
  val messageFactory: MessageFactory = mock[MessageFactory]

  class Helper {
    def actor(): (TestActorRef[CtiLoginStep], CtiLoginStep) = {
      val a = TestActorRef(new CtiLoginStep(messageFactory))
      (a, a.underlyingActor)
    }
  }

  override def beforeAll(): Unit = {
    running(app) {
      ctiLoginStep = system.actorOf(
        Props(new CtiLoginStep(messageFactory)),
        user.username + "CtiLoginStep"
      )
    }
  }

  "A CtiLoginStep actor" should {

    "send LoginId message to the Cti server when receiving StartLogin command message" in new Helper {
      val (ref, ctiLink) = actor()
      when(
        messageFactory.createLoginId(
          "Fred",
          "Fred" + CtiLoginStep.XivoCtiIdentitySuffix
        )
      ).thenReturn(new JSONObject())

      ref ! StartLogin(getXucUser("Fred", "pwd"))
      expectMsgClass(classOf[JSONObject])
      verify(messageFactory).createLoginId(
        "Fred",
        "Fred" + CtiLoginStep.XivoCtiIdentitySuffix
      )
      ctiLink.user should be(getXucUser("Fred", "pwd"))
    }

    "send LoginPass message to the Cti server when receiving LoginIdAck message" in new Helper {
      val (ref, ctiLink) = actor()
      val user: XucUser = getXucUser("Joe", "pwd")
      ctiLink.user = user
      val loginIdAck = new LoginIdAck()
      loginIdAck.sesssionId = "1564877"
      when(
        messageFactory.createLoginPass(
          user.xivoUser.password.get,
          loginIdAck.sesssionId
        )
      ).thenReturn(new JSONObject())

      ref ! loginIdAck
      expectMsgClass(classOf[JSONObject])
      verify(messageFactory).createLoginPass(
        user.xivoUser.password.get,
        loginIdAck.sesssionId
      )

    }

    "send LoginCapas message to the Cti server when receiving LoginPassAck" in new Helper {
      val (ref, ctiLink) = actor()
      val user: XucUser = getXucUser("Joe", "pwd", Some("12345"))
      ctiLink.user = user
      val loginPassAck = new LoginPassAck()
      loginPassAck.capalist = new java.util.ArrayList[Integer]()
      val capaId = 3
      loginPassAck.capalist.add(capaId)
      when(messageFactory.createLoginCapas(capaId, user.phoneNumber.get))
        .thenReturn(new JSONObject())

      ref ! loginPassAck

      expectMsgClass(classOf[JSONObject])
      verify(messageFactory).createLoginCapas(capaId, user.phoneNumber.get)
    }

    "send LoggedOn message to its parent when receiving LoginCapasAck" in new Helper {
      val (ref, ctiLink) = actor()
      ctiLink.user = getXucUser("thierry", "wer554.", Some("2002"))

      val loginCapasAck: LoginCapasAck = createLoginCapasWithUserStatues
      loginCapasAck.userId = "34"
      ref ! loginCapasAck
      expectMsg(
        LoggedOn(
          ctiLink.user,
          "34"
        )
      )
    }

  }

  private def createLoginCapasWithUserStatues: LoginCapasAck = {
    val loginCapasAck = new LoginCapasAck()

    loginCapasAck.capacities = new Capacities()
    val userStatuses: java.util.List[UserStatus] = java.util.Arrays
      .asList(new UserStatus("available"), new UserStatus("disconnected"))
    loginCapasAck.capacities.setUsersStatuses(userStatuses)
    val xlets = new java.util.LinkedList[Xlet]()
    loginCapasAck.xlets = xlets
    loginCapasAck
  }
}
