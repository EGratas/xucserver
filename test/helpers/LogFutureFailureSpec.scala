package helpers

import org.scalatest.concurrent.ScalaFutures
import play.api.Logger
import org.slf4j.{Logger => Slf4jLogger}
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class LogFutureFailureSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar
    with ScalaFutures {
  "LogFutureFailure" should {
    "log an error and return the failure back" in {
      val slfLogger = mock[Slf4jLogger]
      val logger    = new Logger(slfLogger)
      val t         = new Exception("Some exception")
      val message   = "My message"
      when(slfLogger.isErrorEnabled()).thenReturn(true)

      val r = LogFutureFailure.logFailure(message)(logger).apply(t)

      verify(slfLogger).error(message, t)
      whenReady(r.failed) { e =>
        e should be(t)
      }
    }
  }
}
