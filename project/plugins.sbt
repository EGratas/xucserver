// Comment to get more information during initialization
logLevel := Level.Warn

// The Typesafe repository
resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

resolvers += Resolver.file("LocalIvy", file(Path.userHome + "/.ivy2/local"))(
  Resolver.ivyStylePatterns
)

// Use the Play sbt plugin for Play projects
addSbtPlugin("org.playframework" % "sbt-plugin" % "3.0.0")

addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.9")

addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.11.0")

addSbtPlugin("org.clapper" % "sbt-editsource" % "1.0.0")

//addSbtPlugin("org.scoverage" % "sbt-scoverage" % "2.0.9") not migrated to scala 3

addSbtPlugin("org.scala-debugger" % "sbt-jdi-tools" % "1.1.1")

addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.11.1")

addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.5.2")
