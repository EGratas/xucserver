import play.sbt.PlayImport._
import sbt._

object Version {
  val asteriskjava       = "2.0.4.c.XIVOCC"
  val pekko              = "1.0.1"
  val xivojavactilib     = "2018.05.08"
  val playauthentication = "2023.11.00-play3"
  val metrics            = "4.2.20"
  val dbunit             = "2.7.3"
  val scalatestplay      = "7.0.0"
  val postgresql         = "42.6.0"
  val nScalaTime         = "2.30.0"
  val rabbitmq           = "5.19.0"
  val quartz             = "2.3.2"
  val play               = "3.0.0"
  val http4sVersion      = "0.21.34"
  val reflect            = "2.13.9"
  val anorm              = "2.7.0"
}

object Library {
  val asteriskjava = ("org.asteriskjava" % "asterisk-java" % Version.asteriskjava
    exclude ("javax.jms", "jms") exclude ("com.sun.jdmk", "jmxtools") exclude ("com.sun.jmx", "jmxri"))
  val xivojavactilib = "org.xivo" % "xivo-javactilib" % Version.xivojavactilib
  val playauthentication =
    "solutions.xivo" %% "play-authentication" % Version.playauthentication
  val metrics    = "io.dropwizard.metrics" % "metrics-core" % Version.metrics
  val metricsJvm = "io.dropwizard.metrics" % "metrics-jvm"  % Version.metrics
  val metricsJmx = "io.dropwizard.metrics" % "metrics-jmx"  % Version.metrics
  val metricsLogback =
    "io.dropwizard.metrics" % "metrics-logback" % Version.metrics
  val pekkoTestkit = "org.apache.pekko" %% "pekko-testkit" % Version.pekko
  val pekkoStreamTestkit =
    "org.apache.pekko" %% "pekko-stream-testkit" % Version.pekko
  val pekkoActor  = "org.apache.pekko" %% "pekko-actor"       % Version.pekko
  val pekkoStream = "org.apache.pekko" %% "pekko-stream"      % Version.pekko
  val pekkoSlf4j  = "org.apache.pekko" %% "pekko-slf4j"       % Version.pekko
  val pekkoTyped  = "org.apache.pekko" %% "pekko-actor-typed" % Version.pekko
  val pekkoJackson =
    "org.apache.pekko" %% "pekko-serialization-jackson" % Version.pekko
  val pekkoCluster = "org.apache.pekko" %% "pekko-cluster" % Version.pekko
  val pekkoClusterTyped =
    "org.apache.pekko" %% "pekko-cluster-typed" % Version.pekko
  val pekkoClusterTools =
    "org.apache.pekko" %% "pekko-cluster-tools" % Version.pekko

  val dbunit     = "org.dbunit"     % "dbunit"     % Version.dbunit
  val postgresql = "org.postgresql" % "postgresql" % Version.postgresql
  val nScalaTime =
    "com.github.nscala-time" %% "nscala-time" % Version.nScalaTime
  val rabbitmq = "com.rabbitmq"         % "amqp-client" % Version.rabbitmq
  val quartz   = "org.quartz-scheduler" % "quartz"      % Version.quartz
  val xerces   = "xerces"               % "xercesImpl"  % "2.12.1"

  lazy val scalaReflect =
    "org.scala-lang" % "scala-reflect" % Version.reflect
  lazy val decline    = "com.monovore"  %% "decline-effect" % "2.1.0"
  lazy val catscore   = "org.typelevel" %% "cats-core"      % "2.9.0"
  lazy val catseffect = "org.typelevel" %% "cats-effect"    % "3.1.1"
  lazy val oslib      = "com.lihaoyi"   %% "os-lib"         % "0.9.1"
  lazy val mockitocore = "org.mockito" % "mockito-core"   % "5.7.0"
  lazy val jaxbapi        = "jakarta.xml.bind" % "jakarta.xml.bind-api" % "2.3.3"
  lazy val jaxbruntime    = "org.glassfish.jaxb" % "jaxb-runtime" % "2.3.3"
}

object PlayLibrary {
  val playws = "org.playframework" %% "play-ahc-ws-standalone" % Version.play
  val playwsjson =
    "org.playframework" %% "play-ws-standalone-json" % Version.play
  val anorm = "org.playframework.anorm" %% "anorm" % Version.anorm
  val jwt = "com.github.jwt-scala" %% "jwt-play-json" % "9.4.5"
  val playjsonjoda = "org.playframework" %% "play-json-joda" % Version.play
  val scalatestplay =
    "org.scalatestplus.play" %% "scalatestplus-play" % Version.scalatestplay
}

object CliDependencies {
  import Library._

  val libraries: Seq[ModuleID] = Seq(
    catscore,
    catseffect,
    decline,
    scalaReflect,
    oslib
  )
}

object Dependencies {

  import Library._
  import PlayLibrary._

  val scalaVersion = "3.3.1"
  val play         = "3.0.0"

  val resolutionRepos: Seq[MavenRepository] = Seq(
    "Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository",
    "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"
  )

  val runDep: Seq[sbt.ModuleID] = run(
    asteriskjava,
    xivojavactilib,
    playauthentication,
    playws,
    playwsjson,
    javaWs,
    metrics,
    metricsJvm,
    metricsJmx,
    metricsLogback,
    postgresql,
    jdbc,
    anorm,
    guice,
    ws,
    nScalaTime,
    jwt,
    filters,
    rabbitmq,
    quartz,
    xerces,
    playjsonjoda,
    catscore,
    pekkoActor,
    pekkoTyped,
    pekkoStream,
    pekkoSlf4j,
    pekkoJackson,
    pekkoClusterTools,
    pekkoCluster,
    pekkoClusterTyped,
    jaxbapi,
    jaxbruntime
  )

  val testDep: Seq[sbt.ModuleID] = test(
    pekkoTestkit,
    dbunit,
    scalatestplay,
    pekkoStreamTestkit,
    mockitocore
  )

  def run(deps: ModuleID*): Seq[ModuleID]  = deps
  def test(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
