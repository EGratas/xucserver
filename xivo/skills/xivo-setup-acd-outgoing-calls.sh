#!/bin/bash

# See: https://documentation.xivo.solutions/en/latest/xivocc/install_and_config/installation/installation.html#acd-outgoing-calls-for-call-blending

set -xe

cd /etc/asterisk/extensions_extra.d/
wget https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/xuc_outcall_acd.conf
chown asterisk:www-data xuc_outcall_acd.conf
chmod 660 xuc_outcall_acd.conf

cd /usr/local/sbin/
wget https://gitlab.com/xivoxc/xucserver/raw/master/xivo/skills/generate_agent_skills.py
chown root:root generate_agent_skills.py
chmod 755 generate_agent_skills.py

cd /etc/asterisk/
mv queueskills.conf queueskills.conf.BACKUP
wget https://gitlab.com/xivoxc/xucserver/raw/master/xivo/skills/queueskills.conf
chown asterisk:www-data queueskills.conf
chmod 660 queueskills.conf

xivo-fix-paths-rights
asterisk -rx 'core reload'