package xuccli

import cats.implicits._
import cats.effect._
import cats._
import javax.management.remote.{
  JMXConnector,
  JMXConnectorFactory,
  JMXServiceURL
}
import javax.management.MBeanServerConnection
import scala.reflect.runtime.universe.TypeTag
import scala.util.Try
import sun.management.ConnectorAddressLink
import com.sun.tools.attach.VirtualMachine

class JmxClientBuilder private (val pid: Int) {

  private def getPidAddress(): IO[String] = {
    IO.fromTry(Try(ConnectorAddressLink.importFrom(pid)))
  }

  private def startManagementAgent(): IO[Unit] = {
    val home      = os.Path(System.getProperty("java.home"))
    val jreLib    = home / "jre" / "lib" / "management-agent.jar"
    val directLib = home / "lib" / "management-agent.jar"

    IO(os.exists(jreLib))
      .flatMap(exists =>
        if (exists) IO(jreLib)
        else IO.raiseError(new Exception("manage-agent.jar not found"))
      )
      .orElse(
        IO(os.exists(directLib))
          .flatMap(exists =>
            if (exists) IO(directLib)
            else IO.raiseError(new Exception("manage-agent.jar not found"))
          )
      )
      .flatMap(lib =>
        IO.fromTry(
          Try(VirtualMachine.attach(pid.toString).loadAgent(lib.toString))
        )
      )
  }

  private def connect: Resource[IO, JMXConnector] = {
    val address = getPidAddress().flatMap(address => {
      if (address == null) {
        startManagementAgent() >> getPidAddress()
      } else {
        IO.pure(address)
      }
    })

    Resource.make({
      address
        .flatMap(adr => IO.fromTry(Try(new JMXServiceURL(adr))))
        .flatMap(url => IO.fromTry(Try(JMXConnectorFactory.connect(url, null))))
    })(cnx => IO.delay(cnx.close))
  }

  def resource: Resource[IO, MBeanServerConnection] =
    for {
      jmxCnx <- connect
    } yield jmxCnx.getMBeanServerConnection

}

object JmxClientBuilder {
  def apply(pid: Int): JmxClientBuilder =
    new JmxClientBuilder(pid)
}

object JmxClient {

  private def unsafeGetAttribute[T](
      p: BeanProperty[T]
  )(implicit server: MBeanServerConnection): Try[Object] =
    Try(server.getAttribute(p.bean.toObjectName(), p.attribute))

  private def safeCast[T](o: Object)(implicit typeTag: TypeTag[T]): Try[T] =
    Try(o.asInstanceOf[T])

  def getAttribute[T](p: BeanProperty[T])(implicit
      server: MBeanServerConnection,
      typeTag: TypeTag[T],
      ae: ApplicativeError[IO, Throwable]
  ): IO[BeanResult[T]] =
    IO(unsafeGetAttribute(p))
      .flatMap(IO.fromTry)
      .flatMap(o => IO(safeCast[T](o)))
      .flatMap(IO.fromTry)
      .map(v => BeanResult[T](p, v))

  def getAttributes(
      l: List[BeanProperty[_]]
  )(implicit server: MBeanServerConnection): IO[List[BeanResult[_]]] =
    l.map(b => getAttribute(b)).sequence

  def invoke(
      op: BeanOperation
  )(implicit server: MBeanServerConnection): IO[Unit] =
    IO.fromTry(
      Try(server.invoke(op.bean.toObjectName(), op.operation, null, null))
    )

}
